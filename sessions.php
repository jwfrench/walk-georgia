<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/sessions_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Sessions', $_SESSION['valid']);
HTML_ELEMENT::top_nav();
//START EDITING HERE
?>
<!-- Beginning of Main Section -->
<div class="main">
	<!-- Page Header -->
	<div class="peach-bg" style="margin-top:-10px;">
		<div class="row" style="margin-top:40px;">
			<div class="large-12 columns">
				<h1 class="custom-font-big-white" style="float:left;">
                	my sessions
                </h1>

				<hr style="color:white; border-color:white; margin-top:-5px; margin-bottom:20px;">
			</div>
		</div>
		<div class="row">
			<div class="large-6 columns">
				<a class="button small round" href="#" data-reveal-id="create-session">Create a Session</a>
				<a class="button small round secondary" href="#" data-reveal-id="completed-sessions">View Completed Sessions</a>
                <a class="button peachButton small round hide-for-small" style="float:right;" href="faq.php#G1" target="_blank">Help</a>
			</div>
		</div>
	</div>
	<!-- End Page Header -->

	<!-- Current Sessions -->
	<div class="row" style="margin-top:20px;">
		<div class="large-12 columns">
			<h2 class="global-h2">Current Sessions:</h2>
			<!-- Sessions Module -->
			<?php $current = SESSIONS::listCurrentSessions(); ?>
		</div>
	</div>
	<!-- End Current Sessions -->

	<!-- Create a Session Modal -->
	<div id="create-session" class="reveal-modal" data-reveal>
		<div class="row">
		<div class="large-12 columns">
			<h2 class="custom-font-small-blue">Create a Session</h2>
			<hr />
		</div>
		</div>
		<div class="row">
		<div class="large-12 columns">
		<form action="session/create_session.php" id="session-form" method="post" data-abide >
                    <div>
			<label>Session Name
			<input type="text" id="S_NAME" name="S_NAME" placeholder="Enter your session name." required pattern="[a-zA-Z]+">
                        </label>
                        <small class="error">A session name is required and must be a string.</small>
                   </div>
			<input type="hidden" id="PARENT_ID" name="PARENT_ID" value="<?php echo $_SESSION['ID']; ?>">
			<label>Session Description</label>
			<textarea id="S_DESC" name="S_DESC" placeholder="Enter your session description here." style="height:100px;"></textarea>
			<label>Session Visibility</label>
			<select>
			<option id="S_VIS_1" name="S_VIS_1" value="1">Public (Any user can find the session and join)</option>
			<option id="S_VIS_0" name="S_VIS_0" value="0">Private (Only users who have the link to the session's page may join)</option>
			</select>

			<!-- Start Date -->
			<?php
			$day = date('d');
			$month = date('m');
			$year = date('Y');
			?>
			<div class="row">
			<div class="large-6 medium-12 columns">
				<h3 class="global-h2">Start Date:</h3>
				<div class="row date">

					<!-- Month -->
					<div class="large-4 medium-4 columns">
					<label>Month
						<select id="month1" name="month1" onchange="checkmonth1(this)" required>
							<option value="1" <?php if ($month=='1'){echo 'selected="selected"';} ?>>01 January</option>
						 		<option value="2" <?php if ($month=='2'){echo 'selected="selected"';} ?>>02 February</option>
							 <option value="3" <?php if ($month=='3'){echo 'selected="selected"';} ?>>03 March</option>
							 <option value="4" <?php if ($month=='4'){echo 'selected="selected"';} ?>>04 April</option>
							 <option value="5" <?php if ($month=='5'){echo 'selected="selected"';} ?>>05 May</option>
							 <option value="6" <?php if ($month=='6'){echo 'selected="selected"';} ?>>06 June</option>
							 <option value="7" <?php if ($month=='7'){echo 'selected="selected"';} ?>>07 July</option>
							 <option value="8" <?php if ($month=='8'){echo 'selected="selected"';} ?>>08 August</option>
							 <option value="9" <?php if ($month=='9'){echo 'selected="selected"';} ?>>09 September</option>
							 <option value="10" <?php if ($month=='10'){echo 'selected="selected"';} ?>>10 October</option>
							 <option value="11" <?php if ($month=='11'){echo 'selected="selected"';} ?>>11 November</option>
							 <option value="12" <?php if ($month=='12'){echo 'selected="selected"';} ?>>12 December</option>
						</select>
					</label>
					</div>
					<!-- End Month -->

					<!-- Day -->
					<div class="large-4 medium-4 columns">
					<label>Day
						<select id="day1" name="day1" onchange="checkday1(this)" required>
						<option value="1" <?php if ($day=='1'){echo 'selected="selected"';} ?>>01</option>
					<option value="2" <?php if ($day=='2'){echo 'selected="selected"';} ?>>02</option>
					<option value="3" <?php if ($day=='3'){echo 'selected="selected"';} ?>>03</option>
					<option value="4" <?php if ($day=='4'){echo 'selected="selected"';} ?>>04</option>
					<option value="5" <?php if ($day=='5'){echo 'selected="selected"';} ?>>05</option>
					<option value="6" <?php if ($day=='6'){echo 'selected="selected"';} ?>>06</option>
					<option value="7" <?php if ($day=='7'){echo 'selected="selected"';} ?>>07</option>
					<option value="8" <?php if ($day=='8'){echo 'selected="selected"';} ?>>08</option>
					<option value="9" <?php if ($day=='9'){echo 'selected="selected"';} ?>>09</option>
					<option value="10" <?php if ($day=='10'){echo 'selected="selected"';} ?>>10</option>
					<option value="11" <?php if ($day=='11'){echo 'selected="selected"';} ?>>11</option>
					<option value="12" <?php if ($day=='12'){echo 'selected="selected"';} ?>>12</option>
					<option value="13" <?php if ($day=='13'){echo 'selected="selected"';} ?>>13</option>
					<option value="14" <?php if ($day=='14'){echo 'selected="selected"';} ?>>14</option>
					<option value="15" <?php if ($day=='15'){echo 'selected="selected"';} ?>>15</option>
					<option value="16" <?php if ($day=='16'){echo 'selected="selected"';} ?>>16</option>
					<option value="17" <?php if ($day=='17'){echo 'selected="selected"';} ?>>17</option>
					<option value="18" <?php if ($day=='18'){echo 'selected="selected"';} ?>>18</option>
					<option value="19" <?php if ($day=='19'){echo 'selected="selected"';} ?>>19</option>
					<option value="20" <?php if ($day=='20'){echo 'selected="selected"';} ?>>20</option>
					<option value="21" <?php if ($day=='21'){echo 'selected="selected"';} ?>>21</option>
					<option value="22" <?php if ($day=='22'){echo 'selected="selected"';} ?>>22</option>
					<option value="23" <?php if ($day=='23'){echo 'selected="selected"';} ?>>23</option>
					<option value="24" <?php if ($day=='24'){echo 'selected="selected"';} ?>>24</option>
					<option value="25" <?php if ($day=='25'){echo 'selected="selected"';} ?>>25</option>
					<option value="26" <?php if ($day=='26'){echo 'selected="selected"';} ?>>26</option>
					<option value="27" <?php if ($day=='27'){echo 'selected="selected"';} ?>>27</option>
					<option value="28" <?php if ($day=='28'){echo 'selected="selected"';} ?>>28</option>
					<option value="29" <?php if ($day=='29'){echo 'selected="selected"';} ?>>29</option>
					<option value="30" <?php if ($day=='30'){echo 'selected="selected"';} ?>>30</option>
					<option value="31" <?php if ($day=='31'){echo 'selected="selected"';} ?>>31</option>
						</select>
					</label>
					 </div>
					 <!-- End Day -->

					 <!-- Year -->
					 <div class="large-4 medium-4 columns">
					 <label>Year
						 <select id="year1" name="year1" onchange="checkyear1(this)" required>
						 <option value="2008" <?php if ($year=='2008'){echo 'selected="selected"';} ?>>2008</option>
					<option value="2009" <?php if ($year=='2009'){echo 'selected="selected"';} ?>>2009</option>
					<option value="2010" <?php if ($year=='2010'){echo 'selected="selected"';} ?>>2010</option>
					<option value="2011" <?php if ($year=='2011'){echo 'selected="selected"';} ?>>2011</option>
					<option value="2012" <?php if ($year=='2012'){echo 'selected="selected"';} ?>>2012</option>
					<option value="2013" <?php if ($year=='2013'){echo 'selected="selected"';} ?>>2013</option>
					<option value="2014" <?php if ($year=='2014'){echo 'selected="selected"';} ?>>2014</option>
					<option value="2015" <?php if ($year=='2015'){echo 'selected="selected"';} ?>>2015</option>
                                        <option value="2016" <?php if ($year=='2016'){echo 'selected="selected"';} ?>>2016</option>
						 </select>
					 </label>
					 </div>
					 <!-- End Year -->

				 </div>
				</div>
				</div>
				<!-- End Start Date -->

				<!-- End Date -->
			<div class="row">
			<div class="large-6 medium-12 columns">
				<h3 class="global-h2">End Date:</h3>
				<div class="row date">

					<!-- Month -->
					<div class="large-4 medium-4 columns">
					<label>Month
						<select id="month2" name="month2" onchange="checkmonth2(this)" required>
						<option value="1" <?php if ($month=='1'){echo 'selected="selected"';} ?>>01 January</option>
					<option value="2" <?php if ($month=='2'){echo 'selected="selected"';} ?>>02 February</option>
					<option value="3" <?php if ($month=='3'){echo 'selected="selected"';} ?>>03 March</option>
					<option value="4" <?php if ($month=='4'){echo 'selected="selected"';} ?>>04 April</option>
					<option value="5" <?php if ($month=='5'){echo 'selected="selected"';} ?>>05 May</option>
					<option value="6" <?php if ($month=='6'){echo 'selected="selected"';} ?>>06 June</option>
					<option value="7" <?php if ($month=='7'){echo 'selected="selected"';} ?>>07 July</option>
					<option value="8" <?php if ($month=='8'){echo 'selected="selected"';} ?>>08 August</option>
					<option value="9" <?php if ($month=='9'){echo 'selected="selected"';} ?>>09 September</option>
					<option value="10" <?php if ($month=='10'){echo 'selected="selected"';} ?>>10 October</option>
					<option value="11" <?php if ($month=='11'){echo 'selected="selected"';} ?>>11 November</option>
					<option value="12" <?php if ($month=='12'){echo 'selected="selected"';} ?>>12 December</option>
						</select>
					</label>
					</div>
					<!-- End Month -->

					<!-- Day -->
					<div class="large-4 medium-4 columns">
					<label>Day
						<select id="day2" name="day2" onchange="checkday2(this)" required>
						<option value="1" <?php if ($day=='1'){echo 'selected="selected"';} ?>>01</option>
					<option value="2" <?php if ($day=='2'){echo 'selected="selected"';} ?>>02</option>
					<option value="3" <?php if ($day=='3'){echo 'selected="selected"';} ?>>03</option>
					<option value="4" <?php if ($day=='4'){echo 'selected="selected"';} ?>>04</option>
					<option value="5" <?php if ($day=='5'){echo 'selected="selected"';} ?>>05</option>
					<option value="6" <?php if ($day=='6'){echo 'selected="selected"';} ?>>06</option>
					<option value="7" <?php if ($day=='7'){echo 'selected="selected"';} ?>>07</option>
					<option value="8" <?php if ($day=='8'){echo 'selected="selected"';} ?>>08</option>
					<option value="9" <?php if ($day=='9'){echo 'selected="selected"';} ?>>09</option>
					<option value="10" <?php if ($day=='10'){echo 'selected="selected"';} ?>>10</option>
					<option value="11" <?php if ($day=='11'){echo 'selected="selected"';} ?>>11</option>
					<option value="12" <?php if ($day=='12'){echo 'selected="selected"';} ?>>12</option>
					<option value="13" <?php if ($day=='13'){echo 'selected="selected"';} ?>>13</option>
					<option value="14" <?php if ($day=='14'){echo 'selected="selected"';} ?>>14</option>
					<option value="15" <?php if ($day=='15'){echo 'selected="selected"';} ?>>15</option>
					<option value="16" <?php if ($day=='16'){echo 'selected="selected"';} ?>>16</option>
					<option value="17" <?php if ($day=='17'){echo 'selected="selected"';} ?>>17</option>
					<option value="18" <?php if ($day=='18'){echo 'selected="selected"';} ?>>18</option>
					<option value="19" <?php if ($day=='19'){echo 'selected="selected"';} ?>>19</option>
					<option value="20" <?php if ($day=='20'){echo 'selected="selected"';} ?>>20</option>
					<option value="21" <?php if ($day=='21'){echo 'selected="selected"';} ?>>21</option>
					<option value="22" <?php if ($day=='22'){echo 'selected="selected"';} ?>>22</option>
					<option value="23" <?php if ($day=='23'){echo 'selected="selected"';} ?>>23</option>
					<option value="24" <?php if ($day=='24'){echo 'selected="selected"';} ?>>24</option>
					<option value="25" <?php if ($day=='25'){echo 'selected="selected"';} ?>>25</option>
					<option value="26" <?php if ($day=='26'){echo 'selected="selected"';} ?>>26</option>
					<option value="27" <?php if ($day=='27'){echo 'selected="selected"';} ?>>27</option>
					<option value="28" <?php if ($day=='28'){echo 'selected="selected"';} ?>>28</option>
					<option value="29" <?php if ($day=='29'){echo 'selected="selected"';} ?>>29</option>
					<option value="30" <?php if ($day=='30'){echo 'selected="selected"';} ?>>30</option>
					<option value="31" <?php if ($day=='31'){echo 'selected="selected"';} ?>>31</option>
						</select>
					</label>
					</div>
					 <!-- End Day -->

					 <!-- Year -->
					 <div class="large-4 medium-4 columns">
					 <label>Year
						 <select id="year2" name="year2" onchange="checkyear2(this)" required>
						 <option value="2008" <?php if ($year=='2008'){echo 'selected="selected"';} ?>>2008</option>
					<option value="2009" <?php if ($year=='2009'){echo 'selected="selected"';} ?>>2009</option>
					<option value="2010" <?php if ($year=='2010'){echo 'selected="selected"';} ?>>2010</option>
					<option value="2011" <?php if ($year=='2011'){echo 'selected="selected"';} ?>>2011</option>
					<option value="2012" <?php if ($year=='2012'){echo 'selected="selected"';} ?>>2012</option>
					<option value="2013" <?php if ($year=='2013'){echo 'selected="selected"';} ?>>2013</option>
					<option value="2014" <?php if ($year=='2014'){echo 'selected="selected"';} ?>>2014</option>
					<option value="2015" <?php if ($year=='2015'){echo 'selected="selected"';} ?>>2015</option>
                                        <option value="2016" <?php if ($year=='2016'){echo 'selected="selected"';} ?>>2016</option>
						 </select>
					 </label>
					 </div>
					 <!-- End Year -->

				</div>
				</div>
				</div>
				<!-- End End Date -->



			<button class="tiny submit" onclick="return submit_form();">Done</button>
		</form>
		</div>
	</div>

	<a class="close-reveal-modal">&#215;</a>
	</div>
	<!-- End Create a Session Modal -->

	<!-- Completed Sessions Modal -->
	<div id="completed-sessions" class="reveal-modal small" data-reveal>
		<div class="row">
		<div class="large-12 columns">
			<h2 class="custom-font-small-blue">Completed Sessions:</h2>
			<hr />
			<ul style="list-style:none;">
				<?php $upcoming = SESSIONS::listCompletedSessions(); ?>
			</ul>
		</div>
		</div>
		<a class="close-reveal-modal">&#215;</a>
	</div>
	<!-- End Completed Sessions Modal -->

	<!-- Upcoming Sessions -->

	<div class="row" style="margin-top:20px;">
	<div class="large-12 columns">
		<h2 class="global-h2">Upcoming Sessions:</h2>
		<?php $upcoming = SESSIONS::listUpcomingSessions(); ?>
	</div>
	</div>

	<!-- End Upcoming Sessions -->

<!-- End of the Main Section -->
</div>
<script>
                //create date object using current date
                       var today = new Date();
                       var month1 = today.getMonth() +1;
                       var day1 = today.getDate();
                       var year1 = today.getFullYear();
                       var month2 = today.getMonth() +1;
                       var day2 = today.getDate();
                       var year2 = today.getFullYear();
                //change date values if new dropdown items selected for start date
                       function checkmonth1(selBox) {
                            month1 = parseInt(selBox.options[selBox.selectedIndex].value);
                        }
                        function checkday1(selBox) {
                            day1 = parseInt(selBox.options[selBox.selectedIndex].value);
                        }
                        function checkyear1(selBox) {
                            year1 = parseInt(selBox.options[selBox.selectedIndex].value);
                        }
                //change date values if new dropdown items selected for end date
                       function checkmonth2(selBox) {
                            month2 = parseInt(selBox.options[selBox.selectedIndex].value);
                        }
                        function checkday2(selBox) {
                            day2 = parseInt(selBox.options[selBox.selectedIndex].value);
                        }
                        function checkyear2(selBox) {
                            year2 = parseInt(selBox.options[selBox.selectedIndex].value);
                        }
                //make new date object out of date variables changed by dropdowns; check if date is in future: if so return false and prevent submission
                        function validate() {
                            var d1 = new Date();
                            var d2 = new Date();
                            d1.setFullYear(year1, month1-1, day1);
                            d1.setHours(0);
                            d1.setMinutes(0);
                            d2.setFullYear(year2, month2-1, day2);
                            d2.setHours(0);
                            d2.setMinutes(0);
                            if(d2 < d1){
                                alert("Your session end date has to be after your session start date.");
                                return false;
                            }
                        return true;
            }
                </script>
<!-- End of the Main Section -->
<?php

//END EDITING HERE

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script>
		<?php echo $current; echo $upcoming;?>
	</script>
	<!-- <script type="text/javascript" src="js/frontend.js"></script> -->
	<!-- End Footer -->
	</body>
</html>
