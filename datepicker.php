<?php
include_once("lib/template_api.php");
include_once("lib/back_api.php");
include_once("lib/groups_api.php");
$ss = SESSION::secure_session();
if($_GET['logout']==1){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Registration', $_SESSION['valid']);
HTML_ELEMENT::top_nav();

//EDITING STARTS HERE
?>
<div class="row">
              <div class="large-6 columns">
                <div class="row date collapse" id="dp3" data-date="12-02-2012" data-date-format="dd-mm-yyyy">
                  <div class="small-9 columns">
                    <input class="" size="16" type="text" value="12-02-2012" readonly>
                  </div>
                  <div class="small-3 columns">
                    <span class="postfix end"><i class="fi-calendar size-36"></i></span>
                  </div>
                </div>
              </div>
            </div>
<?php
//EDITING ENDS HERE

HTML_ELEMENT::footer();
//JAVASCRIPTS HERE
?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../js/forms.js"></script>
    <script src="../js/sha512.js"></script>
    <script src="../js/foundation/foundation.js"></script>
    <script src="../js/foundation/foundation.topbar.js"></script>
    <script src="../js/foundation/foundation.abide.js"></script>
	<script type="text/javascript" src="js/foundation-datepicker.js"></script>
    <script>
      $(document).foundation();
    </script>
    <script>
		$(function () {
			$('#dp3').fdatepicker();
		});
	</script>
  <!-- End Footer -->
  </body>
</html>