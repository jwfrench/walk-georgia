<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/groups_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Groups', $_SESSION['valid']);
HTML_ELEMENT::top_nav();

//START EDITING HERE

?>
<!-- Beginning of Main Section -->
<div class="main">
    <?php
  //alerts if a group was just added to favorites or removed from favorites
  if(filter_input(INPUT_GET, 'fav', FILTER_SANITIZE_STRING) != null){
        $name = filter_input(INPUT_GET, 'fav', FILTER_SANITIZE_STRING);
	echo "<div data-alert class=\"alert-box success\" style=\"margin-bottom:0px; text-align: center;\">You have added $name to your favorites <a class=\"close\">&times;</a></div>";
}
 if(filter_input(INPUT_GET, 'remfav', FILTER_SANITIZE_STRING) != null){
	$name = filter_input(INPUT_GET, 'remfav', FILTER_SANITIZE_STRING);
        echo "<div data-alert class=\"alert-box success\" style=\"margin-bottom:0px; text-align: center;\">You have removed $name from your favorites <a class=\"close\">&times;</a></div>";
}
//alerts if a group was just deleted
 if(filter_input(INPUT_GET, 'del', FILTER_SANITIZE_STRING) != null){
        $name = filter_input(INPUT_GET, 'del', FILTER_SANITIZE_STRING);
	echo "<div data-alert class=\"alert-box success\" style=\"margin-bottom:0px; text-align: center;\">You have deleted the group $name<a class=\"close\">&times;</a></div>";
}
?>
<!-- Beginning of Main Section -->

  <!-- Page Header -->
  <div class="blue-bg" style="margin-top:-10px;">
    <div class="row" style="margin-top:40px;">
      <div class="large-12 columns center">
        <h1 class="custom-font-big-white" style="float:left;">My Groups</h1>
        <hr style="color:white; border-color:white; margin-top:-5px; margin-bottom:20px;">

      </div>
    </div>
    <div class="row">
      <div class="large-6 columns">
        <a class="button big round success" href="#" data-reveal-id="create-a-group"><span class="fi-plus" style="margin-right: 10px; font-size: 1.5em;"></span><span style="font-size: 1.5rem;">Create a Group</span></a>
        <a class="button small round" style="" href="faq.php#F1" target="_blank">Help</a>
        <?php /*<a class="button small round secondary" href="#" data-reveal-id="create-a-group"> Manage Groups</a> */ ?>
      </div>
    </div>
  </div>
  <!-- End Page Header -->

  <!-- Favorite Groups -->

  <div class="row" style="margin-top:20px;">
    <div class="large-12 columns">
      <h2 class="global-h2" style="margin-bottom:20px;">Favorite Groups:</h2>
    </div>
  </div>

  <div class="row">
    <div class="large-12 columns">

        <?php
			$favorites = GROUP::list_favorites($_SESSION['ID']);
		?>

    </div>
  </div>

  <!-- End Favorite Groups -->

  <div class="row" style="margin-top:20px;">
    <div class="large-12 columns">
      <h2 class="global-h2">All My Groups</h2>
    </div>
  </div>

  <!-- All My Groups -->
  <div class="bs row">
    <div class="large-12 columns">
      <?php $activegroups = GROUP::listJoinedGroups($_SESSION['ID']);?>
  </div>
  </div>
  <!-- End All My Groups -->

  <!-- Create A Group Modal -->
  <div id='create-a-group' class='reveal-modal' data-reveal>
    <div class='row'>
      <div class="large-4 medium-4 columns center">
          <div class="gray-bg">
            <i class="fi-torsos-all size-60 blue"></i>
            <h2 class="global-h2">Default Group</h2>
            <hr style="margin-top:-5px; margin-bottom:10px;">
            <a href="#" class="button tiny expand" data-reveal-id="default-form">Create Group</a>
            <p class="global-p-small justify">This is the default group type.  It is robust enough to handle pretty much anything you’d want to throw at it.  You can use this type of group to keep track of your fitness with your friends, family, etc.  If you’re not sure what type of group to make, you can’t go wrong with this one.</p>
          </div>
        </div>

        <div class="large-4 medium-4 columns center">
          <div class="white-bg">
            <i class="fi-book-bookmark size-60 blue"></i>
            <h2 class="global-h2">Schools</h2>
            <hr style="margin-top:-5px; margin-bottom:10px;">
            <a href="#" class="button tiny expand" data-reveal-id="school-fork">Create School Group</a>
            <p class="global-p-small justify">If you are a teacher or administrator, then this is the group for you.  For example, if you are a P.E. teacher, this group type allows you to actually log activity for your whole class within the group page itself.  That way, your personal stats stay separate from your class, which makes record keeping much easier. </p>
          </div>
        </div>

        <div class="large-4 medium-4 columns center">
          <div class="white-bg">
            <i class="fi-address-book size-60 blue"></i>
            <h2 class="global-h2">Organizations</h2>
            <hr style="margin-top:-5px; margin-bottom:10px;">
            <a href="#" class="button tiny expand" data-reveal-id="organization-form">Create Organization Group</a>
            <p class="global-p-small justify">If you want to use Walk Georgia to run your company’s wellness program, provide incentives, or just encourage friendly competition among your employees, then this is the group for you.  Or if you are a charity or organization, this template gives you a few tools to make running your program easier. </p>
          </div>
        </div>
    </div>
    <a class="close-reveal-modal">×</a>
  </div>
  <!-- Create A Group Modal -->

  <!-- Default Group Form Modal-->
  <div id='default-form' class='reveal-modal' data-reveal>
    <div class="row">
      <div class="large-12 columns">
        <h2 class="custom-font-small-blue">Create a Group:</h2>
        <hr />
      </div>
    </div>
    <div class='row'>
      <div class="large-12 columns">
        <form action="group/create_group.php" id="group-form" method="post" data-abide>

          <div>
            <label>Group Name</label>
            <input type="text" id="G_NAME" name="G_NAME" maxlength="50" placeholder="Enter your group name." required/>
            <input type="hidden" id="PARENT_ID" name="PARENT_ID" value="<?php echo $_SESSION['UUID'] ?>" />
            <input type="hidden" id="G_ISROOTGROUP" name="G_ISROOTGROUP" value="1" />
            <input type="hidden" id="G_GLOG" name="G_GLOG" value="0" />
            <small class="error">You must enter a group name.</small>
          </div>

          <div>
            <label>Group Description: <span class="description-text-remaining">300/300 Characters remaining</span></label>
            <textarea id="G_DESC" class="G_DESC" name="G_DESC" placeholder="Enter your group description here." style="height:100px;" maxlength="300" required></textarea>
            <small class="error">You must enter a short description of your group.</small>
          </div>

          <div>
            <label>Group Visibility</label>
            <select id="G_VIS" name="G_VIS" required>
              <option id="G_VIS_1" name="G_VIS_0" value="0">Public (Any user can find the group and join)</option>
              <option id="G_VIS_0" name="G_VIS_1" value="1">Private (Only users who have the link to the group's page may join)</option>
            </select>
          </div>

          <div>
          <label>Request Profile Information</label>
          <input id="name" name="name" type="checkbox"><label for="name">Name</label>
          <input id="email" name="email" type="checkbox"><label for="email">Email</label>
          <input id="county" name="county" type="checkbox"><label for="county">County</label>
          </div>

          <h2 class="global-h2">Extra Info:</h2>
        <hr style="margin-top:-5px;" />

        <label>You may want to require new group members to submit specific information when joining, i.e., employee number, birthday, or favorite exercise! If you want to collect extra info from your members, put the required data on the left and your personal answer on the right.</label>
        <br />
        <br />
        <div class="row">
        <div id="extender_info"></div>
        </div>
        <br />
        <br />
        <a class="button tiny round secondary" id="add_info" href="#">Add extra info</a>
        <hr />

       <div class="row">
       <button class="tiny submit" onclick="return submit_form();">Create Group</button>
       </div>

        </form>
      </div>
    </div>
    <a class="close-reveal-modal">×</a>
  </div>
  <!-- End Default Group Form Modal -->

  <!-- Single Classroom Group Form Modal-->
  <div id='single-class-form' class='reveal-modal' data-reveal>
    <div class='row'>
      <div class="large-12 columns">
      <h2 class="custom-font-small-blue">Create a Single Classroom Group:</h2>
      <hr />

      <!-- Group Creation Disclaimer -->
      <div data-alert class="alert-box">
        Disclaimer: Once you create a group, the template can not be changed.  Only create a single classroom group if you are sure no other grades, classes, or schools are needed to be a part of the group as well.  If you do need to include other groups, <a href="#" style="color:white; text-decoration:underline;" data-reveal-id="school-fork">click here to go back</a>.  Otherwise, continue on your way!
      <a href="#" class="close">&times;</a>
      </div>
      <!-- End Group Creation Disclaimer -->

      <form action="group/create_group.php" id="group-form" method="post" data-abide>

        <div>
          <label>Class Name</label>
          <input type="text" id="G_NAME" name="G_NAME" maxlength="50" placeholder="Enter the name of your class." required />
          <input type="hidden" id="PARENT_ID" name="PARENT_ID" value="<?php echo $_SESSION['UUID'] ?>" />
          <input type="hidden" id="G_ISROOTGROUP" name="G_ISROOTGROUP" value="1" />
          <input type="hidden" id="G_GLOG" name="G_GLOG" value="1" />
          <small class="error">You must enter a name for your class.</small>
        </div>

        <div>
          <label>Class Description: <span class="description-text-remaining">300/300 Characters remaining</span></label>
          <textarea id="G_DESC" class="G_DESC" name="G_DESC" placeholder="Enter your group description here." style="height:100px;" maxlength="300" required></textarea>
          <small class="error">You must enter a short description for your class.</small>
        </div>

        <div>
          <label>Visibility</label>
            <select id="G_VIS" name="G_VIS" required>
              <option id="G_VIS_1" name="G_VIS_0" value="0">Public (Any user can find the group and join)</option>
              <option id="G_VIS_0" name="G_VIS_1" value="1">Private (Only users who have the link to the group's page may join)</option>
            </select>
          <br />
        </div>

        <button class="tiny submit" onclick="return submit_form();">Done</button>


      </form>
      </div>
    <a class="close-reveal-modal">×</a>
    </div>
  </div>
  <!-- End Single Classroom Form Modal -->

  <!-- Single School Group Form Modal-->
  <div id='single-school-form' class='reveal-modal' data-reveal>
    <div class='row'>
      <div class="large-12 columns">
        <h2 class="custom-font-small-blue">Create a Single School Group:</h2>
        <hr />
      </div>
    </div>

    <div class="row">
      <div class="large-12 columns">

      <!-- Group Creation Disclaimer -->
      <div data-alert class="alert-box">
        Disclaimer: Once you create a group, the template can not be changed.  Only create a single school group if you are sure no other schools are needed to be a part of the group as well.  If you do need to include other schools, <a href="#" style="color:white; text-decoration:underline;" data-reveal-id="school-system-form">click here to create a School System Group</a>.  Otherwise, continue on your way!
      <a href="#" class="close">&times;</a>
      </div>
      <!-- End Group Creation Disclaimer -->

      <form action="group/create_group.php" id="group-form" method="post" data-abide>
        <h2 class="global-h2">General Info:</h2>

        <div>
          <label>School Name</label>
          <input type="text" id="G_NAME" name="G_NAME" maxlength="50" placeholder="Ex: Clarke Central High School"  required/>
          <input type="hidden" id="PARENT_ID" name="PARENT_ID" value="<?php echo $_SESSION['UUID'] ?>" />
          <input type="hidden" id="G_ISROOTGROUP" name="G_ISROOTGROUP" value="1" />
          <input type="hidden" id="G_GLOG" name="G_GLOG" value="1" />
          <small class="error">You must enter a name for your school.</small>
        </div>

        <label>School Description: <span class="description-text-remaining">300/300 Characters remaining</span></label>
        <textarea id="G_DESC" class="G_DESC" name="G_DESC" placeholder="Enter your school's description here." style="height:100px;" maxlength="300"></textarea>


        <label>Visibility</label>
        <select id="G_VIS" name="G_VIS" required>
          <option id="G_VIS_0" name="G_VIS_0" value="0">Public (Any user can find the group and join)</option>
          <option id="G_VIS_1" name="G_VIS_1" value="1">Private (Only users who have the link to the group's page may join)</option>
        </select>


        <h2 class="global-h2">Select Grades to Create:</h2>
        <p class="global-p-small">Click the buttons of the grades you'd like to create.  Green buttons will be created as subgroups.</p>

        <div class="center" style="align-content:center;">

        <div id="ck-button">
          <label>
            <input id="sub_0" class="grade" name="sub_0" type="checkbox" value="Kindergarten"><span>Kindergarten</span>
          </label>
        </div>

        <div id="ck-button">
          <label>
            <input id="sub_1" class="grade" name="sub_1" type="checkbox" value="First Grade"><span>1st Grade</span>
          </label>
        </div>

        <div id="ck-button">
          <label>
            <input id="sub_2" class="grade" name="sub_2" type="checkbox" value="Second Grade"><span>2nd Grade</span>
          </label>
        </div>

        <div id="ck-button">
          <label>
            <input id="sub_3" class="grade" name="sub_3" type="checkbox" value="Third Grade"><span>3rd Grade</span>
          </label>
        </div>

        <div id="ck-button">
          <label>
            <input id="sub_4" class="grade" name="sub_4" type="checkbox" value="Fourth Grade"><span>4th Grade</span>
          </label>
        </div>

        <div id="ck-button">
          <label>
            <input id="sub_5" class="grade" name="sub_5" type="checkbox" value="Fifth Grade"><span>5th Grade</span>
          </label>
        </div>

        <div id="ck-button">
          <label>
            <input id="sub_6" class="grade" name="sub_6" type="checkbox" value="Sixth Grade"><span>6th Grade</span>
          </label>
        </div>

        <div id="ck-button">
          <label>
            <input id="sub_7" class="grade" name="sub_7" type="checkbox" value="Seventh Grade"><span>7th Grade</span>
          </label>
        </div>

        <div id="ck-button">
          <label>
            <input id="sub_8" class="grade" name="sub_8" type="checkbox" value="Eighth Grade"><span>8th Grade</span>
          </label>
        </div>

        <div id="ck-button">
          <label>
            <input id="sub_9" class="grade" name="sub_9" type="checkbox" value="Ninth Grade"><span>9th Grade</span>
          </label>
        </div>

        <div id="ck-button">
          <label>
            <input id="sub_10" class="grade" name="sub_10" type="checkbox" value="Tenth Grade"><span>10th Grade</span>
          </label>
        </div>

        <div id="ck-button">
          <label>
            <input id="sub_11" class="grade" name="sub_11" type="checkbox" value="Eleventh Grade"><span>11th Grade</span>
          </label>
        </div>

        <div id="ck-button">
          <label>
            <input id="sub_12" class="grade" name="sub_12" type="checkbox" value="Twelfth Grade"><span>12th Grade</span>
          </label>
        </div>

        </div>

        </div>

      </div>
      <div class="row">
        <div class="large-12 columns">
          <button style="margin-top:20px; float:left" onclick="return false;" id="check-all">Check all</button>
        </div>
      </div>

      <div class="row">
        <div class="large-12 columns">
          <button style="margin-top:20px; float:left" class="tiny submit" onclick="return submit_form();">Create Group</button>
        </div>
      </div>



      </form>


      <a class="close-reveal-modal">×</a>
      </div>

    </div>
  </div>
  <!-- Single School Form Modal -->

  <!-- School System Group Form Modal-->
  <div id="school-system-form" class='reveal-modal' data-reveal>
    <div class='row'>
    <div class="large-12 columns">
      <h2 class="custom-font-small-blue">Create a School System Group:</h2>
      <hr />

      <form action="group/create_group.php" id="school-system-group-form" method="post" data-abide>

        <h2 class="global-h2">School System Info:</h2>
        <hr style="margin-top:-5px;" />

        <div>
          <label>School System Name</label>
          <input type="hidden" id="PARENT_ID" name="PARENT_ID" value="<?php echo $_SESSION['UUID'] ?>" />
          <input type="hidden" id="G_ISROOTGROUP" name="G_ISROOTGROUP" value="1" />
          <input type="hidden" id="G_GLOG" name="G_GLOG" value="1" />
          <input type="text" id="G_NAME" maxlength="50" name="G_NAME" placeholder="Ex: Clarke County School System" required />
          <small class="error">You must enter a name for your school system.</small>
        </div>

        <div>
        <label>School System Description: <span class="description-text-remaining">300/300 Characters remaining</span></label>
        <textarea id="G_DESC" class="G_DESC" name="G_DESC" placeholder="Enter your school system description here." style="height:100px;" maxlength="300" required></textarea>
        <small class="error">You must enter a short description of your school system.</small>
        </div>

        <label>Visibility</label>
        <select id="G_VIS" name="G_VIS" required>
          <option id="G_VIS_0" name="G_VIS_0" value="0">Public (Any user can find the group and join)</option>
          <option id="G_VIS_1" name="G_VIS_1" value="1">Private (Only users who have the link to the group's page may join)</option>
        </select>

        <h2 class="global-h2">Subgroups for Schools within System:</h2>
        <hr style="margin-top:-5px;" />
        <div id="extender"></div>
        <br />
        <br />
        <a class="button tiny round secondary" id="add_btn" href="#">Add a School</a>
        <hr />

        <br />
        <br />
        <button class="tiny submit" onclick="return submit_form();">Create School System</button>
      </form>
    </div>
    </div>
    <a class="close-reveal-modal">×</a>
  </div>
  <!-- School System Form Modal -->

  <!-- School Fork Modal -->
  <div id='school-fork' class='reveal-modal' data-reveal>
    <div class="row center" style="text-align:center;">
      <div class="large-12 columns center">

      <div class="large-4 medium-4 columns center">
          <div class="white-bg">
            <i class="fi-results-demographics size-60 blue"></i>
            <h2 class="global-h2">School System</h2>
            <hr style="margin-top:-5px; margin-bottom:10px;">
            <a href="#" class="button tiny expand" data-reveal-id="school-system-form">Create School System Group</a>
            <p class="global-p-small justify">This form allows you to create a school sysetm and all of the schools that belong inside it. Afterwards, you can add the grade levels and classrooms if you want to collect information on them.</p>
          </div>
        </div>

      <div class="large-4 medium-4 columns center">
          <div class="white-bg">
            <i class="fi-book-bookmark size-60 blue"></i>
            <h2 class="global-h2">Single School</h2>
            <hr style="margin-top:-5px; margin-bottom:10px;">
            <a href="#" class="button tiny expand" data-reveal-id="single-school-form">Create School Group</a>
            <p class="global-p-small justify">This form creates a school and allows you to make subgroups according to grade level.  From there you can actually create more subgroups for individual calsses.</p>
          </div>
        </div>

      <div class="large-4 medium-4 columns center">
          <div class="white-bg">
            <i class="fi-torso-business size-60 blue"></i>
            <h2 class="global-h2">Single Class</h2>
            <hr style="margin-top:-5px; margin-bottom:10px;">
            <a href="#" class="button tiny expand" data-reveal-id="single-class-form">Create Class Group</a>
            <p class="global-p-small justify">This form creates a group for single classroom.  This template could be used if there is no chance of an entire school using Walk Georgia, but one class or teacher would still like to participate on their own.</p>
          </div>
        </div>

    </div>
    </div>
    <a class="close-reveal-modal">×</a>
  </div>
  <!-- School Fork Modal -->

  <!-- Organization Modal -->
  <div id='organization-form' class='reveal-modal' data-reveal>
    <div class="row">
      <div class="large-12 columns">
        <h2 class="custom-font-small-blue">Create a Group (Organization):</h2>
        <hr />
      </div>
    </div>
    <div class='row'>
      <div class="large-12 columns">
        <form action="group/create_group.php" id="org-group-form" method="post" data-abide>


          <div>
            <label>Organization Name</label>
            <input type="text" id="G_NAME" name="G_NAME" maxlength="50" placeholder="Enter your organization name." required autofocus />
            <input type="hidden" id="PARENT_ID" name="PARENT_ID" value="<?php echo $_SESSION['UUID'] ?>" />
            <input type="hidden" id="G_ISROOTGROUP" name="G_ISROOTGROUP" value="1" />
            <input type="hidden" id="G_GLOG" name="G_GLOG" value="0" />
            <small class="error">You must enter a name for your organization.</small>
          </div>

          <div>
          <label>Organization Description: <span class="description-text-remaining">300/300 Characters remaining</span></label>
           <textarea id="G_DESC" class="G_DESC" name="G_DESC" placeholder="Enter your organization's group description here." style="height:100px;" maxlength="300" required></textarea>
           <small class="error">You must enter a short description of your organization.</small>
          </div>


          <label>Organization Visibility</label>
          <select id="G_VIS" name="G_VIS" required>
            <option id="G_VIS_0" name="G_VIS_0" value="0">Public (Any user can find the group and join)</option>
            <option id="G_VIS_1" name="G_VIS_1" value="0">Private (Only users who have the link to the group's page may join)</option>
          </select>



          <label>Request Profile Information</label>
          <input id="name" name="name" type="checkbox"><label for="name">Name</label>
          <input id="email" name="email" type="checkbox"><label for="email">Email</label>
          <input id="county" name="county" type="checkbox"><label for="county">County</label>


          <hr style="margin-top:-5px;" />

        <label>You may want to require new group members to submit specific information when joining, i.e., employee number, birthday, or favorite exercise! If you want to collect extra info from your members, put the required data on the left and your personal answer on the right.</label>
        <br />
        <br />
        <div class="row">
        <div id="org_extender_info"></div>
        </div>
        <br />
        <br />
        <a class="button tiny round secondary" id="org_add_info" href="#">Add extra info</a>
        <hr />

       <div class="row">
       <button class="tiny submit" onclick="return submit_form();">Create Group</button>
       </div>

        </form>
      </div>
    </div>
    <a class="close-reveal-modal">×</a>
  </div>
  <!-- End Organization Modal -->

<!-- End of the Main Section -->
</div>
<!-- End of the Main Section -->
<?php

//END EDITING HERE

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script>
		<?php echo $activegroups; ?>
	</script>
	<script>
		$(document).ready(function() {
			$('.G_DESC').on('keyup', function() {
				if(this.value.length > 300) {
					return false;
				}
				$('.description-text-remaining').html((300 - this.value.length) + "/300 Characters remaining");
			});
		});
	</script>
        <script>
            $(document).ready(function() {
    $("#check-all").click(function() {
        var checkBoxes = $("input[class='grade']");
        checkBoxes.prop("checked", !checkBoxes.prop("checked"));
    });
});
        </script>
  <!-- End Footer -->
  </body>
</html>
