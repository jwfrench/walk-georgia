<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout')){
	SESSION::logout();
	REDIRECT::home();
}
if(filter_input(INPUT_POST, 'file')){
		$img = ACCOUNT::avatar_upload($_SESSION['ID']);
	}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();
//If the user is logged in
if($_SESSION['valid']){
	if(filter_input(INPUT_GET, 'uid', FILTER_SANITIZE_STRING) != null){
		$ID =filter_input(INPUT_GET, 'uid', FILTER_SANITIZE_STRING);
	}else{
		$ID= $_SESSION['ID'];
	}
	$info = ACCOUNT::get_info($ID);
//EDIT HERE
?> 
<div class="main nojava">
<!-- Main Section -->
  <div class="row" style="margin-top:2em;">
  <!-- Global Pilot Alert -->
     <div data-alert style="display: none;" id="change_success" class="alert-box success radius">
  Account info successfully changed.
  <a href="#" class="close">&times;</a>
        </div>
  <div class="large-12 columns">
    
  <!-- End Global Pilot Alert -->
  <!-- Sidebar -->
    <div class="large-5 medium-5 columns">
        <div class="sidebar">
          <div class="center"> 
            <!-- Avatar -->
            <?php
			  $avatar = ACCOUNT::avatar_medium($_SESSION['ID']); 
			?>
            <!-- End Avatar -->
            <!-- Change Avatar Modal -->
            <div id="avatar" class="reveal-modal" data-reveal>
              <div class="row">
                <?php 
		          $avatar = ACCOUNT::avatar_change_modal($avatar);
		        ?>
              </div>
            <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End Change Avatar Modal -->
            <h2 class="custom-font-small-blue"><?php echo $info['FNAME']." ".$info['LNAME'] ?></h2>
            
                    
      <!-- Delete Account Modal -->
        <div id="delete-account" class="reveal-modal tiny" data-reveal="">
          <div class="row">
            <div class="large-12 columns">
              <h2 class="custom-font-small-blue">Delete Your Account</h2>
              <hr style="margin-top:-5px;" />
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <p class="global-p-small">Deleting your account is a permanent action.  All data from stored exercises, reports, group membership, and other information will be lost.  And while we are sad to see you go, we hope you continue to move more and live more.  Thank you for being a part of Walk Georgia.</p>
              <br />
              <a href="delete_user.php?uuid=<?php echo $_SESSION['UUID']; ?>" class="button alert round tiny">Permanently Delete My Account</a> 
              <a class="button tiny round" onclick="$('#delete-account').foundation('reveal', 'close');">Cancel</a>
            </div>
            <a class="close-reveal-modal">×</a>
          </div>
        </div>
      <!-- End Delete Account Modal -->
      </div>
     </div>
     <a href="#" class="button alert expand tiny" data-reveal-id="delete-account">Delete Account</a>
     
     <h2 class="global-h2">Help us make Walk Georgia Better!</h2>
     <hr style="margin-top:-5px;" />
     <a href="https://ugeorgia.qualtrics.com/SE/?SID=SV_bCr3FZTBFV7h96R" target="_blank" class="button tiny round">Help Us With Research</a>
     
     <!-- Connect Social Media Buttons -->
     <!--
     <div class="row center">
       <div class="large-12 columns">
         <h3 class="custom-font-small-blue">Connected Accounts:</h3>
         <hr style="margin-top:-5px;" />
         <a class="button" data-reveal-id="connect-facebook"><i class="fi-social-facebook size-36"></i></a> <a class="button success" data-reveal-id="connect-twitter"><i class="fi-social-twitter size-36"></i></a> <a class="button" data-reveal-id="connect-google-plus"><i class="fi-social-google-plus size-36"></i></a>
       </div>
     </div>
     -->
     <!-- Connect Social Media Buttons -->
     
     <!-- Social Media Modals -->
     
       <!-- Facebook Modal -->
       <div id="connect-facebook" class="reveal-modal tiny" data-reveal>
         <div class="row">
           <div class="large-12 columns">
             <h2 class="custom-font-small-blue">Connect Facebook:</h2>
             <hr style="margin-top:-5px;" />
             <span class="label round secondary">Status: Not Connected</span>
             <br />
             <br />
             <p class="global-p-small">Connecting your Facebook account can allow you to do all sorts of things, like share activity you've logged automatically or even sync your profile picture.  To connect your Facebook account, click the button below.</p>
             <br />
             <a class="button tiny round">Link My Facebook Account</a>
           </div>
         </div>
         <a class="close-reveal-modal">&#215;</a>
       </div>
       <!-- End Facebook Modal -->
       
       <!-- Twitter Modal -->
       <div id="connect-twitter" class="reveal-modal tiny" data-reveal>
         <div class="row">
           <div class="large-12 columns">
             <h2 class="custom-font-small-blue">Connect Twitter:</h2>
             <hr style="margin-top:-5px;" />
             <span class="label round success">Status: Connected</span>
             <br />
             <br />
             <p class="global-p-small">Connecting your Twitter account can allow you to do all sorts of things, like share activity you've logged automatically or even sync your profile picture.  To connect your Facebook account, click the button below.</p>
             <br />
             <a class="button tiny round alert">Disconnect My Twitter Account</a>
           </div>
         </div>
         <a class="close-reveal-modal">&#215;</a>
       </div>
       <!-- End Twitter Modal -->
       
       <!-- Google Plus Modal -->
       <div id="connect-google-plus" class="reveal-modal tiny" data-reveal>
         <div class="row">
           <div class="large-12 columns">
             <h2 class="custom-font-small-blue">Connect Google:</h2>
             <hr style="margin-top:-5px;" />
             <span class="label round secondary">Status: Not Connected</span>
             <br />
             <br />
             <p class="global-p-small">Connecting your Google account can allow you to do all sorts of things, like share activity you've logged automatically or even sync your profile picture.  To connect your Facebook account, click the button below.</p>
             <br />
             <a class="button tiny round">Link My Google Account</a>
           </div>
         </div>
         <a class="close-reveal-modal">&#215;</a>
       </div>
       <!-- End Google Plus Modal -->
       
     
     <!-- End Social Media Modals -->
     
 </div>
 <!-- End Sidebar -->
 
 
    
    <!-- Right Side Account Edits -->
      <div class="small-12 medium-7 large-7 columns">

        
    <h1 class="custom-font-small-blue">Edit My Account Info:</h1>
    <hr style="margin-top:-5px;" />
    <small>**NOTE** If you make changes, you must save them by clicking the button at the bottom of the page.</small>
    <br />
    
    <br />
      <form id ="edit-preferences" action="edit_account.php" method="post" data-abide>
          <div class="name-field">
            <label>First Name</label>
            <input type="text" name="FN" id="FN" value="<?PHP echo $info['FNAME']; ?>" required pattern="alpha">
            <small class="error" data-error-message="">An alphabetical first name required.</small>
          </div>
          <div class="name-field">
            <label>Last Name</label>
            <input type="text" name="LN" id="LN" value="<?PHP echo $info['LNAME']; ?>" required pattern="alpha">
            <small class="error" data-error-message="">An alphabetical last name required</small>
          </div>
          <div class="">
            <label><b>Current Email: <?php echo $info['EMAIL']; ?></b></label>
            <br />
            <h2 class="global-h2">Reset Email:</h2>
            <hr />
          </div>
          <div class="email-field">
  		  <label>New Email</label>
  		  <input type="email" name="EM" id="EM" value="<?PHP echo $info['EMAIL']; ?>" required pattern="email">
 		  <small class="error" data-error-message="">A valid email address is required.</small>
        </div>
          <div class="email-field">
            <label>Confirm New Email</label>
           <input type="email" data-equalto="email" required></input>
           <small class="error">The emails must match.</small>
           <hr />
          </div>
          <h3 class="global-h2">County:</h3>
              <hr />
          <div class="county">
            <label>County of Participation
               <select name="CN" id="CN" required>
                <option ></option>
                <option value="Appling" <?php if($info['COUNTY'] == 'Appling'){ echo 'selected=selected';}?>>Appling</option>
                <option value="Atkinson" <?php if($info['COUNTY'] == 'Atkinson'){ echo 'selected=selected';}?>>Atkinson</option>
                <option value="Bacon"  <?php if($info['COUNTY'] == 'Bacon'){ echo 'selected=selected';}?>>Bacon</option>
                <option value="Baker"  <?php if($info['COUNTY'] == 'Baker'){ echo 'selected=selected';}?>>Baker</option>
                <option value="Baldwin"  <?php if($info['COUNTY'] == 'Baldwin'){ echo 'selected=selected';}?>>Baldwin</option>
                <option value="Banks"  <?php if($info['COUNTY'] == 'Banks'){ echo 'selected=selected';}?>>Banks</option>
                <option value="Barrow"  <?php if($info['COUNTY'] == 'Barrow'){ echo 'selected=selected';}?>>Barrow</option>
                <option value="Bartow"  <?php if($info['COUNTY'] == 'Bartow'){ echo 'selected=selected';}?>>Bartow</option>
                <option value="Ben Hill"  <?php if($info['COUNTY'] == 'Ben Hill'){ echo 'selected=selected';}?>>Ben Hill</option>
                <option value="Berrien"  <?php if($info['COUNTY'] == 'Berrien'){ echo 'selected=selected';}?>>Berrien</option>
                <option value="Bibb"  <?php if($info['COUNTY'] == 'Bibb'){ echo 'selected=selected';}?>>Bibb</option>
                <option value="Bleckley"  <?php if($info['COUNTY'] == 'Bleckley'){ echo 'selected=selected';}?>>Bleckley</option>
                <option value="Brantley"  <?php if($info['COUNTY'] == 'Brantley'){ echo 'selected=selected';}?>>Brantley</option>
                <option value="Brooks"  <?php if($info['COUNTY'] == 'Brooks'){ echo 'selected=selected';}?>>Brooks</option>
                <option value="Bryan"  <?php if($info['COUNTY'] == 'Bryan'){ echo 'selected=selected';}?>>Bryan</option>
                <option value="Bulloch"  <?php if($info['COUNTY'] == 'Bulloch'){ echo 'selected=selected';}?>>Bulloch</option>
                <option value="Burke"  <?php if($info['COUNTY'] == 'Burke'){ echo 'selected=selected';}?>>Burke</option>
                <option value="Butts"  <?php if($info['COUNTY'] == 'Butts'){ echo 'selected=selected';}?>>Butts</option>
                <option value="Calhoun"  <?php if($info['COUNTY'] == 'Calhoun'){ echo 'selected=selected';}?>>Calhoun</option>
                <option value="Camden"  <?php if($info['COUNTY'] == 'Camden'){ echo 'selected=selected';}?>>Camden</option>
                <option value="Candler"  <?php if($info['COUNTY'] == 'Candler'){ echo 'selected=selected';}?>>Candler</option>
                <option value="Carroll"  <?php if($info['COUNTY'] == 'Carroll'){ echo 'selected=selected';}?>>Carroll</option>
                <option value="Catoosa"  <?php if($info['COUNTY'] == 'Catoosa'){ echo 'selected=selected';}?>>Catoosa</option>
                <option value="Charlton"  <?php if($info['COUNTY'] == 'Charlton'){ echo 'selected=selected';}?>>Charlton</option>
                <option value="Chatham"  <?php if($info['COUNTY'] == 'Chatham'){ echo 'selected=selected';}?>>Chatham</option>
                <option value="Chattahoochee" <?php if($info['COUNTY'] == 'Chattahoochee'){ echo 'selected=selected';}?>>Chattahoochee</option>
                <option value="Chattooga" <?php if($info['COUNTY'] == 'Chattooga'){ echo 'selected=selected';}?>>Chattooga</option>
                <option value="Cherokee" <?php if($info['COUNTY'] == 'Cherokee'){ echo 'selected=selected';}?>>Cherokee</option>
                <option value="Clarke"  <?php if($info['COUNTY'] == 'Clarke'){ echo 'selected=selected';}?>>Clarke</option>
                <option value="Clay" <?php if($info['COUNTY'] == 'Clay'){ echo 'selected=selected';}?>>Clay</option>
                <option value="Clayton" <?php if($info['COUNTY'] == 'Clayton'){ echo 'selected=selected';}?>>Clayton</option>
                <option value="Clinch" <?php if($info['COUNTY'] == 'Clinch'){ echo 'selected=selected';}?>>Clinch</option>
                <option value="Cobb" <?php if($info['COUNTY'] == 'Cobb'){ echo 'selected=selected';}?>>Cobb</option>
                <option value="Coffee" <?php if($info['COUNTY'] == 'Coffee'){ echo 'selected=selected';}?>>Coffee</option>
                <option value="Colquitt" <?php if($info['COUNTY'] == 'Colquitt'){ echo 'selected=selected';}?>>Colquitt</option>
                <option value="Columbia" <?php if($info['COUNTY'] == 'Columbia'){ echo 'selected=selected';}?>>Columbia</option>
                <option value="Cook" <?php if($info['COUNTY'] == 'Cook'){ echo 'selected=selected';}?>>Cook</option>
                <option value="Coweta" <?php if($info['COUNTY'] == 'Coweta'){ echo 'selected=selected';}?>>Coweta</option>
                <option value="Crawford" <?php if($info['COUNTY'] == 'Crawford'){ echo 'selected=selected';}?>>Crawford</option>
                <option value="Crisp" <?php if($info['COUNTY'] == 'Crisp'){ echo 'selected=selected';}?>>Crisp</option>
                <option value="Dade" <?php if($info['COUNTY'] == 'Dade'){ echo 'selected=selected';}?>>Dade</option>
                <option value="Dawson" <?php if($info['COUNTY'] == 'Dawson'){ echo 'selected=selected';}?>>Dawson</option>
                <option value="Decatur" <?php if($info['COUNTY'] == 'Decatur'){ echo 'selected=selected';}?>>Decatur</option>
                <option value="DeKalb" <?php if($info['COUNTY'] == 'DeKalb'){ echo 'selected=selected';}?>>DeKalb</option>
                <option value="Dodge" <?php if($info['COUNTY'] == 'Dodge'){ echo 'selected=selected';}?>>Dodge</option>
                <option value="Dooly" <?php if($info['COUNTY'] == 'Dooly'){ echo 'selected=selected';}?>>Dooly</option>
                <option value="Dougherty" <?php if($info['COUNTY'] == 'Dougherty'){ echo 'selected=selected';}?>>Dougherty</option>
                <option value="Douglas" <?php if($info['COUNTY'] == 'Douglas'){ echo 'selected=selected';}?>>Douglas</option>
                <option value="Early" <?php if($info['COUNTY'] == 'Early'){ echo 'selected=selected';}?>>Early</option>
                <option value="Echols" <?php if($info['COUNTY'] == 'Echols'){ echo 'selected=selected';}?>>Echols</option>
                <option value="Effingham" <?php if($info['COUNTY'] == 'Effingham'){ echo 'selected=selected';}?>>Effingham</option>
                <option value="Elbert" <?php if($info['COUNTY'] == 'Elbert'){ echo 'selected=selected';}?>>Elbert</option>
                <option value="Emanuel" <?php if($info['COUNTY'] == 'Emanuel'){ echo 'selected=selected';}?>>Emanuel</option>
                <option value="Evans" <?php if($info['COUNTY'] == 'Evans'){ echo 'selected=selected';}?>>Evans</option>
                <option value="Fannin" <?php if($info['COUNTY'] == 'Fannin'){ echo 'selected=selected';}?>>Fannin</option>
                <option value="Fayette" <?php if($info['COUNTY'] == 'Fayette'){ echo 'selected=selected';}?>>Fayette</option>
                <option value="Floyd" <?php if($info['COUNTY'] == 'Floyd'){ echo 'selected=selected';}?>>Floyd</option>
                <option value="Forsyth" <?php if($info['COUNTY'] == 'Forsyth'){ echo 'selected=selected';}?>>Forsyth</option>
                <option value="Franklin" <?php if($info['COUNTY'] == 'Franklin'){ echo 'selected=selected';}?>>Franklin</option>
                <option value="Fulton" <?php if($info['COUNTY'] == 'Fulton'){ echo 'selected=selected';}?>>Fulton</option>
                <option value="Gilmer" <?php if($info['COUNTY'] == 'Gilmer'){ echo 'selected=selected';}?>>Gilmer</option>
                <option value="Glascock" <?php if($info['COUNTY'] == 'Glascock'){ echo 'selected=selected';}?>>Glascock</option>
                <option value="Glynn" <?php if($info['COUNTY'] == 'Glynn'){ echo 'selected=selected';}?>>Glynn</option>
                <option value="Gordon" <?php if($info['COUNTY'] == 'Gordon'){ echo 'selected=selected';}?>>Gordon</option>
                <option value="Grady" <?php if($info['COUNTY'] == 'Grady'){ echo 'selected=selected';}?>>Grady</option>
                <option value="Greene" <?php if($info['COUNTY'] == 'Greene'){ echo 'selected=selected';}?>>Greene</option>
                <option value="Gwinnett" <?php if($info['COUNTY'] == 'Gwinnett'){ echo 'selected=selected';}?>>Gwinnett</option>
                <option value="Habersham" <?php if($info['COUNTY'] == 'Habersham'){ echo 'selected=selected';}?>>Habersham</option>
                <option value="Hall" <?php if($info['COUNTY'] == 'Hall'){ echo 'selected=selected';}?>>Hall</option>
                <option value="Hancock" <?php if($info['COUNTY'] == 'Hancock'){ echo 'selected=selected';}?>>Hancock</option>
                <option value="Haralson" <?php if($info['COUNTY'] == 'Haralson'){ echo 'selected=selected';}?>>Haralson</option>
                <option value="Harris" <?php if($info['COUNTY'] == 'Harris'){ echo 'selected=selected';}?>>Harris</option>
                <option value="Hart" <?php if($info['COUNTY'] == 'Hart'){ echo 'selected=selected';}?>>Hart</option>
                <option value="Heard" <?php if($info['COUNTY'] == 'Heard'){ echo 'selected=selected';}?>>Heard</option>
                <option value="Henry" <?php if($info['COUNTY'] == 'Henry'){ echo 'selected=selected';}?>>Henry</option>
                <option value="Houston" <?php if($info['COUNTY'] == 'Houston'){ echo 'selected=selected';}?>>Houston</option>
                <option value="Irwin" <?php if($info['COUNTY'] == 'Irwin'){ echo 'selected=selected';}?>>Irwin</option>
                <option value="Jackson" <?php if($info['COUNTY'] == 'Jackson'){ echo 'selected=selected';}?>>Jackson</option>
                <option value="Jasper" <?php if($info['COUNTY'] == 'Jasper'){ echo 'selected=selected';}?>>Jasper</option>
                <option value="Jeff Davis" <?php if($info['COUNTY'] == 'Jeff Davis'){ echo 'selected=selected';}?>>Jeff Davis</option>
                <option value="Jefferson" <?php if($info['COUNTY'] == 'Jefferson'){ echo 'selected=selected';}?>>Jefferson</option>
                <option value="Jenkins" <?php if($info['COUNTY'] == 'Jenkins'){ echo 'selected=selected';}?>>Jenkins</option>
                <option value="Johnson" <?php if($info['COUNTY'] == 'Johnson'){ echo 'selected=selected';}?>>Johnson</option>
                <option value="Jones" <?php if($info['COUNTY'] == 'Jones'){ echo 'selected=selected';}?>>Jones</option>
                <option value="Lamar" <?php if($info['COUNTY'] == 'Lamar'){ echo 'selected=selected';}?>>Lamar</option>
                <option value="Lanier" <?php if($info['COUNTY'] == 'Lanier'){ echo 'selected=selected';}?>>Lanier</option>
                <option value="Laurens" <?php if($info['COUNTY'] == 'Laurens'){ echo 'selected=selected';}?>>Laurens</option>
                <option value="Lee" <?php if($info['COUNTY'] == 'Lee'){ echo 'selected=selected';}?>>Lee</option>
                <option value="Liberty" <?php if($info['COUNTY'] == 'Liberty'){ echo 'selected=selected';}?>>Liberty</option>
                <option value="Lincoln" <?php if($info['COUNTY'] == 'Lincoln'){ echo 'selected=selected';}?>>Lincoln</option>
                <option value="Long" <?php if($info['COUNTY'] == 'Long'){ echo 'selected=selected';}?>>Long</option>
                <option value="Lowndes" <?php if($info['COUNTY'] == 'Lowndes'){ echo 'selected=selected';}?>>Lowndes</option>
                <option value="Lumpkin" <?php if($info['COUNTY'] == 'Lumpkin'){ echo 'selected=selected';}?>>Lumpkin</option>
                <option value="Macon" <?php if($info['COUNTY'] == 'Macon'){ echo 'selected=selected';}?>>Macon</option>
                <option value="Madison" <?php if($info['COUNTY'] == 'Madison'){ echo 'selected=selected';}?>>Madison</option>
                <option value="Marion" <?php if($info['COUNTY'] == 'Marion'){ echo 'selected=selected';}?>>Marion</option>
                <option value="McDuffie" <?php if($info['COUNTY'] == 'McDuffie'){ echo 'selected=selected';}?>>McDuffie</option>
                <option value="McIntosh" <?php if($info['COUNTY'] == 'McIntosh'){ echo 'selected=selected';}?>>McIntosh</option>
                <option value="Meriwether" <?php if($info['COUNTY'] == 'Meriwether'){ echo 'selected=selected';}?>>Meriwether</option>
                <option value="Miller" <?php if($info['COUNTY'] == 'Miller'){ echo 'selected=selected';}?>>Miller</option>
                <option value="Mitchell" <?php if($info['COUNTY'] == 'Mitchell'){ echo 'selected=selected';}?>>Mitchell</option>
                <option value="Monroe" <?php if($info['COUNTY'] == 'Monroe'){ echo 'selected=selected';}?>>Monroe</option>
                <option value="Montgomery" <?php if($info['COUNTY'] == 'Montgomery'){ echo 'selected=selected';}?>>Montgomery</option>
                <option value="Morgan" <?php if($info['COUNTY'] == 'Morgan'){ echo 'selected=selected';}?>>Morgan</option>
                <option value="Murray" <?php if($info['COUNTY'] == 'Murray'){ echo 'selected=selected';}?>>Murray</option>
                <option value="Muscogee" <?php if($info['COUNTY'] == 'Muscogee'){ echo 'selected=selected';}?>>Muscogee</option>
                <option value="Newton" <?php if($info['COUNTY'] == 'Newton'){ echo 'selected=selected';}?>>Newton</option>
                <option value="Oconee" <?php if($info['COUNTY'] == 'Oconee'){ echo 'selected=selected';}?>>Oconee</option>
                <option value="Oglethorpe" <?php if($info['COUNTY'] == 'Oglethorpe'){ echo 'selected=selected';}?>>Oglethorpe</option>
                <option value="Paulding" <?php if($info['COUNTY'] == 'Paulding'){ echo 'selected=selected';}?>>Paulding</option>
                <option value="Peach" <?php if($info['COUNTY'] == 'Peach'){ echo 'selected=selected';}?>>Peach</option>
                <option value="Pickens" <?php if($info['COUNTY'] == 'Pickens'){ echo 'selected=selected';}?>>Pickens</option>
                <option value="Pierce" <?php if($info['COUNTY'] == 'Pierce'){ echo 'selected=selected';}?>>Pierce</option>
                <option value="Pike" <?php if($info['COUNTY'] == 'Pike'){ echo 'selected=selected';}?>>Pike</option>
                <option value="Polk" <?php if($info['COUNTY'] == 'Polk'){ echo 'selected=selected';}?>>Polk</option>
                <option value="Pulaski" <?php if($info['COUNTY'] == 'Pulaski'){ echo 'selected=selected';}?>>Pulaski</option>
                <option value="Putnam" <?php if($info['COUNTY'] == 'Putnam'){ echo 'selected=selected';}?>>Putnam</option>
                <option value="Quitman" <?php if($info['COUNTY'] == 'Quitman'){ echo 'selected=selected';}?>>Quitman</option>
                <option value="Rabun" <?php if($info['COUNTY'] == 'Rabun'){ echo 'selected=selected';}?>>Rabun</option>
                <option value="Randolph" <?php if($info['COUNTY'] == 'Randolph'){ echo 'selected=selected';}?>>Randolph</option>
                <option value="Richmond" <?php if($info['COUNTY'] == 'Richmond'){ echo 'selected=selected';}?>>Richmond</option>
                <option value="Rockdale" <?php if($info['COUNTY'] == 'Rockdale'){ echo 'selected=selected';}?>>Rockdale</option>
                <option value="Schley" <?php if($info['COUNTY'] == 'Schley'){ echo 'selected=selected';}?>>Schley</option>
                <option value="Screven" <?php if($info['COUNTY'] == 'Screven'){ echo 'selected=selected';}?>>Screven</option>
                <option value="Seminole" <?php if($info['COUNTY'] == 'Seminole'){ echo 'selected=selected';}?>>Seminole</option>
                <option value="Spalding" <?php if($info['COUNTY'] == 'Spalding'){ echo 'selected=selected';}?>>Spalding</option>
                <option value="Stephens" <?php if($info['COUNTY'] == 'Stephens'){ echo 'selected=selected';}?>>Stephens</option>
                <option value="Stewart" <?php if($info['COUNTY'] == 'Stewart'){ echo 'selected=selected';}?>>Stewart</option>
                <option value="Sumter" <?php if($info['COUNTY'] == 'Sumter'){ echo 'selected=selected';}?>>Sumter</option>
                <option value="Talbot" <?php if($info['COUNTY'] == 'Talbot'){ echo 'selected=selected';}?>>Talbot</option>
                <option value="Taliaferro" <?php if($info['COUNTY'] == 'Taliaferro'){ echo 'selected=selected';}?>>Taliaferro</option>
                <option value="Tattnall" <?php if($info['COUNTY'] == 'Tattnall'){ echo 'selected=selected';}?>>Tattnall</option>
                <option value="Taylor" <?php if($info['COUNTY'] == 'Taylor'){ echo 'selected=selected';}?>>Taylor</option>
                <option value="Telfair" <?php if($info['COUNTY'] == 'Telfair'){ echo 'selected=selected';}?>>Telfair</option>
                <option value="Terrell" <?php if($info['COUNTY'] == 'Terrell'){ echo 'selected=selected';}?>>Terrell</option>
                <option value="Thomas" <?php if($info['COUNTY'] == 'Thomas'){ echo 'selected=selected';}?>>Thomas</option>
                <option value="Tift" <?php if($info['COUNTY'] == 'Tift'){ echo 'selected=selected';}?>>Tift</option>
                <option value="Toombs" <?php if($info['COUNTY'] == 'Toombs'){ echo 'selected=selected';}?>>Toombs</option>
                <option value="Towns" <?php if($info['COUNTY'] == 'Towns'){ echo 'selected=selected';}?>>Towns</option>
                <option value="Treutlen" <?php if($info['COUNTY'] == 'Treutlen'){ echo 'selected=selected';}?>>Treutlen</option>
                <option value="Troup" <?php if($info['COUNTY'] == 'Troup'){ echo 'selected=selected';}?>>Troup</option>
                <option value="Turner" <?php if($info['COUNTY'] == 'Turner'){ echo 'selected=selected';}?>>Turner</option>
                <option value="Twiggs" <?php if($info['COUNTY'] == 'Twiggs'){ echo 'selected=selected';}?>>Twiggs</option>
                <option value="Union" <?php if($info['COUNTY'] == 'Union'){ echo 'selected=selected';}?>>Union</option>
                <option value="Upson" <?php if($info['COUNTY'] == 'Upson'){ echo 'selected=selected';}?>>Upson</option>
                <option value="Walker" <?php if($info['COUNTY'] == 'Walker'){ echo 'selected=selected';}?>>Walker</option>
                <option value="Walton" <?php if($info['COUNTY'] == 'Walton'){ echo 'selected=selected';}?>>Walton</option>
                <option value="Ware" <?php if($info['COUNTY'] == 'Ware'){ echo 'selected=selected';}?>>Ware</option>
                <option value="Warren" <?php if($info['COUNTY'] == 'Warren'){ echo 'selected=selected';}?>>Warren</option>
                <option value="Washington" <?php if($info['COUNTY'] == 'Washington'){ echo 'selected=selected';}?>>Washington</option>
                <option value="Wayne" <?php if($info['COUNTY'] == 'Wayne'){ echo 'selected=selected';}?>>Wayne</option>
                <option value="Webster" <?php if($info['COUNTY'] == 'Webster'){ echo 'selected=selected';}?>>Webster</option>
                <option value="Wheeler" <?php if($info['COUNTY'] == 'Wheeler'){ echo 'selected=selected';}?>>Wheeler</option>
                <option value="White" <?php if($info['COUNTY'] == 'White'){ echo 'selected=selected';}?>>White</option>
                <option value="Whitfield" <?php if($info['COUNTY'] == 'Whitfield'){ echo 'selected=selected';}?>>Whitfield</option>
                <option value="Wilcox" <?php if($info['COUNTY'] == 'Wilcox'){ echo 'selected=selected';}?>>Wilcox</option>
                <option value="Wilkes" <?php if($info['COUNTY'] == 'Wilkes'){ echo 'selected=selected';}?>>Wilkes</option>
                <option value="Wilkinson" <?php if($info['COUNTY'] == 'Wilkinson'){ echo 'selected=selected';}?>>Wilkinson</option>
                <option value="Worth" <?php if($info['COUNTY'] == 'Worth'){ echo 'selected=selected';}?>>Worth</option>                       
              </select>
            </label>
            <hr />
          </div>
          
          <div class="row">
            <div class="large-12 columns">
              <h3 class="global-h2">Profile Visibility:</h3>
              <hr />
              <div class="center">
                <ul class="small-block-grid-2">
                  <li>
                    <i class="fi-torsos-all size-36"></i>
                    <br />
                    <input type="radio" name="privacy" value="0" id="public" <?php if(!$info['PRIVACY']){echo 'checked="checked"';} ?>><label for="">Make My Profile Public</label>
                    <br />
                    <span data-tooltip aria-haspopup="true" class="has-tip" title="A public profile allows other users to search for you and verify that you have an account to more easily be able to add you to groups or promote you to administrative positions.  You can always change your choice by going to My Account and clicking Preferences.">Why Public?</span>
                  </li>
                  <li>
                    <i class="fi-prohibited size-36"></i>
                    <br />
                    <input type="radio" name="privacy" value="1" id="private" <?php if($info['PRIVACY']){echo 'checked="checked"';} ?>><label for="">Make My Profile Private</label>
                    <br />
                    <span data-tooltip aria-haspopup="true" class="has-tip" title="Private profiles are for users who aren't interested in joining groups or participating with others. If you just want to get more fit by yourself, this is the type for you.  You can always change your choice by going to My Account and clicking Preferences.">Why Private?</span>
                  </li>
                </ul>
              </div>
              <hr />
            </div>
          </div>
      
          <div class="row">
            <div class="medium-12 large-12 columns">
            <h3 class="global-h2">Newsletter:</h3>
              <hr />
               <label for="checkbox2">
                 <input type="checkbox" id="newsletter-sign-up" name="newsletter-sign-up" checked /> I would like to receive official newsletters from Walk Georgia.
               </label>
               <hr />
               <br />
               <br />         
               <input type="submit" value="Save" class="tiny button"/>
               <a href="http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/index.php" class="button small expand secondary">Cancel</a>
               <br />
               
            </div>
          </div>	
      </form>
        
        </div>
        
      </div>
    </div>
    <!-- End Right Side Sub-Group Creation -->

<?php 
}
//If user is not logged in
		else{
			REDIRECT::login();
		}

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
  <!-- End Footer -->
  </body>
</html>
