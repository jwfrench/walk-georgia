<?php
include_once("lib/back_api.php");
//GET THE COUNTY INFO
if(isset($_GET['day1'])){
	$date1 = $_GET['year1'].'-'.$_GET['month1'].'-'.$_GET['day1'];
	$date2 = $_GET['year2'].'-'.$_GET['month2'].'-'.$_GET['day2'];
	$date_range = 'AL_DATE > \''.$date1.'\' AND AL_DATE < \''.$date2.'\' AND';
}


//query for total users
$sql0 = 'SELECT COUNT(DISTINCT U_ID) AS COUNT FROM LOGIN WHERE L_COUNTY =  \''.$_GET['county'].'\'';
$total_users = MSSQL::query($sql0);

//query for user data
$user_order ='AL_PA DESC';
if(isset($_GET['user_order'])){
	$user_order=$_GET['user_order'];
}
$sql1 = 'SELECT L_ID, L_FNAME, L_LNAME, L_COUNTY, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\' FROM LOG INNER JOIN LOGIN ON AL_UID = L_ID  WHERE  '.$date_range.' L_COUNTY =  \''.$_GET['county'].'\' GROUP BY L_FNAME, L_LNAME, L_COUNTY, L_ID ORDER BY '.$user_order.';';
$user = MSSQL::query($sql1);

//query for activity
$activity_order ='AL_PA DESC';
if(isset($_GET['activity_order'])){
	$activity_order=$_GET['activity_order'];
}
$sql2 = 'SELECT AL_AID, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\', SUM(AL_UNIT) AS \'AL_UNIT\', COUNT(AL_AID) AS COUNT FROM LOG WHERE '.$date_range.' L_COUNTY =  \''.$_GET['county'].'\' GROUP BY AL_AID ORDER BY '.$activity_order.';';
$activity = MSSQL::query($sql2);

//query for subgroups
$subs_order ='POINTS DESC';
if(isset($_GET['subs_order'])){
	$subs_order=$_GET['subs_order'];
}
$sql4 = 'SELECT GROUPS.G_ID, G_NAME, SUM(AL_PA) AS POINTS, SUM(AL_TIME) AS SECONDS, SUM(AL_UNIT) AS DISTANCE, COUNT(1) AS RECORDS FROM GROUPS INNER JOIN GROUP_MEMBER ON GROUP_MEMBER.G_ID = GROUPS.G_ID INNER JOIN LOG ON AL_UID = U_ID WHERE G_PID=\''.$_GET['group'].'\' GROUP BY GROUPS.G_ID, G_NAME ORDER BY '.$subs_order.';';
$subs = MSSQL::query($sql4);


$sql1 = 'SELECT AL_UID, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\', SUM(AL_UNIT) AS \'AL_UNIT\' FROM LOG WHERE AL_UID IN (SELECT L_ID FROM LOGIN WHERE '.$date_range.' L_COUNTY = \''.$_GET['county'].'\') GROUP BY AL_UID ORDER BY AL_PA DESC;';
$sql2 = 'SELECT AL_AID, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\', SUM(AL_UNIT) AS \'AL_UNIT\' FROM LOG WHERE AL_UID IN (SELECT L_ID FROM LOGIN WHERE '.$date_range.' L_COUNTY = \''.$_GET['county'].'\') GROUP BY AL_AID ORDER BY AL_PA DESC;';
$user = MSSQL::query($sql1);
$activity = MSSQL::query($sql2);
$count = 0;
while(odbc_fetch_array($activity)){
	$aid = odbc_result($activity, 'AL_AID');
	$time += odbc_result($activity, 'AL_TIME');
	$points += odbc_result($activity, 'AL_PA');
	if(($aid == 1) ||($aid == 2) ||($aid == 3) ||($aid == 47) ||($aid == 50) ||($aid == 68) ||($aid == 70)){
		$distance += odbc_result($activity, 'AL_UNIT');
		$count +=1;
	}
}
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Walk Georgia | Reporting</title>
    <link rel="stylesheet" href="../../css/foundation.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    
  <div id="main">
    
    <div style="margin-top:20px;"></div>
    
    <!-- Header -->
      <div class="row" style="margin-bottom:20px;">
        <div class="large-12 columns center">
          <img src="img/single-color-logo.png" alt="logo" />
          <h1 class="custom-font-small">Official Report</h1>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <h2 class="custom-font-small"><?php echo $_GET['county']; ?></h2>
          <!-- <a href="#" class="button tiny">Printer Friendly Version</a> -->
          
          <!-- Date Range -->
          
          <?php if(isset($_GET['day1'])){
			echo 'Current Date Range: '.$_GET['month1'].'-'.$_GET['day1'].'-'.$_GET['year1'].' to '.$_GET['month2'].'-'.$_GET['day2'].'-'.$_GET['year2'];
		  }?>
          
          <a href="#" class="button tiny" data-reveal-id="reporting-date-range" style="float:right; margin-top:-40px;">Select Date Range</a>
          
          <div id="reporting-date-range" class="reveal-modal" data-reveal>
            <div class="row">
              <div class="large-12 columns">
                <h2 class="global-h2">Date Range for Report</h2>
                <hr />
              </div>
              <form>
              <input type="hidden" name="group" id="group" value="<?php echo $_GET['group']; ?>" >
              <div class="row">
                <div class="large-6 colmns">
                  <div class="row">
                    <div class="large-12 columns">
                      <h3 class="global-h2-gray">Start Date:</h3>
                    </div>
                  </div>
                  <div class="row">
                    <div class="large-12 columns">
                       <!-- Month -->
           <?php
		    $day = date('d');
			$month = date('m');
			$year = date('Y'); 
		   ?>
             <div class="large-4 columns">
               <label>Month
                 <select id="month1" name="month1" required>
                   <option value="1" <?php if ($month=='1'){echo 'selected="selected"';} ?>>01 January</option>
                   <option value="2" <?php if ($month=='2'){echo 'selected="selected"';} ?>>02 February</option>
                   <option value="3" <?php if ($month=='3'){echo 'selected="selected"';} ?>>03 March</option>
                   <option value="4" <?php if ($month=='4'){echo 'selected="selected"';} ?>>04 April</option>
                   <option value="5" <?php if ($month=='5'){echo 'selected="selected"';} ?>>05 May</option>
                   <option value="6" <?php if ($month=='6'){echo 'selected="selected"';} ?>>06 June</option>
                   <option value="7" <?php if ($month=='7'){echo 'selected="selected"';} ?>>07 July</option>
                   <option value="8" <?php if ($month=='8'){echo 'selected="selected"';} ?>>08 August</option>
                   <option value="9" <?php if ($month=='9'){echo 'selected="selected"';} ?>>09 September</option>
                   <option value="10" <?php if ($month=='10'){echo 'selected="selected"';} ?>>10 October</option>
                   <option value="11" <?php if ($month=='11'){echo 'selected="selected"';} ?>>11 November</option>
                   <option value="12" <?php if ($month=='12'){echo 'selected="selected"';} ?>>12 December</option>
                 </select>
               </label>
             </div>
           <!-- End Month -->
         
           <!-- Day -->
             <div class="large-4 columns">
               <label>Day
                 <select id="day1" name="day1" required>
                   <option value="1" <?php if ($day=='1'){echo 'selected="selected"';} ?>>01</option>
                   <option value="2" <?php if ($day=='2'){echo 'selected="selected"';} ?>>02</option>
                   <option value="3" <?php if ($day=='3'){echo 'selected="selected"';} ?>>03</option>
                   <option value="4" <?php if ($day=='4'){echo 'selected="selected"';} ?>>04</option>
                   <option value="5" <?php if ($day=='5'){echo 'selected="selected"';} ?>>05</option>
                   <option value="6" <?php if ($day=='6'){echo 'selected="selected"';} ?>>06</option>
                   <option value="7" <?php if ($day=='7'){echo 'selected="selected"';} ?>>07</option>
                   <option value="8" <?php if ($day=='8'){echo 'selected="selected"';} ?>>08</option>
                   <option value="9" <?php if ($day=='9'){echo 'selected="selected"';} ?>>09</option>
                   <option value="10" <?php if ($day=='10'){echo 'selected="selected"';} ?>>10</option>
                   <option value="11" <?php if ($day=='11'){echo 'selected="selected"';} ?>>11</option>
                   <option value="12" <?php if ($day=='12'){echo 'selected="selected"';} ?>>12</option>
                   <option value="13" <?php if ($day=='13'){echo 'selected="selected"';} ?>>13</option>
                   <option value="14" <?php if ($day=='14'){echo 'selected="selected"';} ?>>14</option>
                   <option value="15" <?php if ($day=='15'){echo 'selected="selected"';} ?>>15</option>
                   <option value="16" <?php if ($day=='16'){echo 'selected="selected"';} ?>>16</option>
                   <option value="17" <?php if ($day=='17'){echo 'selected="selected"';} ?>>17</option>
                   <option value="18" <?php if ($day=='18'){echo 'selected="selected"';} ?>>18</option>
                   <option value="19" <?php if ($day=='19'){echo 'selected="selected"';} ?>>19</option>
                   <option value="20" <?php if ($day=='20'){echo 'selected="selected"';} ?>>20</option>
                   <option value="21" <?php if ($day=='21'){echo 'selected="selected"';} ?>>21</option>
                   <option value="22" <?php if ($day=='22'){echo 'selected="selected"';} ?>>22</option>
                   <option value="23" <?php if ($day=='23'){echo 'selected="selected"';} ?>>23</option>
                   <option value="24" <?php if ($day=='24'){echo 'selected="selected"';} ?>>24</option>
                   <option value="25" <?php if ($day=='25'){echo 'selected="selected"';} ?>>25</option>
                   <option value="26" <?php if ($day=='26'){echo 'selected="selected"';} ?>>26</option>
                   <option value="27" <?php if ($day=='27'){echo 'selected="selected"';} ?>>27</option>
                   <option value="28" <?php if ($day=='28'){echo 'selected="selected"';} ?>>28</option>
                   <option value="29" <?php if ($day=='29'){echo 'selected="selected"';} ?>>29</option>
                   <option value="30" <?php if ($day=='30'){echo 'selected="selected"';} ?>>30</option>
                   <option value="31" <?php if ($day=='31'){echo 'selected="selected"';} ?>>31</option>
                 </select>
               </label>
             </div>
           <!-- End Day -->
         
           <!-- Year -->
             <div class="large-4 columns">
               <label>Year
                 <select id="year1" name="year1" required>
                   <option value="2008" <?php if ($year=='2008'){echo 'selected="selected"';} ?>>2008</option>
                   <option value="2009" <?php if ($year=='2009'){echo 'selected="selected"';} ?>>2009</option>
                   <option value="2010" <?php if ($year=='2010'){echo 'selected="selected"';} ?>>2010</option>
                   <option value="2011" <?php if ($year=='2011'){echo 'selected="selected"';} ?>>2011</option>
                   <option value="2012" <?php if ($year=='2012'){echo 'selected="selected"';} ?>>2012</option>
                   <option value="2013" <?php if ($year=='2013'){echo 'selected="selected"';} ?>>2013</option>
                   <option value="2014" <?php if ($year=='2014'){echo 'selected="selected"';} ?>>2014</option>
                 </select>
               </label>
             </div>
           <!-- End Year -->
                    </div>
                  </div>
                </div>
                <div class="large-6 colmns">
                  <div class="row">
                    <div class="large-12 columns">
                      <h3 class="global-h2-gray">End Date:</h3>
                    </div>
                  </div>
                  <div class="row">
                    <div class="large-12 columns">
                       <!-- Month -->
           <?php
		    $day = date('d');
			$month = date('m');
			$year = date('Y'); 
		   ?>
             <div class="large-4 columns">
               <label>Month
                 <select id="month2" name="month2" required>
                   <option value="1" <?php if ($month=='1'){echo 'selected="selected"';} ?>>01 January</option>
                   <option value="2" <?php if ($month=='2'){echo 'selected="selected"';} ?>>02 February</option>
                   <option value="3" <?php if ($month=='3'){echo 'selected="selected"';} ?>>03 March</option>
                   <option value="4" <?php if ($month=='4'){echo 'selected="selected"';} ?>>04 April</option>
                   <option value="5" <?php if ($month=='5'){echo 'selected="selected"';} ?>>05 May</option>
                   <option value="6" <?php if ($month=='6'){echo 'selected="selected"';} ?>>06 June</option>
                   <option value="7" <?php if ($month=='7'){echo 'selected="selected"';} ?>>07 July</option>
                   <option value="8" <?php if ($month=='8'){echo 'selected="selected"';} ?>>08 August</option>
                   <option value="9" <?php if ($month=='9'){echo 'selected="selected"';} ?>>09 September</option>
                   <option value="10" <?php if ($month=='10'){echo 'selected="selected"';} ?>>10 October</option>
                   <option value="11" <?php if ($month=='11'){echo 'selected="selected"';} ?>>11 November</option>
                   <option value="12" <?php if ($month=='12'){echo 'selected="selected"';} ?>>12 December</option>
                 </select>
               </label>
             </div>
           <!-- End Month -->
         
           <!-- Day -->
             <div class="large-4 columns">
               <label>Day
                 <select id="day2" name="day2" required>
                   <option value="1" <?php if ($day=='1'){echo 'selected="selected"';} ?>>01</option>
                   <option value="2" <?php if ($day=='2'){echo 'selected="selected"';} ?>>02</option>
                   <option value="3" <?php if ($day=='3'){echo 'selected="selected"';} ?>>03</option>
                   <option value="4" <?php if ($day=='4'){echo 'selected="selected"';} ?>>04</option>
                   <option value="5" <?php if ($day=='5'){echo 'selected="selected"';} ?>>05</option>
                   <option value="6" <?php if ($day=='6'){echo 'selected="selected"';} ?>>06</option>
                   <option value="7" <?php if ($day=='7'){echo 'selected="selected"';} ?>>07</option>
                   <option value="8" <?php if ($day=='8'){echo 'selected="selected"';} ?>>08</option>
                   <option value="9" <?php if ($day=='9'){echo 'selected="selected"';} ?>>09</option>
                   <option value="10" <?php if ($day=='10'){echo 'selected="selected"';} ?>>10</option>
                   <option value="11" <?php if ($day=='11'){echo 'selected="selected"';} ?>>11</option>
                   <option value="12" <?php if ($day=='12'){echo 'selected="selected"';} ?>>12</option>
                   <option value="13" <?php if ($day=='13'){echo 'selected="selected"';} ?>>13</option>
                   <option value="14" <?php if ($day=='14'){echo 'selected="selected"';} ?>>14</option>
                   <option value="15" <?php if ($day=='15'){echo 'selected="selected"';} ?>>15</option>
                   <option value="16" <?php if ($day=='16'){echo 'selected="selected"';} ?>>16</option>
                   <option value="17" <?php if ($day=='17'){echo 'selected="selected"';} ?>>17</option>
                   <option value="18" <?php if ($day=='18'){echo 'selected="selected"';} ?>>18</option>
                   <option value="19" <?php if ($day=='19'){echo 'selected="selected"';} ?>>19</option>
                   <option value="20" <?php if ($day=='20'){echo 'selected="selected"';} ?>>20</option>
                   <option value="21" <?php if ($day=='21'){echo 'selected="selected"';} ?>>21</option>
                   <option value="22" <?php if ($day=='22'){echo 'selected="selected"';} ?>>22</option>
                   <option value="23" <?php if ($day=='23'){echo 'selected="selected"';} ?>>23</option>
                   <option value="24" <?php if ($day=='24'){echo 'selected="selected"';} ?>>24</option>
                   <option value="25" <?php if ($day=='25'){echo 'selected="selected"';} ?>>25</option>
                   <option value="26" <?php if ($day=='26'){echo 'selected="selected"';} ?>>26</option>
                   <option value="27" <?php if ($day=='27'){echo 'selected="selected"';} ?>>27</option>
                   <option value="28" <?php if ($day=='28'){echo 'selected="selected"';} ?>>28</option>
                   <option value="29" <?php if ($day=='29'){echo 'selected="selected"';} ?>>29</option>
                   <option value="30" <?php if ($day=='30'){echo 'selected="selected"';} ?>>30</option>
                   <option value="31" <?php if ($day=='31'){echo 'selected="selected"';} ?>>31</option>
                 </select>
               </label>
             </div>
           <!-- End Day -->
         
           <!-- Year -->
             <div class="large-4 columns">
               <label>Year
                 <select id="year2" name="year2" required>
                   <option value="2008" <?php if ($year=='2008'){echo 'selected="selected"';} ?>>2008</option>
                   <option value="2009" <?php if ($year=='2009'){echo 'selected="selected"';} ?>>2009</option>
                   <option value="2010" <?php if ($year=='2010'){echo 'selected="selected"';} ?>>2010</option>
                   <option value="2011" <?php if ($year=='2011'){echo 'selected="selected"';} ?>>2011</option>
                   <option value="2012" <?php if ($year=='2012'){echo 'selected="selected"';} ?>>2012</option>
                   <option value="2013" <?php if ($year=='2013'){echo 'selected="selected"';} ?>>2013</option>
                   <option value="2014" <?php if ($year=='2014'){echo 'selected="selected"';} ?>>2014</option>
                 </select>
               </label>
             </div>
           <!-- End Year -->
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                  <input type="submit" class="tiny button" value="Submit">

              </div>
              </form>
            </div>
          <a class="close-reveal-modal">&#215;</a>
          </div>
          
          <!-- End Date Range -->
          
        </div>
      </div>
    <!-- End Header -->
    
    <!-- Report Body -->
    
      <div class="row">
      
      <!-- County Info -->
        <div class="large-6 columns">
          <h2 class="global-h2">County Information:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          
          <ul class="global-p" style="list-style:none; line-height:2">
            <li><b>Total Number of Users: </b><?php echo odbc_num_rows($user); ?></li>
            <li><b>Total Number of Active Users <span data-tooltip aria-haspopup="true" class="has-tip" title="Any user who has actually logged activity.">(?)</span>:</b>
			<?php echo odbc_num_rows($user); ?>
            <li><b>Total Number of Groups in County:</b> (Add this, Aaron)</li>
            <li><b>Groups by Type:</b> Feature coming soon.</li>
          </ul>
          
        </div>
      <!-- End County Info -->
      
      <!-- Overall Stats -->
        <div class="large-6 columns">
          <h2 class="global-h2">Overall Stats:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          
          <ul class="global-p" style="list-style:none; line-height:2">
            <li><b>Total Points Earned: </b><?php echo $points; ?></li>
            <li><b>Total Time Exercised: </b><?php echo floor($time/3600).' Hours '.($time%3600)/60 .' Minutes'; ?></li>
            <li><b>Total Miles From Distance Exercises <span data-tooltip aria-haspopup="true" class="has-tip" title="The total miles of all user's logged distance-based activites, such as running, biking, hiking, etc.">(?)</span>:</b><?php echo $distance; ?></li>
            <li><b>Virtual "Miles Walked" <span data-tooltip aria-haspopup="true" class="has-tip" title="The previous version of Walk Georgia converted all exercise (including things like yoga, which does not involve distance) into steps for the sake of comparison. We include this stat for members who still find this useful.">(?)</span>:</b> <?php echo number_format($points/107); ?></li>
          </ul>
        </div>
      <!-- End Overall Stats -->
      
      </div>
      
      <div class="row">
      
      <!-- County Users -->
        <div class="large-6 columns">
          <h2 class="global-h2">County Users: (<?php echo odbc_num_rows($user); ?>)</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          
        <!-- Member Filtering -->
          <form>
            <div class="row">
              <div class="large-12 columns">
                <label>Filter Users
                 <select>
                   <option value="">List by Points (Highest to Lowest)</option>
                   <option value="">List by Points (Lowest to Highest)</option>
                   <option value="">List by Time (Highest to Lowest)</option>
                   <option value="">List by Time (Lowest to Highest)</option>
                   <option value="">List in alphabetical order (Ascending)</option>
                   <option value="">List in alphabetical order (Descending)</option>
                 </select>
               </label>
             </div>
            </div>
          </form>  
        <!-- End Member Filtering -->
        
          <label style="margin-top:-20px;">
          By default, only the top ten users are shown based on total points.  To change this, select the filter options above.  To show all users, click the "show all" button below.
          </label>
          
          <ol class="global-p" style="line-height:2;">
            <li>
              <b>John Doe</b>
              <ol class="global-p">2004 Points</ol>
              <ol class="global-p">15H 43M Exercised</ol>
              <ol class="global-p">12 Miles From Disrance Exercises</ol>
              <ol class="global-p">johndoe@johndoe.com</ol>
              <ol class="global-p">Groups: (List of groups)</ol>
            </li>
            <li>
              <b>John Doe</b>
              <ol class="global-p">2004 Points</ol>
              <ol class="global-p">15H 43M Exercised</ol>
              <ol class="global-p">12 Miles From Disrance Exercises</ol>
              <ol class="global-p">johndoe@johndoe.com</ol>
              <ol class="global-p">Groups: (List of groups)</ol>
            </li>
            <li>
              <b>John Doe</b>
              <ol class="global-p">2004 Points</ol>
              <ol class="global-p">15H 43M Exercised</ol>
              <ol class="global-p">12 Miles From Disrance Exercises</ol>
              <ol class="global-p">johndoe@johndoe.com</ol>
              <ol class="global-p">Groups: (List of groups)</ol>
            </li>
          </ol>
          
          <a href="#" class="tiny button">Show All Users</a> <a href="#" class="tiny button secondary">Download Users Info Spreadsheet</a>
        </div>
      <!-- End County Users -->
      
      <!-- County Groups -->
        <div class="large-6 columns">
          <h2 class="global-h2">Groups In County: (Add Number)</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          
          <!-- Subgroup Filtering -->
          <form>
            <div class="row">
              <div class="large-12 columns">
                <label>Filter Groups
                 <select>
                   <option value="">List by Points (Highest to Lowest)</option>
                   <option value="">List by Points (Lowest to Highest)</option>
                   <option value="">List by Time (Highest to Lowest)</option>
                   <option value="">List by Time (Lowest to Highest)</option>
                   <option value="">List in alphabetical order (Ascending)</option>
                   <option value="">List in alphabetical order (Descending)</option>
                 </select>
               </label>
             </div>
            </div>
          </form>  
        <!-- End Subgroup Filtering -->
          
          <label style="margin-top:-20px;">
          By default, the top three groups are shown based on amount of total points.  To change this, select the filter options above.  To show all user activites, click the "show all" button below.
          </label>
          
          <ol class="global-p" style="line-height:2;">
            <li>
              <b>Group Title</b>
              <ol class="global-p">Total Members:</ol>
              <ol class="global-p">Total Subgroups:</ol>
              <ol class="global-p">Total Points:</ol>
              <ol class="global-p">Total Time:</ol>
              <ol class="global-p">Total Distance <span data-tooltip aria-haspopup="true" class="has-tip" title="The total miles of all user's logged distance-based activites, such as running, biking, hiking, etc.">(?)</span>:</ol>
              <ol class="global-p">Points Into Miles Walked <span data-tooltip aria-haspopup="true" class="has-tip" title="The previous version of Walk Georgia converted all exercise (including things like yoga, which does not involve distance) into steps for the sake of comparison. We include this stat for members who still find this useful.">(?)</span>:</ol>
            </li>
            <li>
              <b>Group Title</b>
              <ol class="global-p">Total Members:</ol>
              <ol class="global-p">Total Subgroups:</ol>
              <ol class="global-p">Total Points:</ol>
              <ol class="global-p">Total Time:</ol>
              <ol class="global-p">Total Distance <span data-tooltip aria-haspopup="true" class="has-tip" title="The total miles of all user's logged distance-based activites, such as running, biking, hiking, etc.">(?)</span>:</ol>
              <ol class="global-p">Points Into Miles Walked <span data-tooltip aria-haspopup="true" class="has-tip" title="The previous version of Walk Georgia converted all exercise (including things like yoga, which does not involve distance) into steps for the sake of comparison. We include this stat for members who still find this useful.">(?)</span>:</ol>
            </li>
            <li>
              <b>Group Title</b>
              <ol class="global-p">Total Members:</ol>
              <ol class="global-p">Total Subgroups:</ol>
              <ol class="global-p">Total Points:</ol>
              <ol class="global-p">Total Time:</ol>
              <ol class="global-p">Total Distance <span data-tooltip aria-haspopup="true" class="has-tip" title="The total miles of all user's logged distance-based activites, such as running, biking, hiking, etc.">(?)</span>:</ol>
              <ol class="global-p">Points Into Miles Walked <span data-tooltip aria-haspopup="true" class="has-tip" title="The previous version of Walk Georgia converted all exercise (including things like yoga, which does not involve distance) into steps for the sake of comparison. We include this stat for members who still find this useful.">(?)</span>:</ol>
            </li>
          </ol>
          
          <a href="#" class="tiny button">Show All Groups</a>
          
        </div>
      <!-- End County Groups -->
      
      </div>
      
      <div class="row">
      
      <!-- Activity Breakdown -->
        <div class="large-6 columns">
          <h2 class="global-h2">Activity Breakdown:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          
          <!-- Activity Breakdown Filtering -->
          <form>
            <div class="row">
              <div class="large-12 columns">
                <label>Filter Activities
                 <select>
                   <option value="">List by Frequency (Highest to Lowest)</option>
                   <option value="">List by Frequency (Lowest to Highest)</option>
                   <option value="">List in alphabetical order (Ascending)</option>
                   <option value="">List in alphabetical order (Descending)</option>
                 </select>
               </label>
             </div>
            </div>
          </form>  
        <!-- Activity Breakdown Filtering -->
          
          <label style="margin-top:-20px;">
          By default, the top three user activities are shown based on amount of times logged.  To change this, select the filter options above.  To show all user activites, click the "show all" button below.
          </label>
          
          <ol class="global-p" style="line-height:2;">
            <li>
              <b>Walking</b>
              <ol class="global-p">Users Logging: 12</ol>
              <ol class="global-p">Points Earned: 4789</ol>
              <ol class="global-p">Time Spent: 2H 42M</ol>
            </li>
            <li>
              <b>Running</b>
              <ol class="global-p">Users Logging: 12</ol>
              <ol class="global-p">Points Earned: 4789</ol>
              <ol class="global-p">Time Spent: 2H 42M</ol>
            </li>
            <li>
              <b>Hiking</b>
              <ol class="global-p">Users Logging: 12</ol>
              <ol class="global-p">Points Earned: 4789</ol>
              <ol class="global-p">Time Spent: 2H 42M</ol>
            </li>
          </ol>
          
          <a href="#" class="tiny button">Show All Activities</a>
          
        </div>
      <!-- End Activity Breakdown -->
      
      <!-- Sessions -->
        <div class="large-6 columns">
          <h2 class="global-h2">Session Dates:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <p class="global-p">
            No sessions were attached to this report.
          </p>
          <label>
            You can also narrow the report to a specific date range by using the bottom at the top right of this report.
          </label>
        </div>
      <!-- End Session -->
      
      </div>
    
      <div class="row">
      
      <!-- County Sessions -->
        <div class="large-6 columns">
          <h2 class="global-h2">Session Dates:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <p class="global-p">
            No sessions were attached to this report.
          </p>
          <label>
            You can also narrow the report to a specific date range by using the bottom at the top right of this report.
          </label>
        </div>
      <!-- End County Session -->
      
      </div>
      <!-- Activity Breakdown -->
      <div class="row">
        <div class="large-12 columns">
          <h2 class="global-h2">Activity Breakdown:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <p class="global-p">This section gives the totals for each exercised users intered.  You can use this data to get an idea of how many users were doing certain types of activity.</p>   
        </div>
      </div>
      
      <div class="row" data-equalizer>
      <?php
      for($i =0; $i <= $count; $i++){
		  odbc_fetch_row($activity, $i);
	  ?>
      	<div class="large-4 columns center">
          <div class="panel" data-equalizer-watch>
            <p class="global-p"><b><?php echo ACTIVITY::activity_to_form(odbc_result($activity, 'AL_AID'));?></b></p>
            <hr style="margin-top:-10px;margin-bottom:10px;" />
            <ul class="global-p" style="list-style:none; line-height:2.5">
              <li><b>Time: <?php echo number_format(odbc_result($activity, 'AL_TIME')/3600)."h ".((odbc_result($activity, 'AL_TIME') % 3600)/60)."m"; ?></b></li>
              <li><b>Points: <?php echo odbc_result($activity, 'AL_PA'); ?></b></li>
              <li><b>Distance: <?php echo number_format(odbc_result($activity, 'AL_UNIT')) ?> Miles</b></li>
            </ul>
          </div>
        </div>
	  <?php 
	  }
	  ?>
        
      </div>
      <!-- End Activity Breakdown -->
      
      <div class="row">
        <div class="large-6 columns">
          <h2 class="global-h2">Top 10 Users:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
            <ol class="global-p">
            <?php
				$j=0;
				while(odbc_fetch_array($user)){
				    
						$stats = ACCOUNT::get_info(odbc_result($user, 'AL_UID'));
						?>
                        <li>
                			<?php echo $stats['FNAME'].' '.$stats['LNAME']; ?> | <?php echo odbc_result($user, 'AL_PA'); ?> Points 
              			</li>
                        <?php
				    
				}
				?>
            </ol>   
        </div>
      </div>
      
    <!-- End Report Body -->
    
     
         
      
    <!-- End Main Content -->
     
  </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/foundation/foundation.reveal.js"></script>
    <script src="js/foundation/foundation.alert.js"></script>
    <script src="js/foundation/foundation.magellan.js"></script>
    <script src="js/foundation/foundation.dropdown.js"></script>
    <script src="js/foundation/foundation.equalizer.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
