<?php
include_once("lib/template_api.php");
include_once("lib/back_api.php");
include_once("lib/sessions_api.php");
$ss = SESSION::secure_session();
if($_GET['logout']==1){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia', $_SESSION['valid']);
HTML_ELEMENT::top_nav();

//EDIT FROM HERE

?>
<div class="main">
<!-- End of the Beginning of Main Section -->

<!-- Page Header -->
<div class="row" style="margin-top:10px; padding:10px;">
  <div class="large-12" columns>
    <h1 class="global-h1"><b>My Calendar:</b></h1>
    <hr style="margin-top:10px;" />
  </div>
</div>
<!-- End Page Header -->

<div class="row">
  <div class="large-12 columns">    
  	<h2 class="global-h2">Current Sessions:</h2>
    <?php 
	$query = MSSQL::query("SELECT * FROM SESSION_MEMBER WHERE U_ID='".$_SESSION['ID']."'");
	while($search = odbc_fetch_array($query)) {  
    	$sessions[] = $search['S_ID']; 
	}
	$current = SESSIONS::listCurrentSessions($sessions); ?>
    <h2 class="global-h2" style="margin-top:40px;">Upcoming Sessions:</h2>
    <?php
	$upcoming = SESSIONS::listUpcomingSessions($sessions); ?>
    <br />
    <br />
    
    <div class="large-12 columns">
      <button class="small" data-reveal-id="event-modal">Create a New Session</button>
      <br />
      <small style="margin-left:15px; padding-top:-10px;"><a data-reveal-id="completed-event-modal">View Completed Sessions</a></small>
    
  </div>
</div>
</div>

<div id="event-modal" class="reveal-modal" data-reveal>
  <form id="create_session" method="post" action="session/create_session.php">
  <div class="row">
  	<input type="hidden" id="PARENT_ID" name="PARENT_ID" value="U.<?php echo $_SESSION['ID']; ?>" />
    <div class="large-12 columns">
    <h5 class="modal-header">Create a Session:</h5>
    <hr />
      <label>
        <input type="text" id="S_NAME" name="S_NAME"  placeholder="Session / Event Title" />
      </label>
    </div>
  </div>
  <div class="row">
    <div class="large-12 columns">
      <label>
        <textarea style="height:100px;" id="S_DESC" name="S_DESC" placeholder="Session / Event Description"></textarea>
      </label>
    </div>
  </div>
    <div class="row">
    <div class="large-12 columns">
      <h5 class="modal-header">Start/End Dates:</h5>
      Starts:<br /><input type="date" id="STARTDATE" name="STARTDATE" /><br />
      Ends:<br /><input type="date" id="ENDDATE" name="ENDDATE" />
      <br />
    </div>
  </div>
  <div class="row">
    <div class="large-12 columns">
      <label>Session Visibility</label>
      <input type="radio" id="S_VIS_1" name="S_VIS_1" value="1"><label for="S_VIS_1">Public</label>
      <p class="global-p" style="margin-top:-15px; margin-bottom:10px;">(Any user can find the group and join)</p>
      <input type="radio" id="S_VIS_0" name="S_VIS_0" value="0"><label for="S_VIS_0">Private</label>
      <br />
      <button class="small">Next</button>
    </div>
  </div>
</form>


  <a class="close-reveal-modal">&#215;</a>
</div>

<div id="completed-event-modal" class="reveal-modal" data-reveal>
  <div class="row">
    <div class="large-12 columns">
          
      <h5 class="calendarheader"><b>Completed Sessions and Events:</b></h5>
    
        <?php $current = SESSIONS::listCompletedSessions($sessions); ?>
        
    </div>
  </div>
  <a class="close-reveal-modal">&#215;</a>
</div>

<!-- End of the Main Section -->
</div>
<!-- End of the Main Section -->

<!-- END MAIN SECTION -->
<?php

//STOP EDITING

HTML_ELEMENT::footer();

	//JAVASCRIPTS GO HERE
?>
	<script type="text/javascript">
        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script src="../js/foundation/foundation.reveal.js"></script>
    <script src="../js/foundation/foundation.tooltip.js"></script>
    <script src="../js/foundation/foundation.offcanvas.js"></script>
    <script src="../js/foundation/foundation.equalizer.js"></script>
    <script src="../js/foundation/foundation.dropdown.js"></script>
    <script src="../js/foundation/foundation.alert.js"></script>
	<script src="../js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>