<?php
include_once("lib/template_api.php");
include_once ("lib/back_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

<div id="main">

  <!-- Global Header Container -->
  <div id="top nojava">

    <!-- Header Image Container -->
    <div id="slideshow">

      <!-- Page Title Container -->
      <div id="slideshow-title">
        <div class="row nojava center">
          <h2 class="custom-font-big-white">implementation guides</h2>
          <p class="gotham-small-white">Help Getting Walk Georgia Up and Running with Your Program</p>
          <br /><br /><br />

          <!-- Header Buttons
          <div class="large-12 columns expand">
            <div class="row center nojava">
              <div class="small-12 medium-4 large-4 columns">
                <a href="#ourStory" class="button small expand round">Our Story</a>
              </div>
              <div class="small-12 medium-4 large-4 columns">
                <a href="faq.php" class="button small success expand round">F.A.Q.</a>
              </div>
              <div class="small-12 medium-4 large-4 columns">
                <a href="resources/index.php" class="button small expand round secondary">Resources</a>
              </div>
            </div>
          </div>
           End Header Buttons -->

        </div>
      </div>
      <!-- End Page Title Container -->

    </div>
    <!-- End Header Image Container -->

  </div>
  <!-- End Global Header Container -->

    <!-- Main Content -->

    <div class="row" style="margin-top:20px;">
      <div class="large-12 columns">
        <h2 class="custom-font-small-blue">Select Your Program Type</h2>
        <hr style="margin-top:-5px;" />
      </div>
    </div>
    <div class="row center">
      <div class="large-6 medium-6 columns">
        <div class="white-bg">
            <i class="fi-book-bookmark size-60 blue"></i>
            <h2 class="global-h2">Schools</h2>
            <hr style="margin-top:-5px; margin-bottom:10px;">
            <a href="resources/implementation/SchoolImplementation2015.pdf" target="_blank" class="button tiny expand">Download School Guide</a>
            <p class="global-p justify">If you are a teacher or administrator, we've put together some resources especially for you.  Walk Georgia has a lot of helpful tools designed specifically for use by school systems, individual schools, and even individual classes.  Download the guide to find out more.</p>
          </div>
      </div>
      <div class="large-6 medium-6 columns">
        <div class="white-bg">
            <i class="fi-address-book size-60 blue"></i>
            <h2 class="global-h2">Organizations</h2>
            <hr style="margin-top:-5px; margin-bottom:10px;">
            <a href="resources/implementation/OrganizationImplementation2015.pdf" target="_blank" class="button tiny expand">Download Organization Guide</a>
            <p class="global-p justify">If you are the head of an orgaization, we've got a guide for you.  Whether you are part of a non-profit, corporation, religious group, or social club - this guide can help you get started with Walk Georgia.</p>
          </div>
      </div>
    </div>


  <!-- End Main -->
  </div>
  <!-- End Main -->

<?php

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script src="js/foundation/foundation.abide.js"></script>
    <script src="js/foundation/foundation.reveal.js"></script>
    <script src="js/foundation/foundation.tooltip.js"></script>
    <script src="js/foundation/foundation.offcanvas.js"></script>
    <script src="js/foundation/foundation.equalizer.js"></script>
    <script src="js/foundation/foundation.dropdown.js"></script>
    <script type="text/javascript" src="js/sha512.js"></script>
    <script type="text/javascript" src="js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>
