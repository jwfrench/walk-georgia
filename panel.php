<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING)==1){
	SESSION::logout();
	REDIRECT::home();
}
//If the user is logged in
if($_SESSION['valid']){
	if(filter_input(INPUT_GET, 'uid', FILTER_SANITIZE_STRING) != null){
		$ID =filter_input(INPUT_GET, 'uid', FILTER_SANITIZE_STRING);
	}else{
		$ID= $_SESSION['ID'];
	}
	$info = ACCOUNT::get_info($ID);
}
if(!($info['IS_ADMIN'])){
	REDIRECT::home();
}
HTML_ELEMENT::head('Administration', $_SESSION['valid']);
HTML_ELEMENT::top_nav();

//START EDITING HERE

?>
<div style="margin-top:20px;"></div>
    
    <!-- Main Content -->
    
      <div class="row">
        <div class="large-12 columns center">
          <h1 class="global-h1"><a href="panel.php">Admin Backend</a></h1>
        </div>
      </div>
      
    <!-- Search Function -->  
      <div class="row">
        <div class="large-12 columns">
          <h2 class="global-h1-small">Search For Users:</h2>
        </div>
      </div>
      <div class="row">
        <div class="large-12 columns">
          <form>
            <div class="row">
              <div class="large-6 columns">
                <div class="row collapse">
                  <div class="small-10 columns">
                    <input type="text" name="search" id="search" placeholder="Search by name or email.">
                  </div>
                  <div class="small-2 columns">
                    <input class="button postfix" type="submit" value="Search">
                  </div>
                </div>
              </div>
            </div>
          </form >
        </div>
      </div>
      
    <!-- End Search Function -->
    <?php
		if(filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING)){
			$search = filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING);
			$sql = "SELECT * FROM LOGIN WHERE ( (L_E LIKE '%$search%') OR (L_FNAME LIKE '%$search%') OR (L_LNAME LIKE '%$search%')) ORDER BY L_LNAME";
			$execute = MSSQL::query($sql);
			
	?>
      <div class="row">
        <div class="large-12 columns">
        Search results:
        <?php
		$i=0;
		 while(odbc_fetch_row($execute)){
			$i++;
			$USER_ID = odbc_result($execute, 1);
			$info = ACCOUNT::get_info($USER_ID);
			?>
        <!-- User -->
          <div class="group-unit">
            <div class="row">
              <div class="large-8 medium-7 small-12 columns">
              <?php
          		ACCOUNT::avatar_small($USER_ID);
		  	  ?>
                <h3 class="global-h3" style="padding-top:18px;"><?php echo $info['FNAME']." ".$info['LNAME'];?> | <?php echo $info['EMAIL'];?></h3>
              </div>
              <div class="large-4 medium-5 small-12 columns right" style="padding-top:15px; padding-left:20%; text-align:left;">
                <a href="" class="button tiny" data-dropdown="drop<?php echo $USER_ID; ?>">Admin Options</a>
          
                <ul id="drop<?php echo $USER_ID; ?>" class="f-dropdown" style="text-align:left;" data-dropdown-content>
                  <li><a href="index.php?uid=<?php echo $USER_ID; ?>" class="alert">Visit User Page</a></li>
                  <li><a href="edit_user_groups.php?id=<?php echo $USER_ID; ?>">Edit User Groups</a></li>
                  <li><a href="edit_user_sessions.php?id=<?php echo $USER_ID; ?>">Edit User Sessions</a></li>
                  <li><a href="promote_to_county_admin.php?id=<?php echo $USER_ID; ?>">Promote User To CA</a></li>
                  <li><a href="reset_password.php?EMAIL=<?php echo $info['EMAIL']; ?>">Reset Password</a></li>
                  <li><a href="delete_user.php?id=<?php echo $USER_ID; ?>" class="alert">Delete User</a></li>
                </ul>
              </div>
            </div>
          </div>
        <!-- End User -->
        <?php 
		 }
		}else{
			?>
            <div class="row">
            Please search for the user you wish to help.
            </div>
            <?php
}
		?>
        </div>
      </div>
    <!-- User List -->
    
    
    
    <!-- End User List -->
    
    <!-- End Main Content -->
    <hr />
    
<?php
//END EDITING HERE

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
  <!-- End Footer -->
  </body>
</html>