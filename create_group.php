<?php
include_once("lib/template_api.php");
include_once ("lib/back_api.php");
$ss = SESSION::secure_session();
if($_GET['logout']==1){
	SESSION::logout();
	REDIRECT::home();
}
if(isset($_POST['file'])){
		$img = ACCOUNT::avatar_upload($_SESSION['ID']);
	}
HTML_ELEMENT::head('TEST');
HTML_ELEMENT::top_nav();
//If the user is logged in
if($_SESSION['valid']){
	if(isset($_GET['uid'])){
		$ID =$_GET['uid'];
	}else{
		$ID= $_SESSION['ID'];
	}
	$info = ACCOUNT::get_info($ID);
//EDIT HERE
?> 

<div id="main">
  <div id="top nojava">
  
  
    
  </div>

  <!-- End of Top Section -->
  
  <!-- Main Section -->
  
  <div class="row">
    <div class="large-12 columns">
   
      <h2 class="custom-font-small-blue">Create a Group</h2>
      <hr style="margin-top:-5px; margin-bottom:-20px;">
   
      <!-- Group Type Select -->   
      <div class="row">
    
        <div class="large-4 medium-4 columns center">
          <div class="gray-bg">
            <i class="fi-torsos-all size-60 blue"></i>
            <h2 class="global-h2">Default Group</h2>
            <hr style="margin-top:-5px; margin-bottom:10px;">
            <a href="#" class="button tiny expand">Create Group</a>
            <label class="justify">This is the default group type.  It is robust enough to handle pretty much anything you’d want to throw at it.  You can use this type of group to keep track of your fitness with your friends, family, etc.  If you’re not sure what type of group to make, you can’t go wrong with this one.</label>
          </div>
        </div>
      
        <div class="large-4 medium-4 columns center">
          <div class="white-bg">
            <i class="fi-book-bookmark size-60 blue"></i>
            <h2 class="global-h2">Schools</h2>
            <hr style="margin-top:-5px; margin-bottom:10px;">
            <a href="#" class="button tiny expand">Create School Group</a>
            <label class="justify">If you are a teacher or administrator, then this is the group for you.  For example, if you are a P.E. teacher, this group type allows you to actually log activity for your whole class within the group page itself.  That way, your personal stats stay separate from your class, which makes record keeping much easier. </label>
          </div>
        </div>
      
        <div class="large-4 medium-4 columns center">
          <div class="white-bg">
            <i class="fi-address-book size-60 blue"></i>
            <h2 class="global-h2">Organizations</h2>
            <hr style="margin-top:-5px; margin-bottom:10px;">
            <a href="#" class="button tiny expand">Create Organization Group</a>
            <label class="justify">If you want to use Walk Georgia to run your company’s wellness program, provide incentives, or just encourage friendly competition among your employees, then this is the group for you.  Or if you are a charity or organization, this template gives you a few tools to make running your program easier. </label>
          </div>
        </div>  
      
    </div>
    <!-- End Group Type Select -->
    
    <!-- Group Name and Info -->
    <div class="row" style="margin-top:20px;">
      <div class="large-12 columns">
        <h2 class="global-h2">Group Information:</h2>
        <hr style="margin-top:-5px; margin-bottom:10px;">
        <p class="global-p">
          Aaron, this is where the default Group Info slide goes. 
        </p>
      </div>
    </div>
    <!-- End Group Name and Info -->
    
    <!-- School Info -->
    <div class="row">
      <div class="large-12 columns">
        <h2 class="global-h2">School Information:</h2>
        <hr style="margin-top:-5px; margin-bottom:10px;">
      </div>
    </div>
    <div class="row">
      <div class="large-6 columns">
        <h2 class="global-h2-gray">School Type:</h2>
        <br />
        <br />
        <form>
        <label>Select School Type
        <select>
          <option value="">Elementary</option>
          <option value="">Middle</option>
          <option value="">High-School</option>
          <option value="">College/University</option>
          <option value="">Homeschool</option>
          <option value="">Other</option>
        </select>
      </label>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="large-6 columns">
        <h2 class="global-h2-gray">Next Steps:</h2>
        <label>After this group is created you can add grades and classrooms as subgroups to this main group in whatever fashion you like.  To add a subgroup, simply click the "Group Tools" button after the group is created and then click "Manage Subgroups."  You can then invite your teachers to be members and promote them to administrators of their classes.  Once they are administrators, they are able to log activity for their entire class by visiting their class page.</label>
      </div>
    </div>
    <!-- End School Info -->
    
    <!-- Organization Info -->
    <div class="row" style="margin-top:20px;">
      <div class="large-12 columns">
        <h2 class="global-h2">Organization Information:</h2>
        <hr style="margin-top:-5px; margin-bottom:10px;">
      </div>
    </div>
    <div class="row">
      <div class="large-6 columns">
        <h2 class="global-h2-gray">Organization Type:</h2>
        <br />
        <br />
        <form>
        <label>Select Organization Type
        <select>
          <option value="">Business</option>
          <option value="">Non-Profit</option>
          <option value="">Organization</option>
          <option value="">Other</option>
        </select>
      </label>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="large-6 columns">
        <h2 class="global-h2-gray">Next Steps:</h2>
        <label>After this group is created you can add subgroups to keep track of different parts of your organization.  Once you are on the group page. simply click on "Group Tools" and then click on "Manage Subgroups" to add these different levels.  You can then promote individual members to be administrators to aid in logging for a group of people, or just simply keeping track of small changes.  How you do it from here is totally up to you.</label>
      </div>
    </div>
    <!-- End Organization Info -->
    
    <!-- Additional Info Section -->
    <div class="row" style="margin-top:20px;">
      <div class="large-6 columns">
        <h2 class="global-h2">Additional Information:</h2>
        <hr style="margin-top:-5px; margin-bottom:10px;">
        <p class="global-p">Aaron, I think we need to overhaul this part.  I think they need to be able to add multiple fields and then be able to see what they have added.  We can talk about this after lunch.</p>
      </div>
    </div>
    <!-- End Additional Info Section -->
    
  </div>
  </div>
  
  <!-- End of Main Section --> 

<?php 
}
//If user is not logged in
		else{
			?>
<div id="main">
  <div id="top nojava">
  
  
    
  </div>

  <!-- End of Top Section -->
  
  <!-- Main Section -->
  
  
  <!-- End of Main Section --> 

        	<?php
		}

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script type="text/javascript" src="js/foundation.min.js"></script>
    <script type="text/javascript" src="js/sha512.js"></script>
    <script type="text/javascript" src="js/log_form.js"></script>
    <script type="text/javascript" src="js/frontend.js"></script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script>
      $(document).foundation();
	</script>
    <?php
		$first_time = ACCOUNT::first_visit($_SESSION['ID']);
		if($first_time){
			?>
            <script>
				$(document).foundation('joyride', 'start');
			</script>
            <?php
		$first_visit_removal = ACCOUNT::first_visit_remove($_SESSION['ID']);
		}
	?>
    
     
  <!-- End Footer -->
  </body>
</html>