<?php
include_once("lib/back_api.php");
include_once("lib/groups_api.php");
//GET THE COUNTY INFO
$sql0 = 'SELECT COUNT(DISTINCT U_ID) AS COUNT FROM GROUP_MEMBER WHERE G_ID =  \''.$_GET['group'].'\'';
$sql1 = 'SELECT AL_UID, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\', SUM(AL_UNIT) AS \'AL_UNIT\' FROM LOG WHERE AL_UID IN (SELECT U_ID FROM GROUP_MEMBER WHERE G_ID = \''.$_GET['group'].'\') GROUP BY AL_UID ORDER BY AL_UNIT DESC, AL_PA DESC;';
$sql2 = 'SELECT AL_AID, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\', SUM(AL_UNIT) AS \'AL_UNIT\' FROM LOG WHERE AL_UID IN (SELECT U_ID FROM GROUP_MEMBER WHERE G_ID = \''.$_GET['group'].'\') GROUP BY AL_AID ORDER BY AL_UNIT DESC, AL_PA DESC;';
$group = GROUP::get_info($_GET['group']);
$total_users = MSSQL::query($sql0);
$user = MSSQL::query($sql1);
$activity = MSSQL::query($sql2);
$count = 0;
$no_of_activities = odbc_num_rows($activity);
while(odbc_fetch_array($activity)){
	$aid = odbc_result($activity, 'AL_AID');
	$time += odbc_result($activity, 'AL_TIME');
	$points += odbc_result($activity, 'AL_PA');
	if(($aid == 1) ||($aid == 2) ||($aid == 3) ||($aid == 47) ||($aid == 50) ||($aid == 68) ||($aid == 70)){
		$distance += odbc_result($activity, 'AL_UNIT');
		$count +=1;
	}
}
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Foundation | Welcome</title>
    <link rel="stylesheet" href="../../css/foundation.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    
  <div id="main">
    
    <div style="margin-top:20px;"></div>
    
    <!-- Header -->
      <div class="row" style="margin-bottom:20px;">
        <div class="large-12 columns center">
          <img src="img/single-color-logo.png" alt="logo" />
          <h1 class="custom-font-small">Official Report</h1>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <h2 class="custom-font-small"><?php echo $group['G_NAME']; ?></h2>
          <!-- <a href="#" class="button tiny">Printer Friendly Version</a> -->
        </div>
      </div>
    <!-- End Header -->
    <!-- Report Body -->
      <div class="row">
      <!-- Overall Stats -->
        <div class="large-6 columns">
          <h2 class="global-h2">Overall Stats:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <ul class="global-p" style="list-style:none; line-height:2.5">
            <li><b>Total Number of Users: </b><?php echo odbc_result($total_users, 'COUNT'); ?></li>
            <li><b>Total Number of Active Users: </b><?php echo odbc_num_rows($user); ?></li>
            <li><b>Total Time Exercised: </b><?php echo floor($time/3600).' Hours '.($time%3600)/60 .' Minutes'; ?></li>
            <li><b>Total Actual Miles Traveled: </b><?php echo $distance; ?></li>
            <li><b>Total Points Earned: </b><?php echo $points; ?></li>
            <li><b>Points Converted Into Miles Walked: </b><?php echo number_format($points/80); ?></li>
          </ul>
        </div>
      <!-- End Overall Stats -->
      <!-- Sessions -->
        <div class="large-6 columns">
          <h2 class="global-h2">Session Dates:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <ul class="global-p" style="list-style:none; line-height:2.5">
            No sessions were attached to this report.
            <!-- If no sessions were attached to group or county, then use this:
            No sessions were attached to this report.
             -->
          </ul>
        </div>
      <!-- End Session -->  
      </div>
      <!-- Activity Breakdown -->
      <div class="row">
        <div class="large-12 columns">
          <h2 class="global-h2">Activity Breakdown:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <p class="global-p">This section gives the totals for each exercised users intered.  You can use this data to get an idea of how many users were doing certain types of activity.</p>   
        </div>
      </div>
      
      <div class="row" data-equalizer>
      <?php
      for($i =1; $i <= $no_of_activities; $i++){
		  odbc_fetch_row($activity, $i);
	  ?>
      	<div class="large-4 columns center">
          <div class="panel" data-equalizer-watch>
            <p class="global-p"><b><?php echo ACTIVITY::activity_to_form(odbc_result($activity, 'AL_AID'));?></b></p>
            <hr style="margin-top:-10px;margin-bottom:10px;" />
            <ul class="global-p" style="list-style:none; line-height:2.5">
              <li><b>Time: <?php echo number_format(odbc_result($activity, 'AL_TIME')/3600)."h ".((odbc_result($activity, 'AL_TIME') % 3600)/60)."m"; ?></b></li>
              <li><b>Points: <?php echo odbc_result($activity, 'AL_PA'); ?></b></li>
              <?php 
			  	if( (number_format(odbc_result($activity, 'AL_UNIT'))) != '0'){ ?>
              <li><b><?php if((ACTIVITY::is_distance_based(odbc_result($activity, 'AL_AID')))){$unit = 'Distance';}else{$unit = 'Reps';} echo $unit;?>: <?php echo number_format(odbc_result($activity, 'AL_UNIT')) ?> Miles</b></li>
              <?php }else{ echo '<br />';} ?>
            </ul>
          </div>
        </div>
	  <?php 
	  }
	  ?>
        
      </div>
      <!-- End Activity Breakdown -->
      
      <div class="row">
        <div class="large-8 columns">
          <h2 class="global-h2">Users:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
            <ol class="global-p">
            <?php
				$j=0;
				while($j < odbc_num_rows($user)){
					
					odbc_fetch_array($user);
				    
						$stats = ACCOUNT::get_info(odbc_result($user, 'AL_UID'));
						$query = MSSQL::query("SELECT * FROM GROUP_MEMBER WHERE G_ID ='".$_GET['group']."' AND U_ID='".odbc_result($user, 'AL_UID')."'");
						$meta = odbc_result($query, 'META');
						$meta = explode(':', $meta);
						$z = 0;
						?>
                        <li>
                			<?php echo $stats['FNAME'].' '.$stats['LNAME']; ?> | <?php echo odbc_result($user, 'AL_PA'); ?> Points | <?php echo number_format(odbc_result($user, 'AL_TIME')/3600)." h ".number_format((odbc_result($user, 'AL_TIME')%3600)/60)." m"; ?> | <?php echo $meta[0]; ?> | <?php echo $meta[1]; ?>
              			</li>
                        <?php
						$list[] = array(odbc_result($user, 'AL_UID'), $stats['FNAME'], $stats['LNAME'], $stats['EMAIL'], $stats['COUNTY'], number_format(odbc_result($user, 'AL_TIME')/3600)." h ".number_format((odbc_result($user, 'AL_TIME')%3600)/60)." m", $meta[0], $meta[1] );
				    $j++;
				}
				$fp = fopen('group/reports/'.$_GET['group'].'.csv', 'w');
				foreach ($list as $fields) {
					fputcsv($fp, $fields);
				}
				?>
            </ol>   
        </div>
      </div>
      
    <!-- End Report Body -->
    
     
         
      
    <!-- End Main Content -->
     
  </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/foundation/foundation.reveal.js"></script>
    <script src="js/foundation/foundation.alert.js"></script>
    <script src="js/foundation/foundation.magellan.js"></script>
    <script src="js/foundation/foundation.dropdown.js"></script>
    <script src="js/foundation/foundation.equalizer.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
