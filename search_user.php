<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/groups_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
$HTTP_HOST = filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
	SESSION::logout();
	REDIRECT::home();
}
//If the user is logged in
$valid = 0;
if(isset($_SESSION['valid'])){
	$valid = $_SESSION['valid'];
	$ID= $_SESSION['ID'];
	$info = ACCOUNT::get_info($ID);
}
if((filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING)!= null)){
	$_GET['search']='';
}
HTML_ELEMENT::head('Search', $valid);
HTML_ELEMENT::top_nav();

//START EDITING HERE
?>
<div style="margin-top:20px;"></div>
    
    <!-- Main Content -->
    <div class="blue-bg" style="margin-top:-1.3em; margin-bottom:2em;">
      <div class="row">
        <div class="large-12 columns center">
          <h1 class="custom-font-big-white">Search results</h1>
        </div>
      </div>
    </div>
    <?php 
		//First, we generate a table the user can use quickly that searches for users within their county, groups, and sessions as long as those accounts are public
		$sql = 'SELECT * FROM LOGIN WHERE L_PRIVACY =\'0\' AND (L_FNAME LIKE \'%'.filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING).'%\' OR L_LNAME LIKE \'%'.filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING).'%\' OR L_E LIKE \'%'.filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING).'%\') ORDER BY L_LNAME ASC';
		$query = MSSQL::query($sql);
		//now we build a table for DataTables to work on
	?>
    <div class="row">
    	<div class="data-table">
			<table class="display" id="" style="width:100%;">
			  <thead>
				<th class="global-p-white" style="width:60px; padding:5px;">
				  <img src="../../img/default-group-icon.png" alt="default group icon" style="display:block; margin:auto;"/>
				</th>
				<th class="global-h2">
					User Name
				</th>
			  </thead>
			<?php
		while(odbc_fetch_row($query)){  
				$UID = odbc_result($query, 'L_ID');
				$FN = odbc_result($query, 'L_FNAME');
				$LN = odbc_result($query, 'L_LNAME');
				?>
				<tr>
				  <td class="global-p" style="width:60px; padding:5px;">
					  <a href="<?php echo 'http://'.$HTTP_HOST.'/'; ?>index.php?uid=<?php echo $UID; ?>">
						<?php $avatar = ACCOUNT::avatar_small($UID);?>
					</a>
				  </td>
				  <td class="global-p">
					<a href="<?php echo 'http://'.$HTTP_HOST.'/'; ?>index.php?uid=<?php echo $UID; ?>">
					  <br />
					  <p class="global-p" style="font-size:20px; text-decoration:underline;"><?php echo $FN.' '.$LN ?></p>
					</a>
				  </td>
				</tr>
	  <?php			 
		}
	  ?>
			</table>
			</div>
        
    <!-- End Results-->
    </div>
    <div style="height:400px;"></div>
<?php
//END EDITING HERE

//JAVASCRIPTS GO HERE
?>
	<div class="footer">
      <div class="row">
        <div>
          <a class="tiny button alert" href="http://<?php echo $HTTP_HOST; ?>/contact-us.php"> Report a Bug </a>
        </div>
        
        <p class="global-p">
          <a href="http://www.facebook.com/walkgeorgia">Facebook</a> | <a href="https://twitter.com/walkga">Twitter</a> | <a href="http://blog.extension.uga.edu/walkgeorgia">Walk Georgia Blog</a>
        </p>
        <br />
          <small>The College of Agricultural and Environmental Sciences and the College of Family and Consumer Sciences cooperating.
The University of Georgia &copy; 2014. All Rights Reserved.</small>
          <hr><img alt="The University of Georgia Cooperative Extension" style="width:87px; height:31px;" src="../img/ext.png"><br><br>
        </div>
      </div>
    </div>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="../js/foundation.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/plug-ins/380cb78f450/integration/foundation/dataTables.foundation.js"></script>
<script type="text/javascript" src="../js/frontend.js"></script>
<script type="text/javascript" src="../js/log_form.js"></script>
<script>
     $(document).foundation();
</script>
<script>
$(document).ready(function() {
    	  $('table.display').DataTable({
			"oLanguage": {
              "sEmptyTable": " "
            },
			"bFilter":false,
	        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
			responsive:true
		  });
		} );
</script>
</body>
</html>