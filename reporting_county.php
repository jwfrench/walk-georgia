<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/back_api.php");
include_once ("$root/lib/groups_api.php");
$ss = SESSION::secure_session();
$current_user_info = ACCOUNT::get_info($_SESSION['ID']);
$permission = 0;
if (!$current_user_info['IS_COUNTY_ADMIN']) {
    $redirect = REDIRECT::home('Access to reports is limited to County Administrators');
}
$county = (filter_input(INPUT_GET, 'county', FILTER_SANITIZE_STRING)) !== null ? filter_input(INPUT_GET, 'county', FILTER_SANITIZE_STRING) : '';
$date_range = '';
$day1 = '';
$day2 = '';
$month1 = '';
$month2 = '';
$year1 = '';
$year2 = '';
//GET THE DATE INFO
if(filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING) != null) {
  if(filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING) == null) {
    header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting_county.php?county='. filter_input(INPUT_GET, 'county', FILTER_SANITIZE_STRING).'&datepicker='.filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING).'&datepickerEnd='.urlencode(date("m/d/Y")).'');
  }
}
if(filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING) != null) {
  if(filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING) == null) {
    header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting_county.php?county='. filter_input(INPUT_GET, 'county', FILTER_SANITIZE_STRING).'&datepicker=01/01/'.date("Y").'&datepickerEnd='.filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING).'');
  }
}
if(filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING) != null) {
  if(filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING) == null) {
    header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting_county.php?county='. filter_input(INPUT_GET, 'county', FILTER_SANITIZE_STRING).'&datepickerMobile='.filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING).'&datepickerEndMobile='.urlencode(date("m/d/Y")));
  }
}
if(filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING) != null) {
  if(filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING) == null) {
    header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting_county.php?county='. filter_input(INPUT_GET, 'county', FILTER_SANITIZE_STRING).'&datepickerMobile=01/01/'.date("Y").'&datepickerEndMobile='.filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING));
  }
}
if ((filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING) != null) && (filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING) != null)) {
    //day 1
    $date1 = explode('/', filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING));
    if(count($date1) != 3) {
      header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting_county.php?county='. filter_input(INPUT_GET, 'county', FILTER_SANITIZE_STRING).'&datepicker=01/01/'.date("Y").'&datepickerEnd='.urlencode(date("m/d/Y")));
    }
    $month1 = $date1[0];
    $day1 = $date1[1];
    $year1 = $date1[2];

    //day 2
    $date2 = explode('/', filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING));
    if(count($date2) != 3) {
      header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting_county.php?county='. filter_input(INPUT_GET, 'county', FILTER_SANITIZE_STRING).'&datepicker=01/01/'.date("Y").'&datepickerEnd='.urlencode(date("m/d/Y")));
    }
    $month2 = $date2[0];
    $day2 = $date2[1];
    $year2 = $date2[2];
    $date1= $year1.'-'.$month1.'-'.$day1;
    $date2= $year2.'-'.$month2.'-'.$day2;

    $date_range = 'AL_DATE >= \'' . $date1 . '\' AND AL_DATE <= \'' . $date2 . '\' AND';
}
if ((filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING) != null) && (filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING) != null)) {
    //day 1
    $date1 = explode('-', filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING));
    if(count($date1) != 3) {
      header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting_county.php?county='. filter_input(INPUT_GET, 'county', FILTER_SANITIZE_STRING).'&datepickerMobile=01/01/'.date("Y").'&datepickerEndMobile='.urlencode(date("m/d/Y")));
    }
    $month1 = $date1[1];
    $day1 = $date1[2];
    $year1 = $date1[0];

    //day 2
    $date2 = explode('-', filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING));
    if(count($date2) != 3) {
      header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting_county.php?county='. filter_input(INPUT_GET, 'county', FILTER_SANITIZE_STRING).'&datepickerMobile=01/01/'.date("Y").'&datepickerEndMobile='.urlencode(date("m/d/Y")));
    }
    $month2 = $date2[1];
    $day2 = $date2[2];
    $year2 = $date2[0];
    $date1= $year1.'-'.$month1.'-'.$day1;
    $date2= $year2.'-'.$month2.'-'.$day2;

    $date_range = 'AL_DATE >= \'' . $date1 . '\' AND AL_DATE <= \'' . $date2 . '\' AND';
}


//query for total users
$sql0 = 'SELECT COUNT(DISTINCT L_ID) AS COUNT FROM LOGIN WHERE L_COUNTY =  \'' . $county . '\'';

//query for user data
$user_order = 'AL_PA DESC';
if (filter_input(INPUT_GET, 'user_order', FILTER_SANITIZE_STRING) != null) {
    $user_order = filter_input(INPUT_GET, 'user_order', FILTER_SANITIZE_STRING);
}
$sql1 = 'SELECT L_ID, L_FNAME, L_LNAME, L_COUNTY, L_E, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\' FROM LOG INNER JOIN LOGIN ON AL_UID = L_ID  WHERE  ' . $date_range . ' L_COUNTY =  \'' . $county . '\' GROUP BY L_FNAME, L_LNAME, L_COUNTY, L_ID, L_E ORDER BY ' . $user_order . ';';
$user = MSSQL::query($sql1);

//query for activity
$activity_order = 'AL_PA DESC';
if (filter_input(INPUT_GET, 'activity_order', FILTER_SANITIZE_STRING) != null) {
    $activity_order = filter_input(INPUT_GET, 'activity_order', FILTER_SANITIZE_STRING);
}
$sql2 = 'SELECT AL_AID, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\', SUM(AL_UNIT) AS \'AL_UNIT\', COUNT(AL_AID) AS COUNT FROM LOG WHERE ' . $date_range . ' AL_UID IN (SELECT L_ID FROM LOGIN WHERE L_COUNTY =   \'' . $county . '\') GROUP BY AL_AID ORDER BY ' . $activity_order . ';';
$activity = MSSQL::query($sql2);

//query for subgroupsfilter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING)
$subs_order = 'POINTS DESC';
if (filter_input(INPUT_GET, 'subs_order', FILTER_SANITIZE_STRING) != null) {
    $subs_order = filter_input(INPUT_GET, 'subs_order', FILTER_SANITIZE_STRING);
}
$sql4 = 'SELECT GROUPS.G_ID, G_NAME, COUNT(1) AS RECORDS FROM GROUPS INNER JOIN GROUP_MEMBER ON GROUP_MEMBER.G_ID = GROUPS.G_ID INNER JOIN LOG ON AL_UID = U_ID WHERE G_COUNTY=\'' . $county . '\' GROUP BY GROUPS.G_ID, G_NAME;';

//query for non-active user data
$sql5 = 'SELECT L_ID, L_FNAME, L_LNAME, L_E, L_COUNTY FROM LOGIN WHERE L_COUNTY =\'' . $county . '\' AND L_ID NOT IN (SELECT AL_UID FROM LOG) ORDER BY L_LNAME DESC;';

$total_users = MSSQL::query($sql0);
$user = MSSQL::query($sql1);
$activity = MSSQL::query($sql2);
$subs = MSSQL::query($sql4);
$non_active = MSSQL::query($sql5);

$count = 0;
$no_of_activities = odbc_num_rows($activity);

$aid = '';
$time = 0;
$points = 0;
$distance = 0;
while (odbc_fetch_array($activity)) {
    $aid = odbc_result($activity, 'AL_AID');
    $time += odbc_result($activity, 'AL_TIME');
    $points += odbc_result($activity, 'AL_PA');
    if (($aid == 1) || ($aid == 2) || ($aid == 3) || ($aid == 47) || ($aid == 50) || ($aid == 68) || ($aid == 70)) {
        $distance += odbc_result($activity, 'AL_UNIT');
        $count +=1;
    }
}
$subbies = MSSQL::query('SELECT * FROM GROUPS WHERE G_COUNTY=\'' . $county . '\'');
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Walk Georgia | Reporting</title>
    <link rel="stylesheet" href="../../css/foundation.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.1/b-flash-1.2.1/b-html5-1.2.1/b-print-1.2.1/r-2.1.0/datatables.min.css"/>
    <link type="text/css" media="screen" rel="stylesheet" href="css/responsive-tables.css" />
    <script src="js/vendor/modernizr.js"></script>
    <style media="screen">
        .dt-buttons{
            margin-left: 2em;
            margin-top: 1.3em;
        }
    </style>
</head>
<body>

    <div id="main">

        <!-- Header -->
        <div class="row" style="margin-bottom:20px;">
            <div class="large-12 columns center">
                <img src="img/single-color-logo.png" alt="logo" />
                <img src="img/ext.png" alt="UGA extension logo" />
                <br />
                <br />
                <h1 class="custom-font-small font -blue">Official County Report</h1>
                <hr style="margin-top:-5px; margin-bottom:5px;" />
                <h2 class="custom-font-small font -black"><?php echo $county; ?></h2>

                <!-- <a href="#" class="button tiny">Printer Friendly Version</a> -->
                <!-- Date Range -->

                <!-- Date Range -->
                <?php
                if (isset($_GET['datepicker'])) {
                    echo '<h4>Cumulative Data for Current Date Range: ' . $month1. '-' .$day1. '-' . $year1. ' to ' . $month2. '-' . $day2. '-' . $year2. '</h4>';
                }
                ?>

                <form>
                    <!-- MOBILE Date Picker -->
                    <div class="row show-for-small-only">
                        <div class="small-12 columns">
                            <p class="mb font -secondary -bold">
                                Select Start Date
                            </p>
                            <input type="date" id="datepickerMobile" name="datepickerMobile" <?php if((filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING) != null) && (!empty($month1)) && (!empty($day1)) && (!empty($year1))) { echo (' value="' . $month1. '/' .$day1. '/' . $year1. '"'); } ?>>
                            <p class="mb font -secondary -bold">
                                Select End Date
                            </p>
                            <input type="date" id="datepickerEndMobile" name="datepickerEndMobile" <?php if((filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING) != null) && (!empty($month2)) && (!empty($day2)) && (!empty($year2))) { echo (' value="' . $month2. '/' .$day2. '/' . $year2. '"'); } else { echo ('value="'.date('m/d/Y').'"'); } ?>>
                        </div>
                    </div>
                    <!-- End MOBILE Date Picker -->
                    <!-- Medium Up Date Picker -->
                    <div class="row collapse show-for-medium-up pt1">
                        <div class="medium-2 medium-offset-2 columns">
                            <a href="#" id="startDate" class="button postfix font -primary">Select Start Date</a>
                        </div>
                        <div class="medium-2 columns">
                            <input type="text" id="datepicker" name="datepicker" class="font -standard -primary -bold" <?php if((filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING) != null) && (!empty($month1)) && (!empty($day1)) && (!empty($year1))) { echo (' value="' . $month1. '/' .$day1. '/' . $year1. '"'); } ?>>
                        </div>
                        <div class="medium-2 columns">
                            <a href="#" id="endDate" class="button postfix font -primary">Select End Date</a>
                        </div>
                        <div class="medium-2 columns end">
                            <input type="text" id="datepickerEnd" name="datepickerEnd" class="font -primary -standard -bold" <?php if((filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING) != null) && (!empty($month2)) && (!empty($day2)) && (!empty($year2))) { echo (' value="' . $month2. '/' .$day2. '/' . $year2. '"'); } else { echo ('value="'.date('m/d/Y').'"'); } ?>>
                        </div>
                        <!-- End Medium Up Date Picker -->
                    </div>
                    <input type="hidden" id="county" name="county" value="<?= filter_input(INPUT_GET, 'county', FILTER_SANITIZE_STRING); ?>">
                    <div class="tc pb1">
                        <a href="<?php echo 'http://' . filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/reporting_county.php?county=<?= filter_input(INPUT_GET, 'county', FILTER_SANITIZE_STRING); ?>">RESET </a>

                        <!-- End Date Range -->
                    </div>
                    <div class="tc">
                        <input type="submit" name="submit" class="button success font -primary" value ="Generate Report!">
                        <!-- End Date Range -->
                    </div>
                </form>
                <!-- End Date Range -->
            </div>
        </div>
        <!-- End Header -->

        <!-- Overall Stats -->
        <div class="row">
            <div class="large-12 columns pb2">
                <h2 class="global-h2">Overall Stats:<?php if($day1!==''){ echo $month1. '-' .$day1. '-' . $year1. ' to ' . $month2. '-' . $day2. '-' . $year2;} ?></h2>
                <hr style="margin-top:-5px; margin-bottom:5px;" />
                <div class="row pt2">
                    <div class="medium-4 columns tc-ns">
                        <div class="font -secondary -bold -medium pb ">
                            <?php echo $points; ?>
                        </div>
                        <b>Total Points Earned</b>
                    </div>
                    <div class="medium-4 columns tc-ns pt2-s">
                        <div class="font -secondary -bold -medium pb ">
                            <?php echo floor($time / 3600) . ' Hours ' . floor(($time % 3600) / 60) . ' Minutes'; ?>
                        </div>
                        <b>Total Time Exercised </b>
                    </div>
                    <div class="medium-4 columns tc-ns pt2-s">
                        <div class="font -secondary -bold -medium pb ">
                            <?php echo $distance; ?>
                        </div>
                        <b>Total Miles From Distance Exercises <span data-tooltip aria-haspopup="true" class="has-tip" title="The total miles of all user's logged distance-based activites, such as running, biking, hiking, etc.">(?)</span></b>
                    </div>
                    <!-- <li><b>Virtual "Miles Walked" <span data-tooltip aria-haspopup="true" class="has-tip" title="The previous version of Walk Georgia converted all exercise (including things like yoga, which does not involve distance) into steps for the sake of comparison. We include this stat for members who still find this useful.">(?)</span>:</b>
                    <?php echo number_format(((($points * 100) - 300) / 3.3) / 3660, 2, '.', ''); ?> -->

                </div>
            </div>
        </div>
        <!-- End Overall Stats -->
        <!-- County Info -->
        <div class="row">
            <div class="large-12 columns pb2">
                <h2 class="global-h2">County Information: <?php if($day1!==''){ echo $month1. '-' .$day1. '-' . $year1. ' to ' . $month2. '-' . $day2. '-' . $year2;} ?></h2>
                <hr style="margin-top:-5px; margin-bottom:5px;" />
                <div class="row pt2">
                    <div class="medium-4 columns tc-ns">
                        <div class="font -secondary -bold -medium pb ">
                            <?php echo odbc_result($total_users, 'COUNT'); ?>
                        </div>
                        <b>Total Number of Users</b>
                    </div>
                    <div class="medium-4 columns tc-ns pt2-s">
                        <div class="font -secondary -bold -medium pb ">
                            <?php echo odbc_num_rows($user); ?>
                        </div>
                        <b>Total Number of Active Users <span data-tooltip aria-haspopup="true" class="has-tip" title="Any user who has actually logged activity.">(?)</span></b>
                    </div>
                    <div class="medium-4 columns tc-ns pt2-s">
                        <div class="font -secondary -bold -medium pb ">
                            <?php echo odbc_num_rows($subbies); ?>
                        </div>
                        <b>Number of Groups <span data-tooltip aria-haspopup="true" class="has-tip" title="Lists the different counties of the members.">(?)</span></b>
                    </div>
                </div>
            </div>
        </div>
        <!-- End County Info -->
        <div class="row">

            <!-- County Members -->
            <div class="large-12 columns">
                <h2 class="global-h2">Active County Members: (<?php echo odbc_num_rows($user); ?>)</h2>
                <hr style="margin-top:-5px; margin-bottom:5px;" />
                <!-- End Member Filtering -->
                <ol class="global-p" style="line-height:2;">
                    <table id="" class="responsive display">
                        <thead>
                            <th>
                                First Name
                            </th>
                            <th>
                                Last Name
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Points
                            </th>
                            <th>
                                Time(Hours)
                            </th>
                            <th>
                                Distance(Miles)
                            </th>
                        </thead>
                        <?php
                        while (odbc_fetch_row($user)) {
                            $breakdown = ACTIVITY::get_breakdown(odbc_result($user, 'L_ID'));

                            ?>
                            <tr>
                                <td><a href="<?php echo 'http://' . filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/reporting_individual.php?id=<?php echo odbc_result($user, 'L_ID'); ?>"><?php echo odbc_result($user, 'L_FNAME'); ?></a></td>
                                <td><?php echo odbc_result($user, 'L_LNAME'); ?></td>
                                <td><?php echo odbc_result($user, 'L_E'); ?></td>
                                <td><?php echo odbc_result($user, 'AL_PA'); ?> </td>
                                <td><?php echo number_format((odbc_result($user, 'AL_TIME') / 3600), 2, '.', ''); ?></td>
                                <td><?php
                                if(!is_string($breakdown['TOTALS']['DISTANCE'])){
                                    echo array_sum($breakdown['TOTALS']['DISTANCE']);
                                }else{
                                    echo "0";

                                } ?></td>
                            </tr>

                            <?php
                        }
                        ?>
                        <?php
                        while (odbc_fetch_row($non_active)) {
                            ?>
                            <tr>
                                <td><a href="<?php echo 'http://' . filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/reporting_individual.php?id=<?php echo odbc_result($non_active, 'L_ID'); ?>"><?php echo odbc_result($non_active, 'L_FNAME'); ?></a></td>
                                <td><?php echo odbc_result($non_active, 'L_LNAME'); ?></td>
                                <td><?php echo odbc_result($non_active, 'L_E'); ?></td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>

                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </ol>
            </div>
            <!-- End County Members -->
            <!-- Groups -->
            <div class="large-12 columns">
                <h2 class="global-h2">Active Groups: <?php echo odbc_num_rows($subs); ?></h2>
                <hr style="margin-top:-5px; margin-bottom:5px;" />
                <ol class="global-p" style="line-height:2;">
                    <table id="" class="responsive display">
                        <thead>
                            <th>
                                Group Name
                            </th>
                            <th>
                                Members
                            </th>
                            <th>
                                Points
                            </th>
                            <th>
                                Time(Hours)
                            </th>
                            <th>
                                Distance(Miles)<span data-tooltip aria-haspopup="true" class="has-tip" title="The total miles of all user's logged distance-based activites, such as running, biking, hiking, etc.">(?)</span>
                            </th>
                        </thead>
                        <?php
                        $j = 0;
                        while (odbc_fetch_array($subs)) {
                            $gdown = GROUP::getBreakdown(odbc_result($subs, 'G_ID'));
                            $count_subby_query = 'SELECT COUNT(DISTINCT U_ID) AS COUNT FROM GROUP_MEMBER WHERE G_ID =  \'' . odbc_result($subs, 'G_ID') . '\'';
                            $count_subby = MSSQL::query($count_subby_query);
                            $count_subby = odbc_result($count_subby, 'COUNT');
                            if (isset($gdown['POINTS']) && !is_string($gdown['POINTS'])) {

                            } else {
                                $gdown['POINTS'][] = 0;
                            }

                            if (isset($gdown['DISTANCE']) && !is_string($gdown['DISTANCE'])) {

                            } else {
                                $gdown['DISTANCE'][] = 0;
                            }
                            if (isset($gdown['TIME']) && !is_string($gdown['TIME'])) {

                            } else {
                                $gdown['TIME'][] = 0;
                            }
                            ?>
                            <tr>
                                <td><a href="<?php echo 'http://' . filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/reporting.php?group=<?php echo odbc_result($subs, 'G_ID'); ?>"><?php echo odbc_result($subs, 'G_NAME'); ?></a></td>
                                <td><?php echo $count_subby; ?> </td>
                                <td><?php echo array_sum($gdown['POINTS']); ?></td>
                                <td><?php echo number_format(array_sum($gdown['TIME']) / 3600, 2, '.', ''); ?></td>
                                <td><?php echo array_sum($gdown['DISTANCE']); ?></td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <!-- End Subgroups -->

                </div>
                <?php /*
                <!-- Activity Breakdown -->
                <div class="row">
                <div class="large-12 columns">
                <h2 class="global-h2">Activity Breakdown:</h2>
                <hr style="margin-top:-5px; margin-bottom:5px;" />
                <ol class="global-p" style="line-height:2;">
                <table id="" class="responsive display">
                <thead>
                <th>
                Activity
                </th>
                <th>
                No. of Users
                </th>
                <th>
                Points
                </th>
                <th>
                Time(Hours)
                </th>
                <th>
                Amount Logged
                </th>
                <th>
                Distance (Miles)
                </th>
                </thead>
                <?php
                for ($i = 1; $i <= $no_of_activities; $i++) {
                odbc_fetch_row($activity, $i);
                $count_query = 'SELECT COUNT(DISTINCT AL_UID) AS COUNT FROM LOG WHERE AL_AID = \'' . odbc_result($activity, 'AL_AID') . '\' AND AL_UID IN (SELECT U_ID FROM GROUP_MEMBER WHERE G_ID IN (SELECT G_ID FROM GROUPS WHERE G_COUNTY = \'' . $county . '\'))';
                $j = MSSQL::query($count_query);
                $activity_count = odbc_result($j, 'COUNT');
                ?>
                <tr>
                <td><?php echo ACTIVITY::activity_to_form(odbc_result($activity, 'AL_AID'), 2, '.', ''); ?></td>
                <td><?php echo $activity_count; ?></td>
                <td><?php echo odbc_result($activity, 'AL_PA'); ?></td>
                <td><?php echo number_format(odbc_result($activity, 'AL_TIME') / 3600); ?></td>
                <td><?php echo odbc_result($activity, 'COUNT'); ?></td>
                <td>
                <?php
                if (ACTIVITY::is_distance_based(odbc_result($activity, 'AL_AID'))) {
                echo number_format(odbc_result($activity, 'AL_UNIT'));
            } else {
            echo number_format((((odbc_result($activity, 'AL_PA') * 100) - 300) / 3.3) / 3660, 2, '.', '');
        }
        ?></td>
        </tr>
        <?php
    }
    ?>
    </table>
    </div>
    <!-- End Activity Breakdown -->
    *
    */
    ?>
    <!-- Sessions -->
    <!-- <div class="large-6 columns" style="float:left;">
    <h2 class="global-h2">Session Dates:</h2>
    <hr style="margin-top:-5px; margin-bottom:5px;" />
    <p class="global-p">
    No sessions were attached to this report.
</p>
<label>
You can also narrow the report to a specific date range by using the bottom at the top right of this report.
</label>
</div> -->
<!-- End Session -->

</div>
<!-- END ACTIVITY BREAKDOWN -->
<div class="row">
    <div class="large-12 columns">
        <hr />
        <a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/group/groups.php?group=<?php echo $_GET['group']; ?>" class="button tiny">Back to Group Page</a>
    </div>
</div>

</div>
<!-- End Main Content -->
<script src="https://code.jquery.com/jquery-1.12.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="js/foundation.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.1/b-flash-1.2.1/b-html5-1.2.1/b-print-1.2.1/r-2.1.0/datatables.min.js"></script>


<script type="text/javascript" src="js/responsive-tables.js"></script>
<script>
$(document).foundation();

//DATEPICKER
$(function () {
  $("#datepicker").datepicker({maxDate: new Date("<?php echo(date('m-d-Y')); ?>")});
    $("#startDate").click(function () {
        $("#datepicker").datepicker("show");
    });
    $("#datepickerEnd").datepicker({maxDate: new Date("<?php echo(date('m-d-Y')); ?>")});
    $("#endDate").click(function () {
        $("#datepickerEnd").datepicker("show");
    });
});

$(document).ready(function () {
    $('table.display').DataTable({
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        responsive: true,
        // stateSave: true,
        // "dom": 'T<"clear">lfrtip',
        dom: 'lBfrtip',
        buttons: [
            'copy', 'excel', 'pdf'
        ]
    });
});
</script>
</body>
</html>
