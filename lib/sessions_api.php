<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/back_api.php");
/**
*	
*	WALK GA WEB SESSIONS MANAGER
*
*	MANAGES LOGIN, LOGOUT, REGISTER, VALIDATION, AND ACCOUNT ACCESS
*
*	@author 	Aaron McCoy <herbo4@uga.edu>
*	@copyright 	Copyright (c) 2014, University of Georgia
*	@package 	WalkGA
*	@category 	Back End
*	@version	1.0
*
*/

class SESSIONS{
	
	/**
	*	Creates a valid session
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function create($PARENT_ID, $DESC, $NAME, $VISIBILITY, $STARTDATE, $ENDDATE){
		$DESC = str_replace('\'', '\'\'', $DESC);
		$NAME = str_replace('\'', '\'\'', $NAME);
		$sql = "INSERT INTO SESSION (S_PID, S_DESC, S_NAME, S_VIS, S_STARTDATE, S_ENDDATE) OUTPUT INSERTED.S_ID VALUES ('$PARENT_ID', '$DESC', '$NAME', '$VISIBILITY', CAST('$STARTDATE' AS DATE), CAST('$ENDDATE' AS DATE))";
		$query = MSSQL::query($sql);
		$SESSION_ID = odbc_result($query, 1);
		$join_sessions = SESSIONS::join_session($SESSION_ID);
		
	}
	
	/**
	*	Gets the info for a session respective to Session ID
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function get_info($S_ID){
		$execute = MSSQL::query("SELECT * FROM SESSION WHERE S_ID='$S_ID'");
		$execute1 = MSSQL::query("SELECT U_ID FROM SESSION_MEMBER WHERE S_ID='$S_ID'");
                $user_id = odbc_result($execute1, 'U_ID');
                $execute2 = MSSQL::query("SELECT L_FLAGS FROM LOGIN WHERE L_ID='$user_id'");
                $flags = explode(':', odbc_result($execute2, 'L_FLAGS'));
                if (odbc_num_rows($execute) == 1) {
		$info['S_PID'] = odbc_result($execute, 'S_PID');
		$info['S_DESC'] = odbc_result($execute, 'S_DESC');
		$info['S_NAME'] = odbc_result($execute, 'S_NAME');
		$info['S_VIS'] = odbc_result($execute, 'S_VIS');
		$info['S_STARTDATE'] = odbc_result($execute, 'S_STARTDATE');
		$info['S_ENDDATE'] = odbc_result($execute, 'S_ENDDATE');
                $info['IS_ADMIN'] = isset($flags[3]);
		}else{
			$info = false;
		}
		return $info;
	}
	
	/**
	*	Edits a valid session
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function edit($S_ID, $NAME, $DESC, $VISIBILITY){
		$sql = "UPDATE SESSION SET S_DESC='$DESC', S_NAME='$NAME', S_VIS='$VISIBILITY' WHERE S_ID='$S_ID'";
		$query = MSSQL::query($sql);
	}
	
	/**
	*	Deletes a session
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function delete($SESSION_ID){
		$query = MSSQL::query("DELETE FROM SESSION WHERE S_ID='$SESSION_ID'; DELETE FROM SESSION_MEMBERS WHERE S_ID='$SESSION_ID'");
	}
        
        	/**
	*	Creates a record that ties a user to a session, but when done by an admin
	*	@author 	Juweek Adolphe <jadolphe@uga.edu>
	*	@copyright 	Copyright (c) 2015, University of Georgia
	*/
	public static function join_session_by_add($SESSION_ID, $U_ID){
		$sql = "INSERT INTO SESSION_MEMBER (S_ID, U_ID) VALUES ('$SESSION_ID', '$U_ID')";
		//enter them into the session
		$query = MSSQL::query($sql);
	}
	
	/**
	*	Creates a record that ties a user to a session
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function join_session($SESSION_ID){
		$ID = $_SESSION['ID'];
		//If they have already joined the session, just redirect them to the session's page
		$joined = SESSIONS::hasJoined($SESSION_ID, $ID);
		if($joined){
			$err = '&err_msg=You have already joined this sessions';
			$go_to_sessions = REDIRECT::sessions($SESSION_ID.$err);
			return false;
		}
		$sql = "INSERT INTO SESSION_MEMBER (S_ID, U_ID) VALUES ('$SESSION_ID', '$ID')";
		//enter them into the session
		$query = MSSQL::query($sql);
		//if they are the first member in the session, they are the admin
		$firstMember = odbc_num_rows(MSSQL::query("SELECT * FROM SESSION_MEMBER WHERE S_ID ='".$SESSION_ID."'"));
		if($firstMember === 1){
			$promote = SESSIONS::promote($SESSION_ID);
		}
		//then redirect them to the session's page
		$go_to_sessions = REDIRECT::sessions($SESSION_ID);
	}
	
	/**
	*	Destroys the record that ties a user to a session
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function leave_session($SESSIONS_ID){
		$ID = $_SESSION['ID'];
		$query = MSSQL::query("DELETE FROM SESSION_MEMBER WHERE U_ID='$ID' AND S_ID='$SESSIONS_ID'");
		$go_to_sessions = REDIRECT::sessions($SESSIONS_ID);
	}
	
	/**
	*	Checks to see if a user has joined a session respective to user and session's ID
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function hasJoined($SESSION_ID, $ID){
		$query = MSSQL::query("SELECT * FROM SESSION_MEMBER WHERE S_ID ='".$SESSION_ID."' AND U_ID='".$ID."'");
		$num = odbc_num_rows($query);
		return $num;
	}
	
	/**
	*	Checks to see if a user is a session admin respective to user and session ID
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function isAdmin($SESSIONS_ID){
		$query = MSSQL::query("SELECT * FROM SESSION_MEMBER WHERE S_ID ='".$SESSIONS_ID."' AND U_ID='".$_SESSION['ID']."'");
		$isAdmin = odbc_result($query, 'IS_ADMIN');
		return $isAdmin;
	}
	
	/**
	*	Checks to see if a user is a session admin respective to user and session ID
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function promote($SESSION_ID){
		$query = MSSQL::query("UPDATE SESSION_MEMBER SET IS_ADMIN='1' WHERE S_ID ='".$SESSION_ID."' AND U_ID='".$_SESSION['ID']."'");
		return true;
	}
	
	/**
	*	Lists the sessions associated with a user's ID
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function listCurrentSessions(){
		$today = date('Y-m-d');
                $initiate='';
		$sql = 'SELECT * FROM SESSION WHERE S_STARTDATE <=\''.$today.'\' AND S_ENDDATE >=\''.$today.'\' AND S_ID IN(SELECT S_ID FROM SESSION_MEMBER WHERE U_ID=\''.$_SESSION['ID'].'\')';
		$current_sessions = MSSQL::query($sql);
		if(odbc_num_rows($current_sessions) >0){
			while(odbc_fetch_row($current_sessions)){
				$datetime1 = date_create(date('Y-m-d'));
				$datetime2 = date_create(odbc_result($current_sessions,'S_ENDDATE'));
				$interval = date_diff($datetime1, $datetime2);
			?>
			<div class="current-session" id="<?php echo odbc_result($current_sessions, 'S_ID'); ?>-toggle">
				<!-- Session Avatar -->
				<div class="my-group-avatar">
				<?php SESSIONS::avatar_small(odbc_result($current_sessions, 'S_ID')); ?>
				</div>
				<!-- End Session Avatar -->
			
				<!-- Session Title -->
	
				<?php echo odbc_result($current_sessions, 'S_NAME'); ?>
	
				<!-- End Session Title -->
				
				<div class="expand-icon">⌄</div>
					<div class="my-session-expand" id="<?php echo odbc_result($current_sessions, 'S_ID'); ?>-breakdown" style="margin-top: 20px; display: none;">
						<div class="row">
							<div class="large-12 columns">
								<hr style="border-color:white;">
									<a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);?>/session/sessions.php?session=<?php echo odbc_result($current_sessions, 'S_ID'); ?>" class="button tiny round peachButton">
										View Session
									</a>
									<a href="session/sessions.php?session=<?php echo odbc_result($current_sessions, 'S_ID'); ?>&leave=1" class="button tiny round alert">
										Leave Session
									</a>
								<hr style="border-color:white; margin-top:0px;">
							</div>
						</div>
						
						<div class="row">
							<div class="large-6 columns">
								<h2 class="custom-font-small-white"><?php echo $interval->format('%a Days Left');?></h2>
							</div>
						</div>
						<?php $initiate .= '$("#'.odbc_result($current_sessions, 'S_ID').'-toggle").click(function(){$("#'.odbc_result($current_sessions, 'S_ID').'-breakdown").toggle();});';?>
					</div>
				</div>
			<?php
			}
		}else{
			?>
				<div class="session-unit">
					<div class="row">
						<div class="large-1 medium-2 hide-for-small columns">
							<img src="img/alert.png" alt="" /> 
						</div>
						<div class="large-11 medium-10 columns">
							<p class="global-p" style="padding:10px; margin-bottom:0px;">You do not currently belong to any sessions.</p>
						</div>
					</div>
				</div>
			<?php
		}
		return $initiate;
	}
	
	/**
	*	Lists the sessions associated with a user's ID
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function listUpcomingSessions(){
		$initiate ='';
		$today = date('Y-m-d') ;
		$sql = 'SELECT * FROM SESSION WHERE S_STARTDATE >\''.$today.'\' AND S_ENDDATE >\''.$today.'\' AND S_ID IN(SELECT S_ID FROM SESSION_MEMBER WHERE U_ID=\''.$_SESSION['ID'].'\')';
		$current_sessions = MSSQL::query($sql);
		if(odbc_num_rows($current_sessions) >0){
			while(odbc_fetch_row($current_sessions)){
				$datetime1 = date_create(date('Y-m-d'));
				$datetime2 = date_create(odbc_result($current_sessions,'S_ENDDATE'));
				$interval = date_diff($datetime1, $datetime2);
			?>
			<div class="secondary-session" id="<?php echo odbc_result($current_sessions, 'S_ID'); ?>-toggle">
				<!-- Session Avatar -->
				<div class="my-group-avatar">
				<?php SESSIONS::avatar_small(odbc_result($current_sessions, 'S_ID')); ?>
                </div>
				<!-- End Session Avatar -->
				<!-- Session Title -->
	
				<?php echo odbc_result($current_sessions, 'S_NAME'); ?>
	
				<!-- End Session Title -->
				
				<div class="expand-icon"><i class="fi-list size-24"></i></div>
					<div class="grey-bg" id="<?php echo odbc_result($current_sessions, 'S_ID'); ?>-breakdown" style="margin-top: 20px; display: none;">
						<div class="row">
							<div class="large-12 columns">
								<hr style="border-color:white;">
									<a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);?>/session/sessions.php?session=<?php echo odbc_result($current_sessions, 'S_ID'); ?>" class="button tiny round peachButton">
										View Session
									</a>
									<a href="session/sessions.php?session=<?php echo odbc_result($current_sessions, 'S_ID'); ?>&leave=1" class="button tiny round alert">
										Leave Session
									</a>
								<hr style="border-color:white; margin-top:0px;">
							</div>
						</div>
						
						<div class="row">
							<div class="large-6 columns">
								<h2 class="custom-font-small-white"><?php echo $interval->format('%a Days Left');?></h2>
							</div>
						</div>
						<?php $initiate .= '$("#'.odbc_result($current_sessions, 'S_ID').'-toggle").click(function(){$("#'.odbc_result($current_sessions, 'S_ID').'-breakdown").toggle();});';?>
					</div>
				</div>
			<?php
			}
		}else{
			?>
				<div class="session-unit">
					<div class="row">
						<div class="large-1 medium-2 hide-for-small columns">
							<img src="img/alert.png" alt="" /> 
						</div>
						<div class="large-11 medium-10 columns">
							<p class="global-p" style="padding:10px; margin-bottom:0px;">You do not currently have any upcoming sessions.</p>
						</div>
					</div>
				</div>
			<?php
		}
		return $initiate;
	}
	

	
	/**
	*	Lists the sessions associated with a user's ID
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function listCompletedSessions(){
		$today = date('Y-m-d');
		$sql = 'SELECT * FROM SESSION WHERE S_STARTDATE <=\''.$today.'\' AND S_ENDDATE <=\''.$today.'\' AND S_ID IN(SELECT S_ID FROM SESSION_MEMBER WHERE U_ID=\''.$_SESSION['ID'].'\')';
		$current_sessions = MSSQL::query($sql);
		if(odbc_num_rows($current_sessions) >0){
			while(odbc_fetch_row($current_sessions)){
				?>
					<li><a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);?>/session/sessions.php?session=<?php echo odbc_result($current_sessions, 'S_ID'); ?>"><?php echo odbc_result($current_sessions, 'S_NAME'); ?></a></li>
				<?php
			}
		}
	}
	
	
	public static function list_users_in_session($S_ID) {
		$query = MSSQL::query("SELECT L_ID, L_FNAME, L_LNAME FROM LOGIN WHERE L_ID IN( SELECT U_ID FROM SESSION_MEMBER WHERE S_ID ='".$S_ID."') ORDER BY L_LNAME ASC");
			?>
			<div class="data-table">
			<table class="display" id="" style="width:100%;">
			  <thead>
				<th class="global-p-white" style="width:60px; padding:5px;">
				  <img src="../../img/default-group-icon.png" alt="default group icon" style="display:block; margin:auto;"/>
				</th>
				<th class="global-h2">
			  	  User Name
				</th>
			  </thead>
			<?php
		while(odbc_fetch_row($query)){  
				$UID = odbc_result($query, 'L_ID');
				$FN = odbc_result($query, 'L_FNAME');
				$LN = odbc_result($query, 'L_LNAME');
				?>
				<tr>
				  <td class="global-p" style="width:60px; padding:5px;">
			  		<a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/'; ?>index.php?uid=<?php echo $UID; ?>">
						<?php $avatar = ACCOUNT::avatar_small($UID);?>
					</a>
				  </td>
				  <td class="global-p">
					<a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/'; ?>index.php?uid=<?php echo $UID; ?>">
					  <br />
					  <p class="global-p"><?php echo $FN.' '.$LN ?></p>
					</a>
				  </td>
				</tr>
	  <?php			 
		}
	  ?>
			</table>
			</div>
			<?php
	}
	
	/**
	*	Gets the total points, time, and distance of a session without breaking the server
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function getBreakdown($SESSION_ID, $START, $END){
		$today = date('Y-m-d');
		$info['POINTS']= '';
		$info['USER_POINTS']= '';
		$info['ACTIVITY']= '';
		$info['TOTALS']['TIME']= '';
		$info['TOTALS']['DISTANCE']= '';
		$info['USER_POINTS'][$SESSION_ID] ='';
		$sql = "SELECT AL_AID, SUM(AL_PA) AS 'AL_PA', SUM(AL_TIME) AS 'AL_TIME', SUM(AL_UNIT) AS 'AL_UNIT' FROM LOG WHERE AL_UID IN (SELECT U_ID FROM SESSION_MEMBER WHERE S_ID = '$SESSION_ID') AND AL_DATE <='$END' AND AL_DATE >='$START' GROUP BY (AL_AID);";
		$execute = MSSQL::query($sql);
		while(odbc_fetch_row($execute)){
		  $aid = odbc_result($execute, 'AL_AID');
		  //
                  if(!isset($info['POINTS'][$aid])){
			  $info['POINTS'][$aid]='';
		  }
		  $info['POINTS'][$aid] +=odbc_result($execute, 'AL_PA');
                  //
		  if(!isset($info['USER_POINTS'][$SESSION_ID][$aid])){
			  $info['USER_POINTS'][$SESSION_ID][$aid]='';
		  }
		  $info['USER_POINTS'][$SESSION_ID][$aid] +=odbc_result($execute, 'AL_PA');
                  //
		  $info['ACTIVITY'][$aid]['ID'] =odbc_result($execute, 'AL_AID');
		  if(!isset($info['ACTIVITY'][$aid]['UNITS'])){
			  $info['ACTIVITY'][$aid]['UNITS']='';
		  }
		  $info['ACTIVITY'][$aid]['UNITS'] += odbc_result($execute, 'AL_UNIT');
		  //
                  if(!isset($info['ACTIVITY'][$aid]['TIME'])){
			  $info['ACTIVITY'][$aid]['TIME']='';
		  }
		  $info['ACTIVITY'][$aid]['TIME'] +=odbc_result($execute, 'AL_TIME');
		  //
                  if(!isset($info['TOTALS']['TIME'][$aid])){ 
			  $info['TOTALS']['TIME'][$aid]='';
		  }
		  $info['TOTALS']['TIME'][$aid] += odbc_result($execute, 'AL_TIME');
                  //
		  $DA = MSSQL::query('SELECT A_UNIT FROM ACTIVITY WHERE A_ID=\''.$aid.'\'');
		  $DA = odbc_result($DA, 'A_UNIT');
		  if ($DA == 1){
			  if(!isset($info['TOTALS']['DISTANCE'][$aid])){
				  $info['TOTALS']['DISTANCE'][$aid]='';
			  }
			  $info['TOTALS']['DISTANCE'][$aid] += odbc_result($execute, 'AL_UNIT');
		  }
		}
		if(isset($info)){
			return $info;
		}
	}
	
	/**
	*	Uploads a sessions's image to the server
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function avatar_upload($S_ID) {
		$errmsg="session=".$S_ID."&img_success=1";
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["session_img"]["name"]);
		$extension = end($temp);
		$_FILES["session_img"]["name"];
		if(($_FILES["session_img"]["size"] > 0) &&($_FILES["session_img"]["size"] < 1048576)){
			if (in_array($extension, $allowedExts)){
				move_uploaded_file($_FILES["session_img"]["tmp_name"],
				"img/avatar/" .$S_ID.'.png');
			}else{
				$errmsg = "sessions=".$S_ID."&err_msg=We don't support that type of image.  You can upload a jpeg, jpg, gif, or png.";
				REDIRECT::current_page($errmsg);
			}
		} else {
			$errmsg = "sessions=".$S_ID."&err_msg=Your image is too large for us.  We can only take images 1MB or less.  Try re-sizing it on your computer or uploading a smaller image.";
			REDIRECT::current_page($errmsg);
		}
	}
	
	
	/**
	*	Displays a session's image to the page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function avatar_large($ID){
		  if (file_exists('session/img/avatar/'.$ID.'.png')) {
			    $filename = '/session/img/avatar/'.$ID.'.png';
		  } else {
    			$filename = '/img/default-session-profile.png';
		  }
		  ?>
            <a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/'; ?>session/sessions.php?session=<?php echo $ID; ?>">
                <div class="avatar-large" style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat;">
                </div>
            </a>
          <?php
		  
		  return $filename;
	}
	
	/**
	*	Displays a session's image to the page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function avatar_medium($ID){
		  if (file_exists('session/img/avatar/'.$ID.'.png')) {
			    $filename = '/session/img/avatar/'.$ID.'.png';
		  } else {
    			$filename = '/img/default-session-profile.png';
		  }
		  ?>
            <a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/'; ?>session/sessions.php?session=<?php echo $ID; ?>">
                <div class="avatar-favorite" style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat;">
                </div>
            </a>
          <?php
		  
		  return $filename;
	}
	
	/**
	*	Displays a session's image to the page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function avatar_small($ID){
		  if (file_exists('session/img/avatar/'.$ID.'.png')) {
			    $filename = '/session/img/avatar/'.$ID.'.png';
		  } else {
    			$filename = '/img/default-session-profile.png';
		  }
		  ?>
            <a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/'; ?>session/sessions.php?session=<?php echo $ID; ?>">
                <div class="avatar-small" style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat; background-color:#FFF;">
                </div>
            </a>
          <?php
		  
		  return $filename;
	}
}

?>