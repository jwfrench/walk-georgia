<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/back_api.php");
/**
 *
 * 	WALK GA WEB SITE TEMPLATE API
 *
 * 	@author 	Aaron McCoy <herbo4@uga.edu>
 * 	@author 	John French <jwfrench@uga.edu>
 * 	@copyright 	Copyright (c) 2014, University of Georgia
 * 	@package 	WalkGA
 * 	@category 	Back End
 * 	@version	1.1.0
 *
 */

/**
 *
 * 	WALK GA HTML ELEMENTS
 *
 * 	@author 	Aaron McCoy <herbo4@uga.edu>
 * 	@author 	John French <jwfrench@uga.edu>
 * 	@copyright 	Copyright (c) 2014, University of Georgia
 * 	@package 	WalkGA
 * 	@category 	Front End
 * 	@version	1.0
 *
 */
class HTML_ELEMENT {

    /**
     * 	Generates the <head> html code with respect to login status
     * 	@author 	Aaron McCoy <herbo4@uga.edu>
     * 	@author 	John French <jwfrench@uga.edu>
     * 	@copyright 	Copyright (c) 2014, University of Georgia
     */
    public static function head($title) {

        $HTTP_HOST = filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);
        header("Content-Type: text/html; charset=utf-8");
        ?>
        <!doctype html>
        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <meta property="fb:app_id"          content="276401669208985" />
            <meta property="og:type"            content="website" />
            <?php if (filter_input(INPUT_GET, 'points', FILTER_SANITIZE_STRING) != null) { ?>
                <meta property="og:url"             content="http://www.walkgeorgia.org?name=<?php echo filter_input(INPUT_GET, 'name', FILTER_SANITIZE_STRING) ?>&points=<?php filter_input(INPUT_GET, 'points', FILTER_SANITIZE_STRING) ?>" />
                <meta property="og:title"           content="<?php echo filter_input(INPUT_GET, 'name', FILTER_SANITIZE_STRING) ?> earned <?php echo filter_input(INPUT_GET, 'points', FILTER_SANITIZE_STRING) ?> points on Walk Georgia!" />
            <?php } else { ?>
                <meta property="og:url"             content="http://www.walkgeorgia.org" />
                <meta property="og:title"           content="Walk Georgia - 'Move More, Live More!!" />
            <?php } ?>
            <meta property="og:site_name" 		content="Walk Georgia"/>
            <meta property="og:image"           content="http://<?php echo $HTTP_HOST; ?>/horizontial-logo.png" />

            <meta property="og:description"    	content="Walk Georgia: The place for
                  Georgians to track their fitness progress & receive local, research-based
                  knowledge & resources on wellness." />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <title>Walk Georgia | <?php echo $title; ?></title>
            <link rel="stylesheet" type="text/css" href="http://<?php echo $HTTP_HOST; ?>/css/foundation.css"/>
            <link rel="stylesheet" type="text/css" href="http://<?php echo $HTTP_HOST; ?>/css/dataTables.foundation.css">
            <link rel="stylesheet" type="text/css" href="http://<?php echo $HTTP_HOST; ?>/css/dataTables.tableTools.css">
            <!-- valid session -->
            <script async >
                (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                ga('create', 'UA-18679377-1', 'walkgeorgia.org');
                ga('send', 'pageview');
            </script>
            <script src="http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/js/vendor/modernizr.js"></script>
            <!--[if lte IE 11]>
                    <meta http-equiv="refresh" content="0;url=http://www.walkgeorgia.org/browser-update.html" />
             <script src="js/html5shiv.min.js"></script>
            <![endif]-->
        </head>
        <body>

                    <div id="fb-root"></div>
                    <script>
                        (function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id))
                                return;
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=276401669208985";
                            fjs.parentNode.insertBefore(js, fjs);
                        }
                        (document, 'script', 'facebook-jssdk'));
                    </script>
                    <noscript>
                    <div class="row" style="margin-top:10%;; min-height:400px;">
                        <div align="center">
                            <h1 class="custom-font-small">enable javascript</h1>
                        </div>
                        <div class="medium-6 medium-centered large-centered large-6 columns">
                            For full functionality of this site it is necessary to enable JavaScript.
                            Here are the <a href="http://www.enable-javascript.com/" target="_blank">
                                <style>div.nojava { display:none; }</style>
                        </div>
                    </div>
                    </noscript>
                    <?php
                }

                /**
                 * 	Generates the top bar html code with respect to login status
                 * 	@author 	Aaron McCoy <herbo4@uga.edu>
                 * 	@author 	John French <jwfrench@uga.edu>
                 * 	@copyright 	Copyright (c) 2014, University of Georgia
                 */
                public static function top_nav() {

                    //read in cookie (Account name), generate random number, store in cookie/database
                    if ( (isset($_COOKIE['cookie'])) && (isset($_COOKIE['p']))) {
                        //if cookie is set then auto-login
                        /*
                         * problems: odbc_exec() returning error
                         */
                        $account = $_COOKIE['cookie'];
                        $query = MSSQL::query('SELECT * FROM LOGIN WHERE L_UUID = \''.$account.'\'');
                        $email = odbc_result($query, 'L_UUID');
                        $pw = $_COOKIE['p'];
                        $login = SESSION::login($email, $pw, '');
                    }
                    //If the user is logged in
                    $HTTP_HOST = filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);
                    if (isset($_SESSION['valid'])) {
                        ?>

                        <!-- Top Bar Logged In -->

                        <div class="fixed">
                            <nav class="top-bar" data-topbar>
                                <ul class="title-area">
                                    <li class="name">
                                        <h1 class="gotham-big"><a href="http://<?php echo $HTTP_HOST; ?>/index.php">walk georgia</a></h1>
                                    </li>
                                    <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
                                </ul>

                                <section class="top-bar-section">
                                    <!-- Right Nav Section -->
                                    <ul class="right" style="">
                                        <li><a href="http://<?php echo $HTTP_HOST; ?>/about-us.php">About Us</a></li>
                                        <li class="has-dropdown"><a href="#">Help Center</a>
                                            <ul class="dropdown">
                                                <li><a href="http://<?php echo $HTTP_HOST; ?>/faq.php">FAQ</a></li>
                                                <li><a href="http://<?php echo $HTTP_HOST; ?>/implementation.php">Implementation Guides</a></li>
                                            </ul>
                                        </li>
                                        <li class="has-dropdown"><a href="http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/resources/index.php">Resources</a>
                                            <ul class="dropdown">
                                                <li><a href="http://<?php echo $HTTP_HOST; ?>/map/map.php">My Map</a></li>
                                                <li><a href="http://blog.extension.uga.edu/walkgeorgia" target="_blank">Blog</a></li>
                                                <li><a href="https://blog.extension.uga.edu/walkgeorgia/category/recipes/">Recipes</a></li>
                                                <li><a href="http://<?php echo $HTTP_HOST; ?>/resources/fitness/index.php">Fitness Tips</a></li>
                                                <li><a href="http://<?php echo $HTTP_HOST; ?>/resources/county-resources.php">County Resources</a></li>
                                                <li><a href="http://<?php echo $HTTP_HOST; ?>/lesson-plans.php">Lesson Plans</a></li>
                                                <li><a href="http://<?php echo $HTTP_HOST; ?>/events/index.php">Events</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="http://<?php echo $HTTP_HOST; ?>/contact-us.php">Contact Us</a></li>
                                        <li class="active has-dropdown"><a href="http://<?php echo $HTTP_HOST; ?>/index.php">My Account</a>
                                            <ul class="dropdown">
                                                <li><a href="http://<?php echo $HTTP_HOST; ?>/user-account.php">Preferences</a></li>
                                                <li><a href="http://<?php echo $HTTP_HOST; ?>/?logout=1">Log Out</a></li>
                                            </ul>

                                    </ul>
                                </section>
                            </nav>
                            <?php
                            HTML_ELEMENT::sub_nav();
                            ?>
                        </div>

                        <!-- End Top Bar Logged In -->

                        <?php
                    }
                    //If user is not logged in
                    else {
                        ?>

                        <!-- Top Bar Not Logged In -->

                        <div class="fixed">
                            <nav class="top-bar" data-topbar>
                                <ul class="title-area">
                                    <li class="name">
                                        <h1 class="gotham-big"><a href="http://<?php echo $HTTP_HOST; ?>/index.php">walk georgia</a></h1>
                                    </li>
                                    <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
                                </ul>

                                <section class="top-bar-section">
                                    <!-- Right Nav Section -->
                                    <ul class="right">
                                        <li><a href="http://<?php echo $HTTP_HOST; ?>/about-us.php">About Us</a></li>
                                        <li class="has-dropdown"><a href="#">Help Center</a>
                                            <ul class="dropdown">
                                                <li><a href="http://<?php echo $HTTP_HOST; ?>/faq.php">FAQ</a></li>
                                                <li><a href="http://<?php echo $HTTP_HOST; ?>/implementation.php">Implementation Guides</a></li>
                                            </ul>
                                        </li>
                                        <li class="has-dropdown"><a href="http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/resources/index.php">Resources</a>
                                            <ul class="dropdown">
                                                <li><a href="http://blog.extension.uga.edu/walkgeorgia" target="_blank">Blog</a></li>
                                                <li><a href="https://blog.extension.uga.edu/walkgeorgia/category/recipes/">Recipes</a></li>
                                                <li><a href="http://<?php echo $HTTP_HOST; ?>/resources/fitness/index.php">Fitness Tips</a></li>
                                                <li><a href="http://<?php echo $HTTP_HOST; ?>/resources/county-resources.php">County Resources</a></li>
                                                <li><a href="http://<?php echo $HTTP_HOST; ?>/lesson-plans.php">Lesson Plans</a></li>
                                                <li><a href="http://<?php echo $HTTP_HOST; ?>/events/index.php">Events</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="http://<?php echo $HTTP_HOST; ?>/contact-us.php">Contact Us</a></li>
                                        <li class="active"><a href="http://<?php echo $HTTP_HOST; ?>/login.php">Login</a></li>
                                    </ul>
                                </section>
                            </nav>
                        </div>

                        <!-- End Top Bar Not Logged In -->

                        <?php
                    }
                }

                /**
                 * 	Generates the sub nav html code
                 * 	@author 	Aaron McCoy <herbo4@uga.edu>
                 * 	@author 	John French <jwfrench@uga.edu>
                 * 	@copyright 	Copyright (c) 2014, University of Georgia
                 */
                public static function sub_nav() {
                    $HTTP_HOST = filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);
                    ?>

                            <!--[if lte IE 9]> <style>.left-off-canvas-menu { display:none; }</style>  <![endif]-->


                    <section class="main-section">

                        <!-- Desktop Sub Nav -->

                        <div class="">
                            <nav class="sub-bar hide-for-small">
                                <div class="row">
                                    <div class="large-12 columns">
                                        <dl class="sub-nav">
                                            <?php HTML_ELEMENT::sub_nav_elements(); ?>
                                        </dl>
                                    </div>
                                </div>
                            </nav>
                        </div>

                        <!-- End Desktop Sub Nav -->

                        <!-- Mobile Sub Nav -->

                        <div class="show-for-small-only">
                            <nav class="mobile-sub-bar">
                                <div class="row">
                                    <div class="large-12 columns">
                                        <div style="float:left;">
                                            <a href="#" class="mobile-toggle"><h2 class="global-h2-gray"> &#x25BC; My Tools</h2></a>
                                        </div>
                                    </div>
                                </div>
                            </nav>


                            <!-- Mobile Nav Accordion Content -->
                            <div class="mobile-top-nav" style="margin-bottom:0px;">
                                <?php /* $avatar = ACCOUNT::avatar_small($_SESSION['ID']); */ ?>
                                <br />
                                <ul class="small-block-grid-5 center">
                                    <li>
                                        <a href="http://<?php echo $HTTP_HOST; ?>/my-progress.php">
                                            <i class="fi-torso size-36"></i>
                                            <label style="margin-top:-15px; color:#008CBA;"><b>My Goals</b></label>
                                        </a>
                                    </li>
                                    <?php /*
                                      <li>
                                      <a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/goals.php">
                                      <i class="fi-graph-bar size-36"></i>
                                      <label style="margin-top:-15px; color:#008CBA;"><b>Goals</b></label>
                                      </a>
                                      </li>
                                     */ ?>
                                    <li>
                                        <a href="http://<?php echo $HTTP_HOST; ?>/map/map.php">
                                            <i class="fi-map size-36"></i>
                                            <label style="margin-top:-15px; color:#008CBA;"><b>My Map</b></label>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://<?php echo $HTTP_HOST; ?>/groups.php">
                                            <i class="fi-torsos-all size-36"></i>
                                            <label style="margin-top:-15px; color:#008CBA;"><b>My Groups</b></label>
                                        </a>
                                    </li>
                                    <!-- http://<?php // echo $HTTP_HOST; ?><!--/sessions.php -->
                                    <!--
                                    <li>
                                        <a href="">
                                            <i class="fi-calendar size-36"></i>
                                            <label style="margin-top:-15px; color:#008CBA;"><b>My Sessions</b></label>
                                        </a>
                                    </li>-->

                                    <li>
                                        <a href="#" data-reveal-id="search-modal-landing">
                                            <i class="fi-magnifying-glass size-36"></i>
                                            <label style="margin-top:-15px; color:#008CBA;"><b>Search</b></label>
                                        </a>
                                    </li>

                                </ul>
                                <?php
                                //$form = ACTIVITY::activity_form(); USE THIS WHEN ACTIVITY LOGGING WORKS GLOBALLY
                                //<a href="#" class="button tiny expand success" style="margin-bottom:0px;" data-reveal-id="myModal">Log an Activity</a>
                                ?>

                            </div>

                        </div>
                        <!-- End Mobile Sub Nav -->

                        <!-- Seach Modal -->

                        <div id="search-modal-landing" class="reveal-modal" data-reveal>

                            <div class="center"><h2 class="custom-font-small-blue">What are you looking for?</h2></div>

                            <div class="row">

                                <!-- User Landing -->
                                <div class="large-4 medium-4 columns center">
                                    <div class="white-bg">
                                        <i class="fi-torso size-60 blue"></i>
                                        <h2 class="custom-font-small-blue">Users</h2>
                                        <hr style="margin-top:-5px; margin-bottom:10px;">
                                        <a href="#" class="button tiny expand" data-reveal-id="user-search">Search for a User</a>
                                        <p class="global-p-small justify">Find out if your friends, family, or co-workers are on Walk Georgia!</p>
                                    </div>
                                </div>
                                <!-- End User Landing -->

                                <!-- Group Landing -->
                                <div class="large-4 medium-4 columns center">
                                    <div class="white-bg">
                                        <i class="fi-torsos-all size-60 blue"></i>
                                        <h2 class="custom-font-small-blue">Groups</h2>
                                        <hr style="margin-top:-5px; margin-bottom:10px;">
                                        <a href="#" class="button tiny expand" data-reveal-id="group-search">Search for a Group</a>
                                        <p class="global-p-small justify">If you are trying to join or find a group, type in the name here to find it.</p>
                                    </div>
                                </div>
                                <!-- End Group Landing -->

                                <!--Session Landing -->
                                <div class="large-4 medium-4 columns center">
                                    <div class="white-bg">
                                        <i class="fi-clock size-60 blue"></i>
                                        <h2 class="custom-font-small-blue">Sessions</h2>
                                        <hr style="margin-top:-5px; margin-bottom:10px;">
                                        <a href="#" class="button tiny expand" data-reveal-id="session-search">Search for a Session</a>
                                        <p class="global-p-small justify">Looking for a session to join? Find it fast by searching here.</p>
                                    </div>
                                </div>
                                <!-- End Session Landing -->

                            </div>
                            <a class="close-reveal-modal">&#215;</a>
                        </div>

                        <!-- Search for Users Modal -->
                        <div id="user-search" class="reveal-modal" data-reveal>
                            <div class="row center">
                                <div class="large-12 columns">
                                    <i class="fi-torso size-60 blue"></i>
                                    <h2 class="custom-font-small-blue">Search for Users:</h2>
                                    <p class="global-p">Please note: users, groups, and sessions set to “private" are not included in this search. <a href="../faq.php#F9" target="_blank">?</a></p>
                                    <form id="search_user" name="search_user" action="http://<?php echo $HTTP_HOST; ?>/search_user.php">
                                        <div class="row collapse">
                                            <div class="small-10 columns">
                                                <input id='search' name='search' type="text" placeholder="Enter a Name">
                                            </div>
                                            <div class="small-2 columns">
                                                <input type='submit' value='Go' class="button postfix">
                                            </div>
                                        </div>
                                        <a href="#" style="float:left;" data-reveal-id="search-modal-landing" class="button tiny secondary">Back to Search Options</a>
                                    </form>
                                </div>
                            </div>
                            <a class="close-reveal-modal">&#215;</a>
                        </div>
                        <!-- End Search for Users Modal -->

                        <!-- Search for Groups Modal -->
                        <div id="group-search" class="reveal-modal" data-reveal>
                            <div class="row center">
                                <div class="large-12 columns">
                                    <i class="fi-torsos-all size-60 blue"></i>
                                    <h2 class="custom-font-small-blue">Search for Groups:</h2>
                                    <p class="global-p">Please note: users, groups, and sessions set to “private" are not included in this search. <a href="../faq.php#F9" target="_blank">?</a></p>
                                    <form id="search_group" name="search_group" action="http://<?php echo $HTTP_HOST; ?>/search_group.php">
                                        <div class="row collapse">
                                            <div class="small-10 columns">
                                                <input id='search' name='search' type="text" placeholder="Enter a Group Name">
                                            </div>
                                            <div class="small-2 columns">
                                                <input type='submit' value='Go' class="button postfix">
                                            </div>
                                        </div>
                                        <a href="#" style="float:left;" data-reveal-id="search-modal-landing" class="button tiny secondary">Back to Search Options</a>
                                    </form>
                                </div>
                            </div>
                            <a class="close-reveal-modal">&#215;</a>
                        </div>
                        <!-- End Search for Groups Modal -->

                        <!-- Search for Sessions Modal -->
                        <div id="session-search" class="reveal-modal" data-reveal>
                            <div class="row center">
                                <div class="large-12 columns">
                                    <i class="fi-clock size-60 blue"></i>
                                    <h2 class="custom-font-small-blue">Search for Sessions:</h2>
                                    <p class="global-p">Please note: users, groups, and sessions set to “private" are not included in this search. <a href="../faq.php#F9" target="_blank">?</a></p>
                                    <form id="search_session" name="search_session" action="http://<?php echo $HTTP_HOST; ?>/search_session.php">
                                        <div class="row collapse">
                                            <div class="small-10 columns">
                                                <input id='search' name='search' type="text" placeholder="Enter a Session Name">
                                            </div>
                                            <div class="small-2 columns">
                                                <input type='submit' value='Go' class="button postfix">
                                            </div>
                                        </div>
                                        <a href="#" style="float:left;" data-reveal-id="search-modal-landing" class="button tiny secondary">Back to Search Options</a>
                                    </form>
                                </div>
                            </div>
                            <a class="close-reveal-modal">&#215;</a>
                        </div>
                        <!-- End Search for Sessions Modal -->


                        <!-- End Search Modal -->

                        <?php
                    }

                    public static function sub_nav_elements() {
                        $a = "index.php";
                        $b = "goals.php";
                        $c = "groups.php";
                        $d = "sessions.php";
                        $e = basename(filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_STRING));
                        $f = "display_activity.php";
                        $g = "search.php";
                        $h = "map.php";
                        $i = "my-progress.php";
                        ?>
                        <?php // Only show sessions to the '100 Days of summer Get fit challenge - NEHD'
                        $is_session_shown = false;
                        $session_member_ids = array(7323, 10604, 11445, 11447, 10634, 10694, 10862, 10605, 11045, 10612, 10679, 9971, 7331, 10610, 11479, 11444, 11526, 10628, 10611, 11506, 10607, 9913, 12150, 9809, 8210, 12818, 12796, 1789, 13112);
                        // These users can be found from query 'SELECT * FROM dbo.SESSION_MEMBER INNER JOIN dbo.SESSION ON dbo.SESSION_MEMBER.S_ID = dbo.SESSION.S_ID WHERE S_ENDDATE >= '2016-09-01' ORDER BY dbo.SESSION.S_ID;'
                        // Isaac's ID: 12242
                        if((in_array($_SESSION['user_id'], $session_member_ids))||(in_array($_SESSION['ID'], $session_member_ids))) {
                        $is_session_shown = true; ?>
                            <style>
                            @media only screen and (min-width: 64em) {
                                .primary-nav a {
                                    padding: .75rem 1rem !important;
                                    font-size: 1.25rem;
                                }
                            }
                            .sub-nav dt.active a,
                            .sub-nav dd.active a,
                            .sub-nav li.active a {
                              font-weight: normal;
                              padding: 0.1875rem 1rem;
                            }
                            </style>
                        <?php } else { ?>
                            <style>
                            @media only screen and (min-width: 64em) {
                                .primary-nav a {
                                    padding: .75rem 2rem !important;
                                    font-size: 1.25rem;
                                }
                            }
                            .sub-nav dt.active a,
                            .sub-nav dd.active a,
                            .sub-nav li.active a {
                              font-weight: normal;
                              padding: 0.1875rem 2rem;
                            }
                            </style>
                        <?php } ?>
                        <div class="primary-nav">
                        <dd <?php
                        if (($a == $e) || ($e == $f)) {
                            echo "class=\"active\"";
                        }
                        ?>><a class="ul--blue" href="http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/" >My Activity</a></dd>
                            <?php /*
                              <dd <?php if($b == $e){ echo "class=\"active\"";} ?>><a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/goals.php">Goals</a></dd>
                             */ ?>
                        <!-- My Map -->
                        <dd <?php
                        if ($h == $e) {
                            echo "class=\"active\"";
                        }
                        ?> ><a class="ul--green" href="http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/map/map.php">My Map</a></dd>
                        <!-- My Progress -->
                        <dd  <?php
                        if ($i == $e) {
                            echo "class=\"active\"";
                        }
                        ?>><a class="ul--peach" href="http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/my-progress.php" >My Goals</a></dd>
                        <!-- My Groups -->
                        <dd  <?php
                        if ($c == $e) {
                            echo "class=\"active\"";
                        }
                        ?>><a class="ul--green" href="http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/groups.php" >My Groups</a></dd>
                        <?php if($is_session_shown) { ?>
                            <!-- My Sessions -->
                            <dd <?php
                            if ($d == $e) {
                                echo "class=\"active\"";
                            }
                            ?>><a class="ul--blue" href="http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/sessions.php">My Sessions</a></dd>
                        <?php } ?>
                        <!-- Search -->
                            <?php
                            if ($g == $e) {
                                ?>
                            <dd class="active"><a><i class="fi-magnifying-glass size-20"></i></a></dd>
                        <?php } else {
                            ?>
                            <dd class="primary-nav-search"><a href="#" data-reveal-id="search-modal-landing" class="inbox-icon"><i class="fi-magnifying-glass"></i></a></dd>
                        </div>
                            <?php
                        }
                    }

                    /**
                     * 	Generates the footer html code with respect to login status
                     * 	@author 	Aaron McCoy <herbo4@uga.edu>
                     * 	@copyright 	Copyright (c) 2014, University of Georgia
                     */
                    public static function footer() {
                        //If the user is logged in
                        ?>


                <!-- Footer -->
                <div class="footer">
                    <div class="row pt2">
                        <!-- <a class="tiny button alert" href="http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/contact-us.php"> Report a Bug </a> -->
                        <p class="global-p">
                            <a href="http://www.facebook.com/walkgeorgia">Facebook</a> | <a href="http://twitter.com/walkga">Twitter</a> | <a href="http://blog.extension.uga.edu/walkgeorgia" target="_blank">Walk Georgia Blog</a>
                        </p>
                        <small>The College of Agricultural and Environmental Sciences and the College of Family and Consumer Sciences cooperating.
                            The University of Georgia &copy; <?php echo date("Y"); ?>. All Rights Reserved.</small>
                        <hr>
                        <a href="http://www.caes.uga.edu/extension-outreach/uga-extension.html">
                          <img alt="The University of Georgia Cooperative Extension" src="http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/img/ext.png"><br><br>
                        </a>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/js/log_form.js"></script>
            <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
            <script src="http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/js/foundation.min.js"></script>
            <script type="text/javascript" src="http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/js/frontend.js"></script>
            <script>
                        $(document).foundation();
            </script>
        <?php
    }

}
