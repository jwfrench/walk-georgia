<?php
require_once('Mail.php');
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/sql-config.php");
include_once("$root/lib/sessions_api.php");
/**
*
*	WALK GA WEB SITE Back End API
*
*	@author 	Aaron McCoy <herbo4@uga.edu>
*	@copyright 	Copyright (c) 2014, University of Georgia
*	@package 	WalkGA
*	@category 	Back End
*	@version	1.1.0
*
*/

/**
*
*	WALK GA WEB SESSION MANAGER
*
*	MANAGES LOGIN, LOGOUT, REGISTER, VALIDATION, AND ACCOUNT ACCESS
*
*	@author 	Aaron McCoy <herbo4@uga.edu>
*	@copyright 	Copyright (c) 2014, University of Georgia
*	@package 	WalkGA
*	@category 	Back End
*	@version	1.0
*
*/
class SESSION{

	/**
	*	Sets a valid session with user pertinent session vars
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function login($email, $password, $group){
	  $user_data = MSSQL::query('SELECT L_ID, L_P, L_S, L_A, L_AL, L_UUID, L_COUNTY FROM LOGIN WHERE L_E = \''.$email.'\'');
	  if($user_data) {
      if(odbc_num_rows($user_data) == 1) {
        $L_ID = odbc_result($user_data, 'L_ID');
        $L_P = odbc_result($user_data, 'L_P');
        $L_S = odbc_result($user_data, 'L_S');
        $L_A = odbc_result($user_data, 'L_A');
        $L_AL = odbc_result($user_data, 'L_AL');
        if($L_AL){
          REDIRECT::locked_account();
        }
        // hash the password with the unique salt.
        $password = hash('sha512', $password . $L_S);
        // If the user exists we check if the account is locked
        // from too many login attempts
        $brute = ACCOUNT::checkbrute($L_ID);
        if($brute == true) {
          // Account is locked
          $execute = MSSQL::query('UPDATE LOGIN SET L_AL = \'1\' WHERE L_ID = \''.$L_ID.'\'');
          // Send an email to user saying their account is locked
          REDIRECT::locked_account();
          //return false;
        } else {
          // Check if the password in the database matches
          // the password the user submitted.
          if($L_P == $password) {
            $v = SESSION::validate($user_data, $email);
            if($group) {
              $group = GROUP::join_group($group, $L_ID);
              $redirect = REDIRECT::group($group);
            }
            return true;
          } else {
            // We record this attempt in the database
            $sql = 'UPDATE LOGIN SET L_A = \'$L_A+1\', L_AT = \''.$now.'\' WHERE L_ID = \''.$L_ID.'\'';
            $execute = MSSQL::query($sql);
            return false;
          }
        }
      } else {
        // No user exists.
        return false;
      }
	  }
	}

	public static function validate($user_data, $email){
		// get variables from result.
		$L_ID = odbc_result($user_data, 'L_ID');
		$L_UUID = odbc_result($user_data, 'L_UUID');
		$L_COUNTY = odbc_result($user_data, 'L_COUNTY');
		$now = time();
		// XSS protection as we might print this value
		$user_id = preg_replace('/[^0-9]+/', '', $L_ID);
		$_SESSION['user_id'] = $user_id;
		// XSS protection as we might print this value
		$_SESSION['email'] = $email;
		// Login successful.
		//SET SESSION VARS
		$_SESSION['valid'] = true;
		$_SESSION['ID'] = $L_ID;
		$_SESSION['UUID'] = $L_UUID;
		$_SESSION['COUNTY'] = $L_COUNTY;
		$sql = 'UPDATE LOGIN SET L_A = \'0\', L_AT = \''.$now.'\' WHERE L_ID = \''.$L_ID.'\'';
		$execute = MSSQL::query($sql);
		$rm = filter_input(INPUT_POST, 'remember-me', FILTER_SANITIZE_STRING);
		if($rm == 'true') {
			SESSION::set_tokens($email);
		}
	}

        /**
	*	Sets the Remember Me Token
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function set_tokens($email) {
	  //generate our token first
	  $token = SESSION::getToken(128); //random unguessable from quantifiably large space
	  //then store it with a newly generated series id
	  $sql = 'INSERT INTO REMEMBER (TOKEN) OUTPUT INSERTED.SERIES_ID VALUES (\''.$token.'\')';
	  $query = MSSQL::query($sql);
	  $SID = odbc_result($query, 'SERIES_ID');
	  //then we store our cookies
	  $cookieParams = session_get_cookie_params();
	  $time = time()+(3600*24*30);//set the cookie, expiring in 30 days. we refresh that time if they visit again before it expires
		$walkgaRememberMe = hash('sha512', $token);
		// Minimally encode variables by using the character codes of the text. Moreso used to hide the email and sid
		$SID = join(array_map(function ($n) { return sprintf('%03d', $n); }, unpack('C*', $SID)));
		$email = join(array_map(function ($n) { return sprintf('%03d', $n); }, unpack('C*', $email)));
		setcookie("wgrm", $walkgaRememberMe, $time, $cookieParams['path'], $cookieParams['domain']);
	  setcookie("sid", $SID, $time, $cookieParams['path'], $cookieParams['domain']);
	  setcookie("email", $email, $time, $cookieParams['path'], $cookieParams['domain']);
	}

	/**
	*	Creates a valid, secure session
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function secure_session() {
		$session_name = 'WALK_GA';
		$secure = false;
		$httponly = true;
		if(ini_set('session.use_only_cookies', 1) === FALSE) {
	    header('Location: ../error.php?err=Could not initiate a safe session (ini_set)');
	    exit();
		}
		// Gets current cookies params.
		$cookieParams = session_get_cookie_params();
		session_set_cookie_params(
			$cookieParams['lifetime'],
			$cookieParams['path'],
			$cookieParams['domain'],
			$secure,
			$httponly
		);
		// Sets the session name to the one set above.
		session_name($session_name);
		session_start();            // Start the PHP session
		session_regenerate_id();    // regenerated the session, delete the old one.
		//check to see if the user has cookies that are valid in our 'remember' table
		$sid = filter_input(INPUT_COOKIE, 'sid', FILTER_SANITIZE_STRING);
		$email = filter_input(INPUT_COOKIE, 'email', FILTER_SANITIZE_STRING);
		// Decode the variables.
		$sid  = join(array_map('chr', str_split($sid, 3)));
		$email  = join(array_map('chr', str_split($email, 3)));
		$hashed_cookie_token = filter_input(INPUT_COOKIE, 'wgrm', FILTER_SANITIZE_STRING);
		if(!empty($sid) && !empty($hashed_cookie_token) && !empty($email) && !isset($_SESSION['valid'])) {
			//look up the user's token in the database and compare their hashes
			$sql = "SELECT * FROM REMEMBER WHERE SERIES_ID='$sid'";
			$user = MSSQL::query($sql);
			$num = odbc_num_rows($user);
			if($num == 1) {
		    $hased_database_token = hash('sha512', odbc_result($user, 'TOKEN'));
		    if($hased_database_token === $hashed_cookie_token) {
		      //this user can log in without credentials
		      $user_data = MSSQL::query('SELECT L_ID, L_P, L_S, L_A, L_AL, L_UUID, L_COUNTY FROM LOGIN WHERE L_E = \''.$email.'\'');
		      $v = SESSION::validate($user_data, $email);
		    }
			}
		}
		//if they do, then log them in directly
		//if they don't, load our weclome page
	}


  //works as a more secure replacement for rand() or mt_rand(), thanks to Scott Arciszewski on StackOverflow
  public static function crypto_rand_secure($min, $max) {
    $range = $max - $min;
    if ($range < 1) return $min; // not so random...
    $log = ceil(log($range, 2));
    $bytes = (int) ($log / 8) + 1; // length in bytes
    $bits = (int) $log + 1; // length in bits
    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
    do {
      $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
      $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd >= $range);
    return $min + $rnd;
  }

  //creates an alphabet to use within the token and then creates a string of length $length, thanks to Scott Arciszewski on StackOverflow
  public static function getToken($length) {
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet) - 1;
    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[SESSION::crypto_rand_secure(0, $max)];
    }
    return $token;
  }

	/**
	*	Clears a valid session by destroying all session vars
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function logout(){
		// Unset all of the session variables.
		$_SESSION = array();
		//Destroy the session and cookies
		if (isset($_SERVER['HTTP_COOKIE'])) {
      $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
      foreach($cookies as $cookie) {
        $parts = explode('=', $cookie);
        $name = trim($parts[0]);
        setcookie($name, '', time()-1000);
        setcookie($name, '', time()-1000, '/');
      }
    }
		// Finally, destroy the session.
		session_destroy();
    $_COOKIE = array();
	}
}

/**
*
*	WALK GA WEB ACCOUNT MANAGER
*
*	@author 	Aaron McCoy <herbo4@uga.edu>
*	@copyright 	Copyright (c) 2014, University of Georgia
*	@package 	WalkGA
*	@category 	Back End
*	@version	1.0
*
*/
class ACCOUNT{

	/**
	*	Registers an account to the site
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function register(){
		$error_msg = '';
		if (filter_input(INPUT_POST, 'FN', FILTER_SANITIZE_STRING) != null) {
		    // Sanitize and validate the data passed in
                    if(filter_input(INPUT_POST, 'newsletter-sign-up', FILTER_SANITIZE_STRING)=='on'){
                            $Newsletter = 1;
                    }else{
                            $Newsletter = 0;
                    }
                    $privacy = filter_input(INPUT_POST, 'privacy', FILTER_SANITIZE_STRING);
                    $first_name = filter_input(INPUT_POST, 'FN', FILTER_SANITIZE_STRING);
                    $last_name = filter_input(INPUT_POST, 'LN', FILTER_SANITIZE_STRING);
                    $county = filter_input(INPUT_POST, 'CN', FILTER_SANITIZE_STRING);
		    $email = filter_var(filter_input(INPUT_POST, 'EM', FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL);
		    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		        // Not a valid email
		        $error_msg .= '<p class="error">The email address you entered is not valid</p>';
		    }
		    $password = filter_input(INPUT_POST, 'P', FILTER_SANITIZE_STRING);
		    if (strlen($password) != 128) {
		        // The hashed pwd should be 128 characters long.
		        // If it's not, something really odd has happened
		        $error_msg .= '<p class="error">Invalid password configuration.</p>';
		    }
		    // Username validity and password validity have been checked client side.
		    // This should should be adequate as nobody gains any advantage from
		    // breaking these rules.
		    //
		    $user = MSSQL::query('SELECT L_ID FROM LOGIN WHERE L_E = \''.$email.'\'');
		    if (odbc_num_rows($user) >=1) {
		        // A user with this email address already exists
		    	$error_msg .= '<p class="error">A user with this email address already exists.</p>';
		    }

		    if (empty($error_msg)) {
		        // Create a random salt
		        $random_salt = hash('sha512', uniqid(openssl_random_pseudo_bytes(16), TRUE));

		        // Create salted password
		        $password = hash('sha512', $password . $random_salt);

		        // Insert the new user into the database
		        $query = MSSQL::query('INSERT INTO LOGIN (L_P, L_E, L_S, L_FNAME, L_LNAME, L_COUNTY, L_FLAGS, L_PRIVACY) OUTPUT INSERTED.L_ID VALUES (\''.$password.'\', \''.$email.'\', \''.$random_salt.'\', \''.$first_name.'\', \''.$last_name.'\', \''.$county.'\', \''.$Newsletter.':1:'.$privacy.':0:0'.'\', \''.$privacy.'\')');
				$USER_ID = odbc_result($query, 1);
				return $USER_ID;
		    }else{
				REDIRECT::login($error_msg);
			}
		}
	}

	/**
	*	Confirms an account's registration
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	private function confirm(){

	}

	/**
	*	Confirms an account's registration
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*	@param string	$recipient	- the email address of the recipient
	*/
	private function send_confirm($recipient){
		$headers = 'From: accounts@walkgeorgia.org';
		$subject = 'Walk Georgia Account Confirmation';
		$message = 'Thank you for registering with Walk Georgia! Please follow the link below to confirm your account. After you confirm your account, we\'ll ask you for additional information to help us track your progress and give you help along the way. You\'ve just taken the first step to keeping Georgia fit!';
		mail($recipient, $subject, $message, $headers);
	}

	/**
	*	Resets the password on an account
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function reset_password(){
		$error_msg = '';
		if (filter_input(INPUT_POST, 'EM', FILTER_SANITIZE_EMAIL) != null) {
		    // Sanitize and validate the data passed in
		    $email = filter_input(INPUT_POST, 'EM', FILTER_SANITIZE_EMAIL);
		    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		        // Not a valid email
		        $error_msg .= '<p class="error">The email address you entered is not valid</p>';
		    }
		    $password = filter_input(INPUT_POST, 'P', FILTER_SANITIZE_STRING);
		    if (strlen($password) != 128) {
		        // The hashed pwd should be 128 characters long.
		        // If it's not, something really odd has happened
		        $error_msg .= '<p class="error">Invalid password configuration.</p>';
		    }
		    // Username validity and password validity have been checked client side.
		    // This should should be adequate as nobody gains any advantage from
		    // breaking these rules.
		    //
		    $user = MSSQL::query('SELECT L_ID FROM LOGIN WHERE L_E = \''.$email.'\'');
			$numrows = odbc_num_rows($user);
		    if (odbc_num_rows($user)) {
		        // Create a random salt
		        $random_salt = hash('sha512', uniqid(openssl_random_pseudo_bytes(16), TRUE));

		        // Create salted password
		        $password = hash('sha512', $password . $random_salt);

		        // Insert the new user into the database
			$sql = 'UPDATE LOGIN SET L_P=\''.$password.'\', L_E=\''.$email.'\', L_S=\''.$random_salt.'\', L_A = \'0\', L_AL=\'0\'  WHERE L_E =\''.$email.'\'';
		        $query = MSSQL::query($sql);
		    }
		}
                return $error_msg;
	}

	/**
	*	Sends an email to the account containing a reset password link
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*	@param string 	$email	- a string containing the email of the registrant's county
	*/
	public static function mail_reset($email){
		$UUID = MSSQL::query('SELECT L_UUID FROM LOGIN WHERE L_E =\''.$email.'\'');
		$UUID = odbc_result($UUID, 'L_UUID');
		if(empty($UUID)) {
			return false;
		}
		else {
			/* mail setup recipients, subject etc */
			$headers['From'] = 'walkgeorgia@uga.edu';
			$headers['To'] = $email;
			$headers['Subject'] = 'Password Reset';
			$headers['date'] = date('Y-m-d');
			$mailmsg = 'You are receiving this message because a request has been made to reset your password at www.walkgeorgia.org. To reset your password, click the following link: http://www.walkgeorgia.org/reset_password.php?EM='.$email.'&UUID='.$UUID;
			/* SMTP server name, port, user/passwd */
			$smtpinfo["host"] = "post.uga.edu";
	                $smtpinfo["port"] = "587";
	                $smtpinfo["auth"] = "PLAIN";
	                $smtpinfo["username"] = "s-walkgeorgiamail";
	                $smtpinfo["password"] = "sw33rakeJupE7tEp";
	                $smtpinfo['localhost'] = filter_input(INPUT_SERVER, 'SERVER_NAME', FILTER_SANITIZE_STRING);
			/* Create the mail object using the Mail::factory method */
			$mail_object =& Mail::factory('smtp', $smtpinfo);
			/* Ok send mail */
			$mail_object->send($email, $headers, $mailmsg);
			return true;
		}
	}

	/**
	*	Edits an account
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*	@param int 		$id	- an int id of the user being deleted
	*/
	public static function edit_account($id){
		$FNAME = filter_input(INPUT_POST, 'FN', FILTER_SANITIZE_STRING);
		$LNAME = filter_input(INPUT_POST, 'LN', FILTER_SANITIZE_STRING);
		$EM = filter_input(INPUT_POST, 'EM', FILTER_SANITIZE_STRING);
		$COUNTY = filter_input(INPUT_POST, 'CN', FILTER_SANITIZE_STRING);
		$PRIVACY = filter_input(INPUT_POST, 'privacy', FILTER_SANITIZE_STRING);
		$NEWSLETTER = filter_input(INPUT_POST, 'newsletter-sign-up', FILTER_SANITIZE_STRING);
		if($NEWSLETTER=='on'){
			$NEWSLETTER = 1;
		}else{
			$NEWSLETTER = 0;
		}
		$info = ACCOUNT::get_info($_SESSION['ID']);
		$FLAGS = $NEWSLETTER.':'.$info['FIRST_TIME_VISITOR'].':'.$PRIVACY.':'.$info['IS_ADMIN'].':'.$info['IS_COUNTY_ADMIN'];
		$sql = 'UPDATE LOGIN SET L_FNAME = \''.$FNAME.'\', L_LNAME = \''.$LNAME.'\', L_E = \''.$EM.'\', L_COUNTY = \''.$COUNTY.'\', L_FLAGS = \''.$FLAGS.'\', L_PRIVACY =\''.$PRIVACY.'\' WHERE L_UUID =\''.$id.'\'';
		$execute = MSSQL::query($sql);
	}

	/**
	*	Edits an account
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*	@param int 	$county	- an int containing the MSSQL ID of the registrant's county
	*	@param double 	$weight	- a double containing the registrant's current weight
	*	@param resource	$json 	- a JSON string containing a user's metadata
	*/
	public static function delete_account($id){
		$sql = 'DELETE FROM LOGIN WHERE L_UUID =\''.$id.'\'';
		MSSQL::query($sql);
	}

	/**
	*	Gets an account's user information
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function get_info($ID){
		$execute = MSSQL::query('SELECT L_E, L_FNAME, L_LNAME, L_COUNTY, L_FLAGS, L_DATE_JOINED, L_PRIVACY FROM LOGIN WHERE L_ID =\''.$ID.'\'');
		$info['EMAIL'] = odbc_result($execute, 'L_E');
		$info['FNAME'] = odbc_result($execute, 'L_FNAME');
		$info['LNAME'] = odbc_result($execute, 'L_LNAME');
		$info['COUNTY'] = odbc_result($execute, 'L_COUNTY');
		$flags = explode(':', odbc_result($execute, 'L_FLAGS'));
		$info['DATE_JOINED'] = odbc_result($execute, 'L_DATE_JOINED');
		$info['NEWSLETTER'] = $flags[0];
		$info['FIRST_TIME_VISITOR'] = $flags[1];
		$info['FIRST_TIME_VISITOR'] = isset($flags[1]) ? $flags[1] : 1;
 		$info['PRIVACY'] = odbc_result($execute, 'L_PRIVACY');
		$info['IS_ADMIN'] = $flags[3];
		$info['IS_COUNTY_ADMIN'] = $flags[4];
                $info['IS_ADMIN'] = isset($flags[3]) ? $flags[3] : 0;
		$info['IS_COUNTY_ADMIN'] = isset($flags[4]) ? $flags[4] : 0;
		return $info;
	}

	/**
	*	Gets an account's user information
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function checkbrute($L_ID) {
	    if ($execute = MSSQL::query('SELECT L_A FROM LOGIN WHERE L_ID = \''.$L_ID.'\'')) {
	        // If there have been more than 5 failed logins
	        if (odbc_result($execute, 1) > '5') {
	            return true;
	        } else {
	            return false;
	        }
	    }
	}

	/**
	*	Gets whether an account user is visiting for the first time
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function first_visit($L_ID) {
		$flags = MSSQL::query('SELECT L_FLAGS FROM LOGIN WHERE L_ID = \''.$L_ID.'\'');
		if(isset($flags[1])){
		$flags = odbc_result($flags, 1);
		$flags = explode(':',$flags);
		$first_time = $flags[1];
		}
		//default value for flag is 0, so we want the inverse
	    if (!isset($first_time)) {
			return true;
	    }
		return false;
	}

	/**
	*	Removes the first time visitor flag from an account
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function first_visit_remove($L_ID) {
		$flags = MSSQL::query('SELECT L_FLAGS FROM LOGIN WHERE L_ID = \''.$L_ID.'\'');
		$flags = odbc_result($flags, 1);
		$flags = explode(':',$flags);
		$sql = 'UPDATE LOGIN SET L_FLAGS=\''.$flags[0].':'.'1'.':'.$flags[2].':'.$flags[3].':'.$flags[4].'\' WHERE L_ID = \''.$L_ID.'\'';
		$query = MSSQL::query($sql);
	}

	/**
	*	Displays a user's image to the page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function avatar_large($ID){
		  if (file_exists(filter_input(INPUT_SERVER, 'DOCUMENT_ROOT', FILTER_SANITIZE_STRING).'/img/avatar/'.$ID.'.png')) {
			  $filename = 'img/avatar/'.$ID.'.png';
		  } else {
    			$filename = 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/img/avatar/0.png';
		  }
		  if($_SESSION['ID']==$ID){
		  ?>
            <a href="#" data-reveal-id="avatar" id="joyride-2">
                <div class="avatar" style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat;">
                </div>
            </a>
          <?php
		  }else{
		  ?>
            <a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/'; ?>index.php?uid=<?php echo $ID; ?>">
                <div class="avatar" style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat;">
                </div>
            </a>
          <?php
		  }
		  return $filename;
	}

	public static function avatar_change_modal($filename){
	?>
    <div class="large-12 columns">
      <form method="post" enctype="multipart/form-data">
        <div class="avatar" id="blah" style="background-image:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat; float:left;">

        </div>
        <script type="text/javascript">
	      function readURL(input) {
			if (input.files && input.files[0]) {
			  var reader = new FileReader();
			  reader.readAsDataURL(input.files[0]);
			  reader.onloadend = function () {
				$("#blah").css("background-image", "url(" + this.result + ")");
			  }
			}
		  }
        </script>
        <div style="float:left; margin-left:15px; margin-top:10px;">
          <p class="global-p">Do you want to upload a new image?</p>
          <input type="file" class="" name="file_2" id="file_2" onchange="readURL(this);">
          <br />
          <input type="submit" class="button tiny" name="file" id="file" value="Submit">
        </div>
      </form>
    </div>
    <?php
	}

	/**
	*	Displays a user's image to the page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function avatar_small($ID){
		$HTTP_HOST = filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);
		$SESSION_ID = '';
		if(isset($_SESSION['ID'])){
			$SESSION_ID = $_SESSION['ID'];
		}
	  if (file_exists(filter_input(INPUT_SERVER, 'DOCUMENT_ROOT', FILTER_SANITIZE_STRING).'/img/avatar/'.$ID.'.png')) {
		  $filename = 'http://'.$HTTP_HOST.'/img/avatar/'.$ID.'.png';
	  } else {
			$filename = 'http://'.$HTTP_HOST.'/img/avatar/0.png';
	  }
	  if($SESSION_ID==$ID){
	  ?>
        <a href="#" data-reveal-id="avatar" id="joyride-2">
            <div class="avatar-small" style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat;">
            </div>
        </a>
      <?php
	  }else{
	  ?>
        <a href="<?php echo 'http://'.$HTTP_HOST.'/'; ?>index.php?uid=<?php echo $ID; ?>">
            <div class="avatar-small" style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat;">
            </div>
        </a>
      <?php
	  }
	}

	/**
	*	Displays a user's image to the page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function avatar_medium($ID){
		  if (file_exists(filter_input(INPUT_SERVER, 'DOCUMENT_ROOT', FILTER_SANITIZE_STRING).'/img/avatar/'.$ID.'.png')) {
			  $filename = 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/img/avatar/'.$ID.'.png';
		  } else {
    			$filename = 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/img/avatar/0.png';
		  }
		  if($_SESSION['ID']==$ID){
		  ?>
            <a href="#" data-reveal-id="avatar" id="joyride-2">
                <div class="avatar-medium" style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat;">
                </div>
            </a>
          <?php
		  }else{
		  ?>
            <a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/'; ?>index.php?uid=<?php echo $ID; ?>">
                <div class="avatar-medium" style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat;">
                </div>
            </a>
          <?php
		  }
	}

        /**
	*	Displays a user's image to the page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function avatar_display_only($ID){
		  if (file_exists(filter_input(INPUT_SERVER, 'DOCUMENT_ROOT', FILTER_SANITIZE_STRING).'/img/avatar/'.$ID.'.png')) {
			  $filename = 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/img/avatar/'.$ID.'.png';
		  } else {
    			$filename = 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/img/avatar/0.png';
		  }
		  if($_SESSION['ID']==$ID){
		  ?>
                <div style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat; margin: auto; width: 120px; height: 120px; border-radius: 50%;">
                </div>
          <?php
		  }else{
		  ?>
                <div style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat; margin: auto; width: 120px; height: 120px; border-radius: 50%;">
            </div>
          <?php
		  }
	}

	/**
*	Displays a user's image to the My goals page
*	@author 	Aaron McCoy <herbo4@uga.edu>
*	@copyright 	Copyright (c) 2014, University of Georgia
*/
public static function avatar_goals($ID){
if (file_exists(filter_input(INPUT_SERVER, 'DOCUMENT_ROOT', FILTER_SANITIZE_STRING).'/img/avatar/'.$ID.'.png')) {
	$filename = 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/img/avatar/'.$ID.'.png';
} else {
		$filename = 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/img/avatar/0.png';
}
if($_SESSION['ID']==$ID){
?>
					<div style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat;  width: 120px; height: 120px; border-radius: 50%;">
					</div>
		<?php
}else{
?>
					<div style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat;  width: 120px; height: 120px; border-radius: 50%;">
			</div>
		<?php
}
}


	/**
	*	Uploads a user's image to the server
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function avatar_upload($L_ID) {
		$errmsg='img_success=1';
                $allowedExts = array('gif', 'jpeg', 'jpg', 'png');
		$temp = explode('.', $_FILES['file_2']['name']);
		$extension = strtolower(end($temp));
		if(($_FILES['file_2']['size'] > 0 && $_FILES['file_2']['size'] < 1048576 )){
			if (in_array($extension, $allowedExts)){
				move_uploaded_file($_FILES['file_2']['tmp_name'], ROOT.'/img/avatar/'.$_SESSION['ID'].'.png');
			}else{
				$errmsg = 'err_msg=We don\'t support that type of image.  You can upload a jpeg, jpg, gif, or png.';
				REDIRECT::current_page($errmsg);
			}
		} else {
			$errmsg = 'err_msg=Your image is too large for us.  We can only take images 1MB or less, and your image is about '.  number_format($_FILES['file_2']['size']/1000000, 0).' MB.  Try re-sizing it on your computer or uploading a smaller image.';
			REDIRECT::current_page($errmsg);
		}
		REDIRECT::current_page($errmsg);
	}

	/**
	*	Prints a table with all of the registered user data
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function displayRegistered() {
	    $execute = MSSQL::query('SELECT L_ID FROM LOGIN WHERE L_ID > 0 ORDER BY L_LNAME');
		$i=0;
		while(odbc_fetch_row($execute)){
			$i++;
			$USER_ID = odbc_result($execute, 1);
			$info = ACCOUNT::get_info($USER_ID);
			?>
    <div class="group-unit">
      <div class="row">
        <div class="large-8 medium-7 small-12 columns">
          <?php
          if (file_exists(filter_input(INPUT_SERVER, 'DOCUMENT_ROOT', FILTER_SANITIZE_STRING).'/img/avatar/'.$USER_ID.'.png')) {
			  $filename = '../img/avatar/'.$USER_ID.'.png';
		  } else {
    			$filename = '../img/avatar/0.png';
		  }
		  ?>
          <img src="<?php echo $filename; ?>" class="group-icon" alt="Picture of the User" height="56px" width="56px">
          <h3 class="global-h3" style="padding-top:18px;"><?php echo $info['FNAME'].' '.$info['LNAME'];?> | <?php echo $info['EMAIL'];?> </h3>
        </div>
        <div class="large-4 medium-5 small-12 columns" style="padding-top:15px; padding-left:5%;">
         <a href="#" class="button tiny" data-dropdown="drop<?php echo $USER_ID; ?>">Admin Options</a>

                <ul id="drop<?php echo $USER_ID; ?>" class="f-dropdown" style="text-align:left;" data-dropdown-content>
                  <li><a href="edit_user_groups.php?id=<?php echo $USER_ID; ?>">Edit User Groups</a></li>
                  <li><a href="reset_password.php?EM=<?php echo $info['EMAIL']; ?>">Reset Password</a></li>
                  <li><a href="delete_user.php?id=<?php echo $USER_ID; ?>" class="alert">Delete User</a></li>
                </ul>
        </div>
      </div>
    </div>
	<?php
		}
	?>
    <div class="row">
    TOTAL: <?php echo $i ;?>
    </div>
    <?php
	}
}

/**
*
*	WALK GA WEB MSSQL MANAGER
*
*	@author 	Aaron McCoy <herbo4@uga.edu>
*	@copyright 	Copyright (c) 2014, University of Georgia
*	@package 	WalkGA
*	@category 	Back End
*	@version	1.0
*
*/
class MSSQL{
	/**
	*	Connects through odbc to the WalkGA MSSQL Database
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function connect(){
		// Connect to MSSQL
		$connection = odbc_connect(HOST, USER, PASSWORD);
		return $connection;
	}

	/**
	*	Performs the given query through odbc over the provided link resource
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*	@param string 	$sql - Query to run
	*	@param resource	$link - Connection to a database
	*/
	public static function query($sql){
		$link = MSSQL::connect();
		try{
			$exec = odbc_exec($link, $sql);
		}catch(Exception $e){
			echo odbc_errormsg($exec);
		}
		return $exec;
	}
}

/**
*
*	WALK GA WEB REDIRECT LIST
*
*	@author 	Aaron McCoy <herbo4@uga.edu>
*	@copyright 	Copyright (c) 2014, University of Georgia
*	@package 	WalkGA
*	@category 	Back End
*	@version	1.0
*
*/
class REDIRECT{
	/**
	*	Redirects users to the home page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function home($msg){
		header('Location:http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'?msg='.$msg);
		exit();
	}

	/**
	*	Redirects users to the account dashboard page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function dash($msg){
		header('Location:http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'?msg='.$msg);
		exit();
	}

	/**
	*	Redirects users to the account dashboard page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function group_list($msg){
		header('Location:http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/groups.php?msg='.$msg);
		exit();
	}

        /**
	*	Redirects users to the account dashboard page if group is deleted
	*	@author 	Juweek Adolphe <jadolphe@uga.edu>
	*	@copyright 	Copyright (c) 2015, University of Georgia
	*/
	public static function group_list_del($msg){
		header('Location:http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/groups.php?'.$msg);
		exit();
	}

	/**
	*	Redirects users to the account dashboard page, though after they have added a favorite
	*	@author 	Juweek Adolphe <jadolphe@uga.edu>
	*	@copyright 	Copyright (c) 2015, University of Georgia
	*/
	public static function group_list_fav($msg){
		header('Location:http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/groups.php?'.$msg);
		exit();
	}

	/**
	*	Redirects users to the login page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function login($msg){
		header('Location:http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/login.php?msg='.$msg);
		exit();
	}

	/**
	*	Redirects users to the login with an error message page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function bad_login(){
		header('Location:http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/login.php?fail=1');
		exit();
	}

	/**
	*	Redirects users to the locked account page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function locked_account($msg){
		header('Location:http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/locked.php?msg='.$msg);
		exit();
	}

	/**
	*	Redirects users to the register page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function register($msg){
		header('Location:http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/register.php?msg='.$msg);
		exit();
	}

	/**
	*	Redirects users to the group page specified by ID
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function group($GROUP_ID){
		header('Location:http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/group/groups.php?group='.$GROUP_ID);
		exit();
	}

	/**
	*	Redirects users to the session page specified by ID
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function sessions($SESSION_ID){
		if($SESSION_ID !=0){
			header('Location:http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/session/sessions.php?session='.$SESSION_ID);
		}else{
			header('Location:http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/sessions.php');
		}
		exit();
	}

        /**
	*	Redirects users to the session page specified by ID
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function goals($msg){
		if(!empty($msg)){
			header('Location:http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/my-progress.php?msg='.$msg);
		}else{
			header('Location:http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/my-progress.php');
		}
		exit();
	}

	/**
	*	Redirects users to existing page, with any error messages being posted as GET vars
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function current_page($var){
		header('Location: http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_STRING).'?'.$var);
		die;
	}

	/**
	*	Redirects users to existing page, with any error messages being posted as GET vars
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function subgroup_page($GROUP_ID){
		header('Location: http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/group/subgroups.php?group='.$GROUP_ID);
		die;
	}

	/**
	*	Redirects users to existing page, with any error messages being posted as GET vars
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function user_page($USER_ID, $msg){
		header('Location: http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/index.php?uid='.$USER_ID.'&msg='.$msg);
		die;
	}

        	/**
	*	Redirects users to existing page, with any error messages being posted as GET vars
	*	@author 	Juweek Adolphe <jadolphe@uga.edu>
	*	@copyright 	Copyright (c) 2015, University of Georgia
	*/
	public static function user_account(){
		header('Location: http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/user-account.php?msg=1');
		die;
	}

	/**
	*	Redirects users to existing page, with any error messages being posted as GET vars
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function map($msg){
			header('Location: http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/map/map.php?msg='.$msg);
		die;
	}

        /**
	*	Redirects users to existing page, with any error messages being posted as GET vars
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function honeypot(){
            if(filter_input(INPUT_GET, 'honeypot', FILTER_SANITIZE_STRING)){
		header('Location: http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING));
		die;
            }else{

            }
	}
}

/**
*
*	WALK GA WEB REDIRECT LIST
*
*	@author 	Aaron McCoy <herbo4@uga.edu>
*	@copyright 	Copyright (c) 2014, University of Georgia
*	@package 	WalkGA
*	@category 	Back End
*	@version	1.0
*
*/
class ACTIVITY{
	/**
	*	Logs an activity to a user's account
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function log_activity(){
		$uid = $_SESSION['ID'];
		$sd = filter_input(INPUT_POST, 'sd', FILTER_SANITIZE_STRING);
		$date = filter_input(INPUT_POST, 'year', FILTER_SANITIZE_STRING).'-'.filter_input(INPUT_POST, 'month', FILTER_SANITIZE_STRING).'-'.filter_input(INPUT_POST, 'day', FILTER_SANITIZE_STRING);
		$aid = explode(':',filter_input(INPUT_POST, 'srch', FILTER_SANITIZE_STRING));
		$unit = $aid[0];
		$aid = $aid[1];
		$time = 0;
		$su = 1;
		if(filter_input(INPUT_POST, 'hours', FILTER_SANITIZE_STRING)!= null){
			$time += (filter_input(INPUT_POST, 'hours', FILTER_SANITIZE_STRING)*60*60);
		}
		if(filter_input(INPUT_POST, 'minutes', FILTER_SANITIZE_STRING)!= null){
			$time += (filter_input(INPUT_POST, 'minutes', FILTER_SANITIZE_STRING)*60);
		}
		if((filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING)!= null)){
			$su = (filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING));
		}else{
			$su = 0;
		}
		$query = MSSQL::query('SELECT A_BASE_MET, A_MOD, A_MOD_BASE FROM ACTIVITY WHERE A_ID=\''.$aid.'\'');
		$mets=odbc_result($query, 'A_BASE_MET');
		$mod_rate=odbc_result($query, 'A_MOD');
		$base_speed = odbc_result($query, 'A_MOD_BASE');
		//we need to calculate the appropriate mets
		if($unit == 1){
			$su = (odbc_result($query, 'A_MOD_BASE')*($time/3600));
			if((filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING)!= null) && filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING) !='' ){
				$su = (filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING));
			}
			if((filter_input(INPUT_POST, 'su_units', FILTER_SANITIZE_STRING)!= null) && filter_input(INPUT_POST, 'su_units', FILTER_SANITIZE_STRING) !=''){
				switch(filter_input(INPUT_POST, 'su_units', FILTER_SANITIZE_STRING)){
					case 'miles':
					$mult = 1;
					break;
					case 'kilometers':
					$mult = 1.60934;
					break;
					case 'meters':
					$mult = 1609.34;
					break;
				}
				$su = ($su / $mult);
			}
			//take the baseline speed in mph
			$speed = $su/(($time/60)/60);
			//calulate the difference between user speed and the activity base speed
			$modifier = $speed - $base_speed;
			if ($modifier > 0){
				//now that we have the differnce, we need to account for additional mets per unit of speed
				$bonus_mets = $mod_rate * $modifier;
				$mets = $mets + $bonus_mets;
			}
		}
		//points awarded = (METS * time) + subjective difficulty
		$pa = floor((($time *$mets) + ($sd*100))/100);
                //echo '('.$time.'*'.$mets.'+'.($sd*100).')/100 = '.$pa;
		$sql = 'INSERT INTO LOG (AL_UID, AL_AID, AL_TIME, AL_UNIT, AL_SD, AL_PA, AL_DATE) VALUES (\''.$uid.'\', \''.$aid.'\', \''.$time.'\', \''.$su.'\', \''.$sd.'\', \''.$pa.'\', \''.$date.'\')';
		$insert_activity_query = MSSQL::query($sql);

                //now check goals to see if you have completed the goal
                $UUID = $_SESSION['UUID'];
                $ID = $_SESSION['ID'];

                $count_query = MSSQL::query("SELECT COUNT(G_STARTDATE) as COUNT, G_STARTDATE FROM GOALS WHERE G_UUID ='$UUID' AND G_COMPLETED IS NULL AND  G_STARTDATE >= CAST(DATEADD(dd,-7,GETDATE()) AS DATE) GROUP BY G_STARTDATE");
                $goalCount = odbc_result($count_query, 'COUNT');
                $startDate = new DateTime(odbc_result($count_query, 'G_STARTDATE'));
                $thisDate = new DateTime(date($date));
                if ($goalCount >= 1 && ($startDate <= $thisDate)) {
                    MSSQL::query("UPDATE GOALS SET G_EARNED= G_EARNED + '$pa' WHERE G_UUID ='$UUID' AND G_COMPLETED IS NULL AND  G_STARTDATE >= CAST(DATEADD(dd,-7,GETDATE()) AS DATE)");
                    $pointQuery = MSSQL::query("SELECT G_POINT_GOAL, G_EARNED FROM GOALS WHERE G_UUID ='$UUID' AND G_COMPLETED IS NULL AND  G_STARTDATE >= CAST(DATEADD(dd,-7,GETDATE()) AS DATE)");
                    $points_goal = odbc_result($pointQuery, 'G_POINT_GOAL');
                    $current_points = odbc_result($pointQuery, 'G_EARNED');
                    if ($current_points >= $points_goal){
                        $date = date('Y-m-d');
                        $datetime = date('Y-m-d H:i:s');
                        MSSQL::query("UPDATE GOALS SET G_COMPLETED='$datetime', G_ENDDATE = '$date' WHERE G_UUID ='$UUID' AND G_COMPLETED IS NULL AND  G_STARTDATE >= CAST(DATEADD(dd,-7,GETDATE()) AS DATE)");
                        $_SESSION['completedGoal'] = 1;
                    }
                }
		return $pa;
	}

	/**
	*	Edits an existing activity log
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function edit_activity(){
		$id = filter_input(INPUT_POST, 'activity_log_id', FILTER_SANITIZE_STRING);
		$uid = $_SESSION['ID'];
		$sd = filter_input(INPUT_POST, 'sd', FILTER_SANITIZE_STRING);
		$date = filter_input(INPUT_POST, 'year', FILTER_SANITIZE_STRING).'-'.filter_input(INPUT_POST, 'month', FILTER_SANITIZE_STRING).'-'.filter_input(INPUT_POST, 'day', FILTER_SANITIZE_STRING);
		$aid = explode(':',filter_input(INPUT_POST, 'srch', FILTER_SANITIZE_STRING));
		$unit = $aid[0];
		$aid = $aid[1];
		$time = 0;
		$su = 1;
		if(filter_input(INPUT_POST, 'hours', FILTER_SANITIZE_STRING)!= null){
			$time += (filter_input(INPUT_POST, 'hours', FILTER_SANITIZE_STRING)*60*60);
		}
		if(filter_input(INPUT_POST, 'minutes', FILTER_SANITIZE_STRING)!= null){
			$time += (filter_input(INPUT_POST, 'minutes', FILTER_SANITIZE_STRING)*60);
		}
		if((filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING)!= null)){
			$su = (filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING));
		}
		$query = MSSQL::query('SELECT A_BASE_MET, A_MOD, A_MOD_BASE FROM ACTIVITY WHERE A_ID=\''.$aid.'\'');
		$mets=odbc_result($query, 'A_BASE_MET');
		$mod_rate=odbc_result($query, 'A_MOD');
		$base_speed = odbc_result($query, 'A_MOD_BASE');
		//we need to calculate the appropriate mets
		if($unit == 1){
			$su = (odbc_result($query, 'A_MOD_BASE')*($time/3600));
			if((filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING)!= null) && filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING) !='' ){
				$su = filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING);
			}
			if(filter_input(INPUT_POST, 'su_units', FILTER_SANITIZE_STRING)!= null && filter_input(INPUT_POST, 'su_units', FILTER_SANITIZE_STRING) !=''){
				switch(filter_input(INPUT_POST, 'su_units', FILTER_SANITIZE_STRING)){
					case 'miles':
					$mult = 1;
					break;
					case 'kilometers':
					$mult = 1.60934;
					break;
					case 'meters':
					$mult = 1609.34;
					break;
				}
				$su = ($su / $mult);
			}
			//take the baseline speed in mph
			$speed = $su/(($time/60)/60);
			//calulate the difference between user speed and the activity base speed
			$modifier = $speed - $base_speed;
			if ($modifier > 0){
				//now that we have the differnce, we need to account for additional mets per unit of speed
				$bonus_mets = $mod_rate * $modifier;
				$mets = $mets + $bonus_mets;
			}
		}
		//points awarded = (METS * time) + subjective difficulty
		$pa = floor((($time *$mets) + ($sd*100))/100);
		$sql = 'UPDATE LOG SET AL_AID=\''.$aid.'\', AL_TIME=\''.$time.'\', AL_UNIT=\''.$su.'\', AL_SD=\''.$sd.'\', AL_PA=\''.$pa.'\', AL_DATE=\''.$date.'\' WHERE AL_UID=\''.$uid.'\' AND AL_ID=\''.$id.'\'';


		$query = MSSQL::query($sql);
		REDIRECT::home('Activity Edited');
	}

	/**
	*	Creates the dataset of allowable exercises for the logging form
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function select_activity($aid){
		$query = MSSQL::query('SELECT A_ID, A_NAME, A_UNIT FROM ACTIVITY ORDER BY A_NAME ASC');
		while(odbc_fetch_row($query)){
			$value = odbc_result($query, 'A_NAME');
			$unit = odbc_result($query, 'A_UNIT');
			$id = odbc_result($query, 'A_ID');
			?>
            <option id="<?php echo $id;?>" name="<?php echo $value;?>" value="<?php echo $unit.':'.$id;?>" <?php if($aid==$id){ echo 'selected="selected"';}?>><?php echo odbc_result($query, 2);?></option>
            <?php
		}
	}

        /**
	*	Creates the dataset of allowable exercises for the logging form
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function select_first_activity(){
		$query = MSSQL::query('SELECT A_ID, A_NAME, A_UNIT, A_BASE_MET FROM ACTIVITY ORDER BY A_NAME ASC');
               ?>
                    <option disabled selected></option>
                <?php
		while(odbc_fetch_row($query)){
			$value = odbc_result($query, 'A_NAME');
			$unit = odbc_result($query, 'A_UNIT');
			$id = odbc_result($query, 'A_ID');
                        $mets = odbc_result($query, 'A_BASE_MET');
			?>
            <option id="<?php echo $id;?>" name="<?php echo $value;?>" value="<?php echo $mets;?>"><?php echo odbc_result($query, 2);?></option>
            <?php
		}
	}

        /**
	*	Creates the dataset of allowable exercises for the logging form
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function select_second_activity(){
		$query = MSSQL::query('SELECT A_ID, A_NAME, A_UNIT, A_BASE_MET FROM ACTIVITY ORDER BY A_NAME ASC');
               ?>
                    <option disabled selected></option>
                <?php
		while(odbc_fetch_row($query)){
			$value = odbc_result($query, 'A_NAME');
			$unit = odbc_result($query, 'A_UNIT');
			$id = odbc_result($query, 'A_ID');
                        $mets = odbc_result($query, 'A_BASE_MET');
			?>
            <option id="<?php echo $id;?>" name="<?php echo $value;?>" value="<?php echo $mets;?>"><?php echo odbc_result($query, 2);?></option>
            <?php
		}
	}

	/**
	*	Delets an activity record
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function delete_activity($aid){
		$query = MSSQL::query('DELETE FROM LOG WHERE AL_ID=\''.$aid.'\'');
	}

	/**
	*	Creates the activity logging form
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function activity_form(){
		?>
            <!-- DISTANCE ACTIVITY LOGGING FORM -->

            <div id="activityModal" class="reveal-modal" data-reveal>
            <a class="close-reveal-modal">&#215;</a>
              <div class="row">
                <div class="large-12 columns">
                <h2 class="custom-font-small-blue">Log an Activity</h2>
                <hr />
                </div>
              </div>
              <form id="modal-form" action="log_activity.php" method="post" data-abide novalidate="validate">
                <div class="row">
                  <div class="large-6 medium-12 columns">
                    <h3 class="global-h2">Date of Activity:</h3>
                	<div class="row date">


           <!-- Month -->
           <?php
		    $day = date('d');
			$month = date('m');
			$year = date('Y');
		   ?>
             <div class="large-4 medium-4 columns">
               <label>Month
                 <select id="month" name="month" onchange="checkmonth(this)" required>
                   <option value="1" <?php if ($month=='1'){echo 'selected="selected"';} ?>>01 January</option>
                   <option value="2" <?php if ($month=='2'){echo 'selected="selected"';} ?>>02 February</option>
                   <option value="3" <?php if ($month=='3'){echo 'selected="selected"';} ?>>03 March</option>
                   <option value="4" <?php if ($month=='4'){echo 'selected="selected"';} ?>>04 April</option>
                   <option value="5" <?php if ($month=='5'){echo 'selected="selected"';} ?>>05 May</option>
                   <option value="6" <?php if ($month=='6'){echo 'selected="selected"';} ?>>06 June</option>
                   <option value="7" <?php if ($month=='7'){echo 'selected="selected"';} ?>>07 July</option>
                   <option value="8" <?php if ($month=='8'){echo 'selected="selected"';} ?>>08 August</option>
                   <option value="9" <?php if ($month=='9'){echo 'selected="selected"';} ?>>09 September</option>
                   <option value="10" <?php if ($month=='10'){echo 'selected="selected"';} ?>>10 October</option>
                   <option value="11" <?php if ($month=='11'){echo 'selected="selected"';} ?>>11 November</option>
                   <option value="12" <?php if ($month=='12'){echo 'selected="selected"';} ?>>12 December</option>
                 </select>
               </label>
             </div>
           <!-- End Month -->

           <!-- Day -->
             <div class="large-4 medium-4 columns">
               <label>Day
                 <select id="day" name="day" onchange="checkday(this)" required>
                   <option value="1" <?php if ($day=='1'){echo 'selected="selected"';} ?>>01</option>
                   <option value="2" <?php if ($day=='2'){echo 'selected="selected"';} ?>>02</option>
                   <option value="3" <?php if ($day=='3'){echo 'selected="selected"';} ?>>03</option>
                   <option value="4" <?php if ($day=='4'){echo 'selected="selected"';} ?>>04</option>
                   <option value="5" <?php if ($day=='5'){echo 'selected="selected"';} ?>>05</option>
                   <option value="6" <?php if ($day=='6'){echo 'selected="selected"';} ?>>06</option>
                   <option value="7" <?php if ($day=='7'){echo 'selected="selected"';} ?>>07</option>
                   <option value="8" <?php if ($day=='8'){echo 'selected="selected"';} ?>>08</option>
                   <option value="9" <?php if ($day=='9'){echo 'selected="selected"';} ?>>09</option>
                   <option value="10" <?php if ($day=='10'){echo 'selected="selected"';} ?>>10</option>
                   <option value="11" <?php if ($day=='11'){echo 'selected="selected"';} ?>>11</option>
                   <option value="12" <?php if ($day=='12'){echo 'selected="selected"';} ?>>12</option>
                   <option value="13" <?php if ($day=='13'){echo 'selected="selected"';} ?>>13</option>
                   <option value="14" <?php if ($day=='14'){echo 'selected="selected"';} ?>>14</option>
                   <option value="15" <?php if ($day=='15'){echo 'selected="selected"';} ?>>15</option>
                   <option value="16" <?php if ($day=='16'){echo 'selected="selected"';} ?>>16</option>
                   <option value="17" <?php if ($day=='17'){echo 'selected="selected"';} ?>>17</option>
                   <option value="18" <?php if ($day=='18'){echo 'selected="selected"';} ?>>18</option>
                   <option value="19" <?php if ($day=='19'){echo 'selected="selected"';} ?>>19</option>
                   <option value="20" <?php if ($day=='20'){echo 'selected="selected"';} ?>>20</option>
                   <option value="21" <?php if ($day=='21'){echo 'selected="selected"';} ?>>21</option>
                   <option value="22" <?php if ($day=='22'){echo 'selected="selected"';} ?>>22</option>
                   <option value="23" <?php if ($day=='23'){echo 'selected="selected"';} ?>>23</option>
                   <option value="24" <?php if ($day=='24'){echo 'selected="selected"';} ?>>24</option>
                   <option value="25" <?php if ($day=='25'){echo 'selected="selected"';} ?>>25</option>
                   <option value="26" <?php if ($day=='26'){echo 'selected="selected"';} ?>>26</option>
                   <option value="27" <?php if ($day=='27'){echo 'selected="selected"';} ?>>27</option>
                   <option value="28" <?php if ($day=='28'){echo 'selected="selected"';} ?>>28</option>
                   <option value="29" <?php if ($day=='29'){echo 'selected="selected"';} ?>>29</option>
                   <option value="30" <?php if ($day=='30'){echo 'selected="selected"';} ?>>30</option>
                   <option value="31" <?php if ($day=='31'){echo 'selected="selected"';} ?>>31</option>
                 </select>
               </label>
             </div>
           <!-- End Day -->

           <!-- Year -->
             <div class="large-4 medium-4 columns">
               <label>Year
                 <select id="year" name="year" onchange="checkyear(this)" required>
                   <option value="2008" <?php if ($year=='2008'){echo 'selected="selected"';} ?>>2008</option>
                   <option value="2009" <?php if ($year=='2009'){echo 'selected="selected"';} ?>>2009</option>
                   <option value="2010" <?php if ($year=='2010'){echo 'selected="selected"';} ?>>2010</option>
                   <option value="2011" <?php if ($year=='2011'){echo 'selected="selected"';} ?>>2011</option>
                   <option value="2012" <?php if ($year=='2012'){echo 'selected="selected"';} ?>>2012</option>
                   <option value="2013" <?php if ($year=='2013'){echo 'selected="selected"';} ?>>2013</option>
                   <option value="2014" <?php if ($year=='2014'){echo 'selected="selected"';} ?>>2014</option>
                   <option value="2015" <?php if ($year=='2015'){echo 'selected="selected"';} ?>>2015</option>
                   <option value="2016" <?php if ($year=='2016'){echo 'selected="selected"';} ?>>2016</option>
                   <option value="2017" <?php if ($year=='2017'){echo 'selected="selected"';} ?>>2017</option>
                 </select>
               </label>
             </div>
           <!-- End Year -->


                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="large-6 medium-6 columns">
                  <h3 class="global-h2">Activity:</h3>
                  <div class="row">
                    <div class="large-12 columns">

                    <label>Activity:
                      <select id="exercise" name="srch" onchange="su_magic(this, '');" class="excercise" required>
                        <?php
                            $aid = odbc_result(MSSQL::query('SELECT TOP 1 AL_AID FROM LOG WHERE AL_UID =\''.$_SESSION['ID'].'\' ORDER BY AL_DATE DESC'), 'AL_AID');
                            $list = ACTIVITY::select_activity($aid);
			?>
                      </select>
                    </label>

                    </div>
                    </div>

                  </div>
                </div>
                <div class="row">
                  <div class="large-6 medium-6 columns">
                    <h3 class="global-h2">Time Spent Exercising:</h3>
                  </div>
                </div>
                <div class="row">
                  <div class="large-2 medium-2 columns">
                    <label>Minutes:
                      <input type="number" id="minutes" name="minutes" min="1" max="480" onchange="return checkMinutes(this)" title="Enter the number of minutes exercised for this activity. Limit is 8 hours (480 minutes)." required pattern="^([1-9]|[0-9][0-9]|[1-3][0-9][0-9]|4[0-7][0-9]|480)$">
                    </label>
                  </div>
                </div>
                <div class="row">
                    <div id="su" name = "su">
                    </div>
                </div>
                <div class="row">
                  <div class="large-6 medium-6 columns">
                    <h3 class="global-h2">Difficulty:</h3>
                    <label>How difficult was it to complete?
                      <select name="sd" id="sd" required>
                        <option value="1">Easy</option>
                        <option value="2">Somewhat difficult</option>
                        <option value="3">Difficult</option>
                        <option value="4">More difficult than usual</option>
                        <option value="5">Very difficult</option>
                      </select>
                    </label>
                  </div>
                </div>
                <div class="row">
                  <div class="large-12 columns">
                    <input type="submit"  id="submit" class="tiny button" onclick="return validate()" value="Submit"/>
                  </div>
                </div>
              </form>
            </div>
            <script>
                //create date object using current date
                       var today = new Date();
                       var smonth = today.getMonth() +1;
                       var sday = today.getDate();
                       var syear = today.getFullYear();
                //change date values if new dropdown items selected
                       function checkmonth(selBox) {
                            smonth = parseInt(selBox.options[selBox.selectedIndex].value);
            }
                        function checkday(selBox) {
                            sday = parseInt(selBox.options[selBox.selectedIndex].value);
                        }
                        function checkyear(selBox) {
                            syear = parseInt(selBox.options[selBox.selectedIndex].value);
                        }
												function checkMinutes(elem) {
													if(!isNaN($(elem).val())) {
														if($(elem).val() > 480) {
															return $(elem).val(480);
														}
														else {
															return $(elem).val();
														}
													}
													else {
														return $(elem).val(1);
													}
												}
												function checkDistance(elem) {
													if(!isNaN($(elem).val())) {
														if($(elem).val() > 100) {
															return $(elem).val(100);
														}
														else {
															return $(elem).val();
														}
													}
													else {
														return $(elem).val(1);
													}
												}
                //make new date object out of date variables changed by dropdowns; check if date is in future: if so return false and prevent submission
                        function validate() {
                            var d = new Date();
                            d.setFullYear(syear, smonth-1, sday);
                            d.setHours(0);
                            d.setMinutes(0);
                            if(today < d){
                                alert("You cannot log a future activity. Make sure to check your date.");
                                return false;
                            }
														var input_form = $('div#activityModal form').find(':input').each(function() {
															var this_id = $(this).attr('id');
															if(this_id == 'su') {
																if(parseInt($(this).val()) > 100) {
																	alert("Please enter a distance less than 100.");
																	return false;
																}
															}
															if(this_id == 'minutes') {
																if(parseInt($(this).val()) > 480) {
																	alert("Please enter an activity with less than 8 hours (480 minutes) of activity.");
																	return false;
																}
															}
														});
                        	return true;
            						}
                </script>
            <?php
	}
	/**
	*	Gets an account's current point score for the given date range
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function getPointRange($STARTDATE, $ENDDATE){
            $id = $_SESSION['ID'];
            $date_range = "AL_DATE between '$STARTDATE' and '$ENDDATE'";
            $sql = "SELECT SUM(AL_PA) AS 'AL_PA' FROM LOG WHERE AL_UID ='$id' AND $date_range ORDER BY AL_PA DESC";
            $activity = MSSQL::query($sql);
            return odbc_result($activity, 'AL_PA');
        }
	/**
	*	Gets an account's current point score
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function get_breakdown($ID){
		$info = '';
		$info['POINTS']= '';
		$info['USER_POINTS']= '';
		$info['ACTIVITY']= '';
		$info['TOTALS']['TIME']= '';
		$info['TOTALS']['DISTANCE']= '';
		$info['USER_POINTS'][$ID] ='';
		$daterange = '';
		if (filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING) != null) {
		    //day 1
		    $date1 = explode('/', filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING));

		    $month1 = $date1[0];
		    $day1 = $date1[1];
		    $year1 = $date1[2];

		    //day 2
		    $date2 = explode('/', filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING));

		    $month2 = $date2[0];
		    $day2 = $date2[1];
		    $year2 = $date2[2];
		    $date1= $year1.'-'.$month1.'-'.$day1;
		    $date2= $year2.'-'.$month2.'-'.$day2;

		    $date_range = 'AL_DATE >= \'' . $date1 . '\' AND AL_DATE <= \'' . $date2 . '\' AND';
		}
		if(isset($_GET['session'])){
			echo 'session';
			$session = SESSIONS::get_info(filter_input(INPUT_GET, 'session', FILTER_SANITIZE_STRING));
			$date_range = 'AL_DATE >= \''.$session['S_STARTDATE'].'\' AND AL_DATE <= \''.$session['S_ENDDATE'].'\' AND';
			$date_range2 = 'AL_DATE >= \''.$session['S_STARTDATE'].'\' AND AL_DATE <= \''.$session['S_ENDDATE'].'\' AND';
		}
		$execute = MSSQL::query('SELECT AL_PA, AL_AID, AL_UNIT, AL_TIME FROM LOG WHERE '.$daterange.' AL_UID = \''.$ID.'\'');
		while(odbc_fetch_row($execute)){
		  $aid = odbc_result($execute, 'AL_AID');
		  if(!isset($info['POINTS'][$aid])){
			  $info['POINTS'][$aid]='';
		  }
		  $info['POINTS'][$aid] +=odbc_result($execute, 'AL_PA');
		  if(!isset($info['USER_POINTS'][$ID][$aid])){
			  $info['USER_POINTS'][$ID][$aid]='';
		  }
		  $info['USER_POINTS'][$ID][$aid] +=odbc_result($execute, 'AL_PA');
		  $info['ACTIVITY'][$aid]['ID'] =odbc_result($execute, 'AL_AID');
		  if(!isset($info['ACTIVITY'][$aid]['UNITS'])){
			  $info['ACTIVITY'][$aid]['UNITS']='';
		  }
		  $info['ACTIVITY'][$aid]['UNITS'] += odbc_result($execute, 'AL_UNIT');
		  if(!isset($info['ACTIVITY'][$aid]['TIME'])){
			  $info['ACTIVITY'][$aid]['TIME']='';
		  }
		  $info['ACTIVITY'][$aid]['TIME'] +=odbc_result($execute, 'AL_TIME');
		  if(!isset($info['TOTALS']['TIME'][$aid])){
			  $info['TOTALS']['TIME'][$aid]='';
		  }
		  $info['TOTALS']['TIME'][$aid] += odbc_result($execute, 'AL_TIME');
		  $DA = MSSQL::query('SELECT A_UNIT FROM ACTIVITY WHERE A_ID=\''.$aid.'\'');
		  $DA = odbc_result($DA, 'A_UNIT');
		  if ($DA == 1){
			  if(!isset($info['TOTALS']['DISTANCE'][$aid])){
				  $info['TOTALS']['DISTANCE'][$aid]='';
			  }
			  $info['TOTALS']['DISTANCE'][$aid] += odbc_result($execute, 3);
		  }
		}
		if(isset($info)){
			return $info;
		}
	}

	/**
	*	Gets an account's current point score
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function points_breakdown($ID){
		$points_total = "0";
		$distance_total = "0";
		$time_total = "0";
		$breakdown = ACTIVITY::get_breakdown($ID);
		if(isset($breakdown)){
			if(isset($breakdown['POINTS'])){
				if(is_array($breakdown['POINTS'])){
					$points_total = array_sum($breakdown['POINTS']);

				}
				if ($points_total < 1 || $points_total=='' || $points_total== NULL){
					$points_total = '0';
				}
			}
			if(isset($breakdown['TOTALS']['TIME'])){
				if(is_array($breakdown['TOTALS']['TIME'])){
					$time_total = array_sum($breakdown['TOTALS']['TIME']);
				}
				if ($time_total < 1 || $time_total=='' || $time_total== NULL){
					$time_total = "0";
				}
			}
			if(isset($breakdown['TOTALS']['DISTANCE'])){
				if(is_array($breakdown['TOTALS']['DISTANCE'])){
					$distance_total = array_sum($breakdown['TOTALS']['DISTANCE']);
				}
				if ($distance_total < 1 || $distance_total=='' || $distance_total== NULL){
					$distance_total = "0";
				}
			}
		}
                $UUID = $_SESSION['UUID'];
                $count_query = MSSQL::query("SELECT COUNT(G_STARTDATE) as COUNT FROM GOALS WHERE G_UUID ='$UUID' AND G_COMPLETED IS NULL AND  G_STARTDATE >= CAST(DATEADD(dd,-7,GETDATE()) AS DATE)");
                $goalCount = odbc_result($count_query, 'COUNT');
                if ($goalCount == 1) {
                    $goalLookup = MSSQL::query("SELECT * FROM GOALS WHERE G_UUID ='$UUID' AND G_COMPLETED IS NULL AND  G_STARTDATE >= CAST(DATEADD(dd,-7,GETDATE()) AS DATE)");
                    $sentence = odbc_result($goalLookup, 'G_ACTIVITY');
                    $frequency = odbc_result($goalLookup, 'G_FREQUENCY');
                    $startdate = odbc_result($goalLookup, 'G_STARTDATE');
                    $enddate = odbc_result($goalLookup, 'G_ENDDATE');
                    $startReadable = date('m/d/Y', strtotime($startdate));
                    $endReadable = date('m/d/Y', strtotime($enddate));
                    $pointQuery = MSSQL::query("SELECT G_POINT_GOAL, G_EARNED FROM GOALS WHERE G_UUID ='$UUID' AND G_COMPLETED IS NULL AND  G_STARTDATE >= CAST(DATEADD(dd,-7,GETDATE()) AS DATE)");
                    $points_goal = odbc_result($pointQuery, 'G_POINT_GOAL');
                    $current_points = odbc_result($pointQuery, 'G_EARNED');
                } else {
                    $points_goal = 1;
                    $current_points = 1;
                    $sentence = '';
                    $frequency = 1;
                }
                $percent_complete = number_format(($current_points / $points_goal) * 100, 0, ".", ",");
		?>


      <!-- Quick Stats Interface -->

      <h2 class="custom-font-small-blue" style="padding:0px 10px 0px 10px;;">Quick Stats</h2>
      <hr style="margin-top:-5px; padding:0px 10px 0px 10px;" />
<?php if (!$goalCount <= 0) { ?>
			<!-- GOALS -->
			<div class="peach-bg-link" id="goals-toggle" style="padding: 10px 20px 13px 20px; margin: 0 0 0 0; cursor:pointer;">
				<div style="display:block; ">
					<h2 class="font -large -secondary -white" style="margin-bottom:-25px;padding: .5rem 0 1rem 0"><?= $current_points; ?> / <?= $points_goal; ?></h2>
					<p class="global-h2-white">Goal Progress: <?= $startReadable ?> - <?= $endReadable ?></p>

					<div class="expand-icon" style="margin-top:-.85em;"><i class="fi-list size-24"></i></div>

				</div>
				<!-- Distance Breakdown -->
				<div id="goals-breakdown" style="display:none;">
					<div class="font -primary -standard -white pt pb1 lh-standard">
                                            <ul>
                                                <li>Primary Activities: <?= ucfirst($sentence) ?></li>
                                                <li>Frequency: <?= $frequency ?> times / week</li>
                                                <li>Progress: <?= $percent_complete ?>%</li>
                                            </ul>
					</div>
					<a href="faq.php#G5" target="_blank" class="button round success tiny">What Does This Mean?</a>

				</div>
				<!-- End Distance Breakdown -->

			</div>
			<!-- End GOALS -->
                        <?php } ?>
         <!-- Points -->
      <div class="blue-bg-link" id="points-toggle" style="padding: 10px 20px 10px 20px; margin: 0 0 0 0; cursor:pointer;">

        <h2 class="global-h2-white"><b></b></h2>
        <h2 class="font -large -secondary -white" style="margin-bottom:-25px; padding: .5rem 0 1rem 0;"><?php echo $points_total; ?></h2>
        <h3 class="global-h2-white" style="margin-top:-20px;">Points Earned</h3>
        <div class="expand-icon" style="margin-top:-1em;"><i class="fi-list size-24"></i></div>

        <!-- Points Breakdown -->
        <div id="points-breakdown" style="display:none;">
          <ul class="global-p-white">
            <?php
				if(is_array($breakdown['POINTS'])){
			  		foreach($breakdown['POINTS'] as $key=>$activity){
						?>
							<li><?php echo ACTIVITY::activity_to_form($key).': '.number_format($activity).' points'; ?></li>
						<?php
					}
				}
			  ?>
          </ul>
          <a href="faq.php#C3" target="_blank" class="button round tiny">What are Points?</a>
        </div>
        <!-- End Points Breakdown -->

      </div>
      <!-- End Points -->

      <!-- Time -->
      <div class="green-bg-link" id="time-toggle" style="padding: 10px 20px 10px 20px; margin: 0 0 0 0; cursor:pointer;">
        <div style="display:block; margin-bottom:-15px;">
          <h3 class="global-h2-white"><b>Time Exercised:</b></h3>
        </div>
        <!-- Hours -->
        <div style="display:inline-block; margin-right:15px;">
          <h2 class="font -large -secondary -white" style="margin-bottom:-25px; padding: .5rem 0 1rem 0;"><?php echo $hours = number_format(floor($time_total/3600)); ?></h2>
          <h3 class="global-h2-white" style="margin-top:-20px;"><b>Hours</b></h3>
        </div>
        <!-- End Hours -->
        <!-- Minutes -->
        <div style="display:inline-block">
          <h2 class="font -large -secondary -white" style="margin-bottom:-25px; padding: .5rem 0 1rem 0;" style="margin-bottom:-20px;"><?php echo $minutes = number_format(($time_total%3600)/60); ?></h2>
          <h3 class="global-h2-white" style="margin-top:-20px;"><b>Minutes</b></h3>
        </div>
        <div class="expand-icon" style="margin-top:0.05em;"><i class="fi-list size-24"></i></div>
        <!-- End Minutes -->

        <!-- Time Breakdown -->
        <div id="time-breakdown" style="display:none;">
          <ul class="global-p-white">
            <?php
			if(is_array($breakdown['ACTIVITY'])){
			  	foreach($breakdown['ACTIVITY'] as $activity){
					if ($activity['TIME'] < 60){
					?>
                    <li><?php echo ACTIVITY::activity_to_form($activity['ID']).': '.number_format($activity['TIME']).'s'; ?></li>
                    <?php
					}else if ($activity['TIME'] < 3600){
					?>
                    <li>
					<?php echo ACTIVITY::activity_to_form($activity['ID']).': '.number_format((($activity['TIME'] % 3600)/60)).'m'; ?>
                    </li>
                    <?php
					}else{
					?>
                    <li>
					<?php echo ACTIVITY::activity_to_form($activity['ID']).': '.number_format(floor($activity['TIME']/3600)).'h '.number_format((($activity['TIME'] % 3600)/60)).'m'; ?>
                    </li>
                    <?php
					}
				}
			}
			  ?>
          </ul>
        </div>
        <!-- End Time Breakdown -->

      </div>
      <!-- End Time -->

      <!-- Distance -->
      <div class="peach-bg-link" id="distance-toggle" style="padding: 10px 20px 10px 20px; margin: 0 0 0 0; cursor:pointer;">
        <div class="expand-icon" style="margin-top:0.2em;"><i class="fi-list size-24"></i></div>
        <div style="display:block; margin-bottom:-15px;">
          <h3 class="global-h2-white"><b>Distance Traveled:</b></h3>
        </div>
        <!-- Miles -->
        <div style="display:inline-block; margin-right:15px;">
          <h2 class="font -large -secondary -white" style="margin-bottom:-25px; padding: .5rem 0 1rem 0;"><?php echo number_format($distance_total); ?></h2>
          <h3 class="global-h2-white" style="margin-top:-20px;"><b>Miles</b></h3>
        </div>
        <!-- End Miles -->
        <!-- Distance Breakdown -->
        <div id="distance-breakdown" style="display:none;">
          <ul class="global-p-white">
            <?php
			if(is_array($breakdown['ACTIVITY'])){
			  	foreach($breakdown['ACTIVITY'] as $activity){
					$aid = $activity['ID'];
					$DA = MSSQL::query('SELECT A_UNIT FROM ACTIVITY WHERE A_ID=\''.$aid.'\'');
					$DA = odbc_result($DA, 'A_UNIT');
					if($DA ==1){
					?>
                    <li><?php echo ACTIVITY::activity_to_form($aid).': '.number_format($activity['UNITS']); ?></li>
                    <?php
					}
				}
			}
			  ?>
          </ul>
          <a href="faq.php#D3" target="_blank" class="button round success tiny">What Does This Mean?</a>
        </div>
        <!-- End Distance Breakdown -->

      </div>
      <!-- End Distance -->

      <!-- End Quick Stats Interface -->

        <?php
	}

	/**
	*	Displays recent activity to a user's home page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function display_activity($id, $num_rows, $params){
            $sql = 'SELECT TOP '.$num_rows.' * FROM LOG WHERE AL_UID = \''.$id.'\''.$params.' ORDER BY AL_DATE DESC';
            $query = MSSQL::query($sql);
            ?>
            <div style="margin-top:20px;">
              <a href="#" id="toggle4"><h2 class="custom-font-small-blue">Past Activity</h2></a><hr />
                <div class="toggle4">
                <?php
                        while(odbc_fetch_row($query)){
                        $aid = odbc_result($query, 'AL_AID');
                                $id = odbc_result($query, 'AL_ID');
                                $a = ACTIVITY::activity_to_form($aid);
                                $time = odbc_result($query, 'AL_TIME');
                                $hours = floor($time/3600);
                                if($hours > 1){
                                        $minutes = ($time%3600)/60;
                                        if($minutes > 1){
                                                $time = $hours.' hours '.$minutes.' minutes';
                                        }else{
                                                $time = $hours.' hours';
                                    }
                                }else if($hours ==1){
                                        $minutes = ($time%3600)/60;
                                    if($minutes > 1){
                                            $time = $hours.' hour '.$minutes.' minutes';
                                    }else{
                                            $time = $hours.' hour';
                                    }
                            }else if($hours<1){
                                    $hours = 0;
                                    $minutes = ($time%3600)/60;
                                    $time = $time/60;
                                    if($time >1){
                                            $time = $minutes.' minutes';
                                    }else if($time ==1){
                                            $time = '1 minute';
                                    }
                            }

                            $sd_num = odbc_result($query, 'AL_SD');
                            $sd = strtolower(ACTIVITY::difficulty_to_form($sd_num));
                            $pa = odbc_result($query, 'AL_PA');
                            $date = odbc_result($query, 'AL_DATE');
                                    $unit = odbc_result($query, 'AL_UNIT');
                                    $date_array = explode('-', odbc_result($query, 'AL_DATE'));
                            $string = '<b>'.$date.'</b> - '.$a.' for '.$time.', and it was '.$sd.'. You earned '.$pa.' points.';
                                    //JOHN EDITS HERE
                            ?>
                <a href="#" <?php
                    if(empty(filter_input(INPUT_GET, 'uid', FILTER_SANITIZE_STRING)) || ($_SESSION['ID'] == filter_input(INPUT_GET, 'uid', FILTER_SANITIZE_STRING))){
                        ?> data-reveal-id="M_<?php echo $id;?>" <?php } ?>onclick="su_magic('<?php echo $aid;?>', '<?php echo $id;?>', '<?php echo $unit;?>');">
                  <div data-alert class="alert-box secondary">
                    <?php echo $string; ?>
                  </div>
                </a>
                <div id="M_<?php echo $id;?>" class="reveal-modal" data-reveal>
                  <a class="close-reveal-modal">&#215;</a>
                  <div class="row">
                    <div class="large-12 columns">
                      <h2 class="global-h2">Edit an Activity</h2>
                      <hr />
                    </div>
                  </div>
                  <form id="<?php echo $id;?>" action="edit_activity.php" method="post" data-abide>
                    <div class="row">
                      <div class="large-6 columns">
                            <div class="row" id="dp3">

               <!-- Month -->
                 <div class="large-4 columns">
                   <label>Month
                     <select id="month" name="month" required>
                       <option value="1" <?php if ($date_array[1]=='1'){echo 'selected="selected"';} ?>>01 January</option>
                       <option value="2" <?php if ($date_array[1]=='2'){echo 'selected="selected"';} ?>>02 February</option>
                       <option value="3" <?php if ($date_array[1]=='3'){echo 'selected="selected"';} ?>>03 March</option>
                       <option value="4" <?php if ($date_array[1]=='4'){echo 'selected="selected"';} ?>>04 April</option>
                       <option value="5" <?php if ($date_array[1]=='5'){echo 'selected="selected"';} ?>>05 May</option>
                       <option value="6" <?php if ($date_array[1]=='6'){echo 'selected="selected"';} ?>>06 June</option>
                       <option value="7" <?php if ($date_array[1]=='7'){echo 'selected="selected"';} ?>>07 July</option>
                       <option value="8" <?php if ($date_array[1]=='8'){echo 'selected="selected"';} ?>>08 August</option>
                       <option value="9" <?php if ($date_array[1]=='9'){echo 'selected="selected"';} ?>>09 September</option>
                       <option value="10" <?php if ($date_array[1]=='10'){echo 'selected="selected"';} ?>>10 October</option>
                       <option value="11" <?php if ($date_array[1]=='11'){echo 'selected="selected"';} ?>>11 November</option>
                       <option value="12" <?php if ($date_array[1]=='12'){echo 'selected="selected"';} ?>>12 December</option>
                     </select>
                   </label>
                 </div>
               <!-- End Month -->

               <!-- Day -->
                 <div class="large-4 columns">
                   <label>Day
                      <select id="day" name="day" required>
                       <option value="1" <?php if ($date_array[2]=='1'){echo 'selected="selected"';} ?>>01</option>
                       <option value="2" <?php if ($date_array[2]=='2'){echo 'selected="selected"';} ?>>02</option>
                       <option value="3" <?php if ($date_array[2]=='3'){echo 'selected="selected"';} ?>>03</option>
                       <option value="4" <?php if ($date_array[2]=='4'){echo 'selected="selected"';} ?>>04</option>
                       <option value="5" <?php if ($date_array[2]=='5'){echo 'selected="selected"';} ?>>05</option>
                       <option value="6" <?php if ($date_array[2]=='6'){echo 'selected="selected"';} ?>>06</option>
                       <option value="7" <?php if ($date_array[2]=='7'){echo 'selected="selected"';} ?>>07</option>
                       <option value="8" <?php if ($date_array[2]=='8'){echo 'selected="selected"';} ?>>08</option>
                       <option value="9" <?php if ($date_array[2]=='9'){echo 'selected="selected"';} ?>>09</option>
                       <option value="10" <?php if ($date_array[2]=='10'){echo 'selected="selected"';} ?>>10</option>
                       <option value="11" <?php if ($date_array[2]=='11'){echo 'selected="selected"';} ?>>11</option>
                       <option value="12" <?php if ($date_array[2]=='12'){echo 'selected="selected"';} ?>>12</option>
                       <option value="13" <?php if ($date_array[2]=='13'){echo 'selected="selected"';} ?>>13</option>
                       <option value="14" <?php if ($date_array[2]=='14'){echo 'selected="selected"';} ?>>14</option>
                       <option value="15" <?php if ($date_array[2]=='15'){echo 'selected="selected"';} ?>>15</option>
                       <option value="16" <?php if ($date_array[2]=='16'){echo 'selected="selected"';} ?>>16</option>
                       <option value="17" <?php if ($date_array[2]=='17'){echo 'selected="selected"';} ?>>17</option>
                       <option value="18" <?php if ($date_array[2]=='18'){echo 'selected="selected"';} ?>>18</option>
                       <option value="19" <?php if ($date_array[2]=='19'){echo 'selected="selected"';} ?>>19</option>
                       <option value="20" <?php if ($date_array[2]=='20'){echo 'selected="selected"';} ?>>20</option>
                       <option value="21" <?php if ($date_array[2]=='21'){echo 'selected="selected"';} ?>>21</option>
                       <option value="22" <?php if ($date_array[2]=='22'){echo 'selected="selected"';} ?>>22</option>
                       <option value="23" <?php if ($date_array[2]=='23'){echo 'selected="selected"';} ?>>23</option>
                       <option value="24" <?php if ($date_array[2]=='24'){echo 'selected="selected"';} ?>>24</option>
                       <option value="25" <?php if ($date_array[2]=='25'){echo 'selected="selected"';} ?>>25</option>
                       <option value="26" <?php if ($date_array[2]=='26'){echo 'selected="selected"';} ?>>26</option>
                       <option value="27" <?php if ($date_array[2]=='27'){echo 'selected="selected"';} ?>>27</option>
                       <option value="28" <?php if ($date_array[2]=='28'){echo 'selected="selected"';} ?>>28</option>
                       <option value="29" <?php if ($date_array[2]=='29'){echo 'selected="selected"';} ?>>29</option>
                       <option value="30" <?php if ($date_array[2]=='30'){echo 'selected="selected"';} ?>>30</option>
                       <option value="31" <?php if ($date_array[2]=='31'){echo 'selected="selected"';} ?>>31</option>
                     </select>
                   </label>
                 </div>
               <!-- End Day -->

               <!-- Year -->
                 <div class="large-4 columns">
                   <label>Year
                     <select id="year" name="year" required>
                       <option value="2008" <?php if ($date_array[0]=='2008'){echo 'selected="selected"';} ?>>2008</option>
                       <option value="2009" <?php if ($date_array[0]=='2009'){echo 'selected="selected"';} ?>>2009</option>
                       <option value="2010" <?php if ($date_array[0]=='2010'){echo 'selected="selected"';} ?>>2010</option>
                       <option value="2011" <?php if ($date_array[0]=='2011'){echo 'selected="selected"';} ?>>2011</option>
                       <option value="2012" <?php if ($date_array[0]=='2012'){echo 'selected="selected"';} ?>>2012</option>
                       <option value="2013" <?php if ($date_array[0]=='2013'){echo 'selected="selected"';} ?>>2013</option>
                       <option value="2014" <?php if ($date_array[0]=='2014'){echo 'selected="selected"';} ?>>2014</option>
                       <option value="2015" <?php if ($date_array[0]=='2015'){echo 'selected="selected"';} ?>>2015</option>
                       <option value="2016" <?php if ($date_array[0]=='2016'){echo 'selected="selected"';} ?>>2016</option>
                       <option value="2017" <?php if ($date_array[0]=='2017'){echo 'selected="selected"';} ?>>2017</option>
                     </select>
                   </label>
                 </div>
               <!-- End Year -->
               </div>

                      </div>
                    </div>
                    <div class="row">
                      <div class="large-6 columns">
                        <label>Activity:
                          <select id="exercise" name="srch" onchange="su_magic(this, '<?php echo $id;?>');">
                          <?php
                                        $list = ACTIVITY::select_activity($aid);
                                  ?>
                          </select>
                        </label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="large-12 columns">
                        <p class="global-p">Time Spent Exercising:</p>
                        <hr style="margin-top:-5px; margin-bottom:5px;" />
                      </div>
                    </div>
                    <div class="row">
                      <div class="large-5 columns">
                        <label>Minutes:
                          <input type="number" id="minutes" name ="minutes" value="<?php echo odbc_result($query, 'AL_TIME')/60; ?>" min="1" max="480" onchange="return checkMinutes(this)" title="Enter the number of minutes exercised for this activity. Limit is 8 hours (480 minutes)." required pattern="^([1-9]|[0-9][0-9]|[1-3][0-9][0-9]|4[0-7][0-9]|480)$">
                        </label>
                      </div>
                    </div>
                    <div class="row" id="su<?php echo $id;?>" name = "su<?php echo $id;?>">

                    </div>
                    <div class="row">
                      <div class="large-12 columns">
                        <!-- I would just copy this and some of the other elements over from the existing form
                        This is really just a placeholder for you -->
                        <label>How difficult was it to complete?
                          <select name="sd" id="sd">
                            <option value="1" <?php if($sd_num == '1'){echo 'selected="selected"';} ?>>Easy</option>
                            <option value="2" <?php if($sd_num == '2'){echo 'selected="selected"';} ?>>Somewhat difficult</option>
                            <option value="3" <?php if($sd_num == '3'){echo 'selected="selected"';} ?>>Difficult</option>
                            <option value="4" <?php if($sd_num == '4'){echo 'selected="selected"';} ?>>More difficult than usual</option>
                            <option value="5" <?php if($sd == '5'){echo 'selected="selected"';} ?>>Very difficult</option>
                          </select>
                        </label>

                      </div>
                    </div>
                    <div class="row">
                      <div class="large-12 columns">
                            <input type="hidden" id="activity_log_id" name="activity_log_id" value="<?php echo $id; ?>" />
                        <input type="submit"  id="submit" class="tiny button" value="Submit"/> <a href="delete_activity.php?aid=<?php echo $id;?>" class="button tiny alert">Delete Activity</a>
                      </div>
                    </div>
                  </form>

                </div>
            <?php
			        //END EDITS HERE
		}
		?>
        <a href="past-activity.php" class="button tiny">Show More Past Activity</a>

      </div>
    </div>
    <?php
	}

	/**
	*	Displays recent activity to a user's home page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function display_activity_table($uid){
		$sql = 'SELECT * FROM LOG WHERE AL_UID = \''.$uid.'\' ORDER BY AL_DATE DESC';
		$query = MSSQL::query($sql);
		?>
        <div style="margin-top:20px;">
          <a href="#" id="toggle4"><h2 class="custom-font-small-blue">Past Activity:</h2></a><hr />
          <p class="global-p">Clicking on the top of each columns will arrange the results by ascending or descending order.  You can also search directly for an activity.  If you need to edit an activity, simply press the "edit" button and submit your changes.</p>
            <div class="toggle4">
              <table id='' class='display'>
                <thead>
                  <th>
                    Activity
                  </th>
                  <th>
                    Time(Minutes)
                  </th>
                  <th>
                    Points
                  </th>
                  <th>
                    Difficulty
                  </th>
                  <th>
                    Date
                  </th>
                  <?php if($_SESSION['ID'] == $uid){?>
                  <th>

                  </th>
                  <?php } ?>
                </thead>
            <?php
		    while(odbc_fetch_row($query)){
    		    $aid = odbc_result($query, 'AL_AID');
			    $id = odbc_result($query, 'AL_ID');
			    $a = ACTIVITY::activity_to_form($aid);
			    $time =floor( odbc_result($query, 'AL_TIME')/60);
			    $hours = number_format($time/3600, 2, '.', '');
    			$sd_num = odbc_result($query, 'AL_SD');
    			$sd = strtolower(ACTIVITY::difficulty_to_form($sd_num));
    			$pa = odbc_result($query, 'AL_PA');
    			$date = odbc_result($query, 'AL_DATE');
				$date_array = explode('-', odbc_result($query, 'AL_DATE'));
			        //JOHN EDITS HERE
			?>
                <tr>
                  <td>
                    <?php echo $a; ?>
                  </td>
                  <td>
                    <?php echo $time; ?>
                  </td>
                  <td>
                    <?php echo $pa; ?>
                  </td>
                  <td>
                    <?php echo $sd; ?>
                  </td>
                  <td>
                    <?php echo $date; ?>
                  </td>
                  <?php if($_SESSION['ID'] == $uid){?>
                  <td class="center">
                    <a href="#" data-reveal-id="M_<?php echo $id;?>" onclick="su_magic('<?php echo $aid;?>', '<?php echo $id;?>');">
                      <div style="margin-top:20px;" data-alert class="tiny round button ">
                        Edit
              		  </div>
            		</a>
                  </td>
                  <?php } ?>
                </tr>

            <?php if($_SESSION['ID'] == $uid){?>
            <div id="M_<?php echo $id;?>" class="reveal-modal" data-reveal>
              <a class="close-reveal-modal">&#215;</a>
              <div class="row">
                <div class="large-12 columns">
                  <h2 class="global-h2">Edit an Activity</h2>
                  <hr />
                </div>
              </div>
              <form id="<?php echo $id;?>" action="edit_activity.php" method="post" data-abide>
                <div class="row">
                  <div class="large-6 columns">
                	<div class="row" id="dp3">

           <!-- Month -->
             <div class="large-4 columns">
               <label>Month
                 <select id="month" name="month" required>
                   <option value="1" <?php if ($date_array[1]=='1'){echo 'selected="selected"';} ?>>01 January</option>
                   <option value="2" <?php if ($date_array[1]=='2'){echo 'selected="selected"';} ?>>02 February</option>
                   <option value="3" <?php if ($date_array[1]=='3'){echo 'selected="selected"';} ?>>03 March</option>
                   <option value="4" <?php if ($date_array[1]=='4'){echo 'selected="selected"';} ?>>04 April</option>
                   <option value="5" <?php if ($date_array[1]=='5'){echo 'selected="selected"';} ?>>05 May</option>
                   <option value="6" <?php if ($date_array[1]=='6'){echo 'selected="selected"';} ?>>06 June</option>
                   <option value="7" <?php if ($date_array[1]=='7'){echo 'selected="selected"';} ?>>07 July</option>
                   <option value="8" <?php if ($date_array[1]=='8'){echo 'selected="selected"';} ?>>08 August</option>
                   <option value="9" <?php if ($date_array[1]=='9'){echo 'selected="selected"';} ?>>09 September</option>
                   <option value="10" <?php if ($date_array[1]=='10'){echo 'selected="selected"';} ?>>10 October</option>
                   <option value="11" <?php if ($date_array[1]=='11'){echo 'selected="selected"';} ?>>11 November</option>
                   <option value="12" <?php if ($date_array[1]=='12'){echo 'selected="selected"';} ?>>12 December</option>
                 </select>
               </label>
             </div>
           <!-- End Month -->

           <!-- Day -->
             <div class="large-4 columns">
               <label>Day
                  <select id="day" name="day" required>
                   <option value="1" <?php if ($date_array[2]=='1'){echo 'selected="selected"';} ?>>01</option>
                   <option value="2" <?php if ($date_array[2]=='2'){echo 'selected="selected"';} ?>>02</option>
                   <option value="3" <?php if ($date_array[2]=='3'){echo 'selected="selected"';} ?>>03</option>
                   <option value="4" <?php if ($date_array[2]=='4'){echo 'selected="selected"';} ?>>04</option>
                   <option value="5" <?php if ($date_array[2]=='5'){echo 'selected="selected"';} ?>>05</option>
                   <option value="6" <?php if ($date_array[2]=='6'){echo 'selected="selected"';} ?>>06</option>
                   <option value="7" <?php if ($date_array[2]=='7'){echo 'selected="selected"';} ?>>07</option>
                   <option value="8" <?php if ($date_array[2]=='8'){echo 'selected="selected"';} ?>>08</option>
                   <option value="9" <?php if ($date_array[2]=='9'){echo 'selected="selected"';} ?>>09</option>
                   <option value="10" <?php if ($date_array[2]=='10'){echo 'selected="selected"';} ?>>10</option>
                   <option value="11" <?php if ($date_array[2]=='11'){echo 'selected="selected"';} ?>>11</option>
                   <option value="12" <?php if ($date_array[2]=='12'){echo 'selected="selected"';} ?>>12</option>
                   <option value="13" <?php if ($date_array[2]=='13'){echo 'selected="selected"';} ?>>13</option>
                   <option value="14" <?php if ($date_array[2]=='14'){echo 'selected="selected"';} ?>>14</option>
                   <option value="15" <?php if ($date_array[2]=='15'){echo 'selected="selected"';} ?>>15</option>
                   <option value="16" <?php if ($date_array[2]=='16'){echo 'selected="selected"';} ?>>16</option>
                   <option value="17" <?php if ($date_array[2]=='17'){echo 'selected="selected"';} ?>>17</option>
                   <option value="18" <?php if ($date_array[2]=='18'){echo 'selected="selected"';} ?>>18</option>
                   <option value="19" <?php if ($date_array[2]=='19'){echo 'selected="selected"';} ?>>19</option>
                   <option value="20" <?php if ($date_array[2]=='20'){echo 'selected="selected"';} ?>>20</option>
                   <option value="21" <?php if ($date_array[2]=='21'){echo 'selected="selected"';} ?>>21</option>
                   <option value="22" <?php if ($date_array[2]=='22'){echo 'selected="selected"';} ?>>22</option>
                   <option value="23" <?php if ($date_array[2]=='23'){echo 'selected="selected"';} ?>>23</option>
                   <option value="24" <?php if ($date_array[2]=='24'){echo 'selected="selected"';} ?>>24</option>
                   <option value="25" <?php if ($date_array[2]=='25'){echo 'selected="selected"';} ?>>25</option>
                   <option value="26" <?php if ($date_array[2]=='26'){echo 'selected="selected"';} ?>>26</option>
                   <option value="27" <?php if ($date_array[2]=='27'){echo 'selected="selected"';} ?>>27</option>
                   <option value="28" <?php if ($date_array[2]=='28'){echo 'selected="selected"';} ?>>28</option>
                   <option value="29" <?php if ($date_array[2]=='29'){echo 'selected="selected"';} ?>>29</option>
                   <option value="30" <?php if ($date_array[2]=='30'){echo 'selected="selected"';} ?>>30</option>
                   <option value="31" <?php if ($date_array[2]=='31'){echo 'selected="selected"';} ?>>31</option>
                 </select>
               </label>
             </div>
           <!-- End Day -->

           <!-- Year -->
             <div class="large-4 columns">
               <label>Year
                 <select id="year" name="year" required>
                   <option value="2008" <?php if ($date_array[0]=='2008'){echo 'selected="selected"';} ?>>2008</option>
                   <option value="2009" <?php if ($date_array[0]=='2009'){echo 'selected="selected"';} ?>>2009</option>
                   <option value="2010" <?php if ($date_array[0]=='2010'){echo 'selected="selected"';} ?>>2010</option>
                   <option value="2011" <?php if ($date_array[0]=='2011'){echo 'selected="selected"';} ?>>2011</option>
                   <option value="2012" <?php if ($date_array[0]=='2012'){echo 'selected="selected"';} ?>>2012</option>
                   <option value="2013" <?php if ($date_array[0]=='2013'){echo 'selected="selected"';} ?>>2013</option>
                   <option value="2014" <?php if ($date_array[0]=='2014'){echo 'selected="selected"';} ?>>2014</option>
                   <option value="2015" <?php if ($date_array[0]=='2015'){echo 'selected="selected"';} ?>>2015</option>
                   <option value="2016" <?php if ($date_array[0]=='2016'){echo 'selected="selected"';} ?>>2016</option>
                   <option value="2017" <?php if ($date_array[0]=='2017'){echo 'selected="selected"';} ?>>2017</option>
                 </select>
               </label>
             </div>
           <!-- End Year -->
           </div>

                  </div>
                </div>
                <div class="row">
                  <div class="large-6 columns">
                    <label>Activity:
                      <select id="exercise" name="srch" onchange="su_magic(this, '<?php echo $id;?>');">
                      <?php
    				    $list = ACTIVITY::select_activity($aid);
    			      ?>
                      </select>
                    </label>
                  </div>
                </div>
                <div class="row">
                  <div class="large-12 columns">
                    <p class="global-p">Time Spent Exercising:</p>
                    <hr style="margin-top:-5px; margin-bottom:5px;" />
                  </div>
                </div>
                <div class="row">
                  <div class="large-6 columns">
                    <label>Minutes:
                      <input type="number" id="minutes" name ="minutes"  min="1" max="480" onchange="return checkMinutes(this)" title="Enter the number of minutes exercised for this activity. Limit is 8 hours (480 minutes)." required pattern="^([1-9]|[0-9][0-9]|[1-3][0-9][0-9]|4[0-7][0-9]|480)$">
                    </label>
                  </div>
                </div>
                <div class="row" id="su<?php echo $id;?>" name = "su<?php echo $id;?>">

                </div>
                <div class="row">
                  <div class="large-12 columns">
                    <!-- I would just copy this and some of the other elements over from the existing form
                    This is really just a placeholder for you -->
                    <label>How difficult was it to complete?
                      <select name="sd" id="sd">
                        <option value="1" <?php if($sd_num == '1'){echo 'selected="selected"';} ?>>Easy</option>
                        <option value="2" <?php if($sd_num == '2'){echo 'selected="selected"';} ?>>Somewhat difficult</option>
                        <option value="3" <?php if($sd_num == '3'){echo 'selected="selected"';} ?>>Difficult</option>
                        <option value="4" <?php if($sd_num == '4'){echo 'selected="selected"';} ?>>More difficult than usual</option>
                        <option value="5" <?php if($sd == '5'){echo 'selected="selected"';} ?>>Very difficult</option>
                      </select>
                    </label>

                  </div>
                </div>
                <div class="row">
                  <div class="large-12 columns">
                  	<input type="hidden" id="activity_log_id" name="activity_log_id" value="<?php echo $id; ?>" />
                    <input type="submit"  id="submit" class="tiny button" value="Submit"/> <a href="delete_activity.php?aid=<?php echo $id;?>" class="button tiny alert">Delete Activity</a>
                  </div>
                </div>
              </form>
            </div>
        <?php
			}
			        //END EDITS HERE
		}
		?>
        </table>
      </div>
    </div>
    <?php
	}

	/**
	*	Converts the db int identity of an activity into English
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function activity_to_form($aid){
		$sql = 'SELECT A_NAME FROM ACTIVITY WHERE A_ID=\''.$aid.'\'';
		$query = MSSQL::query($sql);
		$a_name = odbc_result($query, 'A_NAME');
		return $a_name;
	}

	/**
	*	takes an Activity ID and returns true if that activity is distance based, false otherwise
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function is_distance_based($aid){
		$sql = 'SELECT A_UNIT FROM ACTIVITY WHERE A_ID=\''.$aid.'\'';
		$query = MSSQL::query($sql);
		if(odbc_result($query, 'A_UNIT')==1){
			return true;
		}else{
			return false;
		}
	}

	/**
	*	Adds units to the end of primary metrics in the logged activity
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function add_distance($aid){
		$aid = '';
		switch($aid){
			case '1':
			$aid = 'miles';
			break;
			case '2':
			$aid = 'miles';
			break;
			case '3':
			$aid = 'miles';
			break;
			case '4':
			$aid = 'minutes';
			break;
		}
		return $aid;
	}

	/**
	*	Adds units to the end of secondary metrics in the logged activity
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function add_secondary($aid){
		switch($aid){
			case '1':
			$aid = 'minutes';
			break;
			case '2':
			$aid = 'minutes';
			break;
			case '3':
			$aid = 'seconds';
			break;
			case '4':
			$aid = 'minutes';
			break;
			case '5':
			$aid = 'minutes';
			break;
			case '6':
			$aid = 'minutes';
			break;
			case '7':
			$aid = 'minutes';
			break;
			case '8':
			$aid = 'minutes';
			break;
			case '9':
			$aid = 'minutes';
			break;
			case '10':
			$aid = 'minutes';
			break;
			case '11':
			$aid = 'minutes';
			break;
			case '12':
			$aid = 'lbs';
			break;
			case '13':
			$aid = 'minutes';
			break;
		}
		return $aid;
	}

	/**
	*	Converts the db int identity of an difficulty into English
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function difficulty_to_form($sd){
		switch($sd){
			case '1':
			$aid = 'Easy';
			break;
			case '2':
			$aid = 'Somewhat Difficult';
			break;
			case '3':
			$aid = 'Difficult';
			break;
			case '4':
			$aid = 'More difficult than usual';
			break;
			case '5':
			$aid = 'Very difficult';
			break;
		}
		return $aid;
	}


}
?>
