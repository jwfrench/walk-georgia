<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/sql-config.php");
include_once(ROOT."/lib/back_api.php");
/**
*
*	WALK GA WEB GROUP MANAGER
*
*	MANAGES LOGIN, LOGOUT, REGISTER, VALIDATION, AND ACCOUNT ACCESS
*
*	@author	 Aaron McCoy <herbo4@uga.edu>
*	@copyright	 Copyright (c) 2014, University of Georgia
*	@package	 WalkGA
*	@category	 Back End
*	@version	1.0
*
*/
class GROUP{
	/**
	*	Creates a valid group
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function create($PARENT_ID, $DESC, $NAME, $VISIBILITY, $META, $PERMISSIONS, $ISROOT, $COUNTY, $GROUP_LOGGING){
		$DESC = str_replace('\'', '\'\'', $DESC);
		$NAME = str_replace('\'', '\'\'', $NAME);
		$META = str_replace('\'', '\'\'', $META);
		$sql = "INSERT INTO GROUPS (G_PID, G_DESC, G_NAME, G_VIS, G_META, G_PERMISSIONS, G_ISROOTGROUP, G_GLOG) OUTPUT INSERTED.G_ID VALUES ('$PARENT_ID', '$DESC', '$NAME', '$VISIBILITY', '$META', '$PERMISSIONS', '$ISROOT', '$GROUP_LOGGING')";

                $query = MSSQL::query($sql);
		$GROUP_ID = odbc_result($query, 'G_ID');

		$join_group = GROUP::join_group($GROUP_ID, $_SESSION['ID']);
		$sql = 'UPDATE GROUP_MEMBER SET IS_ADMIN = \'1\' WHERE G_ID=\''.$GROUP_ID.'\' AND U_ID=\''.$_SESSION['ID'].'\'';
		$query = MSSQL::query($sql);
		return $GROUP_ID;
	}

	/**
	*	Creates a valid group
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function create_sub($PARENT_ID, $DESC, $NAME, $VISIBILITY, $META, $PERMISSIONS, $ISROOT, $COUNTY, $GROUP_LOGGING){
		$DESC = str_replace('\'', '\'\'', $DESC);
		$NAME = str_replace('\'', '\'\'', $NAME);
		$META = str_replace('\'', '\'\'', $META);
		$sql = "INSERT INTO GROUPS (G_PID, G_DESC, G_NAME, G_VIS, G_META, G_PERMISSIONS, G_ISROOTGROUP, G_COUNTY, G_GLOG) OUTPUT INSERTED.G_ID VALUES ('$PARENT_ID', '$DESC', '$NAME', '$VISIBILITY', '$META', '$PERMISSIONS', '0', '$COUNTY', '$GROUP_LOGGING')";
		$query = MSSQL::query($sql);
		$GROUP_ID = odbc_result($query, 'G_ID');
		$join_group = GROUP::join_group($GROUP_ID, $_SESSION['ID']);
		$sql = 'UPDATE GROUP_MEMBER SET IS_ADMIN = \'1\' WHERE G_ID=\''.$GROUP_ID.'\' AND U_ID=\''.$_SESSION['ID'].'\'';
		$query = MSSQL::query($sql);
		return $GROUP_ID;
	}

	/**
	*	Gets the info for a group respective to Group ID
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function get_info($G_ID){
		$execute = MSSQL::query('SELECT * FROM GROUPS WHERE G_ID=\''.$G_ID.'\'');
		if (odbc_num_rows($execute) == 1) {
		$info['G_PID'] = odbc_result($execute, 'G_PID');
		$info['G_DESC'] = odbc_result($execute, 'G_DESC');
		$info['G_NAME'] = odbc_result($execute, 'G_NAME');
		$info['G_VIS'] = odbc_result($execute, 'G_VIS');
		$info['G_META'] = odbc_result($execute, 'G_META');
		$info['G_COUNTY'] = odbc_result($execute, 'G_COUNTY');
		$info['G_GLOG'] = odbc_result($execute, 'G_GLOG');
		$info['G_PERMISSIONS'] = odbc_result($execute, 'G_PERMISSIONS');
		$info['G_ISROOTGROUP'] = odbc_result($execute, 'G_ISROOTGROUP');
		$info['G_UUID'] = odbc_result($execute, 'G_UUID');
                $info['G_DATE_CREATED'] = odbc_result($execute, 'G_DATE_CREATED');
		$execute = MSSQL::query("SELECT * FROM GROUP_MEMBER WHERE G_ID='$G_ID'");
		$info['G_NUM_USERS'] = odbc_num_rows($execute);
		}else{
			$info = false;
		}
		return $info;
	}

	/**
	*	Edits a valid group
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function edit($G_ID, $DESC, $NAME, $VISIBILITY){
		$sql="UPDATE GROUPS SET G_DESC='$DESC', G_NAME='$NAME', G_VIS='$VISIBILITY' WHERE G_ID='$G_ID'";
		$query = MSSQL::query($sql);
	}

	/**
	*	Returns true if a group exists
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function exists($G_ID){
		$sql='SELECT * FROM GROUPS WHERE G_ID=\''.$G_ID.'\'';
		$query = MSSQL::query($sql);
		return odbc_num_rows($query);
	}

	/**
	*	Displays a group's image to the page
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function avatar_large($ID){
		  if (file_exists(ROOT."/group/img/$ID.png")) {
				$filename = '/group/img/'.$ID.'.png';
		  } else {
				$filename = '/img/default-group-profile.png';
		  }
		  ?>
			<a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/'; ?>group/groups.php?group=<?php echo $ID; ?>">
				<div class="avatar-large" style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat; background-color:#008CBA;">
				</div>
			</a>
		  <?php

		  return $filename;
	}

	/**
	*	Displays a group's image to the page
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function avatar_medium($ID){
		  if (file_exists(ROOT."/group/img/$ID.png")) {
				$filename = '/group/img/'.$ID.'.png';
		  } else {
				$filename = '/img/default-group-profile.png';
		  }
		  ?>
			<a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/'; ?>group/groups.php?group=<?php echo $ID; ?>">
				<div class="avatar-favorite" style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat;background-color:#008CBA;">
				</div>
			</a>
		  <?php

		  return $filename;
	}

	/**
	*	Displays a group's image to the page
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function avatar_small($ID){
		  if (file_exists(ROOT."/group/img/$ID.png")) {
				$filename = '/group/img/'.$ID.'.png';
		  } else {
				$filename = '/img/default-group-profile.png';
		  }
		  ?>
			<a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/'; ?>group/groups.php?group=<?php echo $ID; ?>">
				<div class="avatar-small" style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat;background-color:#008CBA;">
				</div>
			</a>
		  <?php

		  return $filename;
	}

	/**
	*	Deletes a group
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function delete($GROUP_ID){
		$subs = array();
		$subs = GROUP::getSubs($GROUP_ID, $subs);
                $delete = MSSQL::query("SELECT G_NAME FROM GROUPS WHERE G_ID ='".$GROUP_ID."'");
		$name = odbc_result($delete, 'G_NAME');
		$query = MSSQL::query("DELETE FROM GROUPS WHERE G_ID='$GROUP_ID'; DELETE FROM GROUPS WHERE G_ID='".implode("' OR G_ID = '", $subs)."';");
                $msg = 'del='.$name;
                REDIRECT::group_list_del($msg);
	}

	/**
	*	Creates a record that ties a user to a group
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function join_group($GROUP_ID, $ID){
                //record number of group members before anything else
                $member_array = [];
                GROUP::getAllMembers($GROUP_ID, $member_array);

		//If they have already joined the group do nothing
		$joined = GROUP::hasJoined($GROUP_ID, $ID);
		if($joined){
			//DO NOTHING
		}
                else{
			//check if the group has metadata associated with it
			$hasMetadata = MSSQL::query("SELECT G_META FROM GROUPS WHERE G_ID ='".$GROUP_ID."'");
			$Metadata = odbc_result($hasMetadata, 'G_META');
			if($Metadata){
				$info = GROUP::get_info($GROUP_ID);
				//if they are joining and the group has meta data, check for meta data posted from a form or gotten from a URL
				$meta = '';
				for($i=0;$i<150;$i++){
					if(isset($_POST['meta_'.$i])){
						$meta .=$_POST['meta_'.$i].":";
					}
					if(isset($_GET['meta_'.$i])){
						$meta .=$_GET['meta_'.$i].":";
					}
				}
				//if this is a subgroup, then meta data might be stored already in the parent group
				if(!$info['G_ISROOTGROUP'] && $meta ==''){;
					$sql_meta = 'SELECT META FROM GROUP_MEMBER WHERE U_ID=\''.$_SESSION['ID'].'\' AND G_ID IN (SELECT G_ID FROM GROUPS WHERE G_UUID = \''.$info['G_PID'].'\') ';
					$meta_query = MSSQL::query($sql_meta);
					$meta = odbc_result($meta_query, 'META');
				}
				//if they have meta data submitted, log it
				if($meta !=''){
					$meta = rtrim($meta, ':');
					$sql = "INSERT INTO GROUP_MEMBER (G_ID, U_ID, META) VALUES ('$GROUP_ID', '$ID', '$meta')";
					$query = MSSQL::query($sql);
					$join_parents = GROUP::join_parents($GROUP_ID, $ID);
				}
			}
                        else{
				//otherwise sign them up without meta data
				$sql = "INSERT INTO GROUP_MEMBER (G_ID, U_ID) VALUES ('$GROUP_ID', '$ID')";
				$query = MSSQL::query($sql);
				$join_parents = GROUP::join_parents($GROUP_ID, $ID);
			}
		}
    //check to see if group has members; if not, make user admin
    if(count($member_array) == 1){
        $sql = 'UPDATE GROUP_MEMBER SET IS_ADMIN = \'1\' WHERE G_ID=\''.$GROUP_ID.'\' AND U_ID=\''.$ID.'\'';
        $query = MSSQL::query($sql);
    }
	}

	/**
	*	Joins all parents of the group a user is joining
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function join_parents($G_ID, $ID){
		$info = GROUP::get_info($G_ID);
		while($info['G_ISROOTGROUP']!='1'){
			$PARENT_ID = odbc_result(MSSQL::query('SELECT G_ID FROM GROUPS WHERE G_UUID=\''.$info['G_PID'].'\''), 'G_ID');
			$join = GROUP::join_group($PARENT_ID, $ID);
			$info = GROUP::get_info($PARENT_ID);
		}
	}

	/**
	*	Destroys the record that ties a user to a group
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function leave_group($GROUP_ID, $ID){
		$query = MSSQL::query("DELETE FROM GROUP_MEMBER WHERE U_ID='$ID' AND G_ID='$GROUP_ID'");
		//$go_to_group = REDIRECT::group($GROUP_ID);
	}

	/**
	*	Parses the Meta-Data headers into form inputs
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function meta_form($G_META){
		$headers = explode(':', $G_META);
		$i = 0;
		foreach ($headers as $header){
			?>
			<div class="large-12 columns">
			  <label><?php echo $header; ?>:<input type="text" id="meta_<?php echo $i; ?>" name="meta_<?php echo $i; ?>" required/></label>
			  <?php $i++; ?>
			</div>
			<?php
		}
		return true;
	}

	/**
	*	Checks to see if a user has joined a group respective to user and group ID
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function hasJoined($GROUP_ID, $ID){
		$query = MSSQL::query("SELECT * FROM GROUP_MEMBER WHERE G_ID ='".$GROUP_ID."' AND U_ID='".$ID."'");
		$num = odbc_num_rows($query);
		return $num;
	}

	/**
	*	Checks to see if a user has joined and is an admin group respective to user and group ID
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function isAdmin($GROUP_ID){
		if(isset($_SESSION['ID'])){
                        //get the account info to see if this is a web admin
			$info = ACCOUNT::get_info($_SESSION['ID']);
			$isWebAdmin = $info['IS_ADMIN'];
                        //see if the user is the parent of the group
			$sql = "SELECT G_PID FROM GROUPS WHERE G_ID ='".$GROUP_ID."'";
			$query = MSSQL::query($sql);
			$G_PID = odbc_result($query, 'G_PID');
                        //if they are the parent but are not an admin, set that status now
			if($G_PID == $_SESSION['UUID']){
				$sql = "UPDATE GROUP_MEMBER SET IS_ADMIN='1' WHERE G_ID ='".$GROUP_ID."' AND U_ID='".$_SESSION['ID']."'";
				$query = MSSQL::query($sql);
			}
                        //see if they are directly an admin of the group
			$sql = "SELECT IS_ADMIN FROM GROUP_MEMBER WHERE G_ID ='".$GROUP_ID."' AND U_ID='".$_SESSION['ID']."'";
			$query = MSSQL::query($sql);
			$isAdmin = odbc_result($query, 1);
			if($isAdmin>0){
				return $isAdmin;
			}else if($isWebAdmin){
				return $isWebAdmin;
			}
		}else{
			return false;
		}
	}

	/**
	*	Returns an array listing of all sub-groups of the provided group
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function getSubs($GROUP_ID, $sub_array){
		$subs_query = 'SELECT G_ID FROM GROUPS WHERE G_PID IN (SELECT G_UUID FROM GROUPS WHERE G_ID=\''.$GROUP_ID.'\');';
		$subs = MSSQL::query($subs_query);
		while(odbc_fetch_row($subs)){
			$sub_array[] = odbc_result($subs, 'G_ID');
			$sub_array = GROUP::getSubs(odbc_result($subs, 'G_ID'), $sub_array);
		}
		return $sub_array;
	}

	/**
	*	Gets the total points, time, and distance of a group and its sub groups as well as their details, without breaking the server
	*	Returns an array that contains points, time, and distance arranged into smaller arrays
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function getBreakdown($GROUP_ID){
		//now we need to tabluate all the subgroups in order to include them in this calculation
		$subs = array();
		$breakdown = array();
		$members = array();
		$subs = GROUP::getSubs($GROUP_ID, $subs);
		$members = GROUP::getAllMembers($GROUP_ID, $members);
		$breakdown = GROUP::getTotalGroupPoints($GROUP_ID, $breakdown);
		if(!empty($subs)){
			for($i=0; $i < count($subs); $i++){
				$members = GROUP::getAllMembers($subs[$i], $members);
			}
			for($i=0; $i < count($subs); $i++){
				$breakdown = GROUP::getTotalGroupPoints($subs[$i], $breakdown);
			}
		}
		$breakdown = GROUP::getTotalIndividualPoints($GROUP_ID, $breakdown, $members);
		return $breakdown;
	}

	/**
	*	Gets the total points, time, and distance of a group
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function getAllMembers($GROUP_ID, $members){
		$info = GROUP::get_info($GROUP_ID);
		if($info['G_GLOG']){
			//do nothing
		}else{
			$sql = "SELECT U_ID FROM GROUP_MEMBER WHERE G_ID = '$GROUP_ID'";
			$query = MSSQL::query($sql);
			while(odbc_fetch_row($query)){
				$members[] = odbc_result($query, 'U_ID');
			}
		}
		return $members;
	}

	/**
	*	Gets the total points, time, and distance of a group
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function getTotalGroupPoints($GROUP_ID, $breakdown){
		$info = GROUP::get_info($GROUP_ID);
		if($info['G_GLOG']){
			$date_range = '';
			$day1 = '';
			$day2 = '';
			$month1 = '';
			$month2 = '';
			$year1 = '';
			$year2 = '';
			//GET THE COUNTY INFO
			if (filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING) != null) {
					//day 1
					$date1 = explode('/', filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING));

					$month1 = $date1[0];
					$day1 = $date1[1];
					$year1 = $date1[2];

					//day 2
					$date2 = explode('/', filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING));

					$month2 = $date2[0];
					$day2 = $date2[1];
					$year2 = $date2[2];
					$date1= $year1.'-'.$month1.'-'.$day1;
					$date2= $year2.'-'.$month2.'-'.$day2;

					$date_range = 'GA_DATE >= \'' . $date1 . '\' AND GA_DATE <= \'' . $date2 . '\' AND';
			}
			if (filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING) != null) {
					//day 1
					$date1 = explode('/', filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING));

					$month1 = $date1[0];
					$day1 = $date1[1];
					$year1 = $date1[2];

					//day 2
					$date2 = explode('/', filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING));

					$month2 = $date2[0];
					$day2 = $date2[1];
					$year2 = $date2[2];
					$date1= $year1.'-'.$month1.'-'.$day1;
					$date2= $year2.'-'.$month2.'-'.$day2;

					$date_range = 'GA_DATE >= \'' . $date1 . '\' AND GA_DATE <= \'' . $date2 . '\' AND';
			}

			$sql = "SELECT GA_AID AS 'AID', SUM(GA_PA) AS 'PA', SUM(GA_TIME) AS 'TIME', SUM(GA_UNIT) AS 'UNIT', GA_MEMBER_COUNT AS 'COUNT' FROM GROUP_ACTIVITY WHERE GA_GID IN (SELECT G_ID FROM GROUPS WHERE ".$date_range." G_ID = '$GROUP_ID') GROUP BY GA_AID, GA_MEMBER_COUNT;";
			$query = MSSQL::query($sql);
			while(odbc_fetch_row($query)){
				$aid = odbc_result($query, 'AID');
				if(!isset($breakdown['POINTS'][$aid])){
					$breakdown['POINTS'][$aid]='';
				}
				if(!isset($breakdown['TIME'][$aid])){
					$breakdown['TIME'][$aid]='';
				}
				if(!isset($breakdown['DISTANCE'][$aid])){
					$breakdown['DISTANCE'][$aid]='';
				}
				if(!isset($breakdown['MEMBER_COUNT'][$aid])){
					$breakdown['MEMBER_COUNT'][$aid]='';
				}
				if($info['G_GLOG']){
					$breakdown['MEMBER_COUNT'][$aid] += odbc_result($query, 'COUNT');
				}else{
					$breakdown['MEMBER_COUNT'][$aid] += 1;
				}
				$breakdown['POINTS'][$aid] += odbc_result($query, 'PA');
				$breakdown['TIME'][$aid] += odbc_result($query, 'TIME');
				//checks if this is a distance activity, hardcoded for speed
				$DA = 0;
				if(($aid == 1) ||($aid == 2) ||($aid == 3) ||($aid == 47) ||($aid == 50) ||($aid == 68) ||($aid == 70)){$DA=1;}
				if ($DA == 1){
					  $breakdown['DISTANCE'][$aid] += odbc_result($query, 'UNIT');
				}
			}
		}else{
			//do nothing
		}
		return $breakdown;
	}

	/**
	*	Gets the total points, time, and distance of a group
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function getTotalIndividualPoints($GROUP_ID, $breakdown, $members){
		$info = GROUP::get_info($GROUP_ID);
		if($info['G_GLOG']){
			//do nothing
		}else{
			$date_range = '';
			$day1 = '';
			$day2 = '';
			$month1 = '';
			$month2 = '';
			$year1 = '';
			$year2 = '';
			//GET THE COUNTY INFO
			if (filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING) != null) {
			    //day 1
			    $date1 = explode('/', filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING));

			    $month1 = $date1[0];
			    $day1 = $date1[1];
			    $year1 = $date1[2];

			    //day 2
			    $date2 = explode('/', filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING));

			    $month2 = $date2[0];
			    $day2 = $date2[1];
			    $year2 = $date2[2];
			    $date1= $year1.'-'.$month1.'-'.$day1;
			    $date2= $year2.'-'.$month2.'-'.$day2;

			    $date_range = 'AL_DATE >= \'' . $date1 . '\' AND AL_DATE <= \'' . $date2 . '\' AND';
			}
			if (filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING) != null) {
			    //day 1
			    $date1 = explode('/', filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING));

			    $month1 = $date1[0];
			    $day1 = $date1[1];
			    $year1 = $date1[2];

			    //day 2
			    $date2 = explode('/', filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING));

			    $month2 = $date2[0];
			    $day2 = $date2[1];
			    $year2 = $date2[2];
			    $date1= $year1.'-'.$month1.'-'.$day1;
			    $date2= $year2.'-'.$month2.'-'.$day2;

			    $date_range = 'AL_DATE >= \'' . $date1 . '\' AND AL_DATE <= \'' . $date2 . '\' AND';
			}

			$sql = "SELECT AL_AID AS 'AID', SUM(AL_PA) AS 'PA', SUM(AL_TIME) AS 'TIME', SUM(AL_UNIT) AS 'UNIT', AL_UID AS 'COUNT' FROM LOG WHERE ".$date_range." AL_UID IN (SELECT L_ID FROM LOGIN WHERE L_ID = '".implode("' OR L_ID = '", $members)."') GROUP BY AL_AID, AL_UID;";
			$query = MSSQL::query($sql);
			while(odbc_fetch_row($query)){
				$aid = odbc_result($query, 'AID');
				if(!isset($breakdown['POINTS'][$aid])){
					$breakdown['POINTS'][$aid]='';
				}
				if(!isset($breakdown['TIME'][$aid])){
					$breakdown['TIME'][$aid]='';
				}
				if(!isset($breakdown['DISTANCE'][$aid])){
					$breakdown['DISTANCE'][$aid]='';
				}
				if(!isset($breakdown['MEMBER_COUNT'][$aid])){
					$breakdown['MEMBER_COUNT'][$aid]='';
				}
				if($info['G_GLOG']){
					$breakdown['MEMBER_COUNT'][$aid] += odbc_result($query, 'COUNT');
				}else{
					$breakdown['MEMBER_COUNT'][$aid] += 1;
				}
				$breakdown['POINTS'][$aid] += odbc_result($query, 'PA');
				$breakdown['TIME'][$aid] += odbc_result($query, 'TIME');
				//checks if this is a distance activity, hardcoded for speed
				$DA = 0;
				if(($aid == 1) ||($aid == 2) ||($aid == 3) ||($aid == 47) ||($aid == 50) ||($aid == 68) ||($aid == 70)){$DA=1;}
				if ($DA == 1){
					  $breakdown['DISTANCE'][$aid] += odbc_result($query, 'UNIT');
				}
			}
		}
		return $breakdown;
	}

	/**
	*	Lists the groups associated with a user's ID
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function listJoinedGroups($S_ID){
		$initiate ='';
		$query = MSSQL::query('SELECT * FROM GROUP_MEMBER WHERE G_ID IN(SELECT G_ID FROM GROUPS WHERE G_ISROOTGROUP=\'1\') AND U_ID=\''.$S_ID.'\' AND IS_VISIBLE = \'1\'');
		if(odbc_num_rows($query)){
		$i=0;
		?>
  <div class="row">
	<div class="large-12 columns">
		<?php
		while(odbc_fetch_row($query)){
			$i++;
			$GROUP_ID = odbc_result($query, 'G_ID');
			$info = GROUP::get_info($GROUP_ID);
			?>

	 <!-- Main Section -->
	  <div class="blue-bg-group" id="<?php echo $GROUP_ID; ?>-toggle">
		  <div class="my-group-avatar">
			<?php
			  GROUP::avatar_small($GROUP_ID);
			?>
		  </div>
		  <?php
			echo $info['G_NAME'];
		  ?>
		<div class="expand-icon" style="margin-top:-0.5em;"><i class="fi-list size-24"></i></div>
		<div class="my-group-expand" id="<?php echo $GROUP_ID; ?>-breakdown" style="display:none; margin-top:20px;">

			  <hr style="border-color:white;" />
			  <a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/group/groups.php?group='.$GROUP_ID; ?>" class="button tiny round secondary">Visit Group</a>
			  <?php
				if(($_SESSION['ID'] == ('2' || '95' || '209')) && ($_SESSION['ID'] != $S_ID) ){
			  ?>
				  <a href="#" class="button tiny round alert" data-reveal-id="remove_user_<?php echo $GROUP_ID; ?>">Remove User</a>
                                  <div id="remove_user_<?php echo $GROUP_ID; ?>" class="reveal-modal" data-reveal>
                                    <h3 class="global-h3">Remove user from this group?</h3>
                                    <br />
                                    <a href="remove_user_from_group.php?gid=<?php echo $GROUP_ID; ?>&uid=<?php echo $S_ID; ?>" class="tiny button alert">I'm Sure</a>
                                    <a href="javascript:history.go(0)" class="tiny button">Whoops! Cancel</a>
                                    <a class="close-reveal-modal">&#215;</a>
                                  </div>
			  <?php
				}
				if(($_SESSION['ID'] == $S_ID) ){
			  ?>
				  <a href="group/groups.php?group=<?php echo $GROUP_ID; ?>&leave=1" class="button tiny round alert">Leave Group</a>
			  <?php
				}
			  ?>
			  <hr style="border-color:white; margin-top:0px;" />

		  <div class="row">
			<div class="large-6 columns">
			  <h2 class="global-h2" style="color:white;">Subgroups:</h2>
			</div>
		  </div>
		  <div class="row">
			<div class="large-block-grid-3 medium-block-grid-3 small-block-grid-1 columns">
			  <?php GROUP::list_subgroups($GROUP_ID); ?>
			</div>
		  </div>
		  <div id="group-status_<?php echo $GROUP_ID; ?>" class="reveal-modal" data-reveal>
			  <h3 class="global-h3">You are currently a member of this group.</h3>
			  <br />
			  <a href="group/groups.php?group=<?php echo $GROUP_ID; ?>&leave=1" class="tiny button alert">I'm Sure</a>
			  <a href="javascript:history.go(0)" class="tiny button">Whoops! Cancel</a>
			  <a class="close-reveal-modal">&#215;</a>
			</div>
		</div>
	  </div>
	  <?php
		$initiate .=
		  '$("#'.$GROUP_ID.'-toggle").click(function(){
			  $("#'.$GROUP_ID.'-breakdown").toggle();
			});';
	  }
	  ?>
	</div>
  </div>

  <!-- End of Main Section -->
			<?php

		}else{
			?>
	  <div class="group-unit">
		<div class="row">
		  <div class="large-1 medium-2 hide-for-small columns">
			<img src="img/alert.png" alt="" />
		  </div>
		  <div class="large-11 medium-10 columns">
			<p class="global-p" style="padding:10px; margin-bottom:0px;">You do not currently belong to any groups.</p>
		  </div>
		</div>
	  </div>
			<?php

		}
		return $initiate;
	}

	/**
	*	Uploads a group's image to the server
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function avatar_upload($G_ID) {
		$errmsg="group=".$G_ID."&img_success=1";
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["group_img"]["name"]);
		$extension = end($temp);
                $size = $_FILES["group_img"]["size"];
		if(($_FILES["group_img"]["size"] > 0) &&($_FILES["group_img"]["size"] < 1048576)){
			if (in_array($extension, $allowedExts)){
				$upload = move_uploaded_file($_FILES["group_img"]["tmp_name"], ROOT."/group/img/$G_ID.png");
                                echo $_FILES["group_img"]["tmp_name"].':'.ROOT."\group\img\\$G_ID.png:".$upload;
                                //REDIRECT::current_page($errmsg);
			}else{
				$errmsg = "group=".$G_ID."&err_msg=We don't support that type of image.  You can upload a jpeg, jpg, gif, or png.";
				//REDIRECT::current_page($errmsg);
			}
		} else {
			$errmsg = "group=".$G_ID."&err_msg=Your image is  too large for us at $size bytes.  We can only take images 1MB or less.  Try re-sizing it on your computer or uploading a smaller image.";
			//REDIRECT::current_page($errmsg);
		}
	}

	/**
	*	Renders breadcrumbs for subgroup management respective to current subgroup
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function breadcrumbs_sub($G_ID) {
		$info = GROUP::get_info($G_ID);
		//get the group's name for use in the breadcrumbs
		$g_name = $info['G_NAME'];
		//add it to the bc array
		$BC['name'][] = $g_name;
		//Add the parent ID to the array for link writing
		$parent = odbc_result(MSSQL::query('SELECT G_ID FROM GROUPS WHERE G_UUID=\''.$info['G_PID'].'\''), 'G_ID');
		$BC['parent'][] = $parent;
		//check to see if this is the root group
		$isroot = $info['G_ISROOTGROUP'];
		$i = 0;
		while($isroot != 1){
			$i++;
			if($i > 10){
				$isroot = 1;
			}
			$info = GROUP::get_info($parent);
			$g_name = $info['G_NAME'];
			$BC['name'][] = $g_name;
			$parent = odbc_result(MSSQL::query('SELECT G_ID FROM GROUPS WHERE G_UUID=\''.$info['G_PID'].'\''), 'G_ID');
			$BC['parent'][] = $parent;
			$isroot = $info['G_ISROOTGROUP'];
		}
		for($i=count($BC['name']);$i>0;$i--){
			if($i == 1){
				$current = 'class="current"';
				?>
				<li class="current"><?php echo $BC['name'][$i-1]; ?></li>
				<?php
			}else{
			?>
			<li><a href="subgroups.php?group=<?php echo $BC['parent'][$i-2];?>"><?php echo $BC['name'][$i-1]; ?></a></li>
			<?php
			}
		}
	}

	/**
	*	Renders breadcrumbs for subgroup management respective to current subgroup
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function breadcrumbs($G_ID) {
		$info = GROUP::get_info($G_ID);
		//get the group's name for use in the breadcrumbs
		$g_name = $info['G_NAME'];
		//add it to the bc array
		$BC['name'][] = $g_name;
		//Add the parent ID to the array for link writing
		$parent = odbc_result(MSSQL::query('SELECT G_ID FROM GROUPS WHERE G_UUID=\''.$info['G_PID'].'\''), 'G_ID');
		$BC['parent'][] = $parent;
		//check to see if this is the root group
		$isroot = $info['G_ISROOTGROUP'];
		$i = 0;
		while($isroot != 1){
			$i++;
			if($i > 10){
				$isroot = 1;
			}
			$info = GROUP::get_info($parent);
			$g_name = $info['G_NAME'];
			$BC['name'][] = $g_name;
			$parent = odbc_result(MSSQL::query('SELECT G_ID FROM GROUPS WHERE G_UUID=\''.$info['G_PID'].'\''), 'G_ID');
			$BC['parent'][] = $parent;
			$isroot = $info['G_ISROOTGROUP'];
		}
		for($i=count($BC['name']);$i>0;$i--){
			if($i == 1){
				$current = 'class="current"';
				?>
				  <li <?php echo $current; ?>><?php echo $BC['name'][$i-1]; ?></li>
				<?php
			}else{
				?>
				  <li><a href="groups.php?group=<?php echo $BC['parent'][$i-2];?>"><?php echo $BC['name'][$i-1]; ?></a></li>
				<?php
			}
		}
	}

	/**
	*	Renders subgroups for subgroup management respective to current subgroup
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function list_subgroups_table($G_ID) {
		$PID = odbc_result(MSSQL::query('SELECT G_UUID FROM GROUPS WHERE G_ID=\''.$G_ID.'\''), 'G_UUID');
		$query = MSSQL::query("SELECT * FROM GROUPS WHERE G_PID ='".$PID."'");

			?>
		  <div class="data-table">
			<table class="display" id="" style="width:100%;">
			  <thead>
				<th class="global-p-white" style="width:60px; padding:5px;">
				  <img src="../../img/default-group-icon.png" alt="default group icon" style="display:block; margin:auto;"/>
				</th>
				<th class="global-h2">
				  Subgroup Title
				  </th>
			  </thead>
			<?php
			while(odbc_fetch_row($query)){
				$GROUP_NAME = odbc_result($query, 4);
				?>
				<tr>
				  <td class="global-p" style="width:60px; padding:5px;" onclick="location.href='?group=<?php echo odbc_result($query, 'G_ID'); ?>'">
					<a href="?group=<?php echo odbc_result($query, 'G_ID'); ?>">
					  <?php $avatar = GROUP::avatar_small(odbc_result($query, 'G_ID'));?>
					</a>
				  </td>
				  <td class="global-p" onclick="location.href='?group=<?php echo odbc_result($query, 'G_ID'); ?>'">
					<a href="?group=<?php echo odbc_result($query, 'G_ID'); ?>">
					  <br />
					  <p class="global-p"><?php echo $GROUP_NAME; ?></p>
					</a>
				  </td>
				</tr>
			<?php
			}
			?>
			  </table>
		  </div>
		  <?php
	}

	/**
	*	Renders subgroups for subgroup management respective to current subgroup
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function list_subgroups($G_ID) {
		$PID = odbc_result(MSSQL::query('SELECT G_UUID FROM GROUPS WHERE G_ID=\''.$G_ID.'\''), 'G_UUID');
		$query = MSSQL::query("SELECT * FROM GROUPS WHERE G_PID ='".$PID."'");
		while(odbc_fetch_row($query)){
			?>
			<li style="margin-bottom:0px; padding-bottom:0px;">
			  <a href="#" class="button tiny round expand <?php if(GROUP::hasJoined(odbc_result($query, 'G_ID'), $_SESSION['ID'])){ echo 'success';} ?>" style="text-align:left; padding-left:15px;" data-reveal-id="subgroup-options-<?php echo odbc_result($query, 'G_ID');?>"><?php echo odbc_result($query, 'G_NAME'); ?></a>
			</li>
			  <!-- Subgroup Modal -->
		  <div id="subgroup-options-<?php echo odbc_result($query, 'G_ID');?>" class="reveal-modal tiny" data-reveal>

			<div class="row center">
			  <div class="large-12 columns">

				<div class="panel-subgroup">
				<?php GROUP::avatar_medium(odbc_result($query, 'G_ID')); ?>
				<h2 class="global-h2-white"><?php echo odbc_result($query, 'G_NAME'); ?></h2>
				</div>

			  </div>
			</div>

			<div class="row center">
			  <div class="large-12 columns">
				<a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/group/groups.php?group='.odbc_result($query, 'G_ID'); ?>" class="button tiny round">Visit Group</a>
				<!--<a href="#" class="button tiny round secondary" data-reveal-id="hide-subgroup-<?php echo odbc_result($query, 'G_ID');?>">Hide Group</a>-->
				<a href="#" class="button tiny round alert" data-reveal-id="leave-subgroup-<?php echo odbc_result($query, 'G_ID');?>">Leave Group</a>
			  </div>
			</div>

			<a class="close-reveal-modal">&#215;</a>
		  </div>
		  <!-- End Subgroup Modal -->

		  <!-- Hide Subgroup Modal
		  <div id="hide-subgroup-<?php echo odbc_result($query, 'G_ID');?>" class="reveal-modal tiny" data-reveal>

			<div class="row center">
			  <div class="panel-subgroup">
				<?php GROUP::avatar_medium(odbc_result($query, 'G_ID')); ?>
				<h2 class="global-h2-white"><?php echo odbc_result($query, 'G_NAME'); ?></h2>
			  </div>
			</div>
			<div class="row center">
			  <div class="large-12 columns">
				<label>Are you sure you want to hide this group?  If you ever want to find it again, simply find it by navigating through the subgroup menu on your group page.</label>
				<br />
				<a href="hide_group.php?gid=<?php echo odbc_result($query, 'G_ID');?>" class="button tiny round success">I'm sure. Hide this group.</a> <a href="#" class="button tiny round secondary">Don't hide this group.</a>
			  </div>
			</div>

			<a class="close-reveal-modal">&#215;</a>
		  </div>
		  End Hide Subgroup Modal -->

		  <!-- Leave Subgroup Modal -->
		  <div id="leave-subgroup-<?php echo odbc_result($query, 'G_ID');?>" class="reveal-modal tiny" data-reveal>

			<div class="row center">
			  <div class="panel-subgroup">
				<?php GROUP::avatar_medium(odbc_result($query, 'G_ID')); ?>
				<h2 class="global-h2-white"><?php echo odbc_result($query, 'G_NAME'); ?></h2>
			  </div>
			</div>
			<div class="row center">
			  <div class="large-12 columns">
				<label>Are you sure you want to leave this group? </label>
				<br />
				<a href="group/groups.php?group=<?php echo $G_ID; ?>&leave=1" class="button tiny round alert">I'm sure. Leave this group.</a>
				<a href="javascript:history.go(0)" class="button tiny secondary round">Whoops. Cancel.</a>
			  </div>
			</div>

			<a class="close-reveal-modal">&#215;</a>
		  </div>
		  <!-- End Leave Subgroup Modal -->
			<?php
			}
	}

	/**
	*	Adds favorite groups for group management respective to current user
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function add_favorite($G_ID, $U_ID) {
		$sql = 'UPDATE GROUP_MEMBER SET IS_FAVORITE=\'1\' WHERE U_ID=\''.$U_ID.'\' AND G_ID=\''.$G_ID.'\'';
		$query = MSSQL::query($sql);
                $fav = MSSQL::query("SELECT G_NAME FROM GROUPS WHERE G_ID ='".$G_ID."'");
		$name = odbc_result($fav, 'G_NAME');
                $msg = 'fav='.$name;
                REDIRECT::group_list_fav($msg);
	}

	/**
	*	Removes favorite groups for group management respective to current user
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function remove_favorite($G_ID, $U_ID) {
		$sql = 'UPDATE GROUP_MEMBER SET IS_FAVORITE=\'0\' WHERE U_ID=\''.$U_ID.'\' AND G_ID=\''.$G_ID.'\'';
		$query = MSSQL::query($sql);
                $remfav = MSSQL::query("SELECT G_NAME FROM GROUPS WHERE G_ID ='".$G_ID."'");
		$name = odbc_result($remfav, 'G_NAME');
                $msg = 'remfav='.$name;
                REDIRECT::group_list_fav($msg);
	}

	/**
	*	Renders favorite groups for group management respective to current user
	*	@author	 Aaron McCoy <herbo4@uga.edu>
	*	@copyright	 Copyright (c) 2014, University of Georgia
	*/
	public static function list_favorites($U_ID) {
		$sql = 'SELECT G_ID, G_NAME FROM GROUPS WHERE G_ID IN (SELECT G_ID FROM GROUP_MEMBER WHERE U_ID=\''.$U_ID.'\' AND IS_FAVORITE=\'1\')';
		$query = MSSQL::query($sql);
		$row_count = odbc_num_rows($query);
		if($row_count < 4){
			?>

		  <div data-reveal-id="add-favorite-modal" class="button small round secondary center">
			Add a Favorite
		  </div>
		  <div class="bs"><a href="faq.php#F3" target="_blank" class="font -primary -extra-small">What's This?</a></div>


		<div id="add-favorite-modal" class="reveal-modal tiny" data-reveal>
			<div class="row center">
			  <div class="large-12 columns">
				<h2 class="custom-font-small-blue">Add a Favorite</h2>
				<hr />
			  </div>
			</div>
			<div class="row">
			  <div class="large-12 columns">
				<?php
				$sql2 = 'SELECT G_ID, G_NAME FROM GROUPS WHERE G_ID IN (SELECT G_ID FROM GROUP_MEMBER WHERE U_ID=\''.$_SESSION['ID'].'\' AND IS_FAVORITE=\'0\')';
				$query2 = MSSQL::query($sql2);
				$nonfavs = odbc_num_rows($query2);
				?>
				<form action='../group/add_favorite.php'>
				  <label>Select a group from the dropdown to add it to your "Favorites" menu.
				  <select id='gid' name='gid'>
					<?php
					  for($i=0;$i < $nonfavs;$i++){
						  odbc_fetch_row($query2);
						  ?>
						  <option value='<?php echo odbc_result($query2, 'G_ID');?>'><?php echo odbc_result($query2, 'G_NAME') ?></option>
						  <?php
					  }
					?>
				  </select>
				  </label>
				  <input class="button tiny" type="submit" value="Add to Favorites" />
				</form>
			  </div>
			</div>
			<a class="close-reveal-modal">&#215;</a>
		</div>

			<?php
		}
		?>
		<div class="row">
		<?php
		for($i=0;$i<$row_count;$i++){
			?>

		<!-- Begin favorite groups -->
		<div class="favorite-groups">
			<div class="small-12 medium-6 columns">
				<ul class="no-bullet">
					 <li>
					  <div class="button expand secondary center" style="color:#333333;">
						<?php $img= GROUP::avatar_medium(odbc_result($query, 'G_ID')); ?>
						<p class="font -secondary ts"><?php echo odbc_result($query, 'G_NAME') ?></p>
					  </div>
					  <div class="edit-favorite">
					  	<a data-reveal-id="edit-favorite-<?php echo odbc_result($query, 'G_ID') ?>" href="#" class="button expand">Edit</a>
					  </div>
					</li>
				</ul>
			</div>
	</div>

		<!-- end favorite groups -->

		<!-- Edit Favorite Modal -->
		 <div id="edit-favorite-<?php echo odbc_result($query, 'G_ID') ?>" class="reveal-modal small" data-reveal>
		   <div class="row center">
			 <div class="large-12 columns">
			   <h2 class="custom-font-small-blue">Edit Favorite</h2>
			   <hr />
			   <a href="group/remove_favorite.php?gid=<?php echo odbc_result($query, 'G_ID'); ?>" class="button tiny round secondary">Remove From Favorites</a>  <a href="group/groups.php?group=<?php echo odbc_result($query, 'G_ID'); ?>&leave=1" class="button tiny round alert">Leave Group</a>
			 </div>
		   </div>
		   <a class="close-reveal-modal">&#215;</a>
		 </div>
		<!-- End Edit Favorite Modal -->

			<?php
			odbc_fetch_row($query);
		}
		?>
		</div>
		<?php
	}

	/**
	*	Creates the activity logging form for groups
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function group_activity_form(){
		?>


			<!-- DISTANCE ACTIVITY LOGGING FORM -->

			<div id="group_logging" class="reveal-modal" data-reveal>
			<a class="close-reveal-modal">&#215;</a>
			  <div class="row">
				<div class="large-12 columns">
				<h2 class="custom-font-small-blue">Log an Activity For Group</h2>
				<hr />
				</div>
			  </div>
			  <form id="modal-form" action="log_group_activity.php" method="post" data-abide>
              	<input type="hidden" id="gid" name="gid" value ="<?php echo filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING); ?>" />
				<div class="row">
				  <div class="large-6 medium-12 columns">
					<h3 class="global-h2">Date of Activity:</h3>
					<div class="row date">
		   <!-- Month -->
		   <?php
			$day = date('d');
			$month = date('m');
			$year = date('Y');
		   ?>
			 <div class="large-4 medium-4 columns">
			   <label>Month
				 <select id="month" name="month" required>
				   <option value="1" <?php if ($month=='1'){echo 'selected="selected"';} ?>>01 January</option>
				   <option value="2" <?php if ($month=='2'){echo 'selected="selected"';} ?>>02 February</option>
				   <option value="3" <?php if ($month=='3'){echo 'selected="selected"';} ?>>03 March</option>
				   <option value="4" <?php if ($month=='4'){echo 'selected="selected"';} ?>>04 April</option>
				   <option value="5" <?php if ($month=='5'){echo 'selected="selected"';} ?>>05 May</option>
				   <option value="6" <?php if ($month=='6'){echo 'selected="selected"';} ?>>06 June</option>
				   <option value="7" <?php if ($month=='7'){echo 'selected="selected"';} ?>>07 July</option>
				   <option value="8" <?php if ($month=='8'){echo 'selected="selected"';} ?>>08 August</option>
				   <option value="9" <?php if ($month=='9'){echo 'selected="selected"';} ?>>09 September</option>
				   <option value="10" <?php if ($month=='10'){echo 'selected="selected"';} ?>>10 October</option>
				   <option value="11" <?php if ($month=='11'){echo 'selected="selected"';} ?>>11 November</option>
				   <option value="12" <?php if ($month=='12'){echo 'selected="selected"';} ?>>12 December</option>
				 </select>
			   </label>
			 </div>
		   <!-- End Month -->

		   <!-- Day -->
			 <div class="large-4 medium-4 columns">
			   <label>Day
				 <select id="day" name="day" required>
				   <option value="1" <?php if ($day=='1'){echo 'selected="selected"';} ?>>01</option>
				   <option value="2" <?php if ($day=='2'){echo 'selected="selected"';} ?>>02</option>
				   <option value="3" <?php if ($day=='3'){echo 'selected="selected"';} ?>>03</option>
				   <option value="4" <?php if ($day=='4'){echo 'selected="selected"';} ?>>04</option>
				   <option value="5" <?php if ($day=='5'){echo 'selected="selected"';} ?>>05</option>
				   <option value="6" <?php if ($day=='6'){echo 'selected="selected"';} ?>>06</option>
				   <option value="7" <?php if ($day=='7'){echo 'selected="selected"';} ?>>07</option>
				   <option value="8" <?php if ($day=='8'){echo 'selected="selected"';} ?>>08</option>
				   <option value="9" <?php if ($day=='9'){echo 'selected="selected"';} ?>>09</option>
				   <option value="10" <?php if ($day=='10'){echo 'selected="selected"';} ?>>10</option>
				   <option value="11" <?php if ($day=='11'){echo 'selected="selected"';} ?>>11</option>
				   <option value="12" <?php if ($day=='12'){echo 'selected="selected"';} ?>>12</option>
				   <option value="13" <?php if ($day=='13'){echo 'selected="selected"';} ?>>13</option>
				   <option value="14" <?php if ($day=='14'){echo 'selected="selected"';} ?>>14</option>
				   <option value="15" <?php if ($day=='15'){echo 'selected="selected"';} ?>>15</option>
				   <option value="16" <?php if ($day=='16'){echo 'selected="selected"';} ?>>16</option>
				   <option value="17" <?php if ($day=='17'){echo 'selected="selected"';} ?>>17</option>
				   <option value="18" <?php if ($day=='18'){echo 'selected="selected"';} ?>>18</option>
				   <option value="19" <?php if ($day=='19'){echo 'selected="selected"';} ?>>19</option>
				   <option value="20" <?php if ($day=='20'){echo 'selected="selected"';} ?>>20</option>
				   <option value="21" <?php if ($day=='21'){echo 'selected="selected"';} ?>>21</option>
				   <option value="22" <?php if ($day=='22'){echo 'selected="selected"';} ?>>22</option>
				   <option value="23" <?php if ($day=='23'){echo 'selected="selected"';} ?>>23</option>
				   <option value="24" <?php if ($day=='24'){echo 'selected="selected"';} ?>>24</option>
				   <option value="25" <?php if ($day=='25'){echo 'selected="selected"';} ?>>25</option>
				   <option value="26" <?php if ($day=='26'){echo 'selected="selected"';} ?>>26</option>
				   <option value="27" <?php if ($day=='27'){echo 'selected="selected"';} ?>>27</option>
				   <option value="28" <?php if ($day=='28'){echo 'selected="selected"';} ?>>28</option>
				   <option value="29" <?php if ($day=='29'){echo 'selected="selected"';} ?>>29</option>
				   <option value="30" <?php if ($day=='30'){echo 'selected="selected"';} ?>>30</option>
				   <option value="31" <?php if ($day=='31'){echo 'selected="selected"';} ?>>31</option>
				 </select>
			   </label>
			 </div>
		   <!-- End Day -->

		   <!-- Year -->
			 <div class="large-4 medium-4 columns">
			   <label>Year
				 <select id="year" name="year" required>
				   <option value="2008" <?php if ($year=='2008'){echo 'selected="selected"';} ?>>2008</option>
				   <option value="2009" <?php if ($year=='2009'){echo 'selected="selected"';} ?>>2009</option>
				   <option value="2010" <?php if ($year=='2010'){echo 'selected="selected"';} ?>>2010</option>
				   <option value="2011" <?php if ($year=='2011'){echo 'selected="selected"';} ?>>2011</option>
				   <option value="2012" <?php if ($year=='2012'){echo 'selected="selected"';} ?>>2012</option>
				   <option value="2013" <?php if ($year=='2013'){echo 'selected="selected"';} ?>>2013</option>
				   <option value="2014" <?php if ($year=='2014'){echo 'selected="selected"';} ?>>2014</option>
				   <option value="2015" <?php if ($year=='2015'){echo 'selected="selected"';} ?>>2015</option>
                                   <option value="2016" <?php if ($year=='2016'){echo 'selected="selected"';} ?>>2016</option>
                                   <option value="2017" <?php if ($year=='2017'){echo 'selected="selected"';} ?>>2017</option>
				 </select>
			   </label>
			 </div>
		   <!-- End Year -->
					</div>
				  </div>
				</div>
				<div class="row">
				  <div class="large-6 medium-6 columns">
				  <h3 class="global-h2">Activity:</h3>
				  <div class="row">
					<div class="large-12 columns">

					<label>Activity:
					  <select id="exercise" name="srch" onchange="su_magic(this, '');" class="excercise" required>
						 <?php
						  $aid = odbc_result(MSSQL::query('SELECT TOP 1 AL_AID FROM LOG WHERE AL_UID =\''.$_SESSION['ID'].'\' ORDER BY AL_DATE DESC'), 'AL_AID');
				  		  $list = ACTIVITY::select_activity($aid);
						?>
					  </select>
					</label>

					</div>
					</div>

				  </div>
				</div>
				<div class="row">
				  <div class="large-6 medium-6 columns">
					<h3 class="global-h2">Time Spent Exercising:</h3>
				  </div>
				</div>
                <input type='hidden' name="sd" id="sd"  value ='1' required>
				<div class="row">
				  <div class="large-6 medium-6 columns">
					<label>Minutes:
					  <input type="number" id="minutes" name ="minutes" required>
					</label>
				  </div>

				</div>
                <div class="row">
                  <div class="large-6 columns">
                    <div class="" id="su" name = "su">
                    </div>
				  </div>
                </div>
                <div class="row">
				  <div class="large-6 medium-6 columns">
					<h3 class="global-h2">Number of Members/Participants:</h3>
				  </div>
				</div>
                <div class="row">
                  <div class="large-6 medium-6 columns">
					<label>Members:
					  <input type="number" id="members" name ="members" required>
					</label>
				  </div>
                </div>
				<div class="row">
				  <div class="large-12 columns">
					<input type="submit"  id="submit" class="tiny button" value="Submit"/>
				  </div>
				</div>
			  </form>

			</div>
			<?php
	}

	/**
	*	Logs an activity to a group's records
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function log_activity(){
		$aid = explode(':',filter_input(INPUT_POST, 'srch', FILTER_SANITIZE_STRING));
		$unit = $aid[0];
		$aid = $aid[1];
		$time = 0;
		if(isset($_POST['minutes'])){
			$time += ($_POST['minutes']*60);
		}
		if(filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING) != null){
			$su = (filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING));
		}else{
			$su = 0;
		}
		$query = MSSQL::query('SELECT A_BASE_MET, A_MOD, A_MOD_BASE FROM ACTIVITY WHERE A_ID=\''.$aid.'\'');
		$mets=odbc_result($query, 'A_BASE_MET');
		$mod_rate=odbc_result($query, 'A_MOD');
		$base_speed = odbc_result($query, 'A_MOD_BASE');
		//we need to calculate the appropriate mets
		if($unit == 1){
			$su = (odbc_result($query, 'A_MOD_BASE')*($time/3600));
			if((filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING) != null) && filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING) !='' ){
				$su = (filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING));
			}
			if((filter_input(INPUT_POST, 'su_units', FILTER_SANITIZE_STRING) != null) && filter_input(INPUT_POST, 'su_units', FILTER_SANITIZE_STRING) !=''){
				switch(filter_input(INPUT_POST, 'su_units', FILTER_SANITIZE_STRING)){
					case 'miles':
					$mult = 1;
					break;
					case 'kilometers':
					$mult = 1.60934;
					break;
					case 'meters':
					$mult = 1609.34;
					break;
				}
				$su = ($su / $mult);
			}
			//take the baseline speed in mph
			$speed = ($su/(($time/60)/60));
			//calulate the difference between user speed and the activity base speed
			$modifier = $speed - $base_speed;
			if ($modifier > 0){
				//now that we have the differnce, we need to account for additional mets per unit of speed
				$bonus_mets = $mod_rate * $modifier;
				$mets = $mets + $bonus_mets;
			}
		}
		//points awarded = (METS * time) + subjective difficulty
		$pa = floor($time *$mets*$_POST['members']/100);
		$sql = 'INSERT INTO GROUP_ACTIVITY (GA_GID, GA_AID, GA_TIME, GA_UNIT, GA_PA, GA_DATE, GA_MEMBER_COUNT) VALUES (\''.filter_input(INPUT_POST, 'gid', FILTER_SANITIZE_STRING).'\', \''.$aid.'\', \''.$time*$_POST['members'].'\', \''.$su*$_POST['members'].'\', \''.$pa.'\', \''.filter_input(INPUT_POST, 'year', FILTER_SANITIZE_STRING).'-'.filter_input(INPUT_POST, 'month', FILTER_SANITIZE_STRING).'-'.filter_input(INPUT_POST, 'day', FILTER_SANITIZE_STRING).'\', \''.filter_input(INPUT_POST, 'members', FILTER_SANITIZE_STRING).'\')';
		$query = MSSQL::query($sql);
	}



	/**
	*	Displays recent activity to a group's page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function display_activity($num_rows){
		?>
        <div style="margin-top:-9px;">
          <a href="#" id="toggle4"><h2 class="custom-font-small-blue">Group Activity</h2></a>
            <div class="toggle4">
            <hr/>
            <?php
            $group = filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING);
			$isAdmin = GROUP::isAdmin($group);
		if(isset($group)){
			$info = GROUP::get_info($group);
		}
		if($info['G_GLOG']){
			if($isAdmin){
						?>
            <!-- Group Logging On Indicatior -->
           <a href="#" class="button round success small expand" data-reveal-id="activity-settings">
             <i class="fi-wrench size-16" style="margin-right:.5em;"></i>Group Logging Is Currently On
           </a>
            <!-- End Group Logging On Indicatior -->
            <?php
			}
			$sql = 'SELECT TOP '.$num_rows.' * FROM GROUP_ACTIVITY WHERE GA_GID = \''.$group.'\' ORDER BY GA_DATE DESC';
			$query = MSSQL::query($sql);
			while(odbc_fetch_row($query)){
				$members = odbc_result($query, 'GA_MEMBER_COUNT');
				$units = odbc_result($query, 'GA_UNIT')/$members;
    		    $aid = odbc_result($query, 'GA_AID');
			    $id = odbc_result($query, 'GA_ID');
			    $a = ACTIVITY::activity_to_form($aid);
			    $time = odbc_result($query, 'GA_TIME');
				$time =  $time/$members;
			    $hours = floor($time/3600);
			    if($hours > 1){
				    $minutes = ($time%3600)/60;
				    if($minutes > 1){
					    $time = $hours.' hours '.$minutes.' minutes';
				    }else{
					    $time = $hours.' hours';
			        }
			    }else if($hours ==1){
				    $minutes = ($time%3600)/60;
    				if($minutes > 1){
    					$time = $hours.' hour '.$minutes.' minutes';
    				}else{
    					$time = $hours.' hour';
    				}
    			}else if($hours<1){
    				$hours = 0;
    				$minutes = ($time%3600)/60;
    				$time = $time/60;
    				if($time >1){
    					$time = $minutes.' minutes';
    				}else if($time ==1){
    					$time = '1 minute';
    				}
    			}
    			$pa = odbc_result($query, 'GA_PA');
    			$date = odbc_result($query, 'GA_DATE');
				$date_array = explode('-', odbc_result($query, 'GA_DATE'));
    			$string = '<b>'.$date.'</b> - '.$a.' for '.$time.' with '.$members.' members. You earned '.$pa/$members.' points per member.';
			        //JOHN EDITS HERE

			?>
            <a href="#" data-reveal-id="M_<?php echo $id;?>" onclick="su_magic('<?php echo $aid;?>', '<?php echo $id;?>', '<?php echo $units;?>' );">
              <div data-alert class="alert-box secondary">
                <?php echo $string; ?>
              </div>
            </a>
        <?php
			        //END EDITS HERE
			}
                    if($isAdmin){
						?>
                    <br />
                    <a href="#" class="button success expand" style="margin-bottom:0px;" data-reveal-id='group_logging' data-reveal>Log Activity for Group</a>
                    <?php $form=GROUP::group_activity_form();
					}
		}else{
			if($isAdmin){
						?>
            <!-- Individual Logging On Indicatior -->
           <a href="#" class="button round small expand" data-reveal-id="activity-settings">
             <i class="fi-wrench size-16" style="margin-right:.5em;"></i>Individual Logging Is Currently On
           </a>
            <!-- End Individual Logging On Indicatior -->
            <?php
			}
			$sql = 'SELECT TOP '.$num_rows.' * FROM LOG WHERE AL_UID IN (SELECT U_ID FROM GROUP_MEMBER WHERE G_ID=\''.$group.'\') ORDER BY AL_DATE DESC';
			$query = MSSQL::query($sql);
			while(odbc_fetch_row($query)){
				$uid = odbc_result($query, 'AL_UID');
    		    $aid = odbc_result($query, 'AL_AID');
			    $id = odbc_result($query, 'AL_ID');
			    $a = ACTIVITY::activity_to_form($aid);
			    $time = odbc_result($query, 'AL_TIME');
			    $hours = floor($time/3600);
			    if($hours > 1){
				    $minutes = ($time%3600)/60;
				    if($minutes > 1){
					    $time = $hours.' hours '.$minutes.' minutes';
				    }else{
					    $time = $hours.' hours';
			        }
			    }else if($hours ==1){
				    $minutes = ($time%3600)/60;
    				if($minutes > 1){
    					$time = $hours.' hour '.$minutes.' minutes';
    				}else{
    					$time = $hours.' hour';
    				}
    			}else if($hours<1){
    				$hours = 0;
    				$minutes = ($time%3600)/60;
    				$time = $time/60;
    				if($time >1){
    					$time = $minutes.' minutes';
    				}else if($time ==1){
    					$time = '1 minute';
    				}
    			}
				$name_query = MSSQL::query('SELECT L_FNAME, L_LNAME FROM LOGIN WHERE L_ID=\''.$uid.'\'');
				$fname = odbc_result($name_query, 'L_FNAME');
				$lname = odbc_result($name_query, 'L_LNAME');
    			$sd_num = odbc_result($query, 'AL_SD');
    			$sd = strtolower(ACTIVITY::difficulty_to_form($sd_num));
    			$pa = odbc_result($query, 'AL_PA');
    			$date = odbc_result($query, 'AL_DATE');
				$date_array = explode('-', odbc_result($query, 'AL_DATE'));
    			$string = '<b>'.$date.'</b> - '.$fname.' '.$lname.': '.$a.' for '.$time.', and it was '.$sd.'. You earned '.$pa.' points.';
			        //JOHN EDITS HERE
			?>
            <a href="#">
              <div data-alert class="alert-box secondary">
                <?php echo $string; ?>
              </div>
            </a>
        <?php
			        //END EDITS HERE
			}
		}
		?>
        <!--<a href="#" data-reveal-id="group-past-activity" class="button tiny expand" style="margin-top:0px;">Show More Past Activity</a>-->

      </div>
    </div>
    <?php
	}

	/**
	*	Displays recent activity to a user's home page
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function display_activity_table($gid){
		$sql = 'SELECT * FROM GROUP_ACTIVITY WHERE GA_GID = \''.$gid.'\' ORDER BY GA_DATE DESC';
		$query = MSSQL::query($sql);
		?>
        <div style="margin-top:20px;">
          <a href="#" id="toggle4"><h2 class="custom-font-small-blue">Past Group Activity:</h2></a><hr />
          <p class="global-p">Clicking on the top of each columns will arrange the results by ascending or descending order.  You can also search directly for an activity.  If you need to edit an activity, simply press the "edit" button and submit your changes.</p>
            <div class="toggle4">
              <table id='' class='display'>
                <thead>
                  <th>
                    Activity
                  </th>
                  <th>
                    Time(Minutes)
                  </th>
                  <th>
                    Points
                  </th>
                  <th>
                    Members
                  </th>
                  <th>
                    Date
                  </th>
                  <?php $isAdmin = GROUP::isAdmin(filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING));
                    if($isAdmin){?>
                  <th>

                  </th>
                  <?php } ?>
                </thead>
            <?php
		    while(odbc_fetch_row($query)){
    		    $members = odbc_result($query, 'GA_MEMBER_COUNT');
				$units = odbc_result($query, 'GA_UNIT')/$members;
    		    $aid = odbc_result($query, 'GA_AID');
			    $id = odbc_result($query, 'GA_ID');
				$gid = odbc_result($query, 'GA_GID');
			    $a = ACTIVITY::activity_to_form($aid);
			    $time = odbc_result($query, 'GA_TIME');
				$time =  $time/$members;
			    $hours = floor($time/3600);
			    if($hours > 1){
				    $minutes = ($time%3600)/60;
				    if($minutes > 1){
					    $time = $hours.' hours '.$minutes.' minutes';
				    }else{
					    $time = $hours.' hours';
			        }
			    }else if($hours ==1){
				    $minutes = ($time%3600)/60;
    				if($minutes > 1){
    					$time = $hours.' hour '.$minutes.' minutes';
    				}else{
    					$time = $hours.' hour';
    				}
    			}else if($hours<1){
    				$hours = 0;
    				$minutes = ($time%3600)/60;
    				$time = $time/60;
    				if($time >1){
    					$time = $minutes.' minutes';
    				}else if($time ==1){
    					$time = '1 minute';
    				}
    			}
    			$pa = odbc_result($query, 'GA_PA');
    			$date = odbc_result($query, 'GA_DATE');
				$date_array = explode('-', odbc_result($query, 'GA_DATE'));
			        //JOHN EDITS HERE
			?>
                <tr>
                  <td>
                    <?php echo $a; ?>
                  </td>
                  <td>
                    <?php echo $time; ?>
                  </td>
                  <td>
                    <?php echo $pa; ?>
                  </td>
                  <td>
                    <?php echo $members; ?>
                  </td>
                  <td>
                    <?php echo $date; ?>
                  </td>
                  <?php if($isAdmin){?>
                  <td class="center">
                    <a href="#" data-reveal-id="M_<?php echo $id;?>" onclick="su_magic('<?php echo $aid;?>', '<?php echo $id;?>');">
                      <div style="margin-top:20px;" data-alert class="tiny round button ">
                        Edit
              		  </div>
            		</a>
                  </td>
                  <?php } ?>
                </tr>

            <?php if($isAdmin){?>
            <div id="M_<?php echo $id;?>" class="reveal-modal" data-reveal>
              <a class="close-reveal-modal">&#215;</a>
              <div class="row">
                <div class="large-12 columns">
                  <h2 class="global-h2">Edit an Activity</h2>
                  <hr />
                </div>
              </div>
              <form id="<?php echo $id;?>" action="edit_group_activity.php" method="post" data-abide>
                <div class="row">
                  <div class="large-6 columns">
                	<div class="row" id="dp3">

           <!-- Month -->
             <div class="large-4 columns">
               <label>Month
                 <select id="month" name="month" required>
                   <option value="1" <?php if ($date_array[1]=='1'){echo 'selected="selected"';} ?>>01 January</option>
                   <option value="2" <?php if ($date_array[1]=='2'){echo 'selected="selected"';} ?>>02 February</option>
                   <option value="3" <?php if ($date_array[1]=='3'){echo 'selected="selected"';} ?>>03 March</option>
                   <option value="4" <?php if ($date_array[1]=='4'){echo 'selected="selected"';} ?>>04 April</option>
                   <option value="5" <?php if ($date_array[1]=='5'){echo 'selected="selected"';} ?>>05 May</option>
                   <option value="6" <?php if ($date_array[1]=='6'){echo 'selected="selected"';} ?>>06 June</option>
                   <option value="7" <?php if ($date_array[1]=='7'){echo 'selected="selected"';} ?>>07 July</option>
                   <option value="8" <?php if ($date_array[1]=='8'){echo 'selected="selected"';} ?>>08 August</option>
                   <option value="9" <?php if ($date_array[1]=='9'){echo 'selected="selected"';} ?>>09 September</option>
                   <option value="10" <?php if ($date_array[1]=='10'){echo 'selected="selected"';} ?>>10 October</option>
                   <option value="11" <?php if ($date_array[1]=='11'){echo 'selected="selected"';} ?>>11 November</option>
                   <option value="12" <?php if ($date_array[1]=='12'){echo 'selected="selected"';} ?>>12 December</option>
                 </select>
               </label>
             </div>
           <!-- End Month -->

           <!-- Day -->
             <div class="large-4 columns">
               <label>Day
                  <select id="day" name="day" required>
                   <option value="1" <?php if ($date_array[2]=='1'){echo 'selected="selected"';} ?>>01</option>
                   <option value="2" <?php if ($date_array[2]=='2'){echo 'selected="selected"';} ?>>02</option>
                   <option value="3" <?php if ($date_array[2]=='3'){echo 'selected="selected"';} ?>>03</option>
                   <option value="4" <?php if ($date_array[2]=='4'){echo 'selected="selected"';} ?>>04</option>
                   <option value="5" <?php if ($date_array[2]=='5'){echo 'selected="selected"';} ?>>05</option>
                   <option value="6" <?php if ($date_array[2]=='6'){echo 'selected="selected"';} ?>>06</option>
                   <option value="7" <?php if ($date_array[2]=='7'){echo 'selected="selected"';} ?>>07</option>
                   <option value="8" <?php if ($date_array[2]=='8'){echo 'selected="selected"';} ?>>08</option>
                   <option value="9" <?php if ($date_array[2]=='9'){echo 'selected="selected"';} ?>>09</option>
                   <option value="10" <?php if ($date_array[2]=='10'){echo 'selected="selected"';} ?>>10</option>
                   <option value="11" <?php if ($date_array[2]=='11'){echo 'selected="selected"';} ?>>11</option>
                   <option value="12" <?php if ($date_array[2]=='12'){echo 'selected="selected"';} ?>>12</option>
                   <option value="13" <?php if ($date_array[2]=='13'){echo 'selected="selected"';} ?>>13</option>
                   <option value="14" <?php if ($date_array[2]=='14'){echo 'selected="selected"';} ?>>14</option>
                   <option value="15" <?php if ($date_array[2]=='15'){echo 'selected="selected"';} ?>>15</option>
                   <option value="16" <?php if ($date_array[2]=='16'){echo 'selected="selected"';} ?>>16</option>
                   <option value="17" <?php if ($date_array[2]=='17'){echo 'selected="selected"';} ?>>17</option>
                   <option value="18" <?php if ($date_array[2]=='18'){echo 'selected="selected"';} ?>>18</option>
                   <option value="19" <?php if ($date_array[2]=='19'){echo 'selected="selected"';} ?>>19</option>
                   <option value="20" <?php if ($date_array[2]=='20'){echo 'selected="selected"';} ?>>20</option>
                   <option value="21" <?php if ($date_array[2]=='21'){echo 'selected="selected"';} ?>>21</option>
                   <option value="22" <?php if ($date_array[2]=='22'){echo 'selected="selected"';} ?>>22</option>
                   <option value="23" <?php if ($date_array[2]=='23'){echo 'selected="selected"';} ?>>23</option>
                   <option value="24" <?php if ($date_array[2]=='24'){echo 'selected="selected"';} ?>>24</option>
                   <option value="25" <?php if ($date_array[2]=='25'){echo 'selected="selected"';} ?>>25</option>
                   <option value="26" <?php if ($date_array[2]=='26'){echo 'selected="selected"';} ?>>26</option>
                   <option value="27" <?php if ($date_array[2]=='27'){echo 'selected="selected"';} ?>>27</option>
                   <option value="28" <?php if ($date_array[2]=='28'){echo 'selected="selected"';} ?>>28</option>
                   <option value="29" <?php if ($date_array[2]=='29'){echo 'selected="selected"';} ?>>29</option>
                   <option value="30" <?php if ($date_array[2]=='30'){echo 'selected="selected"';} ?>>30</option>
                   <option value="31" <?php if ($date_array[2]=='31'){echo 'selected="selected"';} ?>>31</option>
                 </select>
               </label>
             </div>
           <!-- End Day -->

           <!-- Year -->
             <div class="large-4 columns">
               <label>Year
                 <select id="year" name="year" required>
                   <option value="2008" <?php if ($date_array[0]=='2008'){echo 'selected="selected"';} ?>>2008</option>
                   <option value="2009" <?php if ($date_array[0]=='2009'){echo 'selected="selected"';} ?>>2009</option>
                   <option value="2010" <?php if ($date_array[0]=='2010'){echo 'selected="selected"';} ?>>2010</option>
                   <option value="2011" <?php if ($date_array[0]=='2011'){echo 'selected="selected"';} ?>>2011</option>
                   <option value="2012" <?php if ($date_array[0]=='2012'){echo 'selected="selected"';} ?>>2012</option>
                   <option value="2013" <?php if ($date_array[0]=='2013'){echo 'selected="selected"';} ?>>2013</option>
                   <option value="2014" <?php if ($date_array[0]=='2014'){echo 'selected="selected"';} ?>>2014</option>
                   <option value="2015" <?php if ($date_array[0]=='2015'){echo 'selected="selected"';} ?>>2015</option>
                   <option value="2016" <?php if ($year=='2016'){echo 'selected="selected"';} ?>>2016</option>
                   <option value="2017" <?php if ($year=='2017'){echo 'selected="selected"';} ?>>2017</option>
                 </select>
               </label>
             </div>
           <!-- End Year -->
           </div>

                  </div>
                </div>
                <div class="row">
                  <div class="large-6 columns">
                    <label>Activity:
                      <select id="exercise" name="srch" onchange="su_magic(this, '<?php echo $id;?>'), '<?php echo $units; ?>'">
                      <?php
    				    $list = ACTIVITY::select_activity($aid);
    			      ?>
                      </select>
                    </label>
                  </div>
                </div>
                <div class="row">
                  <div class="large-12 columns">
                    <p class="global-p">Time Spent Exercising:</p>
                    <hr style="margin-top:-5px; margin-bottom:5px;" />
                  </div>
                </div>
                <div class="row">
                  <div class="large-5 columns">
                    <label>Minutes:
                      <input type="number" id="minutes" name ="minutes" value="<?php echo odbc_result($query, 'GA_TIME')/60/$members; ?>" required>
                    </label>
                  </div>
                </div>
                <div class="row">
                  <div class="large-5 columns" id="su<?php echo $id;?>" name = "su<?php echo $id;?>"></div>
                </div>
                <div class="row">
                  <div class="large-5 columns">
                    <label>Members:
                      <input type="number" id="members" name ="members" value="<?php echo $members; ?>" required>
                    </label>
                  </div>
                </div>
                <div class="row">
                  <div class="large-12 columns">
                  	<input type="hidden" id="gid" name="gid" value="<?php echo filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING); ?>" />
                    <input type="hidden" id="activity_log_id" name="activity_log_id" value="<?php echo $id; ?>" />
                    <input type="submit"  id="submit" class="tiny button" value="Submit"/><a href="delete_activity.php?gaid=<?php echo $id;?>&group=<?php echo $gid;?>" class="button tiny alert">Delete Activity</a>
                  </div>
                </div>
              </form>

            </div>
        <?php
			}
			        //END EDITS HERE
		}
		?>
        </table>
      </div>
    </div>
    <?php
	}

                /**
     * 	Displays recent activity of group when individual logging is on
     * 	@author 	Juweek Adolphe <jadolphe@uga.edu>
     * 	@copyright 	Copyright (c) 2016, University of Georgia
     */
    public static function display_activity_table_indiv($gid) {

        $sql = 'SELECT * FROM GROUP_ACTIVITY WHERE GA_GID = \'' . $gid . '\' ORDER BY GA_DATE DESC';
        $query = MSSQL::query($sql);
        ?>
        <div style="margin-top:20px;">
            <a href="#" id="toggle4"><h2 class="custom-font-small-blue">Past Group Activity:</h2></a><hr />
            <p class="global-p">Clicking on the top of each columns will arrange the results by ascending or descending order.  You can also search directly for an activity.  If you need to edit an activity, simply press the "edit" button and submit your changes.</p>
            <div class="toggle4">
                <table id='' class='display'>
                    <thead>
                    <th>
                        Activity
                    </th>
                    <th>
                        Time(Minutes)
                    </th>
                    <th>
                        Points
                    </th>
                    <th>
                        Member
                    </th>
                    <th>
                        Date
                    </th>
        <?php $isAdmin = GROUP::isAdmin(filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING));
        if ($isAdmin) {
            ?>
                        <th>

                        </th>
                    <?php } ?>
                    </thead>
                        <?php
                        $sql = 'SELECT * FROM LOG WHERE AL_UID IN (SELECT U_ID FROM GROUP_MEMBER WHERE G_ID=\'' . $gid . '\') ORDER BY AL_DATE DESC';
                        $query = MSSQL::query($sql);
                        while (odbc_fetch_row($query)) {
                            $uid = odbc_result($query, 'AL_UID');
                            $aid = odbc_result($query, 'AL_AID');
                            $id = odbc_result($query, 'AL_ID');
                            $a = ACTIVITY::activity_to_form($aid);
                            $time = odbc_result($query, 'AL_TIME');
                            $hours = floor($time / 3600);
                            if ($hours > 1) {
                                $minutes = ($time % 3600) / 60;
                                if ($minutes > 1) {
                                    $time = $hours . ' hours ' . $minutes . ' minutes';
                                } else {
                                    $time = $hours . ' hours';
                                }
                            } else if ($hours == 1) {
                                $minutes = ($time % 3600) / 60;
                                if ($minutes > 1) {
                                    $time = $hours . ' hour ' . $minutes . ' minutes';
                                } else {
                                    $time = $hours . ' hour';
                                }
                            } else if ($hours < 1) {
                                $hours = 0;
                                $minutes = ($time % 3600) / 60;
                                $time = $time / 60;
                                if ($time > 1) {
                                    $time = $minutes . ' minutes';
                                } else if ($time == 1) {
                                    $time = '1 minute';
                                }
                            }
                            $name_query = MSSQL::query('SELECT L_FNAME, L_LNAME FROM LOGIN WHERE L_ID=\'' . $uid . '\'');
                            $fname = odbc_result($name_query, 'L_FNAME');
                            $lname = odbc_result($name_query, 'L_LNAME');
                            $member = $fname . ' ' . $lname;
                            $sd_num = odbc_result($query, 'AL_SD');
                            $sd = strtolower(ACTIVITY::difficulty_to_form($sd_num));
                            $pa = odbc_result($query, 'AL_PA');
                            $date = odbc_result($query, 'AL_DATE');
                            $date_array = explode('-', odbc_result($query, 'AL_DATE'));
                            //JOHN EDITS HERE
                            ?>
                        <tr>
                            <td>
            <?php echo $a; ?>
                            </td>
                            <td>
            <?php echo $time; ?>
                            </td>
                            <td>
            <?php echo $pa; ?>
                            </td>
                            <td>
            <?php echo $member; ?>
                            </td>
                            <td>
            <?php echo $date; ?>
                            </td>
                <?php if ($isAdmin) { ?>
                                <td class="center">
                                    <a href="#" data-reveal-id="M_<?php echo $id; ?>" onclick="su_magic('<?php echo $aid; ?>', '<?php echo $id; ?>');">
                                        <div style="margin-top:20px;" data-alert class="tiny round button ">
                                            Edit
                                        </div>
                                    </a>
                                </td>
            <?php
            }

            //END EDITS HERE
                        }
            ?>
                    </table>
                </div>
            </div>
                    <?php
                }

	/**
	*	Takes in a group record with a null county field and updates it to mathc it's parent's county
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function update_group_county(){

		 echo $sql = 'SELECT * FROM GROUPS WHERE G_COUNTY=\'\'';
		 $query = MSSQL::query($sql);
		 while(odbc_fetch_row($query)){
			 $pid = odbc_result($query, 'G_PID');

			  $parent_county = odbc_result(MSSQL::query('SELECT G_COUNTY FROM GROUPS WHERE G_UUID=\''.$pid.'\''), 'G_COUNTY');
			 if($parent_county=='' && odbc_result($query, 'G_ISROOTGROUP')){
				 $parent_county = odbc_result(MSSQL::query('SELECT L_COUNTY FROM LOGIN WHERE L_UUID=\''.$pid.'\''), 'L_COUNTY');
			 }

			 $update = 'UPDATE GROUPS SET G_COUNTY =\''.$parent_county.'\' WHERE G_ID=\''.odbc_result($query, 'G_ID').'\'';

			 $query2 = MSSQL::query($update);

		 }
	}


	/**
	*	Edits a group activity record
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function edit_activity(){
		$aid = explode(':',filter_input(INPUT_POST, 'srch', FILTER_SANITIZE_STRING));
		$unit = $aid[0];
		$aid = $aid[1];
		$time = 0;
		if(isset($_POST['minutes'])){
			$time += ($_POST['minutes']*60);
		}
		if(filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING) != null){
			$su = (filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING));
		}else{
			$su = 0;
		}
		$query = MSSQL::query('SELECT A_BASE_MET, A_MOD, A_MOD_BASE FROM ACTIVITY WHERE A_ID=\''.$aid.'\'');
		$mets=odbc_result($query, 'A_BASE_MET');
		$mod_rate=odbc_result($query, 'A_MOD');
		$base_speed = odbc_result($query, 'A_MOD_BASE');
		//we need to calculate the appropriate mets
		if($unit == 1){
			$su = (odbc_result($query, 'A_MOD_BASE')*($time/3600));
			if((filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING) != null) && filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING) !='' ){
				$su = (filter_input(INPUT_POST, 'su', FILTER_SANITIZE_STRING));
			}
			if((filter_input(INPUT_POST, 'su_units', FILTER_SANITIZE_STRING) != null) && filter_input(INPUT_POST, 'su_units', FILTER_SANITIZE_STRING) !=''){
				switch(filter_input(INPUT_POST, 'su_units', FILTER_SANITIZE_STRING)){
					case 'miles':
					$mult = 1;
					break;
					case 'kilometers':
					$mult = 1.60934;
					break;
					case 'meters':
					$mult = 1609.34;
					break;
				}
				$su = ($su / $mult);
			}
			//take the baseline speed in mph
			$speed = ($su/(($time/60)/60));
			//calulate the difference between user speed and the activity base speed
			$modifier = $speed - $base_speed;
			if ($modifier > 0){
				//now that we have the differnce, we need to account for additional mets per unit of speed
				$bonus_mets = $mod_rate * $modifier;
				$mets = $mets + $bonus_mets;
			}
		}
		//points awarded = (METS * time) + subjective difficulty
		$pa = (floor($time *$mets)*$_POST['members'])/100;
		echo $sql = 'UPDATE GROUP_ACTIVITY SET GA_AID=\''.$aid.'\', GA_TIME=\''.$time*$_POST['members'].'\', GA_UNIT=\''.$su*$_POST['members'].'\', GA_PA=\''.$pa.'\', GA_DATE=\''.filter_input(INPUT_POST, 'year', FILTER_SANITIZE_STRING).'-'.filter_input(INPUT_POST, 'month', FILTER_SANITIZE_STRING).'-'.filter_input(INPUT_POST, 'day', FILTER_SANITIZE_STRING).'\', GA_MEMBER_COUNT=\''.filter_input(INPUT_POST, 'members', FILTER_SANITIZE_STRING).'\' WHERE GA_ID=\''.filter_input(INPUT_POST, 'activity_log_id', FILTER_SANITIZE_STRING).'\'';
		$query = MSSQL::query($sql);
	}

	public static function list_users_in_group($G_ID) {
		$query = MSSQL::query("SELECT L_ID, L_FNAME, L_LNAME FROM LOGIN WHERE L_ID IN( SELECT U_ID FROM GROUP_MEMBER WHERE G_ID ='".$G_ID."') ORDER BY L_LNAME ASC");
		$isAdmin = GROUP::isAdmin($G_ID);
			?>
			<div class="data-table">
			<table class="display" id="" style="width:100%;">
			  <thead>
				<th class="global-p-white" style="width:60px; padding:5px;">
				  <img src="../../img/default-group-icon.png" alt="default group icon" style="display:block; margin:auto;"/>
				</th>
				<th class="global-h2">
					User Name
				</th>
				<?php
					if($isAdmin){
						?>
				<th class="global-h2">
					Admin Status
				</th>
						<?php
					}
				?>
			  </thead>
			<?php
		while(odbc_fetch_row($query)){
				  $filename = "../img/avatar/0.png";
				$UID = odbc_result($query, 'L_ID');
				$FN = odbc_result($query, 'L_FNAME');
				$LN = odbc_result($query, 'L_LNAME');
				$HTTP_HOST = filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);
				$group = filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING);
				?>
				<tr>
				  <td class="global-p" style="width:60px; padding:5px;">
					  <a href="<?php echo 'http://'.$HTTP_HOST.'/'; ?>index.php?uid=<?php echo $UID; ?>">
						<?php $avatar = ACCOUNT::avatar_small($UID);?>
					</a>
				  </td>
				  <td class="global-p">
					<a href="<?php echo 'http://'.$HTTP_HOST.'/'; ?>index.php?uid=<?php echo $UID; ?>">
					  <br />
					  <p class="global-p"><?php echo $FN.' '.$LN ?></p>
					</a>
				  </td>
				  <?php if($isAdmin){ ?>
				  <td class="global-p">
					<?php
				$sql = 'SELECT IS_ADMIN FROM GROUP_MEMBER WHERE G_ID=\''.$group.'\' AND U_ID=\''.$UID.'\'';
				$GroupAdmin = MSSQL::query($sql);
				if(odbc_result($GroupAdmin, 'IS_ADMIN')){
					?>
					<a href="<?php echo 'http://'.$HTTP_HOST.'/'; ?>group/demote_admin.php?group=<?php echo $group; ?>&uid=<?php echo $UID; ?>" class="button tiny expand alert">Remove Admin Status</a>
					<?php
				}else{
				?>
				  <a href="<?php echo 'http://'.$HTTP_HOST.'/'; ?>group/promote_to_admin.php?group=<?php echo $group; ?>&uid=<?php echo $UID; ?>" class="button tiny expand">Make Admin</a>
				  <?php } ?>
				  </td>
				  <?php } ?>
				</tr>
	  <?php
		}
	  ?>
			</table>
			</div>
			<?php
	}

	public static function list_all_users_page(){
	  if(filter_input(INPUT_GET, 'sort', FILTER_SANITIZE_STRING) != null){
		  $sort = filter_input(INPUT_GET, 'sort', FILTER_SANITIZE_STRING);
	  }else{
		  $sort = 'L_LNAME ASC';
	  }
	  if(filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING) != null){
	  $search = 'AND ((L_E LIKE \'%'.filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING).'%\') OR (L_FNAME LIKE \'%'.filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING).'%\') OR (L_LNAME LIKE \'%'.filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING).'%\'))';
	  }else{
		  $search = '';
	  }
	  $group = filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING);
	  $sql = '
	  SELECT L_ID, L_FNAME, L_LNAME, SUM(AL_PA) AS \'POINTS\', SUM(AL_TIME) AS \'SECONDS\'
	  FROM LOGIN INNER JOIN LOG ON L_ID = AL_UID
	  WHERE
	  L_ID IN(SELECT U_ID FROM GROUP_MEMBER WHERE G_ID =\''.$group.'\')
	  '.$search.'
	  GROUP BY L_FNAME, L_LNAME, L_ID
	  ORDER BY '.$sort;
	  $query = MSSQL::query($sql);
	  ?>
	  <table class="display" id="" style="width:100%;">
		<thead>
		  <th class="global-p-white">
			Profile Image
		  </th>
		  <th class="global-p-white">
			Name
		  </th>
		  <th class="global-p-white">
			Points
		  </th>
		  <th class="global-p-white">
			Time
		  </th>
		  <?php if(GROUP::isAdmin($group)){ ?>
		  <th class="global-p-white">
			Admin Options
		  </th>
		  <?php } ?>
		</thead>
	  <?php
	  while(odbc_fetch_row($query)){
		$UID = odbc_result($query, 'L_ID');
		$FN = odbc_result($query, 'L_FNAME');
		$LN = odbc_result($query, 'L_LNAME');
		$P = odbc_result($query, 'POINTS');
		$T = odbc_result($query, 'SECONDS');
	  ?>
		<tr>
		  <td class="global-p">
			<?php $avatar = ACCOUNT::avatar_small($UID);?>
		  </td>

		  <td class="global-p">
			<?php echo $FN.' '.$LN ?>
		  </td>

		  <td class="global-p">
			<?php echo $P; ?> Points
		  </td>

		  <td class="global-p">
			<?php echo number_format($T/3600); ?>H <?php echo number_format(($T%3600)/60); ?>M</h2>
		  </td>

		  <?php if(GROUP::isAdmin($group)){ ?>
			<td class="global-p">
			  <a href="#" class="tiny button" data-reveal-id="user-<?php echo $UID; ?>">Admin Options</a>
			</td>
		  <?php } ?>

		</tr>
		<?php if(GROUP::isAdmin($group)){ ?>
			<div id="user-<?php echo $UID; ?>" class="reveal-modal" data-reveal>
			  <div class="row">
				<div class="large-12 columns">
				  <h2 class="global-h2">User Options:</h2>
				  <hr />
				</div>
			  </div>
			  <div class="row">
				<div class="large-4 columns">
				<?php
				$sql = 'SELECT IS_ADMIN FROM GROUP_MEMBER WHERE G_ID=\''.$group.'\' AND U_ID=\''.$UID.'\'';
				$isAdmin = MSSQL::query($sql);
				if(odbc_result($isAdmin, 'IS_ADMIN')){
					?>
					<a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/'; ?>group/demote_admin.php?group=<?php echo $group; ?>&uid=<?php echo $UID; ?>" class="button tiny expand alert">Remove Admin Status</a>
					<?php
				}else{
				?>
				  <a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/'; ?>group/promote_to_admin.php?group=<?php echo $group; ?>&uid=<?php echo $UID; ?>" class="button tiny expand">Make Admin</a>
				  <?php } ?>
				</div>
			  </div>
			  <div class="row">
				<div class="large-4 columns">
				  <h2 class="global-h2-gray">Other Options:</h2>
				  <br />
				  <br />
				  Coming soon
				  <!-- <a href="#" class="button tiny expand">Move User</a> -->
				</div>
			  </div>
			  <div class="row">
				<div class="large-4 columns">
				  <!-- <a href="#" class="button tiny expand">User Report</a> -->
				</div>
			  </div>
			  <a class="close-reveal-modal">&#215;</a>
			</div>
		  <?php }
	  }
	  ?>
	  </table>
	  <?php
	}

	public static function list_all_subgroups_page(){
	  if(filter_input(INPUT_GET, 'sort', FILTER_SANITIZE_STRING) != null){
		  $sort = filter_input(INPUT_GET, 'sort', FILTER_SANITIZE_STRING);
	  }else{
		  $sort = 'G_NAME ASC';
	  }
	  if(filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING) != null){
	  $search = 'AND ((G_NAME LIKE \'%'.filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING).'%\'))';
	  }else{
		  $search = '';
	  }
	  $PID = odbc_result(MSSQL::query('SELECT G_UUID FROM GROUPS WHERE G_ID=\''.filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).'\''), 'G_UUID');
	  $query = MSSQL::query('SELECT * FROM GROUPS WHERE G_PID =\''.$PID.'\' '.$search);
	  ?>
	  <table class="display" id="">
		<thead>
		  <th class="global-p-white">
			Group Image
		  </th>
		  <th class="global-p-white">
			Group Name
		  </th>
		  <th class="global-p-white">
			Group Description
		  </th>
		  <th class="global-p-white">
			  Admin Options
		  </th>
		</thead>
		<?php
	  while(odbc_fetch_row($query)){
		$GID = odbc_result($query, 'G_ID');
		$NAME = odbc_result($query, 'G_NAME');
		$DESC = odbc_result($query, 'G_DESC');
		$isAdmin = GROUP::isAdmin($GID);
	  ?>
	  <tr>
		<?php
		  if (file_exists(filter_input(INPUT_SERVER, 'DOCUMENT_ROOT', FILTER_SANITIZE_STRING))."/group/img/avatar/".$GID.".png") {
			  $filename = "/group/img/avatar/".filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).".png";
		  } else {
				$filename = "../img/default-group-profile.png";
		  }
		?>
		<td class="global-p">
			<a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/'; ?>group/groups.php?group=<?php echo $GID; ?>">
			<div class="avatar-small" style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat; background-color:#008cba;"></div>
			<!-- End Avatar -->
			</a>
		</td>
		<td class="global-p">
		  <?php echo $NAME; ?>
		</td>
		<td class="global-p">
		  <?php echo $DESC; ?>
		</td>
		<td class="global-p">
		  <?php if(GROUP::isAdmin($GID)){ ?>
			<a href="#" class="tiny button" data-reveal-id="group-<?php echo $GID; ?>">Admin Options</a>
		  <?php }else{ ?>
			You are not an Admin of this group.
		  <?php } ?>
		</td>
	  </tr>
	  <?php
		if(GROUP::isAdmin($GID)){ ?>
			<div id="group-<?php echo $GID; ?>" class="reveal-modal" data-reveal>
			  <div class="row">
				<div class="large-12 columns">
				  <h2 class="global-h2">Group Options:</h2>
				  <hr />
				</div>
			  </div>
			  <div class="row">
				<div class="large-4 columns">
				  <a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/'; ?>group/delete_group.php?group=<?php echo $GID;?>" class="button tiny expand alert">Delete Group</a>
				</div>
			  </div>
			  <div class="row">
				<div class="large-4 columns">
				  <a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/'; ?>reporting.php?group=<?php echo $GID;?>" class="button tiny expand">Generate Report</a>
				</div>
			  </div>
			  <a class="close-reveal-modal">&#215;</a>
			</div>
			<?php
		}
	  }
	  ?>
	  </table>
	  <?php
	}
}

?>
