<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/back_api.php");
/**
*	
*	WALK GA WEB SITE Leaderboard API
*
*	@author 	Aaron McCoy <herbo4@uga.edu>
*	@copyright 	Copyright (c) 2014, University of Georgia
*	@package 	WalkGA
*	@category 	Back End
*	@version	1.1.0
*
*/

/**
*	
*	WALK GA LEADERBOARD WIDGET
*
*	DISPLAYS VARIOUS METHODS BY WHICH USERS CAN COMPARE THEIR PROGRESS TO OTHERS
*
*	@author 	Aaron McCoy <herbo4@uga.edu>
*	@copyright 	Copyright (c) 2014, University of Georgia
*	@package 	WalkGA
*	@category 	Back End
*	@version	1.0
*
*/
class LEADERBOARD{
	
	/**
	*	Displays a list of users relative to the session user's progress
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function display_leaderboard(){
		
	}
	
	/**
	*	Updates a User's Leaderboard Options to the database
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function update_leaderboard($LB){
		
		$query = MSSQL::query('SELECT * FROM LEADERBOARD WHERE LB_UID=\''.$ID.'\'');
	}
	
	/**
	*	Saves a User's Leaderboard Options to the database
	*	@author 	Aaron McCoy <herbo4@uga.edu>
	*	@copyright 	Copyright (c) 2014, University of Georgia
	*/
	public static function get_leaderboard($ID){
			$query = MSSQL::query('SELECT * FROM LEADERBOARD WHERE LB_UID=\''.$ID.'\'');
			$LB['AREA'] = odbc_result($query, 'AREA');
			$LB['TIMEFRAME'] = odbc_result($query, 'TIMERFRAME');
			$LB['AID'] = odbc_result($query, 'AID');
			return $LB;
	}
	
}
?>
