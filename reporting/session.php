<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/sessions_api.php");
//get the session info
$session = SESSIONS::get_info(filter_input(INPUT_GET, 'session', FILTER_SANITIZE_STRING));
$counties='';
$time='';
$points='';
$distance='';
$session_variable = filter_input(INPUT_GET, 'session', FILTER_SANITIZE_STRING);
//GET THE COUNTY INFO
if(isset($session['S_STARTDATE'])){
	$date_range = 'AL_DATE >= \''.$session['S_STARTDATE'].'\' AND AL_DATE <= \''.$session['S_ENDDATE'].'\' AND';
	$date_range2 = 'AL_DATE >= \''.$session['S_STARTDATE'].'\' AND AL_DATE <= \''.$session['S_ENDDATE'].'\' AND';
}
//query for total users
$sql0 = 'SELECT COUNT(DISTINCT U_ID) AS COUNT FROM SESSION_MEMBER WHERE S_ID =  \''.$session_variable.'\'';
$total_users = MSSQL::query($sql0);

//query for user data
$user_order ='AL_PA DESC';
if(filter_input(INPUT_GET, 'user_order', FILTER_SANITIZE_STRING) != null){
	$user_order=filter_input(INPUT_GET, 'user_order', FILTER_SANITIZE_STRING);
}
$sql1 = 'SELECT L_ID, L_FNAME, L_LNAME, L_COUNTY, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\' FROM LOG INNER JOIN LOGIN ON AL_UID = L_ID  WHERE  '.$date_range.' L_ID IN (SELECT U_ID FROM SESSION_MEMBER WHERE S_ID= \''.$session_variable.'\') GROUP BY L_FNAME, L_LNAME, L_COUNTY, L_ID ORDER BY '.$user_order.';';
$user = MSSQL::query($sql1);

//query for activity
$activity_order ='AL_PA DESC';filter_input(INPUT_GET, 'day1', FILTER_SANITIZE_STRING);
if(filter_input(INPUT_GET, 'activity_order', FILTER_SANITIZE_STRING) != null) {
	$activity_order=filter_input(INPUT_GET, 'activity_order', FILTER_SANITIZE_STRING);
}
$sql2 = 'SELECT AL_AID, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\', SUM(AL_UNIT) AS \'AL_UNIT\', COUNT(AL_AID) AS COUNT FROM LOG WHERE AL_UID IN (SELECT U_ID FROM SESSION_MEMBER WHERE '.$date_range.' S_ID = \''.$session_variable.'\') GROUP BY AL_AID ORDER BY '.$activity_order.';';
$activity = MSSQL::query($sql2);

//query for counties
$sql3 = 'SELECT L_COUNTY FROM LOGIN WHERE L_ID IN (SELECT U_ID FROM SESSION_MEMBER WHERE S_ID = \''.$session_variable.'\') GROUP BY L_COUNTY ORDER BY L_COUNTY ASC;';
$county_query = MSSQL::query($sql3);
while(odbc_fetch_array($county_query)){
  $counties .= '<a href="http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/reporting_county.php?county='.odbc_result($county_query, 'L_COUNTY').'">'.odbc_result($county_query, 'L_COUNTY').'</a>, ';
}



$count = 0;
$no_of_activities = odbc_num_rows($activity);
while(odbc_fetch_array($activity)){
	$aid = odbc_result($activity, 'AL_AID');
	$time += odbc_result($activity, 'AL_TIME');
	$points += odbc_result($activity, 'AL_PA');
	if(($aid == 1) ||($aid == 2) ||($aid == 3) ||($aid == 47) ||($aid == 50) ||($aid == 68) ||($aid == 70)){
		$distance += odbc_result($activity, 'AL_UNIT');
		$count +=1;
	}
}


?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Walk Georgia | Reporting</title>
    <link rel="stylesheet" href="../../css/foundation.css" />
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.3/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="css/dataTables.tableTools.css">
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    
  <div id="main">
    
    <div style="margin-top:20px;"></div>
    
    <!-- Header -->
      <div class="row" style="margin-bottom:20px;">
        <div class="large-12 columns center">
          <img src="img/single-color-logo.png" alt="logo" />
          <h1 class="custom-font-small">Official Report</h1>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <h2 class="custom-font-small"><?php echo $session['S_NAME']; ?></h2>
          <!-- <a href="#" class="button tiny">Printer Friendly Version</a> -->
          
          <!-- Date Range -->
          
          <?php if(filter_input(INPUT_GET, 'day1', FILTER_SANITIZE_STRING) != null){
			echo 'Current Date Range: '.filter_input(INPUT_GET, 'month1', FILTER_SANITIZE_STRING).'-'.filter_input(INPUT_GET, 'day1', FILTER_SANITIZE_STRING).'-'.filter_input(INPUT_GET, 'year1', FILTER_SANITIZE_STRING).' to '.filter_input(INPUT_GET, 'month2', FILTER_SANITIZE_STRING).'-'.filter_input(INPUT_GET, 'day2', FILTER_SANITIZE_STRING).'-'.filter_input(INPUT_GET, 'year2', FILTER_SANITIZE_STRING);
		  }?>
          <div id="reporting-date-range" class="reveal-modal" data-reveal>
            <div class="row">
              <div class="large-12 columns">
                <h2 class="global-h2">Date Range for Report</h2>
                <hr />
              </div>
              <form>
              <input type="hidden" name="session" id="session" value="<?php echo $session_variable; ?>" >
              <div class="row">
                <div class="large-6 colmns">
                  <div class="row">
                    <div class="large-12 columns">
                      <h3 class="global-h2-gray">Start Date:</h3>
                    </div>
                  </div>
                  <div class="row">
                    <div class="large-12 columns">
                       <!-- Month -->
           <?php
		    $day = date('d');
			$month = date('m');
			$year = date('Y'); 
		   ?>
             <div class="large-4 columns">
               <label>Month
                 <select id="month1" name="month1" required>
                   <option value="1" <?php if ($month=='1'){echo 'selected="selected"';} ?>>01 January</option>
                   <option value="2" <?php if ($month=='2'){echo 'selected="selected"';} ?>>02 February</option>
                   <option value="3" <?php if ($month=='3'){echo 'selected="selected"';} ?>>03 March</option>
                   <option value="4" <?php if ($month=='4'){echo 'selected="selected"';} ?>>04 April</option>
                   <option value="5" <?php if ($month=='5'){echo 'selected="selected"';} ?>>05 May</option>
                   <option value="6" <?php if ($month=='6'){echo 'selected="selected"';} ?>>06 June</option>
                   <option value="7" <?php if ($month=='7'){echo 'selected="selected"';} ?>>07 July</option>
                   <option value="8" <?php if ($month=='8'){echo 'selected="selected"';} ?>>08 August</option>
                   <option value="9" <?php if ($month=='9'){echo 'selected="selected"';} ?>>09 September</option>
                   <option value="10" <?php if ($month=='10'){echo 'selected="selected"';} ?>>10 October</option>
                   <option value="11" <?php if ($month=='11'){echo 'selected="selected"';} ?>>11 November</option>
                   <option value="12" <?php if ($month=='12'){echo 'selected="selected"';} ?>>12 December</option>
                 </select>
               </label>
             </div>
           <!-- End Month -->
         
           <!-- Day -->
             <div class="large-4 columns">
               <label>Day
                 <select id="day1" name="day1" required>
                   <option value="1" <?php if ($day=='1'){echo 'selected="selected"';} ?>>01</option>
                   <option value="2" <?php if ($day=='2'){echo 'selected="selected"';} ?>>02</option>
                   <option value="3" <?php if ($day=='3'){echo 'selected="selected"';} ?>>03</option>
                   <option value="4" <?php if ($day=='4'){echo 'selected="selected"';} ?>>04</option>
                   <option value="5" <?php if ($day=='5'){echo 'selected="selected"';} ?>>05</option>
                   <option value="6" <?php if ($day=='6'){echo 'selected="selected"';} ?>>06</option>
                   <option value="7" <?php if ($day=='7'){echo 'selected="selected"';} ?>>07</option>
                   <option value="8" <?php if ($day=='8'){echo 'selected="selected"';} ?>>08</option>
                   <option value="9" <?php if ($day=='9'){echo 'selected="selected"';} ?>>09</option>
                   <option value="10" <?php if ($day=='10'){echo 'selected="selected"';} ?>>10</option>
                   <option value="11" <?php if ($day=='11'){echo 'selected="selected"';} ?>>11</option>
                   <option value="12" <?php if ($day=='12'){echo 'selected="selected"';} ?>>12</option>
                   <option value="13" <?php if ($day=='13'){echo 'selected="selected"';} ?>>13</option>
                   <option value="14" <?php if ($day=='14'){echo 'selected="selected"';} ?>>14</option>
                   <option value="15" <?php if ($day=='15'){echo 'selected="selected"';} ?>>15</option>
                   <option value="16" <?php if ($day=='16'){echo 'selected="selected"';} ?>>16</option>
                   <option value="17" <?php if ($day=='17'){echo 'selected="selected"';} ?>>17</option>
                   <option value="18" <?php if ($day=='18'){echo 'selected="selected"';} ?>>18</option>
                   <option value="19" <?php if ($day=='19'){echo 'selected="selected"';} ?>>19</option>
                   <option value="20" <?php if ($day=='20'){echo 'selected="selected"';} ?>>20</option>
                   <option value="21" <?php if ($day=='21'){echo 'selected="selected"';} ?>>21</option>
                   <option value="22" <?php if ($day=='22'){echo 'selected="selected"';} ?>>22</option>
                   <option value="23" <?php if ($day=='23'){echo 'selected="selected"';} ?>>23</option>
                   <option value="24" <?php if ($day=='24'){echo 'selected="selected"';} ?>>24</option>
                   <option value="25" <?php if ($day=='25'){echo 'selected="selected"';} ?>>25</option>
                   <option value="26" <?php if ($day=='26'){echo 'selected="selected"';} ?>>26</option>
                   <option value="27" <?php if ($day=='27'){echo 'selected="selected"';} ?>>27</option>
                   <option value="28" <?php if ($day=='28'){echo 'selected="selected"';} ?>>28</option>
                   <option value="29" <?php if ($day=='29'){echo 'selected="selected"';} ?>>29</option>
                   <option value="30" <?php if ($day=='30'){echo 'selected="selected"';} ?>>30</option>
                   <option value="31" <?php if ($day=='31'){echo 'selected="selected"';} ?>>31</option>
                 </select>
               </label>
             </div>
           <!-- End Day -->
         
           <!-- Year -->
             <div class="large-4 columns">
               <label>Year
                 <select id="year1" name="year1" required>
                   <option value="2008" <?php if ($year=='2008'){echo 'selected="selected"';} ?>>2008</option>
                   <option value="2009" <?php if ($year=='2009'){echo 'selected="selected"';} ?>>2009</option>
                   <option value="2010" <?php if ($year=='2010'){echo 'selected="selected"';} ?>>2010</option>
                   <option value="2011" <?php if ($year=='2011'){echo 'selected="selected"';} ?>>2011</option>
                   <option value="2012" <?php if ($year=='2012'){echo 'selected="selected"';} ?>>2012</option>
                   <option value="2013" <?php if ($year=='2013'){echo 'selected="selected"';} ?>>2013</option>
                   <option value="2014" <?php if ($year=='2014'){echo 'selected="selected"';} ?>>2014</option>
                   <option value="2015" <?php if ($year=='2015'){echo 'selected="selected"';} ?>>2015</option>
                 </select>
               </label>
             </div>
           <!-- End Year -->
                    </div>
                  </div>
                </div>
                <div class="large-6 colmns">
                  <div class="row">
                    <div class="large-12 columns">
                      <h3 class="global-h2-gray">End Date:</h3>
                    </div>
                  </div>
                  <div class="row">
                    <div class="large-12 columns">
                       <!-- Month -->
           <?php
		    $day = date('d');
			$month = date('m');
			$year = date('Y'); 
		   ?>
             <div class="large-4 columns">
               <label>Month
                 <select id="month2" name="month2" required>
                   <option value="1" <?php if ($month=='1'){echo 'selected="selected"';} ?>>01 January</option>
                   <option value="2" <?php if ($month=='2'){echo 'selected="selected"';} ?>>02 February</option>
                   <option value="3" <?php if ($month=='3'){echo 'selected="selected"';} ?>>03 March</option>
                   <option value="4" <?php if ($month=='4'){echo 'selected="selected"';} ?>>04 April</option>
                   <option value="5" <?php if ($month=='5'){echo 'selected="selected"';} ?>>05 May</option>
                   <option value="6" <?php if ($month=='6'){echo 'selected="selected"';} ?>>06 June</option>
                   <option value="7" <?php if ($month=='7'){echo 'selected="selected"';} ?>>07 July</option>
                   <option value="8" <?php if ($month=='8'){echo 'selected="selected"';} ?>>08 August</option>
                   <option value="9" <?php if ($month=='9'){echo 'selected="selected"';} ?>>09 September</option>
                   <option value="10" <?php if ($month=='10'){echo 'selected="selected"';} ?>>10 October</option>
                   <option value="11" <?php if ($month=='11'){echo 'selected="selected"';} ?>>11 November</option>
                   <option value="12" <?php if ($month=='12'){echo 'selected="selected"';} ?>>12 December</option>
                 </select>
               </label>
             </div>
           <!-- End Month -->
         
           <!-- Day -->
             <div class="large-4 columns">
               <label>Day
                 <select id="day2" name="day2" required>
                   <option value="1" <?php if ($day=='1'){echo 'selected="selected"';} ?>>01</option>
                   <option value="2" <?php if ($day=='2'){echo 'selected="selected"';} ?>>02</option>
                   <option value="3" <?php if ($day=='3'){echo 'selected="selected"';} ?>>03</option>
                   <option value="4" <?php if ($day=='4'){echo 'selected="selected"';} ?>>04</option>
                   <option value="5" <?php if ($day=='5'){echo 'selected="selected"';} ?>>05</option>
                   <option value="6" <?php if ($day=='6'){echo 'selected="selected"';} ?>>06</option>
                   <option value="7" <?php if ($day=='7'){echo 'selected="selected"';} ?>>07</option>
                   <option value="8" <?php if ($day=='8'){echo 'selected="selected"';} ?>>08</option>
                   <option value="9" <?php if ($day=='9'){echo 'selected="selected"';} ?>>09</option>
                   <option value="10" <?php if ($day=='10'){echo 'selected="selected"';} ?>>10</option>
                   <option value="11" <?php if ($day=='11'){echo 'selected="selected"';} ?>>11</option>
                   <option value="12" <?php if ($day=='12'){echo 'selected="selected"';} ?>>12</option>
                   <option value="13" <?php if ($day=='13'){echo 'selected="selected"';} ?>>13</option>
                   <option value="14" <?php if ($day=='14'){echo 'selected="selected"';} ?>>14</option>
                   <option value="15" <?php if ($day=='15'){echo 'selected="selected"';} ?>>15</option>
                   <option value="16" <?php if ($day=='16'){echo 'selected="selected"';} ?>>16</option>
                   <option value="17" <?php if ($day=='17'){echo 'selected="selected"';} ?>>17</option>
                   <option value="18" <?php if ($day=='18'){echo 'selected="selected"';} ?>>18</option>
                   <option value="19" <?php if ($day=='19'){echo 'selected="selected"';} ?>>19</option>
                   <option value="20" <?php if ($day=='20'){echo 'selected="selected"';} ?>>20</option>
                   <option value="21" <?php if ($day=='21'){echo 'selected="selected"';} ?>>21</option>
                   <option value="22" <?php if ($day=='22'){echo 'selected="selected"';} ?>>22</option>
                   <option value="23" <?php if ($day=='23'){echo 'selected="selected"';} ?>>23</option>
                   <option value="24" <?php if ($day=='24'){echo 'selected="selected"';} ?>>24</option>
                   <option value="25" <?php if ($day=='25'){echo 'selected="selected"';} ?>>25</option>
                   <option value="26" <?php if ($day=='26'){echo 'selected="selected"';} ?>>26</option>
                   <option value="27" <?php if ($day=='27'){echo 'selected="selected"';} ?>>27</option>
                   <option value="28" <?php if ($day=='28'){echo 'selected="selected"';} ?>>28</option>
                   <option value="29" <?php if ($day=='29'){echo 'selected="selected"';} ?>>29</option>
                   <option value="30" <?php if ($day=='30'){echo 'selected="selected"';} ?>>30</option>
                   <option value="31" <?php if ($day=='31'){echo 'selected="selected"';} ?>>31</option>
                 </select>
               </label>
             </div>
           <!-- End Day -->
         
           <!-- Year -->
             <div class="large-4 columns">
               <label>Year
                 <select id="year2" name="year2" required>
                   <option value="2008" <?php if ($year=='2008'){echo 'selected="selected"';} ?>>2008</option>
                   <option value="2009" <?php if ($year=='2009'){echo 'selected="selected"';} ?>>2009</option>
                   <option value="2010" <?php if ($year=='2010'){echo 'selected="selected"';} ?>>2010</option>
                   <option value="2011" <?php if ($year=='2011'){echo 'selected="selected"';} ?>>2011</option>
                   <option value="2012" <?php if ($year=='2012'){echo 'selected="selected"';} ?>>2012</option>
                   <option value="2013" <?php if ($year=='2013'){echo 'selected="selected"';} ?>>2013</option>
                   <option value="2014" <?php if ($year=='2014'){echo 'selected="selected"';} ?>>2014</option>
                   <option value="2015" <?php if ($year=='2015'){echo 'selected="selected"';} ?>>2015</option>
                 </select>
               </label>
             </div>
           <!-- End Year -->
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                  <input type="submit" class="tiny button" value="Submit">

              </div>
              </form>
            </div>
          <a class="close-reveal-modal">&#215;</a>
          </div>
          
          <!-- End Date Range -->
          
          
        </div>
      </div>
    <!-- End Header -->
    
    <!-- Report Body -->
    
      <div class="row">
      
      <!-- Group Info -->
        <div class="large-6 columns">
          <h2 class="global-h2">Group Information:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <ul class="global-p" style="list-style:none; line-height:2">
            <li><b>Total Number of Users: </b><?php echo odbc_result($total_users, 'COUNT'); ?></li>
            <li><b>Total Number of Active Users <span data-tooltip aria-haspopup="true" class="has-tip" title="Any user who has actually logged activity.">(?)</span>:</b>
			<?php echo odbc_num_rows($user); ?></li>
            <li><b>County Associations <span data-tooltip aria-haspopup="true" class="has-tip" title="Lists the different counties of the members.">(?)</span>:</b> <?php echo $counties; ?></li>
          </ul>
        </div>
      <!-- End Group Info -->
      
      <!-- Overall Stats -->
        <div class="large-6 columns">
          <h2 class="global-h2">Overall Stats:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <ul class="global-p" style="list-style:none; line-height:2">
            <li><b>Total Points Earned: </b><?php echo $points; ?></li>
            <li><b>Total Time Exercised: </b><?php echo floor($time/3600).' Hours '.($time%3600)/60 .' Minutes'; ?></li>
            <li><b>Total Miles From Distance Exercises <span data-tooltip aria-haspopup="true" class="has-tip" title="The total miles of all user's logged distance-based activites, such as running, biking, hiking, etc.">(?)</span>:</b> <?php echo $distance; ?></li>
            <li><b>Virtual "Miles Walked" <span data-tooltip aria-haspopup="true" class="has-tip" title="The previous version of Walk Georgia converted all exercise (including things like yoga, which does not involve distance) into steps for the sake of comparison. We include this stat for members who still find this useful.">(?)</span>:</b>
			<?php echo number_format(((($points*100)-300)/3.3)/3660, 2, '.', ''); ?></li>
          </ul>
        </div>
      <!-- End Overall Stats -->
  
      </div>
      
      <div class="row"> 
      
      <!-- Group Members -->
        <div class="large-12 columns">
          <h2 class="global-h2">Group Members: (<?php echo odbc_result($total_users, 'COUNT'); ?>)</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
        <!-- End Member Filtering -->
          <ol class="global-p" style="line-height:2;">
            <table id="" class="display">
              <thead>
              	<th>
                  First Name
                </th>
                <th>
                  Last Name
                </th>
                <th>
                  Points
                </th>
                <th>
                  Time(Hours)
                </th>
                <th>
                  Distance(Miles)
                </th>
                <th>
                  Virtual Distance (Miles)
                </th>
              </thead>
            <?php
			  $j=0;
			  while(odbc_fetch_array($user)){
				
				$user_query = "SELECT * FROM SESSION_MEMBER WHERE S_ID ='".$session_variable."' AND U_ID='".odbc_result($user, 'L_ID')."'";
				$query = MSSQL::query($user_query);
				$breakdown = ACTIVITY::get_breakdown(odbc_result($user, 'L_ID'));
			?>
            <tr>
              <td><a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);?>/reporting_individual.php?id=<?php echo odbc_result($user, 'L_ID'); ?>"><?php echo odbc_result($user, 'L_FNAME');?></a></td>
              <td><?php echo odbc_result($user, 'L_LNAME'); ?></td>
              <td><?php echo odbc_result($user, 'AL_PA'); ?> </td>
              <td><?php echo number_format((odbc_result($user, 'AL_TIME')/3600), 2, '.', ''); ?></td>
              <td><?php echo array_sum($breakdown['TOTALS']['DISTANCE']); ?></td>
              <td><?php echo number_format((((odbc_result($user, 'AL_PA')*100)-300)/3.3)/3660, 2, '.', ''); ?></td>
            </tr> 
            <?php
			  }
			?>
            </table>
          </ol>
        </div>
      <!-- End Group Members -->
      </div>
      
      <div class="row">
        
      <!-- Activity Breakdown -->
        <div class="large-12 columns">
          <h2 class="global-h2">Activity Breakdown:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <ol class="global-p" style="line-height:2;">
          <table id="" class="display">
            <thead>
              	<th>
                  Activity
                </th>
                <th>
                  No. of Users
                </th>
                <th>
                  Points
                </th>
                <th>
                  Time(Hours)
                </th>
                <th>
                  Amount Logged
                </th>
                <th>
                  Distance (Miles)
                </th>
              </thead>
          <?php
      	  for($i =1; $i <= $no_of_activities ; $i++){
		    odbc_fetch_row($activity, $i);
			$count_query = 'SELECT COUNT(DISTINCT AL_UID) AS COUNT FROM LOG WHERE AL_AID = \''.odbc_result($activity, 'AL_AID').'\' AND AL_UID IN (SELECT U_ID FROM SESSION_MEMBER WHERE S_ID=\''.$session_variable.'\')';
			$j = MSSQL::query($count_query);
			$activity_count = odbc_result($j, 'COUNT');
	      ?>
      	    <tr>
              <td><?php echo ACTIVITY::activity_to_form(odbc_result($activity, 'AL_AID'), 2, '.', '');?></td>
              <td><?php echo $activity_count; ?></td>
              <td><?php echo odbc_result($activity, 'AL_PA'); ?></td>
              <td><?php echo number_format(odbc_result($activity, 'AL_TIME')/3600);?></td>
              <td><?php echo odbc_result($activity, 'COUNT'); ?></td>
              <td>
			  <?php 
			    if(ACTIVITY::is_distance_based(odbc_result($activity, 'AL_AID'))){ 
				  echo number_format(odbc_result($activity, 'AL_UNIT'));
				}else{
				  echo number_format((((odbc_result($activity, 'AL_PA')*100)-300)/3.3)/3660, 2, '.', '');
				}?></td>
            </tr>
	  <?php 
	  }
	  ?>
          </table>
        </div>
      <!-- End Activity Breakdown -->  
      </div>
      
      <div class="row">
        <div class="large-12 columns">
          <hr />
          <a href= "http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);?>/session/sessions.php?session=<?php echo $session_variable; ?>" class="button tiny">Back to Session Page</a>
        </div>
      </div>
      
      <div class="row">
      
      
    <!-- End Report Body -->
    
     
         
      
    <!-- End Main Content -->
     
  </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.tableTools.js"></script>
    <script>
      $(document).foundation();
	  $(document).ready(function() {
    	$('table.display').DataTable({
	      "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
		  responsive:true,
		  stateSave: true,
		  "dom": 'T<"clear">lfrtip',
		  "tableTools": {
            "sSwfPath": "http://datatables.net/release-datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
          }
		});
	  } );
    </script>
  </body>
</html>
