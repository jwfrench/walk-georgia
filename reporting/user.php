<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/groups_api.php");
$ss = SESSION::secure_session();
$current_user_info = ACCOUNT::get_info($_SESSION['ID']);
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$info = ACCOUNT::get_info($id);
$permission =0;
if($current_user_info['IS_COUNTY_ADMIN']){
	$permission += 1;
}
if($id == $_SESSION['ID']){
	$permission += 1;
}
if(filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING) != null){
	if(GROUP::isAdmin(filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING))){
		$permission += 1;
	}
}
if($permission < 1 ){
	$redirect =REDIRECT::home('Access to reports is limited to County Administrators, Group Admins, and the respective individuals');
}
$breakdown = ACTIVITY::get_breakdown($id);
$past_activity = MSSQL::query('SELECT * FROM LOG WHERE AL_UID=\''.$id.'\'');
$group = MSSQL::query('SELECT * FROM GROUPS WHERE G_ID IN (SELECT G_ID FROM GROUP_MEMBER WHERE U_ID=\''.$id.'\') ORDER BY G_ID;');
$sql2 = 'SELECT AL_AID, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\', SUM(AL_UNIT) AS \'AL_UNIT\', COUNT(AL_AID) AS COUNT FROM LOG WHERE AL_UID =\''.$id.'\' GROUP BY AL_AID ORDER BY AL_PA DESC;';
$activity = MSSQL::query($sql2);
$no_of_activities = odbc_num_rows($activity);
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Walk Georgia | Reporting</title>
    <link rel="stylesheet" href="../../css/foundation.css" />
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.3/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" type="text/css" href="css/dataTables.tableTools.css">
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    
  <div id="main">
    
    <div style="margin-top:20px;"></div>
    
    <!-- Header -->
      <div class="row" style="margin-bottom:20px;">
        <div class="large-12 columns center">
          <img src="img/single-color-logo.png" alt="logo" />
          <img src="img/ext.png" alt="UGA extension logo" />
          <br />
          <br />
          <h1 class="custom-font-small">Official Report</h1>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <h2 class="custom-font-small"><?php echo $info['FNAME']." ".$info['LNAME'] ?></h2>
          <!-- <a href="#" class="button tiny">Printer Friendly Version</a> -->
        </div>
      </div>
    <!-- End Header -->
    
    <!-- Report Body -->
    
      <div class="row">
      
      <!-- Avatar -->
        <div class="large-3 columns">
          <?php 
		    $avatar = ACCOUNT::avatar_medium($id);
		  ?>
        </div>
      <!-- End Avatar -->
      
      <!-- Personal Info -->
        <div class="large-4 columns">
          <h2 class="global-h2">Personal Information:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          
          <ul class="global-p" style="list-style:none; line-height:2">
            <li><b>Name:</b> <?php echo $info['FNAME']." ".$info['LNAME'];?></li>
            <li><b>Email:</b> <?php echo $info['EMAIL'] ;?></li>
            <li><b>County:</b> <?php echo $info['COUNTY'] ;?></li>
           <li><b>Member Since:</b> <?php echo $info['DATE_JOINED']; ?></li>
          </ul>
          
        </div>
      <!-- End Personal Info -->
      
      <!-- Overall Stats -->
        <div class="large-5 columns">
          <h2 class="global-h2">Overall Stats:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          
          <ul class="global-p" style="list-style:none; line-height:2">
            <li><b>Total Points Earned: </b><?php echo array_sum($breakdown['POINTS']);?></li>
            <li><b>Total Time Exercised: </b><?php echo floor(array_sum($breakdown['TOTALS']['TIME'])/3600).' Hours '.floor((array_sum($breakdown['TOTALS']['TIME'])%3600)/60) .' Minutes'; ?></li>
            <li><b>Total Miles From Distance Exercises <span data-tooltip aria-haspopup="true" class="has-tip" title="The total miles of all user's logged distance-based activites, such as running, biking, hiking, etc.">(?)</span>:</b> 
			<?php 
				if(!is_string($breakdown['TOTALS']['DISTANCE'])){echo array_sum($breakdown['TOTALS']['DISTANCE']);}else{echo '0';}?></li>
            <li><b>Points Converted Into "Miles Walked" <span data-tooltip aria-haspopup="true" class="has-tip" title="The previous version of Walk Georgia converted all exercise (including things like yoga, which does not involve distance) into steps for the sake of comparison. We include this stat for members who still find this useful.">(?)</span>:</b> <?php echo number_format((((array_sum($breakdown['POINTS'])*100)-300)/3.3)/3660, 2, '.', ''); ?></li>
          </ul>  
        </div>
      <!-- End Overal Stats -->
      
      </div>
      
      <div class="row" style="margin-top:20px;">
      
      <!-- Past Activity -->
        <div class="large-12 columns">
          <h2 class="global-h2">Past Activity</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <table id='' class='display'>
            <thead>
              	<th>
                  Activity
                </th>
                <th>
                  Points
                </th>
                <th>
                  Time(Hours)
                </th>
                <th>
                  Distance (Miles)
                </th>
                <th>
                  Date
                </th>
            </thead>
          <?php while(odbc_fetch_row($past_activity)){?>
            <tr>
              <td>
                <?php echo ACTIVITY::activity_to_form(odbc_result($past_activity, 'AL_AID')); ?>
              </td>
              <td>
                <?php echo odbc_result($past_activity, 'AL_PA'); ?>
              </td>
              <td>
                <?php echo number_format(odbc_result($past_activity, 'AL_TIME')/3600, 2, '.', ''); ?>
              </td>
              <td>
			  <?php 
			    if(ACTIVITY::is_distance_based(odbc_result($past_activity, 'AL_AID'))){ 
				  echo number_format(odbc_result($past_activity, 'AL_UNIT'));
				}else{
				  echo number_format((((odbc_result($past_activity, 'AL_PA')*100)-300)/3.3)/3660, 2, '.', '');
				}?></td>
                <td>
                <?php echo odbc_result($past_activity, 'AL_DATE'); ?>
              </td>
            </tr>
          <?php }?>
          </table>
        </div>
        <br>
        
      <!-- End Past Activity -->
      
      <!-- User's Groups -->
        <div class="large-12 columns">
          <h2 class="global-h2">User's Groups: (Show Number of Groups)</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <table id='' class='display'>
            <thead>
              	<th>
                  Group
                </th>
                <th>
                  Members
                </th>
                <th>
                  Points
                </th>
                <th>
                  Time (Hours)
                </th>
                <th>
                  Distance (Miles)
                </th>
                <th>
                  Virtual Distance (Miles)
                </th>
            </thead>
          <?php 
		    while(odbc_fetch_row($group)){
		      $group_info = GROUP::getBreakdown(odbc_result($group, 'G_ID'));
			  $g_members = odbc_result(MSSQL::query('SELECT COUNT(DISTINCT U_ID) AS COUNT FROM GROUP_MEMBER WHERE G_ID =  \''.odbc_result($group, 'G_ID').'\''), 'COUNT'); 
		  ?>
            <tr>
              <td>
                <a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);?>/reporting.php?group=<?php echo odbc_result($group, 'G_ID'); ?>"><?php echo odbc_result($group, 'G_NAME'); ?></a>
              </td>
              <td>
                <?php echo $g_members; ?>
              </td>
              <td><?php if(isset($group_info['POINTS'])){echo array_sum($group_info['POINTS']);}else{echo '0';} ?></td>
              <td><?php if(isset($group_info['TIME'])){echo number_format(array_sum($group_info['TIME'])/3600, 2, '.', '');}else{echo '0';}?></td>
              <td><?php if(isset($group_info['DISTANCE'])){echo array_sum($group_info['DISTANCE']);}else{echo '0';} ?></td>
              <td><?php if(isset($group_info['POINTS'])){echo number_format((((array_sum($group_info['POINTS'])*100)-300)/3.3)/3660, 2, '.', '');}else{echo '0';} ?></td>
            </tr>
          <?php }?>
          </table>
          
        </div>
      <!-- End Subgroups -->
      
      </div>
      
      <div class="row">
        
      <!-- Activity Breakdown -->
        <div class="large-12 columns">
          <h2 class="global-h2">Activity Breakdown:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <table id="" class="display">
            <thead>
              	<th>
                  Activity
                </th>
                <th>
                  Points
                </th>
                <th>
                  Time(Hours)
                </th>
                <th>
                  Times Logged
                </th>
                <th>
                  Distance (Miles)
                </th>
              </thead>
          <?php
      	  for($i =1; $i <= $no_of_activities ; $i++){
		    odbc_fetch_row($activity, $i);
	      ?>
      	    <tr>
              <td><?php echo ACTIVITY::activity_to_form(odbc_result($activity, 'AL_AID'), 2, '.', '');?></td>
              <td><?php echo odbc_result($activity, 'AL_PA'); ?></td>
              <td><?php echo number_format(odbc_result($activity, 'AL_TIME')/3600);?></td>
              <td><?php echo odbc_result($activity, 'COUNT'); ?></td>
              <td>
			  <?php 
			    if(ACTIVITY::is_distance_based(odbc_result($activity, 'AL_AID'))){ 
				  echo number_format(odbc_result($activity, 'AL_UNIT'));
				}else{
				  echo number_format((((odbc_result($activity, 'AL_PA')*100)-300)/3.3)/3660, 2, '.', '');
				}?></td>
            </tr>
	  <?php 
	  }
	  ?>
          </table>
        </div>
      <!-- End Activity Breakdown -->   
        
      </div>
    <!-- End Report Body -->
    
     
         
      
    <!-- End Main Content -->
     </div>
  </div> 
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.tableTools.js"></script>
    <script>
      $(document).foundation();
	  $('table.display').DataTable({
	      "lengthMenu": [[15, 20, 35, 60, -1], [15, 20, 35, 60, "All"]],
		  responsive:true,
		  stateSave: true,
		  "dom": 'T<"clear">lfrtip',
		  "tableTools": {
            "sSwfPath": "http://datatables.net/release-datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
          }
		});
    </script>
  </body>
</html>
