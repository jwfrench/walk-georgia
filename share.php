<?php
if(isset($_GET['points'])){
	$name = $_GET['name'];   //read the name from the url
	$points = $_GET['points'];   //read the points from the url
}
?>

<!DOCTYPE html>
<head>
	<meta property="fb:app_id" content="276401669208985" /> 
    <meta property="og:title" content="<?php echo $name ?> earned <?php echo $points ?> points on Walk Georgia!"/>
    <meta property="og:description" content="Walk Georgia: The place for
Georgians to track their fitness progress & receive local, research-based
knowledge & resources on wellness." />
    <meta property="og:image" content="http://www.walkgeorgia.org/horizontial-logo.png" />
    <meta property="og:type" content="website" /> 
    <meta property="og:site_name" content="Walk Georgia"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>