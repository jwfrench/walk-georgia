<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout')==1){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

<div id="main">
  
  <div class="row">
    <div class="large-12 columns">
      <!-- Admin Promotion Button (just add the data-reveal-id with your tweaks to each user -->
      <a class="button tiny" data-reveal-id="admin-promotion-modal">Click Here For the Promotion Modal</a>
    </div>
  </div>
  
  <!-- Admin Promotion Modal -->
  <div id="admin-promotion-modal" class="reveal-modal" data-reveal>
    <div class="row">
      <div class="large-12 columns">
        <h2 class="global-h2">Promote User</h2>
        <hr />
      </div>
    </div>
    <div class="row">
      <div class="large-4 columns">
        Avatar goes here.
      </div>
      <div class="large-8 columns">
        <p class="global-p">
          Administrators can promote other users as well as create and edit subgroups.  Activate the switch below to promote this user.
        </p>
        
        <p class="global-h2-gray" style="float:left; margin-top:4px;">Make Admin</p> 
        <fieldset class="switch round" tabindex="0" style="width:40px;">
          <input id="exampleCheckboxSwitch4" type="checkbox">
          <label for="exampleCheckboxSwitch4"></label>
        </fieldset>
        
        
      </div>
    </div>
  <a class="close-reveal-modal">&#215;</a>
  </div>
  <!-- End Admin Promotion Modal -->    
            
</div>

<?php

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script src="js/foundation/foundation.abide.js"></script>
    <script src="js/foundation/foundation.reveal.js"></script>
    <script src="js/foundation/foundation.tooltip.js"></script>
    <script src="js/foundation/foundation.offcanvas.js"></script>
    <script src="js/foundation/foundation.equalizer.js"></script>
    <script src="js/foundation/foundation.dropdown.js"></script>
    <script type="text/javascript" src="js/sha512.js"></script>
    <script type="text/javascript" src="js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>