<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING)==1){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia', $_SESSION['valid']);
HTML_ELEMENT::top_nav();

//START EDITING HERE
?>
	<div class="row large-12">
<?php
    $list = ACTIVITY::display_activity($_SESSION['ID'], '50');
?>
	</div>
<?php
//END EDITING HERE

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
    <script src="../js/foundation/foundation.reveal.js"></script>
    <script src="../js/foundation/foundation.tooltip.js"></script>
    <script src="../js/foundation/foundation.offcanvas.js"></script>
    <script src="../js/foundation/foundation.equalizer.js"></script>
    <script src="../js/foundation/foundation.dropdown.js"></script>
    <script src="../js/foundation/foundation.alert.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>