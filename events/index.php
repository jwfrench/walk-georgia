<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

<div id="main">

  <!-- Global Header Container -->
  <div id="top nojava">

    <!-- Header Image Container -->
    <div id="slideshow">

      <!-- Page Title Container -->
      <div id="slideshow-title">
        <div class="row nojava center pt5">
          <h2 class="custom-font-big-white">events</h2>
          <p class="gotham-small-white">Info on upcoming events with Walk Georgia</p>
          <br /><br /><br />

          <!-- Header Buttons
          <div class="large-12 columns expand">
            <div class="row center nojava">
              <div class="small-12 medium-4 large-4 columns">
                <a href="recipes/index.php" class="button small expand round">Recipes</a>
              </div>
              <div class="small-12 medium-4 large-4 columns">
                <a href="fitness/index.php" class="button small success expand round">Fitness Tips</a>
              </div>
              <div class="small-12 medium-4 large-4 columns">
                <a href="#" class="button small expand round secondary">Lesson Plans</a>
              </div>
            </div>
          </div>
          -->

        </div>
      </div>
      <!-- End Page Title Container -->

    </div>
    <!-- End Header Image Container -->

  </div>
  <!-- End Global Header Container -->


  <!-- Main Content -->


	<!-- Start MARCH MADNESS EVENT -->
	<div class="bg--white">
	<!-- <div class="row center nojava">
		<div class="large-10 large-centered columns">
			<h2 class="font -blue -medium -lowercase -secondary  hide-for-small-only">Check out our<a href="https://www.facebook.com/walkgeorgia"> Facebook page</a> for pictures from our last event!</h2>
			<h2 class="font -blue -medium -lowercase -secondary show-for-small-only">Check out our<a href="https://www.facebook.com/walkgeorgia"> Facebook page</a> for pictures from our last event!</h2>
		</div>
	</div>
	<div class="row tc-ns nojava" style="margin-top:10px;">
		<div class="small-12 columns">
				<p>
					Call 1-800-ASK-UGA1 to learn about upcoming Walk Georgia events in your county!
				</p>
		</div>
	</div> -->
	<!-- EVENT -->

	<!-- <div class="cf mw8-ns center pa4-ns pb5-ns pa3 tl tc-ns">
			<div class="pb3">
				<img class="pb3" src="veterans-5k.png" alt="" />
				<h2 class="ff-secondary f2-ns">Saturday, November 12, 2016</h2>

				<h4 class="fw2">Franklin D. Roosevelt State Park
					<br>
            2970 Georgia Highway 190
						<br>
            Pine Mountain, GA 31822</h4>
			</div>
				<a href="trail-run.pdf" class="button primary radius ttu mb0">Download Registration Form!</a>
		</div> -->
	<!-- End MARCH MADENESS EVENT -->




  </div> <!--End Main -->

<?php

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
  <!-- End Footer -->
  </body>
</html>
