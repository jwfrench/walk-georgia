var group_form = document.getElementById("group-form");
var group_array;
//creates a subgroup respective to a parent
function create_subgroup(parent_id, last_child_id){
	
	//create a form input
	p = document.createElement("input")
	group_form.appendChild(p);
    p.name = parent_id;
	p.id = 'sub_'+last_child_id++;
    p.type = "hidden";
	
	//create an image to represent the subgroup
	sub_row = document.getElementById("sub_row");;
	img = document.createElement("img");
	sub_row.appendChild(img);
    img.name = parent_id;
	img.id = 'sub_'+last_child_id++;
    img.src = "hidden";
	img.onclick(menu_render(p.id));
}

function delete_subgroup(subgroup_id){
	group_form.removeElement(group_form, subgroup_id);
}

//navigates to a subgroup respective to a parent
function navigate_to_subgroups(subgroup_id){
	current_node = subgroup_id;
	menu_render(current_node);
}
//navigates to a parent group respective to a subgroup
function navigate_to_parent(parent_id){
	current_node = parent_id;
	menu_render(current_node);
}
//returns all of the current node's children
function get_subgroups(current_node_ID){
	var parent = document.getElementById(current_node_ID);
	var children = document.getElementsByName(parent);
	return children;
}
//returns the id of a parent node
function get_parent(current_node_ID){
	var node = document.getElementById(current_node_ID);
	return node.name;
}
//renders all of the given nodes and hides all others
function list_subgroups(children){
	var all_nodes = get_subgroups('sub_row');
	for(var j=0; j<all_nodes.length; j++) {
		all_nodes[i].style.display = 'none';
	}
	for(var i=0; i<children.length; i++) {
		children[i].style.display = "block";
	}
}
//renders the breadcrumbs respective to the current node
function breadcrumbs(GroupName){
	var parent = document.getElementById(current_node_ID);
	var bc_array =[GroupName];
	while(parent){
		if(bc_array){
		  bc_array[bc_array.length] = parent.name;
		  id_array[bc_array.length] = parent.id;
		  parent = get_parent(parent.id);
		}
	}
	var ul = document.getElementById("breadcrumbs");
	var i = bc_array.length-1;
	var str = '<ul id="breadcrumbs" name="breadcrumbs" class="breadcrumbs hide-for-small-only">';
	while(i<0){
		str += '<li><a id ="BC_'+id_array[i]+'" title="'+bc_array[i]+'" href="index.php" onclick="menu_render('+id_array[i]+');return false;">'+bc_array[i]+'</a></li>';
		i--;
	}
	str += '</ul>';
	return str;
}
//renders the menu based on current group/subgroup
function menu_render(current_node_ID, group_name){
	//display name of group
	if(group_name == 'base'){
		group_name = document.getElementById("G_NAME").value;
	}
	document.getElementById("group_title").innerHTML = group_name;
	document.getElementById("main_group").innerHTML = group_name;
	
	//display subgroups based on 
	var nodes = get_subgroups(current_node_ID);
	list_subgroups(nodes);
	//breadcrumbs(current_node_ID);
	
}