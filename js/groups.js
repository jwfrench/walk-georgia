var group_form = document.getElementById("group-form");
var group_array = [0];
//creates a subgroup respective to a parent
function create_subgroup(parent_address, group_name){
	parent_address[parent_address.length]['name'] = group_name;
	parent_address[parent_address.length]['parent'] = parent_address;
}

function delete_subgroup(subgroup_address){
	delete group_array[subgroup_address];
}
//renders all of the given nodes and hides all others
function list_subgroups(parent_address){
	subgroups = group_array.slice(parent_address);
	for(i=0;i<subgroups.length;i++){
		//render the individual subgroups on this layer
		
	}
}
//renders the breadcrumbs respective to the current node
function breadcrumbs(current_address){
	parent = current_address;
	var str = '<ul id="breadcrumbs" name="breadcrumbs" class="breadcrumbs hide-for-small-only">';
	while(parent!=0){
		str += '<li><a id ="BC_'+parent+'" title="'+parent['name']+'" href="index.php" onclick="menu_render('+parent+');return false;">'+parent['name']+'</a></li>';
		//make the current node equal to it's parent
		parent = parent['parent'];
	}
	str += '</ul>';
	return str;
}
//renders the menu based on current group/subgroup
function menu_render(current_address){
	//display name of group
	if(current_address == 0){
		group_name = document.getElementById("G_NAME").value;
		group_array[0]['name'] = group_name;
		group_array[0]['parent'] = 0;
	}
	document.getElementById("group_title").innerHTML = current_address['name'];
	document.getElementById("main_group").innerHTML = current_address['name'];
		
}