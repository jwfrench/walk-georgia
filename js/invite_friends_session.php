<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<div class="row">
    <div class="large-12 columns">
      <h5 class="modal-header">Invite Other Users:</h5>
      <hr />
      <small>Invite entire groups by clicking on the checkbox, or expand the groups to invite specific members.  If you do not want to invite any users, simply proceed.</small>
      <br />
      <br />
      <dl class="accordion" data-accordion>
        <dd>
        <a href="#panel1">Group 1</a>
          <div id="panel1" class="content active">
            <button class="tiny">Select all Users</button> <button class="tiny">Deselect</button>
            
            <label>Subgroup 1</label>
              <input id="checkbox1" type="checkbox"><label for="checkbox1">User 1</label>
              <input id="checkbox2" type="checkbox"><label for="checkbox2">User 2</label>
              <input id="checkbox1" type="checkbox"><label for="checkbox1">User 3</label>
              <input id="checkbox2" type="checkbox"><label for="checkbox2">User 4</label>
              <input id="checkbox1" type="checkbox"><label for="checkbox1">User 5</label>
              <input id="checkbox2" type="checkbox"><label for="checkbox2">User 6</label>
              
            <label>Subgroup 2</label>
              <input id="checkbox1" type="checkbox"><label for="checkbox1">User 1</label>
              <input id="checkbox2" type="checkbox"><label for="checkbox2">User 2</label>
              <input id="checkbox1" type="checkbox"><label for="checkbox1">User 3</label>
              <input id="checkbox2" type="checkbox"><label for="checkbox2">User 4</label>
              <input id="checkbox1" type="checkbox"><label for="checkbox1">User 5</label>
              <input id="checkbox2" type="checkbox"><label for="checkbox2">User 6</label>
          </div>
        </dd>
        <dd>
        <a href="#panel2">Group 2</a>
          <div id="panel2" class="content">
            <button class="tiny">Select all Users</button> <button class="tiny">Deselect</button>
          
          <label>Subgroup 1</label>
              <input id="checkbox1" type="checkbox"><label for="checkbox1">User 1</label>
              <input id="checkbox2" type="checkbox"><label for="checkbox2">User 2</label>
              <input id="checkbox1" type="checkbox"><label for="checkbox1">User 3</label>
              <input id="checkbox2" type="checkbox"><label for="checkbox2">User 4</label>
              <input id="checkbox1" type="checkbox"><label for="checkbox1">User 5</label>
              <input id="checkbox2" type="checkbox"><label for="checkbox2">User 6</label>
          </div>
        </dd>
        <dd>
        <a href="#panel3">Group 3</a>
          <div id="panel3" class="content">
            <button class="tiny">Select all Users</button> <button class="tiny">Deselect</button>
          
            <label>Subgroup 1</label>
              <input id="checkbox1" type="checkbox"><label for="checkbox1">User 1</label>
              <input id="checkbox2" type="checkbox"><label for="checkbox2">User 2</label>
              <input id="checkbox1" type="checkbox"><label for="checkbox1">User 3</label>
              <input id="checkbox2" type="checkbox"><label for="checkbox2">User 4</label>
              <input id="checkbox1" type="checkbox"><label for="checkbox1">User 5</label>
              <input id="checkbox2" type="checkbox"><label for="checkbox2">User 6</label>
          </div>
        </dd>
      </dl>
      <br />
      <button class="small">Skip</button> <button class="small disabled">Invite</button> <button class="small">Invite</button>
    </div>
  </div>
<body>
</body>
</html>