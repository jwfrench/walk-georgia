// All John's scripts for his front-end UI elements
    
	  // Mobile Sub-Nav
      $(document).ready(function(){
        $(".mobile-toggle").click(function(){
        $(".mobile-top-nav").slideToggle("fast");
        });
      });
      // End Mobile Sub-Nav
	  
	  // Past Activity Log Toggle
	  $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });
	  // End Past Activity Toggle
	  
	  // Quick Stats Accordion
	  $(document).ready(function(){
  			$("#points-toggle").click(function(){
    			$("#points-breakdown").toggle();
  			});
			$("#time-toggle").click(function(){
    			$("#time-breakdown").toggle();
  			});
			$("#distance-toggle").click(function(){
    			$("#distance-breakdown").toggle();
  			});
                        $("#goals-toggle").click(function(){
    			$("#goals-breakdown").toggle();
  			});
		});
	  // End Quick Stats Accordion
	  
	  // School System Add Input Script 
	  
	  $(function() {
        //set a counter
		var i = 1;

		//add input
		$('a#add_btn').click(function () {
    		$('<p><input type="text" placeholder="Enter the school name here" name="sub_' + i + '" id="sub_' + i + '"  /><label style="float:left;" for="sub_' + i + '"></label>' +
		        '<a class="dynamic-link label alert" style="float:right;" href="#step2">Remove</a></p>').fadeIn("slow").appendTo('#extender');
		    i++;
		    return false;
		});


		//fadeout selected item and remove
		$("#school-system-group-form").on('click', '.dynamic-link', function () {
		    $(this).parent().fadeOut(300, function () {
		        $(this).empty();
		        return false;
			});
		});
	  });  
	  // End School System Add Input Script
	  
	  // Meta Data Add Input Script 	  
	  $(function() {
        //set a counter
		var i = 1;

		//add input
		$('a#add_info').click(function () {
    		$('<div id=\'infowrap\'><div class=\'small-6 columns\'><input type="text" placeholder="Ex: Employee Number" name="create_meta_' + i + '" id="create_meta_' + i + '"  /></div><div class=\'small-6 columns\'><input type="text" placeholder="Ex: 5555555" name="meta_' + i + '" id="create_meta_' + i + '"  /></div>' +
		        '<a class="dynamic-link label alert" style="margin-left:15px;margin-bottom:15px;" href="#step2">Remove</a></div>').fadeIn("slow").appendTo('#extender_info');
		    i++;
		    return false;
		});


		//fadeout selected item and remove
		$("#group-form").on('click', '.dynamic-link', function () {
		    $(this).parent().fadeOut(300, function () {
		        $(this).empty();
		        return false;
			});
		});
	  });  
	  // End Meta Data Add Input Script
	  
	  // Organization Meta Data Add Input Script 	  
	  $(function() {
        //set a counter
		var i = 1;

		//add input
		$('a#org_add_info').click(function () {
    		$('<div id=\'org-infowrap\'><div class=\'small-6 columns\'><input type="text" placeholder="Ex: Employee Number" name="create_meta_' + i + '" id="create_meta_' + i + '"  /></div><div class=\'small-6 columns\'><input type="text" placeholder="Ex: 5555555" name="meta_' + i + '" id="meta_' + i + '"  /></div>' +
		        '<a class="org-dynamic-link label alert" style="margin-left:15px;margin-bottom:15px;" href="#org-step2">Remove</a></div>').fadeIn("slow").appendTo('#org_extender_info');
		    i++;
		    return false;
		});


		//fadeout selected item and remove
		$("#org-group-form").on('click', '.org-dynamic-link', function () {
		    $(this).parent().fadeOut(300, function () {
		        $(this).empty();
		        return false;
			});
		});
	  });  
	  // End Organization Meta Data Add Input Script
	  
	  //Trigger the log an activity select box to change so that distance based activities load with distance fields
	  $(document).ready(function() {  
    	$('.excercise').change();
	  });
	  
	  //Trigger the log an activity select box to change so that distance based activities load with distance fields
	  $(document).ready(function() {  
    	$('.g_excercise').change();
	  });

