<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include("$root/includes/sql-config.php");
 header("Content-Type: text/html; charset=utf-8");
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
 
// DB table to use
$table = 'LOGIN';
 
// Table's primary key
$primaryKey = 'L_ID';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$sql1 = 'SELECT AL_UID, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\' FROM LOG GROUP BY AL_UID ORDER BY AL_UID DESC;';
$columns = array(
    array( 'db' => 'AL_UID', 'dt' => 0 ),
    array( 'db' => 'SUM(AL_TIME) AS \'AL_TIME\'',  'dt' => 1 ),
	array( 'db' => 'SUM(AL_PA) AS \'AL_PA\'',  'dt' => 2 )
);
 
// SQL server connection information
$sql_details = array(
    'user' => 'WalkGA_Dev_Webuser',
    'pass' => '17B0st0n73',
    'db'   => 'DEV_EXTENSION_WALKGEORGIA',
    'host' => 'DevWalkGeorgia'
);
 
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require( 'ssp.class.php' );
 
echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);
?>