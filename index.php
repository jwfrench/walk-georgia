<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
$HTTP_HOST = filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
  SESSION::logout('');
  REDIRECT::home('');
}
if((filter_input(INPUT_POST, 'file', FILTER_SANITIZE_STRING)!== null)){
  $img = ACCOUNT::avatar_upload($_SESSION['ID']);
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();
//If the user is logged in
if(isset($_SESSION['valid'])){
  if(filter_input(INPUT_GET, 'uid', FILTER_SANITIZE_STRING) != null){
    $ID =filter_input(INPUT_GET, 'uid', FILTER_SANITIZE_STRING);
  }else{
    $ID= $_SESSION['ID'];
    $UUID= $_SESSION['UUID'];
  }
  $info = ACCOUNT::get_info($ID);
  //EDIT HERE
  if(filter_input(INPUT_GET, 'err_msg', FILTER_SANITIZE_STRING) != null){?>
    <div data-alert class="alert-box alert center">
      <?php echo filter_input(INPUT_GET, 'err_msg', FILTER_SANITIZE_STRING);?></a>
      <a href="#" class="close">&times;</a>
    </div>
    <?php
  }
  $count_query = MSSQL::query("SELECT COUNT(G_STARTDATE) as COUNT FROM GOALS WHERE G_UUID ='$UUID' AND G_COMPLETED IS NULL AND  G_STARTDATE >= CAST(DATEADD(dd,-7,GETDATE()) AS DATE)");
  $tomorrow_query = MSSQL::query("SELECT COUNT(G_STARTDATE) as COUNT FROM GOALS WHERE G_UUID ='$UUID' AND G_COMPLETED IS NULL AND  G_STARTDATE >= CAST(DATEADD(dd,+1,GETDATE()) AS DATE)");
  $goalCount = odbc_result($count_query, 'COUNT');
  $tomorrow_count =  odbc_result($tomorrow_query, 'COUNT');
  ?>

  <!-- Begin Main Section -->
  <div class="main nojava">
    <!-- Begin Main Section -->
    <?php
    if((!isset($ID))||(isset($ID) && ($ID== $_SESSION['ID']))){
      ?>

      <div class="show-for-small-only row" style="margin-top:20px;">
        <div class="large-12 columns">
          <a href="" class="button success expand" style="margin-bottom:0px;" data-reveal-id="activityModal" data-reveal>Log an Activity</a>
          <?php if ($goalCount <= 0) { ?>
            <a href="#" class="button bc--peach expand" data-reveal-id="goalModal">Set a goal!</a>
            <?php } ?>
          </div>
        </div>
        <?php
      }
      ?>
      <div class="row" style="margin-top:20px;">
        <div class="large-5 medium-6 columns">
          <!-- Desktop Sidebar -->
          <div class="sidebar center hide-for-small">
            <!-- Avatar -->
            <?php
            $avatar = ACCOUNT::avatar_large($ID);
            ?>
            <!-- End Avatar -->
            <!-- Change Avatar Modal -->
            <div id="avatar" class="reveal-modal" data-reveal>
              <div class="row">
                <?php
                $avatar = ACCOUNT::avatar_change_modal($avatar);
                ?>
              </div>
              <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End Change Avatar Modal -->
            <br />
            <!-- User's Name -->
            <h2 class="custom-font-small-blue"><?php echo $info['FNAME']." ".$info['LNAME'] ?></h2>
            <!-- End User's Name -->
          </div>
          <!-- End Desktop Sidebar Sidebar -->

          <!-- Desktop "Log an Activity" Button & Admin Tools -->
          <div class="hide-for-small" style="padding:0; margin:0;">
            <?php
            if((!isset($ID))||(isset($ID) && ($ID== $_SESSION['ID']))){
              ?>
              <a href="#" class="button success expand" style="margin-bottom:0px;" data-reveal-id="activityModal" data-reveal>Log an Activity</a>
              <?php
              $today = date("F j, Y");
              $tomorrow = date('F j, Y', strtotime("+1 day", strtotime($today)));
              $nextWeek = date('F j, Y', strtotime("+7 day", strtotime($today)));
              $dayname = date('l');
              if (date('l') == 'Sunday') {
                $leaderboardStartDate = $today;
              } else {
                $leaderboardStartDate = date('F j, Y', strtotime('last Sunday', strtotime($today)));
              }
              if($ID == $_SESSION['ID']){
                if ($goalCount == 1) {
                  $pointQuery = MSSQL::query("SELECT G_POINT_GOAL, G_EARNED FROM GOALS WHERE G_UUID ='$UUID' AND G_COMPLETED IS NULL AND  G_STARTDATE >= CAST(DATEADD(dd,-7,GETDATE()) AS DATE)");
                  $points_goal = odbc_result($pointQuery, 'G_POINT_GOAL');
                  $current_points = odbc_result($pointQuery, 'G_EARNED');
                } else {
                  $points_goal = 1;
                  $current_points = 1;
                }
                $percent_complete = number_format(($current_points / $points_goal) * 100, 0, ".", ",");
                if($percent_complete ==  100 && ($current_points != $points_goal)){
                  $percent_complete = 99;
                }
                if ($goalCount <= 0) { ?>
                  <a href="#" class="button bc--peach expand" data-reveal-id="goalModal">Set a goal!</a>
                  <?php }
                }
                if($info['IS_ADMIN']){?>
                  <a href="panel.php" style="margin-top:0px; margin-bottom:0px;" class="button expand">Admin Tools</a>
                  <a href="/reporting_state.php" class="button expand ma0">State Report</a>
                  <?php }
                  if($info['IS_COUNTY_ADMIN']){?>
                    <a href="#" style="margin-top:0px;" data-reveal-id="county-admin-modal" class="button expand">County Admin Tools</a>
                    <?php }
                  }
                  ?>
                  <!-- County Admin Options -->
                  <div id="county-admin-modal" class="reveal-modal" data-reveal>
                    <div class="row">
                      <div class="large-12 columns">
                        <h2 class="global-h2">County Admin Options</h2>
                        <hr />
                      </div>
                    </div>
                    <form action="reporting_county.php">
                      <div class="row">
                        <div class="large-6 columns">
                          <label>County of Administration
                            <select name="county" id="county" required>
                              <option ></option>
                              <option value="Appling" <?php if($info['COUNTY'] == 'Appling'){ echo 'selected=selected';}?>>Appling</option>
                              <option value="Atkinson" <?php if($info['COUNTY'] == 'Atkinson'){ echo 'selected=selected';}?>>Atkinson</option>
                              <option value="Bacon"  <?php if($info['COUNTY'] == 'Bacon'){ echo 'selected=selected';}?>>Bacon</option>
                              <option value="Baker"  <?php if($info['COUNTY'] == 'Baker'){ echo 'selected=selected';}?>>Baker</option>
                              <option value="Baldwin"  <?php if($info['COUNTY'] == 'Baldwin'){ echo 'selected=selected';}?>>Baldwin</option>
                              <option value="Banks"  <?php if($info['COUNTY'] == 'Banks'){ echo 'selected=selected';}?>>Banks</option>
                              <option value="Barrow"  <?php if($info['COUNTY'] == 'Barrow'){ echo 'selected=selected';}?>>Barrow</option>
                              <option value="Bartow"  <?php if($info['COUNTY'] == 'Bartow'){ echo 'selected=selected';}?>>Bartow</option>
                              <option value="Ben Hill"  <?php if($info['COUNTY'] == 'Ben Hill'){ echo 'selected=selected';}?>>Ben Hill</option>
                              <option value="Berrien"  <?php if($info['COUNTY'] == 'Berrien'){ echo 'selected=selected';}?>>Berrien</option>
                              <option value="Bibb"  <?php if($info['COUNTY'] == 'Bibb'){ echo 'selected=selected';}?>>Bibb</option>
                              <option value="Bleckley"  <?php if($info['COUNTY'] == 'Bleckley'){ echo 'selected=selected';}?>>Bleckley</option>
                              <option value="Brantley"  <?php if($info['COUNTY'] == 'Brantley'){ echo 'selected=selected';}?>>Brantley</option>
                              <option value="Brooks"  <?php if($info['COUNTY'] == 'Brooks'){ echo 'selected=selected';}?>>Brooks</option>
                              <option value="Bryan"  <?php if($info['COUNTY'] == 'Bryan'){ echo 'selected=selected';}?>>Bryan</option>
                              <option value="Bulloch"  <?php if($info['COUNTY'] == 'Bulloch'){ echo 'selected=selected';}?>>Bulloch</option>
                              <option value="Burke"  <?php if($info['COUNTY'] == 'Burke'){ echo 'selected=selected';}?>>Burke</option>
                              <option value="Butts"  <?php if($info['COUNTY'] == 'Butts'){ echo 'selected=selected';}?>>Butts</option>
                              <option value="Calhoun"  <?php if($info['COUNTY'] == 'Calhoun'){ echo 'selected=selected';}?>>Calhoun</option>
                              <option value="Camden"  <?php if($info['COUNTY'] == 'Camden'){ echo 'selected=selected';}?>>Camden</option>
                              <option value="Candler"  <?php if($info['COUNTY'] == 'Candler'){ echo 'selected=selected';}?>>Candler</option>
                              <option value="Carroll"  <?php if($info['COUNTY'] == 'Carroll'){ echo 'selected=selected';}?>>Carroll</option>
                              <option value="Catoosa"  <?php if($info['COUNTY'] == 'Catoosa'){ echo 'selected=selected';}?>>Catoosa</option>
                              <option value="Charlton"  <?php if($info['COUNTY'] == 'Charlton'){ echo 'selected=selected';}?>>Charlton</option>
                              <option value="Chatham"  <?php if($info['COUNTY'] == 'Chatham'){ echo 'selected=selected';}?>>Chatham</option>
                              <option value="Chattahoochee" <?php if($info['COUNTY'] == 'Chattahoochee'){ echo 'selected=selected';}?>>Chattahoochee</option>
                              <option value="Chattooga" <?php if($info['COUNTY'] == 'Chattooga'){ echo 'selected=selected';}?>>Chattooga</option>
                              <option value="Cherokee" <?php if($info['COUNTY'] == 'Cherokee'){ echo 'selected=selected';}?>>Cherokee</option>
                              <option value="Clarke"  <?php if($info['COUNTY'] == 'Clarke'){ echo 'selected=selected';}?>>Clarke</option>
                              <option value="Clay" <?php if($info['COUNTY'] == 'Clay'){ echo 'selected=selected';}?>>Clay</option>
                              <option value="Clayton" <?php if($info['COUNTY'] == 'Clayton'){ echo 'selected=selected';}?>>Clayton</option>
                              <option value="Clinch" <?php if($info['COUNTY'] == 'Clinch'){ echo 'selected=selected';}?>>Clinch</option>
                              <option value="Cobb" <?php if($info['COUNTY'] == 'Cobb'){ echo 'selected=selected';}?>>Cobb</option>
                              <option value="Coffee" <?php if($info['COUNTY'] == 'Coffee'){ echo 'selected=selected';}?>>Coffee</option>
                              <option value="Colquitt" <?php if($info['COUNTY'] == 'Colquitt'){ echo 'selected=selected';}?>>Colquitt</option>
                              <option value="Columbia" <?php if($info['COUNTY'] == 'Columbia'){ echo 'selected=selected';}?>>Columbia</option>
                              <option value="Cook" <?php if($info['COUNTY'] == 'Cook'){ echo 'selected=selected';}?>>Cook</option>
                              <option value="Coweta" <?php if($info['COUNTY'] == 'Coweta'){ echo 'selected=selected';}?>>Coweta</option>
                              <option value="Crawford" <?php if($info['COUNTY'] == 'Crawford'){ echo 'selected=selected';}?>>Crawford</option>
                              <option value="Crisp" <?php if($info['COUNTY'] == 'Crisp'){ echo 'selected=selected';}?>>Crisp</option>
                              <option value="Dade" <?php if($info['COUNTY'] == 'Dade'){ echo 'selected=selected';}?>>Dade</option>
                              <option value="Dawson" <?php if($info['COUNTY'] == 'Dawson'){ echo 'selected=selected';}?>>Dawson</option>
                              <option value="Decatur" <?php if($info['COUNTY'] == 'Decatur'){ echo 'selected=selected';}?>>Decatur</option>
                              <option value="DeKalb" <?php if($info['COUNTY'] == 'DeKalb'){ echo 'selected=selected';}?>>DeKalb</option>
                              <option value="Dodge" <?php if($info['COUNTY'] == 'Dodge'){ echo 'selected=selected';}?>>Dodge</option>
                              <option value="Dooly" <?php if($info['COUNTY'] == 'Dooly'){ echo 'selected=selected';}?>>Dooly</option>
                              <option value="Dougherty" <?php if($info['COUNTY'] == 'Dougherty'){ echo 'selected=selected';}?>>Dougherty</option>
                              <option value="Douglas" <?php if($info['COUNTY'] == 'Douglas'){ echo 'selected=selected';}?>>Douglas</option>
                              <option value="Early" <?php if($info['COUNTY'] == 'Early'){ echo 'selected=selected';}?>>Early</option>
                              <option value="Echols" <?php if($info['COUNTY'] == 'Echols'){ echo 'selected=selected';}?>>Echols</option>
                              <option value="Effingham" <?php if($info['COUNTY'] == 'Effingham'){ echo 'selected=selected';}?>>Effingham</option>
                              <option value="Elbert" <?php if($info['COUNTY'] == 'Elbert'){ echo 'selected=selected';}?>>Elbert</option>
                              <option value="Emanuel" <?php if($info['COUNTY'] == 'Emanuel'){ echo 'selected=selected';}?>>Emanuel</option>
                              <option value="Evans" <?php if($info['COUNTY'] == 'Evans'){ echo 'selected=selected';}?>>Evans</option>
                              <option value="Fannin" <?php if($info['COUNTY'] == 'Fannin'){ echo 'selected=selected';}?>>Fannin</option>
                              <option value="Fayette" <?php if($info['COUNTY'] == 'Fayette'){ echo 'selected=selected';}?>>Fayette</option>
                              <option value="Floyd" <?php if($info['COUNTY'] == 'Floyd'){ echo 'selected=selected';}?>>Floyd</option>
                              <option value="Forsyth" <?php if($info['COUNTY'] == 'Forsyth'){ echo 'selected=selected';}?>>Forsyth</option>
                              <option value="Franklin" <?php if($info['COUNTY'] == 'Franklin'){ echo 'selected=selected';}?>>Franklin</option>
                              <option value="Fulton" <?php if($info['COUNTY'] == 'Fulton'){ echo 'selected=selected';}?>>Fulton</option>
                              <option value="Gilmer" <?php if($info['COUNTY'] == 'Gilmer'){ echo 'selected=selected';}?>>Gilmer</option>
                              <option value="Glascock" <?php if($info['COUNTY'] == 'Glascock'){ echo 'selected=selected';}?>>Glascock</option>
                              <option value="Glynn" <?php if($info['COUNTY'] == 'Glynn'){ echo 'selected=selected';}?>>Glynn</option>
                              <option value="Gordon" <?php if($info['COUNTY'] == 'Gordon'){ echo 'selected=selected';}?>>Gordon</option>
                              <option value="Grady" <?php if($info['COUNTY'] == 'Grady'){ echo 'selected=selected';}?>>Grady</option>
                              <option value="Greene" <?php if($info['COUNTY'] == 'Greene'){ echo 'selected=selected';}?>>Greene</option>
                              <option value="Gwinnett" <?php if($info['COUNTY'] == 'Gwinnett'){ echo 'selected=selected';}?>>Gwinnett</option>
                              <option value="Habersham" <?php if($info['COUNTY'] == 'Habersham'){ echo 'selected=selected';}?>>Habersham</option>
                              <option value="Hall" <?php if($info['COUNTY'] == 'Hall'){ echo 'selected=selected';}?>>Hall</option>
                              <option value="Hancock" <?php if($info['COUNTY'] == 'Hancock'){ echo 'selected=selected';}?>>Hancock</option>
                              <option value="Haralson" <?php if($info['COUNTY'] == 'Haralson'){ echo 'selected=selected';}?>>Haralson</option>
                              <option value="Harris" <?php if($info['COUNTY'] == 'Harris'){ echo 'selected=selected';}?>>Harris</option>
                              <option value="Hart" <?php if($info['COUNTY'] == 'Hart'){ echo 'selected=selected';}?>>Hart</option>
                              <option value="Heard" <?php if($info['COUNTY'] == 'Heard'){ echo 'selected=selected';}?>>Heard</option>
                              <option value="Henry" <?php if($info['COUNTY'] == 'Henry'){ echo 'selected=selected';}?>>Henry</option>
                              <option value="Houston" <?php if($info['COUNTY'] == 'Houston'){ echo 'selected=selected';}?>>Houston</option>
                              <option value="Irwin" <?php if($info['COUNTY'] == 'Irwin'){ echo 'selected=selected';}?>>Irwin</option>
                              <option value="Jackson" <?php if($info['COUNTY'] == 'Jackson'){ echo 'selected=selected';}?>>Jackson</option>
                              <option value="Jasper" <?php if($info['COUNTY'] == 'Jasper'){ echo 'selected=selected';}?>>Jasper</option>
                              <option value="Jeff Davis" <?php if($info['COUNTY'] == 'Jeff Davis'){ echo 'selected=selected';}?>>Jeff Davis</option>
                              <option value="Jefferson" <?php if($info['COUNTY'] == 'Jefferson'){ echo 'selected=selected';}?>>Jefferson</option>
                              <option value="Jenkins" <?php if($info['COUNTY'] == 'Jenkins'){ echo 'selected=selected';}?>>Jenkins</option>
                              <option value="Johnson" <?php if($info['COUNTY'] == 'Johnson'){ echo 'selected=selected';}?>>Johnson</option>
                              <option value="Jones" <?php if($info['COUNTY'] == 'Jones'){ echo 'selected=selected';}?>>Jones</option>
                              <option value="Lamar" <?php if($info['COUNTY'] == 'Lamar'){ echo 'selected=selected';}?>>Lamar</option>
                              <option value="Lanier" <?php if($info['COUNTY'] == 'Lanier'){ echo 'selected=selected';}?>>Lanier</option>
                              <option value="Laurens" <?php if($info['COUNTY'] == 'Laurens'){ echo 'selected=selected';}?>>Laurens</option>
                              <option value="Lee" <?php if($info['COUNTY'] == 'Lee'){ echo 'selected=selected';}?>>Lee</option>
                              <option value="Liberty" <?php if($info['COUNTY'] == 'Liberty'){ echo 'selected=selected';}?>>Liberty</option>
                              <option value="Lincoln" <?php if($info['COUNTY'] == 'Lincoln'){ echo 'selected=selected';}?>>Lincoln</option>
                              <option value="Long" <?php if($info['COUNTY'] == 'Long'){ echo 'selected=selected';}?>>Long</option>
                              <option value="Lowndes" <?php if($info['COUNTY'] == 'Lowndes'){ echo 'selected=selected';}?>>Lowndes</option>
                              <option value="Lumpkin" <?php if($info['COUNTY'] == 'Lumpkin'){ echo 'selected=selected';}?>>Lumpkin</option>
                              <option value="Macon" <?php if($info['COUNTY'] == 'Macon'){ echo 'selected=selected';}?>>Macon</option>
                              <option value="Madison" <?php if($info['COUNTY'] == 'Madison'){ echo 'selected=selected';}?>>Madison</option>
                              <option value="Marion" <?php if($info['COUNTY'] == 'Marion'){ echo 'selected=selected';}?>>Marion</option>
                              <option value="McDuffie" <?php if($info['COUNTY'] == 'McDuffie'){ echo 'selected=selected';}?>>McDuffie</option>
                              <option value="McIntosh" <?php if($info['COUNTY'] == 'McIntosh'){ echo 'selected=selected';}?>>McIntosh</option>
                              <option value="Meriwether" <?php if($info['COUNTY'] == 'Meriwether'){ echo 'selected=selected';}?>>Meriwether</option>
                              <option value="Miller" <?php if($info['COUNTY'] == 'Miller'){ echo 'selected=selected';}?>>Miller</option>
                              <option value="Mitchell" <?php if($info['COUNTY'] == 'Mitchell'){ echo 'selected=selected';}?>>Mitchell</option>
                              <option value="Monroe" <?php if($info['COUNTY'] == 'Monroe'){ echo 'selected=selected';}?>>Monroe</option>
                              <option value="Montgomery" <?php if($info['COUNTY'] == 'Montgomery'){ echo 'selected=selected';}?>>Montgomery</option>
                              <option value="Morgan" <?php if($info['COUNTY'] == 'Morgan'){ echo 'selected=selected';}?>>Morgan</option>
                              <option value="Murray" <?php if($info['COUNTY'] == 'Murray'){ echo 'selected=selected';}?>>Murray</option>
                              <option value="Muscogee" <?php if($info['COUNTY'] == 'Muscogee'){ echo 'selected=selected';}?>>Muscogee</option>
                              <option value="Newton" <?php if($info['COUNTY'] == 'Newton'){ echo 'selected=selected';}?>>Newton</option>
                              <option value="Oconee" <?php if($info['COUNTY'] == 'Oconee'){ echo 'selected=selected';}?>>Oconee</option>
                              <option value="Oglethorpe" <?php if($info['COUNTY'] == 'Oglethorpe'){ echo 'selected=selected';}?>>Oglethorpe</option>
                              <option value="Paulding" <?php if($info['COUNTY'] == 'Paulding'){ echo 'selected=selected';}?>>Paulding</option>
                              <option value="Peach" <?php if($info['COUNTY'] == 'Peach'){ echo 'selected=selected';}?>>Peach</option>
                              <option value="Pickens" <?php if($info['COUNTY'] == 'Pickens'){ echo 'selected=selected';}?>>Pickens</option>
                              <option value="Pierce" <?php if($info['COUNTY'] == 'Pierce'){ echo 'selected=selected';}?>>Pierce</option>
                              <option value="Pike" <?php if($info['COUNTY'] == 'Pike'){ echo 'selected=selected';}?>>Pike</option>
                              <option value="Polk" <?php if($info['COUNTY'] == 'Polk'){ echo 'selected=selected';}?>>Polk</option>
                              <option value="Pulaski" <?php if($info['COUNTY'] == 'Pulaski'){ echo 'selected=selected';}?>>Pulaski</option>
                              <option value="Putnam" <?php if($info['COUNTY'] == 'Putnam'){ echo 'selected=selected';}?>>Putnam</option>
                              <option value="Quitman" <?php if($info['COUNTY'] == 'Quitman'){ echo 'selected=selected';}?>>Quitman</option>
                              <option value="Rabun" <?php if($info['COUNTY'] == 'Rabun'){ echo 'selected=selected';}?>>Rabun</option>
                              <option value="Randolph" <?php if($info['COUNTY'] == 'Randolph'){ echo 'selected=selected';}?>>Randolph</option>
                              <option value="Richmond" <?php if($info['COUNTY'] == 'Richmond'){ echo 'selected=selected';}?>>Richmond</option>
                              <option value="Rockdale" <?php if($info['COUNTY'] == 'Rockdale'){ echo 'selected=selected';}?>>Rockdale</option>
                              <option value="Schley" <?php if($info['COUNTY'] == 'Schley'){ echo 'selected=selected';}?>>Schley</option>
                              <option value="Screven" <?php if($info['COUNTY'] == 'Screven'){ echo 'selected=selected';}?>>Screven</option>
                              <option value="Seminole" <?php if($info['COUNTY'] == 'Seminole'){ echo 'selected=selected';}?>>Seminole</option>
                              <option value="Spalding" <?php if($info['COUNTY'] == 'Spalding'){ echo 'selected=selected';}?>>Spalding</option>
                              <option value="Stephens" <?php if($info['COUNTY'] == 'Stephens'){ echo 'selected=selected';}?>>Stephens</option>
                              <option value="Stewart" <?php if($info['COUNTY'] == 'Stewart'){ echo 'selected=selected';}?>>Stewart</option>
                              <option value="Sumter" <?php if($info['COUNTY'] == 'Sumter'){ echo 'selected=selected';}?>>Sumter</option>
                              <option value="Talbot" <?php if($info['COUNTY'] == 'Talbot'){ echo 'selected=selected';}?>>Talbot</option>
                              <option value="Taliaferro" <?php if($info['COUNTY'] == 'Taliaferro'){ echo 'selected=selected';}?>>Taliaferro</option>
                              <option value="Tattnall" <?php if($info['COUNTY'] == 'Tattnall'){ echo 'selected=selected';}?>>Tattnall</option>
                              <option value="Taylor" <?php if($info['COUNTY'] == 'Taylor'){ echo 'selected=selected';}?>>Taylor</option>
                              <option value="Telfair" <?php if($info['COUNTY'] == 'Telfair'){ echo 'selected=selected';}?>>Telfair</option>
                              <option value="Terrell" <?php if($info['COUNTY'] == 'Terrell'){ echo 'selected=selected';}?>>Terrell</option>
                              <option value="Thomas" <?php if($info['COUNTY'] == 'Thomas'){ echo 'selected=selected';}?>>Thomas</option>
                              <option value="Tift" <?php if($info['COUNTY'] == 'Tift'){ echo 'selected=selected';}?>>Tift</option>
                              <option value="Toombs" <?php if($info['COUNTY'] == 'Toombs'){ echo 'selected=selected';}?>>Toombs</option>
                              <option value="Towns" <?php if($info['COUNTY'] == 'Towns'){ echo 'selected=selected';}?>>Towns</option>
                              <option value="Treutlen" <?php if($info['COUNTY'] == 'Treutlen'){ echo 'selected=selected';}?>>Treutlen</option>
                              <option value="Troup" <?php if($info['COUNTY'] == 'Troup'){ echo 'selected=selected';}?>>Troup</option>
                              <option value="Turner" <?php if($info['COUNTY'] == 'Turner'){ echo 'selected=selected';}?>>Turner</option>
                              <option value="Twiggs" <?php if($info['COUNTY'] == 'Twiggs'){ echo 'selected=selected';}?>>Twiggs</option>
                              <option value="Union" <?php if($info['COUNTY'] == 'Union'){ echo 'selected=selected';}?>>Union</option>
                              <option value="Upson" <?php if($info['COUNTY'] == 'Upson'){ echo 'selected=selected';}?>>Upson</option>
                              <option value="Walker" <?php if($info['COUNTY'] == 'Walker'){ echo 'selected=selected';}?>>Walker</option>
                              <option value="Walton" <?php if($info['COUNTY'] == 'Walton'){ echo 'selected=selected';}?>>Walton</option>
                              <option value="Ware" <?php if($info['COUNTY'] == 'Ware'){ echo 'selected=selected';}?>>Ware</option>
                              <option value="Warren" <?php if($info['COUNTY'] == 'Warren'){ echo 'selected=selected';}?>>Warren</option>
                              <option value="Washington" <?php if($info['COUNTY'] == 'Washington'){ echo 'selected=selected';}?>>Washington</option>
                              <option value="Wayne" <?php if($info['COUNTY'] == 'Wayne'){ echo 'selected=selected';}?>>Wayne</option>
                              <option value="Webster" <?php if($info['COUNTY'] == 'Webster'){ echo 'selected=selected';}?>>Webster</option>
                              <option value="Wheeler" <?php if($info['COUNTY'] == 'Wheeler'){ echo 'selected=selected';}?>>Wheeler</option>
                              <option value="White" <?php if($info['COUNTY'] == 'White'){ echo 'selected=selected';}?>>White</option>
                              <option value="Whitfield" <?php if($info['COUNTY'] == 'Whitfield'){ echo 'selected=selected';}?>>Whitfield</option>
                              <option value="Wilcox" <?php if($info['COUNTY'] == 'Wilcox'){ echo 'selected=selected';}?>>Wilcox</option>
                              <option value="Wilkes" <?php if($info['COUNTY'] == 'Wilkes'){ echo 'selected=selected';}?>>Wilkes</option>
                              <option value="Wilkinson" <?php if($info['COUNTY'] == 'Wilkinson'){ echo 'selected=selected';}?>>Wilkinson</option>
                              <option value="Worth" <?php if($info['COUNTY'] == 'Worth'){ echo 'selected=selected';}?>>Worth</option>
                            </select>
                          </label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="large-6 columns">
                          <!--<a href="#" class="button expand tiny">Generate Report on Groups</a>-->
                          <input type="submit" class="button expand tiny" value="Generate County Report">

                        </div>
                      </div>
                    </form>
                    <a class="close-reveal-modal">&#215;</a>
                  </div>
                  <!-- End County Admin Options -->
                </div>
                <!-- End Desktop "Log an Activity" & Admin Tools Button -->
              </div>
              <div class="large-7 medium-6 small-12 columns">

                <!-- Quick Stats -->
                  <?php
                  $list = ACTIVITY::points_breakdown($ID);
                  ?>
                  <!-- End Quick Stats -->
                  <!--Goal Modal-->
                  <div id="goalModal" class="reveal-modal" style="top: 0" data-reveal>
                    <form id="modal-form" action="create_goal.php" method="post" data-abide>
                      <div class="row">
                        <div class="large-6 columns" >
                          <h2 class="custom-font-small-blue">Create a goal!</h2>
                        </div>
                        <div class="large-6 columns" >
                          <p class="font -standard -primary">
                            For guidance on how to define your goals, click <a target="_blank" href="http://www.cdc.gov/physicalactivity/growingstronger/motivation/define.html">here</a>.
                          </p>
                        </div>
                        <hr />
                      </div>
                      <!-- MAIN ROW -->
                      <!-- GOAL START DATE -->
                      <div class="row">
                        <div class="large-12 medium-12 columns tc-ns">
                          <h3 class="global-h2">Goal Start Date:</h3>
                          <p class="mb font -standard -primary">Goal for the Week of <br><span class="startDate"><?= $today; ?></span> - <span class="endDate"><?= $nextWeek; ?></span></p>
                        </div>
                      </div>
                      <div class="row collapse">
                        <div class="medium-6 small-6 columns">
                          <div id="ck-button" style="float:right">
                            <label>
                              <input type="radio" id="start" name="start" value="<?= date("Y-m-d") ?>" checked="checked" onclick="dateRange(this.value)"><span class="grade font -extra-small -primary" style="width: 100%" >Start <br>Today</span>
                            </label>
                          </div>
                        </div>
                        <div class="medium-6 small-6 columns">
                          <div id="ck-button">
                            <label>
                              <input type="radio" id="start" name="start" value="<?= date('Y-m-d', strtotime("+1 day", strtotime($today))) ?>" onclick="dateRange(this.value)"><span class="grade font -extra-small -primary" style="width: 100%">Start Tomorrow</span>
                            </label>
                          </div>
                          <input type="hidden" name="end" id="end" class="end" value="<?= date('Y-m-d', strtotime("+7 day", strtotime($today))) ?>"/>
                        </div>
                      </div>
                      <!-- MEDIUM -->
                      <div class="pt1">
                        <div class="row">
                          <div class="large-12 medium-12 columns">
                            <table class="center-m">
                              <thead>
                                <tr>
                                  <th width="200">Primary Activities</th>
                                  <th width="150">Times/ Week</th>
                                  <th width="150">Minutes/ Session</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>
                                    <label>
                                      <select id="exercise" onchange="primaryActivityChange(this)" class="exercise" required>
                                        <?php
                                        $aid = odbc_result(MSSQL::query('SELECT TOP 1 AL_AID FROM LOG WHERE AL_UID =\'' . $_SESSION['ID'] . '\' ORDER BY AL_DATE DESC'), 'AL_AID');
                                        $list = ACTIVITY::select_first_activity($aid);
                                        ?>
                                      </select>
                                    </label>
                                  </td>
                                  <td>
                                    <select type="select" id="frequency" name="frequency" class="frequency" onchange="pointsChange()" required>
                                      <option disabled selected></option>
                                      <option value="2">2</option>
                                      <option value="3">3</option>
                                      <option value="4">4</option>
                                      <option value="5">5</option>
                                      <option value="6">6</option>
                                    </select>
                                  </td>
                                  <td>
                                    <select type="select" id="minutes" name="minutes" class="minutes" onchange="pointsChange()" required>
                                      <option disabled selected></option>
                                      <option value="10">10</option>
                                      <option value="20">20</option>
                                      <option value="30">30</option>
                                      <option value="40">40</option>
                                      <option value="50">50</option>
                                      <option value="60">60</option>
                                      <option value="70">70</option>
                                      <option value="80">80</option>
                                      <option value="90">90</option>
                                    </select>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <label>
                                      <select id="exercise2" name="exercise2" class="exercise2" onchange="secondaryActivityChange(this)">
                                        <?php
                                        $list = ACTIVITY::select_second_activity();
                                        ?>
                                      </select>
                                    </label>
                                  </td>
                                  <td>
                                    <select type="select" id="frequency2" name="frequency2" class="frequency2" onchange="pointsChange()">
                                      <option disabled selected></option>
                                      <option value="2">2</option>
                                      <option value="3">3</option>
                                      <option value="4">4</option>
                                      <option value="5">5</option>
                                      <option value="6">6</option>
                                    </select>
                                  </td>
                                  <td>
                                    <select type="select" id="minutes2" name="minutes2" class="minutes2" onchange="pointsChange()">
                                      <option disabled selected></option>
                                      <option value="10">10</option>
                                      <option value="20">20</option>
                                      <option value="30">30</option>
                                      <option value="40">40</option>
                                      <option value="50">50</option>
                                      <option value="60">60</option>
                                      <option value="70">70</option>
                                      <option value="80">80</option>
                                      <option value="90">90</option>
                                    </select>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <!-- END MEDIUM -->
                      <!-- PERSONAL PLAN -->
                      <div class="row">
                        <div class="large-2 columns show-for-large-up">
                          <!-- Avatar -->
                          <?php ACCOUNT::avatar_display_only($_SESSION['ID']); ?>
                          <!-- End Avatar -->
                        </div>
                        <div class="large-10 medium-12 columns">
                          <!--  <h2 class="font -blue -small -uppercase -bold">Personal Plan</h2>-->
                          <h3 class="global-h2">My Goal is to earn <span class="font -small -underline pointsTotal">__</span> points between now and <span class="endDate"><?= $nextWeek; ?></span>! </h3>
                          <p class="font -standard -primary -black" id="sentence">
                            I plan on exercising <span class="font -small  -underline -secondary sessionsPerGoal">_</span> times this week for a total of <span class="font -small  -secondary -underline goalTime">__</span> minutes. Move more and live more by gaining <span class="font -small -secondary -underline pointsTotal">__</span> points through <span class="allActivities"><span class="font -secondary primaryActivity">____</span><span class="font -primary activityAnd"></span><span class="font -secondary secondaryActivity"></span></span>!
                          </p>
                          <input type="hidden" name="activity" id="activity" class="finalActivity" value=""/>
                          <input type="hidden" name="totalFrequency" id="totalFrequency" class="totalFrequency" value=""/>
                          <input type="hidden" name="sentenceInput" id="sentenceInput" class="sentenceInput" value=""/>
                          <input type="hidden" name="points" id="points" class="points" value=""/>
                          <input type="hidden" name="goalCount" id="goalCount" class="goalCount" value="<?= $goalCount ?>"/>
                        </div>
                      </div>
                      <div class="row">
                        <div class="large-10 small-12 large-offset-2 columns show-for-small-only">
                          <div data-alert class="alert-box secondary surgeonGeneral" style="display: none;">
                            That's a good start! The Surgeon General recommends engaging in at least 150 minutes of moderate-intensity activity each week.
                            <a href="#" class="close">&times;</a>
                          </div>
                        </div>
                      </div>
                      <div class="row show-for-medium-up">
                        <div class="large-10 small-12 large-offset-2 columns">
                          <div data-alert class="alert-box secondary">
                            That's a good start! The Surgeon General recommends engaging in at least 150 minutes of moderate-intensity activity each week.
                            <a href="#" class="close">&times;</a>
                          </div>
                        </div>
                      </div>
                      <!-- END PERSONAL PLAN -->
                      <!-- CONFIDENCE LEVEL -->
                      <div class="row">
                        <div class="medium-12 columns">
                          <h3 class="global-h2">Confidence Level:</h3>
                          <label class="font -standard -primary -black">On a scale from 1 to 10, how confident do you feel about achieving your goal?
                            <select name="sd" id="sd" onchange="difficulty(this.value)" required>
                              <option disabled selected></option>
                              <option value="1">1 - Not confident at all</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                              <option value="6">6</option>
                              <option value="7">7</option>
                              <option value="8">8</option>
                              <option value="9">9</option>
                              <option value="10">10 - Totally confident</option>
                            </select>
                          </label>
                          <p class="font -primary">A goal cannot be revised for 7 days, or until the goal is accomplished.</p>
                          <div id="ck-button" class="confirm" style="float:left">
                            <label>
                              <input type="button" id="confirm" name="confirm" onclick="confirmButton()"><span class="grade font -extra-small -primary" style="width: 100%; padding: .25rem 1rem; color: white; background-color: #008cba" >Confirm Goal?</span>
                            </label>
                          </div>
                          <input type="submit"  class="small button success submitReady" value="Set my Goal!" style="display: none;"/>
                        </div>
                      </div>
                      <!-- Confirm Modal -->
                      <div id="confirmModal" class="reveal-modal small" data-reveal>
                        <a href="#" class="button success small" data-reveal-id="confirmModal">Set weekly goal!</a>
                        <a href="#" class="button alert small" data-reveal-id="goalModal">Whoops, take me back to change some things!</a>
                        <p class="font -standard -primary -black" id="sentence">
                          I plan on exercising <span class="font -small  -underline -secondary sessionsPerGoal">_</span> times this week for a total of <span class="font -small  -secondary -underline goalTime">__</span> minutes. Move more and live more by gaining <span class="font -small -secondary -underline pointsTotal">__</span> points through <span class="font -secondary primaryActivity">____</span><span class="font -primary activityAnd"></span><span class="font -secondary secondaryActivity"></span>!
                        </p>
                      </div>
                      <!-- End Confirm Modal-->
                    </form>
                    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                  </div>
                  <!-- /GOAL MODAL -->

                <!-- Exercise Chart -->
                <div class="row">

                  <div class="columns medium-11 large-10">
                    <canvas id="exerciseChart" width="100%" height="100%"></canvas>
                    <canvas id="exerciseChartMonth" width="100%" height="100%" style="display:none;"></canvas>
                  </div>
                  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.bundle.min.js"></script>
                  <?php
                  $date_month = date("Y-m-d", strtotime("today -1 month"));
                  $date_31 = date("Y-m-d", strtotime("today -31 days"));
                  $date_30 = date("Y-m-d", strtotime("today -30 days"));
                  $date_29 = date("Y-m-d", strtotime("today -29 days"));
                  $date_28 = date("Y-m-d", strtotime("today -28 days"));
                  $date_27 = date("Y-m-d", strtotime("today -27 days"));
                  $date_26 = date("Y-m-d", strtotime("today -26 days"));
                  $date_25 = date("Y-m-d", strtotime("today -25 days"));
                  $date_24 = date("Y-m-d", strtotime("today -24 days"));
                  $date_23 = date("Y-m-d", strtotime("today -23 days"));
                  $date_22 = date("Y-m-d", strtotime("today -22 days"));
                  $date_21 = date("Y-m-d", strtotime("today -21 days"));
                  $date_20 = date("Y-m-d", strtotime("today -20 days"));
                  $date_19 = date("Y-m-d", strtotime("today -19 days"));
                  $date_18 = date("Y-m-d", strtotime("today -18 days"));
                  $date_17 = date("Y-m-d", strtotime("today -17 days"));
                  $date_16 = date("Y-m-d", strtotime("today -16 days"));
                  $date_15 = date("Y-m-d", strtotime("today -15 days"));
                  $date_14 = date("Y-m-d", strtotime("today -14 days"));
                  $date_13 = date("Y-m-d", strtotime("today -13 days"));
                  $date_12 = date("Y-m-d", strtotime("today -12 days"));
                  $date_11 = date("Y-m-d", strtotime("today -11 days"));
                  $date_10 = date("Y-m-d", strtotime("today -10 days"));
                  $date_9 = date("Y-m-d", strtotime("today -9 days"));
                  $date_8 = date("Y-m-d", strtotime("today -8 days"));
                  $date_7 = date("Y-m-d", strtotime("today -7 days"));
                  $date_6 = date("Y-m-d", strtotime("today -6 days"));
                  $date_5 = date("Y-m-d", strtotime("today -5 days"));
                  $date_4 = date("Y-m-d", strtotime("today -4 days"));
                  $date_3 = date("Y-m-d", strtotime("today -3 days"));
                  $date_2 = date("Y-m-d", strtotime("today -2 days"));
                  $date_1 = date("Y-m-d", strtotime("today -1 day"));
                  $date_0 = date("Y-m-d", strtotime("today"));
                  $date_range = 'AND AL_DATE >= \''.$date_month.'\'';
                  $sql = 'SELECT * FROM LOG WHERE AL_UID = \''.$ID.'\' '.$date_range.' ORDER BY AL_DATE ASC';
                  $query = MSSQL::query($sql);
                  $chart_labels = [$date_6, $date_5, $date_4, $date_3, $date_2, $date_1, $date_0];
                  $chart_datasets = [];
                  $chart_datasets[0] = new stdClass();
                  $chart_datasets[1] = new stdClass();
                  $chart_datasets[2] = new stdClass();
                  $chart_datasets[0]->label = "Points";
                  $chart_datasets[1]->label = "Time";
                  $chart_datasets[2]->label = "Distance";
                  $chart_datasets[0]->backgroundColor = "rgba(0,140,186,0.4)";
                  $chart_datasets[1]->backgroundColor = "rgba(67,172,106,0.4)";
                  $chart_datasets[2]->backgroundColor = "rgba(255,188,130,0.4)";
                  $chart_datasets[0]->borderColor = "rgba(0,140,186,1)";
                  $chart_datasets[1]->borderColor = "rgba(67,172,106,1)";
                  $chart_datasets[2]->borderColor = "rgba(255,188,130,1)";

                  $chart_month_labels = [$date_31, $date_30, $date_29, $date_28, $date_27, $date_26, $date_25, $date_24, $date_23, $date_22, $date_21, $date_20, $date_19, $date_18, $date_17, $date_16, $date_15, $date_14, $date_13, $date_12, $date_11, $date_10, $date_9, $date_8, $date_7, $date_6, $date_5, $date_4, $date_3, $date_2, $date_1, $date_0];
                  $chart_month_datasets = [];
                  $chart_month_datasets[0] = new stdClass();
                  $chart_month_datasets[1] = new stdClass();
                  $chart_month_datasets[2] = new stdClass();
                  $chart_month_datasets[0]->label = "Points";
                  $chart_month_datasets[1]->label = "Time";
                  $chart_month_datasets[2]->label = "Distance";
                  $chart_month_datasets[0]->backgroundColor = "rgba(0,140,186,0.4)";
                  $chart_month_datasets[1]->backgroundColor = "rgba(67,172,106,0.4)";
                  $chart_month_datasets[2]->backgroundColor = "rgba(255,188,130,0.4)";
                  $chart_month_datasets[0]->borderColor = "rgba(0,140,186,1)";
                  $chart_month_datasets[1]->borderColor = "rgba(67,172,106,1)";
                  $chart_month_datasets[2]->borderColor = "rgba(255,188,130,1)";

                  $chart_labels_count = count($chart_labels);
                  $chart_month_labels_count = count($chart_month_labels);
                  if(odbc_num_rows($query) > 0) {
                    while(odbc_fetch_row($query)) {
                      $aid = odbc_result($query, 'AL_AID');
                      $id = odbc_result($query, 'AL_ID');
                      //$a = ACTIVITY::activity_to_form($aid);
                      $time = odbc_result($query, 'AL_TIME');
                      $sd_num = odbc_result($query, 'AL_SD');
                      //$sd = strtolower(ACTIVITY::difficulty_to_form($sd_num));
                      $pa = odbc_result($query, 'AL_PA');
                      $date = odbc_result($query, 'AL_DATE');
                      $unit = odbc_result($query, 'AL_UNIT');
                      for($x = 0; $x < $chart_labels_count; $x++) {
                        if(!isset($chart_datasets[0]->data[$x])) {
                          $chart_datasets[0]->data[$x] = 0;
                        }
                        if(!isset($chart_datasets[1]->data[$x])) {
                          $chart_datasets[1]->data[$x] = 0;
                        }
                        if(!isset($chart_datasets[2]->data[$x])) {
                          $chart_datasets[2]->data[$x] = 0;
                        }
                        if($date == $chart_labels[$x]) {
                          $chart_datasets[0]->data[$x] += $pa;
                          $chart_datasets[1]->data[$x] += ($time / 60);
                          $chart_datasets[2]->data[$x] += $unit;
                        }
                      }
                      for($x = 0; $x < $chart_month_labels_count; $x++) {
                        if(!isset($chart_month_datasets[0]->data[$x])) {
                          $chart_month_datasets[0]->data[$x] = 0;
                        }
                        if(!isset($chart_month_datasets[1]->data[$x])) {
                          $chart_month_datasets[1]->data[$x] = 0;
                        }
                        if(!isset($chart_month_datasets[2]->data[$x])) {
                          $chart_month_datasets[2]->data[$x] = 0;
                        }
                        if($date == $chart_month_labels[$x]) {
                          $chart_month_datasets[0]->data[$x] += $pa;
                          $chart_month_datasets[1]->data[$x] += ($time / 60);
                          $chart_month_datasets[2]->data[$x] += $unit;
                        }
                      }
                    }
                  }
                  else {
                    for($x = 0; $x < $chart_labels_count; $x++) {
                      if(!isset($chart_datasets[0]->data[$x])) {
                        $chart_datasets[0]->data[$x] = 0;
                      }
                      if(!isset($chart_datasets[1]->data[$x])) {
                        $chart_datasets[1]->data[$x] = 0;
                      }
                      if(!isset($chart_datasets[2]->data[$x])) {
                        $chart_datasets[2]->data[$x] = 0;
                      }
                    }
                    for($x = 0; $x < $chart_month_labels_count; $x++) {
                      if(!isset($chart_month_datasets[0]->data[$x])) {
                        $chart_month_datasets[0]->data[$x] = 0;
                      }
                      if(!isset($chart_month_datasets[1]->data[$x])) {
                        $chart_month_datasets[1]->data[$x] = 0;
                      }
                      if(!isset($chart_month_datasets[2]->data[$x])) {
                        $chart_month_datasets[2]->data[$x] = 0;
                      }
                    }
                  }
                  ?>
                </div>

                <div class="row">
                  <div class="columns small-12">
                    <p>
                      <span>View activities from
                        <select name="exercise_data_select" id="exercise_data_select">
                          <option value="this_week" selected="">this week</option>
                          <option value="this_month">this month</option>
                        </select>
                      </span>
                    </p>
                  </div>
                </div>
                <!-- End Exercise Chart -->
              </div>
              </div>

              <div class="row">
                <div class="large-8 medium-8 columns">
                  <!-- Day by Day Activity Log -->
                  <?php
                  $list = ACTIVITY::display_activity($ID, '7', '');
                  $form = ACTIVITY::activity_form();
                  ?>
                  <!-- End Day by Day Activity Log -->
                </div>
                <div class="large-4 medium-4 columns" style="margin-top:16px;">
                  <h2 class="custom-font-small-blue">News</h2>
                  <hr />
                  <div class="global-p">
                    <script language="JavaScript" src="feed2js/feed2js.php?src=http%3A%2F%2Fblog.extension.uga.edu%2Fwalkgeorgia%2Ffeed%2F&chan=y&num=3&desc=100>1&targ=y&utf=y"  charset="UTF-8" type="text/javascript"></script>
                  </div>
                  <noscript>
                    <a href="feed2js/feed2js.php?src=http%3A%2F%2Fblog.extension.uga.edu%2Fwalkgeorgia%2Ffeed%2F&chan=y&num=3&desc=100>1&targ=y&utf=y&html=y">View RSS feed</a>
                  </noscript>
                </div>
              </div>

              <div class="row">
                <div class="large-6 medium-6 columns">
                  <h2 class="custom-font-small-blue">Help Center</h2>
                  <hr style="margin-top:-5px;" />
                  <ul style="list-style:none;">
                    <li><a href="/faq.php">Need help? View our FAQ!</a></li>
                    <li><a href="/faq.php#I1">Where's the map?</a></li>
                    <li><a href="/resources/county-resources.php">Find local resources in my county.</a></li>
                  </ul>
                </div>
              </div>
              <!-- End Main Section -->
            </div>
          </div>
          <!-- End Main Section -->

          <!-- Joyride -->
          <ol class="joyride-list" data-joyride>

            <li data-text="Next">
              <br />
              <p>Welcome to walk Georgia!  Here is a quick tour of your profile.</p>
            </li>

            <li data-text="Next">
              <br />
              <p>On the left you can click on the default picture to upload one of your own.  Below that, you can click "Log an Activity" to get started.</p>
            </li>

            <li data-text="Next">
              <p>Once you log an activity, your data will start to show up in the "past activity" section.  Clicking on one of your past activities will allow you to edit it if you entered it incorrectly the first time.</p>
            </li>

            <li data-text="Next">
              <p>The quick stats section lets you keep track of your progress.  Clicking on any of the sections will show a breakdown of where your total points, time, and distance came from.</p>
            </li>

            <li data-text="Next">
              <br />
              <p>The Groups section lets you create or join groups to get connected with friends, co-workes, or relatives.  Groups can help you keep track of the activity of multiple users at a time, as well as encourage friendly competition.</p>
            </li>

            <li data-button="End">
              <br />
              <p>That does it for our tour! Before you REALLY get going logging activity, we've got an optional survey to help make our site better.  <a href="https://ugeorgia.qualtrics.com/jfe/form/SV_6RPBzM9OKJgtW1D" target="_blank">If you'd like to participate in the survey, click here now</a>.  If not, continue on to your new account, move more, and live more!</p>
            </li>
          </ol>
          <!-- End Joyride -->

          <div id="facebookShare" class="reveal-modal tiny" data-reveal aria-labelledby="facebookShare" aria-hidden="true" role="dialog">
            <div class="row center">
              <div class="large-12 columns">
                <h3 class="custom-font-small-blue">You Logged:</h3>
                <h3 class="custom-font-big-blue" style="margin-top:0;"><?= filter_input(INPUT_GET, 'pa', FILTER_SANITIZE_STRING); ?></h3>
                <h4 class="global-h2" style="margin-top:-1.5em;">Points</h4>
                <?php
                //growing span here
                if($tomorrow_count != 1 && $goalCount >= 1){
                  ?>
                  <h2 class="font -medium -secondary -blue "><a href="http://<?php echo $HTTP_HOST; ?>/my-progress.php">My Goal</a></h2>
                  <div class="progress tiny success round mt1">
                    <?php if ($percent_complete > 100){$span = 100;}else{$span = $percent_complete;} ?>
                    <span class="meter font -white" style="width: <?= $span ?>%;"><?= $percent_complete ?>%</span>
                  </div>
                  <?php
                }
                if($_SESSION['completedGoal'] == 1){

                  $_SESSION['completedGoal'] = 0;

                  ?>

                  <h2 class="font -medium -secondary -blue "><a href="http://<?php echo $HTTP_HOST; ?>/my-progress.php">Goal Complete</a></h2>
                  <div class="center-stars">
                    <ul>
                      <li>
                        <div id="star-award-left">
                          <i class="fi-star"></i>
                        </div>
                      </li>


                      <li>
                        <div id="star-award-right">
                          <i class="fi-star"></i>
                        </div>
                      </li>
                      <li>
                        <div id="star-award-left">
                          <i class="fi-star"></i>
                        </div>
                      </li>

                      <li>
                        <div id="star-award-right">
                          <i class="fi-star"></i>
                        </div>
                      </li>
                      <li>
                        <div id="star-award-left">
                          <i class="fi-star"></i>
                        </div>
                      </li>

                    </ul>
                  </div>
                  <div class="progress tiny success round mt1">
                    <?php if ($percent_complete > 100){$span = 100;}else{$span = $percent_complete;} ?>
                    <span class="meter font -white" style="width: <?= $span ?>%;"><?= $percent_complete ?>%</span>
                  </div>
                  <?php
                }
                ?>
                <hr />
                <a href="http://<?php echo $HTTP_HOST; ?>" class="button small round">Take Me Back to My Activity</a>
                <br>
                <a href="http://<?php echo $HTTP_HOST; ?>/map/map.php" class="button small round success">Take Me to My Map</a>
                <?php
                if($goalCount == 0){
                  ?>
                  <a href="http://<?php echo $HTTP_HOST; ?>/my-progress.php" class="button small round peachButton">Set a Goal</a>
                  <?php
                }else if($goalCount >= 1){
                  ?>
                  <a href="http://<?php echo $HTTP_HOST; ?>/my-progress.php" class="button small round peachButton">Take Me to My Goal</a>
                  <?php
                }
                ?>

                <hr style="margin-top:-.3em;" />
              </div>
            </div>

            <div class="row center">
              <div class="large-12 columns">
                Share on Facebook: <div class="fb-share-button" data-href="http://www.walkgeorgia.com?name=<?php echo $info['FNAME']; ?>&points=<?php echo filter_input(INPUT_GET, 'pa', FILTER_SANITIZE_STRING); ?>" data-layout="button"></div>
              </div>
            </div>
          </div>
          <?php
        }
        //If user is not logged in
        else{
          ?>


          <!-- MASTHEAD -->
          <div class="home-masthead cf w-100">
            <!-- <div id="index-slideshow" class="dn db-ns ">
            <div class="slide" style="width:100%;">
            <img src="img/chatt-bend-hiking-tower-bw.jpg" class="slide-image" style="" alt="image of georgia landscape"/>
          </div>
          <div class="slide" style="width:100%;">
          <img src="img/don-carter-kayak-bw.jpg" style="max-width: none; min-width:100%; height:100vh;" alt="image of georgia landscape">
        </div>
        <div class="slide" style="width:100%;">
        <img src="img/kolomoki-camping-fishing.jpg" style="max-width: none; min-width:100%; height:100vh;" alt="image of georgia landscape">
      </div>
      <div class="slide" style="width:100%;">
      <img src="img/cloudland-canyon-biking.jpg" style="max-width: none; min-width:100%; height:100vh" alt="image of georgia landscape">
    </div>
    <div class="slide" style="width:100%;">
    <img src="img/fdr-hiking-father-son.jpg" style="max-width: none; min-width:100%; height:100vh" alt="image of spanish moss in goeorgia">
  </div>
  <div class="slide" style="width:100%;">
  <img src="img/fdr-hiking.jpg" style="max-width: none; min-width:100%; height:100vh;" alt="image of georgia landscape">
</div>
<div class="slide" style="width:100%;">
<img src="img/kolomoki-corn-hole.jpg" style="max-width: none; min-width:100%; height:100vh;" alt="image of georgia landscape">
</div>
</div> -->
<div class="mw7-ns center fl-l tl pa5-ns pa3 ">
  <div class="ph5-ns pa3  br1 " style="border-bottom: 10px solid white; border-top: 10px solid white; background-color: rgba(0, 140, 186, .6)">
    <img src="img/wg-logo-white-no-tagline.png" class="db mb2" />
    <div class="ff-primary bg-white mb3 br1 ph4-ns pa3 w-100">
      <h1 class="ff-secondary f3-ns f4 mb1-ns gray">
        Record. Connect. Discover.
      </h1>
      <p class="lh-copy black-60">
        With WalkGeorgia.org you can record all your physical activity, connect and engage your community for competition or fitness accountability, and discover new resources to stay healthy and active.
      </p>
    </div>
    <div class="center">
      <a href="login.php" class="ff-primary button radius secondary w-40-l w-100 ttu">Log In</a>
      <a href="register.php" class="ff-primary button radius success w-40-l w-100 ml3-l ttu">Sign Up</a>
    </div>
  </div>
</div>
</div>
<!-- EVENT -->
<!-- RECORD   -->
<div class="gradient-home">

  <section class="cf pa5-ns pa3 mw9-ns center">
    <div class="fl w-50-ns w-100 tl">
      <h2 class="ff-secondary black-40 f2-ns f3 lh-title measure">
        <span class="db black-10 home-numbers ff-primary">1</span>
        <span class="f1-ns f2 ttu blue">Record</span>
        <br>
        <span class="white">your physical activity</span>
      </h2>
      <p class="ff-primary f3-ns f4 lh-copy measure pr3-ns">
        Do you sow seeds or pound pavement? Walk Georgia can help you set goals and track your physical activity progress, no matter what your favorite recreation.
      </p>
      <a href="faq.php" class="button primary radius ff-primary ttu">Learn More</a>
    </div>
    <div class="fl w-50-ns w-100 ">
      <img src="img/app-screenshot-fade.png" alt="" />
    </div>
  </section>

  <!-- CONNECT   -->
  <div class="bt b--black-20"></div>

  <section class="cf pa5-ns pa3 mw9-ns center">
    <div class="fl w-50-ns w-100">
      <img class="dn db-ns"src="img/groups-screenshot.png" alt="" />
    </div>
    <div class="fl w-50-ns w-100 tl pl3-ns">
      <h2 class="ff-secondary black-40 f2-ns f3 lh-title measure">
        <span class="db black-10 home-numbers ff-primary">2</span>
        <span class="f1-ns f2 ttu blue">Connect</span>
        <br>
        <span class="white">with your community</span>
      </h2>
      <p class="ff-primary f3-ns f4 lh-copy measure">
        Walk Georgia offers the power to form groups, which allows you to create and join communities of people who want to move more.
      </p>
      <p class="ff-primary f3-ns f4 lh-copy measure">
        Want to organize a walking challenge at work? Or what about a family competition? Our website helps you create the perfect group for tracking any type of group wellness initiative.
      </p>
      <a href="resources/county-resources.php" class="button primary radius ff-primary ttu">Find us in your area</a>
    </div>
  </section>


  <div class="bt b--black-20"></div>
  <!-- DISCOVER -->
  <section class="cf pa5-ns pa3 mw9-ns center">
    <div class="fl w-50-ns w-100 tl">
      <h2 class="ff-secondary black-40 f2-ns f3 lh-title measure">
        <span class="db black-10 home-numbers ff-primary">3</span>
        <span class="f1-ns f2 ttu blue">Discover</span>
        <br>
        <span class="white">your potential</span>
      </h2>
      <p class="ff-primary f3-ns f4 lh-copy measure">
        From simple meals to simple moves, Walk Georgia’s resources help you discover easy ways to pursue health.
      </p>
      <p class="ff-primary f3-ns f4 lh-copy measure">
        Walk Georgia’s catalogue of resources contains local county parks and recreation centers, fitness demos, delicious recipes, activities for kids, and so much more!
      </p>
      <a href="resources/index.php" class="button primary radius ff-primary ttu">View our resources</a>
    </div>
    <div class="fl w-50-ns w-100 pl3-ns pt5-ns">
      <img class="dn db-ns br1"src="img/don-carter-kayak-bw.jpg" alt="" />
    </div>
  </section>

</div>
<div class="bt b--black-20"></div>
<!-- SIGN UP/LOGIN -->
<section class="signup-home">
  <div class="mw9-ns center pa5-ns pa3 ">
    <div class="ph5-ns pa3  br1 " style="border-bottom: 10px solid rgba(0, 140, 186, .6); border-top: 10px solid rgba(0, 140, 186, .6); background-color:white ">
      <img src="img/logo-black.png" class="dib" />
      <img src="img/extension.png" class="dib ml3-l" alt="" />
      <!-- <p class="f5 ff-primary pt3 tc-ns tl">
        Walk Georgia is supported in part by a grant from The Coca-Cola Foundation.
      </p> -->
      <div class="center mt3">
        <a href="login.php" class="ff-primary button radius secondary w-25-l w-100 ttu">Log In</a>
        <a href="register.php" class="ff-primary button radius success w-25-l w-100 ml3-l ttu">Sign Up</a>

      </div>
    </div>
  </div>
</section>

<div class="bt b--black-20"></div>
<!-- Begin news section -->
<div class="pa3 pa5-ns mw7-ns center tl">
  <h2 class="tc ma0 pt0 black-40 font -big -lowercase -secondary hide-for-small-only">news</h2>
  <h2 class="tc font -blue -medium -lowercase -secondary show-for-small-only">news<h2>
    <div class="news">
      <div class="ff-primary">
        <script language="JavaScript" src="feed2js/feed2js.php?src=http%3A%2F%2Fblog.extension.uga.edu%2Fwalkgeorgia%2Ffeed%2F&chan=y&num=3&desc=100>1&targ=y&utf=y"  charset="UTF-8" type="text/javascript"></script>
      </div>
    </div>
    <noscript>
      <a href="feed2js/feed2js.php?src=http%3A%2F%2Fblog.extension.uga.edu%2Fwalkgeorgia%2Ffeed%2F&chan=y&num=3&desc=100>1&targ=y&utf=y&html=y">View RSS feed</a>
    </noscript>
  </div>
  <!-- End news section -->

</div>
<!-- End of Main Section -->


<?php
}

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
<script type="text/javascript" src="js/sha512.js"></script>
<script type="text/javascript" src="js/log_form.js"></script>
<script type="text/javascript">
function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
</script>
<script type="text/javascript">
(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
</script>
<script>
$(document).foundation();
</script>
<script>
//done
var $startDate = $(".startDate");
var $endDate = $(".endDate");
var $endValue = $(".end");
var $primaryActivity = $(".primaryActivity");
var $secondaryActivity = $(".secondaryActivity");
var $activityAnd = $(".activityAnd");
var $minutes = $(".minutes");
var $minutes2 = $(".minutes2");
var $frequency = $(".frequency");
var $frequency2 = $(".frequency2");
var $goalTime = $(".goalTime");
var $goodGoal = $(".goodGoal");
var $fairGoal = $(".fairGoal");
var $hardGoal = $(".hardGoal");
var $recommend = $("#recommend");
var $sessionsPerGoal = $(".sessionsPerGoal");
//working

var $pointsTotal = $(".pointsTotal");
var $pointsInput = $(".points");
$activityAnd.hide();
//changes the date range of the goal calculator and updates inputs
function dateRange(date) {

  //create a date object from argument
  var dateFormat = new Date(date);
  //format the value
  var thisDate = new Date(dateFormat.setDate(dateFormat.getDate() + 1));
  var dateFormat = $.datepicker.formatDate('MM d, yy', thisDate);
  //set start date to value
  $startDate.html(dateFormat);

  //create a date object from argument
  var dateFormat = new Date(date);
  //create a date object from argument
  var dateValue = $.datepicker.formatDate('yy-mm-dd', new Date(dateFormat.setDate(dateFormat.getDate() + 8)));
  //format the value
  var dateFormat = $.datepicker.formatDate('MM d, yy', new Date(dateFormat.setDate(dateFormat.getDate())));
  //set the end date to the result

  $endDate.html(dateFormat);
  $endValue.val(dateValue);
}

function primaryActivityChange(activity) {
  $primaryActivity.html($(activity).find("option:selected").text().toLowerCase());
  pointsChange();
}

function secondaryActivityChange(activity) {
  $activityAnd.html(' and ');
  $activityAnd.show();
  $minutes2.prop('required', true);
  $frequency2.prop('required', true);
  $secondaryActivity.html($(activity).find("option:selected").text().toLowerCase());
  pointsChange();
}

function pointsChange() {
  //gather total time
  var minutesArray = $minutes;
  var value1 = minutesArray[0].value;
  var minutesArray = $minutes2;
  var value2 = minutesArray[0].value;
  if (value2 == '') {
    value2 = 0;
  }
  //gather frequency of exercise
  var frequencyArray = $frequency.find("option:selected");
  var freq1 = frequencyArray[0].innerHTML;
  var frequencyArray = $frequency2.find("option:selected");
  var freq2 = frequencyArray[0].innerHTML;
  if (freq2 == '') {
    freq2 = 0;
  }

  //total sessions
  var frequencyNew = parseInt(freq1) + parseInt(freq2);

  //total time
  var totalTime = parseInt(value1) * parseInt(freq1) + parseInt(value2) * parseInt(freq2);

  //gather met data
  var exerciseArray = $(".exercise").find("option:selected");
  var mets = exerciseArray[0].value;

  //gather point totals for first activity : floor((($time *$mets) + ($sd*100))/100);
  console.log(mets);
  console.log(value1*freq1);
  var points = (mets * value1 * freq1 * 60)/100;

  //gather secondary activity met data
  var exerciseArray = $(".exercise2").find("option:selected");
  var mets2 = exerciseArray[0].value;
  if (mets2 != 'Optional second Activity') {

    //gather point totals for second activity
    points = points + (mets2 * value2 * freq2);
  }
  points = points.toFixed(0);
  $sessionsPerGoal.html(frequencyNew);
  $('.totalFrequency').val(frequencyNew);
  $goalTime.html(totalTime);
  $pointsTotal.html(points);
  $pointsInput.val(points);
  console.log(points);
  var sentence = $("#sentence").text();
  $(".sentenceInput").val(sentence);
  if (totalTime < 150) {
    if (totalTime != '0') {
      $(".surgeonGeneral").show();
    }
  } else {
    $(".surgeonGeneral").hide();
  }

  $('.finalActivity').val($('.allActivities').text().toLowerCase());

}

function difficulty(confidence) {
  if (confidence <= 6) {
    $goodGoal.hide();
    $fairGoal.show();
    $hardGoal.hide();
  } else if (confidence > 6 && confidence < 10) {
    $goodGoal.show();
    $fairGoal.hide();
    $hardGoal.hide();
  } else if (confidence > 9) {
    $goodGoal.hide();
    $fairGoal.hide();
    $hardGoal.show();
  }
}

function confirmButton() {
  $(".confirm").hide();
  $(".submitReady").show();
}
</script>
<script>

$(document).ready(function() {
  $("#index-slideshow > div.slide:gt(0)").hide();

  setInterval(function() {
    $('#index-slideshow > div.slide:first')
    .fadeOut(2000)
    .next()
    .fadeIn(2000)
    .end()
    .appendTo('#index-slideshow');
  },  10000);
});
$(".rss-item").each(function() {
  var $this = $(this);
  $this.html($this.html().replace(/&nbsp;/g, ' '));
  $this.html($this.html().replace(/�/g, ' '));
});
</script>
<script>
console.log(<?php echo json_encode($chart_datasets); ?>);
var canvas = document.getElementById('exerciseChart');
var exercise_chart = new Chart(canvas, {
  type: 'line',
  data: {
    labels: <?php echo json_encode($chart_labels); ?>,
    datasets: <?php echo json_encode($chart_datasets); ?>
  },
  options: {
    title: {
      display: true,
      text: "This Week's Activity"
    },
    tooltips: {
      mode: 'label'
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero:true
        }
      }]
    }
  }
});
var canvas_month = document.getElementById('exerciseChartMonth');
var exercise_chart_month = new Chart(canvas_month, {
  type: 'line',
  data: {
    labels: <?php echo json_encode($chart_month_labels); ?>,
    datasets: <?php echo json_encode($chart_month_datasets); ?>
  },
  options: {
    title: {
      display: true,
      text: "This Month's Activity"
    },
    tooltips: {
      mode: 'label'
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero:true
        }
      }],
      xAxes: [{
        display: false
      }]
    }
  }
});

$(document).ready(function() {
  $('#exercise_data_select').on('change', function(){
    if($('#exercise_data_select option:selected').val() == 'this_month') {
      $('#exerciseChart').hide();
      $('#exerciseChartMonth').show();
    }
    else {
      $('#exerciseChartMonth').hide();
      $('#exerciseChart').show();
    }
  });
});
</script>

<?php
if(isset($info['ID'])){
  $first_time = ACCOUNT::first_visit($info['ID']);
  if($first_time){

    $first_visit_removal = ACCOUNT::first_visit_remove($info['ID']);
  }
}
if((filter_input(INPUT_GET, 'msg', FILTER_SANITIZE_STRING))!= null && filter_input(INPUT_GET, 'msg', FILTER_SANITIZE_STRING) == 'activity_logged'){
  ?>
  <script type="text/javascript">
  $(document).ready(function(){
    $('#facebookShare').foundation('reveal', 'open')
  });
  $(document).foundation({
    'reveal': { close_on_background_click: false }
  })
  </script>
  <?php
}
?>

<!-- End Footer -->
</body>
</html>
