﻿<?php
$root = realpath($_SERVER['DOCUMENT_ROOT']);
include_once "$root/lib/template_api.php";
include_once "$root/lib/back_api.php";
$ss = SESSION::secure_session();
if (filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null) {
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

<div id="main">
  <!-- Global Header Container -->
  <div id="top nojava">
    <!-- Header Image Container -->
    <div id="slideshow">
      <!-- Page Title Container -->
      <div id="slideshow-title">
	<div class="row nojava center">
	  <h2 class="custom-font-big-white">county resources</h2>
	  <br />
	  <!-- County Select List -->
	  <div class="large-12 columns expand">
	    <div class="row center nojava">
	      <form>
		<select style="width:80%" size="1" name="dropdown" id="select-anchor">
		  <option>Select Your County</option>
		  <option value="#Appling">Appling</option>
		  <option value="#Atkinson">Atkinson</option>
		  <option value="#Bacon">Bacon</option>
		  <option value="#Baker">Baker</option>
		  <option value="#Baldwin">Baldwin</option>
		  <option value="#Banks">Banks</option>
		  <option value="#Barrow">Barrow</option>
		  <option value="#Bartow">Bartow</option>
		  <option value="#Ben-Hill">Ben Hill</option>
		  <option value="#Berrien">Berrien</option>
		  <option value="#Bibb">Bibb</option>
		  <option value="#Bleckley">Bleckley</option>
		  <option value="#Brantley">Brantley</option>
		  <option value="#Brooks">Brooks</option>
		  <option value="#Bryan">Bryan</option>
		  <option value="#Bulloch">Bulloch</option>
		  <option value="#Burke">Burke</option>
		  <option value="#Butts">Butts</option>
		  <option value="#Calhoun">Calhoun</option>
		  <option value="#Camden">Camden</option>
		  <option value="#Candler">Candler</option>
		  <option value="#Carroll">Carroll</option>
		  <option value="#Catoosa">Catoosa</option>
		  <option value="#Charlton">Charlton</option>
		  <option value="#Chatham">Chatham</option>
		  <option value="#Chattahoochee">Chattahoochee</option>
		  <option value="#Chattooga">Chattooga</option>
		  <option value="#Cherokee">Cherokee</option>
		  <option value="#Clarke">Clarke</option>
		  <option value="#Clay">Clay</option>
		  <option value="#Clayton">Clayton</option>
		  <option value="#Clinch">Clinch</option>
		  <option value="#Cobb">Cobb</option>
		  <option value="#Coffee">Coffee</option>
		  <option value="#Colquitt">Colquitt</option>
		  <option value="#Columbia">Columbia</option>
		  <option value="#Cook">Cook</option>
		  <option value="#Coweta">Coweta</option>
		  <option value="#Crawford">Crawford</option>
		  <option value="#Crisp">Crisp</option>
		  <option value="#Dade">Dade</option>
		  <option value="#Dawson">Dawson</option>
		  <option value="#Decatur">Decatur</option>
		  <option value="#DeKalb">DeKalb</option>
		  <option value="#Dodge">Dodge</option>
		  <option value="#Dooly">Dooly</option>
		  <option value="#Dougherty">Dougherty</option>
		  <option value="#Douglas">Douglas</option>
		  <option value="#Early">Early</option>
		  <option value="#Echols">Echols</option>
		  <option value="#Effingham">Effingham</option>
		  <option value="#Elbert">Elbert</option>
		  <option value="#Emanuel">Emanuel</option>
		  <option value="#Evans">Evans</option>
		  <option value="#Fannin">Fannin</option>
		  <option value="#Fayette">Fayette</option>
		  <option value="#Floyd">Floyd</option>
		  <option value="#Forsyth">Forsyth</option>
		  <option value="#Franklin">Franklin</option>
		  <option value="#Fulton">Fulton</option>
		  <option value="#Gilmer">Gilmer</option>
		  <option value="#Glascock">Glascock</option>
		  <option value="#Glynn">Glynn</option>
		  <option value="#Gordon">Gordon</option>
		  <option value="#Grady">Grady</option>
		  <option value="#Greene">Greene</option>
		  <option value="#Gwinnett">Gwinnett</option>
		  <option value="#Habersham">Habersham</option>
		  <option value="#Hall">Hall</option>
		  <option value="#Hancock">Hancock</option>
		  <option value="#Haralson">Haralson</option>
		  <option value="#Harris">Harris</option>
		  <option value="#Hart">Hart</option>
		  <option value="#Heard">Heard</option>
		  <option value="#Henry">Henry</option>
		  <option value="#Houston">Houston</option>
		  <option value="#Irwin">Irwin</option>
		  <option value="#Jackson">Jackson</option>
		  <option value="#Jasper">Jasper</option>
		  <option value="#Jeff-Davis">Jeff Davis</option>
		  <option value="#Jefferson">Jefferson</option>
		  <option value="#Jenkins">Jenkins</option>
		  <option value="#Johnson">Johnson</option>
		  <option value="#Jones">Jones</option>
		  <option value="#Lamar">Lamar</option>
		  <option value="#Lanier">Lanier</option>
		  <option value="#Laurens">Laurens</option>
		  <option value="#Lee">Lee</option>
		  <option value="#Liberty">Liberty</option>
		  <option value="#Lincoln">Lincoln</option>
		  <option value="#Long">Long</option>
		  <option value="#Lowndes">Lowndes</option>
		  <option value="#Lumpkin">Lumpkin</option>
		  <option value="#Macon">Macon</option>
		  <option value="#Madison">Madison</option>
		  <option value="#Marion">Marion</option>
		  <option value="#McDuffie">McDuffie</option>
		  <option value="#McIntosh">McIntosh</option>
		  <option value="#Meriwether">Meriwether</option>
		  <option value="#Miller">Miller</option>
		  <option value="#Mitchell">Mitchell</option>
		  <option value="#Monroe">Monroe</option>
		  <option value="#Montgomery">Montgomery</option>
		  <option value="#Morgan">Morgan</option>
		  <option value="#Murray">Murray</option>
		  <option value="#Muscogee">Muscogee</option>
		  <option value="#Newton">Newton</option>
		  <option value="#Oconee">Oconee</option>
		  <option value="#Oglethorpe">Oglethorpe</option>
		  <option value="#Paulding">Paulding</option>
		  <option value="#Peach">Peach</option>
		  <option value="#Pickens">Pickens</option>
		  <option value="#Pierce">Pierce</option>
		  <option value="#Pike">Pike</option>
		  <option value="#Polk">Polk</option>
		  <option value="#Pulaski">Pulaski</option>
		  <option value="#Putnam">Putnam</option>
		  <option value="#Quitman">Quitman</option>
		  <option value="#Rabun">Rabun</option>
		  <option value="#Randolph">Randolph</option>
		  <option value="#Richmond">Richmond</option>
		  <option value="#Rockdale">Rockdale</option>
		  <option value="#Schley">Schley</option>
		  <option value="#Screven">Screven</option>
		  <option value="#Seminole">Seminole</option>
		  <option value="#Spalding">Spalding</option>
		  <option value="#Stephens">Stephens</option>
		  <option value="#Stewart">Stewart</option>
		  <option value="#Sumter">Sumter</option>
		  <option value="#Talbot">Talbot</option>
		  <option value="#Taliaferro">Taliaferro</option>
		  <option value="#Tattnall">Tattnall</option>
		  <option value="#Taylor">Taylor</option>
		  <option value="#Telfair">Telfair</option>
		  <option value="#Terrell">Terrell</option>
		  <option value="#Thomas">Thomas</option>
		  <option value="#Tift">Tift</option>
		  <option value="#Toombs">Toombs</option>
		  <option value="#Towns">Towns</option>
		  <option value="#Treutlen">Treutlen</option>
		  <option value="#Troup">Troup</option>
		  <option value="#Turner">Turner</option>
		  <option value="#Twiggs">Twiggs</option>
		  <option value="#Union">Union</option>
		  <option value="#Upson">Upson</option>
		  <option value="#Walker">Walker</option>
		  <option value="#Walton">Walton</option>
		  <option value="#Ware">Ware</option>
		  <option value="#Warren">Warren</option>
		  <option value="#Washington">Washington</option>
		  <option value="#Wayne">Wayne</option>
		  <option value="#Webster">Webster</option>
		  <option value="#Wheeler">Wheeler</option>
		  <option value="#White">White</option>
		  <option value="#Whitfield">Whitfield</option>
		  <option value="#Wilcox">Wilcox</option>
		  <option value="#Wilkes">Wilkes</option>
		  <option value="#Wilkinson">Wilkinson</option>
		  <option value="#Worth">Worth</option>
		</select>
	      </form>
	    </div>
	  </div>
	  <!-- End County Select List -->
	</div>
      </div>
      <!-- End Page Title Container -->
    </div>
    <!-- End Header Image Container -->
  </div>
  <!-- End Global Header Container -->
  <!-- Main Content -->
  <!-- County Resources Section -->
  <div class="row" style="margin-top:20px;">
    <div class="large-3 medium-3 hide-for-small columns">
      <img src="../img/resources-county-logo.png" />
    </div>
    <div class="large-9 medium-9 columns">
      <h2 class="custom-font-small">Get Connected Locally</h2>
      <p class="global-p">
	One of the things that sets Walk Georgia apart from other wellness programs is that we connect to you locally.  Dozens of counties all over the state have unique events, resources, and fitness centers for you to get involved with.  To find out more about what is going on in your area, select your county from the list above, or scroll down the list.
      </p>
    </div>
  </div>
  <div class="row">
    <div class="large-12 columns">
      <hr />
    </div>
  </div>
  <!-- End County Resources Section -->
  <!-- County List -->
  <div class="row">
    <div class="large-12 columns">
      <!-- County -->
      <h2 class="font -blue -medium -secondary" id="Appling">Appling County </h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/appling.html">Appling County Office</a></li>
				<li><a href="http://www.applingrec.com/facilities/">Appling County Parks and Recreation Department </a></li>
				<li><a href="http://www.appling.k12.ga.us/District/News/5842-Health-Resources.html">Appling County Schools Health Resources </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -- >

      <!-- County -->
      <h2 class="font -blue -medium -secondary" id="Atkinson">Atkinson County </h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/atkinson.html">Atkinson County Office</a></li>
				<li><a href="http://www.cityofpearson.com/departments/recreation/">City of Pearson Parks and Rec</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -- >

      <!-- County -->
      <h2 class="font -blue -medium -secondary" id="Bacon">Bacon County </h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/bacon.html">Bacon County Office</a></li>
				<li><a href="http://www.abcrecdept.com">Bacon County Recreation Department </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -- >

      <!-- County -->
      <h2 class="font -blue -medium -secondary" id="Baker">Baker County </h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/baker.html">Baker County Office</a></li>
				<li><a href="https://www.facebook.com/groups/BakerCoGAParentsCorner/">Baker County Ga School Parents Corner </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="font -blue -medium -secondary" id="Baldwin">Baldwin County </h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/baldwin.html">Baldwin County Office</a></li>
				<li><a href="https://www.facebook.com/baldwinrec1">Baldwin County Parks & Recreation </a></li>
				<li><a href="http://lockerly.org/">Lockerly Arboretum </a></li>
				<li><a href="https://www.facebook.com/pages/Bonner-Park-AKA-The-Pit/204407186243211">Bonner Park AKA The Pit </a></li>
				<li><a href="http://www.oconeerivergreenway.org/">Oconee River Greenway </a></li>
				<li><a href="http://www.schoolnutritionandfitness.com/index.php?sid=2107111940113829">Baldwin County Schools </a></li>
				<li><a href="http://www.baldwincountyrec.com">Baldwin County Recreation Department </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="font -blue -medium -secondary" id="Banks">Banks County </h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/banks.html">Banks County Office</a></li>
				<li><a href="https://www.bankscountyrec.org/home.html">Banks County Recreation </a></li>
				<li><a href="https://www.facebook.com/LeopardZumba">Leopard Fitness -- Zumba at Banks County Recreation Department</a></li>
				<li><a href="http://www.cityofauburn-ga.org/index.aspx?NID=273">City of Auburn </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="font -blue -medium -secondary" id="Barrow">Barrow County </h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/barrow.html">Barrow County Office</a></li>
				<li><a href="http://gastateparks.org/FortYargo" >Fort Yargo State Park</a></li>
				<li><a href="http://www.barrowga.org/departments/parks-recreation-department/">Barrow County Parks, Recreation, and Leisure Services </a></li>
				<li><a href="https://www.facebook.com/BarrowCountyParksAndRecreation">Barrow County Parks and Recreation </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Bartow">Bartow County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/bartow.html">Bartow County Office</a></li>
				<li><a href="http://gastateparks.org/EtowahMounds">Etowah Indian Mounds Historic Site</a></li>
				<li><a href="http://gastateparks.org/RedTopMountain ">Red Top Mountain State Park</a></li>
				<li><a href="http://www.bartowga.org/bartow_county_trails.php">Bartow County Trails </a></li>
				<li><a href="http://www.cartersvillechamber.com/files/1810.pdf">Cartersville-Bartow Chamber</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Ben-Hill">Ben Hill County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/benhill.html">Ben Hill County Office</a></li>
				<li><a href="http://gastateparks.org/JeffersonDavis">Jefferson Davis Memorial Historic Site</a></li>
				<li><a href="http://www.n-georgia.com/ben-hill-co-parks.html">Ben Hill Landing-Ocmulgee River Park </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Berrien">Berrien County:</h2>
      <ul class="pt" style="list-style:none;"><li><a href="http://www.berriencountygeorgia.com/index.php?option=com_content&task=view&id=15&Itemid=32">Berrien County Parks and Recreation </a></li>
				<li><a href="http://extension.uga.edu/county-offices/berrien.html">Berrien County Office</a></li>
				<li><a href="https://www.facebook.com/Berrien-County-Parks-Recreation-123190194360154/">Berrien County Parks & Recreation facebook </a></li>
				<li><a href="http://www.cityofnashvillega.net/index.php/2013-03-20-00-42-51/parks-recreation">City of Nashville, Georgia City Parks </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Bibb">Bibb County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/bibb.html">Bibb County Office</a></li>
				<li><a href="http://www.maconbibb.us/recreation/">MaconBibb Parks and Recreation Department </a></li>
				<li><a href="https://www.facebook.com/MaconBibbParksandRec">Macon Bibb Parks & Recreation Facebook </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Bleckley">Bleckley County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/bleckley.html">Bleckley County Office</a></li>
				<li><a href="https://www.facebook.com/Cochran-Bleckley-Recreation-Department-208504365898845/">Cochran-Bleckly Recreation Department Facebook</a></li>
				<li><a href="http://cbrdrecreation.wix.com/cochranbleckleyrec">Cochran Bleckley Recreation Dept </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Brantley">Brantley County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/brantley.html">Brantley County Office</a></li>
				<li><a href="http://bchsherons.com/ATHLETICS.HTM">Brantley County Athletics </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Brooks">Brooks County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/brooks.html">Brooks County Office</a></li>
				<li><a href="http://bchsherons.com/ATHLETICS.HTM">Brooks County Athletics </a></li>
				<li><a href="https://www.facebook.com/brookscorec">Brooks County Recreation Department </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Bryan">Bryan County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/bryan.html">Bryan County Office</a></li>
				<li><a href="http://gastateparks.org/FortMcAllister">Fort McAllister Historic Park</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Bulloch">Bulloch County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/bulloch.html">Bulloch County Office</a></li>
				<li><a href="http://bullochcounty.net/visitors/attractions/">Bulloch County Attractions </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Burke">Burke County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/burke.html">Burke County Office</a></li>
				<li><a href="http://www.wbcrd.com">Burke County Recreation Department </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Butts">Butts County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/butts.html">Butts County Office</a></li>
				<li><a href="http://gastateparks.org/HighFalls">High Falls State Park</a></li>
				<li><a href="http://gastateparks.org/IndianSprings">Indian Springs State Park </a></li>
				<li><a href="http://www.bcdls.org">Butts County Department of Leisure Services</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Calhoun">Calhoun County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/calhoun.html">Calhoun County Office</a></li>
				<li><a href="https://www.facebook.com/Calhoun-County-Georgia-715595515123557/">Calhoun County, Georgia </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Camden">Camden County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/camden.html">Camden County Office</a></li>
				<li><a href="http://gastateparks.org/CrookedRiver">Crooked River State Park </a></li>
				<li><a href="http://www.camdencountypsa.info/facility.php">Camden County Leisure Services </a></li>
				<li><a href="http://woodbinegeorgia.net/riverwalk.html">Welcome to Woodbine </a></li>
				<li><a href="http://www.traillink.com/trail/georgia-coast-rail-trail.aspx">Georgia Coast Rail-Trail </a></li>
				<li><a href="http://www.nps.gov/cuis/index.htm">Cumberland Island </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Candler">Candler County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/candler.html">Candler County Office</a></li>
				<li><a href="https://www.facebook.com/pages/Metter-Candler-Recreation-Department/183375758436419">Metter-Candler Recreation Department </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Carroll">Carroll County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/carroll.html">Carroll County Office</a></li>
				<li><a href="http://www.carrolltongreenbelt.com/">Carrollton Green Belt</a></li>
				<li><a href="http://gastateparks.org/JohnTanner">John Tanner State Park </a></li>
				<li><a href="http://www.carrollcountygarec.com/">Carroll County Recreation </a></li>
				<li><a href="http://www.carrollcountygaparks.com">Carroll County Parks </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Catoosa">Catoosa County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/catoosa.html">Catoosa County Office</a></li>
				<li><a href="http://www.catoosarec.com/">Catoosa Recreation </a></li>
				<li><a href="https://www.facebook.com/CatoosaRec">Catoosa Recreation Facebook </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Charlton">Charlton County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/charlton.html">Charlton County Office</a></li>
				<li><a href="http://www.charltoncountyga.us/index.aspx?nid=218">Charlton County Recreation Program & Parks </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Chatham">Chatham County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/chatham.html">Chatham County Office</a></li>
				<li><a href="http://gastateparks.org/SkidawayIsland">Skidaway Island State Park</a></li>
				<li><a href="http://gastateparks.org/Wormsloe">Wormsloe Historic Site</a></li>
				<li><a href="http://parks.chathamcounty.org/Parks">Chatam County Parks</a></li>
				<li><a href="http://parks.chathamcounty.org/Parks/Weight-Lifting-Centers/Anderson-Cohen-Weightlifting-Center">Chatam County Weight Lifting Center</a></li>
				<li><a href="https://www.facebook.com/chathamcounty.parks">Chatam County Parks Facebook </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Chattahoochee">Chattahoochee County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/hattahoochee.html">Chattahoochee County Office</a></li>
				<li><a href="http://www.ugoccc.us/recreation/rgr_center.asp">Chattahoochee County Recreation </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Chattooga">Chattooga County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/chattooga.html">Chattooga County Office</a></li>
				<li><a href="http://gastateparks.org/JamesHFloyd">James H. "Sloppy" Floyd State Park</a></li>
				<li><a href="http://www.summervillega.org/parksrecreation/parksrecreation.html">Summerville Parks & Recreation</a></li>
				<li><a href="http://www.townoflyerly.com/Parks---Recreation-Department.html">Town of Lyerly Parks & Recreation</a></li>
				<li><a href="http://pinhoti.info/joomla/">Pinhoti Trail </a></li>
				<li><a href="http://www.summervillega.org/parksrecreation/parksrecreationfacilitiesdowdy.html">Summerville Parks & Recreation facilities </a></li>
				<li><a href="http://www.n-georgia.com/chatooga-co-parks.html">Chattooga County Parks </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Cherokee">Cherokee County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/cherokee.html">Cherokee County Office</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Clarke">Clarke County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/clarke.html">Clarke County Office</a></li>
				<li><a href="http://www.athensclarkecounty.com/149/Parks-Facilities">Athens-Clarke County Parks </a></li>
				<li><a href="https://www.athensclarkecounty.com/1746/Fitness-Room--East-Athens-Community-Cent">Athens-Clarke Fitness Room East Athens</a></li>
				<li><a href="https://www.athensclarkecounty.com/1747/Fitness-Room--Lay-Park-Community-Center">Athens-Clarke Fitness Room Lay Park </a></li>
				<li><a href="http://issuu.com/accleisureservices/docs/getfit2013_issuu">Athens-Clarke Leisure Services </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Clay">Clay County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/clay.html">Clay County Office</a></li>
				<li><a href="http://gastateparks.org/GeorgeTBagby">George T. Bagby State Park</a></li>
				<li><a href="http://claycountyga.org/residents/default.asp?PageID=302">Things To Do </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Clayton">Clayton County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/clayton.html">Clayton County Office</a></li>
				<li><a href="http://www.claytonparks.com/">Clayton Parks</a></li>
				<li><a href="https://www.facebook.com/ClaytonCountyParks?fref=photo">Clayton County Parks facebook </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Clinch">Clinch County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/clinch.html">Clinch County Office</a></li>
				<li><a href="http://gastateparks.org/StephenCFoster">Stephen C. Foster State Park</a></li>
				<li><a href="http://cityofacworth.org/index.php/parks-par">City of Acworth parks </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Cobb">Cobb County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/cobb.html">Cobb County Office</a></li>
				<li><a href="http://www.cobb2020.com">Cobb2020 Partnership</a></li>
				<li><a href="http://www.cobbcounty.org/index.php?option=com_content&view=article&id=161&Itemid=107">Cobb County P.A.R.K.S </a></li>
				<li><a href="http://www.n-georgia.com/cobb-co-recreation-centers.html">Cobb County Recreation Center</a></li>
				<li><a href="https://www.facebook.com/CobbRecCenters">Cobb Recreation Centers Facebook </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Coffee">Coffee County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/coffee.html">Coffee County Office</a></li>
				<li><a href="http://gastateparks.org/GeneralCoffee">General Coffee State Park</a></li>
				<li><a href="https://www.facebook.com/DCCPRD">Coffee County Parks & Rec Department </a></li>
				<li><a href="https://www.facebook.com/Coffeecountyevents">Coffee County Events</a></li>
				<li><a href="http://www.cityofdouglas.com/index.aspx?NID=60">City of Douglas </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Colquitt">Colquitt County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/colquitt.html">Colquitt County Office</a></li>
				<li><a href="http://www.moultriega.com/?page_id=19">Moultrie Parks/Recreation </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Columbia">Columbia County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/columbia.html">Columbia County Office</a></li>
				<li><a href="http://gastateparks.org/Mistletoe">Mistletoe State Park</a></li>
				<li><a href="https://www.facebook.com/columbiacountyrec?fref=nf">Columbia County Recreation Facebook </a></li>
				<li><a href="http://en.wikipedia.org/wiki/Patriots_Park_(Columbia_County,_Georgia)">Patriots Park </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Cook">Cook County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/cook.html">Cook County Office</a></li>
				<li><a href="http://gastateparks.org/REedBingham">Reed Bingham State Park</a></li>
				<li><a href="http://www.cityofadel.us/Recreation/Recreation.html">City of Adel Recreation </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Coweta">Coweta County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/coweta.html">Coweta County Office</a></li>
				<li><a href="http://gastateparks.org/REedBingham">Chattahoochee Bend State Park </a></li>
				<li><a href="http://www.coweta.ga.us/Index.aspx?page=185">Coweta County Recreation Department </a></li>
				<li><a href="http://www.coweta.ga.us/Index.aspx?page=155">James E. McGuffey Nature Center at the Coweta County Fairgrounds</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Crawford">Crawford County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/crawford.html">Crawford County Office</a></li>
				<li><a href="http://recreations.crawfordcountyga.org/">Crawford County Recreations </a></li>
				<li><a href="https://www.facebook.com/CrawfordCountyRecreation">Crawford County Recreation Facebook </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Crisp">Crisp County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/crisp.html">Crisp County Office</a></li>
				<li><a href="http://gastateparks.org/GeorgiaVeterans">Georgia Veterans State Park</a></li>
				<li><a href="https://www.facebook.com/CrispRec">Crisp County Recreation </a></li>
				<li><a href="https://sites.google.com/site/crispcountyrecreation/">Crisp County Recreation </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Dade">Dade County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/dade.html">Dade County Office</a></li>
				<li><a href="http://gastateparks.org/CloudlandCanyon">Cloudland Canyon State Park</a></li>
				<li><a href="http://www.dadecountychamber.com/recreation/">Dade County Chamber of Commerce - Recreation </a></li>
				<li><a href="http://www.developingdadega.com/recreation.cfm">Dade County Industrial Development Authority -Recreation</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Dawson">Dawson County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/dawson.html">Dawson County Office</a></li>
				<li><a href="http://gastateparks.org/AmicalolaFalls">Amicalola Falls State Park</a></li>
				<li><a href="http://www.bainbridgecity.com/department/division.php?fDD=6-79">Bainbridge Leisure Services Department </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Decatur">Decatur County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/decatur.html">Decatur County Office</a></li>
				<li><a href="http://www.decaturcountyga.org/recreation.php"></a></li>
				<li><a href="http://www.atlantaga.gov/index.aspx?page=22">City of Atlanta Parks & Recreation </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="DeKalb">DeKalb County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/dekalb.html">Dekalb County Office</a></li>
				<li><a href="http://www.dekalbcountyga.gov/parks/pr-recreathttp://www.atlantaga.gov/index.aspx?page=22ion-centers.html">Dekalb County Recreation Centers</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Dodge">Dodge County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/dodge.html">Dodge County Office</a></li>
				<li><a href="https://www.facebook.com/Eastman-Dodge-County-Recreation-Department-149406893234/">Eastman-Dodge County Recreation Department</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Dooly">Dooly County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/dooly.html">Dooly County Office</a></li>
				<li><a href="http://www.doolyrec.com">Dooly County Recreation Department</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Dougherty">Dougherty County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/dougherty.html">Dougherty County Office</a></li>
				<li><a href="http://www.albany.ga.us/content/1798/2879/117101/default.aspx">Albany Recreation & Parks Department</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Douglas">Douglas County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/douglas.html">Douglas County Office</a></li>	  
				<li><a href="http://gastateparks.org/SweetwaterCreek">Sweetwater Creek State Park</a></li>
				<li><a href="http://www.celebratedouglascounty.com/view/departments/view_dept/&cdept=66&department=Parks%20and%20Recreation">Douglas County Parks and Recreation</a></li>
				<li><a href="http://healthydouglas.org">Live Healthy Douglas </a></li>
				<li><a href="https://www.facebook.com/LiveHealthyDouglas">Live Healthy Douglas - Facebook</a></li>
				<li><a href="https://twitter.com/HealthyDouglas">Live Healthy Douglas - Twitter</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Early">Early County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/early.html">Early County Office</a></li>
				<li><a href="http://gastateparks.org/KolomokiMounds">Kolomoki Mounds Historic Park</a></li>
				<li><a href="https://www.facebook.com/BECRD">Blakely-Early County Recreation Department</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Echols">Echols County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/echols.html">Echols County Office</a></li>
				<li><a href="https://www.facebook.com/Echols.County">Echols County Georgia </a></li>
				<li><a href="https://www.facebook.com/pages/Echols-County-Community-Park/255040834531968">Echols County Community Park</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Effingham">Effingham County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/effingham.html">Effingham County Office</a></li>
				<li><a href="https://www.activityreg.com/ClientPage_t1.wcs?clientid=EFFINGHM&siteid=1">Effingham County Recreation & Parks</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Elbert">Elbert County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/elbert.html">Elbert County Office</a></li>
				<li><a href="http://gastateparks.org/RichardBRussell">Richard B. Russell State Park</a></li>
				<li><a href="http://www.elbertparksandrecreation.com/38001.html">Elbert County Parks & Recreation</a></li>
				<li><a href="https://www.facebook.com/Elbert-County-Parks-and-Recreation-236175198740/">Elbert County Parks & Recreation - Facebook</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Emanuel">Emanuel County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/emanuel.html">Emanuel County Office</a></li>
				<li><a href="http://gastateparks.org/GeorgeLSmith">George L. Smith State Park</a></li>
				<li><a href="http://cityofswainsboro.org/government/departments/recreation/">City of Swainsboro Recreation</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Evans">Evans County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/evans.html">Evans County Office</a></li>
				<li><a href="http://www.evansrecdept.com/facilities.html">Evans County Parks & Recreation </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Fannin">Fannin County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/fannin.html">Fannin County Office</a></li>
				<li><a href="http://fannincountyga.org/recreation-department/">Fannin County Recreation Department</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Fayette">Fayette County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/fayette.html">Fayette County Office</a></li>
				<li><a href="http://www.fayettecountyga.gov/parks_and_recreation/index.htm">Fayette County Parks and Recreation</a></li>
				<li><a href="https://www.geocaching.com/bookmarks/view.aspx?guid=65b08426-3092-4a95-8b79-c627588f9369">Fayette County Geocaching</a></li>
				<li><a href="http://www.n-georgia.com/fayette-co-parks.html">Fayette County Parks</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Floyd">Floyd County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/floyd.html">Floyd County Office</a></li>
				<li><a href="http://rfpra.com">Rome-Floyd Parks & Recreation Authority </a></li>
				<li><a href="http://www.n-georgia.com/floyd_co.htm">Floyd County Parks and Recreation Centers</a></li>
				<li><a href="http://romegeorgia.org/attraction_type/outdoor-adventures/">Outdoor Adventures in Rome, Georgia</a></li>
				<li><a href="http://www.tredromefloyd.org">Trails of Floyd County (T.R.E.D.)</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Forsyth">Forsyth County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/forsyth.html">Forsyth County Office</a></li>
				<li><a href="http://www.sawneemountain.org">Sawnee Mountain Preserve</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Franklin">Franklin County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/franklin.html">Franklin County Office</a></li>
				<li><a href="http://gastateparks.org/Tugaloo">Tugaloo State Park</a></li>
				<li><a href="http://gastateparks.org/VictoriaBryant">Victoria Bryant State Park</a></li>
				<li><a href="http://www.fcrdonline.com">Franklin County Recreation Department</a></li>
				<li><a href="https://www.facebook.com/FCRD2?rf=158455454187282">Franklin County Recreation Department - Facebook</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Fulton">Fulton County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/fulton.html">Fulton 4-H County Office</a></li>
				<li><a href="http://extension.uga.edu/county-offices/fulton.html">Fulton Main Office County Office</a></li>
				<li><a href="http://extension.uga.edu/county-offices/fulton.html">Fulton North Office County Office</a></li>
				<li><a href="http://extension.uga.edu/county-offices/fulton.html">Fulton South Office County Office</a></li>
				<li><a href="http://www.fultoncountyga.gov/fcprd-parks/1824-welcome-all-park-a-multipurpose-facility">Welcome All Park & Multipurpose Facility </a></li>
				<li><a href="http://www.beltline.org">The Atlanta Beltline</a></li>
				<li><a href="http://www.atlantaga.gov/index.aspx?page=22">City of Atlanta Parks & Recreation </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Gilmer">Gilmer County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/gilmer.html">Gilmer County Office</a></li>
				<li><a href="http://www.n-georgia.com/gilmer-county-parks.html">Gilmer County Parks</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Glascock">Glascock County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/glascock.html">Glascock County Office</a></li>
				<li><a href="http://gastateparks.org/Hamburg">Hamburg State Park</a></li>
				<li><a href="http://www.georgiaencyclopedia.org/articles/counties-cities-neighborhoods/glascock-county">Glascock County - New Georgia Encyclopedia </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Glynn">Glynn County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/glynn.html">Glynn County Office</a></li>
				<li><a href="http://gastateparks.org/HofwylBroadfield">Hofwyl-Broadfield Plantation Historic Site</a></li>
				<li><a href="http://www.glynncounty.org/153/Recreation-and-Parks">Glynn County Recreation and Parks</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Gordon">Gordon County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/gordon.html">Gordon County Office</a></li>
				<li><a href="http://www.cityofcalhoun-ga.com/recreation/">City of Calhoun Recreation Park</a></li>
				<li><a href="http://gastateparks.org/NewEchota">New Echota Historic Site</a></li>
				<li><a href="http://gordoncounty.org/departments/parks-recreation/">Gordon County Parks and Recreation Department </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Grady">Grady County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/grady.html">Grady County Office</a></li>
				<li><a href="http://www.gradycountyga.gov/index.php/services/recreation">Grady County Recreation Department</a></li>
					<li><a href="#top">Back to Top</a></li>
      </ul>
				      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Gwinnett">Gwinnett County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/gwinnett.html">Gwinnett County Office</a></li>
				<li><a href="https://www.gwinnettcounty.com/portal/gwinnett/Departments/CommunityServices/ParksandRecreation">Gwinnett County Parks and Recreation </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
				      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Habersham">Habersham County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/habersham.html">Habersham County Office</a></li>
				<li><a href="http://gastateparks.org/MoccasinCreek">Moccasin Creek State Park</a></li>
				<li><a href="http://gastateparks.org/TallulahGorge">Tallulah Gorge State Park</a></li>
				<li><a href="http://www.habershamrec.com">Habersham County Parks, Recreation, and Aquatics </a></li>
				<li><a href="https://www.facebook.com/Habersham-County-Parks-Recreation-Aquatic-Center-143691112353864/">Habersham County Parks, Recreation, & Aquatic Center - Facebook</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Hall">Hall County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/hall.html">Hall County Office</a></li>
				<li><a href="http://gastateparks.org/doncarter">Don Carter State Park</a></li>
				<li><a href="http://www.hallcountysports.com/index.php">Hall County Sports</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Hancock">Hancock County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/hancock.html">Hancock County Office</a></li>
				<li><a href="http://hancockcountyga.gov/?page_id=3970">Hancock County Parks and Recreation</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Haralson">Haralson County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/haralson.html">Haralson County Office</a></li>
				<li><a href="http://haralsonrecreation.com/Home_Page.php">Haralson County Recreation Department</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Harris">Harris County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/harris.html">Harris County Office</a></li>
				<li><a href="http://gastateparks.org/FDRoosevelt">F.D. Roosevelt State Park</a></li>
				<li><a href="http://recreation.harriscountyga.gov">Harris County Recreation Department</a></li>
				<li><a href="https://en.wikipedia.org/wiki/Waverly_Hall,_Georgia">Waverly Hall, Georgia </a></li>
				<li><a href="http://harriscountyga.gov/departments/recreation/">Harris County Recreation </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Hart">Hart County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/hart.html">Hart County Office</a></li>
				<li><a href="http://gastateparks.org/Hart">Hart State Park</a></li>
				<li><a href="https://www.facebook.com/pages/Royston-Wellness-And-Community-Park/100644553361502">Victoria Bryant State Park - Royston Wellness and Community Park</a></li>
				<li><a href="http://hartcountyga.gov/recreation.html">Hart County Recreation Department</a></li>
				<li><a href="https://www.facebook.com/Hart-County-Recreation-and-Parks-Department-342581722564/">Hart County Recreation Department - Facebook</a></li>
				<li><a href="http://www.hart-chamber.org/parks.php">Hart County Parks and Recreation</a></li>
				<li><a href="http://www.n-georgia.com/hart_co.htm">Discover Hart County Georgia Parks</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Heard">Heard County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/heard.html">Heard County Office</a></li>
				<li><a href="http://www.heardcountyrecreationdepartment.com">Heard County Parks and  Recreation </a></li>
				<li><a href="https://www.facebook.com/Heard-County-Parks-Recreation-260230620658840/">Heard County Parks & Recreation - Facebook </a></li>
				<li><a href="http://www.heardcountyga.com/recreation.html">Heard County Recreation Department</a></li>
				<li><a href="http://www.n-georgia.com/heard_co.htm">Discover Heard County Georgia Parks</a></li>
				<li><a href="http://www.franklingeorgia.com/TourismHeard.html">Heard County Tourism and Recreation</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Henry">Henry County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/henry.html">Henry County Office</a></li>
				<li><a href="http://gastateparks.org/PanolaMountain">Panola Mountain State Park</a></li>
				<li><a href="http://www.co.henry.ga.us/parksrecreation/RecreationCenters.shtml">Henry County Recreation Centers</a></li>
				<li><a href="https://www.facebook.com/Henry-County-Government-Parks-and-Recreation-94577686527/">Henry County Government (Parks and Recreation) - Facebook</a></li>
				<li><a href="http://visithenrycountygeorgia.com/list/play">Visit Henry County</a></li>
				<li><a href="http://www.n-georgia.com/henry-co-park-associations.html">Henry County Sports Associations</a></li>
				<li><a href="http://www.exploregeorgia.org/listing/9632-henry-county-parks-and-recreation">Henry County Parks and Recreation</a></li>
				<li><a href="http://www.co.henry.ga.us/parksrecreation/RecCenter_JPMoseley.shtml">J.P. Moseley Recreation Center</a></li>
				<li><a href="http://www.hcprd.org">Henry County Parks & Recreation</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Houston">Houston County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/houston.html">Houston County Office</a></li>
				<li><a href="http://www.n-georgia.com/houston_co.htm">Play in Houston County and City Parks</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Irwin">Irwin County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/irwin.html">Irwin County Office</a></li>
				<li><a href="http://gastateparks.org/JeffersonDavis">Jefferson Davis Memorial Historic Site </a></li>
				<li><a href="http://irwin.gafcp.org/files/Irwin-County-Resource-Book.pdf">Irwin County, GA Resource Book</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Jackson">Jackson County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/jackson.html">Jackson County Office</a></li>
				<li><a href="http://www.jacksoncountygov.com/Index.aspx?page=723">Jackson County Parks & Facilities </a></li>
				<li><a href="http://www.jacksoncountygov.com/Index.aspx?page=735">Jackson County Sports & Athletics </a></li>
				<li><a href="http://www.jeffersonrec.com">City of Jefferson Parks & Recreation Department</a></li>
				<li><a href="http://teenmattersjackson.com">Teen Matters</a></li>
				<li><a href="http://www.northridgemc.com">Northridge Medical Center</a></li>
				<li><a href="https://www.facebook.com/Jeffersonrec">Jefferson Parks & Recreation - Facebook</a></li>
				<li><a href="http://www.jacksonrec.com">Jackson County Parks & Recreation</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Jasper">Jasper County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/jasper.html">Jasper County Office</a></li>
				<li><a href="http://www.jaspercorec.com">Jasper County Recreation Department</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Jeff-Davis">Jeff Davis County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/jeffdavis.html">Jeff Davis County Office</a></li>
				<li><a href="https://www.facebook.com/Jeff-Davis-Recreation-Department-163068523857172/">Jeff Davis Recreation Department - Facebook</a></li>
				<li><a href="http://www.hazlehurst-jeffdavis.org/about/schools-recreation/">Jeff Davis County Schools & Recreation Department</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Jefferson">Jefferson County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/jefferson.html">Jefferson County Office</a></li>
				<li><a href="http://www.jeffersoncountyga.gov">Jefferson County, Georgia </a></li>
				<li><a href="http://www.jeffersoncounty.org/default.htm">Jefferson County - Welcome</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Jenkins">Jenkins County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/jenkins.html">Jenkins County Office</a></li>
				<li><a href="http://gastateparks.org/MagnoliaSprings">Magnolia Springs State Park</a></li>
				<li><a href="http://www.jenkinscountyrecdept.com">Jenkins County Recreation Department</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Johnson">Johnson County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/johnson.html">Johnson County Office</a></li>
				<li><a href="http://johnsonco.org/recreation-department/">Johnson County Recreation Department</a></li>
				<li><a href="http://www.johnson.k12.ga.us/system/index.htm">Johnson County Schools</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Jones">Jones County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/jones.html">Jones County Office</a></li>
				<li><a href="https://www.facebook.com/Jones-County-Parks-and-Recreation-139424366069639/">Jones County Parks and Recreation</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Lamar">Lamar County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/lamar.html">Lamar County Office</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Lanier">Lanier County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/lanier.html">Lanier County Office</a></li>
				<li><a href="http://www.lakelandchamber.org/parks-trails.htm">Lanier Lakeland Parks & Trails</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Laurens">Laurens County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/laurens.html">Laurens County Office</a></li>
				<li><a href="http://www.dlcra.org">Dublin Laurens County Recreational Authority </a></li>
				<li><a href="http://www.georgiawildlife.org/PFA/HughMGillis">Hugh M. Gillis PFA (Laurens County)</a></li>
				<li><a href="http://www.n-georgia.com/laurens-county-parks.html">Laurens County Georgia Parks</a></li>
				<li><a href="http://gastateparks.org/LittleOcmulgee">Little Ocmulgee State Park and Lodge</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Lee">Lee County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/lee.html">Lee County Office</a></li>
				<li><a href="http://www.leecountyga.com">Lee Parks & Recreation</a></li>
				<li><a href="http://www.lee.ga.us/government/departments/parksrec.html">Lee County Parks & Recreation Authority </a></li>
				<li><a href="http://leechamber.biz/recreation">Lee County Recreation</a></li>
				<li><a href="http://www.n-georgia.com/lee_co.htm">Lee County Georgia Parks</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Liberty">Liberty County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/liberty.html">Liberty County Office</a></li>
				<li><a href="http://gastateparks.org/FortMorris">Fort Morris Historic Site </a></li>
				<li><a href="http://gastateparks.org/FortMorris">Liberty County Recreation Department</a></li>
				<li><a href="http://libertycounty.org/see-liberty/parks-recreation-2/">Fort Stewart </a></li>
				<li><a href="https://www.facebook.com/LibertyCountyRecreationDepartment">Liberty County Recreation Department - Facebook</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Lincoln">Lincoln County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/lincoln.html">Lincoln County Office</a></li>
				<li><a href="http://gastateparks.org/ElijahClark">Elijah Clark State Park</a></li>
				<li><a href="http://www.lcgagov.org/parks--recreation.html">Lincoln County Recreation</a></li>
				<li><a href="http://www.n-georgia.com/lincoln_co.htm">Discover Georgia's Lincoln County and City Parks</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Long">Long County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/long.html">Long County Office</a></li>
				<li><a href="http://georgia.gov/cities-counties/ludowici">Ludowici</a></li>
				<li><a href="https://www.facebook.com/pages/Long-County-Rec-Dept/207925939218115">Long County Rec Dept </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Lowndes">Lowndes County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/lowndes.html">Lowndes County Office</a></li>
				<li><a href="http://vlpra.com">Valdosta-Lowndes County Parks & Recreation Authority </a></li>
				<li><a href="http://www.lowndescounty.com/361/Parks-and-Recreation-Authority">Valdosta-Lowndes County Parks & Recreation Authority </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Lumpkin">Lumpkin County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/lumpkin.html">Lumpkin County Office</a></li>
				<li><a href="http://gastateparks.org/DahlonegaGoldMuseum">Dahlonega Gold Museum Historic Site</a></li>
				<li><a href="http://www.lumpkincounty.gov/parks-recreation/">Lumpkin County Government Parks and Recreation</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Macon">Macon County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/macon.html">Macon County Office</a></li>
				<li><a href="http://www.maconcountyga.gov/recreation-department.cfm">Macon County, GA Recreation Department</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Madison">Madison County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/madison.html">Madison County Office</a></li>
				<li><a href="http://gastateparks.org/VictoriaBryant">Victoria Bryant State Park</a></li>
				<li><a href="http://www.walkgeorgia.org/resources/www.madcorec.com"></a></li>
				<li><a href="http://www.madcorec.com">Madison County Recreation Department</a></li>
				<li><a href="http://www.broadriveroutpost.com"></a></li>
				<li><a href="http://www.madison.k12.ga.us/schools/hullsanfordelementary/">Hull-Sanford Elementary</a></li>
				<li><a href="http://www.madison.k12.ga.us/schools/ilaelementary/">Ila Elementary School</a></li>
				<li><a href="http://www.madison.k12.ga.us/schools/mchs/">Madison County High School</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Marion">Marion County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/marion.html">Marion County Office</a></li>
				<li><a href="https://www.facebook.com/Marion-County-Recreation-Department-213721641971865/?ref=ts">Marion County Recreation Department</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="McDuffie">McDuffie County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/mcduffie.html">McDuffie County Office</a></li>
				<li><a href="http://www.thomsonmcduffierecreation.com">Thomson-McDuffie County Recreation and Leisure Services </a></li>
				<li><a href="http://www.exploremcduffiecounty.com/things-to-do/">Thomson-McDuffie County Convention & Visitors Bureau</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="McIntosh">McIntosh County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/mcintosh.html">McIntosh County Office</a></li>
				<li><a href="http://gastateparks.org/FortKingGeorge">Fort King George Historic Site</a></li>
				<li><a href="http://gastateparks.org/ReynoldsMansion">Sapelo Island Reserve and Reynolds Mansion </a></li>
				<li><a href="http://www.n-georgia.com/mcintosh_co.htm">McIntosh County and City Parks</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Meriwether">Meriwether County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/meriwether.html">Meriwether County Office</a></li>
				<li><a href="http://gastateparks.org/FDRoosevelt">F.D. Roosevelt State Park</a></li>
				<li><a href="http://gastateparks.org/LittleWhiteHouse">Little White House Historic State Park</a></li>
				<li><a href="http://manchester-ga.gov/departments/recreation/">City of Manchester Recreation Department</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Miller">Miller County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/miller.html">Miller County Office</a></li>
				<li><a href="http://www.countyoffice.org/ga-miller-county-parks-department/">Miller County Parks Department</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Mitchell">Mitchell County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/mitchell.html">Mitchell County Office</a></li>
				<li><a href="http://www.mitchellcountyga.net/movinghereeducation.html">Mitchell County Education </a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Monroe">Monroe County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/monroe.html">Monroe County Office</a></li>
				<li><a href="http://gastateparks.org/JarrellPlantation">Jarrell Plantation Site</a></li>
				<li><a href="http://monroecountygeorgia.com/pages.php?s=90&p=25"></a></li>
				<li><a href="https://www.facebook.com/Monroe-County-Parks-and-Recreation-199735700081041/">Monroe County Parks and Recreation - Facebook</a></li>
				<li><a href="http://www.mocorec.org/Default.aspx?tabid=626597&mid=650890&newskeyid=HN1&ctl=viewallnews">Monroe County Parks and Recreation</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Montgomery">Montgomery County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/montgomery.html">Montgomery County Office</a></li>
				<li><a href="http://www.gobaronsgo.com">Brewton-Parker Barons</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Morgan">Morgan County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/morgan.html">Morgan County Office</a></li>
				<li><a href="http://gastateparks.org/HardLaborCreek">Hard Labor Creek State Park</a></li>
				<li><a href="http://mcplayrec.org">Morgan County Recreation Department</a></li>
				<li><a href="https://www.facebook.com/Morgan-County-Parks-and-Recreation-Department-311851819088/">Morgan County Parks and Recreation Department - Facebook</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Murray">Murray County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/murray.html">Murray County Office</a></li>
				<li><a href="http://gastateparks.org/ChiefVannHouse">Chief Vann House Historic Site</a></li>
				<li><a href="http://gastateparks.org/FortMountain">Fort Mountain State Park</a></li>
				<li><a href="http://www.murrayrec.com">Murray County Parks and Recreation</a></li>
				<li><a href="http://www.murraycountyga.org/Facilities.aspx?page=list&search=1&CID=1">Murray County Parks and Recreation Facilities </a></li>
				<li><a href="https://www.facebook.com/pages/Murray-County-Recreation-Department/152599611467413">Murray County Recreation Department - Facebook</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Muscogee">Muscogee County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/muscogee.html">Muscogee County Office</a></li>
				<li><a href="http://www.livehealthycolumbus.org">Live Healthy Columbus </a></li>
				<li><a href="http://www.columbusga.org/parks/">Columbus Department of Parks and Recreation</a></li>
				<li><a href="http://www.n-georgia.com/muscogee-county-parks.html">Muscogee County Georgia Parks</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Newton">Newton County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/newton.html">Newton County Office</a></li>
				<li><a href="http://www.newtonrecreation.com">Newton County Recreation</a></li>
				<li><a href="http://www.newtontrails.org/">Newton Trails</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Oconee">Oconee County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/oconee.html">Oconee County Office</a></li>
				<li><a href="http://www.oconeecounty.com/ocprd/">Oconee County Parks and Recreation Department</a></li>
				<li><a href="https://www.facebook.com/OconeeCountyParksandRecreation">Oconee County Parks and Recreation Department Facebook</a></li>
				<li><a href="#top">Back to Top</a></li>
      </ul>
      <!-- End County -->

      <!-- County -->
      <h2 class="custom-font-small-blue" id="Oglethorpe">Oglethorpe County:</h2>
      <ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/oglethorpe.html">Oglethorpe County Office</a></li>
				<li><a href="http://www.n-georgia.com/oglethorpe-county-parks.html">Oglethorpe County Parks/a></li>
				<li><a href="http://www.onlineoglethorpe.com/Senior-Citizens-Center-v-34.html">Senior Citizens Center</a></li>
				<li><a href="http://www.gastateparks.org/WatsonMillBridge">Watson Mill Bridge State Park</a></li>
				<li><a href="http://www.onlineoglethorpe.com/Oglthorpe-County-Parks-and-Recreation-v-14.html">Oglethorpe County Parks and Recreation</a></li>
				<li><a href="https://www.facebook.com/OglethorpeCRD">Oglethorpe County Recreation Department Facebook Page</a></li>
				<li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Paulding">Paulding County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/paulding.html">Paulding County Office</a></li>
			  <li><a href="http://gastateparks.org/PickettsMillBattlefield">Pickett's Mill Battlefield Historic Site</a></li>
			  <li><a href="http://www.paulding.gov/index.aspx?NID=640">Paulding County Parks</a></li>
			  <li><a href="http://www.pauldingchamber.org/todayParks.php">Paulding County Chamber of Commerce Parks and Rec</a></li>
			  <li><a href="http://www.n-georgia.com/paulding_co.htm">Paulding County and City Parks</a></li>
			  <li><a href="http://www.cityofhiramga.gov/129/Parks-Recreation">Hiram Georgia Parks and Recreation</a></li>
			  <li><a href="http://www.accesspaulding.com/parks.html">Access Paulding</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Peach">Peach County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/peach.html">Peach County Office</a></li>
			  <li><a href="http://www.historicmiddlegeorgia.org/everett.php">Historic Middle Georgia</a></li>
			  <li><a href="https://www.facebook.com/pages/Hunt-Educational-Cultural-Center/120803431264659">Hunt Educational and CUltural Center Facebook Page</a></li>
			  <li><a href="http://www.peachcounty.net/recreation.cfm">Peach County Recreation</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Pickens">Pickens County:</h2>
			<ul class="pt" style="list-style:none;">
			  <li><a href="http://extension.uga.edu/county-offices/pickens.html">Pickens County Office</a></li>
			  <li><a href="http://pickenscountyrecdept.com">Pickens County Recreation and Parks Department</a></li>
			  <li><a href="https://www.facebook.com/pickenscountyrecreationdept">Pickens County Recreation Department Facebook Page</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Pierce">Pierce County:</h2>
			<ul class="pt" style="list-style:none;">
			  <li><a href="http://extension.uga.edu/county-offices/pierce.html">Pierce County Office</a></li>
			  <li><a href="https://www.facebook.com/pages/Pierce-County-Parks-Recreation/155583594532936">Pierce County Parks and Recreation Facebook Page</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Pike">Pike County:</h2>
			<ul class="pt" style="list-style:none;">
			  <li><a href="http://extension.uga.edu/county-offices/pike.html">Pike County Office</a></li>
			  <li><a href="http://www.pikecoga.com/#!recreationauthority/cc00">Pike County Parks and Recreation Authority</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Polk">Polk County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/polk.html">Polk County Office</a></li>
			  <li><a href="http://www.polkgeorgia.com/live/parks-recreation">Cedartown Parks and Recreation</a></li>
			  <li><a href="http://www.n-georgia.com/polk-county-parks.html">Polk County Georgia Parkjss</a></li>
			  <li><a href="https://www.facebook.com/pages/Cedartown-Recreation-Department/597252173633985">Cedartown Recreation Department Facebook Page</a></li>
			  <li><a href="http://www.cedartowngeorgia.gov">City of Cedartown</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Pulaski">Pulaski County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/pulaski.html">Pulaski County Office</a></li>
			  <li><a href="http://www.hawkinsvillega.net/Recreation.html">Pulaski County Recreation Department</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Putnam">Putnam County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/putnam.html">Putnam County Office</a></li>
			  <li><a href="http://www.putnamcountyga.us/departments/recreation/">Putnam County Recreation Department</a></li>
			  <li><a href="http://www.n-georgia.com/putnam_co.htm">Putnam County Georgia Parks</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Quitman">Quitman County:</h2>
			<ul class="pt" style="list-style:none;">
			  <li><a href="http://extension.uga.edu/county-offices/quitman.html">Quitman County Office</a></li>
			  <li><a href="http://www.recreation.gov/recreationalAreaDetails.do?contractCode=NRSO&recAreaId=449">New Quitman County</a></li>
			  <li><a href="http://quitman.ga.qce.schoolinsites.com/?PageName='Sports'">Walter F. Georgia Lake</a></li>
			  <li><a href="http://www.georgetown-quitman.org/about/park-marina">Park and Marina</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Rabun">Rabun County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/rabun.html">Rabun County Office</a></li>
			  <li><a href="http://gastateparks.org/BlackRockMountain">Black Rock Mountain State Park</a></li>
			  <li><a href="http://gastateparks.org/TallulahGorge">Tallulah Gorge State Park</a></li>
			  <li><a href="https://www.facebook.com/RabunCountyRecreationDepartment">Rabun County Recreation Department Facebook Page</a></li>
			  <li><a href="http://www.n-georgia.com/rabun_co.htm">Rabun County Parks</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Randolph">Randolph County:</h2>
			<ul class="pt" style="list-style:none;">
			  <li><a href="http://extension.uga.edu/county-offices/randolph.html">Randolph County Office</a></li>
			  <li><a href="http://www.randolphcountychamber.org/Keep-Randolph-Beautiful.html">Keep Randolph Beautiful</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Richmond">Richmond County:</h2>
			<ul class="pt" style="list-style:none;">
			  <li><a href="http://extension.uga.edu/county-offices/richmond.html">Richmond County Office</a></li>
			  <li><a href="http://www.augustaga.gov/645/Recreation-Parks-Facilities">Augusta Recreation, Parks, and Facilities</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Rockdale">Rockdale County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/rockdale.html">Rockdale County Office</a></li>
			  <li><a href="http://www.n-georgia.com/rockdale_co.htm">Rockdale County and City Parks</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Schley">Schley County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/schley.html">Schley County Office</a></li>
				<li><a href="http://www.columusga.org/parks/">Columbus Parks and Recreation Department</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Screven">Screven County:</h2>
			<ul class="pt" style="list-style:none;">
			  <li><a href="http://extension.uga.edu/county-offices/screven.html">Screven County Office</a></li>
			  <li><a href="http://www.screvenrec.com">Screven County Recreation Department</a></li>
			  <li><a href="https://www.facebook.com/pages/Screven-County-Recreation-Department/117804871602445">Screven County Recreation Department Facebook Page</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Seminole">Seminole County:</h2>
			<ul class="pt" style="list-style:none;">
			  <li><a href="http://extension.uga.edu/county-offices/seminole.html">Seminole County Office</a></li>
			  <li><a href="http://gastateparks.org/Seminole">Seminole State Park</a></li>
			  <li><a href="http://www.seminolerecreations.com/volleyball.htm">Seminole County Recreation</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Spalding">Spalding County:</h2>
			<ul class="pt" style="list-style:none;">
			  <li><a href="http://extension.uga.edu/county-offices/spalding.html">Spalding County Office</a></li>
			  <li><a href="http://spaldingparksandrec.com">Spalding County Parks, Public Grounds & Leisure Services</a></li>
			  <li><a href="https://www.facebook.com/pages/Spalding-County-Parks-Recreation/172245249366?ref=nf">Spalding County Paks andn Recreation Facebook Page</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Stephens">Stephens County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/stephens.html">Stephens County Office</a></li>
			  <li><a href="http://gastateparks.org/TravelersRest">Travelers Rest Historic Site</a></li>
			  <li><a href="http://www.stephenscountyga.com/RecreationDepartment.cfm">Stephens County Parks and Recreation</a></li>
			  <li><a href="https://www.facebook.com/pages/Stephens-County-Recreation-Department/198649836876235">Stephens County Recreation Department Facebook Page</a></li>
			  <li><a href="http://www.n-georgia.com/stephens_co.htm">Stephens County Georgia Parks</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Stewart">Stewart County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/stewart.html">Stewart County Office</a></li>
			  <li><a href="http://gastateparks.org/FlorenceMarina">Florence Marina State Park</a></li>
			  <li><a href="http://gastateparks.org/ProvidenceCanyon">Providence Canyon State Park</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Sumter">Sumter County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/sumter.html">Sumter County Office</a></li>
			  <li><a href="http://www.scprd.org">Sumter County Parks and Recreation Department</a></li>
			  <li><a href="http://www.sumtercountyga.us/index.aspx?NID=71">Sumpter County</a></li>
			  <li><a href="http://visitamericusga.com/local-area-events/">Local Area Events</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Talbot">Talbot County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/talbot.html">Talbot County Office</a></li>
			  <li><a href="http://www.talbotcountychamber.org/recreation.php">Things to do in Talbot County Georgia</a></li>
			  <li><a href="https://www.facebook.com/pages/Talbot-County-Recreation/248293742023478">Talbot County Recreation Facebook Page</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Taliaferro">Taliaferro County:</h2>
			<ul class="pt" style="list-style:none;">
			  <li><a href="http://gastateparks.org/AHStephens">A.H. Stephens Historic Park</a></li>
			  <li><a href="http://taliaferrocountyga.org/index.php?page=geocaching">Taliaferro County Geocaching</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Tattnall">Tattnall County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/tattnall.html">Tattnall County Office</a></li>
			  <li><a href="http://gastateparks.org/GordoniaAlatamaha">Gordonia-Alatamaha State Park</a></li>
			  <li><a href="http://www.tattnall.com/Recreation.html">Tattnall County Sports, Parks, and Recreation</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Taylor">Taylor County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/taylor.html">Taylor County Office</a></li>
				<li><a href="http://www.taylorga.us/page36.php">Taylor County Parks and Recreation</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Telfair">Telfair County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/telfair.html">Telfair County Office</a></li>
			  <li><a href="http://gastateparks.org/LittleOcmulgee">Little Ocmulgee State Park</a></li>
			  <li><a href="http://www.exploregeorgia.org/listing/4519-telfair-county-recreation-complex">Telfair County Recreation Complex</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Terrell">Terrell County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/terrell.html">Terrell County Office</a></li>
			  <li><a href="http://www.terrellcountyrec.com/wilbur-t-gamble-jr-recreation-complex.html">Terrell County Recreation Department Facebook Page</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Thomas">Thomas County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/thomas.html">Thomas County Office</a></li>
			  <li><a href="http://gastateparks.org/LaphamPatterson">Lapham-Patterson House Historic Site</a></li>
			  <li><a href="http://www.thomasville.org/Content/Default/10/236/231/city-of-thomasville/parks-and-facilities.html">Thomasville Parks and Recreation</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Tift">Tift County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/tift.html">Tift County Office</a></li>
			  <li><a href="http://www.tiftcounty.org/departmentHome.php?dp=2">Tifton Recreation</a></li>
			  <li><a href="http://www.n-georgia.com/tift-county-parks.html">Tift County and City Parks</a></li>
			  <li><a href="http://www.tifton.net/residents/recreation/">Tift County Recreation </a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Toombs">Toombs County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/toombs.html">Toombs County Office</a></li>
			  <li><a href="http://gastateparks.org/RobertToombsHouse">Robert Toomb's House</a></li>
			  <li><a href="http://lyonsrec.com">Lyons Recreation Department</a></li>
			  <li><a href="http://www.vidaliaga.com/parksrecreation.html">Vidalia Recreation Department</a></li>
			  <li><a href="https://www.facebook.com/VidaliaRecreationDepartment?fref=ts">Vidalia Recreation Department Facebook Page</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>

			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Towns">Towns County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/towns.html">Towns County Office</a></li>
			  <li><a href="http://www.n-georgia.com/towns-county-parks.html">Towns County Parks</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Treutlen">Treutlen County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/treutlen.html">Treutlen County Office</a></li>
			  <li><a href="http://www.treutlencountyga.com/living-here/sports-recreation/">Treutlen County Sports and Recreation </a></li>
			  <li><a href="http://www.soperton-treutlen.org">Soperton, GA</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Troup">Troup County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/troup.html">Troup County Office</a></li>
			  <li><a href="http://www.trouprec.org">Troup County Parks and Recreation</a></li>
			  <li><a href="https://www.facebook.com/troupparks">Troup County Parks and Recreation Facebook</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Turner">Turner County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/turner.html">Turner County Office</a></li>
			  <li><a href="http://turnercountygeorgia.com/departments/recdept.html">Turner County Recreation Department</a></li>
			  <li><a href="http://www.exploregeorgia.org/city/ashburn">Explore Georgia: Ashburn</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Twiggs">Twiggs County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/twiggs.html">Twiggs County Office</a></li>
			  <li><a href="http://www.nps.gov/ocmu/index.htm">National Park Service: Ocmulgee</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Union">Union County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/union.html">Union County Office</a></li>
			  <li><a href="http://gastateparks.org/Vogel">Vogel State Park</a></li>
			  <li><a href="http://www.unioncountyga.gov/Departments/ParksRecreation.aspx">Union County Parks and Recreation </a></li>
			  <li><a href="http://www.uccommunitycenter.com">Union County Community Center </a></li>
			  <li><a href="http://www.blairsvillechamber.com/fun-things-to-do-in-blairsville-ga/outdoor-activities/">Blairsville Outdoor Activities </a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Upson">Upson County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/upson.html">Upson County Office</a></li>
			  <li><a href="http://gastateparks.org/item/167258">Sprewell Bluff State Park</a></li>
			  <li><a href="http://www.upsoncountyga.org/departments/recreation.htm">Upson County Department of Recreation </a></li>
			  <li><a href="https://www.facebook.com/pages/Thomaston-Upson-Civic-Center/153276161433118">Thomaston-Upton Civic Center Facebook Page </a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Walker">Walker County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/walker.html">Walker County Office</a></li>
			  <li><a href="http://www.walkerga.us/living/recreation.aspx">Walker County Recreation </a></li>
			  <li><a href="http://cityoflafayettega.org/recreation/parks-and-recreation/">City of Lafayette Parks and Recreation </a></li>
			  <li><a href="https://www.facebook.com/pages/Lafayette-Parks-and-Recreation-Municipal-Park/132572416812894">Lafayette Parks and Recreation Municipal Park Facebook Page </a></li>
			  <li><a href="http://www.n-georgia.com/walker_co.htm">Walker County and City Parks </a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Walton">Walton County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/walton.html">Walton County Office</a></li>
			  <li><a href="http://www.waltoncountyga.gov/departments/parks-recreation/find-a-park-by-amenities/">Walton County Parks </a></li>
			  <li><a href="http://www.waltoncountyseniorfitness.com/index.htm">Walton County Senior Fitness </a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Ware">Ware County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/ware.html">Ware County Office</a></li>
			  <li><a href="http://gastateparks.org/LauraSWalker">Laura S. Walker State Park</a></li>
			  <li><a href="http://www.warecounty.com/ResidentsParksRecreation1.aspx">Ware County Parks and Recreation </a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Warren">Warren County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/warren.html">Warren County Office</a></li>
				<li><a href="http://www.warrencountyga.com/newcomers-guide.html">Warren County Newcomers Guide </a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Washington">Washington County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/washington.html">Washington County Office</a></li>
			  <li><a href="http://washingtoncountyga.gov/departments/parks-and-recreation/">Washington County Parks and Recreation</a></li>
			  <li><a href="https://www.facebook.com/washingtoncountyrecreationdepartment">Washington County Parks and Recreation Facebook</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Wayne">Wayne County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/wayne.html">Wayne County Office</a></li>
			  <li><a href="https://www.facebook.com/waynecountyrecreation">Wayne County Parks and Recreation Facebook Page</a></li>
			  <li><a href="http://www.waynecountyrecreation.com/index.cfm/contact-us/">Wayne County Parks and Recreation </a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Webster">Webster County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/webster.html">Webster County Office</a></li>
			  <li><a href="http://www.nps.gov/jica/index.htm">Jimmy Carter National Historic Site </a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->


			<!-- County -->
			<h2 class="custom-font-small-blue" id="Wheeler">Wheeler County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/wheeler.html">Wheeler County Office</a></li>
			  <li><a href="http://www.gastateparks.org/LittleOcmulgee">Little Ocmulgee State Park </a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="White">White County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/white.html">White County Office</a></li>
			  <li><a href="https://www.google.com/maps/place/Buck+Shoals+State+Park ">Buck Shoals State Park</a></li>
			  <li><a href="http://gastateparks.org/SmithgallWoods">Smithgall Woods Conservation Area</a></li>
			  <li><a href="http://gastateparks.org/Unicoi">Unicoi State Park</a></li>
			  <li><a href="http://gastateparks.org/HardmanFarm">Hardman Farm State Historic Site</a></li>
			  <li><a href="http://www.whitecounty.net/recreation/rec.htm">White County Recreation Department</a></li>
			  <li><a href="http://www.n-georgia.com/white-county-parks.html">White County Parks </a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Whitfield">Whitfield County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/whitfield.html">Whitfield County Office</a></li>
			  <li><a href="https://www.whitfieldcountyga.com/rec/parkrec.htm">Whitfield County Parks and Recreation</a></li>
			  <li><a href="https://www.facebook.com/whitfieldcountyrecreationdepartment">Whitfield County Recreation Department Facebook</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Wilcox">Wilcox County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/wilcox.html">Wilcox County Office</a></li>
			  <li><a href="http://www.wilcoxrec.com">Wilcox County Recreation Center </a></li>
			  <li><a href="https://www.facebook.com/pages/Wilcox-County-Recreation-Dept/233433425767">Wilcox County Recreation Department Facebook</a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Wilkes">Wilkes County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/wilkes.html">Wilkes County Office</a></li>
			  <li><a href="http://gastateparks.org/RobertToombsHouse">Robert Toombs House Historic Site</a></li>
			  <li><a href="http://www.washingtonwilkes.org/about/parks-recreation">Washington-Wilkes Parks & Recreation </a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Wilkinson">Wilkinson County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/wilkinson.html">Wilkinson County Office</a></li>
			  <li><a href="http://www.wilkinsoncounty.net/departments.php?County-Offices-Recreation-Board-46">Wilkinson County Recreation Board </a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

			<!-- County -->
			<h2 class="custom-font-small-blue" id="Worth">Worth County:</h2>
			<ul class="pt" style="list-style:none;">
				<li><a href="http://extension.uga.edu/county-offices/worth.html">Worth County Office</a></li>
			  <li><a href="https://www.facebook.com/pages/Sylvester-Worth-County-Recreation-Department/136765579758213">Worth County Recreation Department </a></li>
			  <li><a href="#top">Back to Top</a></li>
			</ul>
			<!-- End County -->

      </div>
    </div>
    <!-- End County List -->
    <!-- End Main Content -->
  </div>
</div>

<?php

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
<script type="text/javascript">
$(function() {

	$('#toggle4').click(function() {
		$('.toggle4').slideToggle('fast');
		return false;
	});

});
</script>

<script type="text/javascript">
$(document).ready(function () {
	$('#select-anchor').change( function () {
		var targetPosition = $($(this).val()).offset().top - 150;
		$('html,body').animate({ scrollTop: targetPosition}, 'slow');
	});
});
</script>

<script type="text/javascript">
function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
</script>
<script type="text/javascript">
(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
</script>
<script src="../js/foundation/foundation.abide.js"></script>
<script src="../js/foundation/foundation.reveal.js"></script>
<script src="../js/foundation/foundation.tooltip.js"></script>
<script src="../js/foundation/foundation.offcanvas.js"></script>
<script src="../js/foundation/foundation.equalizer.js"></script>
<script src="../js/foundation/foundation.dropdown.js"></script>
<script type="text/javascript" src="../js/sha512.js"></script>
<script type="text/javascript" src="../js/log_form.js"></script>
<script>
$(document).foundation();
</script>
<!-- End Footer -->
</body>
</html>
