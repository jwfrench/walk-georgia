<?php
include_once("../../lib/template_api.php");
include_once ("../../lib/back_api.php");
$ss = SESSION::secure_session();

$_GET['logout'] = (filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null) ? filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) : ""; //defining variable 'lougout'
if($_GET['logout']==1){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

<div id="main">

  <!-- Global Header Container -->
  <div id="top nojava">
  
    <!-- Header Image Container -->
    <div class="green-bg" style="margin-top:0px;">
      
      <!-- Page Title Container -->
      <div style="margin-top:40px;">
        <div class="row nojava center">
          <h1 class="custom-font-big-white">fish and vegetable bake</h1>
          <p class="gotham-small-white">A meal of mixed vegetables and white fish is one of the most versatile, healthy, and flavorful combinations.</p> 
          <br /><br /><br />
              
        </div>
      </div>
      <!-- End Page Title Container --> 
       
    </div>
    <!-- End Header Image Container -->
    
    <!-- Full Width Sub Menu -->
    
    <div class="blue-bg" style="margin-top:0px; padding:10px; padding-bottom:0px;">
      <a href="index.php" class="button round success tiny" style="margin-top:10px;">Back to Recipe Index</a>
    </div>
    
    <!-- End Full Width Sub Menu -->
    
  </div>
  <!-- End Global Header Container -->
  
    
  <!-- Main Content -->
  
  <div class="row" style="margin-top:40px;">
    
    <div class="large-6 medium-6 columns">
      <img src="img/recipes-fish-veg-bake.png" alt="spaghetti squash picture" />
    </div>
    
    <div class="large-3 medium-3 columns">
    
      <h2 class="custom-font-small">You'll Need:</h2>
      <ul class="global-p">
        <li>1 pound fresh or frozen white fish, thawed</li>
        <li>16 ounces frozen stir fry vegetable mix</li>
        <li>1 tablespoon Mrs. Dash lemon pepper seasoning</li>
        <li>1 tablespoon fresh parsley</li>
        <li>1⁄2 cup low sodium vegetable juice cocktail</li>
        <li>2 tablespoons parmesan cheese</li>
      </ul>
    </div>
      
    <div class="large-3 medium-3 columns">
      <h2 class="custom-font-small">Take Out:</h2>
      <ul class="global-p">
        <li>13x9 inch baking dish</li>
        <li>Heavy foil</li>
        <li>Scissors</li>
        <li>Measuring cups and spoons</li>
      </ul>
    </div>
      
  </div>
  
  <div class="row" style="margin-top:15px;">
    <div class="large-12 columns">
      <h2 class="custom-font-small">Directions:</h2>
    </div>
  </div>
  
  <div class="row">
    <div class="large-1 hide-for-medium-down columns">
      <h3 class="custom-font-big-blue">1.</h3>
    </div>
    <div class="large-11 columns">
      <p class="global-p" style="margin-top:15px;">
        Preheat oven to 400 degrees F.
      </p>
    </div>
  </div>
  
  <div class="row">
    <div class="large-1 hide-for-medium-down columns">
      <h3 class="custom-font-big-blue">2.</h3>
    </div>
    <div class="large-11 columns">
      <p class="global-p" style="margin-top:15px;">
        Line a 13x9 inch baking dish with heavy duty foil.
      </p>
    </div>
  </div>
  
  <div class="row">
    <div class="large-1 hide-for-medium-down columns">
      <h3 class="custom-font-big-blue">3.</h3>
    </div>
    <div class="large-11 columns">
      <p class="global-p" style="margin-top:15px;">
        Place fish on foil and top with vegetables and seasonings.
      </p>
    </div>
  </div>
  
  <div class="row">
    <div class="large-1 hide-for-medium-down columns">
      <h3 class="custom-font-big-blue">4.</h3>
    </div>
    <div class="large-11 columns">
      <p class="global-p" style="margin-top:15px;">
        Pour on juice and top with cheese.
      </p>
    </div>
  </div>
  
  <div class="row">
    <div class="large-1 hide-for-medium-down columns">
      <h3 class="custom-font-big-blue">5.</h3>
    </div>
    <div class="large-11 columns">
      <p class="global-p" style="margin-top:15px;">
        Cover and bake for 30-40 minutes until fish flakes and vegetables are tender crisp.
      </p>
    </div>
  </div>
  
  <div class="row">
    <div class="large-1 hide-for-medium-down columns">
      <h3 class="custom-font-big-blue">6.</h3>
    </div>
    <div class="large-11 columns">
      <p class="global-p" style="margin-top:15px;">
        Serve with rice.
      </p>
    </div>
  </div>
  
  
  <div class="row" style="margin-top:15px;">
    <div class="large-12 columns">
    <h2 class="custom-font-small">Four Servings:</h2>
    <p class="global-p">
      Calories: 152 | Carbohydrate: 9 grams | Fat: 1 gram | Cholesterol: 51 milligrams | Sodium: 153 milligrams | Fiber: 3 grams 
    </p>
    <p class="global-p">
      Exchanges: 3 very lean meats, 2 vegetables
    </p>
    </div>
  </div>
  
  
    
  <!-- End Main Content -->
            
</div>

<?php

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script src="../../js/foundation/foundation.abide.js"></script>
    <script src="../../js/foundation/foundation.reveal.js"></script>
    <script src="../../js/foundation/foundation.tooltip.js"></script>
    <script src="../../js/foundation/foundation.offcanvas.js"></script>
    <script src="../../js/foundation/foundation.equalizer.js"></script>
    <script src="../../js/foundation/foundation.dropdown.js"></script>
    <script type="text/javascript" src="../../js/sha512.js"></script>
    <script type="text/javascript" src="../../js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>