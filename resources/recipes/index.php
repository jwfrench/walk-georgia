<?php
include_once("../../lib/template_api.php");
include_once ("../../lib/back_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

<div id="main">

  <!-- Global Header Container -->
  <div id="top nojava">
  
    <!-- Header Image Container -->
    <div id="slideshow">
      
      <!-- Page Title Container -->
      <div id="slideshow-title">
        <div class="row nojava center">
          <h1 class="font -big -secondary -white">recipes</h1>
          <p class="font -primary -small-medium -white -lowercase -space">We've compiled an archive of easy, healthy, and local recipes for you to try. Find something delicious and easy today below!</p> 
          <br /><br /><br /> 
              
        </div>
      </div>
      <!-- End Page Title Container --> 
       
    </div>
    <!-- End Header Image Container -->
    
  </div>
  <!-- End Global Header Container -->
  
  <!-- Full Width Sub Menu -->
    
    <div class="bg--blue" style="margin-top:0px; padding:10px; padding-bottom:0px;">
      <a href="../index.php" class="button round success tiny" style="margin-top:10px;">Back to Resources Index</a>
    </div>
    
    <!-- End Full Width Sub Menu -->
  
    
  <!-- Main Content -->
<section class="recipes">
  <!-- Recipe Section -->
  <div class="row">
    
    <div class="large-3 medium-3 hide-for-small columns">
      <img src="img/recipes-spaghetti-squash.png" />
    </div>
    
    <div class="large-9 medium-9 columns">
      <h2 class="font -medium -secondary -light-grey">An Herb Lover's Spaghetti Squash</h2>
      <p class="font -extra-small -dark-grey">
      Spaghetti squash is a nutritious substitute for regular pasta.  This recipe allows you to use any sauce you would normally use.  This recipe also goes well with any meat, fish, or poultry.  
      </p>
      <a href="spahettisquash.php" class="button round success">View This Recipe</a>
    </div>
    
  </div>
  <!-- End Recipe Section -->
  
  <!-- Recipe Section -->
  <div class="row">
    
    <div class="large-3 medium-3 hide-for-small columns">
      <img src="img/recipes-balsamic-chicken.png" />
    </div>
    
    <div class="large-9 medium-9 columns">
      <h2 class="font -medium -secondary -light-grey">Balsamic Chicken and Pears</h2>
      <p class="font -extra-small -dark-grey">
      A lean, tasty chicken dish full of protein and fresh ingredients.
      </p>
      <a href="balsamicchicken.php" class="button round success">View This Recipe</a>
    </div>
    
  </div>
  <!-- End Recipe Section -->
    <!-- Recipe Section -->
  <div class="row">
    
    <div class="large-3 medium-3 hide-for-small columns">
      <img src="img/recipes-fish-veg-bake.png" />
    </div>
    
    <div class="large-9 medium-9 columns">
      <h2 class="font -medium -secondary -light-grey">Fish and Vegetable Bake</h2>
      <p class="font -extra-small -dark-grey">
      A meal of mixed vegetables and white fish is one of the most versatile, healthy, and flavorful combinations.
      </p>
      <a href="fish-veg-bake.php" class="button round success">View This Recipe</a>
    </div>
    
  </div>
  <!-- End Recipe Section -->
    <!-- Recipe Section -->
  <div class="row">
    
    <div class="large-3 medium-3 hide-for-small columns">
      <img src="img/recipes-potato-soup.png" />
    </div>
    
    <div class="large-9 medium-9 columns">
      <h2 class="font -medium -secondary -light-grey">Potato Leek Soup</h2>
      <p class="font -extra-small -dark-grey">
      This soup is at the top of our guilt-free comfort food list! This dish is perfect the night before a race.
      </p>
      <a href="potatoSoup.php" class="button round success">View This Recipe</a>
    </div>
    
  </div>
  <!-- End Recipe Section -->
    <!-- Recipe Section -->
  <div class="row">
    
    <div class="large-3 medium-3 hide-for-small columns">
      <img src="img/recipes-baked-salmon.png" />
    </div>
    
    <div class="large-9 medium-9 columns">
      <h2 class="font -medium -secondary -light-grey">Baked Salmon with BBQ Mustard Sauce</h2>
      <p class="font -extra-small -dark-grey">
      This easy, quick and delicious meal will soon become a staple. Omega-rich salmon cooks to perfection in just 10 minutes, and the simple, homemade onion BBQ sauce is low in sugar.
      </p>
      <a href="bakedSalmon.php" class="button round success">View This Recipe</a>
    </div>
    
  </div>
  <!-- End Recipe Section -->
</section>
  <!-- End Main Content -->
            
  </div>

<?php

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script src="../../js/foundation/foundation.abide.js"></script>
    <script src="../../js/foundation/foundation.reveal.js"></script>
    <script src="../../js/foundation/foundation.tooltip.js"></script>
    <script src="../../js/foundation/foundation.offcanvas.js"></script>
    <script src="../../js/foundation/foundation.equalizer.js"></script>
    <script src="../../js/foundation/foundation.dropdown.js"></script>
    <script src="../../js/foundation/foundation.topbar.js"></script>
    <script type="text/javascript" src="../../js/sha512.js"></script>
    <script type="text/javascript" src="../../js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>