<?php
include_once("../../lib/template_api.php");
include_once ("../../lib/back_api.php");
$ss = SESSION::secure_session();

$_GET['logout'] = (filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null) ? filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) : ""; //defining variable 'lougout'
if($_GET['logout']==1){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

<div id="main">

  <!-- Global Header Container -->
  <div id="top nojava">
  
    <!-- Header Image Container -->
    <div class="green-bg" style="margin-top:0px;">
      
      <!-- Page Title Container -->
      <div style="margin-top:40px;">
        <div class="row nojava center">
          <h1 class="custom-font-big-white">baked salmon with BBQ mustard sauce</h1>
          <p class="gotham-small-white">This easy, quick and delicious meal will soon become a staple. Omega-rich salmon cooks to perfection in just 10 minutes, and the simple, homemade onion BBQ sauce is low in sugar. This meal pairs well with just about any grilled vegetable or rice pilaf!</p> 
          <br /><br /><br />
              
        </div>
      </div>
      <!-- End Page Title Container --> 
       
    </div>
    <!-- End Header Image Container -->
    
    <!-- Full Width Sub Menu -->
    
    <div class="blue-bg" style="margin-top:0px; padding:10px; padding-bottom:0px;">
      <a href="index.php" class="button round success tiny" style="margin-top:10px;">Back to Recipe Index</a>
    </div>
    
    <!-- End Full Width Sub Menu -->
    
  </div>
  <!-- End Global Header Container -->
  
    
  <!-- Main Content -->
  
  <div class="row" style="margin-top:40px;">
    
    <div class="large-6 medium-6 columns">
      <img src="img/recipes-baked-salmon.png" alt="baked salmon picture" />
    </div>
    
    <div class="large-3 medium-3 columns">
    
      <h2 class="custom-font-small">You'll Need:</h2>
      <ul class="global-p">
        <li>4 tablespoons cider vinegar</li>
		<li>2 packets artificial sweetener</li>
		<li>2 tablespoons Dijon mustard</li>
		<li>2 teaspoon cornstarch</li>
		<li>2 tablespoons orange juice</li>
		<li>1 green onion, sliced diagonally </li>
        <li>4-4 ounce salmon steaks or fillets </li>
        <li>1 teaspoon paprika</li>
      </ul>
    </div>
      
    <div class="large-3 medium-3 columns">
      <h2 class="custom-font-small">Take Out:</h2>
      <ul class="global-p">
        <li>Small saucepan</li>
		<li>Mixing spoon</li>
		<li>Measuring spoons</li>
		<li>Baking Sheet</li>
      </ul>
    </div>
      
  </div>
  
  <div class="row" style="margin-top:15px;">
    <div class="large-12 columns">
      <h2 class="custom-font-small">Directions:</h2>
    </div>
  </div>
  
  <div class="row">
    <div class="large-1 hide-for-medium-down columns">
      <h3 class="custom-font-big-blue">1.</h3>
    </div>
    <div class="large-11 columns">
      <p class="global-p" style="margin-top:15px;">
        Preheat oven to 425 degrees.
      </p>
    </div>
  </div>
  
  <div class="row">
    <div class="large-1 hide-for-medium-down columns">
      <h3 class="custom-font-big-blue">2.</h3>
    </div>
    <div class="large-11 columns">
      <p class="global-p" style="margin-top:15px;">
        To make sauce, combine vinegar, sweetener, and mustard in small
saucepan. Blend in cornstarch. Cook stirring over medium heat until mixture just boils and gets thick. Take off heat and add juice and onion. Keep warm until ready to serve.
      </p>
    </div>
  </div>
  
  <div class="row">
    <div class="large-1 hide-for-medium-down columns">
      <h3 class="custom-font-big-blue">3.</h3>
    </div>
    <div class="large-11 columns">
      <p class="global-p" style="margin-top:15px;">
        Place salmon on baking sheet. Bake for about 10 minutes or until fish flakes easily. Spoon sauce and paprika over each piece of salmon before serving.
      </p>
    </div>
  </div>

  
  <div class="row" style="margin-top:15px;">
    <div class="large-12 columns">
    <h2 class="custom-font-small">Four Servings:</h2>
    <p class="global-p">
      Calories: 225 | Carbohydrate: 2 grams | Protein: 23 grams | Fat: 12 grams | Cholesterol: 67 milligrams | Sodium:248 milligrams | Fiber:0 grams | Exchanges: 3 medium fat meats
    </p>
    </div>
  </div>
    
  <!-- End Main Content -->
            
</div>

<?php

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script src="../../js/foundation/foundation.abide.js"></script>
    <script src="../../js/foundation/foundation.reveal.js"></script>
    <script src="../../js/foundation/foundation.tooltip.js"></script>
    <script src="../../js/foundation/foundation.offcanvas.js"></script>
    <script src="../../js/foundation/foundation.equalizer.js"></script>
    <script src="../../js/foundation/foundation.dropdown.js"></script>
    <script type="text/javascript" src="../../js/sha512.js"></script>
    <script type="text/javascript" src="../../js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>