<?php
include_once("../../lib/template_api.php");
include_once ("../../lib/back_api.php");
$ss = SESSION::secure_session();
$_GET['logout'] = (filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null) ? filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) : ""; //defining variable 'lougout'

if($_GET['logout']==1){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

<div id="main">

  <!-- Global Header Container -->
  <div id="top nojava">
  
    <!-- Header Image Container -->
    <div class="green-bg" style="margin-top:0px;">
      
      <!-- Page Title Container -->
      <div style="margin-top:40px;">
        <div class="row nojava center">
          <h1 class="custom-font-big-white">an herb lover's spaghetti squash</h1>
          <p class="gotham-small-white">Spaghetti squash is a nutritious substitute for regular pasta. This recipe allows you to use any sauce you would normally use. This recipe also goes well with any meat, fish, or poultry.</p> 
          <br /><br /><br />
              
        </div>
      </div>
      <!-- End Page Title Container --> 
       
    </div>
    <!-- End Header Image Container -->
    
    <!-- Full Width Sub Menu -->
    
    <div class="blue-bg" style="margin-top:0px; padding:10px; padding-bottom:0px;">
      <a href="index.php" class="button round success tiny" style="margin-top:10px;">Back to Recipe Index</a>
    </div>
    
    <!-- End Full Width Sub Menu -->
    
  </div>
  <!-- End Global Header Container -->
  
    
  <!-- Main Content -->
  
  <div class="row" style="margin-top:40px;">
    
    <div class="large-6 medium-6 columns">
      <img src="img/recipes-spaghetti-squash-circle.png" alt="spaghetti squash picture" />
    </div>
    
    <div class="large-3 medium-3 columns">
    
      <h2 class="custom-font-small">You'll Need:</h2>
      <ul class="global-p">
        <li>1 medium spaghetti squash</li>
        <li>2 tablespoons olive oil</li>
        <li>¼ cup chopped onion</li>
        <li>1 tablespoon fresh parsley, chopped</li>
        <li>1 teaspoon dried basil</li>
        <li>¼ teaspoon dried rosemary</li>
        <li>1 clove garlic, minced </li>
        <li>1 tablespoon lemon juice </li>
        <li>Salt and pepper to taste</li>
        <li>3 tablespoons parmesan cheese, grated</li>
      </ul>
    </div>
      
    <div class="large-3 medium-3 columns">
      <h2 class="custom-font-small">Take Out:</h2>
      <ul class="global-p">
        <li>Baking dish</li>
        <li>Food processor</li>
        <li>Grater</li>
        <li>Knife</li>
        <li>Cutting Board</li>
        <li>Spatula</li>
        <li>Fork</li>
        <li>Medium mixing bowl</li>
        <li>Measuring cups</li>
        <li>Measuring spoons</li>
      </ul>
    </div>
      
  </div>
  
  <div class="row" style="margin-top:15px;">
    <div class="large-12 columns">
      <h2 class="custom-font-small">Directions:</h2>
    </div>
  </div>
  
  <div class="row">
    <div class="large-1 hide-for-medium-down columns">
      <h3 class="custom-font-big-blue">1.</h3>
    </div>
    <div class="large-11 columns">
      <p class="global-p" style="margin-top:15px;">
        Cut spaghetti squash in half. Place in baking dish cut side down. Add small amount of water and cover. Cook in microwave on high until fork tender (about 10 minutes). Set aside.
      </p>
    </div>
  </div>
  
  <div class="row">
    <div class="large-1 hide-for-medium-down columns">
      <h3 class="custom-font-big-blue">2.</h3>
    </div>
    <div class="large-11 columns">
      <p class="global-p" style="margin-top:15px;">
        In food processor, blend the onion, garlic, herbs and lemon juice with
the oil. 
      </p>
    </div>
  </div>
  
  <div class="row">
    <div class="large-1 hide-for-medium-down columns">
      <h3 class="custom-font-big-blue">3.</h3>
    </div>
    <div class="large-11 columns">
      <p class="global-p" style="margin-top:15px;">
        With a fork, scrape the squash into the bowl. Season with salt and pepper if desired and add the herb mixture. Toss to coat. Place in shallow baking dish and sprinkle with cheese. 
      </p>
    </div>
  </div>
  
  <div class="row">
    <div class="large-1 hide-for-medium-down columns">
      <h3 class="custom-font-big-blue">4.</h3>
    </div>
    <div class="large-11 columns">
      <p class="global-p" style="margin-top:15px;">
        Microwave for about 3 minutes on high to warm. 
      </p>
    </div>
  </div>
  
  <div class="row" style="margin-top:15px;">
    <div class="large-12 columns">
    <h2 class="custom-font-small">Four Servings:</h2>
    <p class="global-p">
      Calories: 123 | Carbohydrate: 12 grams | Fat: 8 grams | Protein: 3 grams | Sodium (with no added salt): 86 milligrams | Fiber: 2 grams | Cholesterol: 3
milligrams | Exchange: 1 starch, 1 ½ fats
    </p>
    </div>
  </div>
    
  <!-- End Main Content -->
            
</div>

<?php

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script src="../../js/foundation/foundation.abide.js"></script>
    <script src="../../js/foundation/foundation.reveal.js"></script>
    <script src="../../js/foundation/foundation.tooltip.js"></script>
    <script src="../../js/foundation/foundation.offcanvas.js"></script>
    <script src="../../js/foundation/foundation.equalizer.js"></script>
    <script src="../../js/foundation/foundation.dropdown.js"></script>
    <script type="text/javascript" src="../../js/sha512.js"></script>
    <script type="text/javascript" src="../../js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>