<?php
include_once("../../lib/template_api.php");
include_once ("../../lib/back_api.php");
$ss = SESSION::secure_session();

$_GET['logout'] = (filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null) ? filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) : ""; //defining variable 'lougout'
if($_GET['logout']==1){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

<div id="main">

  <!-- Global Header Container -->
  <div id="top nojava">
  
    <!-- Header Image Container -->
    <div class="green-bg" style="margin-top:0px;">
      
      <!-- Page Title Container -->
      <div style="margin-top:40px;">
        <div class="row nojava center">
          <h1 class="custom-font-big-white">potato leek soup</h1>
          <p class="gotham-small-white">This soup is at the top of our guilt-free comfort food list! This dish is perfect the night before a race. Potatoes contain a lot of potassium, vitamins, and carbohydrates, making them an ideal ingredient for athletes. Remember to leave the peels on for flavor and nutrients!</p> 
          <br /><br /><br />
              
        </div>
      </div>
      <!-- End Page Title Container --> 
       
    </div>
    <!-- End Header Image Container -->
    
    <!-- Full Width Sub Menu -->
    
    <div class="blue-bg" style="margin-top:0px; padding:10px; padding-bottom:0px;">
      <a href="index.php" class="button round success tiny" style="margin-top:10px;">Back to Recipe Index</a>
    </div>
    
    <!-- End Full Width Sub Menu -->
    
  </div>
  <!-- End Global Header Container -->
  
    
  <!-- Main Content -->
  
  <div class="row" style="margin-top:40px;">
    
    <div class="large-6 medium-6 columns">
      <img src="img/recipes-potato-soup.png" alt="demonstration of finished potato leak soup" />
      
    </div>
    
    <div class="large-3 medium-3 columns">
    
      <h2 class="custom-font-small">You'll Need:</h2>
      <ul class="global-p">
        <li>1/4 cup olive oil</li>
        <li>5 cups chopped leeks</li>
        <li>2 stalks celery, chopped</li>
        <li>1 large onion, chopped</li>
        <li>4 cups cubed potatoes</li>
        <li>2 quarts chicken stock, low sodium</li>
        <li>2 cups canned skim evaporated milk</li>
        <li>Salt and freshly ground pepper to taste</li>
      </ul>
    </div>
      
    <div class="large-3 medium-3 columns">
      <h2 class="custom-font-small">Take Out:</h2>
      <ul class="global-p">
        <li>Measuring cups and spoons</li> 
        <li>Non-stick Dutch oven</li>
        <li>Knife</li>
        <li>Cutting Board</li>
      </ul>
    </div>
      
  </div>
  
  <div class="row" style="margin-top:15px;">
    <div class="large-12 columns">
      <h2 class="custom-font-small">Directions:</h2>
    </div>
  </div>
  
  <div class="row">
    <div class="large-1 hide-for-medium-down columns">
      <h3 class="custom-font-big-blue">1.</h3>
    </div>
    <div class="large-11 columns">
      <p class="global-p" style="margin-top:15px;">
        Heat the olive oil in a non-stick Dutch oven and add the leeks, celery, and onion. Cook slowly for 10 minutes until golden and soft. Do not let the mixture brown.
      </p>
    </div>
  </div>
  
  <div class="row">
    <div class="large-1 hide-for-medium-down columns">
      <h3 class="custom-font-big-blue">2.</h3>
    </div>
    <div class="large-11 columns">
      <p class="global-p" style="margin-top:15px;">
        Add potatoes and chicken stock; cover, and bring to a boil. Reduce the heat and simmer 20-40 minutes or until potatoes are cooked through.
      </p>
    </div>
  </div>
  
  <div class="row">
    <div class="large-1 hide-for-medium-down columns">
      <h3 class="custom-font-big-blue">3.</h3>
    </div>
    <div class="large-11 columns">
      <p class="global-p" style="margin-top:15px;">
       Mash the vegetables. Heat the milk and add to the soup. Salt and pepper to taste.
      </p>
    </div>
  </div>
  
  <div class="row" style="margin-top:15px;">
    <div class="large-12 columns">
    <h2 class="custom-font-small">Six Servings:</h2>
    <p class="global-p">
      Calories: 314 | Carbohydrate: 45 grams | Fat: 10 grams | Protein: 14 grams | Sodium: 635 milligram with low sodium broth, 869 milligrams with reduced sodium broth | Fiber: 3 grams | Cholesterol: 3 milligrams | Exchanges: 3 starches, 1 lean meat 
    </p>
    </div>
  </div>
    
  <!-- End Main Content -->
            
</div>

<?php

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script src="../../js/foundation/foundation.abide.js"></script>
    <script src="../../js/foundation/foundation.reveal.js"></script>
    <script src="../../js/foundation/foundation.tooltip.js"></script>
    <script src="../../js/foundation/foundation.offcanvas.js"></script>
    <script src="../../js/foundation/foundation.equalizer.js"></script>
    <script src="../../js/foundation/foundation.dropdown.js"></script>
    <script type="text/javascript" src="../../js/sha512.js"></script>
    <script type="text/javascript" src="../../js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>