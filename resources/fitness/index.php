<?php
include_once("../../lib/template_api.php");
include_once("../../lib/back_api.php");
include_once("../../lib/groups_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
SESSION::logout();
REDIRECT::home();
}
HTML_ELEMENT::head('Groups');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

<div id="main">

  <!-- Global Header Container -->
  <div id="top nojava">
  
    <!-- Header Image Container -->
    <div id="slideshow">
      
      <!-- Page Title Container -->
      <div id="slideshow-title">
        <div class="row nojava center">
          <h1 class="custom-font-big-white">fitness tips</h1>
          <p class="gotham-small-white">We've created some very helpful videos with fitness professionals and we've organized them based on skill level.  Check them out below!</p>
          <br /><br /><br /> 
              
        </div>
      </div>
      <!-- End Page Title Container --> 
       
    </div>
    <!-- End Header Image Container -->
    
  </div>
  <!-- End Global Header Container -->
  
  <!-- Full Width Sub Menu -->
    
    <div class="blue-bg" style="margin-top:0px; padding:10px; padding-bottom:0px;">
      <a href="../index.php" class="button round success tiny" style="margin-top:10px;">Back to Resources Index</a>
    </div>
    
    <!-- End Full Width Sub Menu -->
  
    
  <!-- Main Content -->
  
  <!-- Beginner Level section -->
  <div class="row" style="margin-top:20px;">
    
    <div class="small-4 large-3 columns">
      <img src="img/vivian.png" alt="personal trainer, vivian" />
    </div>
    
    <div class="small-8 large-9 columns">
      <h2 class="custom-font-small">Beginner Fitness Tips:</h2>
      
      <p class="global-p">There are exercises designed for people who are new to fitness.  No previous experience is required.  These videos can help you get started.</p>
      
      <ul style="list-style:none">
        <li><a href="#" data-reveal-id="beginner-part1" class="button tiny round">Watch Beginner Part 1</a></li>
        <li><a href="#" data-reveal-id="beginner-part2" class="button tiny round">Watch Beginner Part 2</a></li>
        <li><a href="#" data-reveal-id="beginner-part3" class="button tiny round">Watch Beginner Part 3</a></li>
      </ul>
      
    </div>
    
  </div>
  
    <!-- Beginner Video Modals -->
    
      <!-- Beginner Part 1 Modal -->
      <div id="beginner-part1" class="reveal-modal" data-reveal>
        <div class="row">
          <div class="large-12 columns">
            <h2 class="custom-font-small-blue">Beginner - Part 1</h2>
            <hr style="margin-top:-5px;" />
            
            <div class="flex-video">
              <iframe width="560" height="315" src="//www.youtube.com/embed/ITlr23aeCH4" frameborder="0" allowfullscreen></iframe>
            </div>
            
          </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
      </div>
      <!-- End Beginner Part 1 Modal -->
      
      <!-- Beginner Part 2 Modal -->
      <div id="beginner-part2" class="reveal-modal" data-reveal>
        <div class="row">
          <div class="large-12 columns">
            <h2 class="custom-font-small-blue">Beginner - Part 2</h2>
            <hr style="margin-top:-5px;" />
            
            <div class="flex-video">
              <iframe width="560" height="315" src="//www.youtube.com/embed/BrCvtUIcI04" frameborder="0" allowfullscreen></iframe>
            </div>
            
          </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
      </div>
      <!-- End Beginner Part 2 Modal -->
      
      <!-- Beginner Part 3 Modal -->
      <div id="beginner-part3" class="reveal-modal" data-reveal>
        <div class="row">
          <div class="large-12 columns">
            <h2 class="custom-font-small-blue">Beginner - Part 3</h2>
            <hr style="margin-top:-5px;" />
            
            <div class="flex-video">
              <iframe width="560" height="315" src="//www.youtube.com/embed/f8-DW7_esuE" frameborder="0" allowfullscreen></iframe>
            </div>
            
          </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
      </div>
      <!-- End Beginner Part 3 Modal -->
      
      <!-- Beginner Part 4 Modal -->
      <div id="beginner-part4" class="reveal-modal" data-reveal>
        <div class="row">
          <div class="large-12 columns">
            <h2 class="custom-font-small-blue">Beginner - Part 4</h2>
            <hr style="margin-top:-5px;" />
            
            <div class="flex-video">
              <iframe width="560" height="315" src="//www.youtube.com/embed/f8-DW7_esuE" frameborder="0" allowfullscreen></iframe>
            </div>
            
          </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
      </div>
      <!-- End Beginner Part 4 Modal -->
    
    <!-- End Beginner Video Modals -->
  
  <!-- End Beginner Level Section -->
  
  <!-- Intermediate Level section -->
  <div class="row" style="margin-top:20px;">
    
    <div class="small-4 large-3 columns">
      <img src="img/bill.png" alt="personal trainer, bill" />
    </div>
    
    <div class="small-8 large-9 columns">
      <h2 class="custom-font-small">Intermediate Fitness Tips:</h2>
      
      <p class="global-p">These tips are for people who have experience exercising, but still might need some tips and help along the way.</p>
      
      <ul style="list-style:none">
        <li><a href="#" data-reveal-id="intermediate-part1" class="button tiny round">Watch Intermediate Part 1</a></li>
        <li><a href="#" data-reveal-id="intermediate-part2" class="button tiny round">Watch Intermediate Part 2</a></li>
        <li><a href="#" data-reveal-id="intermediate-part3" class="button tiny round">Watch Intermediate Part 3</a></li>
      </ul>
      
    </div>
    
  </div>
  
    <!-- Beginner Video Modals -->
    
      <!-- Intermediate Part 1 Modal -->
      <div id="intermediate-part1" class="reveal-modal" data-reveal>
        <div class="row">
          <div class="large-12 columns">
            <h2 class="custom-font-small-blue">Intermediate - Part 1</h2>
            <hr style="margin-top:-5px;" />
            
            <div class="flex-video">
              <iframe width="560" height="315" src="//www.youtube.com/embed/WDC7DKuqoWE" frameborder="0" allowfullscreen></iframe>
            </div>
            
          </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
      </div>
      <!-- End Intermediate Part 1 Modal -->
      
      <!-- Intermediate Part 2 Modal -->
      <div id="intermediate-part2" class="reveal-modal" data-reveal>
        <div class="row">
          <div class="large-12 columns">
            <h2 class="custom-font-small-blue">Intermediate - Part 2</h2>
            <hr style="margin-top:-5px;" />
            
            <div class="flex-video">
              <iframe width="560" height="315" src="//www.youtube.com/embed/b6jMJoa6-w8" frameborder="0" allowfullscreen></iframe>
            </div>
            
          </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
      </div>
      <!-- End Intermediate Part 2 Modal -->
      
      <!-- Intermediate Part 3 Modal -->
      <div id="intermediate-part3" class="reveal-modal" data-reveal>
        <div class="row">
          <div class="large-12 columns">
            <h2 class="custom-font-small-blue">Intermediate - Part 3</h2>
            <hr style="margin-top:-5px;" />
            
            <div class="flex-video">
              <iframe width="560" height="315" src="//www.youtube.com/embed/HOavpWTrGQ0" frameborder="0" allowfullscreen></iframe>
            </div>
            
          </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
      </div>
      <!-- End Intermediate Part 3 Modal -->
    
    <!-- End Intermediate Video Modals -->
  
  <!-- End Intermediate Level Section -->
  
  <!-- Advanced Level section -->
  <div class="row" style="margin-top:20px;">
    
    <div class="small-4 large-3 columns">
      <img src="img/leah.png" alt="personal trainer, leah" />
    </div>
    
    <div class="small-8 large-9 columns">
      <h2 class="custom-font-small">Advanced Fitness Tips:</h2>
      
      <p class="global-p">These tips are for people who are experienced with personal fitness and might just be looking for some new exercises to spice up your routine.</p>
      
      <ul style="list-style:none">
        <li><a href="#" data-reveal-id="advanced-part1" class="button tiny round">Watch Intermediate Part 1</a></li>
        <li><a href="#" data-reveal-id="advanced-part2" class="button tiny round">Watch Intermediate Part 2</a></li>
        <li><a href="#" data-reveal-id="advanced-part3" class="button tiny round">Watch Intermediate Part 3</a></li>
        <li><a href="#" data-reveal-id="advanced-part4" class="button tiny round">Watch Intermediate Part 4</a></li>
      </ul>
      
    </div>
    
  </div>
  
    <!-- Advanced Video Modals -->
    
      <!-- Advanced Part 1 Modal -->
      <div id="advanced-part1" class="reveal-modal" data-reveal>
        <div class="row">
          <div class="large-12 columns">
            <h2 class="custom-font-small-blue">Advanced - Part 1</h2>
            <hr style="margin-top:-5px;" />
            
            <div class="flex-video">
              <iframe width="560" height="315" src="//www.youtube.com/embed/dn1B-7ZrgrM" frameborder="0" allowfullscreen></iframe>
            </div>
            
          </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
      </div>
      <!-- End Advanced Part 1 Modal -->
      
      <!-- Advanced Part 2 Modal -->
      <div id="advanced-part2" class="reveal-modal" data-reveal>
        <div class="row">
          <div class="large-12 columns">
            <h2 class="custom-font-small-blue">Advanced - Part 2</h2>
            <hr style="margin-top:-5px;" />
            
            <div class="flex-video">
              <iframe width="560" height="315" src="//www.youtube.com/embed/goKMPtXEjR8" frameborder="0" allowfullscreen></iframe>
            </div>
            
          </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
      </div>
      <!-- End Advanced Part 2 Modal -->
      
      <!-- Advanced Part 3 Modal -->
      <div id="advanced-part3" class="reveal-modal" data-reveal>
        <div class="row">
          <div class="large-12 columns">
            <h2 class="custom-font-small-blue">Advanced - Part 3</h2>
            <hr style="margin-top:-5px;" />
            
            <div class="flex-video">
              <iframe width="560" height="315" src="//www.youtube.com/embed/SEVM3_00wkE" frameborder="0" allowfullscreen></iframe>
            </div>
            
          </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
      </div>
      <!-- End Advanced Part 3 Modal -->
      
      <!-- Advanced Part 4 Modal -->
      <div id="advanced-part4" class="reveal-modal" data-reveal>
        <div class="row">
          <div class="large-12 columns">
            <h2 class="custom-font-small-blue">Advanced - Part 4</h2>
            <hr style="margin-top:-5px;" />
            
            <div class="flex-video">
              <iframe width="560" height="315" src="//www.youtube.com/embed/MxVlxnhWhfc" frameborder="0" allowfullscreen></iframe>
            </div>
            
          </div>
        </div>
        <a class="close-reveal-modal">&#215;</a>
      </div>
      <!-- End Advanced Part 4 Modal -->
    
    <!-- End Advanced Video Modals -->
  
  <!-- End Advanced Level Section -->
    
  <!-- End Main Content -->
            
  </div>

<?php

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script src="../../js/foundation/foundation.abide.js"></script>
    <script src="../../js/foundation/foundation.reveal.js"></script>
    <script src="../../js/foundation/foundation.tooltip.js"></script>
    <script src="../../js/foundation/foundation.offcanvas.js"></script>
    <script src="../../js/foundation/foundation.equalizer.js"></script>
    <script src="../../js/foundation/foundation.dropdown.js"></script>
    <script src="../../js/foundation/foundation.topbar.js"></script>
    <script type="text/javascript" src="../../js/sha512.js"></script>
    <script type="text/javascript" src="../../js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>