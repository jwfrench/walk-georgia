<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

<div id="main">

  <!-- Global Header Container -->
  <div id="top nojava">
  
    <!-- Header Image Container -->
    <div id="slideshow">
      
      <!-- Page Title Container -->
      <div id="slideshow-title">
        <div class="row nojava center">
          <h2 class="custom-font-big-white">resources</h2> 
          <br /><br /><br />
      
          <!-- Header Buttons -->   
          <div class="large-12 columns expand">
            <div class="row center nojava">
              <div class="small-12 medium-4 large-4 columns">
                <a href="recipes/index.php" class="button small expand round">Recipes</a>
              </div>
              <div class="small-12 medium-4 large-4 columns">
                <a href="fitness/index.php" class="button small success expand round">Fitness Tips</a>
              </div>
              <div class="small-12 medium-4 large-4 columns">
                <a href="/lesson-plans.php" class="button small expand round secondary">Lesson Plans</a>
              </div>
            </div>
          </div>
          <!-- End Header Buttons -->
              
        </div>
      </div>
      <!-- End Page Title Container --> 
       
    </div>
    <!-- End Header Image Container -->
    
  </div>
  <!-- End Global Header Container -->
  
    
  <!-- Main Content -->
  
  <!-- Blog Section -->
  <div class="row" style="margin-top:20px;">
    
    <div class="large-3 medium-3 hide-for-small columns">
      <img src="../img/resources-blog-logo.png" />
    </div>
    
    <div class="large-9 medium-9 columns">
      <h2 class="custom-font-small">Our Blog - Notes from the Road:</h2>
      <p class="global-p">
      Our blog is one of the best ways to keep up-to-date with all of our most recent resources as well as all of our new development with the site.  The blog is tied to our newsletter, which is sent weekly to keep you current.
      </p>
      <a href="http://blog.extension.uga.edu/walkgeorgia/" target="_blank" class="button round success">View the Blog</a>
    </div>
    
  </div>
  <!-- End Blog Section -->
  
  <!-- Program Eval Section -->
  <div class="row" style="margin-top:20px;">
    
    <div class="large-3 medium-3 hide-for-small columns">
      &nbsp;
    </div>
    
    <div class="large-9 medium-9 columns">
      <h2 class="custom-font-small">Project Evaluation Report:</h2>
      <p class="global-p">
	In 2013, UGA Extension received a three-year, $1 million
	grant from The Coca-Cola Foundation with the
	goal of helping Georgians be more physically
	active. This report provides a summary of the
	impact and success of Walk Georgia as a result of
	our innovative public-private partnership.
      </p>
      <a href="http://walkgeorgia.org/Walk-Georgia-Evaluation-Report-2017.pdf" class="button round secondary">Project Evaluation Report</a>
    </div>
    
  </div>
  <!-- End Recipes Section -->
  
  <!-- Recipes Section -->
  <div class="row" style="margin-top:20px;">
    
    <div class="large-3 medium-3 hide-for-small columns">
      <img src="../img/resources-recipes-logo.png" />
    </div>
    
    <div class="large-9 medium-9 columns">
      <h2 class="custom-font-small">Recipes Archive:</h2>
      <p class="global-p">
      While exercise can be an effective means of change and improvement, it can sometimes not be enough on its own.  A healthy diet will greatly increase not only your results, but also your quality of life.  We've compiled an archive of easy, healthy, and local recipies for you to try.  There is truly something for everyone in the archive.
      </p>
      <a href="http://blog.extension.uga.edu/walkgeorgia/category/recipes" class="button round peachButton">View the Recipes</a>
    </div>
    
  </div>
  <!-- End Recipes Section -->
  
  <!-- Fitness Tips Section -->
  <div class="row" style="margin-top:20px;">
    
    <div class="large-3 medium-3 hide-for-small columns">
      <img src="../img/resources-fitness-logo.png" />
    </div>
    
    <div class="large-9 medium-9 columns">
      <h2 class="custom-font-small">Fitness Tips:</h2>
      <p class="global-p">
      We all lead busy lives.  It can be so hard to find time to exercise, and even if you can, it can be daunting to even decide what to do.  We've created some very helpful videos with fitness professionals and we've organized them based on skill level.  We have tips for beginners and pros alike.  If you are trying to find new exercises to get started, check out our videos on our Fitness Tips page.
      </p>
      <a href="fitness/index.php"  class="button round">View Our Fitness Tips</a>
    </div>
    
  </div>
  <!-- End Fitness Tips Section -->
  
  <!-- County Resources Section -->
  <div class="row" style="margin-top:20px;">
    
    <div class="large-3 medium-3 hide-for-small columns">
      <img src="../img/resources-county-logo.png" />
    </div>
    
    <div class="large-9 medium-9 columns">
      <h2 class="custom-font-small">County Resources:</h2>
      <p class="global-p">
      One of the things that sets Walk Georgia apart from other wellness programs is that we connect to you locally.  Dozens of counties all over the state have unique events and resources for you to participate in.  To find out who to contact in your area, visit our County Resources page.
      </p>
      <a href="county-resources.php" target="_blank" class="button secondary round">View the County Resources</a>
    </div>
    
  </div>
  <!-- End County Resources Section -->
  
  <!-- Lesson Plans Section -->
  <div class="row" style="margin-top:20px;">
    
    <div class="large-3 medium-3 hide-for-small columns">
      <img src="../img/resources-lessons-logo.png" />
    </div>
    
    <div class="large-9 medium-9 columns">
      <h2 class="custom-font-small">Lesson Plans:</h2>
      <p class="global-p">
      We strive very hard to provide resources for educators.  We believe that a healthy lifesyle starts early by enabling younger people to have the resources they need.  We've developed lesson plans to try to help this process and give people the information they need to move more and live more.
      </p>
      <a href="/lesson-plans.php" target="_blank" class="button success round">View the Lesson Plans</a>
    </div>
    
  </div>
  <!-- End Lesson Plans Section -->    
    
  <!-- End Main Content -->
            
  </div>
<!-- Begin Footer -->
<?php

//STOPEDITING

$footer = HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
<!-- End Footer -->
  </body>
</html>