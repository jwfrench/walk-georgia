<?php
include_once("lib/template_api.php");
include_once ("lib/back_api.php");
$ss = SESSION::secure_session();
if (isset($_GET['logout'])) {
    SESSION::logout('');
    REDIRECT::home('');
}
if (isset($_POST['file'])) {
    $img = ACCOUNT::avatar_upload($_SESSION['ID']);
}
if (!isset($_SESSION['valid'])) {
    REDIRECT::login('You must log in to view this content');
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();
//If the user is logged in
if (isset($_SESSION['valid'])) {
    $ID = $_SESSION['ID'];
    $UUID = $_SESSION['UUID'];
    $info = ACCOUNT::get_info($ID);
//EDIT HERE
//gather the info you need for page
    $count_query = MSSQL::query("SELECT COUNT(G_STARTDATE) as COUNT FROM GOALS WHERE G_UUID ='$UUID' AND G_COMPLETED IS NULL AND  G_STARTDATE >= CAST(DATEADD(dd,-7,GETDATE()) AS DATE)");
    $sentence = odbc_result(MSSQL::query("SELECT G_SENTENCE FROM GOALS WHERE G_UUID ='$UUID' AND G_COMPLETED IS NULL AND  G_STARTDATE >= CAST(DATEADD(dd,-7,GETDATE()) AS DATE)"), 'G_SENTENCE');
    $goalCount = odbc_result($count_query, 'COUNT');
    if ($goalCount == 1) {
        $pointQuery = MSSQL::query("SELECT * FROM GOALS WHERE G_UUID ='$UUID' AND G_COMPLETED IS NULL AND  G_STARTDATE >= CAST(DATEADD(dd,-7,GETDATE()) AS DATE)");
        $points_goal = odbc_result($pointQuery, 'G_POINT_GOAL');
        $current_points = odbc_result($pointQuery, 'G_EARNED');
        $startdate = odbc_result($pointQuery, 'G_STARTDATE');
        $enddate = odbc_result($pointQuery, 'G_ENDDATE');
    } else {
        $points_goal = 1;
        $current_points = 1;
    }
    $percent_complete = number_format(($current_points / $points_goal) * 100, 0, ".", ",");
    $today = date("F j, Y");
    $tomorrow = date('F j, Y', strtotime("+1 day", strtotime($today)));
    $nextWeek = date('F j, Y', strtotime("+7 day", strtotime($today)));
    $dayname = date('l');
    ?>


    <!--Goal Modal-->
    <div id="goalModal" class="reveal-modal" style="top: 0" data-reveal>
        <form id="modal-form" action="create_goal.php" method="post" data-abide>
            <div class="row">
                <div class="large-6 columns" >
                    <h2 class="custom-font-small-blue">Create a goal!</h2>
                </div>
                <div class="large-6 columns" >
                    <p class="font -standard -primary">
                        For guidance on how to define your goals, click <a target="_blank" href="http://blog.extension.uga.edu/walkgeorgia/extra-extra-my-goals-live-on-walkgeorgia-org/">here</a>.
                    </p>
                </div>
                <hr />

            </div>
            <!-- MAIN ROW -->
            <!-- GOAL START DATE -->
            <div class="row">
                <div class="large-12 medium-12 columns tc-ns">
                    <h3 class="global-h2">Goal Start Date:</h3>
                    <p class="mb font -standard -primary">Goal for the Week of <br><span class="startDate"><?= $today; ?></span> - <span class="endDate"><?= $nextWeek; ?></span></p>
                </div>
            </div>
            <div class="row collapse">
                <div class="medium-6 small-6 columns">
                    <div id="ck-button" style="float:right">
                        <label>
                            <input type="radio" id="start" name="start" value="<?= date("Y-m-d") ?>" checked="checked" onclick="dateRange(this.value)"><span class="grade font -extra-small -primary" style="width: 100%" >Start <br>Today</span>
                        </label>
                    </div>
                </div>
                <div class="medium-6 small-6 columns">
                    <div id="ck-button">
                        <label>
                            <input type="radio" id="start" name="start" value="<?= date('Y-m-d', strtotime("+1 day", strtotime($today))) ?>" onclick="dateRange(this.value)"><span class="grade font -extra-small -primary" style="width: 100%">Start Tomorrow</span>
                        </label>
                    </div>
                    <input type="hidden" name="end" id="end" class="end" value="<?= date('Y-m-d', strtotime("+7 day", strtotime($today))) ?>"/>
                </div>
            </div>
            <!-- MEDIUM -->
            <div class="pt1">
                <div class="row">
                    <div class="large-12 medium-12 columns">
                        <table class="center-m">
                            <thead>
                                <tr>
                                    <th width="200">Primary Activities</th>
                                    <th width="150">Times/ Week</th>
                                    <th width="150">Minutes/ Session</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <label>
                                            <select id="exercise" onchange="primaryActivityChange(this)" class="exercise" required>
                                                <?php
                                                $aid = odbc_result(MSSQL::query('SELECT TOP 1 AL_AID FROM LOG WHERE AL_UID =\'' . $_SESSION['ID'] . '\' ORDER BY AL_DATE DESC'), 'AL_AID');
                                                $list = ACTIVITY::select_first_activity($aid);
                                                ?>
                                            </select>
                                        </label>
                                    </td>
                                    <td>
                                        <select type="select" id="frequency" name="frequency" class="frequency" onchange="pointsChange()" required>
                                            <option disabled selected></option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                        </select>
                                    </td>
                                    <td>
                                      <select type="select" id="minutes" name="minutes" class="minutes" onchange="pointsChange()" required>
                                          <option disabled selected></option>
                                          <option value="10">10</option>
                                          <option value="20">20</option>
                                          <option value="30">30</option>
                                          <option value="40">40</option>
                                          <option value="50">50</option>
                                          <option value="60">60</option>
                                          <option value="70">70</option>
                                          <option value="80">80</option>
                                          <option value="90">90</option>
                                      </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>
                                            <select id="exercise2" name="exercise2" class="exercise2" onchange="secondaryActivityChange(this)">
                                                <?php
                                                $list = ACTIVITY::select_second_activity();
                                                ?>
                                            </select>
                                        </label>
                                    </td>
                                    <td>
                                        <select type="select" id="frequency2" name="frequency2" class="frequency2" onchange="pointsChange()">
                                            <option disabled selected></option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                        </select>
                                    </td>
                                    <td>
                                      <select type="select" id="minutes2" name="minutes2" class="minutes2" onchange="pointsChange()">
                                          <option disabled selected></option>
                                          <option value="10">10</option>
                                          <option value="20">20</option>
                                          <option value="30">30</option>
                                          <option value="40">40</option>
                                          <option value="50">50</option>
                                          <option value="60">60</option>
                                          <option value="70">70</option>
                                          <option value="80">80</option>
                                          <option value="90">90</option>
                                      </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END MEDIUM -->


            <!-- PERSONAL PLAN -->
            <div class="row">
                <div class="large-2 columns show-for-large-up">
                    <!-- Avatar -->
                    <?php ACCOUNT::avatar_display_only($_SESSION['ID']); ?>
                    <!-- End Avatar -->
                </div>
                <div class="large-10 medium-12 columns">
                    <!--  <h2 class="font -blue -small -uppercase -bold">Personal Plan</h2>-->
                    <h3 class="global-h2">My Goal is to earn <span class="font -small -underline pointsTotal">__</span> points between now and <span class="endDate"><?= $nextWeek; ?></span>! </h3>
                    <p class="font -standard -primary -black" id="sentence">
                        I plan on exercising <span class="font -small  -underline -secondary sessionsPerGoal">_</span> times this week for a total of <span class="font -small  -secondary -underline goalTime">__</span> minutes. Move more and live more by gaining <span class="font -small -secondary -underline pointsTotal">__</span> points through <span class="allActivities"><span class="font -secondary primaryActivity">____</span><span class="font -primary activityAnd"></span><span class="font -secondary secondaryActivity"></span></span>!
                    </p>
                    <input type="hidden" name="activity" id="activity" class="finalActivity" value=""/>
                    <input type="hidden" name="totalFrequency" id="totalFrequency" class="totalFrequency" value=""/>
                    <input type="hidden" name="sentenceInput" id="sentenceInput" class="sentenceInput" value=""/>
                    <input type="hidden" name="points" id="points" class="points" value=""/>
                    <input type="hidden" name="goalCount" id="goalCount" class="goalCount" value="<?= $goalCount ?>"/>
                </div>
            </div>
            <div class="row">
                <div class="large-10 small-12 large-offset-2 columns show-for-small-only">
                    <div data-alert class="alert-box secondary surgeonGeneral" style="display: none;">
                        That's a good start! The Surgeon General recommends engaging in at least 150 minutes of moderate-intensity activity each week.
                    </div>
                </div>
            </div>
            <div class="row show-for-medium-up">
                <div class="large-10 small-12 large-offset-2 columns">
                    <div data-alert class="alert-box secondary">
                        That's a good start! The Surgeon General recommends engaging in at least 150 minutes of moderate-intensity activity each week.
                    </div>
                </div>
            </div>
            <!-- END PERSONAL PLAN -->
            <!-- CONFIDENCE LEVEL -->
            <div class="row">
                <div class="medium-12 columns">
                    <h3 class="global-h2">Confidence Level:</h3>
                    <label class="font -standard -primary -black">On a scale from 1 to 10, how confident do you feel about achieving your goal?
                        <select name="sd" id="sd" onchange="difficulty(this.value)" required>
                            <option disabled selected></option>
                            <option value="1">1 - Not confident at all</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10 - Totally confident</option>
                        </select>
                    </label>
                    <p class="font -primary">A goal cannot be revised for 7 days, or until the goal is accomplished.</p>
                    <div id="ck-button" class="confirm" style="float:left">
                        <label>
                            <input type="button" id="confirm" name="confirm" onclick="confirmButton()"><span class="grade font -extra-small -primary" style="width: 100%; padding: .25rem 1rem; color: white; background-color: #008cba" >Confirm Goal?</span>
                        </label>
                    </div>
                    <input type="submit"  class="small button success submitReady" value="Set my Goal!" style="display: none;"/>
                </div>
            </div>
            <!-- Confirm Modal -->
            <div id="confirmModal" class="reveal-modal small" data-reveal>
                <a href="#" class="button success small" data-reveal-id="confirmModal">Set weekly goal!</a>
                <a href="#" class="button alert small" data-reveal-id="goalModal">Whoops, take me back to change some things!</a>
                <p class="font -standard -primary -black" id="sentence">
                        I plan on exercising <span class="font -small  -underline -secondary sessionsPerGoal">_</span> times this week for a total of <span class="font -small  -secondary -underline goalTime">__</span> minutes. Move more and live more by gaining <span class="font -small -secondary -underline pointsTotal">__</span> points through <span class="font -secondary primaryActivity">____</span><span class="font -primary activityAnd"></span><span class="font -secondary secondaryActivity"></span>!
                    </p>
            </div>
            <!-- End Confirm Modal-->
        </form>
        <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    </div>
    <!-- /GOAL MODAL -->




    <!-- Page Header -->
    <section class="header bg_map bg--peach" style="margin-top: -10px;">
        <div class="row">
            <div class="large-12 columns">
                <h1 class="font -secondary -big -white">My Goals</h1>
                <hr>
                <p class="font -standard -primary -white show-for-medium-up">Create a fitness goal you can stick to with the help of Walk Georgia. Choose goals that push you to move more and live more. For guidance on how to define your goals, click <a target="_blank" class="font -white" href="http://www.cdc.gov/physicalactivity/growingstronger/motivation/define.html">here</a>.</p>
            </div>
        </div>
        <div class="row">
            <div class="large-6 columns">
                <a class="font -primary -white" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/faq.php#G5" target="_blank">Need Help?</a>
            </div>
        </div>
    </section>
    <!-- End Page Header -->

    <!-- Start Main Content -->
    <?php
    if($goalCount == 1){
        $startReadable = date('m-d-Y', strtotime($startdate));
        $endReadable = date('m-d-Y', strtotime($enddate));
        $currentDateRange = "$startReadable to $endReadable";
    }else{
        $currentDateRange = "Current Goal";
    }
    ?>
    <div class="pt2 bs row">
        <div class="medium-6 columns">
            <h3 class="font -secondary">My Weekly Goal</h3>
            <?php if ($goalCount <= 0) { ?>
                <a href="#" class="button round success small" data-reveal-id="goalModal">Set weekly goal!</a>
            <?php } else { ?>
                <!-- Current Goal -->
                <p class="font -secondary -small" style="margin-bottom: 5px;"><?= $currentDateRange; ?>:</p>
                <div class="row">
                    <div class="large-4 columns show-for-medium-up">
                  <?php ACCOUNT::avatar_goals($ID); ?>
                  </div>
                  <div class="large-8 columns">
                  <p class="font -primary -small">  <?= $sentence; ?></p>
                  </div>

              </div>
                <p class="font -secondary -small" style="margin-bottom: 5px;">Goal Progress: <?= $current_points; ?> / <?= $points_goal; ?> Points</p>
                <div class="progress tiny success round">
                    <?php if ($percent_complete > 100){$span = 100;}else{$span = $percent_complete;} ?>
                    <span class="meter font -white" style="width: <?= $span ?>%;"><?= $percent_complete; ?>%</span>
                </div>
                </p>
                <!-- End Current Goal -->
            <?php }
            $sql = "SELECT COUNT(G_STARTDATE) as COUNT, SUM(CASE WHEN G_COMPLETED IS NULL THEN 0 ELSE 1 END) AS BADGES FROM GOALS WHERE G_UUID ='$UUID' AND (G_STARTDATE <= CAST(DATEADD(dd,-7,GETDATE()) AS DATE) OR G_COMPLETED IS NOT NULL)";
            $goals = MSSQL::query($sql);
            ?>
        </div>
        <!-- Completed Goals -->
        <div class="medium-6 columns">
            <h3 class="font -secondary">Past Goals</h3>
            <?php

            if (odbc_result($goals, 'COUNT') >= 1) {
                $sql = "SELECT TOP 5 * FROM GOALS WHERE (G_UUID ='$UUID' AND  G_STARTDATE <= CAST(DATEADD(dd,-7,GETDATE()) AS DATE)) OR (G_UUID ='$UUID' AND G_COMPLETED IS NOT NULL) ORDER BY G_SETDATE DESC";
                $rows = array();
                $goals = MSSQL::query($sql);
                while ($goalArray = odbc_fetch_array($goals)) {
                    $rows[] = $goalArray;
                }
                foreach ($rows as $goal) {
                    $pointsEarned = $goal['G_EARNED'];
                    if(empty($pointsEarned) || $pointsEarned == ''){
                        $pointsEarned = 0;
                    }
                    ?>
                    <div data-alert="" class="alert-box secondary">
                        <b><?= $goal['G_STARTDATE']; ?></b> -  You earned <?= $pointsEarned; ?>/<?= $goal['G_POINT_GOAL']; ?> points.
                        <?php
                        if ($pointsEarned >= $goal['G_POINT_GOAL']) {
                            ?> <i class="fi-check font -green -small"></i> <?php }
                        ?>
                    </div>
                    <?php
                }
                ?>
            <?php } else { ?>
                <p>You currently have no past goals.</p>
            <?php } ?>
        </div>
        <!-- End Completed Goal -->
    </div>
    </div>
    <!-- End Main Content -->




    <?php
}
//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
<script>
//done
    var $startDate = $(".startDate");
    var $endDate = $(".endDate");
    var $endValue = $(".end");
    var $primaryActivity = $(".primaryActivity");
    var $secondaryActivity = $(".secondaryActivity");
    var $activityAnd = $(".activityAnd");
    var $minutes = $(".minutes");
    var $minutes2 = $(".minutes2");
    var $frequency = $(".frequency");
    var $frequency2 = $(".frequency2");
    var $goalTime = $(".goalTime");
    var $goodGoal = $(".goodGoal");
    var $fairGoal = $(".fairGoal");
    var $hardGoal = $(".hardGoal");
    var $recommend = $("#recommend");
    var $sessionsPerGoal = $(".sessionsPerGoal");
//working

    var $pointsTotal = $(".pointsTotal");
    var $pointsInput = $(".points");
    $activityAnd.hide();
//changes the date range of the goal calculator and updates inputs
    function dateRange(date) {

        //create a date object from argument
        var dateFormat = new Date(date);
        //format the value
        var thisDate = new Date(dateFormat.setDate(dateFormat.getDate() + 1));
        var dateFormat = $.datepicker.formatDate('MM d, yy', thisDate);
        //set start date to value
        $startDate.html(dateFormat);

        //create a date object from argument
        var dateFormat = new Date(date);
        //create a date object from argument
        var dateValue = $.datepicker.formatDate('yy-mm-dd', new Date(dateFormat.setDate(dateFormat.getDate() + 8)));
        //format the value
        var dateFormat = $.datepicker.formatDate('MM d, yy', new Date(dateFormat.setDate(dateFormat.getDate())));
        //set the end date to the result

        $endDate.html(dateFormat);
        $endValue.val(dateValue);
    }

    function primaryActivityChange(activity) {
        $primaryActivity.html($(activity).find("option:selected").text().toLowerCase());
        pointsChange();
    }

    function secondaryActivityChange(activity) {
        $activityAnd.html(' and ');
        $activityAnd.show();
        $minutes2.prop('required', true);
        $frequency2.prop('required', true);
        $secondaryActivity.html($(activity).find("option:selected").text().toLowerCase());
        pointsChange();
    }

    function pointsChange() {
        //gather total time
        var minutesArray = $minutes;
        var value1 = minutesArray[0].value;
        var minutesArray = $minutes2;
        var value2 = minutesArray[0].value;
        if (value2 == '') {
            value2 = 0;
        }
        //gather frequency of exercise
        var frequencyArray = $frequency.find("option:selected");
        var freq1 = frequencyArray[0].innerHTML;
        var frequencyArray = $frequency2.find("option:selected");
        var freq2 = frequencyArray[0].innerHTML;
        if (freq2 == '') {
            freq2 = 0;
        }

        //total sessions
        var frequencyNew = parseInt(freq1) + parseInt(freq2);

        //total time
        var totalTime = parseInt(value1) * parseInt(freq1) + parseInt(value2) * parseInt(freq2);

        //gather met data
        var exerciseArray = $(".exercise").find("option:selected");
        var mets = exerciseArray[0].value;

        //gather point totals for first activity : floor((($time *$mets) + ($sd*100))/100);
        console.log(mets);
        console.log(value1*freq1);
        var points = (mets * value1 * freq1 * 60)/100;

        //gather secondary activity met data
        var exerciseArray = $(".exercise2").find("option:selected");
        var mets2 = exerciseArray[0].value;
        if (mets2 != 'Optional second Activity') {

            //gather point totals for second activity
            points = points + (mets2 * value2 * freq2);
        }
        points = points.toFixed(0);
        $sessionsPerGoal.html(frequencyNew);
        $('.totalFrequency').val(frequencyNew);
        $goalTime.html(totalTime);
        $pointsTotal.html(points);
        $pointsInput.val(points);
        console.log(points);
        var sentence = $("#sentence").text();
        $(".sentenceInput").val(sentence);
        if (totalTime < 150) {
            if (totalTime != '0') {
                $(".surgeonGeneral").show();
            }
        } else {
            $(".surgeonGeneral").hide();
        }

        $('.finalActivity').val($('.allActivities').text().toLowerCase());

    }

    function confirmButton() {
        $(".confirm").hide();
        $(".submitReady").show();
    }
</script>
<script type="text/javascript" src="js/responsive-tables.js"></script>
<script>
    $(document).foundation();
</script>
