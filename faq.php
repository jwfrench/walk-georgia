﻿<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

<div id="main">

  <!-- Global Header Container -->
  <div id="top nojava">

    <!-- Header Image Container -->
    <div id="slideshow">

      <!-- Page Title Container -->
      <div id="slideshow-title">
        <div class="row nojava center pa5-ns pt3">
          <h2 class="font -secondary -white -big -lowercase center">FAQ</h2>
          <p class="font -primary -small-medium -white -spacing -lowercase">(Frequently Asked Questions)</p>
          <br /><br /><br />

        </div>
      </div>
      <!-- End Page Title Container -->

    </div>
    <!-- End Header Image Container -->

  </div>
  <!-- End Global Header Container -->

    <!-- Main Content -->

    <div class="row" id="faqTop" style="margin-top:20px;">

      <div class="large-4 medium-4 hide-for-small columns">
        <h2 class="font -medium -light-grey -secondary">Index</h2>

        <ul class="side-nav" role="navigation" title="Link List">
          <li class="menuitem"><h2 class="font -standard -light-grey -bold -uppercase">General Info:</h2></li>
          <li role="menuitem"><a href="#A1" class="scroll">What is Walk Georgia?</a></li>
          <li role="menuitem"><a href="#A2" class="scroll">Does it cost anything?</a></li>
          <li role="menuitem"><a href="#A3" class="scroll">How can something so great be free?</a></li>
          <li role="menuitem"><a href="#A4" class="scroll">How do I get started?</a></li>
          <li role="menuitem"><a href="#A5" class="scroll">Why does the website look funny on my screen?</a></li>
          <li role="menuitem"><a href="#A6" class="scroll">I really want to be fit, but I always get caught up in life and cannot make it to the gym every week. Suggestions?</a></li>
          <li role="menuitem"><a href="#A7" class="scroll">How can I remind myself to track my physical activity?</a></li>
          <li class="divider"></li>
          <li class="menuitem"><h2 class="font -standard -light-grey -bold -uppercase">Accounts:</h2></li>
          <li role="menuitem"><a href="#B1" class="scroll">How can I sign up for Walk Georgia?</a></li>
          <li role="menuitem"><a href="#B2" class="scroll">What are the password requirements?</a></li>
          <li role="menuitem"><a href="#B3" class="scroll">I don't remember my password. What do I do?</a></li>
          <li role="menuitem"><a href="#B4" class="scroll">Why do you ask me for my "County of Participation?"</a></li>
          <li role="menuitem"><a href="#B5" class="scroll">Why can’t children under 13 years of age have an account on Walk Georgia?</a></li>
          <li role="menuitem"><a href="#B6" class="scroll">Who can see my profile information?</a></li>
          <li role="menuitem"><a href="#B7" class="scroll">I’ve already signed up, but I can’t login!</a></li>
          <li role="menuitem"><a href="#B8" class="scroll">I need help with my Walk Georgia account. Who should I contact?</a></li>
          <li class="divider"></li>
          <li class="menuitem"><h2 class="font -standard -light-grey -bold -uppercase">My Profile:</h2></li>
          <li role="menuitem"><a href="#C1" class="scroll">How do I edit my profile and account info?</a></li>
          <li role="menuitem"><a href="#C2" class="scroll">How do I change my profile picture?</a></li>
          <li class="divider"></li>
          <li class="menuitem"><h2 class="font -standard -light-grey -bold -uppercase">Logging Activity:</h2></li>
          <li role="menuitem"><a href="#D1" class="scroll">How do I log an activity?</a></li>
          <li role="menuitem"><a href="#D2" class="scroll">What activities are available?</a></li>
          <li role="menuitem"><a href="#D3" class="scroll">What is “Distance Traveled” in my Quick stats bar?</a></li>
          <li class="divider"></li>
          <li class="menuitem"><h2 class="font -standard -light-grey -bold -uppercase">Points:</h2></li>
          <li role="menuitem"><a href="#E1" class="scroll">I love my points, but what do they actually mean?</a></li>
          <li role="menuitem"><a href="#E2" class="scroll">How are METs assigned to physical activities?</a></li>
          <li role="menuitem"><a href="#E3" class="scroll">How can I use my points and record of activity?</a></li>
          <li class="divider"></li>
          <li class="menuitem"><h2 class="font -standard -light-grey -bold -uppercase">My Groups:</h2></li>
          <li role="menuitem"><a href="#F1" class="scroll">What is a group?</a></li>
          <li role="menuitem"><a href="#F2" class="scroll">What is a subgroup?</a></li>
          <li role="menuitem"><a href="#F3" class="scroll">What are "Favorite Groups?"</a></li>
          <li role="menuitem"><a href="#F4" class="scroll">I can’t find the group I made! Where is it?</a></li>
          <li role="menuitem"><a href="#F5" class="scroll">How do I join a group?</a></li>
          <li role="menuitem"><a href="#F6" class="scroll">How do I create a group?</a></li>
          <li role="menuitem"><a href="#F7" class="scroll">What information will I be able to gather from my group members? </a></li>
          <li role="menuitem"><a href="#F8" class="scroll">How do I invite people to join my group?</a></li>
          <li role="menuitem"><a href="#F9" class="scroll">Who can see my group information?</a></li>
          <li role="menuitem"><a href="#F10" class="scroll">How do I change my group’s visibility?</a></li>
          <li role="menuitem"><a href="#F11" class="scroll">How do I add groups to “My Favorites”</a></li>
          <li role="menuitem"><a href="#F12" class="scroll">How do I create subgroups? What are sub-groups used for?</a></li>
          <li role="menuitem"><a href="#F13" class="scroll">How do I join a subgroup?</a></li>
          <li class="divider"></li>

         <!-- remvoed at request of mbowie 2/20/17 bjw
          <li class="menuitem"><h2 class="font -standard -light-grey -bold -uppercase">Sessions:</h2></li>
          <li role="menuitem"><a href="#G1" class="scroll">What are “Sessions”?</a></li>
          <li role="menuitem"><a href="#G2" class="scroll">On the old Walk Georgia, the site would only be available twice a year. Is this still the case?</a></li> <li role="menuitem"><a href="#G3" class="scroll">How do I create a session?</a></li> 
          <li role="menuitem"><a href="#G4" class="scroll">How do I invite people to join my session?</a></li>
          <li role="menuitem"><a href="#G5" class="scroll">Who can see my session?</a></li>
          <li class="divider"></li>
	 -->
					<li class="menuitem"><h2 class="font -standard -light-grey -bold -uppercase">My Goals:</h2></li>
          <li role="menuitem"><a href="#K1" class="scroll">What are "Primary Activities"?</a></li>
          <li role="menuitem"><a href="#K2" class="scroll">What if I don’t complete any of my “Primary Activities” this week— will I miss my goal?</a></li>
					<li class="divider"></li>
          <li class="menuitem"><h2 class="font -standard -light-grey -bold -uppercase">Administration & Reporting:</h2></li>
          <li role="menuitem"><a href="#H1" class="scroll">How do I know if I am a group administrator? How do I become one?</a></li>
          <li role="menuitem"><a href="#H2" class="scroll">What are the county administrators??</a></li>
          <li role="menuitem"><a href="#H3" class="scroll">I’m the administrator of a group. How can I generate a report for my entire group?</a></li>
          <li role="menuitem"><a href="#H4" class="scroll">I’m the administrator of a group. How can I generate a report on just one person?</a></li>
          <li role="menuitem"><a href="#H5" class="scroll">I’m a schoolteacher. How can I use Walk Georgia in my Classroom?</a></li>
          <li role="menuitem"><a href="#H6" class="scroll">I’m the leader of a group, how can I use Walk Georgia in my group?</a></li>
          <li role="menuitem"><a href="#H7" class="scroll">I’m a local county leader or Extension agent, how can I educate citizens and organizations in my county about Walk Georgia?</a></li>
          <li class="divider"></li>
          <li class="menuitem"><h2 class="font -standard -light-grey -bold -uppercase">The Map:</h2></li>
          <li role="menuitem"><a href="#I1" class="scroll">What is The Map?!</a></li>
          <li role="menuitem"><a href="#I2" class="scroll">I’ve been logging activity for a long time on WalkGeorgia.org; can I use those points to earn counties?</a></li>
          <li role="menuitem"><a href="#I3" class="scroll">I see something wrong with the information in my county! How do I change it?</a></li>
          <li role="menuitem"><a href="#I4" class="scroll">How do I restart the game?</a></li>
          <li role="menuitem"><a href="#I5" class="scroll">If I’m using group logging, how can we participate in the Map at one time?</a></li>
          <li class="divider"></li>
          <li class="menuitem"><h2 class="font -standard -light-grey -bold -uppercase">Other Common Questions:</h2></li>
          <li role="menuitem"><a href="#J1" class="scroll">Does Walk Georgia integrate with my Fitbit? How Can I record the amount of steps I take each day?</a></li>
          <li role="menuitem"><a href="#J2" class="scroll">Is there a Walk Georgia app?</a></li>
          <li role="menuitem"><a href="#J3" class="scroll">I have tried to reset my password, but each time I receive the link, it takes me back to the same screen. Why am I stuck in a loop?</a></li>
          <li role="menuitem"><a href="#J4" class="scroll">What happens to my activity after the spring or fall campaign? Does my activity ever get erased?</a></li>
          <li role="menuitem"><a href="#J5" class="scroll">How do I share my activity through Facebook?</a></li>
        </ul>
      </div>


        <div class="large-8 medium-8 columns">
        <!-- Begin F.A.Q. Articles/Content -->

          <!-- Heading Title -->
            <div class="center"><h2 class="font -big -blue -secondary">General Info:</h2></div>
          <!-- End Heading Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="A1">What is Walk Georgia?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Walk Georgia is a free Web-based fitness program designed to encourage physical activity through accountability and community. The Walk Georgia program provides free fitness tracking, research-based knowledge and resources on fitness.</p>
          <p class="font -extra-small -dark-grey -justify">Users can even create custom group challenges. The website also provides resources on health, plus tools and information to get physically active in each Georgia county. Users can choose to participate individually, or within a group to foster accountability or competition.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>

          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="A2">Does it cost anything?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Nope! There is no cost to be a part of Walk Georgia.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="A3">How can something so great be free?</h2>
          <p class="font -extra-small -dark-grey -justify">
          <!--Walk Georgia is sponsored in part by a grant from The Coca-Cola Foundation. -->UGA Extension built Walk Georgia in order to improve the quality of life for all Georgians.
          </p>
          <p class="font -extra-small -dark-grey -justify">
          UGA Extension offers lifelong learning opportunities to the people of Georgia through research-based education. Extension exists to help all Georgians become healthier, more productive, financially independent, and environmentally responsible. Extension offices are in 158 of the 159 Georgia counties and they work with the issues relevant to each local community. You can contact your local Extension office with any additional questions at 1-800-ASK-UGA1.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="A4">How do I get started?</h2>
          <p class="font -extra-small -dark-grey -justify">
          First you'll need to <a href="register.php">create an account</a> to get started.  After that, you can start logging your activity, join or create groups, learn about Georgia through our interactive Map, and more.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="A5">Why does the website look funny on my screen?</h2>
          <p class="font -extra-small -dark-grey -justify">
          If the webpage looks confusing or odd, chances are your browser isn’t up to date. The Walk Georgia website works best on Google Chrome, Mozilla Firefox, and Apple Safari browsers. It is also compatible with Microsoft Edge. If the website appears off, check into updating your browser. An up-to-date browser will help the Walk Georgia website function best.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="A6">I really want to be fit, but I always get caught up in life and cannot make it to the gym every week. Suggestions?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Why yes! We have suggestions. Head over to our <a href="http://blog.extension.uga.edu/walkgeorgia/">blog</a> or our <a href="/resources/fitness/index.php">"Fitness Tips"</a> page. Behind your screen is a team of real people who have busy lives too and we totally understand. For more encouragement, <a href="resources/index.php">check out our "Resources" section</a> and engaged with us on social media, Facebook, and Twitter.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="A7">How can I remind myself to track my physical activity?</h2>
          <p class="font -extra-small -dark-grey -justify">
         We recommend logging your activity each day alongside another habit like when you check your email, Facebook, or have your first coffee. You can also set up a calendar reminder or save the WG homepage as an icon on your smart phone to help remind you to log that activity.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->




          <hr style="margin-top:-5px;" />


          <!-- ACCOUNTS SECTION -->

          <!-- Heading Title -->
            <div class="center"><h2 class="font -big -blue -secondary">Accounts:</h2></div>
          <!-- End Heading Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="B1">How can I sign up for Walk Georgia?</h2>
          <p class="font -extra-small -dark-grey -justify">
          It’s as simple as having an email address. Simply visit our homepage <a href="http://www.walkgeorgia.org">walkgeorgia.org</a>, and click the green “Create an Account” button. You’ll be asked to enter a small amount of information, and create a password. Each time you visit our site, you’ll log into your account with your email address and password. The site will remember your login info for up to 30 days.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="B2">What are the password requirements?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Walk Georgia passwords must:
          <br />
          1.  Be 4-30 characters long
          <br />
          2.  Include one number
          <br />
          3. Passwords are case-sensitive
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="B3">I don't remember my password. What do I do?</h2>
          <p class="font -extra-small -dark-grey -justify">
          After you’ve clicked the blue “Log in” button on the homepage, the next screen carries a “Forgot your password?” link at the bottom. Simply clicking this link will direct you to reset your password. If you are still having trouble, email walkga@uga.edu.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="B4">Why do you ask me for my "County of Participation?"</h2>
          <p class="font -extra-small -dark-grey -justify">
          Walk Georgia is a University of Georgia Extension program. UGA Extension stays in touch with relevant issues through localized county Extension offices. There is a county Extension office administering the Walk Georgia program in 158 of Georgia’s 159 counties. We ask for your county to ensure you’re registered where we can best serve you.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="B5">Why can’t children under 13 years of age have an account on Walk Georgia?</h2>
          <p class="font -extra-small -dark-grey -justify">
          In deference to the Children’s Online Privacy Protection Act (COPPA), we don’t permit children under 13 years old to use the Walk Georgia website as individuals. Physical activity data for children under 13 can be logged by a parent or guardian, or can be logged as part of a group by a teacher, 4-H Extension agent, or other adult group leader. Each group administrator has the ability to log activity as a group under the “Group Tools”.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="B6">Who can see my profile information?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Great question! When you sign up, your default profile setting is “private”, unless you select otherwise. This means only you can see your information. You can change your privacy settings under “My Account” > “Preferences” > “Profile Visibility". Making your profile public will allow other users to search for you and view your basic activity information. Should you choose to keep your profile private, only you and Walk Georgia website administrators will be able to see your profile. Please note that if you join a group, the members of that specific group can also see your profile.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="B7">I’ve already signed up, but I can’t log in!</h2>
          <p class="font -extra-small -dark-grey -justify">
          OK, deep breath. It’s either <em>our fault</em> or <em>your finger's</em>. After triple-checking your typing, we suggest resetting your password-- it’s super quick! To reset your password, click the link at the bottom of the “Log In” page that says, "Forgot your password?” and follow the directions. Please remember that passwords are case-sensitive.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="B8">I need help with my Walk Georgia account. Who should I contact?</h2>
          <p class="font -extra-small -dark-grey -justify">
          If you are having trouble with the website email <a href="mailto:walkga@uga.edu">walkga@uga.edu</a>. If you would like more information about using this program contact your county’s UGA Extension office. You can find their contact information by visiting <a href="http://www.extension.uga.edu" target="_blank">extension.uga.edu</a> or by calling 1-800-ASK-UGA1.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->



          <hr style="margin-top:-5px;" />



          <!-- MY PROFILE SECTION -->

          <!-- Heading Title -->
            <div class="center"><h2 class="font -big -blue -secondary">My Profile:</h2></div>
          <!-- End Heading Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="C1">How do I edit my profile and account info?</h2>
          <p class="font -extra-small -dark-grey -justify">
          After you’ve logged into the site, you can edit your profile and account information by navigating to “My Account” at the top right of the page. From there, simply click “Preferences". Once you have made the necessary changes in your account information, click the green “Save” button at the bottom of the page.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="C2">How do I change my profile picture?</h2>
          <p class="font -extra-small -dark-grey -justify">
          We know you’re looking good! Share your picture by clicking on the round profile picture icon. A box will appear asking, “Do you want to upload a new image?” Choose the file you’d like to upload from your computer and once it’s uploaded, hit “Submit." Our site accepts .jpg, .gif, and .png files.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->


          <hr style="margin-top:-5px;" />



          <!-- LOGGING ACTIVITY SECTION -->



          <!-- Heading Title -->
            <div class="center"><h2 class="font -big -blue -secondary">Logging Activity</h2></div>
          <!-- End Heading Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="D1">How do I log an activity?</h2>
          <p class="font -extra-small -dark-grey -justify">
          First, navigate to your “My Activity” page in the grey navigation bar near the top of the site. Once you’re there, click on the green “Log an Activity” button under your name and profile image. Once you’ve completed the form, hit “Submit” and enjoy watching your points add up!
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="D2">What activities are available?</h2>
          <p class="font -extra-small -dark-grey -justify">
          We have over 70 activities to choose from! Take a look on the site, under the “Log an Activity” button and if you don’t see your favorite, email us with your suggestion at <a href="mailto:walkga@uga.edu">walkga@uga.edu</a>.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="D3">What is “Distance Traveled” in my Quick stats bar?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Many Walk Georgia users engage in distance-based fitness activities (like running, walking, bicycling, etc.).  When you log these distance-based activities, we keep track. It’s fun to look back and see just how far (literally) you’ve come in your fitness journey! Remember, don’t be discouraged if that number is low, jumping rope may not get your feet anywhere, but it will raise your heart rate and mood in no time!
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->



          <hr style="margin-top:-5px;" />



          <!-- POINTS SECTION -->



          <!-- Heading Title -->
            <div class="center"><h2 class="font -big -blue -secondary">Points</h2></div>
          <!-- End Heading Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="E1">I love my points, but what do they actually mean?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Your “Points” are a simple way for you to measure how active you have been. The points are based on a modern and scientifically sound formula:
(T x M) + P – where “T” is time, “M” stands for METs, and “P” is perceived difficulty of the activity on a scale of one to five.
          </p>
          <p class="font -extra-small -dark-grey -justify">
          <strong>“MET”</strong> stands for “metabolic equivalent of task” and in a nutshell, it represents the amount of oxygen demanded by your body for varying physical activities. As a result, METs indicate an activity’s intensity. For example, one MET is the estimated cost of sitting quietly. As you engage in more vigorous activity, your oxygen demands go up, and so do the METs assigned to that activity. For example, activities such as walking slowly, yoga and housework use about 3 METs, fast walking and doubles tennis use 3 to 6, and jogging, shoveling snow, and singles tennis use more than 6. (From http://www.health.harvard.edu)
          </p>
          <p class="font -extra-small -dark-grey -justify">
          To learn even more about the science behind these points click here.
          <br>
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->



          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="E2">How are METs assigned to physical activities?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Actually, there’s a handy guide – published by Arizona State University and the National Cancer Institute at the National Institutes of Health – that we pulled our METs information from called the “Compendium of Physical Activities.” <a href="https://sites.google.com/site/compendiumofphysicalactivities/home" target="_blank"> You can click here to view the compendium</a>.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="E3">How can I use my points and record of activity?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Some groups use Walk Georgia and the point system to award prizes to fellow group members; some groups gain insurance discounts or time off for employees and members who record a certain amount of activity, and some just share their accomplishments through our “Share to Facebook” feature! If you work with a group, suggest they use Walk Georgia to promote wellness accountability, or call your local extension office for more ideas!
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->


          <hr style="margin-top:-5px;" />



          <!-- GROUPS SECTION -->



          <!-- Heading Title -->
            <div class="center"><h2 class="font -big -blue -secondary">Groups</h2></div>
          <!-- End Heading Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="F1">What is a group?</h2>
          <p class="font -extra-small -dark-grey -justify">
          You could think of a “group” like a fitness book club. We offer the group function so that you can invite like-minded Walk Georgia participants to join you in your wellness journey. You can use groups to compete, work toward the same goal as other members, or as a way to track everyone’s collective progress no matter what the pace! Groups allow you to see other members’ progress alongside your own. Groups are a great way to give encouragement, get encouragement, or create a friendly challenge.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="F2">What is a subgroup?</h2>
          <p class="font -extra-small -dark-grey -justify">
          A subgroup is simply a group inside a main group. The ability to create subgroups allows organizations to have greater flexibility when creating groups and challenges. For example, if the CEO of Acme Services Company creates the Acme Fitness Group for the company’s wellness challenge, the company can also create subgroups, such as the “Human Resources Group” or “Accounting Group” to get specific reports on those departments, too. Subgroups make the site a dynamic tool for tracking and rewarding progress, no matter how your organization is designed!
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="F3">What are "Favorite Groups?"</h2>
          <p class="font -extra-small -dark-grey -justify">
          Your Favorite groups are enabled for quick access to the groups you use most often. You can add a “Favorite” by going to your main "My Groups" page and clicking on the light grey “Add a Favorite” button. This will allow you to choose your favorite group from a drop down menu. Once you’ve chosen your favorite, simply click on the blue “Add to Favorites” button.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="F4">I can’t find the group I made! Where is it?</h2>
          <p class="font -extra-small -dark-grey -justify">
          All groups in which you’re a member should be listed at the bottom of your main "My Groups" page under the heading “All My Groups” (this list does not include subgroups). To navigate to your main group page, simply click on “My Groups” at the top, in the grey navigation bar. If you still can’t find your group, send an email to walkga@uga.edu.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="F5">How do I join a group?</h2>
          <p class="font -extra-small -dark-grey -justify">
          If the group is public, you can simply search for the group in the light grey navigation bar at the top (look for the magnifying glass). Once you visit the group, click on the grey “Join Group” button, to the right of the group image. You will know you have joined the group if the button beside the group image is green and says “Member Options." If the group is private, you will need to receive an invitation from that group’s administrator.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="F6">How do I create a group?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Once you’ve logged in to your account, navigate to the “My Groups” page. At the top of the “My Groups” page, there’s a green “Create a Group" button. Clicking on this will make a box appear offering three group types: “Default”, “Schools”, or “Organizations".
          </p>
          <p class="font -extra-small -dark-grey -justify">
            After choosing the group type that best fits your needs, you’ll decide the new group’s Name, Description, and Visibility (which indicates whether you want the group to be public [anyone can search for and join] or private [you’ll need to send the custom group link to the specific group members]).
          </p>
          <p class="font -extra-small -dark-grey -justify">
          If you will need info from joining group members like an employee identification number or a department, just click the “Add Extra Info” button. Once you have completed the entire “Create a Group” form, hit the blue “Create Group” button at the bottom of the box.
          </p>
          <p class="font -extra-small -dark-grey -justify">
            This will lead you to your new group page where you’ll see a grey button that hosts your “Group Tools". Here you can get a custom link to invite new members to your group, view your group’s reports, add subgroups, or edit your group!
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="F7">What information will I be able to gather from my group members?</h2>
          <p class="font -extra-small -dark-grey -justify">
          If you are the member who is setting up your group, you can require that joining group members fill out additional information like an employee ID number, department, favorite food, etc. To do this, simply fill out the “Add Extra Info” box while creating a group. In addition to this information, any group administrator will be able to view all group members’ activity information.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="F8">How do I invite people to join my group?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Only the group’s creator or a group administrator can invite people to join a group. You can do this by clicking on the grey “Group Tools” button. Under “Group Tools” you have the ability to “Invite Members." This button provides a custom link that you can copy and send however you choose — we use email!
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="F9">Who can see my group information?</h2>
          <p class="font -extra-small -dark-grey -justify">
          If your group is marked “Private," then only you and members of the group can see your information. If your group is public, then anyone can see your group.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="F10">How do I change my group’s visibility?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Only the group’s creator/administrator has access to this function. If you’d like to change your group’s status to “public” or “private," simply click on the grey “Group Tools” button > “Edit Group” > “Group Visibility” > “Save Changes."
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="F11">How do I add groups to “My Favorites?”</h2>
          <p class="font -extra-small -dark-grey -justify">
          You can add a “Favorite” group for quick access by going to your main "Group" page and clicking on the grey “Add a Favorite” box. This will allow you to choose your favorite group from a dropdown menu. Once you’ve chosen your favorite, simply click on the blue “Add to Favorites” button.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="F12">How do I create subgroups? What are subgroups used for?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Subgroups give you flexibility as you organize your wellness competition or challenge. If you want additional competitions according to departments, floors of your building, etc., then you can use and create subgroups to catalog those individuals’ participation. It is the group administrator's duty to create the subgroup(s). To create one, simply click on "Group Tools" > "Add/Edit Subgroups" > green "Create a new subgroup" - you will then see a modal window pop up asking for your subgroup information. Once you create the subgroup, you will see it appear on the list of subgroups for that group. To create a deeper level of subgroup within an existing subgroup, click on the subgroup you'd like to add more levels to, click "Add/Edit to this Subgroup" and then repeat the process listed here. A helpful way to think about the subgroup system, is thinking about the way folders work on your computer. You can create a folder, and then create more folders within that folder, and even more folders within those folders. Subgroups on our site work the same way!
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="F13">How do I join a subgroup?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Joining a subgroup works in the same way as joining a main group. The administrator of the subgroup can send you an invitation, or you can join a subgroup by navigating to it through the main group page. From that "Group" page, you can scroll down to see "Subgroups" on the right. Simply click on the subgroup you need to join, and then click "Join Group" at the top of the page.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->




	<!-- removed at request of Mbowie, 2/20/17 bjw

          <hr style="margin-top:-5px;" />

          <div class="center"><h2 class="font -big -blue -secondary">Sessions</h2></div>

          <h2 class="font -medium -secondary -blue" id="G1">What are “Sessions?"</h2>
          <p class="font -extra-small -dark-grey -justify">
          Good question! Sessions are just a period of time that members can set in order to host a competition or wellness promotion within Walk Georgia. They can be as long or as short as you want. For example, many companies create a group and set a 12-week session during the New Year. At the end of the 12 weeks, they award prizes, like time off to employees who have made the most progress! Pretty cool, huh?
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>

          <h2 class="font -medium -secondary -blue" id="G2">On the old Walk Georgia, the site would only be available twice a year. Is this still the case?</h2>
          <p class="font -extra-small -dark-grey -justify">
          WalkGeorgia.org will never close or shut down because the new Walk Georgia allows you to create your own sessions! Walk Georgia will continue to feature a session in the spring and in the fall—but that’s just for fun. If you are a part of a group, you can create your own session(s) for any period of time, as little or as often as you’d like!
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>

          <h2 class="font -medium -secondary -blue" id="G3">How do I create a session?</h2>
          <p class="font -extra-small -dark-grey -justify" >
          When you are logged in, start by clicking the "My Sessions" link in the grey menu bar at the top. Once you're on the "My Sessions" page, you'll see a blue button that says, "Create a Session." When you click this button, a window appears. Fill in the requested information and desired start and end date for your session. That’s it! The site should automatically take you to your new session page where you can invite users to join.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>

          <h2 class="font -medium -secondary -blue" id="G4">How do I invite people to join my session?</h2>
          <p class="font -extra-small -dark-grey -justify" >
          On the session page, the session’s creator should see a grey button that says "Session Tools." The button is only available to session creators and administrators. When you click this button, a new window will appear with options for what you can add or edit. There is a button to "Invite Members". Clicking on that button will give you a unique URL for your session that you can share with whoever you like. Email it out to all your friends or post it on promotional posters. It is up to you!
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>

          <h2 class="font -medium -secondary -blue" id="G5">Who can see my session?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Like everything else in Walk Georgia, sessions are private by default. This means that the only people that can see your session, if it is set to private, are the people you have invited. If you set your session to “public”, other people will be able to search for and find your session and even join it if they feel inclined.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
	-->





          <hr style="margin-top:-5px;" />

					<!-- Heading Title -->
						<div class="center"><h2 class="font -big -blue -secondary">My Goals</h2></div>
					<!-- End Heading Title -->

					<!-- Question Title -->
					<h2 class="font -medium -secondary -blue" id="K1">What are "Primary Activities"?</h2>
					<p class="font -extra-small -dark-grey -justify">
					"Primary Activities" are the activities in which you are most likely to participate this week, or the ones you enjoy doing the most. By choosing at least one primary activity, you help us create a personal and customized point goal that is both challenging and attainable!
					<br />
					<a href="#faqTop" class="scroll">Back to Top</a>
					</p>
					<!-- End Question Title -->

					<!-- Question Title -->
					<h2 class="font -medium -secondary -blue" id="K2">What if I don’t complete any of my “Primary Activities” this week— will I miss my goal? </h2>
					<p class="font -extra-small -dark-grey -justify">
					Not if we can help it! Any activity you log on Walk Georgia will contribute to your point goal. We encourage you to get active in the things you enjoy, but if that’s not possible, be flexible and get moving to meet your goal no matter what activity you try!
					<br />
					<a href="#faqTop" class="scroll">Back to Top</a>
					</p>
					<!-- End Question Title -->

					<hr style="margin-top:-5px;" />

          <!-- ADMIN & REPORTING SECTION -->



          <!-- Heading Title -->
            <div class="center"><h2 class="font -big -blue -secondary">Administration & Reporting</h2></div>
          <!-- End Heading Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="H1">How do I know if I am a group administrator? How do I become one?</h2>
          <p class="font -extra-small -dark-grey -justify">
          The person who creates the group is automatically the group’s administrator. However, the group creator and any administrator have the right to grant admin status to other members. All administrators have a grey “Group Tools" button located at the top of the group page, beside “Member Options”.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->
          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="H2">What are the county administrators?</h2>
          <p class="font -extra-small -dark-grey -justify">
          The county administrator is the director of Walk Georgia in your county, and is a member of your local UGA Extension office. They will be able to find an answer to any of your questions about Walk Georgia. To locate them, call your local UGA Extension office or visit the <a href="http://extension.uga.edu/about/county/index.cfm">Extension website</a>.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->


          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="H3">I’m the administrator of a group. How can I generate a report for my entire group?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Under the "Quick Stats" on your group’s home page you will see a blue button that will allow you to generate your group’s report.
You can also click on the grey “Group Tools” button, which will lead you to another “Generate Group Report” button.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="H4">I’m the administrator of a group. How can I generate a report on just one person?</h2>
          <p class="font -extra-small -dark-grey -justify">
To get a report on an individual, first generate the group’s report, and then click on the individual’s first name within the group report.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="H5">I’m a school teacher. How can I use Walk Georgia in my classroom?</h2>
          <p class="font -extra-small -dark-grey -justify">
          When we built the new Walk Georgia, you were always on our mind. We have made it easy to use the “Groups” function to track and encourage a classroom, a grade, a school, or a school system! For a quick implementation guide, see the "Help Center" on the toolbar at the top of the screen and click "Implementation Guides." We have also worked with UGA’s College of Education to create lesson plans for your classroom. For a list of those lesson plans, click HERE.
         </p>
        <p class="font -extra-small -dark-grey -justify">
          We also encourage you to explore our “My Groups” option and use this tool to help encourage your students to move more consistently!
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="H6">I’m the leader of a group. How can I use Walk Georgia in my group?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Awesome! Well, let’s help you lead your group to better health. We’ve found the best way for groups to participate is through competition, which is why you have access to comprehensive group reports through your “Group Tools”. However, we also understand that different things motivate people - the endgame is different for all folks looking to lead healthier lives. Walk Georgia is perfect for your group because it allows those reports to be viewed in a myriad of ways. You can challenge and reward your group based on consistency, points earned, time spent, and other ways to make sure you encourage any and every effort. You can read our quick tutorial on how to set up your group, or email <a href="mailto:walkga@uga.edu">walkga@uga.edu</a> for more suggestions.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="H7">I’m a local county leader or Extension agent, how can I educate citizens and organizations in my county about Walk Georgia?</h2>
          <p class="font -extra-small -dark-grey -justify">
          Awesome! We have created a comprehensive implementation plan just for you. Simply go to the top menu bar under "Help Center" and click on "Implementation Guides." We also encourage you to email Walk Georgia at walkga@uga.edu. Each county in Georgia is so special, and our specialty is ideas. We can tailor Walk Georgia’s resources and information to the organizations and needs in your county. Walk Georgia is such a cool program because it has the flexibility to meet people where they are. Walk Georgia wants to help ALL people get moving.

          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->


          <hr style="margin-top:-5px;" />



          <!-- MAP SECTION -->



          <!-- Heading Title -->
            <div class="center"><h2 class="font -big -blue -secondary">The Map</h2></div>
          <!-- End Heading Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="I1">What's The Map?!</h2>
          <p class="font -extra-small -dark-grey -justify">The Map is an interactive adventure enabling Walk Georgia users to learn more about our great state and quantify the progress they are making! For every 65 points of activity logged on Walk Georgia, you can virtually travel to one new Georgia county and learn about its history and fun facts! If you earn 3-4 counties each week, you’ll be able to “Walk Georgia” in a year!
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="I2">I’ve been logging activity for a long time on WalkGeorgia.org, why can’t I use those points to earn counties?</h2>
          <p class="font -extra-small -dark-grey -justify">
          You’ll need to log new activity after clicking “Start the Game!” to earn counties. The interactive Map does not use previously logged points so that we encourage users to move more, live more, and learn more!
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="I3">I see something wrong with the information in my county! How do I change it?</h2>
          <p class="font -extra-small -dark-grey -justify">
          We’ve attempted to compile the best information from a variety of local residents, but we know mistakes are possible! If you feel there is an error in your county’s information, please send an email to walkga@uga.edu. Keep in mind, not all counties have state parks or historic sites, which is why we’ve included “Nearby State Parks and Historic Sites”. You may learn of a new place in a neighboring county.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="I4">How do I restart the Map game?</h2>
          <p class="font -extra-small -dark-grey -justify">
          First, ensure this is what you want to do! Resetting your Map is a permanent action and it clears all of your counties earned on the Map.
To reset the game, scroll to the bottom of the “My Map” page. Underneath the list of “Unlocked Counties”, there is a blue link that asks, “Need to reset your counties?” Click on this link and hit the red “Reset!” button if you want to confirm.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->


<!-- Heading Title -->
            <div class="center"><h2 class="font -big -blue -secondary">Other Common Questions</h2></div>
          <!-- End Heading Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="J1">Does Walk Georgia integrate with my Fitbit? How Can I record the amount of steps I take each day?</h2>
          <p class="font -extra-small -dark-grey -justify">Walk Georgia does not integrate with other fitness tracking devices. We suggest converting your steps to miles—it’s simple!
             </p>
          <p class="font -extra-small -dark-grey -justify">
          One mile converts to about 2,000 steps, so it’s easy to enter how many miles you’ve walked each day!
           </p>
          <p class="font -extra-small -dark-grey -justify">
            For instance, if you walk 10,000 steps at a normal pace throughout your 16-hour day, you can simply enter "5 miles", and that the pace was “Easy”.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="J2">Is there a Walk Georgia app?</h2>
          <p class="font -extra-small -dark-grey -justify">
          The Walk Georgia site is scalable to all mobile devices, so that it functions just like an app, without having to download anything.
          </p>
          <p class="font -extra-small -dark-grey -justify">
            To create an icon on your home screen, just like an app, take a screenshot and most devices offer you the option to “add to home screen” for quick access!
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="J3">I have tried to reset my password, but each time I receive the link, it takes me back to the same screen. Why am I stuck in a loop?</h2>
          <p class="font -extra-small -dark-grey -justify">
          We're sorry that you were having trouble logging into the site. Have you created an account on the Walk Georgia site after February 2015? Though we kept the website address, the new interface launched in February of 2015 was all-new.
          </p>
           <p class="font -extra-small -dark-grey -justify">
            If you are trying to reset your password with an email that has not yet been registered, the "password loop" happens.
          </p>
             <p class="font -extra-small -dark-grey -justify">
            If you were registered on the old site, or the pilot site, the new site will still require you to hit that bright green button on the homepage and "Create an Account" before you can begin. If you believe you have registered, and you still cannot access your account, please send an email to <a href="mailto:walkga@uga.edu">walkga@uga.edu</a>.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="J4">What happens to my activity after the spring or fall campaign? Does my activity ever get erased?</h2>
          <p class="font -extra-small -dark-grey -justify">
          We will never erase your activity now that the site is open year around. If you would like to view your activity during a certain time period, you can simply create a session.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

          <!-- Question Title -->
          <h2 class="font -medium -secondary -blue" id="J5">How do I share my activity through Facebook?</h2>
          <p class="font -extra-small -dark-grey -justify">
          When you log activity on your “My Activity” page, you will receive a dropdown confirmation with the amount of points you received. At the bottom of this box, you will have the option to share your activity on Facebook. If you are not already logged into Facebook, the site will prompt you to log in before you are able to share.
          <br />
          <a href="#faqTop" class="scroll">Back to Top</a>
          </p>
          <!-- End Question Title -->

      </div>


    </div>

  </div>

<?php

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>

    <!-- Smooth Anchor Scroll -->

    <script type="text/javascript">
    jQuery(document).ready(function($) {

	$(".scroll").click(function(event){
		event.preventDefault();
		$('html,body').animate({scrollTop:$(this.hash).offset().top - 150}, 500);
	});
});
    </script>

    <!-- End Smooth Anchor Scroll -->

	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script src="js/foundation/foundation.abide.js"></script>
    <script src="js/foundation/foundation.reveal.js"></script>
    <script src="js/foundation/foundation.tooltip.js"></script>
    <script src="js/foundation/foundation.offcanvas.js"></script>
    <script src="js/foundation/foundation.equalizer.js"></script>
    <script src="js/foundation/foundation.dropdown.js"></script>
    <script type="text/javascript" src="js/sha512.js"></script>
    <script type="text/javascript" src="js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>
