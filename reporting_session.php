<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/back_api.php");
include_once ("$root/lib/sessions_api.php");
//get the session info
$session = SESSIONS::get_info(filter_input(INPUT_GET, 'session', FILTER_SANITIZE_STRING));
$counties='';
$time='';
$points='';
$distance='';
$session_variable = filter_input(INPUT_GET, 'session', FILTER_SANITIZE_STRING);
//GET THE COUNTY INFO
if(isset($session['S_STARTDATE'])){
	$date_range = 'AL_DATE >= \''.$session['S_STARTDATE'].'\' AND AL_DATE <= \''.$session['S_ENDDATE'].'\' AND';
	$date_range2 = 'AL_DATE >= \''.$session['S_STARTDATE'].'\' AND AL_DATE <= \''.$session['S_ENDDATE'].'\' AND';
}
//query for total users
$sql0 = 'SELECT COUNT(DISTINCT U_ID) AS COUNT FROM SESSION_MEMBER WHERE S_ID =  \''.$session_variable.'\'';
$total_users = MSSQL::query($sql0);

//query for user data
$user_order ='AL_PA DESC';
if(filter_input(INPUT_GET, 'user_order', FILTER_SANITIZE_STRING) != null){
	$user_order=filter_input(INPUT_GET, 'user_order', FILTER_SANITIZE_STRING);
}
$sql1 = 'SELECT L_ID, L_FNAME, L_LNAME, L_COUNTY, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\' FROM LOG INNER JOIN LOGIN ON AL_UID = L_ID  WHERE  '.$date_range.' L_ID IN (SELECT U_ID FROM SESSION_MEMBER WHERE S_ID= \''.$session_variable.'\') GROUP BY L_FNAME, L_LNAME, L_COUNTY, L_ID ORDER BY '.$user_order.';';
$user = MSSQL::query($sql1);

//query for activity
$activity_order ='AL_PA DESC';filter_input(INPUT_GET, 'day1', FILTER_SANITIZE_STRING);
if(filter_input(INPUT_GET, 'activity_order', FILTER_SANITIZE_STRING) != null) {
	$activity_order=filter_input(INPUT_GET, 'activity_order', FILTER_SANITIZE_STRING);
}
$sql2 = 'SELECT AL_AID, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\', SUM(AL_UNIT) AS \'AL_UNIT\', COUNT(AL_AID) AS COUNT FROM LOG WHERE AL_UID IN (SELECT U_ID FROM SESSION_MEMBER WHERE '.$date_range.' S_ID = \''.$session_variable.'\') GROUP BY AL_AID ORDER BY '.$activity_order.';';
$activity = MSSQL::query($sql2);

//query for counties
$sql3 = 'SELECT L_COUNTY FROM LOGIN WHERE L_ID IN (SELECT U_ID FROM SESSION_MEMBER WHERE S_ID = \''.$session_variable.'\') GROUP BY L_COUNTY ORDER BY L_COUNTY ASC;';
$county_query = MSSQL::query($sql3);
while(odbc_fetch_array($county_query)){
  $counties .= '<a href="http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/reporting_county.php?county='.odbc_result($county_query, 'L_COUNTY').'">'.odbc_result($county_query, 'L_COUNTY').'</a>, ';
}



$count = 0;
$no_of_activities = odbc_num_rows($activity);
while(odbc_fetch_array($activity)){
	$aid = odbc_result($activity, 'AL_AID');
	$time += odbc_result($activity, 'AL_TIME');
	$points += odbc_result($activity, 'AL_PA');
	if (($aid == 1) || ($aid == 2) || ($aid == 3) || ($aid == 47) || ($aid == 50) || ($aid == 68) || ($aid == 70)) {
			$distance += odbc_result($activity, 'AL_UNIT');
			$count +=1;
	}
}


?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Walk Georgia | Reporting</title>
    <link rel="stylesheet" href="../../css/foundation.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.1/b-flash-1.2.1/b-html5-1.2.1/b-print-1.2.1/r-2.1.0/datatables.min.css"/>
		<link type="text/css" media="screen" rel="stylesheet" href="css/responsive-tables.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>

  <div id="main">

    <div style="margin-top:20px;"></div>

    <!-- Header -->
      <div class="row" style="margin-bottom:20px;">
        <div class="large-12 columns center">
          <img src="img/single-color-logo.png" alt="logo" />
          <h1 class="custom-font-small font -blue">Official Session Report</h1>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <h2 class="custom-font-small font -black"><?php echo $session['S_NAME']; ?></h2>
          <h2 class="custom-font-small font -black"><?php echo date('m-d-Y', strtotime($session['S_STARTDATE'])).' to '.date('m-d-Y', strtotime($session['S_ENDDATE'])); ?></h2>
          <!-- <a href="#" class="button tiny">Printer Friendly Version</a> -->
        </div>
      </div>
    <!-- End Header -->

    <!-- Report Body -->
      <div class="row">

      <!-- Overall Stats -->
        <div class="large-12 columns pb2">
          <h2 class="global-h2">Overall Stats:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
					<div class="row pt2">
						<div class="medium-4 columns tc-ns">
							<div class="font -secondary -bold -medium pb ">
								<?php echo $points; ?>
							</div>
          		<b>Total Points Earned </b>
						</div>
						<div class="medium-4 columns tc-ns pt2-s">
							<div class="font -secondary -bold -medium pb ">
								<?php echo floor($time/3600).' Hours '.($time%3600)/60 .' Minutes'; ?>
							</div>
        			<b>Total Time Exercised</b>
						</div>
						<div class="medium-4 columns tc-ns pt2-s">
							<div class="font -secondary -bold -medium pb ">
								<?php echo $distance; ?>
							</div>
          		<b>Total Miles From Distance Exercises <span data-tooltip aria-haspopup="true" class="has-tip" title="The total miles of all user's logged distance-based activites, such as running, biking, hiking, etc.">(?)</span></b>
						</div>
        	</div>
				</div>
			</div>
      <!-- End Overall Stats -->
			<!-- Group Info -->
			<div class="row">
				<div class="large-12 columns pb2">
					<h2 class="global-h2">Session Information:</h2>
					<hr style="margin-top:-5px; margin-bottom:5px;" />
						<div class="row pt2">
							<div class="medium-4 columns tc-ns">
								<div class="font -secondary -bold -medium pb ">
									<?php echo odbc_result($total_users, 'COUNT'); ?>
								</div>
								<b>Total Number of Users: </b>
							</div>
							<div class="medium-4 columns tc-ns">
								<div class="font -secondary -bold -medium pb pt2-s">
									<?php echo odbc_num_rows($user); ?>
								</div>
								<b>Total Number of Active Users <span data-tooltip aria-haspopup="true" class="has-tip" title="Any user who has actually logged activity.">(?)</span>:</b>
							</div>
							<div class="medium-4 columns tc-ns">
								<div class="font -secondary -bold -medium pb pt2-s">
									<?php echo $counties; ?>
								</div>
								<b>County Associations <span data-tooltip aria-haspopup="true" class="has-tip" title="Lists the different counties of the members.">(?)</span>:</b>
							</div>
					</div>
				</div>
			<!-- End Group Info -->
      </div>

      <div class="row">

      <!-- Group Members -->
        <div class="large-12 columns">
          <h2 class="global-h2">Session Members: (<?php echo odbc_result($total_users, 'COUNT'); ?>)</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
        <!-- End Member Filtering -->
          <ol class="global-p" style="line-height:2;">
            <table id="" class="responsive display">
              <thead>
              	<th>
                  First Name
                </th>
                <th>
                  Last Name
                </th>
                <th>
                  Points
                </th>
                <th>
                  Time(Hours)
                </th>
                <th>
                  Distance(Miles)
                </th>
              </thead>
            <?php
			  $j=0;
			  while(odbc_fetch_array($user)){

				$user_query = "SELECT * FROM SESSION_MEMBER WHERE S_ID ='".$session_variable."' AND U_ID='".odbc_result($user, 'L_ID')."'";
				$query = MSSQL::query($user_query);
				$breakdown = ACTIVITY::get_breakdown(odbc_result($user, 'L_ID'));
			?>
            <tr>
              <td><a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);?>/reporting_individual.php?id=<?php echo odbc_result($user, 'L_ID'); ?>"><?php echo odbc_result($user, 'L_FNAME');?></a></td>
              <td><?php echo odbc_result($user, 'L_LNAME'); ?></td>
              <td><?php echo odbc_result($user, 'AL_PA'); ?> </td>
              <td><?php echo number_format((odbc_result($user, 'AL_TIME')/3600), 2, '.', ''); ?></td>
              <td><?php echo array_sum($breakdown['TOTALS']['DISTANCE']); ?></td>
            </tr>
            <?php
			  }
			?>
            </table>
          </ol>
        </div>
      <!-- End Group Members -->
      </div>

      <div class="row">
        <div class="large-12 columns">
          <hr />
          <a href= "http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);?>/session/sessions.php?session=<?php echo $session_variable; ?>" class="button tiny">Back to Session Page</a>
        </div>
      </div>

      <div class="row">


    <!-- End Report Body -->




    <!-- End Main Content -->

  </div>

	<script src="https://code.jquery.com/jquery-1.12.1.min.js"></script>
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		<script src="js/foundation.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.1/b-flash-1.2.1/b-html5-1.2.1/b-print-1.2.1/r-2.1.0/datatables.min.js"></script>
		<script type="text/javascript" src="js/responsive-tables.js"></script>
		<script>
			$(document).foundation();

			//DATEPICKER
			$(function () {
				$("#datepicker").datepicker();
					$("#startDate").click(function() {
						$("#datepicker").datepicker("show");
					});
				$("#datepickerEnd").datepicker();
					$("#endDate").click(function() {
						$("#datepickerEnd").datepicker("show");
					});
			});
      $(document).ready(function () {
          $('table.display').DataTable({
              "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
              responsive: true,
              // stateSave: true,
              // "dom": 'T<"clear">lfrtip',
              dom: 'Bfrtip',
              buttons: [
                  'copy', 'excel', 'pdf'
              ]
          });
      });
	  } );
    </script>
  </body>
</html>
