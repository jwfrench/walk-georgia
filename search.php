<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/groups_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING)==1){
	SESSION::logout();
	REDIRECT::home();
}
//If the user is logged in
if($_SESSION['valid']){
	if(filter_input(INPUT_GET, 'uid', FILTER_SANITIZE_STRING) != null){
		$ID =filter_input(INPUT_GET, 'uid', FILTER_SANITIZE_STRING);
	}else{
		$ID= $_SESSION['ID'];
	}
	$info = ACCOUNT::get_info($ID);
}
if(filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING)){	
}else{
	REDIRECT::home();
}
HTML_ELEMENT::head('Search', $_SESSION['valid']);
HTML_ELEMENT::top_nav();

//START EDITING HERE
?>
<div style="margin-top:20px;"></div>
    
    <!-- Main Content -->
    
      <div class="row">
        <div class="large-12 columns center">
          <h1 class="global-h1">Search</h1>
        </div>
      </div>
      
    <!-- Search Function -->  
      <div class="row">
        <div class="large-12 columns">
          <h2 class="global-h1-small">Search For <?php echo filter_input(INPUT_GET, 'verbage', FILTER_SANITIZE_STRING);?>:</h2>
        </div>
      </div>
      <div class="row">
        <div class="large-12 columns">
          <form>
            <div class="row">
              <div class="large-6 columns">
                <div class="row collapse">
                  <div class="small-10 columns">
                    <input type="text" name="search" id="search" placeholder="<?php echo filter_input(INPUT_GET, 'placeholder', FILTER_SANITIZE_STRING); ?>">
                    <input name="table" id="table"  type="hidden" value="<?php echo filter_input(INPUT_GET, 'srch', FILTER_SANITIZE_STRING); ?>"/>
                	<input name="verbage" id="verbage"  type="hidden" value="<?php echo filter_input(INPUT_GET, 'verbage', FILTER_SANITIZE_STRING); ?>"/>
                	<input name="placeholder" id="placeholder"  type="hidden" value="<?php echo filter_input(INPUT_GET, 'placeholder', FILTER_SANITIZE_STRING); ?>"/>
                  </div>
                  <div class="small-2 columns">
                    <input class="button postfix" type="submit" value="Search">
                  </div>
                </div>
              </div>
            </div>
          </form >
        </div>
      </div>
      
    <!-- End Search Function -->
    <?php
		if(filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING)){
			switch (filter_input(INPUT_GET, 'table', FILTER_SANITIZE_STRING)){
				case 'LOGIN':
					$sql = 'SELECT * FROM '.filter_input(INPUT_GET, 'table', FILTER_SANITIZE_STRING).' WHERE ( (L_E LIKE \'%'.filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING).'%\') OR (L_FNAME LIKE \'%'.filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING).'%\') OR (L_LNAME LIKE \'%'.filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING).'%\')) ORDER BY L_LNAME';
					$execute = MSSQL::query($sql);
					?>
      <div class="row">
        <div class="large-12 columns">
        Search results:
        <?php
		$i=0;
		 while(odbc_fetch_row($execute)){
			$i++;
			$USER_ID = odbc_result($execute, 'L_ID');
			$FLAGS = explode(':', odbc_result($execute, 'L_FLAGS'));
			$privacy = $FLAGS[2];
			$info = ACCOUNT::get_info($USER_ID);
			if($info['PRIVACY']){
			?>
        <!-- User -->
          <div class="group-unit">
            <div class="row">
              <div class="large-8 medium-7 small-12 columns">
              <?php
          		ACCOUNT::avatar_small($USER_ID);
		  	  ?>
                <h3 class="global-h3" style="padding-top:18px;"><?php echo $info['FNAME']." ".$info['LNAME'];?> | <?php echo $info['EMAIL'];?></h3>
              </div>
              <div class="large-4 medium-5 small-12 columns right" style="padding-top:15px; padding-left:20%; text-align:left;">
                <a href="index.php?uid=<?php echo $USER_ID; ?>" class="button tiny">Visit</a>
                </ul>
              </div>
            </div>
          </div>
        <!-- End User -->
        <?php 
			}
		 }
		 ?>
         </div>
      </div>
      <?php
				break;
				case 'GROUPS':
					$sql = 'SELECT * FROM '.filter_input(INPUT_GET, 'table', FILTER_SANITIZE_STRING).' WHERE ( (G_DESC LIKE \'%'.filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING).'%\') OR (G_NAME LIKE \'%'.filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING).'%\')) ORDER BY G_NAME';
					$execute = MSSQL::query($sql);
					?>
                    <div class="row">
        <div class="large-12 columns">
        Search results:
        <?php
		$i=0;
		 while(odbc_fetch_row($execute)){
			$i++;
			$NAME = odbc_result($execute, 'G_NAME');
			$DESC = odbc_result($execute, 'G_DESC');
			$VIS = odbc_result($execute, 'G_VIS');
			$ID = odbc_result($execute, 'G_ID');
			if(!$VIS){
			?>
        <!-- User -->
          <div class="group-unit">
            <div class="row">
              <div class="large-8 medium-7 small-12 columns">
             <?php
          if (file_exists(filter_input(INPUT_SERVER, 'DOCUMENT_ROOT', FILTER_SANITIZE_STRING)."/group/img/avatar/".$ID.".png")) {
			  $filename = "/group/img/avatar/".$ID.".png";
		  } else {
    			$filename = "../img/default-group-profile.png";
		  }
		  if('U.'.$_SESSION['ID'] == $info['G_PID'] || $isAdmin){
		  ?>
            <!-- Avatar -->
            <?php
		  }
			?>
            	<div class="avatar-small" style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat; background-color:#008cba;"></div>
                <h3 class="global-h3" style="padding-top:18px;"><?php echo $NAME;?> | <?php echo $DESC;?></h3>
              </div>
              <div class="large-4 medium-5 small-12 columns right" style="padding-top:15px; padding-left:20%; text-align:left;">
                <a href="group/groups.php?group=<?php echo $ID; ?>" class="button tiny">Visit</a>
                </ul>
              </div>
            </div>
          </div>
        <!-- End User -->
        <?php 
			}
		 }
		 ?>
         </div>
      </div>
                    <?php
				break;
				case 'SESSION':
					echo $sql = 'SELECT * FROM '.filter_input(INPUT_GET, 'table', FILTER_SANITIZE_STRING).' WHERE ( (S_DESC LIKE \'%'.filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING).'%\') OR (S_NAME LIKE \'%'.filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING).'%\')) ORDER BY S_NAME';
					$execute = MSSQL::query($sql);
				break;
			}
		}
		?>
    <!-- User List -->
    
    
    
    <!-- End User List -->
    
    <!-- End Main Content -->
    <hr />
    
<?php
//END EDITING HERE

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/foundation/foundation.js"></script>
    <script src="js/foundation/foundation.topbar.js"></script>
    <script src="js/foundation/foundation.reveal.js"></script>
    <script src="js/foundation/foundation.tooltip.js"></script>
    <script src="js/foundation/foundation.offcanvas.js"></script>
    <script src="js/foundation/foundation.equalizer.js"></script>
    <script src="js/foundation/foundation.dropdown.js"></script>
    <script src="js/foundation/foundation.alert.js"></script>
    <script type="text/javascript" src="js/frontend.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>