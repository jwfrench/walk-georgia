<?php
include_once("lib/template_api.php");
include_once ("lib/back_api.php");
$ss = SESSION::secure_session();
if(isset($_GET['logout'])){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

<div id="main">

  <!-- Global Header Container -->
  <div id="top nojava">
  
    <!-- Header Image Container -->
    <div id="slideshow">
      
      <!-- Page Title Container -->
      <div id="slideshow-title">
        <div class="row nojava center">
          <h2 class="custom-font-big-white">official map test page</h2> 
          <br /><br /><br />
      
          <!-- Header Buttons
          <div class="large-12 columns expand">
            <div class="row center nojava">
              <div class="small-12 medium-4 large-4 columns">
                <a href="#ourStory" class="button small expand round">Our Story</a>
              </div>
              <div class="small-12 medium-4 large-4 columns">
                <a href="faq.php" class="button small success expand round">F.A.Q.</a>
              </div>
              <div class="small-12 medium-4 large-4 columns">
                <a href="resources/index.php" class="button small expand round secondary">Resources</a>
              </div>
            </div>
          </div>
          -->
              
        </div>
      </div>
      <!-- End Page Title Container --> 
       
    </div>
    <!-- End Header Image Container -->
    
  </div>
  <!-- End Global Header Container -->
    
    <!-- Main Content -->
    <a name="top"></a>
    
    <div class="row" style="margin-top:2em;">
    	<div class="large-12 columns">
        	<a href="#" class="button success expand" style="margin-bottom:0px;" data-reveal-id="myModal" data-reveal>Log an Activity</a>
        </div>
    </div>
    
    <!-- Share to Facebook Landing Modal -->
    <div id="facebookShare" class="reveal-modal tiny" data-reveal aria-labelledby="facebookShare" aria-hidden="true" role="dialog">
  
    	<div class="row center">
        	<div class="large-12 columns">
            	<h3 class="custom-font-small-blue">You Logged:</h3>
                <h3 class="custom-font-big-blue" style="margin-top:0;">110</h3>
                <h4 class="global-h2" style="margin-top:-1.5em;">Points</h4>
                
                <hr />
                <a href="http://www.walkgeorgia.org" class="button small round secondary">Nice! Take Me Back to My Activity</a>
                <hr style="margin-top:-.3em;" />
                
            </div>
        </div>
        
        <div class="row center">
        	<div class="large-12 columns">
            	<div class="fb-share-button" data-href="http://www.walkgeorgia.com/share.php?name=Aaron&points=110&activiy=Walking" data-layout="link"></div>
            </div>
        </div>
    </div>
    <!-- End Share to Facebook Landing Modal -->
    
    <!-- Share to Facebook Preferences Modal -->
  
    	<div class="row center">
        	<div class="large-12 columns">
            	<h3 class="custom-font-small-blue">Sharing Preferences:</h3>
                <hr />
                
                <a href="#" class="button small radius expand">Only Share to Facebook This Time</a>
                <a href="user-account.php" class="button small radius success expand">Share to Facebook Every Time I Log Activity</a> 
                
            </div>
        </div>
    </div>
    <!-- End Share to Facebook Preferences Modal -->
    
    
    
</div>
        
<div id="myModal" class="reveal-modal" data-reveal>
            <a class="close-reveal-modal">&#215;</a>
              <div class="row">
                <div class="large-12 columns">
                <h2 class="custom-font-small-blue">Log an Activity</h2>
                <hr />
                </div>
              </div>
              <form id="modal-form" method="post" data-abide>
              	<input type="hidden" id="points" name="points" value="26" />
                <div class="row">
                  <div class="large-6 medium-12 columns">
                    <h3 class="global-h2">Date of Activity:</h3>
                	<div class="row date">
                  	  
     
           <!-- Month -->
           <?php
		    $day = date('d');
			$month = date('m');
			$year = date('Y'); 
		   ?>
             <div class="large-4 medium-4 columns">
               <label>Month
                 <select id="month" name="month" required>
                   <option value="1" <?php if ($month=='1'){echo 'selected="selected"';} ?>>01 January</option>
                   <option value="2" <?php if ($month=='2'){echo 'selected="selected"';} ?>>02 February</option>
                   <option value="3" <?php if ($month=='3'){echo 'selected="selected"';} ?>>03 March</option>
                   <option value="4" <?php if ($month=='4'){echo 'selected="selected"';} ?>>04 April</option>
                   <option value="5" <?php if ($month=='5'){echo 'selected="selected"';} ?>>05 May</option>
                   <option value="6" <?php if ($month=='6'){echo 'selected="selected"';} ?>>06 June</option>
                   <option value="7" <?php if ($month=='7'){echo 'selected="selected"';} ?>>07 July</option>
                   <option value="8" <?php if ($month=='8'){echo 'selected="selected"';} ?>>08 August</option>
                   <option value="9" <?php if ($month=='9'){echo 'selected="selected"';} ?>>09 September</option>
                   <option value="10" <?php if ($month=='10'){echo 'selected="selected"';} ?>>10 October</option>
                   <option value="11" <?php if ($month=='11'){echo 'selected="selected"';} ?>>11 November</option>
                   <option value="12" <?php if ($month=='12'){echo 'selected="selected"';} ?>>12 December</option>
                 </select>
               </label>
             </div>
           <!-- End Month -->
         
           <!-- Day -->
             <div class="large-4 medium-4 columns">
               <label>Day
                 <select id="day" name="day" required>
                   <option value="1" <?php if ($day=='1'){echo 'selected="selected"';} ?>>01</option>
                   <option value="2" <?php if ($day=='2'){echo 'selected="selected"';} ?>>02</option>
                   <option value="3" <?php if ($day=='3'){echo 'selected="selected"';} ?>>03</option>
                   <option value="4" <?php if ($day=='4'){echo 'selected="selected"';} ?>>04</option>
                   <option value="5" <?php if ($day=='5'){echo 'selected="selected"';} ?>>05</option>
                   <option value="6" <?php if ($day=='6'){echo 'selected="selected"';} ?>>06</option>
                   <option value="7" <?php if ($day=='7'){echo 'selected="selected"';} ?>>07</option>
                   <option value="8" <?php if ($day=='8'){echo 'selected="selected"';} ?>>08</option>
                   <option value="9" <?php if ($day=='9'){echo 'selected="selected"';} ?>>09</option>
                   <option value="10" <?php if ($day=='10'){echo 'selected="selected"';} ?>>10</option>
                   <option value="11" <?php if ($day=='11'){echo 'selected="selected"';} ?>>11</option>
                   <option value="12" <?php if ($day=='12'){echo 'selected="selected"';} ?>>12</option>
                   <option value="13" <?php if ($day=='13'){echo 'selected="selected"';} ?>>13</option>
                   <option value="14" <?php if ($day=='14'){echo 'selected="selected"';} ?>>14</option>
                   <option value="15" <?php if ($day=='15'){echo 'selected="selected"';} ?>>15</option>
                   <option value="16" <?php if ($day=='16'){echo 'selected="selected"';} ?>>16</option>
                   <option value="17" <?php if ($day=='17'){echo 'selected="selected"';} ?>>17</option>
                   <option value="18" <?php if ($day=='18'){echo 'selected="selected"';} ?>>18</option>
                   <option value="19" <?php if ($day=='19'){echo 'selected="selected"';} ?>>19</option>
                   <option value="20" <?php if ($day=='20'){echo 'selected="selected"';} ?>>20</option>
                   <option value="21" <?php if ($day=='21'){echo 'selected="selected"';} ?>>21</option>
                   <option value="22" <?php if ($day=='22'){echo 'selected="selected"';} ?>>22</option>
                   <option value="23" <?php if ($day=='23'){echo 'selected="selected"';} ?>>23</option>
                   <option value="24" <?php if ($day=='24'){echo 'selected="selected"';} ?>>24</option>
                   <option value="25" <?php if ($day=='25'){echo 'selected="selected"';} ?>>25</option>
                   <option value="26" <?php if ($day=='26'){echo 'selected="selected"';} ?>>26</option>
                   <option value="27" <?php if ($day=='27'){echo 'selected="selected"';} ?>>27</option>
                   <option value="28" <?php if ($day=='28'){echo 'selected="selected"';} ?>>28</option>
                   <option value="29" <?php if ($day=='29'){echo 'selected="selected"';} ?>>29</option>
                   <option value="30" <?php if ($day=='30'){echo 'selected="selected"';} ?>>30</option>
                   <option value="31" <?php if ($day=='31'){echo 'selected="selected"';} ?>>31</option>
                 </select>
               </label>
             </div>
           <!-- End Day -->
         
           <!-- Year -->
             <div class="large-4 medium-4 columns">
               <label>Year
                 <select id="year" name="year" required>
                   <option value="2008" <?php if ($year=='2008'){echo 'selected="selected"';} ?>>2008</option>
                   <option value="2009" <?php if ($year=='2009'){echo 'selected="selected"';} ?>>2009</option>
                   <option value="2010" <?php if ($year=='2010'){echo 'selected="selected"';} ?>>2010</option>
                   <option value="2011" <?php if ($year=='2011'){echo 'selected="selected"';} ?>>2011</option>
                   <option value="2012" <?php if ($year=='2012'){echo 'selected="selected"';} ?>>2012</option>
                   <option value="2013" <?php if ($year=='2013'){echo 'selected="selected"';} ?>>2013</option>
                   <option value="2014" <?php if ($year=='2014'){echo 'selected="selected"';} ?>>2014</option>
                   <option value="2015" <?php if ($year=='2015'){echo 'selected="selected"';} ?>>2015</option>
                 </select>
               </label>
             </div>
           <!-- End Year -->
           
                      
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="large-6 medium-6 columns">
                  <h3 class="global-h2">Activity:</h3>
                  <div class="row">
                    <div class="large-12 columns">
                    
                    <label>Activity:
                      <select id="exercise" name="srch" onchange="su_magic(this, '');" class="excercise" required>
                         <?php
						  $aid = odbc_result(MSSQL::query('SELECT TOP 1 AL_AID FROM LOG WHERE AL_UID =\''.$_SESSION['ID'].'\' ORDER BY AL_DATE DESC'), 'AL_AID');
				  	      $list = ACTIVITY::select_activity($aid);
				        ?>
                      </select>
                    </label>
                    
                    </div>
                    </div>
                    
                  </div>
                </div>
                <div class="row">
                  <div class="large-6 medium-6 columns">
                    <h3 class="global-h2">Time Spent Exercising:</h3>
                  </div>
                </div>
                <div class="row">
                  <div class="large-2 medium-2 columns">
                    <label>Minutes:
                      <input type="number" id="minutes" name ="minutes" min="1" max='480' required>
                    </label>
                  </div>
                  <div class="large-8 medium-8 columns"></div>
                </div>
                <div class="row">
                <div class="large-6 columns" id="su" name = "su"> 
                </div>
                </div>
                <div class="row">
                  <div class="large-6 medium-6 columns">
                    <h3 class="global-h2">Difficulty:</h3>
                    <label>How difficult was it to complete?
                      <select name="sd" id="sd" required>
                        <option value="1">Easy</option>
                        <option value="2">Somewhat difficult</option>
                        <option value="3">Difficult</option>
                        <option value="4">More difficult than usual</option>
                        <option value="5">Very difficult</option>
                      </select>
                    </label>
                  </div>
                </div>
                <div class="row">
                  <div class="large-12 columns">
                    <input type="submit"  id="submit" class="tiny button" value="Submit"/>                
                  </div>
                </div>
              </form>
              
            </div>
<?php

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
if(isset($_POST['points'])){
	?>
    <script type="text/javascript">
    $(document).ready(function(){
		$('#facebookShare').foundation('reveal', 'open')
	});
	</script>
    <?php
}
?>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script src="js/foundation/foundation.abide.js"></script>
    <script src="js/foundation/foundation.reveal.js"></script>
    <script src="js/foundation/foundation.tooltip.js"></script>
    <script src="js/foundation/foundation.offcanvas.js"></script>
    <script src="js/foundation/foundation.equalizer.js"></script>
    <script src="js/foundation/foundation.dropdown.js"></script>
    <script type="text/javascript" src="js/sha512.js"></script>
    <script type="text/javascript" src="js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>