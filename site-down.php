<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING)==1){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Test Page', $_SESSION['valid']);
HTML_ELEMENT::top_nav();

//EDITING STARTS HERE

?>
<div class="main">
  <div class="row">
    <div class="large-12 columns center">
      <img src="img/single-color-logo.png" alt="walk georgia" />
      <h1 class="custom-font-small">We're currently undergoing some light maintence.</h1>  
      <h2 class="global-h2">We'll have you back online in no time! Thanks for your patience while we take care of the nerdy stuff that makes this site work.</h2>
    </div>
  </div>
</div>

<?php

//EDITING ENDS HERE

HTML_ELEMENT::footer();
//JAVASCRIPTS HERE
?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../js/forms.js"></script>
    <script src="../js/sha512.js"></script>
    <script src="../js/foundation/foundation.js"></script>
    <script src="../js/foundation/foundation.topbar.js"></script>
    <script src="../js/foundation/foundation.abide.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>