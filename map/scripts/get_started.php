<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
//enter a record into the database for the user to start the game
$breakdown = ACTIVITY::get_breakdown($_SESSION['ID']);
    //get the user's current point total
    $points_total = '0';
    if(isset($breakdown['POINTS'])){
  	if(is_array($breakdown['POINTS'])){
  		$points_total = array_sum($breakdown['POINTS']);
  	}
  	if ($points_total < 1 || $points_total=='' || $points_total== NULL){
  		$points_total = '0';
  	}
  }
$info = ACCOUNT::get_info($_SESSION['ID']);
$sql = 'INSERT INTO MAP_DATA (M_UUID, M_STARTPOINTS, M_'.str_replace(' ', '', strtoupper($info['COUNTY'])).') VALUES (\''.$_SESSION['UUID'].'\', \''.$points_total.'\', 1)';
$query = MSSQL::query($sql);
REDIRECT::map('Have fun!');
?>