<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(isset($_POST['county']) && !empty($_POST['county'])) {
    $county_id = 'M_'.strtoupper(str_replace(' ', '', $_POST['county']));
    //grab the user's map data
    $mapdata = get_map_data();
    //detect whether or not the county has been unlocked
    $unlocked = odbc_result($mapdata, $county_id);
    //if it has been unlocked
    $data = get_county_data($unlocked);
    //then return the the quick facts to be written    
    echo json_encode($data);
}

function get_map_data(){
    //write the query to grab map data
    $sql = 'SELECT * FROM MAP_DATA WHERE M_UUID = \''.$_SESSION['UUID'].'\'';
    //execute that query
    $query = MSSQL::query($sql);
    //return results
    return $query;
}

function get_county_data($unlocked){
    //write the query to grab map facts
    $sql = 'SELECT * FROM MAP_FACTS WHERE name = \''.$_POST['county'].'\'';
    //execute that query
    $query = MSSQL::query($sql);
    //return results
    if($unlocked){
        $array = odbc_fetch_array($query);
        $array['population'] = number_format($array['population']);
    }else{
        $array = 
        array(
                "name"              =>  $_POST['county'],
                "population"        =>  "?",
                "county_seat"       =>  "?",
                "commodities"       =>  "?",
                "fun_facts"         =>  "?",
                "farmers_markets"   =>  "?",
                "parks_sites"       =>  "?"
            );
    }
    return $array;
}
?>