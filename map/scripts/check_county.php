<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(isset($_POST['county']) && !empty($_POST['county'])) {
    $county_id = 'M_'.strtoupper(str_replace(' ', '', $_POST['county']));
    //write the query to grab map data
    $sql = 'SELECT '.$county_id.', M_STARTPOINTS, M_SPENTPOINTS FROM MAP_DATA WHERE M_UUID = \''.$_SESSION['UUID'].'\'';
    //execute that query
    $query = MSSQL::query($sql);
    //gather variables necessary to calculate available points and return the unlocked status
    $unlocked = odbc_result($query, $county_id);
    $startpoints = odbc_result($query, 'M_STARTPOINTS');
    $spentpoints = odbc_result($query, 'M_SPENTPOINTS');
    $breakdown = ACTIVITY::get_breakdown($_SESSION['ID']);
    //get the user's current point total
    $points_total = '0';
    if(isset($breakdown['POINTS'])){
		if(is_array($breakdown['POINTS'])){
			$points_total = array_sum($breakdown['POINTS']);
		}
		if ($points_total < 1 || $points_total=='' || $points_total== NULL){
			$points_total = '0';
		}
	}
	//calculate how many points we have to spend
	$points_available= $points_total - $startpoints - $spentpoints;
	//echo an array with the two important values here: points available and whether the county is already unlocked or not
	$array = 
        array(
                "unlocked"  =>  $unlocked,
                "points"    =>  $points_available
            );
    //return results
    echo json_encode($array);
}
?>