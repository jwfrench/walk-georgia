<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(isset($_POST['county']) && !empty($_POST['county'])) {
    $county_id = 'M_'.strtoupper(str_replace(' ', '', $_POST['county']));
    //write the query to grab map data
    $sql = 'SELECT '.$county_id.' FROM MAP_DATA WHERE M_UUID = \''.$_SESSION['UUID'].'\'';
    //execute that query
    $query = MSSQL::query($sql);
    //echo the result
    $result = odbc_result($query, 'M_'.$_POST['county']);
    if($result == 1){
        //county is already unlocked, how did we even get here
        //echo 'unlock_county.php: county already unlocked';
        $response['message'] = "county already unlocked";
    }else{
        //unlock the county and add 65 points to points spent
        $sql = 'UPDATE MAP_DATA SET '.$county_id.' = \'1\', M_SPENTPOINTS = (M_SPENTPOINTS+65)  WHERE M_UUID = \''.$_SESSION['UUID'].'\'';
        $query = MSSQL::query($sql);
        //echo 'unlock_county.php: success';
        $response['message'] = "success";
    }
    $sql = 'SELECT * FROM MAP_DATA WHERE M_UUID = \''.$_SESSION['UUID'].'\'';
    $query = MSSQL::query($sql);
    $response['map_complete'] = 1;
    $response['county_count'] = 159;
    for($i=4; ($i) <= odbc_num_fields($query); $i++){
      if(odbc_result($query, $i) == 0) {
        $response['map_complete'] = 0;
        $response['county_count']--;
      }
    }
    echo json_encode($response);
}
?>
