<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
SESSION::logout();
REDIRECT::home();
}
if(!isset($_SESSION['valid'])){
	REDIRECT::login('You must be logged in to view that content.');
}
HTML_ELEMENT::head('My Map', $_SESSION['valid']);
HTML_ELEMENT::top_nav();
//START EDITING HERE
//function for first time visitors
//look up which counties a user has unlocked
$sql = 'SELECT * FROM MAP_DATA WHERE M_UUID = \''.$_SESSION['UUID'].'\'';
//execute that query
$query = MSSQL::query($sql);
if(odbc_num_rows($query) > 0){
  //get points to next county
  $spentpoints = odbc_result($query, 'M_SPENTPOINTS');
  $startpoints = odbc_result($query, 'M_STARTPOINTS');
  $breakdown = ACTIVITY::get_breakdown($_SESSION['ID']);
    //get the user's current point total
    $points_total = '0';
    if(isset($breakdown['POINTS'])){
  	if(is_array($breakdown['POINTS'])){
  		$points_total = array_sum($breakdown['POINTS']);
  	}
  	if ($points_total < 1 || $points_total=='' || $points_total== NULL){
  		$points_total = '0';
  	}
  }
  //calculate how many points we have to spend
  $points_available= ($points_total - $startpoints - $spentpoints);
  $counties_available = floor($points_available/65) ;
  $points_towards_unlock = ($points_available%65);
  //calculate the percentage of counties acquired
  $county_count=0;
  for($i=4; ($i) <= odbc_num_fields($query); $i++){
         if(odbc_result($query, $i) ==1){
           $county_count++;
         }
  }
  $percent = number_format($county_count/159*100, 0);
  $info = ACCOUNT::get_info($_SESSION['ID']);
  $county_info = MSSQL::query('SELECT * FROM MAP_FACTS WHERE name =\''.$info['COUNTY'].'\'');
}else{
  $info = ACCOUNT::get_info($_SESSION['ID']);
  $county_info = MSSQL::query('SELECT * FROM MAP_FACTS WHERE name =\''.$info['COUNTY'].'\'');
  $first_timer = 1;
  $points_available= 0;
  $counties_available = 0 ;
  $points_towards_unlock = 0;
  //calculate the percentage of counties acquired
  $county_count=0;
  $percent = 0;
}
?>

<!-- Map stylesheet -->
<link rel="stylesheet" property="stylesheet" href="style.css">
<script src="../bower_components/webcomponentsjs/webcomponents-lite.js"></script>
<link rel="import" href="../bower_components/emoji-rain/emoji-rain.html">
<emoji-rain id="rainmaker" use-twemoji></emoji-rain>


<div class="map_small">
<!-- Map Modal -->
<div id="map__modal" class="reveal-modal tiny center" data-reveal>
  <h2 class="font -secondary">Welcome to the Walk Georgia Map!</h2>
  <p class="lead">For every 65 points you gain by exercising, you can earn access to resources in one Georgia county. Earn 3-4 counties a week and you'll "Walk Georgia" in a year!<br><br> To begin earning counties, click “Start the Game!”, and then begin logging your activity on Walk Georgia. The interactive Map will not use your previously logged points toward counties.</p>
  <a class="button small round success" href="scripts/get_started.php">Start the Game!</a>
  <a class="button small round" href="../index.php">No, thanks. Take me back to My Activity.</a>
</div>
<!-- End Map Modal -->
<!-- Purchase Modal-->
<div id="certificate__modal" class="reveal-modal small center" data-reveal>
    <h2 class="font -secondary">Your Certificate</h2>
		<img class="pb3" src="img/certificate.png" alt="Certificate of Completion" />
    <a href="map-certificate-of-completion.php" class="button small round success download_certificate_trigger">Download</a>
    <a class="close-reveal-modal">&#215;</a>
</div>
<!-- Purchase Modal-->
<div id="purchase__modal" class="reveal-modal tiny center" data-reveal>
    <h2 class="font -secondary">Unlock <div class="county_name"></div> County</h2>
    <a class="button small round success unlock_trigger">Unlock!</a>
    <a class="close-reveal-modal">&#215;</a>
</div>
<!-- County Info Modal-->
<div id="county__modal" class="reveal-modal small" data-reveal>
    <h2 class="font -secondary center"><span class="county_name"></span> County</h2>
    <div class="center"><img class="county_image" src="img/clarke.jpg"></img></div>
    <div class="not_unlocked"></div>
    <br>
    <ul class="no-bullet">
          <li class="pop_wrap"><strong>Population</strong>: <span class="population"><?php echo number_format(odbc_result($county_info, 'population')); ?></span></li>
          <li class="seat_wrap"><strong>County Seat:</strong> <span class="county_seat"><?php echo odbc_result($county_info, 'county_seat'); ?></span></li>
          <li class="com_wrap"><strong>Commodities:</strong> <span class="commodities"><?php echo odbc_result($county_info, 'commodities'); ?></span></li>
          <li class="fm_wrap"><strong>Farmer's Markets:</strong> <span class="farmers_markets"><?php echo odbc_result($county_info, 'farmers_markets'); ?></span></li>
          <li class="sp_wrap"><strong>Nearby State Parks and Historic Sites:</strong> <span class="parks_sites"><?php echo odbc_result($county_info, 'parks_sites'); ?></span></li>
          <li class="sp_wrap"><strong>Fun Facts:</strong> <span class="fun_facts"><?php echo odbc_result($county_info, 'fun_facts'); ?></span></li>
        </ul>
    <a class="close-reveal-modal">&#215;</a>
</div>
<!-- End Purchase Modal -->
<!-- Reset Counties Modal -->
<div id="reset_counties_modal" class="reveal-modal small center" data-reveal>
  <h2 class="font -secondary">Reset Counties?</h2>
    <a class="button small round alert reset_trigger" onclick="reset_counties()">Reset!</a>
    <a class="button small round" onclick="$('#reset_counties_modal').foundation('reveal', 'close');">Cancel</a>
    <a class="close-reveal-modal">&#215;</a>
</div>
<!-- End Reset Counties Modal -->
<!-- Page Header -->
<section class="header bg_map bg--blue" style="margin-top: -10px;">
      <div class="row">
        <div class="large-12 columns">
          <h1 class="font -secondary -big -white">My Walk Georgia Map</h1>
          <hr>
          <p class="font -white show-for-medium-up">We're here to help you Walk Georgia! For every 65 points you earn exercising, you can gain access to resources and fun facts in a new Georgia county. Earn 3-4 counties each week and you'll "Walk Georgia" in a year!</p>
        </div>
      </div>
      <div class="row">
        <div class="large-6 columns">
          <a class="font -white" href="http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/faq.php#I1" target="_blank">Need Help?</a>
        </div>
      </div>
</section>
<!-- End Page Header -->
<!-- alert box -->
<div id="map_complete_alert" data-alert="" class="alert-box success" style="<?php if($county_count >= 159) {} else {echo("display:none;"); } ?> margin-bottom:0px; text-align: center;">
  <p class="f3 pt3 mb1">
    Congratulations! You have completed the Map!
  </p>

  <p>
  <a href="javascript:open_certificate();" class="claim-certificate-btn font -white -bold"><span style="font-weight:100;">⭐</span> Click here to view your certificate<span style="font-weight:100;">⭐</span></a><a class="close">×</a></div>
  </p>
<!-- end alert box -->
<!-- Page Main -->
<section class="main_map">
<div class="row">
  <!-- The Map -->
  <div id="main_map_section" class="large-6 medium-5 columns">
    <div class="show-for-small-only font -primary center" style="padding-bottom: 1em; font-weight: bold;">Tap on any county to unlock it!</div>
    <div class="show-for-medium-up font -primary" style="padding-bottom: 1em; padding-top: .8em; font-weight: bold;">Click on any county to unlock it!</div>
    <!-- Don't Change This -->
    <img class="svg" src="img/map.svg"/>
    <!-- End Don't Change This -->

  </div>
  <!-- End Map -->
  <!-- Map Stats -->
  <div class="quick-stats">
    <div class="large-6 medium-7 columns">
      <div class="county-facts" id="county-facts" name ="county-facts">
        <h2 class="font -medium -secondary" id="county-name" name="county-name"><span class="county_name"> <?php echo odbc_result($county_info, 'name'); ?></span> County Quick Facts</h3>

        <ul class="show-for-medium-up quick_facts--small">
          <li class="pop_wrap"><strong>Population</strong>: <span class="population"><?php echo number_format(odbc_result($county_info, 'population')); ?></span></li>
          <li class="seat_wrap"><strong>County Seat:</strong> <span class="county_seat"><?php echo odbc_result($county_info, 'county_seat'); ?></span></li>
          <li class="com_wrap"><strong>Commodities:</strong> <span class="commodities"><?php echo odbc_result($county_info, 'commodities'); ?></span></li>
          <li class="fm_wrap"><strong>Farmer's Markets:</strong> <span class="farmers_markets"><?php echo odbc_result($county_info, 'farmers_markets'); ?></span></li>
          <li class="sp_wrap"><strong>Nearby State Parks and Historic Sites:</strong> <span class="parks_sites"><?php echo odbc_result($county_info, 'parks_sites'); ?></span></li>
        </ul>
        <div class="unlock_button" style="display:none;"><a class="button small round success">Unlock this county!</a></div>
      </div>
      <hr>
			<?php
			// if they can unlock the entire map
			if($counties_available >= (159 - $county_count)) { ?>
				<div class="map_complete" <?php if($county_count == 159) {} else {echo('style="display:none;"'); } ?>>
					<h2 class="font -medium -secondary -blue">Map Rewards</h2>
					<!--Emoji Rain! -->
		        <script>
		        var rainmaker = document.getElementById('rainmaker');
						var main_map_section = document.getElementById('main_map_section');
		        function rain(button) {
		          if (rainmaker.active) {
								main_map_section.style.zIndex='0';
		            rainmaker.stop();
		            button.innerHTML = "Emoji Rain 🎉";
		          } else {
								main_map_section.style.zIndex='-2';
		            rainmaker.start();
		            button.innerHTML = "Stop Rain ☔️";
		          }
		        }
		        </script>
		      <a class="button medium emoji-rain-btn" onclick="rain(this)">Emoji Rain 🎉</a>
					<a href="certificate-of-completion.pdf" class="button medium download-certificate-btn" download="Walk Georgia Map Game Certificate of Completion">Download My Certificate 🌟</a>
	        <!--End Emoji Rain! -->
				</div>
			<?php }
			if($county_count == 159) {} else { ?>
	      <?php if($counties_available >0){
	        ?>
	        <!-- Counties ready to purchase -->
	        <div class="progress_status">
	          <div class="quick-stats__block quick-stats__block--peach">
	            <h2 class="font -standard -secondary -uppercase -white">Counties Ready to Unlock:</h2>
	            <h2 class="font -big -secondary -white"><span class="county_credits"><?php if($counties_available > (159 - $county_count)) {echo (159 - $county_count);} else {echo $counties_available; } ?></span></h2>
	            <h2 class="font -standard -secondary -uppercase -white show-for-medium-up" style="margin-top:-20px;">Counties</h2>
	          </div>
	        </div>
	        <!-- End Counties ready to purchase -->
	        <!-- Progress towards next county -->
	        <div class="points_status" style="display:none;">
	          <h2 class="font -medium -secondary -blue"><?php echo (65-$points_towards_unlock); ?> Points Away from the Next County</h2>
	          <span class="county_credits" style="display:none;">0</span>
	          <div class="progress small success round">
	            <span class="meter county_individual_percent" style="width: <?php echo number_format(($points_towards_unlock)/65*100); ?>%;"><?php echo number_format($points_towards_unlock/65*100); ?>%</span>
	          </div>
	        </div>
	        <!-- End Progress towards next county -->
	        <?php
	      }else{
	        ?>
	        <!-- Counties ready to purchase -->
	        <div class="progress-status" style="display:none;">
	          <div class="quick-stats__block quick-stats__block--peach">
	            <h2 class="font -standard -secondary -uppercase -white">Counties Ready to Unlock:</h2>
	            <h2 class="font -big -secondary -white"><span class="county_credits"><?php if($counties_available > (159 - $county_count)) {echo (159 - $county_count);} else {echo $counties_available; } ?></span></h2>
	            <h2 class="font -standard -secondary -uppercase -white show-for-medium-up" style="margin-top:-20px;">Credits</h2>
	          </div>
	        </div>
	        <!-- End Counties ready to purchase -->
	        <!-- Progress towards next county -->
	        <div class="points_status">
	          <h2 class="font -medium -secondary -blue"><?php echo (65-$points_towards_unlock); ?> Points Away from the Next County</h2>
	          <span class="county_credits" style="display:none;">0</span>
	          <div class="progress small success round">
	            <span class="meter county_individual_percent" style="width: <?php echo number_format(($points_towards_unlock)/65*100); ?>%;"><?php echo number_format($points_towards_unlock/65*100); ?>%</span>
	          </div>
	        </div>
	        <!-- End Progress towards next county -->
	        <?php
	      }
			}
	  	?>
      <hr>
      <!-- Progress Bar -->
      <h2 class="font -medium -secondary -blue ">Map Progress (<span class="county_progress"><?php echo $county_count; ?></span>/159)</h2>
      <div class="progress small success round">
        <span class="meter county_percent" style="width: <?php echo $percent; ?>%;"><?php echo $percent; ?>%</span>
      </div>
      <!-- End Progress Bar -->
      <hr class="show-for-small-only">
    </div>
  </div>
</div>
<!-- End Map Stats -->
</section>
<!-- End of the Main Section -->

<!-- unlocked Counties -->
<section class="unlocked-counties">
<div class="row">
  <div class="large-12 columns">
    <h2 class="font -medium -secondary -blue">Unlocked Counties:</h2>
    <hr />
    <p class="font -primary show-for-medium-up">Below are the counties you've earned! Click on any county name to get its complete list of county facts, resources, and nearby destinations.</p>
    <ul class="font -primary no-bullet county_list large-block-grid-4 small-block-grid-2 center">
      <?php
        if(odbc_num_rows($query) < 1){
            //if the no counties are unlocked
            ?><li>You haven't unlocked any counties yet!</li><?php
        }else{
            //else print each unlocked county
            for($i=4; ($i) <= odbc_num_fields($query); $i++){
               if(odbc_result($query, $i) ==1){
                 $name = ucfirst(strtolower(str_replace( 'M_', '', odbc_field_name($query, $i))));
                 $id = '';
                 ?>
            <li><a onclick="rewrite(<?php echo "'".$name."'"; ?>);"><img src="img/thumbs/<?php echo $name; ?>.jpg" class="avatar-favorite"><br><?php echo $name; ?></a></li><?php
               }
            }

        }
      ?>
    </ul>
    <hr>
    <p><a href="#" data-reveal-id="reset_counties_modal">Need to reset your counties?</a></p>
  </div>
</div>

</div>
</section>
<!-- End unlocked Counties -->
</div>

<?php

//END EDITING HERE

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
<!-- End Footer -->
  <script>
    var $unlock_button = $(".unlock_button");
    var $county_name = $(".county_name");
    var $county_list = $(".county_list");
    var $county_percent = $(".county_percent");
    var $county_progress = $(".county_progress");
    var $county_credits = $(".county_credits");
    var $population = $(".population");
    var $pop_wrap = $(".pop_wrap");
    var $county_seat = $(".county_seat");
    var $seat_wrap = $(".seat_wrap");
    var $commodities = $(".commodities");
    var $com_wrap = $(".com_wrap");
    var $parks_sites = $(".parks_sites");
    var $sp_wrap = $(".sp_wrap");
    var $fun_facts = $(".fun_facts");
    var $farmers_markets = $(".farmers_markets");
    var $fm_wrap = $(".fm_wrap");
    var $not_unlocked = $(".not_unlocked");
    var $unlock_trigger = $(".unlock_trigger");
    var $county_image = $(".county_image");
    var $progress_status = $(".progress_status");
    var $points_status = $(".points_status");
		var $map_complete = $('.map_complete');
		var $emoji_rain_btn = $('.emoji-rain-btn');
		var $map_complete_alert = $('#map_complete_alert');

    $(document).ready(function() {

      $.when(load_map()).done(load_counties);
      function load_counties(){
          $.ajax({ url: 'scripts/get_unlocked_counties.php',
            data: {user_id: '1'},
            type: 'post',
            dataType: 'json',
            success: function(output) {
              $.each(output, function(key, value){
                if(value == 1){
                  var id = key.replace("M_", "").toLowerCase();
                  //add exceptions for counties with spaces in them
                  if(id === 'jeffdavis'){
                      id='jeff-davis';
                  }
                  if(id === 'benhill'){
                      id='ben-hill';
                  }
                  //if a county is unlocked add the county_unlocked class
                  $("#"+id).attr('class', 'county_unlocked');
                  }else{
                    //do nothing, becuase the weird errors slow the map down
                  }
              })
            },
            error: function(output){
              console.log(output);
              console.log('Failure in get_unlocked_counties ajax call');
            }
        });
      }
      function load_map(){
        return $.Deferred(function() {
          var self = this;
          /*
           * Replace all SVG images with inline SVG
           */
          $('img.svg').each(function(){
            //cache everything first
              var $img = $(this);
              var imgID = $img.attr('id');
              var imgClass = $img.attr('class');
              var imgURL = $img.attr('src');

              $.get(imgURL, function(data) {
                  // Get the SVG tag, ignore the rest
                  var $svg = $(data).find('svg');

                  // Add replaced image's ID to the new SVG
                  if(typeof imgID !== 'undefined') {
                      $svg = $svg.attr('id', imgID);
                  }


                   // Add replaced image's classes to the new SVG
                  if(typeof imgClass !== 'undefined') {
                    //detect whether the county is unlocked or not
                    imgClass = imgClass +' replaced-svg';
                  }

                  // Remove any invalid XML tags as per http://validator.w3.org
                  $svg = $svg.removeAttr('xmlns:a');

                  // Replace image with new SVG
                  $img.replaceWith($svg);

                  self.resolve();
                  // Add an handler
                  $('path[name]').each(function() {
                    var $this_path = $(this);
                    var name = $this_path.attr("name");
                    var id = $this_path.attr("id");
                    //when you click a county
                    $this_path.click(function() {
                      var name =$this_path.attr('name');
                      var id =$this_path.attr('id');
                      //first make the unlock / fun facts button visible
                      $unlock_button.attr('style', 'display:visible;');
                      $unlock_button.children('a').attr('class', 'button small round success');

                      //then clear all nonpurchased counties of classes
                      $('.county_selected.county_unlocked').attr('class', 'county_unlocked');
                      $('.county_selected').attr('class', '');

                      if($this_path.attr('class') == 'county_unlocked'){
                         $this_path.attr('class', 'county_selected county_unlocked');
                      }else{
                        $this_path.attr('class', 'county_selected');
                      }

                      //Rewrite the quick facts div via an ajax call
                      $.ajax({ url: 'scripts/map_controller.php',
                             data: {county: name},
                             type: 'post',
                             dataType: 'json',
                             success: function(output) {
                                //rewrite any county_name divs with the clicked county's name
                                $county_name.html(output.name);
                                //add the facts list to the quick facts section
                                if(output.population){
                                    $pop_wrap.attr ('style', 'display: visible;');
                                    $population.html(output.population);
                                }else{
                                    $pop_wrap.attr ('style', 'display: none;');
                                }
                                if(output.county_seat){
                                    $seat_wrap.attr ('style', 'display: visible;');
                                    $county_seat.html(output.county_seat);
                                }else{
                                    $seat_wrap.attr ('style', 'display: none;');
                                }
                                if(output.commodities){
                                    $com_wrap.attr ('style', 'display: visible;');
                                    $commodities.html(output.commodities);
                                }else{
                                    $com_wrap.attr ('style', 'display: none;');
                                }
                                if(output.parks_sites){
                                    $sp_wrap.attr ('style', 'display: visible;');
                                    $parks_sites.html(output.parks_sites);
                                }else{
                                    $sp_wrap.attr ('style', 'display: none;');
                                }
                                if(output.farmers_markets){
                                    $fm_wrap.attr ('style', 'display: visible;');
                                    $farmers_markets.html(output.farmers_markets);
                                }else{
                                    $fm_wrap.attr ('style', 'display: none;');
                                }
                                if(output.fun_facts){
                                    $sp_wrap.attr ('style', 'display: visible;');
                                    $fun_facts.html(output.fun_facts);
                                }else{
                                    $sp_wrap.attr ('style', 'display: none;');
                                }
                                $not_unlocked.html('');
                                $unlock_button.children('a').html('More Facts...').attr('onclick', 'county__modal()');
                                $unlock_button.children ('a').attr ('style', 'display: visible;');
                                $county_image.attr('src','img/'+id+'.jpg');
                                if(output.population == '?'){
                                  $not_unlocked.html(output.name+' County is waiting to be unlocked!');
                                  $unlock_button.children('a').html('Unlock this County!').attr('onclick', 'purchase__modal()').attr('style', 'display: visible;');
                                  $unlock_button.children('a').attr('style', 'display: visible;');
                                  $unlock_trigger.attr('onclick', 'unlock_county(\''+id+'\',\''+name+'\');');
                                }
                              },
                              error: function(output){
                                console.log(output);
                                console.log('Failure in map_controller ajax call');
                              }
                      });
                    });
                 });
              });

          });
        });
      }
    });
    //when the unlock button is clicked
    function unlock_county(id, name) {
      //close the reval modal first
      $('#purchase__modal').foundation('reveal','close');
      //Check the county to see if it is unlockable or not
      $.ajax({
        url: 'scripts/check_county.php',
        data: {county: name},
        type: 'post',
        dataType: 'json',
        success: function(output) {
          //If the county has not been unlocked
          if(output.unlocked == 0){
            //If the points are available
            if(output.points >= 65){
              $("#"+id).attr('class', 'county_unlocked');
              //rewrite the points available to be 65 less than they were
              output.points = output.points - 65;
              //make an ajax call to update the database
              $.ajax({
                url: 'scripts/unlock_county.php',
                data: {county: name},
                type: 'post',
                dataType: 'json',
                success: function(output) {
                  //if successful, re-write the percentage bar, re-write the county info, and decrease counties available by 1
                  var counties_purchased = parseFloat($county_progress.html());
                  var county_credits = parseFloat($county_credits.html());
                  county_credits--;
                  counties_purchased++;
									var percent = ((counties_purchased/159)*100).toFixed(0);
                  $county_percent.html(percent+'%').attr('style', 'width:'+percent+'%;');
                  $county_progress.html(counties_purchased);
                  $county_credits.html(county_credits);
									if(output.map_complete) {
										$progress_status.attr('style', 'display:none;');
										$map_complete.attr('style', 'display:block;');
										document.getElementById('map_complete_alert').style.display="block";
									}
									else {
	                  if($county_credits.html()=='0'){
	                    $progress_status.attr('style', 'display:none;');
	                    $points_status.attr('style', 'display:visible;');
	                  }
									}
                  //Rewrite the quick facts div via an ajax call
                  $.ajax({ url: 'scripts/map_controller.php',
                         data: {county: name},
                         type: 'post',
                         dataType: 'json',
                         success: function(output) {
                            //rewrite any county_name divs with the clicked county's name
                            $county_name.html(output.name);
                            //add the facts list to the quick facts section
                            if(output.population){
                                $pop_wrap.attr ('style', 'display: visible;');
                                $population.html(output.population);
                            }else{
                                $pop_wrap.attr ('style', 'display: none;');
                            }
                            if(output.county_seat){
                                $seat_wrap.attr ('style', 'display: visible;');
                                $county_seat.html(output.county_seat);
                            }else{
                                $seat_wrap.attr ('style', 'display: none;');
                            }
                            if(output.commodities){
                                $com_wrap.attr ('style', 'display: visible;');
                                $commodities.html(output.commodities);
                            }else{
                                $com_wrap.attr ('style', 'display: none;');
                            }
                            if(output.parks_sites){
                                $sp_wrap.attr ('style', 'display: visible;');
                                $parks_sites.html(output.parks_sites);
                            }else{
                                $sp_wrap.attr ('style', 'display: none;');
                            }
                            if(output.farmers_markets){
                                $fm_wrap.attr ('style', 'display: visible;');
                                $farmers_markets.html(output.farmers_markets);
                            }else{
                                $fm_wrap.attr ('style', 'display: none;');
                            }
                            if(output.fun_facts){
                                $sp_wrap.attr ('style', 'display: visible;');
                                $fun_facts.html(output.fun_facts);
                            }else{
                                $sp_wrap.attr ('style', 'display: none;');
                            }
                            $not_unlocked.html('');
                            $unlock_button.children('a').html('More Facts...').attr('onclick', 'county__modal()').attr('style', 'display:visible');
                            $county_image.attr('src','img/'+id+'.jpg');
                            //
                            if(output.population == '?'){
                              $not_unlocked.html(output.name+' County is waiting to be unlocked!');
                              $unlock_button.attr('style', 'display:visible');
                              $unlock_button.children('a').html('Unlock this County!');
                              $unlock_button.children('a').attr('onclick', 'purchase__modal()');
                              $unlock_trigger.attr('onclick', 'unlock_county(\''+id+'\',\''+name+'\');');
                            }
                          },
                          error: function(output){
                            console.log(output);
                            console.log('Failure in unlock_county ajax call');
                          }
                  });
                  $county_list.append('<li><a onclick="rewrite(\''+name+'\');"><img src="img/thumbs/'+name+'.jpg" class="avatar-favorite"><br>'+name+'</a></li>');
                  console.log('County Unlocked');
                  //pull modal for county info down immediately
                  county__modal();
                },
                error: function(output){
                  //if not successful lets print that to the console to see why
                  console.log('Failure in unlock_county ajax call');
                }
              });
            }else{
              console.log('Failure in unlocking county');
              console.log('not enough points');
            }
          }else{
            console.log('Failure in unlocking county');
            console.log('already unlocked');
          }
        },
        error: function(output){
          console.log('Failure in check_county ajax call');
        }
      });
    }

//when the reset button is clicked
function reset_counties() {
    //close the reval modal first
    $('#reset_counties_modal').foundation('reveal','close');
    //make an ajax call to the reset_map.php file
    $.ajax({
        url: 'scripts/reset_map.php',
        data: {reset: 1},
        type: 'post',
        dataType: 'text',
        success:
            function(output) {
                console.log('Reset Successful');
                console.log(output);
            },
        error:
            function(output){
                //if error print to the console
                console.log('Failure in reset_map ajax call');
            }
    });
    location.reload(true);
}

    function purchase__modal() {
      if($county_credits.html() > 0 ){
        $('#purchase__modal').foundation('reveal','open');

      }else{
        $unlock_button.children('a').html('Not enough Points!');
        $unlock_button.children('a').attr('class', 'button small round alert');
      }
    }

		$(document).on('closed.fndtn.reveal', '#certificate__modal', function () {
		  close_certificate();
		});

		function open_certificate() {
			$('#certificate__modal').foundation('reveal','open');
			$emoji_rain_btn.trigger('click');
		}

		function close_certificate() {
			$emoji_rain_btn.trigger('click');
		}

    function county__modal() {
        setTimeout(function(){
            $('#county__modal').foundation('reveal','open');
        },500); // 1 seconds.
    }
    //when you click a county name from the list
    function rewrite(name) {
      var $this_button = $(this);
      var id = $('path[name='+name+']').attr('id');
      $('.county_selected.county_unlocked').attr('class', 'county_unlocked');
      $('.county_selected').attr('class', '');
      if($this_button.attr('class') == 'county_unlocked'){
         $this_button.attr('class', 'county_selected county_unlocked');
      }else{
        $this_button.attr('class', 'county_selected');
      }

      //Rewrite the quick facts div via an ajax call
      $.ajax({ url: 'scripts/map_controller.php',
        data: {county: name},
        type: 'post',
        dataType: 'json',
        success: function(output) {
          //rewrite any county_name divs with the clicked county's name
          $county_name.html(output.name);
          //add the facts list to the quick facts section
          if(output.population){
            $pop_wrap.attr ('style', 'display: visible;');
            $population.html(output.population);
          }else{
            $pop_wrap.attr ('style', 'display: none;');
          }
          if(output.county_seat){
            $seat_wrap.attr ('style', 'display: visible;');
            $county_seat.html(output.county_seat);
          }else{
            $seat_wrap.attr ('style', 'display: none;');
          }
          if(output.commodities){
            $com_wrap.attr ('style', 'display: visible;');
            $commodities.html(output.commodities);
          }else{
            $com_wrap.attr ('style', 'display: none;');
          }
          if(output.parks_sites){
            $sp_wrap.attr ('style', 'display: visible;');
            $parks_sites.html(output.parks_sites);
          }else{
            $sp_wrap.attr ('style', 'display: none;');
          }
          if(output.farmers_markets){
            $fm_wrap.attr ('style', 'display: visible;');
            $farmers_markets.html(output.farmers_markets);
          }else{
            $fm_wrap.attr ('style', 'display: none;');
          }
          if(output.fun_facts){
            $sp_wrap.attr ('style', 'display: visible;');
            $fun_facts.html(output.fun_facts);
          }else{
            $sp_wrap.attr ('style', 'display: none;');
          }
          $not_unlocked.html('');
          $unlock_button.attr('style', 'display:visible');
          $unlock_button.children('a').html('More Facts...');
          $unlock_button.children('a').attr('onclick', 'county__modal()');
          $county_image.attr('src','img/'+id+'.jpg');
          if(output.population == '?'){
            $not_unlocked.html(output.name+' County is waiting to be unlocked!');
            $unlock_button.attr('style', 'display:visible');
            $unlock_button.children('a').html('Unlock this County!');
            $unlock_button.children('a').attr('onclick', 'purchase__modal()');
            $unlock_trigger.attr('onclick', 'unlock_county(\''+id+'\',\''+name+'\');');
          }
        },
        error: function(output){
          console.log(output);
          console.log('Failure in map_controller ajax call');
        }
      });
      $('#county__modal').foundation('reveal','open');
    };


  </script>
  <?php
  if (isset($first_timer)){
    ?>
  <script type="text/javascript">
    $(function(){
      $('#map__modal').foundation('reveal', 'open')
    });
    $(document).foundation({
      'reveal': { close_on_background_click: false }
    })
  </script>
  <?php
  }
  ?>
</body>
</html>
