<?php
// We'll be outputting a PDF
header('Content-type: application/pdf');

// It will be called downloaded.pdf
header('Content-Disposition: attachment; filename="Walk Georgia Map Game Certificate of Completion.pdf"');

// The PDF source is in original.pdf
readfile('certificate-of-completion.pdf');
?>
