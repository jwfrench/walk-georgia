<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/back_api.php");
include_once ("$root/lib/groups_api.php");
//GET THE COUNTY INFO
$date_range = '';
$group_activity_date_range = '';
$counties='';
$day1 = '';
$day2 = '';
$month1 = '';
$month2 = '';
$year1 = '';
$year2 = '';
//GET THE DATE INFO
if(filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING) != null) {
  if(filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING) == null) {
    header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting.php?group='. filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).'&datepicker='.filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING).'&datepickerEnd='.urlencode(date("m/d/Y")).'');
  }
}
else if(filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING) != null) {
  if(filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING) == null) {
    header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting.php?group='. filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).'&datepicker=01/01/'.date("Y").'&datepickerEnd='.filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING).'');
  }
}
else if(filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING) != null) {
  if(filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING) == null) {
    header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting.php?group='. filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).'&datepickerMobile='.filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING).'&datepickerEndMobile='.urlencode(date("m/d/Y")));
  }
}
else if(filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING) != null) {
  if(filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING) == null) {
    header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting.php?group='. filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).'&datepickerMobile=01/01/'.date("Y").'&datepickerEndMobile='.filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING));
  }
}
if ((filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING) != null) && (filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING) != null)) {
  //day 1
  $date1 = explode('/', filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING));
  if(count($date1) != 3) {
    header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting.php?group='. filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).'&datepicker=01/01/'.date("Y").'&datepickerEnd='.urlencode(date("m/d/Y")));
  }
  $month1 = $date1[0];
  $day1 = $date1[1];
  $year1 = $date1[2];

  //day 2
  $date2 = explode('/', filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING));
  if(count($date2) != 3) {
    header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting.php?group='. filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).'&datepicker=01/01/'.date("Y").'&datepickerEnd='.urlencode(date("m/d/Y")));
  }
  $month2 = $date2[0];
  $day2 = $date2[1];
  $year2 = $date2[2];
  $date1= $year1.'-'.$month1.'-'.$day1;
  $date2= $year2.'-'.$month2.'-'.$day2;

  $date_range = 'AL_DATE >= \'' . $date1 . '\' AND AL_DATE <= \'' . $date2 . '\' AND';
  $group_activity_date_range = 'GA_DATE >= \'' . $date1 . '\' AND GA_DATE <= \'' . $date2 . '\' AND';
}
if ((filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING) != null) && (filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING) != null)) {
  //day 1
  $date1 = explode('-', filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING));
  if(count($date1) != 3) {
    header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting.php?group='. filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).'&datepickerMobile=01/01/'.date("Y").'&datepickerEndMobile='.urlencode(date("m/d/Y")));
  }
  $month1 = $date1[1];
  $day1 = $date1[2];
  $year1 = $date1[0];

  //day 2
  $date2 = explode('-', filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING));
  if(count($date2) != 3) {
    header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting.php?group='. filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).'&datepickerMobile=01/01/'.date("Y").'&datepickerEndMobile='.urlencode(date("m/d/Y")));
  }
  $month2 = $date2[1];
  $day2 = $date2[2];
  $year2 = $date2[0];
  $date1= $year1.'-'.$month1.'-'.$day1;
  $date2= $year2.'-'.$month2.'-'.$day2;

  $date_range = 'AL_DATE >= \'' . $date1 . '\' AND AL_DATE <= \'' . $date2 . '\' AND';
  $group_activity_date_range = 'GA_DATE >= \'' . $date1 . '\' AND GA_DATE <= \'' . $date2 . '\' AND';
}

//get the group info
$group_id = filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING);
if(empty($group_id)||!isset($group_id)){
  $gohome = REDIRECT::home('');
}else{
  if(is_numeric ($group_id) && GROUP::exists($group_id)){
    $info = GROUP::get_info($group_id);
  }else{
    $gohome = REDIRECT::home('');
    exit();
  }
}
//$group = GROUP::get_info($group_id);
$isAdmin = GROUP::isAdmin($group_id);
if($isAdmin){
  $gohome = REDIRECT::home('');
  exit();
}

// Get Group Info, Subgroup IDs, Member IDs, if Group Logging is turned on, get Total Group Points else use Members to calculate points.
$is_group_logging = false;
$breakdown_begin = microtime(true);
$group['info'] = GROUP::get_info($group_id);
$group['subgroups'] = GROUP::getSubs($group_id, array());
$group['members'] = GROUP::getAllMembers($group_id, array());
$breakdown_end = microtime(true);

// Only get group data from group created date. Make this for any new groups created after 7/19/2016
if($group_id > 153201) {
  $date1 =  date("m/d/Y", strtotime($group['info']['G_DATE_CREATED']));
  $date2 = date("m/d/Y");
  if(empty($date_range)) {
    $date_range = 'AL_DATE >= \'' . $date1 . '\' AND AL_DATE <= \'' . $date2 . '\' AND';;
  }
  if(empty($group_activity_date_range)) {
    $group_activity_date_range = 'GA_DATE >= \'' . $date1 . '\' AND GA_DATE <= \'' . $date2 . '\' AND';
  }
}

// Query to load all user data. Can sort by AL_PA == Activity Log, Points Added. So members with the most points are at the top.
$user_order = 'AL_PA DESC';
if(isset($_GET['user_order'])){
  $user_order = filter_input(INPUT_GET, 'user_order', FILTER_SANITIZE_STRING);
}
$all_users_begin = microtime(true);
$sql = 'SELECT L_FNAME, L_LNAME, L_COUNTY, L_ID, L_E FROM LOGIN WHERE L_ID IN (SELECT U_ID FROM GROUP_MEMBER WHERE G_ID= \''.$group_id.'\') GROUP BY L_FNAME, L_LNAME, L_COUNTY, L_ID, L_E';
$all_users_non_active = MSSQL::query($sql);
$count_users = odbc_num_rows($all_users_non_active);
$all_users = array();
$sql1 = 'SELECT L_ID, L_FNAME, L_LNAME, L_COUNTY, L_E, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\', SUM(AL_UNIT) AS \'AL_UNIT\' FROM LOG INNER JOIN LOGIN ON AL_UID = L_ID  WHERE  '.$date_range.' L_ID IN (SELECT U_ID FROM GROUP_MEMBER WHERE G_ID= \''.$group_id.'\') GROUP BY L_FNAME, L_LNAME, L_COUNTY, L_ID, L_E ORDER BY '.$user_order.';';
$user = MSSQL::query($sql1);

// For each row of users with activity, set their id, fname, lname, email, county, points, seconds, and miles. And then add that user to the All_users array.
$x_user_array_count = 0;
while(odbc_fetch_array($user)) {
  $this_array = array();
  $this_array['id'] = odbc_result($user, 'L_ID');
  $this_array['fname'] = odbc_result($user, 'L_FNAME');
  $this_array['lname'] = odbc_result($user, 'L_LNAME');
  $this_array['email'] = odbc_result($user, 'L_E');
  $this_array['county'] = odbc_result($user, 'L_COUNTY');
  $this_array['points'] = odbc_result($user, 'AL_PA');
  $this_array['seconds'] = odbc_result($user, 'AL_TIME');
  $this_array['miles'] = odbc_result($user, 'AL_unit');
  $all_users[$x_user_array_count] = $this_array;
  $all_users_key_value[$this_array['id']] = $x_user_array_count;
  $x_user_array_count++;
}
$all_users_length = count($all_users);
$all_users_end = microtime(true);

$subgroups = array();
$group['total_members'] = count($group['members']);
$group['active_members'] = array();
$group['activities'] = array();
$group['counties'] = array();
$group['points'] = 0;
$group['seconds'] = 0;
$group['miles'] = 0;
if($group['info']['G_COUNTY'] != null) {
  // Need to make sure $group['counties'] is not null so in_array() does not throw an error
  if(count($group['counties']) > 0) {
    if(!in_array($group['info']['G_COUNTY'], $group['counties'])) {
      $group['counties'][] = $group['info']['G_COUNTY'];
    }
  }
  else {
    $group['counties'][] = $group['info']['G_COUNTY'];
  }
}

$all_activity_types = array();
$sql2 = 'SELECT AL_AID, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\', SUM(AL_UNIT) AS \'AL_UNIT\' FROM dbo.LOG WHERE '.$date_range.' AL_UID IN (SELECT U_ID FROM GROUP_MEMBER WHERE G_ID = \''.$group_id.'\') GROUP BY AL_AID, AL_PA';
$all_activities = MSSQL::query($sql2);
while(odbc_fetch_array($all_activities)) {
  if(isset($all_activity_types[odbc_result($all_activities, 'AL_AID')])) {
    $all_activity_types[odbc_result($all_activities, 'AL_AID')]['seconds'] += odbc_result($all_activities, 'AL_TIME');
    $all_activity_types[odbc_result($all_activities, 'AL_AID')]['miles'] += odbc_result($all_activities, 'AL_UNIT');
    $all_activity_types[odbc_result($all_activities, 'AL_AID')]['points'] += odbc_result($all_activities, 'AL_PA');
  }
  else {
    $sql4 = "SELECT A_NAME, A_UNIT FROM ACTIVITY WHERE A_ID = '".odbc_result($all_activities, 'AL_AID')."'";
    $activity_information = MSSQL::query($sql4);
    $all_activity_types[odbc_result($all_activities, 'AL_AID')]['name'] = odbc_result($activity_information, 'A_NAME');
    $all_activity_types[odbc_result($all_activities, 'AL_AID')]['metric'] = odbc_result($activity_information, 'A_UNIT');
    $all_activity_types[odbc_result($all_activities, 'AL_AID')]['seconds'] = odbc_result($all_activities, 'AL_TIME');
    $all_activity_types[odbc_result($all_activities, 'AL_AID')]['miles'] = odbc_result($all_activities, 'AL_UNIT');
    $all_activity_types[odbc_result($all_activities, 'AL_AID')]['points'] = odbc_result($all_activities, 'AL_PA');
  }
  // Add to the total group points / time / distance (if the activity is distance based (metric == 1))
  $group['seconds'] += intval(odbc_result($all_activities, 'AL_TIME'));
  if($all_activity_types[odbc_result($all_activities, 'AL_AID')]['metric'] == 1 ) {
    $group['miles'] += intval(odbc_result($all_activities, 'AL_UNIT'));
  }
  $group['points'] += intval(odbc_result($all_activities, 'AL_PA'));
}

// Begin Group Log Reporting
if($group["info"]["G_GLOG"] == '1') {
  $is_group_logging = true;
  //$group['group_log_points'] = GROUP::getTotalGroupPoints($group_id, array());
  $sql2 = "SELECT GA_AID, GA_TIME, GA_UNIT, GA_PA, GA_DATE, GA_MEMBER_COUNT FROM GROUP_ACTIVITY WHERE ".$group_activity_date_range." GA_GID='".$group_id."'";
  //echo("156: ".$sql2."<BR>");
  $group_activities = MSSQL::query($sql2);
  while(odbc_fetch_array($group_activities)) {
    $this_array = array();
    $this_array['group'] = $group["info"]["G_NAME"];
    $this_array['group_id'] = $group_id;
    if(isset($all_activity_types[odbc_result($group_activities, 'GA_AID')])) {
      $this_array['activity'] = $all_activity_types[odbc_result($group_activities, 'GA_AID')]['name'];
    }
    else {
      $sql4 = "SELECT A_NAME, A_UNIT FROM ACTIVITY WHERE A_ID = '".odbc_result($group_activities, 'GA_AID')."'";
      $activity_information = MSSQL::query($sql4);
      $this_array['activity'] = odbc_result($activity_information, 'A_NAME');
      $all_activity_types[odbc_result($group_activities, 'GA_AID')]['name'] = odbc_result($activity_information, 'A_NAME');
      $all_activity_types[odbc_result($group_activities, 'GA_AID')]['metric'] = odbc_result($activity_information, 'A_UNIT');
    }
    $this_array['seconds'] = odbc_result($group_activities, 'GA_TIME');
    if($all_activity_types[odbc_result($group_activities, 'GA_AID')]['metric'] == 1) {
      $this_array['miles'] = odbc_result($group_activities, 'GA_UNIT');
    }
    else {
      $this_array['miles'] = 0;
    }
    $this_array['points'] = odbc_result($group_activities, 'GA_PA');
    $this_array['date'] = odbc_result($group_activities, 'GA_DATE');
    $this_array['members'] = odbc_result($group_activities, 'GA_MEMBER_COUNT');
    array_push($group['activities'], $this_array);
    $group['points'] += $this_array['points'];
    $group['seconds'] += $this_array['seconds'];
    $group['miles'] += $this_array['miles'];
  }
  $subgroup_breakdown_begin = microtime(true);
  $subgroups_length = count($group['subgroups']);
  for($i = 0; $i < $subgroups_length; $i++) {
    $subgroup_begin = microtime(true);
    $subgroups[$i]["info"] = GROUP::get_info($group['subgroups'][$i]);
    $subgroups[$i]["info"]["G_ID"] = $group['subgroups'][$i];
    $subgroups[$i]['points'] = 0;
    $subgroups[$i]['seconds'] = 0;
    $subgroups[$i]['miles'] = 0;

    // If group logging is turned on, points are equal to the group's log points.
    if($subgroups[$i]["info"]["G_GLOG"] == '1') {
      $is_group_logging = true;
      $subgroups[$i]['activities'] = array();
      $sql3 = "SELECT GA_AID, GA_TIME, GA_UNIT, GA_PA, GA_DATE, GA_MEMBER_COUNT FROM GROUP_ACTIVITY WHERE ".$group_activity_date_range." GA_GID = '".$subgroups[$i]["info"]["G_ID"]."'";
      //echo("339: ".$sql3."<BR>");
      $subgroup_activities = MSSQL::query($sql3);
      while(odbc_fetch_array($subgroup_activities)) {
        $this_array = array();
        $this_array['group'] = $subgroups[$i]["info"]["G_NAME"];
        $this_array['group_id'] = $subgroups[$i]["info"]["G_ID"];
        if(isset($all_activity_types[odbc_result($subgroup_activities, 'GA_AID')])) {
          $this_array['activity'] = $all_activity_types[odbc_result($subgroup_activities, 'GA_AID')]['name'];
        }
        else {
          $sql4 = "SELECT A_NAME, A_UNIT FROM ACTIVITY WHERE A_ID = '".odbc_result($subgroup_activities, 'GA_AID')."'";
          $activity_information = MSSQL::query($sql4);
          $this_array['activity'] = odbc_result($activity_information, 'A_NAME');
          $all_activity_types[odbc_result($subgroup_activities, 'GA_AID')]['name'] = odbc_result($activity_information, 'A_NAME');
          $all_activity_types[odbc_result($subgroup_activities, 'GA_AID')]['metric'] = odbc_result($activity_information, 'A_UNIT');
        }
        $this_array['seconds'] = odbc_result($subgroup_activities, 'GA_TIME');
        if($all_activity_types[odbc_result($subgroup_activities, 'GA_AID')]['metric'] == 1) {
          $this_array['miles'] = odbc_result($subgroup_activities, 'GA_UNIT');
        }
        else {
          $this_array['miles'] = 0;
        }
        $this_array['points'] = odbc_result($subgroup_activities, 'GA_PA');
        $this_array['date'] = odbc_result($subgroup_activities, 'GA_DATE');
        $this_array['members'] = odbc_result($subgroup_activities, 'GA_MEMBER_COUNT');
        array_push($subgroups[$i]['activities'], $this_array);
        array_push($group['activities'], $this_array);
        $subgroups[$i]['points'] += $this_array['points'];
        $subgroups[$i]['seconds'] += $this_array['seconds'];
        $subgroups[$i]['miles'] += $this_array['miles'];
        $group['points'] += $this_array['points'];
        $group['seconds'] += $this_array['seconds'];
        $group['miles'] += $this_array['miles'];
      }
    }
    else {
      $subgroups[$i]['member_ids'] = GROUP::getAllMembers($group['subgroups'][$i], array());
      // Declaring variables and counts.
      $subgroups[$i]['active_members'] = 0;
      $subgroups[$i]['total_members'] = count($subgroups[$i]['member_ids']);

      $all_users_key_begin = microtime(true);
      for($y = 0; $y < $subgroups[$i]['total_members']; $y++) {
        // If the member id is in here, they have an activity, if not then they are inactive.
        if(isset($all_users_key_value[$subgroups[$i]['member_ids'][$y]])) {
          $subgroups[$i]['active_members']++;
          // To get the user information. Get the $all_users array position by sending the current id of the subgroup member to the key_value array.
          $subgroups[$i]['points'] += intval($all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['points']);
          $subgroups[$i]['seconds'] += intval($all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['seconds']);
          $subgroups[$i]['miles'] += intval($all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['miles']);
          if($all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['county'] != null) {
            // Need to make sure $group['counties'] is not null so in_array() does not throw an error
            if(count($group['counties']) > 0) {
              if(!in_array($all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['county'], $group['counties'])) {
                $group['counties'][] = $all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['county'];
              }
            }
            else {
              $group['counties'][] = $all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['county'];
            }
          }

          // Unset the value to get the remaining users in the main group, not any subgroup, and add their points. After all subgroups run, $group['members'] will only contain the ids of those not in a subgroup
          if(($key = array_search($all_users_key_value[$subgroups[$i]['member_ids'][$y]], $group['members']) !== false)) {
            unset($group['members'][$key]);
          }
        }
      }
      $all_users_key_end = microtime(true);
      $subgroups[$i]['Points Time Taken'] = ($all_users_key_end - $all_users_key_begin);

      // Saving for later, although this method is a bottleneck. We can use the already loaded user's and their points rather than re-querying it for every subgroup.
      //$subgroups[$i]['Individual Points'] = GROUP::getTotalIndividualPoints($group['subgroups'][$i], array(), $subgroups[$i]['member_ids']);
    }
    $subgroup_end = microtime(true);
    $subgroups[$i]["Time Taken"] = ($subgroup_end - $subgroup_begin." seconds.");
  }
  $subgroup_breakdown_end = microtime(true);
}
// Begin Individual Reporting
else {
  // Declaring counts and variables. Lengths are ALWAYS to be defined outside of a for() loop otherwise it recounts the group every single loop.
  $subgroup_breakdown_begin = microtime(true);
  $subgroups_length = count($group['subgroups']);
  // If there are subgroups get subgroup info and points based off of members.
  if($subgroups_length) {
    // For each subgroup, get their info (Manually have to set the group id), and get their member ids,
    for($i = 0; $i < $subgroups_length; $i++) {
      $subgroup_begin = microtime(true);
      $subgroups[$i]["info"] = GROUP::get_info($group['subgroups'][$i]);
      $subgroups[$i]["info"]["G_ID"] = $group['subgroups'][$i];
      $subgroups[$i]['points'] = 0;
      $subgroups[$i]['seconds'] = 0;
      $subgroups[$i]['miles'] = 0;

      // If group logging is turned on, points are equal to the group's log points.
      if($subgroups[$i]["info"]["G_GLOG"] == '1') {
        $is_group_logging = true;
        $subgroups[$i]['activities'] = array();
        $sql3 = "SELECT GA_AID, GA_TIME, GA_UNIT, GA_PA, GA_DATE, GA_MEMBER_COUNT FROM GROUP_ACTIVITY WHERE ".$group_activity_date_range." GA_GID = '".$subgroups[$i]["info"]["G_ID"]."'";
        //echo("339: ".$sql3."<BR>");
        $subgroup_activities = MSSQL::query($sql3);
        while(odbc_fetch_array($subgroup_activities)) {
          $this_array = array();
          $this_array['group'] = $subgroups[$i]["info"]["G_NAME"];
          $this_array['group_id'] = $subgroups[$i]["info"]["G_ID"];
          if(isset($all_activity_types[odbc_result($subgroup_activities, 'GA_AID')])) {
            $this_array['activity'] = $all_activity_types[odbc_result($subgroup_activities, 'GA_AID')]['name'];
          }
          else {
            $sql4 = "SELECT A_NAME, A_UNIT FROM ACTIVITY WHERE A_ID = '".odbc_result($subgroup_activities, 'GA_AID')."'";
            $activity_information = MSSQL::query($sql4);
            $this_array['activity'] = odbc_result($activity_information, 'A_NAME');
            $all_activity_types[odbc_result($subgroup_activities, 'GA_AID')]['name'] = odbc_result($activity_information, 'A_NAME');
            $all_activity_types[odbc_result($subgroup_activities, 'GA_AID')]['metric'] = odbc_result($activity_information, 'A_UNIT');
          }
          $this_array['seconds'] = odbc_result($subgroup_activities, 'GA_TIME');
          if($all_activity_types[odbc_result($subgroup_activities, 'GA_AID')]['metric'] == 1) {
            $this_array['miles'] = odbc_result($subgroup_activities, 'GA_UNIT');
          }
          else {
            $this_array['miles'] = 0;
          }
          $this_array['points'] = odbc_result($subgroup_activities, 'GA_PA');
          $this_array['date'] = odbc_result($subgroup_activities, 'GA_DATE');
          $this_array['members'] = odbc_result($subgroup_activities, 'GA_MEMBER_COUNT');
          array_push($subgroups[$i]['activities'], $this_array);
          array_push($group['activities'], $this_array);
          $subgroups[$i]['points'] += $this_array['points'];
          $subgroups[$i]['seconds'] += $this_array['seconds'];
          $subgroups[$i]['miles'] += $this_array['miles'];
          $group['points'] += $this_array['points'];
          $group['seconds'] += $this_array['seconds'];
          $group['miles'] += $this_array['miles'];
        }
      }
      else {
        $subgroups[$i]['member_ids'] = GROUP::getAllMembers($group['subgroups'][$i], array());
        // Declaring variables and counts.
        $subgroups[$i]['active_members'] = 0;
        $subgroups[$i]['total_members'] = count($subgroups[$i]['member_ids']);

        $all_users_key_begin = microtime(true);
        for($y = 0; $y < $subgroups[$i]['total_members']; $y++) {
          // If the member id is in here, they have an activity, if not then they are inactive.
          if(isset($all_users_key_value[$subgroups[$i]['member_ids'][$y]])) {
            $subgroups[$i]['active_members']++;
            // To get the user information. Get the $all_users array position by sending the current id of the subgroup member to the key_value array.
            $subgroups[$i]['points'] += intval($all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['points']);
            $subgroups[$i]['seconds'] += intval($all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['seconds']);
            $subgroups[$i]['miles'] += intval($all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['miles']);
            if($all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['county'] != null) {
              // Need to make sure $group['counties'] is not null so in_array() does not throw an error
              if(count($group['counties']) > 0) {
                if(!in_array($all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['county'], $group['counties'])) {
                  $group['counties'][] = $all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['county'];
                }
              }
              else {
                $group['counties'][] = $all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['county'];
              }
            }

            // Unset the value to get the remaining users in the main group, not any subgroup, and add their points. After all subgroups run, $group['members'] will only contain the ids of those not in a subgroup
            if(($key = array_search($all_users_key_value[$subgroups[$i]['member_ids'][$y]], $group['members']) !== false)) {
              unset($group['members'][$key]);
            }
          }
        }
        $all_users_key_end = microtime(true);
        $subgroups[$i]['Points Time Taken'] = ($all_users_key_end - $all_users_key_begin);

        // Saving for later, although this method is a bottleneck. We can use the already loaded user's and their points rather than re-querying it for every subgroup.
        //$subgroups[$i]['Individual Points'] = GROUP::getTotalIndividualPoints($group['subgroups'][$i], array(), $subgroups[$i]['member_ids']);
      }
      $subgroup_end = microtime(true);
      $subgroups[$i]["Time Taken"] = ($subgroup_end - $subgroup_begin." seconds.");
    }
  }
  else {
    // If no subgroups
  }
  $subgroup_breakdown_end = microtime(true);
} // END Individual Reporting

foreach($all_users_key_value as $key => $val) {
  $group['active_members'][] = $all_users[$val];

  // Note: This means counties are only active counties.
  if($all_users[$val]['county'] != null) {
    // Need to make sure $group['counties'] is not null so in_array() does not throw an error
    if(count($group['counties']) > 0) {
      if(!in_array($all_users[$val]['county'], $group['counties'])) {
        $group['counties'][] = $all_users[$val]['county'];
      }
    }
    else {
      $group['counties'][] = $all_users[$val]['county'];
    }
  }
}
sort($group['counties']);

//add it to the bc array
$BC['name'][] = $group['info']['G_NAME'];
//Add the parent ID to the array for link writing
$parent = odbc_result(MSSQL::query('SELECT G_ID FROM GROUPS WHERE G_UUID=\''.$group['info']['G_PID'].'\''), 'G_ID');
$BC['parent'][] = $parent;
//check to see if this is the root group
$isroot = $group['info']['G_ISROOTGROUP'];
$i = 0;
while($isroot != 1){
  $i++;
  if($i > 10){
    $isroot = 1;
  }
  $info = GROUP::get_info($parent);
  $g_name = $info['G_NAME'];
  $BC['name'][] = $g_name;
  $parent = odbc_result(MSSQL::query('SELECT G_ID FROM GROUPS WHERE G_UUID=\''.$info['G_PID'].'\''), 'G_ID');
  $BC['parent'][] = $parent;
  $isroot = $info['G_ISROOTGROUP'];
}

// echo("All key value <br>");
// echo("<pre>");
// var_dump($all_users_key_value);
// echo("</pre>");
// echo("Group array. <br>");
// echo("<pre>");
// var_dump($group);
// echo("</pre>");
// echo("Subgroup time taken: ".($subgroup_breakdown_end - $subgroup_breakdown_begin)." seconds. <br>");
// echo("<pre>");
// var_dump($subgroups);
// echo("</pre>");
// exit();

// OLD CODE
//   $counties .= '<a href="http://'.$_SERVER['HTTP_HOST'].'/reporting_county.php?county='.odbc_result($county_query, 'L_COUNTY').'">'.odbc_result($county_query, 'L_COUNTY').'</a>, ';
// }
//
// //query for subgroups
// $subs_order ='POINTS DESC';
// if(isset($_GET['subs_order'])){
//   $subs_order=$_GET['subs_order'];
// }
// $sql4 = 'SELECT GROUPS.G_ID, G_NAME, G_META, SUM(AL_PA) AS POINTS, SUM(AL_TIME) AS SECONDS, SUM(AL_UNIT) AS DISTANCE, COUNT(1) AS RECORDS FROM GROUPS INNER JOIN GROUP_MEMBER ON GROUP_MEMBER.G_ID = GROUPS.G_ID INNER JOIN LOG ON AL_UID = U_ID WHERE '.$date_range.' G_PID=\''.$parent.'\' GROUP BY GROUPS.G_ID, G_NAME, G_META ORDER BY '.$subs_order.';';
// $subs = MSSQL::query($sql4);
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Walk Georgia | Reporting</title>
  <link rel="stylesheet" href="../../css/foundation.css" />
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.1/b-flash-1.2.1/b-html5-1.2.1/b-print-1.2.1/r-2.1.0/datatables.min.css"/>
  <link type="text/css" media="screen" rel="stylesheet" href="css/responsive-tables.css" />
  <script src="js/vendor/modernizr.js"></script>
  <style media="screen">
  .dt-buttons{
    margin-left: 2em;
    margin-top: 1.3em;
  }
  </style>
</head>
<body>
  <div id="main">
    <!-- Header -->
    <div class="row" style="margin-bottom:20px;">
      <div class="large-12 columns center">
        <img src="img/single-color-logo.png" alt="logo" />
        <img src="img/ext.png" alt="UGA extension logo" />
        <br />
        <br />
        <h1 class="custom-font-small font -blue">Official <?php if($group['info']['G_ISROOTGROUP']) {echo "Group";} else {echo "Subgroup";} ?> Report</h1>
        <hr style="margin-top:-5px; margin-bottom:5px;" />
        <h2 class="custom-font-small font -black"><?php echo $group['info']['G_NAME']; ?></h2>
        <div class="hide-for-small" style="margin:20px;">
          <?php
          if(!$group['info']['G_ISROOTGROUP']){
            for($i = count($BC['name']); $i > 0; $i--) {
              if($i == 1){
                $current = 'class="current"';
                ?>
                <!-- <?php echo $BC['name'][$i-1]; ?> -->
                <?php
              } elseif ($i == 2){
                ?>
                Parent Group: <a href="reporting.php?group=<?php echo $BC['parent'][$i-2];?>"><?php echo $BC['name'][$i-1]; ?></a>
                <?php
              }
            }
          }
          ?>
        </div>
        <!-- ADD GROUP CREATED PHP BELOW -->

        <p class="font -primary">
          Group created: <?= date('m-d-Y', strtotime($group['info']['G_DATE_CREATED'])) ?>
        </p>
        <?php
        if(((filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING) != null) && (filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING) != null)) || ((filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING) != null) && (filter_input(INPUT_GET, 'datepickerMobileEnd', FILTER_SANITIZE_STRING) != null))) {
          echo '<h4>Cumulative Data for Current Date Range: ' . $month1. '-' .$day1. '-' . $year1. ' to ' . $month2. '-' . $day2. '-' . $year2. '</h4>';
        }
        else if($group_id > 153201) {
          echo '<h4>Cumulative Data for Current Date Range: ' . $date1 . ' to ' . $date2 . '</h4>';
        }
        else {
          echo '<h4>Cumulative Data for Current Date Range: All Time '; //'.date('m-d-Y', strtotime($group['info']['G_DATE_CREATED'])). ' to ' . date('m-d-Y');
        }
        ?>
        <!-- <a href="#" class="button tiny">Printer Friendly Version</a> -->
        <!-- Date Range -->
        <?php if((isset($_GET['day1'])) && (isset($_GET['day2']))) {
          echo '<h4>Current Date Range: '.$_GET['month1'].'-'.$_GET['day1'].'-'.$_GET['year1'].' to '.$_GET['month2'].'-'.$_GET['day2'].'-'.$_GET['year2'].'</h4>';
        }?>
        <form action="" method="GET" enctype="application/x-www-form-urlencoded">
          <!-- MOBILE Date Picker -->
          <div class="row show-for-small-only">
            <div class="small-12 columns">
              <p class="mb font -secondary -bold">
                Select Start Date
              </p>
              <input autocomplete="off" type="date" id="datepickerMobile" name="datepickerMobile" <?php if((filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING) != null) && (!empty($month1)) && (!empty($day1)) && (!empty($year1))) { echo (' value="' . $month1. '/' .$day1. '/' . $year1. '"'); } else { if($group_id > 153201) {echo (' value="'.date('m/d/Y', strtotime($group['info']['G_DATE_CREATED'])).'"'); } } ?>/>
              <p class="mb font -secondary -bold">
                Select End Date
              </p>
              <input autocomplete="off" type="date" id="datepickerEndMobile" name="datepickerEndMobile" <?php if((filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING) != null) && (!empty($month2)) && (!empty($day2)) && (!empty($year2))) { echo (' value="' . $month2. '/' .$day2. '/' . $year2. '"'); } else { echo (' value="'.date('m/d/Y').'"'); } ?>/>
            </div>
          </div>
          <!-- End MOBILE Date Picker -->
          <!-- Medium Up Date Picker -->
          <div class="row collapse show-for-medium-up pt1">
            <div class="medium-2 medium-offset-2 columns">
              <a href="#" id="startDate" class="button postfix font -primary">Select Start Date</a>
            </div>
            <div class="medium-2 columns">
              <input autocomplete="off" type="text" id="datepicker" name="datepicker" class="font -standard -primary -bold" <?php if((filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING) != null) && (!empty($month1)) && (!empty($day1)) && (!empty($year1))) { echo (' value="' . $month1. '/' .$day1. '/' . $year1. '"'); } else { if($group_id > 153201) {echo (' value="'.date('m/d/Y', strtotime($group['info']['G_DATE_CREATED'])).'"'); } } ?>/>
            </div>
            <div class="medium-2 columns">
              <a href="#" id="endDate" class="button postfix font -primary">Select End Date</a>
            </div>
            <div class="medium-2 columns end">
              <input autocomplete="off" type="text" id="datepickerEnd" name="datepickerEnd" class="font -primary -standard -bold" <?php if((filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING) != null) && (!empty($month2)) && (!empty($day2)) && (!empty($year2))) { echo (' value="' . $month2. '/' .$day2. '/' . $year2. '"'); } else { echo (' value="'.date('m/d/Y').'"'); } ?>/>
            </div>
          </div>
          <!-- End Medium Up Date Picker -->
          <input type="hidden" id="group" name="group" value="<?= filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING); ?>">
          <div class="tc pb1">
            <a href="<?php echo 'http://' . filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/reporting.php?group=<?= filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING); ?>">RESET </a>
            <!-- End Date Range -->
          </div>
          <div class="tc">
            <input type="submit" name="submit" class="button success font -primary" value ="Generate Report!">
            <!-- End Date Range -->
          </div>
        </form>
      </div>
      <div class="tc">
      </div>
    </div>
    <!-- End Header -->
    <!-- Report Body -->
    <div class="row">
      <!-- Overall Stats -->
      <div class="large-12 columns pb2">
        <h2 class="global-h2">Overall Stats: <?php if($day1!==''){ echo $month1. '-' .$day1. '-' . $year1. ' to ' . $month2. '-' . $day2. '-' . $year2;} ?></h2>
        <hr style="margin-top:-5px; margin-bottom:5px;" />
        <div class="row pt2">
          <div class="medium-4 columns tc-ns">
            <div class="font -secondary -bold -medium pb">
              <?php echo $group['points']; ?>
            </div>
            <b>Total Points Earned</b>
          </div>
          <div class="medium-4 columns tc-ns pt2-s">
            <div class="font -secondary -bold -medium pb">
              <?php echo number_format(floor($group['seconds']/3600)).' Hours '.number_format(floor(($group['seconds']%3600)/60)) .' Minutes'; ?>
            </div>
            <b>Total Time Exercised </b>
          </div>
          <div class="medium-4 columns tc-ns pt2-s">
            <div class="font -secondary -bold -medium pb">
              <?php echo $group['miles']; ?>
            </div>
            <b>Total Miles From Distance-based Exercises <span data-tooltip aria-haspopup="true" class="has-tip" title="The total miles of all user's logged distance-based activites, such as running, biking, hiking, etc.">(?)</span></b>
          </div>
          <!-- <li><b>Virtual "Miles Walked" <span data-tooltip aria-haspopup="true" class="has-tip" title="The previous version of Walk Georgia converted all exercise (including things like yoga, which does not involve distance) into steps for the sake of comparison. We include this stat for members who still find this useful.">(?)</span>:</b>
          <?php //echo round(number_format(((($points*100)-300)/3.3)/3660)); ?></li>
          </ul>-->
        </div>
      </div>
      <!-- End Overall Stats -->
    </div>
    <!-- Group Info -->
    <div class="row">
      <div class="large-12 columns pb2">
        <h2 class="global-h2">Group Information: <?php if($day1!==''){ echo $month1. '-' .$day1. '-' . $year1. ' to ' . $month2. '-' . $day2. '-' . $year2;} ?></h2>
        <hr style="margin-top:-5px; margin-bottom:5px;" />
        <div class="row pt2">
          <div class="medium-4 columns tc-ns ">
            <div class="font -secondary -bold -medium pb ">
              <?php echo $count_users; ?>
            </div>
            <b>Total Number of Users</b>
          </div>
          <div class="medium-4 columns tc-ns pt2-s">
            <div class="font -secondary -bold -medium pb">
              <?php if(count($group['active_members']) >= 1) {
                echo count($group['active_members']); ?>
              </div>
              <b>Total Number of Active Users <span data-tooltip aria-haspopup="true" class="has-tip" title="Any user who has actually logged activity.">(?)</span>:</b>
              <?php } else {
                if(count($group['activities']) >= 1) {
                  echo count($group['activities']); ?>
                </div>
                <b>Total Number of Group Activities</b>
                <?php }
                else {
                  echo '0'; ?>
                </div>
                <b>Total Number of Active Users <span data-tooltip aria-haspopup="true" class="has-tip" title="Any user who has actually logged activity.">(?)</span>:</b>
                <?php }
              } ?>
            </div>
            <div class="medium-4 columns tc-ns pt2-s">
              <?php $all_counties_length = count($group['counties']); ?>
              <div class="font -secondary -bold <?php if($all_counties_length >= 10) {echo '-small';} else {echo '-medium';} ?> pb">
                <?php
                for($i = 0; $i < $all_counties_length; $i++) {
                  if($i == 0) {
                    echo ('<a href="http://'.$_SERVER['HTTP_HOST'].'/reporting_county.php?county='.$group['counties'][$i].'">'.$group['counties'][$i].'</a>');
                  }
                  else {
                    echo (', <a href="http://'.$_SERVER['HTTP_HOST'].'/reporting_county.php?county='.$group['counties'][$i].'">'.$group['counties'][$i].'</a>');
                  }
                }
                ?>
              </div>
              <b>County Associations <span data-tooltip aria-haspopup="true" class="has-tip" title="Lists the different counties of the members.">(?)</span></b>
            </div>
          </div>
        </div>
      </div>
      <!-- End Group Info -->
      <div class="row">
        <!-- Group Members -->
        <div class="large-12 columns">
          <h2 class="global-h2">Active Group Members: (<?php echo count($group['active_members']); ?>)</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <!-- End Member Filtering -->
          <ol class="global-p" style="line-height:2;">
            <table id="" class="display responsive">
              <thead>
                <th>
                  First Name
                </th>
                <th>
                  Last Name
                </th>
                <th>
                  Email
                </th>
                <th>
                  Points
                </th>
                <th>
                  Time(Hours)
                </th>
                <th>
                  Distance(Miles)
                </th>
              </thead>
              <?php
              $group_active_members_length = count($group['active_members']);
              for($i = 0; $i < $group_active_members_length; $i++) {
                ?>
                <tr>
                  <td><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'];?>/reporting_individual.php?id=<?php echo $group['active_members'][$i]['id']; ?>&group=<?php echo $group_id;?>"><?php echo $group['active_members'][$i]['fname'];?></a></td>
                  <td><?php echo $group['active_members'][$i]['lname'] ?></td>
                  <td><?php echo $group['active_members'][$i]['email'] ?></td>
                  <td><?php echo $group['active_members'][$i]['points'] ?></td>
                  <td><?php echo number_format(($group['active_members'][$i]['seconds']/3600), 2, '.', ''); ?></td>
                  <td><?php echo $group['active_members'][$i]['miles']; ?>
                </tr>
              <?php
              }
              ?>
            </table>
          </ol>
        </div>
        <!-- End Group Members -->
        <?php if($is_group_logging) { ?>
          <!-- Group Activities for Group Logging -->
          <div class="large-12 columns">
            <h2 class="global-h2">Group Activities: (<?php echo count($group['activities']); ?>) <span data-tooltip aria-haspopup="true" class="" title="Any activity by Groups who have Group Logging enabled will show up here.">(?)</span></h2>
            <hr style="margin-top:-5px; margin-bottom:5px;" />
            <ol class="global-p" style="line-height:2;">
              <table id="" class="display responsive">
                <thead>
                  <th>
                    Group Name
                  </th>
                  <th>
                    Date
                  </th>
                  <th>
                    Activity
                  </th>
                  <th>
                    Members
                  </th>
                  <th>
                    Points
                  </th>
                  <th>
                    Time(Hours)
                  </th>
                  <th>
                    Distance(Miles)
                  </th>
                </thead>
                <?php
                $group_activities_length = count($group['activities']);
                for($i = 0; $i < $group_activities_length; $i++) {
                  ?>
                  <tr>
                    <td><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'];?>/reporting.php?group=<?php echo $group['activities'][$i]['group_id']; ?>"><?php echo $group['activities'][$i]['group']; ?></a></td>
                    <td><?php echo $group['activities'][$i]['date'] ?></td>
                    <td><?php echo $group['activities'][$i]['activity'] ?></td>
                    <td><?php echo $group['activities'][$i]['members'] ?></td>
                    <td><?php echo intval($group['activities'][$i]['points']) ?></td>
                    <td><?php echo number_format(((($group['activities'][$i]['seconds'])/intval($group['activities'][$i]['members']))/3600), 2, '.', ''); ?></td>
                    <td><?php echo number_format((intval($group['activities'][$i]['miles']) / intval($group['activities'][$i]['members'])), 2, '.', ''); ?></td>
                  </tr>
                <?php
              }
              ?>
            </table>
          </ol>
        </div>
        <!-- End Group Activities -->
        <?php } ?>
        <!-- Subgroups -->
        <div class="large-12 columns">
          <h2 class="global-h2">Active Subgroups: <?php echo count($group['subgroups']); ?></h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <ol class="global-p" style="line-height:2;">
            <table id="" class="display responsive">
              <thead>
                <th>
                  Group Name
                </th>
                <th>
                  Members
                </th>
                <?php if($is_group_logging)  { ?>
                  <th>
                    Group Activities
                  </th>
                  <?php } ?>
                  <th>
                    Points
                  </th>
                  <th>
                    Time(Hours)
                  </th>
                  <th>
                    Distance(Miles)<span data-tooltip aria-haspopup="true" class="has-tip" title="The total miles of all user's logged distance-based activites, such as running, biking, hiking, etc.">(?)</span>
                  </th>
                  <!--<th>
                  Virtual Distance (Miles)<span data-tooltip aria-haspopup="true" class="has-tip" title="The previous version of Walk Georgia converted all exercise (including things like yoga, which does not involve distance) into steps for the sake of comparison. We include this stat for members who still find this useful.">(?)</span>
                </th>-->
              </thead>
              <?php
              if(isset($subgroups)) {
                $subgroups_length = count($subgroups);
                for($i = 0; $i < $subgroups_length; $i++) {
                  ?>
                  <tr>
                    <td><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'];?>/reporting.php?group=<?php echo $subgroups[$i]['info']['G_ID']; ?>"><?php echo $subgroups[$i]['info']['G_NAME']; ?></a></td>
                    <td><?php if(isset($subgroups[$i]['member_ids'])) {echo count($subgroups[$i]['member_ids']); } else { echo '0'; } ?> </td>
                    <?php if($is_group_logging) { ?>
                      <td><?php if(isset($subgroups[$i]['activities'])) {echo count($subgroups[$i]['activities']); } else { echo '0'; } ?></td>
                      <?php } ?>
                      <td><?php if(isset($subgroups[$i]['points'])) {echo intval($subgroups[$i]['points']); } else { echo '0'; } ?></td>
                      <td><?php if(isset($subgroups[$i]['seconds'])) {echo number_format((intval($subgroups[$i]['seconds']) / 3600) , 2, '.', ''); } else { echo '0'; } ?></td>
                      <td><?php if(isset($subgroups[$i]['miles'])) {echo intval($subgroups[$i]['miles']); } else { echo '0'; } ?></td>
                      <!--<td><?php // if(isset($gdown['POINTS'])){echo array_sum($gdown['POINTS']);}else{echo '0';} ?></td>
                      <td><?php // if(isset($gdown['TIME'])){echo number_format(array_sum($gdown['TIME'])/3600, 2, '.', '');}else{echo '0';}?></td>
                      <td><?php // if(isset($gdown['DISTANCE'])){echo array_sum($gdown['DISTANCE']);}else{echo '0';} ?></td>
                      <td><?php // if(isset($gdown['POINTS'])){echo number_format((((array_sum($gdown['POINTS'])*100)-300)/3.3)/3660, 2, '.', '');}else{echo '0';} ?></td>-->
                    </tr>
                    <?php }
                  } ?>
                </table>
              </div>
              <!-- End Subgroups -->
            </div>
            <?php /*
            <!-- Activity Breakdown -->

            <div class="row">
            <div class="large-12 columns">
            <h2 class="global-h2">Activity Breakdown:</h2>
            <hr style="margin-top:-5px; margin-bottom:5px;" />
            <ol class="global-p" style="line-height:2;">
            <table id="" class="display responsive">
            <thead>
            <th>
            Activity
            </th>
            <th>
            No. of Users
            </th>
            <th>
            Points
            </th>
            <th>
            Time(Hours)
            </th>
            <th>
            Amount Logged
            </th>
            <th>
            Distance (Miles)
            </th>
            </thead
            ><?php
            if(isset($group_breakdown['POINTS'])){
            foreach(array_keys($group_breakdown['POINTS']) as $aid) {
            $count_activity = odbc_result(MSSQL::query('SELECT COUNT(AL_ID) AS COUNT FROM LOG WHERE AL_UID IN (SELECT U_ID FROM GROUP_MEMBER WHERE G_ID = '.$group_id.') AND AL_AID=\''.$aid.'\''), 'COUNT');
            ?>
            <tr>
            <td><?php echo ACTIVITY::activity_to_form($aid, 2, '.', '');?></td>
            <td><?php echo $group_breakdown['MEMBER_COUNT'][$aid]; ?></td>
            <td><?php echo $group_breakdown['POINTS'][$aid]; ?></td>
            <td><?php echo number_format($group_breakdown['TIME'][$aid]/3600);?></td>
            <td><?php echo $count_activity; ?></td>
            <td><?php echo $group_breakdown['DISTANCE'][$aid]; ?></td>
            </tr>
            <?php
          }
        }
        ?>
        </table>
        </div>
        <!-- End Activity Breakdown -->
        *
        */
        ?>
        <!-- Sessions -->
        <!-- <div class="large-6 columns" style="float:left;">
        <h2 class="global-h2">Session Dates:</h2>
        <hr style="margin-top:-5px; margin-bottom:5px;" />
        <p class="global-p">
        No sessions were attached to this report.
      </p>
      <label>
      You can also narrow the report to a specific date range by using the bottom at the top right of this report.
    </label>
  </div> -->
  <!-- End Session -->
</div>
<div class="row">
  <div class="large-12 columns">
    <hr />
    <a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/group/groups.php?group=<?php echo $group_id; ?>" class="button tiny">Back to Group Page</a>
  </div>
</div>
<!-- End Report Body -->
<!-- End Main Content -->
</div>
<script src="https://code.jquery.com/jquery-1.12.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="js/foundation.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.1/b-flash-1.2.1/b-html5-1.2.1/b-print-1.2.1/r-2.1.0/datatables.min.js"></script>
<script type="text/javascript" src="js/responsive-tables.js"></script>
<script>
$(document).foundation();
// $("#endDate").click(function() {
//   $("#datepickerEnd").datepicker("show");
// });
$(document).ready(function() {
  //DATEPICKER
  $(function () {
    $("#datepicker").datepicker({<?php if($group_id > 153201) { ?>minDate: new Date(<?php echo($date1); ?>),<?php } ?>maxDate: new Date("<?php echo(date('m-d-Y')); ?>")});
    $("#startDate").click(function() {
      $("#datepicker").datepicker("show");
    });
    $("#datepickerEnd").datepicker({<?php if($group_id > 153201) { ?>minDate: new Date(<?php echo($date1); ?>),<?php } ?>maxDate: new Date("<?php echo(date('m-d-Y')); ?>")});
    $("#endDate").click(function() {
      $("#datepickerEnd").datepicker("show");
    });
  });
  $(document).ready(function () {
    $('table.display').DataTable({
      "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
      responsive: true,
      // stateSave: true,
      // "dom": 'T<"clear">lfrtip',
      dom: 'lBfrtip',
      buttons: [
        'copy', 'excel', 'pdf'
      ]
    });
  });
});
</script>
</body>
</html>
