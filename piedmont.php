<?php
include_once("lib/template_api.php");
include_once("lib/front_api.php");
$ss = SESSION::secure_session();
if($_GET['logout']==1){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Test Page', $_SESSION['valid']);
HTML_ELEMENT::top_nav();

//EDITING STARTS HERE

if($_SESSION['valid']){
			$redirect = REDIRECT::dash(); 
		}
		//If user is not logged in
		else{
			if($_GET['success']==1){
				$redirect = REDIRECT::login();
			}
			else if(isset($_POST['FN'])){
				ACCOUNT::register();
			}else{
			?>
  <div class="row" style="margin-top:0%">
    <div class="medium-6 medium-centered large-centered large-6 columns center">
    
      <img src="img/single-color-logo.png" alt="walk georgia logo" />
      <h1 class="custom-font-small">Welcome, Piedmont!</h1>
      <p class="global-p">We are so glad to have you help us test the Walk Georgia Pilot.  Above everything, we hope you have have fun in this program.  If you run into any bugs or problems, please feel free to report them so that we can make Walk Georgia all it can be togehter.</p>
      <p class="global-p"><strong>The pilot will begin on June 16th.  Some aspects of the site may be unresponsive until that date as we get everything ready.</strong></p>
      <hr />
    </div>
       <br />
       <div class="medium-6 medium-centered large-centered large-6 columns">
     <form data-abide method="post">
          <div class="name-field">
          <label>First Name</label>
          <input type="text" name="FN" id="FN" required pattern="[a-zA-Z]+">
          <small class="error" data-error-message="">A first and last name is required.</small>
          </div>
          <div class="name-field">
          <label>Last Name</label>
          <input type="text" name="LN" id="LN" required pattern="[a-zA-Z]+">
          <small class="error" data-error-message="">A first and last name is required.</small>
          </div>
          <div class="date-field">
          <label>Date of Birth</label>
          <input type="date" name="DOB" id="DOB" required>
          <small class="error" data-error-message="">A birth date is required.</small>
          </div>
          <div class="number-field">
          <label>Employee ID</label>
          <input type="text" name="EMN" id="EMN" required>
          <small class="error" data-error-message="">An employee ID is required.</small>
          </div>
          <div class="email-field">
          <label>Email</label>
        <input id="email" type="email" name="EM" id="EM" required></input>
       <small class="error">An email address is required.</small>
       </div>
          <div class="email-field">
          <label>Re-enter Email</label>
        <input type="email" required></input>
       <small class="error">The emails must match.</small>
       </div>
        <div class="password">
           <label for="password">Password</label>
           <input id="password" type="password" name="password" id="password" required name="password"></input>
           <small class="error">Your password must contain 8 characters. It must also have at least one upper and lower case letter, as well as a special character like !</small>
        </div>
        <div class="password2">

           <label for="confirmPassword">Confirm Password</label>
           <input id="confirmPassword" type="password" data-equalto="password" required name="confirmPassword" data-invalid=""></input>
           <small class="error" data-error-message="">Passwords must match.</small>

        </div>
        <div class="county">
          <label>County of Participation
            <select name="CN" id="CN">
              <option value="Appling">Appling</option>
              <option value="Atkinson">Atkinson</option>
              <option value="Bacon">Bacon</option>
              <option value="Baker">Baker</option>
              <option value="Baldwin">Baldwin</option>
              <option value="Banks">Banks</option>
              <option value="Barrow">Barrow</option>
              <option value="Bartow">Bartow</option>
              <option value="Ben Hill">Ben Hill</option>
              <option value="Berrien">Berrien</option>
              <option value="Bibb">Bibb</option>
              <option value="Bleckley">Bleckley</option>
              <option value="Brantley">Brantley</option>
              <option value="Brooks">Brooks</option>
              <option value="Bryan">Bryan</option>
              <option value="Bulloch">Bulloch</option>
              <option value="Burke">Burke</option>
              <option value="Butts">Butts</option>
              <option value="Calhoun">Calhoun</option>
              <option value="Camden">Camden</option>
              <option value="Candler">Candler</option>
              <option value="Carroll">Carroll</option>
              <option value="Catoosa">Catoosa</option>
              <option value="Charlton">Charlton</option>
              <option value="Chatham">Chatham</option>
              <option value="Chattahoochee">Chattahoochee</option>
              <option value="Chattooga">Chattooga</option>
              <option value="Cherokee">Cherokee</option>
              <option value="Clarke">Clarke</option>
              <option value="Clay">Clay</option>
              <option value="Clayton">Clayton</option>
              <option value="Clinch">Clinch</option>
              <option value="Cobb">Cobb</option>
              <option value="Coffee">Coffee</option>
              <option value="Colquitt">Colquitt</option>
              <option value="Columbia">Columbia</option>
              <option value="Cook">Cook</option>
              <option value="Coweta">Coweta</option>
              <option value="Crawford">Crawford</option>
              <option value="Crisp">Crisp</option>
              <option value="Dade">Dade</option>
              <option value="Dawson">Dawson</option>
              <option value="Decatur">Decatur</option>
              <option value="DeKalb">DeKalb</option>
              <option value="Dodge">Dodge</option>
              <option value="Dooly">Dooly</option>
              <option value="Dougherty">Dougherty</option>
              <option value="Douglas">Douglas</option>
              <option value="Early">Early</option>
              <option value="Echols">Echols</option>
              <option value="Effingham">Effingham</option>
              <option value="Elbert">Elbert</option>
              <option value="Emanuel">Emanuel</option>
              <option value="Evans">Evans</option>
              <option value="Fannin">Fannin</option>
              <option value="Fayette">Fayette</option>
              <option value="Floyd">Floyd</option>
              <option value="Forsyth">Forsyth</option>
              <option value="Franklin">Franklin</option>
              <option value="Fulton">Fulton</option>
              <option value="Gilmer">Gilmer</option>
              <option value="Glascock">Glascock</option>
              <option value="Glynn">Glynn</option>
              <option value="Gordon">Gordon</option>
              <option value="Grady">Grady</option>
              <option value="Greene">Greene</option>
              <option value="Gwinnett">Gwinnett</option>
              <option value="Habersham">Habersham</option>
              <option value="Hall">Hall</option>
              <option value="Hancock">Hancock</option>
              <option value="Haralson">Haralson</option>
              <option value="Harris">Harris</option>
              <option value="Hart">Hart</option>
              <option value="Heard">Heard</option>
              <option value="Henry">Henry</option>
              <option value="Houston">Houston</option>
              <option value="Irwin">Irwin</option>
              <option value="Jackson">Jackson</option>
              <option value="Jasper">Jasper</option>
              <option value="Jeff Davis">Jeff Davis</option>
              <option value="Jefferson">Jefferson</option>
              <option value="Jenkins">Jenkins</option>
              <option value="Johnson">Johnson</option>
              <option value="Jones">Jones</option>
              <option value="Lamar">Lamar</option>
              <option value="Lanier">Lanier</option>
              <option value="Laurens">Laurens</option>
              <option value="Lee">Lee</option>
              <option value="Liberty">Liberty</option>
              <option value="Lincoln">Lincoln</option>
              <option value="Long">Long</option>
              <option value="Lowndes">Lowndes</option>
              <option value="Lumpkin">Lumpkin</option>
              <option value="Macon">Macon</option>
              <option value="Madison">Madison</option>
              <option value="Marion">Marion</option>
              <option value="McDuffie">McDuffie</option>
              <option value="McIntosh">McIntosh</option>
              <option value="Meriwether">Meriwether</option>
              <option value="Miller">Miller</option>
              <option value="Mitchell">Mitchell</option>
              <option value="Monroe">Monroe</option>
              <option value="Montgomery">Montgomery</option>
              <option value="Morgan">Morgan</option>
              <option value="Murray">Murray</option>
              <option value="Muscogee">Muscogee</option>
              <option value="Newton">Newton</option>
              <option value="Oconee">Oconee</option>
              <option value="Oglethorpe">Oglethorpe</option>
              <option value="Paulding">Paulding</option>
              <option value="Peach">Peach</option>
              <option value="Pickens">Pickens</option>
              <option value="Pierce">Pierce</option>
              <option value="Pike">Pike</option>
              <option value="Polk">Polk</option>
              <option value="Pulaski">Pulaski</option>
              <option value="Putnam">Putnam</option>
              <option value="Quitman">Quitman</option>
              <option value="Rabun">Rabun</option>
              <option value="Randolph">Randolph</option>
              <option value="Richmond">Richmond</option>
              <option value="Rockdale">Rockdale</option>
              <option value="Schley">Schley</option>
              <option value="Screven">Screven</option>
              <option value="Seminole">Seminole</option>
              <option value="Spalding">Spalding</option>
              <option value="Stephens">Stephens</option>
              <option value="Stewart">Stewart</option>
              <option value="Sumter">Sumter</option>
              <option value="Talbot">Talbot</option>
              <option value="Taliaferro">Taliaferro</option>
              <option value="Tattnall">Tattnall</option>
              <option value="Taylor">Taylor</option>
              <option value="Telfair">Telfair</option>
              <option value="Terrell">Terrell</option>
              <option value="Thomas">Thomas</option>
              <option value="Tift">Tift</option>
              <option value="Toombs">Toombs</option>
              <option value="Towns">Towns</option>
              <option value="Treutlen">Treutlen</option>
              <option value="Troup">Troup</option>
              <option value="Turner">Turner</option>
              <option value="Twiggs">Twiggs</option>
              <option value="Union">Union</option>
              <option value="Upson">Upson</option>
              <option value="Walker">Walker</option>
              <option value="Walton">Walton</option>
              <option value="Ware">Ware</option>
              <option value="Warren">Warren</option>
              <option value="Washington">Washington</option>
              <option value="Wayne">Wayne</option>
              <option value="Webster">Webster</option>
              <option value="Wheeler">Wheeler</option>
              <option value="White">White</option>
              <option value="Whitfield">Whitfield</option>
              <option value="Wilcox">Wilcox</option>
              <option value="Wilkes">Wilkes</option>
              <option value="Wilkinson">Wilkinson</option>
              <option value="Worth">Worth</option>                       
            </select>
          </label>
        </div>
        
        <div class="terms">
           <label><small>By creating an account you have read and agree to the <a href="#">terms and conditions</a>.</small></label>
           <input id="checkbox1" type="checkbox" required></input><label for="checkbox1">I Agree</label>
           <small class="error">You must agree to the terms and conditions.</small>
        </div>
          <br />
          <br />         
          <input type="button" class="button" value="Create Account" onclick="return regformhash(this.form,this.form.password);" />
          <br />
          <small>Sign in with <a href="#">Facebook</a>, <a href="#">Google</a>, or <a href="#">Twitter</a>.</small>

      </form>
      </div>
</div>
        	<?php
			}
			}

//EDITING ENDS HERE

HTML_ELEMENT::footer();
//JAVASCRIPTS HERE
?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../js/forms.js"></script>
    <script src="../js/sha512.js"></script>
    <script src="../js/foundation/foundation.js"></script>
    <script src="../js/foundation/foundation.topbar.js"></script>
    <script src="../js/foundation/foundation.abide.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>