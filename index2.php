<?php
include_once("lib/template_api.php");
include_once ("lib/back_api.php");
$ss = SESSION::secure_session();
if($_GET['logout']==1){
	SESSION::logout();
	REDIRECT::home();
}
if(isset($_POST['file'])){
		$img = ACCOUNT::avatar_upload($_SESSION['ID']);
	}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();
//If the user is logged in
if($_SESSION['valid']){
	if(isset($_GET['uid'])){
		$ID =$_GET['uid'];
	}else{
		$ID= $_SESSION['ID'];
	}
	$info = ACCOUNT::get_info($ID);
//EDIT HERE
?> 
<div class="main nojava">
<!-- Main Section -->
  <div class="row">
  <!-- Global Pilot Alert -->
  <div class="large-12 columns">
    <div data-alert class="alert-box center">
      We are currently overhauling the "Log an Activity" feature to remove bugs.  We'll have it back and better than ever in just a bit.
      <a href="#" class="close">&times;</a>
    </div>
    <?php if(isset($_GET['err_msg'])){?>
    <div data-alert class="alert-box alert center">
      <?php echo $_GET['err_msg'];?></a>
      <a href="#" class="close">&times;</a>
    </div>
        <?php
	}?>
  </div>
  <!-- End Global Pilot Alert -->
  <!-- Sidebar -->
    <div class="large-5 medium-5 columns">
      <div class="hide-for-small">
        <div class="sidebar">
          <div class="center"> 
          <?php
		  if (file_exists($_SERVER['DOCUMENT_ROOT']."/img/avatar/".$ID.".png")) {
			  $filename = "../img/avatar/".$ID.".png";
		  } else {
    			$filename = "../img/avatar/0.png";
		  }
		  ?>
            <!-- Avatar -->
            <a href="#" data-reveal-id="avatar" id="joyride-2">
            <img alt="Your Picture" src="<?php echo $filename; ?>"  style="border-radius:50%; max-height:240px; max-width:240px;"/>
            <?php odbc_result_all($info)?>
            </a>
            <!-- End Avatar -->
            <!-- Change Avatar Modal -->
            <div id="avatar" class="reveal-modal" data-reveal>
              <div class="row">
                <div class="large-12 columns">
                  <form method="post" enctype="multipart/form-data">
                    <img id="blah" src="<?php echo $filename; ?>" alt="your image" style="border-radius:50%; max-height:240px; max-width:240px;" />
                    <br />
                    <br />
                    <script type="text/javascript">
	        			function readURL(input) {
			        	    if (input.files && input.files[0]) {
			    	            var reader = new FileReader();
			    	            reader.onload = function (e) {
				                    $('#blah').attr('src', e.target.result);
				                }
				                reader.readAsDataURL(input.files[0]);
				            }
			    	    }
				    </script>
                    <p class="global-p">Do you want to upload a new image?</p>
                    <input type="file" class="" name="file_2" id="file_2" onchange="readURL(this);">
                    <br>
                    <input type="submit" class="button tiny" name="file" id-"file" value="Submit">
                  </form>
                </div>
              </div>
            <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End Change Avatar Modal -->
            <br />
            <h2 class="global-h2-gray"><?php echo $info['FNAME']." ".$info['LNAME'] ?></h2>
            <br />
            <br />
            <div id="joyride-3"><?php $form = ACTIVITY::activity_form(); ?></div>
            <!-- Search Bar 
             <div class="span">
               <div class="row collapse">
                 <div class="small-10 columns">
                   <label style="display:none;" for="search">SEARCH</label>
                   <input type="text" placeholder="Find users, groups, and sessions" id="search">
                 </div>
                 <div class="small-2 columns">
                   <a style="padding-top:2px;" href="#" class="button postfix"><i class="fi-magnifying-glass size-21"></i></a>
                 </div>
              </div>
            </div>
            End Search Bar -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Sidebar -->
    
    <!-- Quick Stats -->
    <div id="joyride-5">
    <?php
		  	$list = ACTIVITY::points_breakdown($ID);
	?> 
    </div>
    <!-- End Quick Stats -->
    
  </div>
  <div class="row">
    <!-- Day by Day Activity Log -->
    <div id="joyride-4">
    <?php
		  	$list = ACTIVITY::display_activity($ID, '7');
	?>
    </div>
    <!-- End Day by Day Activity Log -->
  </div> 
  
  </section>

<a class="exit-off-canvas"></a>
</div>
</div>
</div>

<!-- Joyride -->
<ol class="joyride-list" data-joyride>
  <li data-text="Next">
    <p>Welcome to walk Georgia!  Here is a quick tour of your profile.</p>
  </li>
  <li data-id="joyride-2" data-class="custom so-awesome" data-text="Next">
    <h4>Stop #1</h4>
    <p>You can start by clicking here to add a picture and make it personal.  Notice the menu above, also.  That will help you navigate between your profile and all the goals, groups, and sessions you have going on!</p>
  </li>
  <li data-id="joyride-3" data-class="custom so-awesome" data-text="Next">
    <h4>Stop #2</h4>
    <p>You can click this button to log an activity.  A new window will open that will walk you through the process.</p>
  </li>
  <li data-id="joyride-4" data-class="custom so-awesome" data-text="Next">
    <h4>Stop #3</h4>
    <p>The activity you log will show up here, so you can keep track of what you've done and even edit if you entered something wrong.</p>
  </li>
  <li data-text="Next">
    <h4>Stop #4</h4>
    <p>Every activity you log will earn you points and add to your overall stats.  You can click on the "Quick Stats" headings and see the breakdown of the stats you've accumulated.</p>
  </li>
  <li data-button="End">
    <h4>Stop #5</h4>
    <p>That does it for our tour!  Now get going!  Move more.  Live more.</p>
  </li>
</ol>
<!-- End Joyride -->

<?php 
}
//If user is not logged in
		else{
			?>
<div id="main">
  <div id="top nojava">
  
  
    <div class="header-1" style="padding:30px;"> 
    
    <!-- Sign up and Log in Buttons -->
      <div class="row nojava center">
      <img src="img/logo-float-white.png" width="40%" height="40%" class="center" /> 
      <br /><br /><br />    
        <div class="large-12 columns expand">
          <div class="row center nojava">
            <div class="small-12 medium-6 large-6 columns">
              <a href="login.php" class="button expand">Log In</a>
            </div>
            <div class="small-12 medium-6 large-6 columns">
            <a href="register.php" class="button expand">Create an Account</a>
          </div>
          </div>
        </div>
      </div>
    <!-- End Sign up and Log in Buttons -->
    </div>
    
    </div>

  <!-- End of Top Section -->
  
  <!-- Get Started Section -->
  <div class="blue-bg" style="margin-top:0px;">
    <div class="row center nojava">    
      <div class="large-12 columns">
        <h1 class="custom-font-big-white hide-for-small-only">get started</h1>
        <h1 class="custom-font-small-white show-for-small-only">get started</h1>
      </div>
    </div>
    <div class="row center nojava" style="margin-top:10px;">
      <div class="small-12 medium-12 large-12 columns">
      <p class="global-p-white-strong">
        Walk Georgia is a site dedicated to helping you move more and live more.  Whether you are a complete beginner or a seasoned fitness pro, Walk Georgia can help you improve.  Create an account and start logging your activity.
      </p>
      </div>
    </div>
    <div class="row center nojava" style="margin-top:30px;">
      <!-- <img src="img/sample-1.png" alt="A laptop showing the user interface" /> -->
    </div>
    <div class="row center nojava" style="margin-top:20px;">
     <!-- <a class="button success round">View the F.A.Q.</a> -->
    </div>
  </div>
  <!-- End Get Started Section -->
  
  <!-- Track Your Growth Section -->
  <div class="white-bg">
    <div class="row center nojava">    
      <div class="large-12 columns">
        <h2 class="custom-font-big-blue hide-for-small-only">connect locally</h2>
        <h2 class="custom-font-small-blue show-for-small-only">connect locally</h2>
      </div>
    </div>
    <div class="row center nojava" style="margin-top:10px;">
      <div class="small-12 medium-12 large-12 columns">
      <p class="global-p-strong">
        What sets Walk Georgia apart from other programs?  It's local.  Counties all over Georgia have opportunities to connect, events, and resources for you to use right here at home.  
      </p>
      </div>
    </div>
    <div class="row center nojava" style="margin-top:30px;">
      <!-- <img src="img/sample-1.png" alt="A laptop showing the user interface" /> -->
    </div>
  </div>
  <!-- End Track Your Growth Section -->
  
  <!-- Move Together Section -->
  <div class="peach-bg">
    <div class="row center nojava">    
      <div class="large-12 columns">
        <h2 class="custom-font-big-white hide-for-small-only">move together</h2>
        <h2 class="custom-font-small-white show-for-small-only">move together</h2>
      </div>
    </div>
    <div class="row center nojava" style="margin-top:10px;">
      <div class="small-12 medium-12 large-12 columns">
      <p class="global-p-white-strong">
        At the core of Walk Georgia is a personal, individual profile to help you move more.  But we also see that people like to work together.  That's why we made creating groups easy.  Whether your a company wanting to do a wellness program for your employees or just a group of neighborhood runners who want to have a friendly competition, Walk Georgia can help you connect and keep track of your activity in a simple and detailed way.
      </p>
      </div>
    </div>
    <div class="row center nojava" style="margin-top:30px;">
      <!-- <img src="img/sample-1.png" alt="A laptop showing the user interface" /> -->
    </div>
  </div>
  <!-- End Move Together Section -->
  
  <!-- Learn More Section -->
  <div class="white-bg">
    <div class="row center nojava">    
      <div class="large-12 columns">
        <h2 class="custom-font-big-peach hide-for-small-only">learn more</h2>
        <h2 class="custom-font-small-peach show-for-small-only">learn more</h2>
      </div>
    </div>
    <div class="row center nojava" style="margin-top:10px;">
      <div class="small-12 medium-12 large-12 columns">
      <p class="global-p-strong">
        We're constantly compiling helpful resources to equip you to do more, from healthy recipies to exercise tips.  We distribute this material through our blog and newsletter and are currently working on complining them all into a helpful archive.
      </p>
      </div>
    </div>
    <div class="row center nojava" style="margin-top:30px;">
      <!-- <img src="img/sample-1.png" alt="A laptop showing the user interface" /> -->
    </div>
  </div>
  <!-- End Learn More Section -->
  

  
</div>
<!-- End of Main Section --> 

        	<?php
		}

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../js/modernizr.js"></script>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script src="js/foundation/foundation.js"></script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script src="js/foundation/foundation.joyride.js"></script>
    <script src="js/vendor/jquery.cookie.js"></script>
    <script src="js/foundation/foundation.topbar.js"></script>
    <script src="js/foundation/foundation.abide.js"></script>
    <script src="js/foundation/foundation.reveal.js"></script>
    <script src="js/foundation/foundation.tooltip.js"></script>
    <script src="js/foundation/foundation.offcanvas.js"></script>
    <script src="js/foundation/foundation.equalizer.js"></script>
    <script src="js/foundation/foundation.dropdown.js"></script>
    <script src="js/foundation/foundation.joyride.js"></script>
    <script type="text/javascript" src="js/foundation-datepicker.js"></script>
    <script src="js/vendor/jquery.cookie.js"></script> <!-- Optional -->
    <script type="text/javascript" src="js/sha512.js"></script>
    <script type="text/javascript" src="js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
    <script>
		$(function () {
			$('#dp3').fdatepicker();
		});
	</script>
    <?php
		$first_time = ACCOUNT::first_visit($_SESSION['ID']);
		if($first_time){
			?>
            <script>
				$(document).foundation('joyride', 'start');
			</script>
            <?php
		$first_visit_removal = ACCOUNT::first_visit_remove($_SESSION['ID']);
		}
	?>
  <!-- End Footer -->
  </body>
</html>