
<!doctype html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta property="fb:app_id"          content="276401669208985" /> 
    <meta property="og:type"            content="website" /> 
        <meta property="og:url"             content="http://www.walkgeorgia.org" />
	<meta property="og:title"           content="Walk Georgia - 'Move More, Live More!!" />
	    <meta property="og:site_name" 		content="Walk Georgia"/>
    <meta property="og:image"           content="http://dev.walkgeorgia.com/horizontial-logo.png" />

    <meta property="og:description"    	content="Walk Georgia: The place for
Georgians to track their fitness progress & receive local, research-based
knowledge & resources on wellness." />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Walk Georgia | Welcome to Walk Georgia</title>
    <link rel="stylesheet" type="text/css" href="http://dev.walkgeorgia.com/css/foundation.css"/>
    <link rel="stylesheet" type="text/css" href="http://dev.walkgeorgia.com/css/dataTables.foundation.css">
    <link rel="stylesheet" type="text/css" href="http://dev.walkgeorgia.com/css/dataTables.tableTools.css">
    <!-- valid session -->
    <script async >
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-18679377-1', 'walkgeorgia.org');
		ga('send', 'pageview');
    </script>
    <!--[if lte IE 8]>
            <meta http-equiv="refresh" content="0;url=http://www.walkgeorgia.org/browser-update.html" />

    <![endif]-->
  </head>
  <body>
  <div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=276401669208985";
			fjs.parentNode.insertBefore(js, fjs);
		}
		(document, 'script', 'facebook-jssdk'));
   </script>
  <noscript>
    <div class="row" style="margin-top:10%;; min-height:400px;">
      <div align="center">
        <h1 class="custom-font-small">enable javascript</h1>
      </div>
      <div class="medium-6 medium-centered large-centered large-6 columns">
        For full functionality of this site it is necessary to enable JavaScript.
        Here are the <a href="http://www.enable-javascript.com/" target="_blank">
		<style>div.nojava { display:none; }</style>
      </div>
   </div>
  </noscript>
          
  <!-- Top Bar Logged In -->
    
    <div class="fixed">
      <nav class="top-bar" data-topbar>
        <ul class="title-area">
          <li class="name">
            <h1 class="gotham-big"><a href="index.php">walk georgia</a></h1>
          </li>
          <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
        </ul>

        <section class="top-bar-section">
          <!-- Right Nav Section -->
          <ul class="right" style="">
            <li><a href="http://dev.walkgeorgia.com/about-us.php">About Us</a></li>
            <li class="has-dropdown"><a href="#">Help Center</a>
              <ul class="dropdown">
                <li><a href="http://dev.walkgeorgia.com/faq.php">FAQ</a></li>
                <li><a href="http://dev.walkgeorgia.com/implementation.php">Implementation Guides</a></li>
              </ul>
            </li>
            <li class="has-dropdown"><a href="http://dev.walkgeorgia.com/resources/index.php">Resources</a>
              <ul class="dropdown">
                <li><a href="http://blog.extension.uga.edu/walkgeorgia" target="_blank">Blog</a></li>
                <li><a href="http://dev.walkgeorgia.com/resources/recipes/index.php">Recipes</a></li>
                <li><a href="http://dev.walkgeorgia.com/resources/fitness/index.php">Fitness Tips</a></li>
                <li><a href="http://dev.walkgeorgia.com/resources/county-resources.php">County Resources</a></li>
                <li><a href="http://dev.walkgeorgia.com/lesson-plans.php">Lesson Plans</a></li>
                <li><a href="http://dev.walkgeorgia.com/events/index.php">Events</a></li>
              </ul>
            </li>
            <li><a href="http://dev.walkgeorgia.com/contact-us.php">Contact Us</a></li>
            <li class="active has-dropdown"><a href="http://dev.walkgeorgia.com/index.php">My Account</a>
              <ul class="dropdown">
                <li><a href="http://dev.walkgeorgia.com/user-account.php">Preferences</a></li>
                <li><a href="http://dev.walkgeorgia.com/?logout=1">Log Out</a></li>
              </ul>
                
     </ul>
        </section>
      </nav>
      
    <!--[if lte IE 9]> <style>.left-off-canvas-menu { display:none; }</style>  <![endif]-->  
        

    <section class="main-section">
    
    <!-- Desktop Sub Nav -->
    
      <div class="">
        <nav class="sub-bar hide-for-small">
          <div class="row">
            <div class="large-12 columns">  
              <dl class="sub-nav">  
                        <dd ><a href="http://dev.walkgeorgia.com/">My Activity</a></dd>
                <dd ><a href="http://dev.walkgeorgia.com/groups.php">Groups</a></dd>
        
        <dd ><a href="http://dev.walkgeorgia.com/sessions.php">Sessions</a></dd>
		        <dd><a href="#" data-reveal-id="search-modal-landing" class="inbox-icon"><i class="fi-magnifying-glass size-24"></i></a></dd>
		              </dl>
            </div>   
          </div>
        </nav>
      </div>
      
    <!-- End Desktop Sub Nav -->

    <!-- Mobile Sub Nav -->
    
      <div class="show-for-small-only">
        <nav class="mobile-sub-bar">  
          <div class="row">
            <div class="large-12 columns">  
              <div style="float:left;">
                <a href="#" class="mobile-toggle"><h2 class="global-h2-gray"> &#x25BC; My Tools</h2></a>
              </div>
            </div>     
          </div>
        </nav>
      
      
      <!-- Mobile Nav Accordion Content -->  
        <div class="mobile-top-nav" style="margin-bottom:0px;">
                <br />
          <ul class="small-block-grid-4 center">
            <li>
              <a href="http://dev.walkgeorgia.com/">
                <i class="fi-home size-36"></i>
                <label style="margin-top:-15px; color:#008CBA;"><b>Home</b></label>
              </a>
            </li>
            <li>
        <a href="http://dev.walkgeorgia.com/groups.php">
          <i class="fi-torsos-all size-36"></i>
            <label style="margin-top:-15px; color:#008CBA;"><b>Groups</b></label>
        </a>
      </li>
      
      <li>
        <a href="http://dev.walkgeorgia.com/sessions.php">
          <i class="fi-calendar size-36"></i>
          <label style="margin-top:-15px; color:#008CBA;"><b>Sessions</b></label>
        </a>
      </li>  
      
      <li>
        <a href="#" data-reveal-id="search-modal-landing">
          <i class="fi-magnifying-glass size-36"></i>
            <label style="margin-top:-15px; color:#008CBA;"><b>Search</b></label>
        </a>
      </li>
      
    </ul>
         
  </div>

</div>         
<!-- End Mobile Sub Nav -->

<!-- Seach Modal -->

<div id="search-modal-landing" class="reveal-modal" data-reveal>
  	      
          <div class="center"><h2 class="custom-font-small-blue">What are you looking for?</h2></div>
          
          <div class="row">
            
            <!-- User Landing -->
            <div class="large-4 medium-4 columns center">
              <div class="white-bg">
                <i class="fi-torso size-60 blue"></i>
                <h2 class="custom-font-small-blue">Users</h2>
                <hr style="margin-top:-5px; margin-bottom:10px;">
                <a href="#" class="button tiny expand" data-reveal-id="user-search">Search for a User</a>
                <p class="global-p-small justify">Find out if your friends, family, or co-workers are on Walk Georgia!</p>
              </div>
            </div>
            <!-- End User Landing -->
      
            <!-- Group Landing -->
            <div class="large-4 medium-4 columns center">
              <div class="white-bg">
                <i class="fi-torsos-all size-60 blue"></i>
                <h2 class="custom-font-small-blue">Groups</h2>
                <hr style="margin-top:-5px; margin-bottom:10px;">
                <a href="#" class="button tiny expand" data-reveal-id="group-search">Search for a Group</a>
                <p class="global-p-small justify">If you are trying to join or find a group, type in the name here to find it.</p>
              </div>
            </div>
            <!-- End Group Landing -->
            
            <!--Session Landing -->
            <div class="large-4 medium-4 columns center">
              <div class="white-bg">
                <i class="fi-clock size-60 blue"></i>
                <h2 class="custom-font-small-blue">Sessions</h2>
                <hr style="margin-top:-5px; margin-bottom:10px;">
                <a href="#" class="button tiny expand" data-reveal-id="session-search">Search for a Session</a>
                <p class="global-p-small justify">Looking for a session to join? Find it fast by searching here.</p>
              </div>
            </div>
            <!-- End Session Landing -->
              
          </div>
          <a class="close-reveal-modal">&#215;</a>
        </div>
        
          <!-- Search for Users Modal -->
          <div id="user-search" class="reveal-modal" data-reveal>
            <div class="row center">
              <div class="large-12 columns">
                <i class="fi-torso size-60 blue"></i>
                <h2 class="custom-font-small-blue">Search for Users:</h2>
                 <p class="global-p">Please note: users, groups, and sessions set to “private" are not included in this search. <a href="../faq.php#F9" target="_blank">?</a></p>
                <form id="search_user" name="search_user" action="http://dev.walkgeorgia.com/search_user.php">
                  <div class="row collapse">
                    <div class="small-10 columns">
                      <input id='search' name='search' type="text" placeholder="Enter a Name">
                    </div>
                    <div class="small-2 columns">
                      <input type='submit' value='Go' class="button postfix">
                    </div>
                  </div>
                  <a href="#" style="float:left;" data-reveal-id="search-modal-landing" class="button tiny secondary">Back to Search Options</a>
                </form>
              </div>
            </div>
            <a class="close-reveal-modal">&#215;</a>
          </div>
          <!-- End Search for Users Modal -->
          
          <!-- Search for Groups Modal -->
          <div id="group-search" class="reveal-modal" data-reveal>
            <div class="row center">
              <div class="large-12 columns">
                <i class="fi-torsos-all size-60 blue"></i>
                <h2 class="custom-font-small-blue">Search for Groups:</h2>
                <p class="global-p">Please note: users, groups, and sessions set to “private" are not included in this search. <a href="../faq.php#F9" target="_blank">?</a></p>
                <form id="search_group" name="search_group" action="http://dev.walkgeorgia.com/search_group.php">
                  <div class="row collapse">
                    <div class="small-10 columns">
                      <input id='search' name='search' type="text" placeholder="Enter a Group Name">
                    </div>
                    <div class="small-2 columns">
                      <input type='submit' value='Go' class="button postfix">
                    </div>
                  </div>
                  <a href="#" style="float:left;" data-reveal-id="search-modal-landing" class="button tiny secondary">Back to Search Options</a>
                </form>
              </div>
            </div>
            <a class="close-reveal-modal">&#215;</a>
          </div>
          <!-- End Search for Groups Modal -->
          
          <!-- Search for Sessions Modal -->
          <div id="session-search" class="reveal-modal" data-reveal>
            <div class="row center">
              <div class="large-12 columns">
                <i class="fi-clock size-60 blue"></i>
                <h2 class="custom-font-small-blue">Search for Sessions:</h2>
                <p class="global-p">Please note: users, groups, and sessions set to “private" are not included in this search. <a href="../faq.php#F9" target="_blank">?</a></p>
                <form id="search_session" name="search_session" action="http://dev.walkgeorgia.com/search_session.php">
                  <div class="row collapse">
                    <div class="small-10 columns">
                      <input id='search' name='search' type="text" placeholder="Enter a Session Name">
                    </div>
                    <div class="small-2 columns">
                      <input type='submit' value='Go' class="button postfix">
                    </div>
                  </div>
                  <a href="#" style="float:left;" data-reveal-id="search-modal-landing" class="button tiny secondary">Back to Search Options</a>
                </form>
              </div>
            </div>
            <a class="close-reveal-modal">&#215;</a>
          </div>
          <!-- End Search for Sessions Modal -->
  
  
<!-- End Search Modal -->

            </div>
    
  <!-- End Top Bar Logged In -->
  
        	 

<!-- Begin Main Section -->
<div class="main nojava">
<!-- Begin Main Section -->
  <div class="show-for-small-only row" style="margin-top:20px;">
    <div class="large-12 columns">
      <a href="#" class="button success expand" style="margin-bottom:0px;" data-reveal-id="myModal" data-reveal>Log an Activity</a>
    </div>
  </div>
  <div class="row" style="margin-top:20px;">

    <div class="large-5 medium-6 columns">
    
      <!-- Desktop Sidebar -->
      <div class="sidebar center hide-for-small">
      
        <!-- Avatar -->
                    <a href="#" data-reveal-id="avatar" id="joyride-2">
                <div class="avatar" style="background:url(http://dev.walkgeorgia.com/img/avatar/0.png);background-position: center;background-size:cover;background-repeat: no-repeat;">
                </div>
            </a>
                  <!-- End Avatar -->
        
        <!-- Change Avatar Modal -->
        <div id="avatar" class="reveal-modal" data-reveal>
          <div class="row">
              <div class="large-12 columns">
      <form method="post" enctype="multipart/form-data">
        <div class="avatar" id="blah" style="background-image:url(http://dev.walkgeorgia.com/img/avatar/0.png);background-position: center;background-size:cover;background-repeat: no-repeat; float:left;">
        
        </div>
        <script type="text/javascript">
	      function readURL(input) {
			if (input.files && input.files[0]) {
			  var reader = new FileReader();
			  reader.readAsDataURL(input.files[0]);
			  reader.onloadend = function () {
				$("#blah").css("background-image", "url(" + this.result + ")");
			  }
			}
		  }
        </script>
        <div style="float:left; margin-left:15px; margin-top:10px;">
          <p class="global-p">Do you want to upload a new image?</p>
          <input type="file" class="" name="file_2" id="file_2" onchange="readURL(this);">
          <br />
          <input type="submit" class="button tiny" name="file" id-"file" value="Submit">
        </div>
      </form>
    </div>
     
          </div>
          <a class="close-reveal-modal">&#215;</a>
        </div>
        <!-- End Change Avatar Modal -->
        
        <br />
        
        <!-- User's Name -->
        <h2 class="custom-font-small-blue">Terry Terry</h2>
        <!-- End User's Name -->
      </div>
      <!-- End Desktop Sidebar Sidebar -->
      
      <!-- Desktop "Log an Activity" Button & Admin Tools -->
      <div class="hide-for-small" style="padding:0; margin:0;">
              <a href="#" class="button success expand" style="margin-bottom:0px;" data-reveal-id="myModal" data-reveal>Log an Activity</a>
                <a href="#" style="margin-top:0px;" data-reveal-id="county-admin-modal" class="button expand">County Admin Tools</a>
                
        <!-- County Admin Options -->
        <div id="county-admin-modal" class="reveal-modal" data-reveal>
        
          <div class="row">
            <div class="large-12 columns">
              <h2 class="global-h2">County Admin Options</h2>
              <hr />
            </div>
          </div>
          
          
          <form action="reporting_county.php">
          
          <div class="row">
            <div class="large-6 columns">
              
                <label>County of Administration
                <select name="county" id="county" required>
                <option ></option>
                <option value="Appling" >Appling</option>
                <option value="Atkinson" >Atkinson</option>
                <option value="Bacon"  >Bacon</option>
                <option value="Baker"  >Baker</option>
                <option value="Baldwin"  >Baldwin</option>
                <option value="Banks"  >Banks</option>
                <option value="Barrow"  >Barrow</option>
                <option value="Bartow"  >Bartow</option>
                <option value="Ben Hill"  >Ben Hill</option>
                <option value="Berrien"  >Berrien</option>
                <option value="Bibb"  >Bibb</option>
                <option value="Bleckley"  >Bleckley</option>
                <option value="Brantley"  >Brantley</option>
                <option value="Brooks"  >Brooks</option>
                <option value="Bryan"  >Bryan</option>
                <option value="Bulloch"  >Bulloch</option>
                <option value="Burke"  >Burke</option>
                <option value="Butts"  >Butts</option>
                <option value="Calhoun"  >Calhoun</option>
                <option value="Camden"  >Camden</option>
                <option value="Candler"  >Candler</option>
                <option value="Carroll"  >Carroll</option>
                <option value="Catoosa"  >Catoosa</option>
                <option value="Charlton"  >Charlton</option>
                <option value="Chatham"  >Chatham</option>
                <option value="Chattahoochee" >Chattahoochee</option>
                <option value="Chattooga" >Chattooga</option>
                <option value="Cherokee" >Cherokee</option>
                <option value="Clarke"  selected=selected>Clarke</option>
                <option value="Clay" >Clay</option>
                <option value="Clayton" >Clayton</option>
                <option value="Clinch" >Clinch</option>
                <option value="Cobb" >Cobb</option>
                <option value="Coffee" >Coffee</option>
                <option value="Colquitt" >Colquitt</option>
                <option value="Columbia" >Columbia</option>
                <option value="Cook" >Cook</option>
                <option value="Coweta" >Coweta</option>
                <option value="Crawford" >Crawford</option>
                <option value="Crisp" >Crisp</option>
                <option value="Dade" >Dade</option>
                <option value="Dawson" >Dawson</option>
                <option value="Decatur" >Decatur</option>
                <option value="DeKalb" >DeKalb</option>
                <option value="Dodge" >Dodge</option>
                <option value="Dooly" >Dooly</option>
                <option value="Dougherty" >Dougherty</option>
                <option value="Douglas" >Douglas</option>
                <option value="Early" >Early</option>
                <option value="Echols" >Echols</option>
                <option value="Effingham" >Effingham</option>
                <option value="Elbert" >Elbert</option>
                <option value="Emanuel" >Emanuel</option>
                <option value="Evans" >Evans</option>
                <option value="Fannin" >Fannin</option>
                <option value="Fayette" >Fayette</option>
                <option value="Floyd" >Floyd</option>
                <option value="Forsyth" >Forsyth</option>
                <option value="Franklin" >Franklin</option>
                <option value="Fulton" >Fulton</option>
                <option value="Gilmer" >Gilmer</option>
                <option value="Glascock" >Glascock</option>
                <option value="Glynn" >Glynn</option>
                <option value="Gordon" >Gordon</option>
                <option value="Grady" >Grady</option>
                <option value="Greene" >Greene</option>
                <option value="Gwinnett" >Gwinnett</option>
                <option value="Habersham" >Habersham</option>
                <option value="Hall" >Hall</option>
                <option value="Hancock" >Hancock</option>
                <option value="Haralson" >Haralson</option>
                <option value="Harris" >Harris</option>
                <option value="Hart" >Hart</option>
                <option value="Heard" >Heard</option>
                <option value="Henry" >Henry</option>
                <option value="Houston" >Houston</option>
                <option value="Irwin" >Irwin</option>
                <option value="Jackson" >Jackson</option>
                <option value="Jasper" >Jasper</option>
                <option value="Jeff Davis" >Jeff Davis</option>
                <option value="Jefferson" >Jefferson</option>
                <option value="Jenkins" >Jenkins</option>
                <option value="Johnson" >Johnson</option>
                <option value="Jones" >Jones</option>
                <option value="Lamar" >Lamar</option>
                <option value="Lanier" >Lanier</option>
                <option value="Laurens" >Laurens</option>
                <option value="Lee" >Lee</option>
                <option value="Liberty" >Liberty</option>
                <option value="Lincoln" >Lincoln</option>
                <option value="Long" >Long</option>
                <option value="Lowndes" >Lowndes</option>
                <option value="Lumpkin" >Lumpkin</option>
                <option value="Macon" >Macon</option>
                <option value="Madison" >Madison</option>
                <option value="Marion" >Marion</option>
                <option value="McDuffie" >McDuffie</option>
                <option value="McIntosh" >McIntosh</option>
                <option value="Meriwether" >Meriwether</option>
                <option value="Miller" >Miller</option>
                <option value="Mitchell" >Mitchell</option>
                <option value="Monroe" >Monroe</option>
                <option value="Montgomery" >Montgomery</option>
                <option value="Morgan" >Morgan</option>
                <option value="Murray" >Murray</option>
                <option value="Muscogee" >Muscogee</option>
                <option value="Newton" >Newton</option>
                <option value="Oconee" >Oconee</option>
                <option value="Oglethorpe" >Oglethorpe</option>
                <option value="Paulding" >Paulding</option>
                <option value="Peach" >Peach</option>
                <option value="Pickens" >Pickens</option>
                <option value="Pierce" >Pierce</option>
                <option value="Pike" >Pike</option>
                <option value="Polk" >Polk</option>
                <option value="Pulaski" >Pulaski</option>
                <option value="Putnam" >Putnam</option>
                <option value="Quitman" >Quitman</option>
                <option value="Rabun" >Rabun</option>
                <option value="Randolph" >Randolph</option>
                <option value="Richmond" >Richmond</option>
                <option value="Rockdale" >Rockdale</option>
                <option value="Schley" >Schley</option>
                <option value="Screven" >Screven</option>
                <option value="Seminole" >Seminole</option>
                <option value="Spalding" >Spalding</option>
                <option value="Stephens" >Stephens</option>
                <option value="Stewart" >Stewart</option>
                <option value="Sumter" >Sumter</option>
                <option value="Talbot" >Talbot</option>
                <option value="Taliaferro" >Taliaferro</option>
                <option value="Tattnall" >Tattnall</option>
                <option value="Taylor" >Taylor</option>
                <option value="Telfair" >Telfair</option>
                <option value="Terrell" >Terrell</option>
                <option value="Thomas" >Thomas</option>
                <option value="Tift" >Tift</option>
                <option value="Toombs" >Toombs</option>
                <option value="Towns" >Towns</option>
                <option value="Treutlen" >Treutlen</option>
                <option value="Troup" >Troup</option>
                <option value="Turner" >Turner</option>
                <option value="Twiggs" >Twiggs</option>
                <option value="Union" >Union</option>
                <option value="Upson" >Upson</option>
                <option value="Walker" >Walker</option>
                <option value="Walton" >Walton</option>
                <option value="Ware" >Ware</option>
                <option value="Warren" >Warren</option>
                <option value="Washington" >Washington</option>
                <option value="Wayne" >Wayne</option>
                <option value="Webster" >Webster</option>
                <option value="Wheeler" >Wheeler</option>
                <option value="White" >White</option>
                <option value="Whitfield" >Whitfield</option>
                <option value="Wilcox" >Wilcox</option>
                <option value="Wilkes" >Wilkes</option>
                <option value="Wilkinson" >Wilkinson</option>
                <option value="Worth" >Worth</option>                       
              </select>
            </label>
              
            </div>
          </div>
          <div class="row">
            <div class="large-6 columns">
              <!--<a href="#" class="button expand tiny">Generate Report on Groups</a>-->
              <input type="submit" class="button expand tiny" value="Generate County Report">
              
            </div>
          </div>
          
          </form>
      
        <a class="close-reveal-modal">&#215;</a>
        </div>
        <!-- End County Admin Options -->
        
        
      </div>
      <!-- End Desktop "Log an Activity" & Admin Tools Button -->
    
    </div>
    
    <div class="large-7 medium-6 small-12 columns">
    
      <!-- Quick Stats -->
      <div>
            

      <!-- Quick Stats Interface -->
      
      <h2 class="custom-font-small-blue" style="padding:0px 10px 0px 10px;;">Quick Stats</h2>
      <hr style="margin-top:-5px; padding:0px 10px 0px 10px;" />
      
         <!-- Points -->
      <div class="blue-bg-link" id="points-toggle" style="padding: 10px 20px 10px 20px; margin: 0 0 0 0; cursor:pointer;">
        
        <h2 class="global-h2-white"><b></b></h2>
        <h2 class="custom-font-big-white" style="margin-bottom:-20px;">3</h2>
        <h3 class="global-h2-white" style="margin-top:-20px;">Points Earned</h3>
        <div class="expand-icon" style="margin-top:-1em;"><i class="fi-list size-24"></i></div>
        
        <!-- Points Breakdown -->
        <div id="points-breakdown" style="display:none;">
          <ul class="global-p-white">
            							<li>Active Stretching: 3 points</li>
						          </ul>
          <a href="faq.php#C3" target="_blank" class="button round tiny">What are Points?</a>
        </div>
        <!-- End Points Breakdown -->
        
      </div>
      <!-- End Points -->
      
      <!-- Time -->
      <div class="peach-bg-link" id="time-toggle" style="padding: 10px 20px 10px 20px; margin: 0 0 0 0; cursor:pointer;">
        <div style="display:block; margin-bottom:-15px;">
          <h3 class="global-h2-white"><b>Time Exercised:</b></h3>
        </div>
        <!-- Hours -->
        <div style="display:inline-block; margin-right:15px;">
          <h2 class="custom-font-big-white" style="margin-bottom:-20px;">0</h2>  
          <h3 class="global-h2-white" style="margin-top:-20px;"><b>Hours</b></h3> 
        </div>
        <!-- End Hours -->
        <!-- Minutes -->
        <div style="display:inline-block"> 
          <h2 class="custom-font-big-white" style="margin-bottom:-20px;">2</h2>  
          <h3 class="global-h2-white" style="margin-top:-20px;"><b>Minutes</b></h3> 
        </div>
        <div class="expand-icon" style="margin-top:0.05em;"><i class="fi-list size-24"></i></div>
        <!-- End Minutes -->

        <!-- Time Breakdown -->
        <div id="time-breakdown" style="display:none;">
          <ul class="global-p-white">
                                <li>
					Active Stretching: 2m                    </li>
                              </ul>
        </div>
        <!-- End Time Breakdown -->
        
      </div>
      <!-- End Time -->
      
      <!-- Distance -->
      <div class="green-bg-link" id="distance-toggle" style="padding: 10px 20px 10px 20px; margin: 0 0 0 0; cursor:pointer;">
        <div class="expand-icon" style="margin-top:0.2em;"><i class="fi-list size-24"></i></div>
        <div style="display:block; margin-bottom:-15px;">
          <h3 class="global-h2-white"><b>Distance Traveled:</b></h3>
        </div>
        <!-- Miles -->
        <div style="display:inline-block; margin-right:15px;">
          <h2 class="custom-font-big-white" style="margin-bottom:-20px;">0</h2>  
          <h3 class="global-h2-white" style="margin-top:-20px;"><b>Miles</b></h3> 
        </div>
        <!-- End Miles -->

        <!-- Distance Breakdown -->
        <div id="distance-breakdown" style="display:none;">
          <ul class="global-p-white">
                      </ul>
          <a href="faq.php#D3" target="_blank" class="button round success tiny">What Does This Mean?</a>
        </div>
        <!-- End Distance Breakdown -->
        
      </div>
      <!-- End Distance -->
      
      <!-- End Quick Stats Interface -->
    
         
      </div>
      <!-- End Quick Stats -->
      
    </div>
    
  </div>
  
  <div class="row">
  
    <div class="large-8 medium-8 columns">
    
      <!-- Day by Day Activity Log -->
      <div>
                <div style="margin-top:20px;">
          <a href="#" id="toggle4"><h2 class="custom-font-small-blue">Past Activity</h2></a><hr />
            <div class="toggle4">
                        <a href="#"  data-reveal-id="M_36901" onclick="su_magic('77', '36901', '0');">
              <div data-alert class="alert-box secondary">
                <b>2015-08-05</b> - Active Stretching for 2 minutes, and it was easy. You earned 3 points.              </div>
            </a>
            <div id="M_36901" class="reveal-modal" data-reveal>
              <a class="close-reveal-modal">&#215;</a>
              <div class="row">
                <div class="large-12 columns">
                  <h2 class="global-h2">Edit an Activity</h2>
                  <hr />
                </div>
              </div>
              <form id="36901" action="edit_activity.php" method="post" data-abide>
                <div class="row">
                  <div class="large-6 columns">
                	<div class="row" id="dp3">
     
           <!-- Month -->
             <div class="large-4 columns">
               <label>Month
                 <select id="month" name="month" required>
                   <option value="1" >01 January</option>
                   <option value="2" >02 February</option>
                   <option value="3" >03 March</option>
                   <option value="4" >04 April</option>
                   <option value="5" >05 May</option>
                   <option value="6" >06 June</option>
                   <option value="7" >07 July</option>
                   <option value="8" selected="selected">08 August</option>
                   <option value="9" >09 September</option>
                   <option value="10" >10 October</option>
                   <option value="11" >11 November</option>
                   <option value="12" >12 December</option>
                 </select>
               </label>
             </div>
           <!-- End Month -->
         
           <!-- Day -->
             <div class="large-4 columns">
               <label>Day
                  <select id="day" name="day" required>
                   <option value="1" >01</option>
                   <option value="2" >02</option>
                   <option value="3" >03</option>
                   <option value="4" >04</option>
                   <option value="5" selected="selected">05</option>
                   <option value="6" >06</option>
                   <option value="7" >07</option>
                   <option value="8" >08</option>
                   <option value="9" >09</option>
                   <option value="10" >10</option>
                   <option value="11" >11</option>
                   <option value="12" >12</option>
                   <option value="13" >13</option>
                   <option value="14" >14</option>
                   <option value="15" >15</option>
                   <option value="16" >16</option>
                   <option value="17" >17</option>
                   <option value="18" >18</option>
                   <option value="19" >19</option>
                   <option value="20" >20</option>
                   <option value="21" >21</option>
                   <option value="22" >22</option>
                   <option value="23" >23</option>
                   <option value="24" >24</option>
                   <option value="25" >25</option>
                   <option value="26" >26</option>
                   <option value="27" >27</option>
                   <option value="28" >28</option>
                   <option value="29" >29</option>
                   <option value="30" >30</option>
                   <option value="31" >31</option>
                 </select>
               </label>
             </div>
           <!-- End Day -->
         
           <!-- Year -->
             <div class="large-4 columns">
               <label>Year
                 <select id="year" name="year" required>
                   <option value="2008" >2008</option>
                   <option value="2009" >2009</option>
                   <option value="2010" >2010</option>
                   <option value="2011" >2011</option>
                   <option value="2012" >2012</option>
                   <option value="2013" >2013</option>
                   <option value="2014" >2014</option>
                   <option value="2015" selected="selected">2015</option>
                 </select>
               </label>
             </div>
           <!-- End Year -->
           </div>
                      
                  </div>
                </div>
                <div class="row">
                  <div class="large-6 columns">
                    <label>Activity:
                      <select id="exercise" name="srch" onchange="su_magic(this, '36901');">
                                  <option id="77" name="Active Stretching" value="0:77" selected="selected">Active Stretching</option>
                        <option id="75" name="Badminton" value="0:75" >Badminton</option>
                        <option id="17" name="Basketball" value="0:17" >Basketball</option>
                        <option id="1" name="Biking" value="1:1" >Biking</option>
                        <option id="18" name="Bowling" value="0:18" >Bowling</option>
                        <option id="19" name="Boxing" value="0:19" >Boxing</option>
                        <option id="65" name="Calisthenics" value="0:65" >Calisthenics</option>
                        <option id="48" name="Canoeing" value="0:48" >Canoeing</option>
                        <option id="74" name="Cardio Session" value="0:74" >Cardio Session</option>
                        <option id="14" name="Carpentry" value="0:14" >Carpentry</option>
                        <option id="13" name="Child Care" value="0:13" >Child Care</option>
                        <option id="66" name="Circuit Training" value="0:66" >Circuit Training</option>
                        <option id="11" name="Cleaning" value="0:11" >Cleaning</option>
                        <option id="59" name="CrossFit" value="0:59" >CrossFit</option>
                        <option id="6" name="Dancing (Aerobic)" value="0:6" >Dancing (Aerobic)</option>
                        <option id="5" name="Dancing (Ballet, Modern, Jazz, Tap)" value="0:5" >Dancing (Ballet, Modern, Jazz, Tap)</option>
                        <option id="73" name="Disc Golf" value="0:73" >Disc Golf</option>
                        <option id="76" name="Elliptical" value="0:76" >Elliptical</option>
                        <option id="25" name="Field Hockey" value="0:25" >Field Hockey</option>
                        <option id="7" name="Fishing" value="0:7" >Fishing</option>
                        <option id="20" name="Football (Competitive)" value="0:20" >Football (Competitive)</option>
                        <option id="22" name="Frisbee (General)" value="0:22" >Frisbee (General)</option>
                        <option id="21" name="Frisbee (Ultimate)" value="0:21" >Frisbee (Ultimate)</option>
                        <option id="16" name="Gardening" value="0:16" >Gardening</option>
                        <option id="23" name="Golf (General)" value="0:23" >Golf (General)</option>
                        <option id="24" name="Gymnastics (General)" value="0:24" >Gymnastics (General)</option>
                        <option id="47" name="Hiking" value="1:47" >Hiking</option>
                        <option id="27" name="Horseback Riding" value="0:27" >Horseback Riding</option>
                        <option id="8" name="Hunting" value="0:8" >Hunting</option>
                        <option id="26" name="Ice Hockey" value="0:26" >Ice Hockey</option>
                        <option id="54" name="Ice Skating" value="0:54" >Ice Skating</option>
                        <option id="29" name="Juggling" value="0:29" >Juggling</option>
                        <option id="36" name="Jumping Rope" value="5:36" >Jumping Rope</option>
                        <option id="68" name="Kayaking" value="1:68" >Kayaking</option>
                        <option id="30" name="Kickball" value="0:30" >Kickball</option>
                        <option id="72" name="Kickboxing" value="0:72" >Kickboxing</option>
                        <option id="31" name="Lacrosse" value="0:31" >Lacrosse</option>
                        <option id="15" name="Lawn Mowing (Push Mower)" value="0:15" >Lawn Mowing (Push Mower)</option>
                        <option id="28" name="Martial Arts" value="0:28" >Martial Arts</option>
                        <option id="10" name="Mopping" value="0:10" >Mopping</option>
                        <option id="32" name="Motor-Cross" value="0:32" >Motor-Cross</option>
                        <option id="79" name="Pilates" value="0:79" >Pilates</option>
                        <option id="64" name="Planks" value="2:64" >Planks</option>
                        <option id="33" name="Polo" value="0:33" >Polo</option>
                        <option id="63" name="Pull-ups" value="2:63" >Pull-ups</option>
                        <option id="61" name="Push-ups" value="2:61" >Push-ups</option>
                        <option id="34" name="Racquetball" value="0:34" >Racquetball</option>
                        <option id="78" name="Recumbent Biking" value="1:78" >Recumbent Biking</option>
                        <option id="35" name="Rock Climbing" value="0:35" >Rock Climbing</option>
                        <option id="58" name="Rowing" value="5:58" >Rowing</option>
                        <option id="37" name="Rugby" value="0:37" >Rugby</option>
                        <option id="2" name="Running / Jogging" value="1:2" >Running / Jogging</option>
                        <option id="62" name="Sit-ups" value="2:62" >Sit-ups</option>
                        <option id="38" name="Skateboarding" value="0:38" >Skateboarding</option>
                        <option id="56" name="Skiing" value="0:56" >Skiing</option>
                        <option id="39" name="Soccer" value="0:39" >Soccer</option>
                        <option id="40" name="Softball" value="0:40" >Softball</option>
                        <option id="55" name="Speed Skating" value="0:55" >Speed Skating</option>
                        <option id="69" name="Spinning" value="0:69" >Spinning</option>
                        <option id="71" name="Squats" value="2:71" >Squats</option>
                        <option id="70" name="Stair Climbing" value="1:70" >Stair Climbing</option>
                        <option id="49" name="Surfing" value="0:49" >Surfing</option>
                        <option id="9" name="Sweeping" value="0:9" >Sweeping</option>
                        <option id="50" name="Swimming" value="1:50" >Swimming</option>
                        <option id="41" name="Table Tennis" value="0:41" >Table Tennis</option>
                        <option id="42" name="Tai Chi" value="0:42" >Tai Chi</option>
                        <option id="43" name="Tennis" value="0:43" >Tennis</option>
                        <option id="46" name="Track and Field" value="0:46" >Track and Field</option>
                        <option id="12" name="Vacuuming" value="0:12" >Vacuuming</option>
                        <option id="44" name="Volleyball" value="0:44" >Volleyball</option>
                        <option id="3" name="Walking" value="1:3" >Walking</option>
                        <option id="51" name="Water Aerobics" value="0:51" >Water Aerobics</option>
                        <option id="52" name="Water Polo" value="0:52" >Water Polo</option>
                        <option id="53" name="Water Volleyball" value="0:53" >Water Volleyball</option>
                        <option id="57" name="Weight Lifting" value="6:57" >Weight Lifting</option>
                        <option id="45" name="Wrestling" value="0:45" >Wrestling</option>
                        <option id="67" name="Yard Work" value="0:67" >Yard Work</option>
                        <option id="4" name="Yoga" value="0:4" >Yoga</option>
                        <option id="60" name="Zumba" value="0:60" >Zumba</option>
                                  </select>
                    </label>
                  </div>
                </div>
                <div class="row">
                  <div class="large-12 columns">
                    <p class="global-p">Time Spent Exercising:</p>
                    <hr style="margin-top:-5px; margin-bottom:5px;" />
                  </div>
                </div>
                <div class="row">
                  <div class="large-5 columns">
                    <label>Minutes:
                      <input type="number" id="minutes" name ="minutes" value="2" required>
                    </label>
                  </div>
                </div>
                <div class="row" id="su36901" name = "su36901">
                  
                </div>
                <div class="row">
                  <div class="large-12 columns">
                    <!-- I would just copy this and some of the other elements over from the existing form
                    This is really just a placeholder for you -->
                    <label>How difficult was it to complete?
                      <select name="sd" id="sd">
                        <option value="1" selected="selected">Easy</option>
                        <option value="2" >Somewhat difficult</option>
                        <option value="3" >Difficult</option>
                        <option value="4" >More difficult than usual</option>
                        <option value="5" >Very difficult</option>
                      </select>
                    </label>
            
                  </div>
                </div>
                <div class="row">
                  <div class="large-12 columns">
                  	<input type="hidden" id="activity_log_id" name="activity_log_id" value="36901" />
                    <input type="submit"  id="submit" class="tiny button" value="Submit"/> <a href="delete_activity.php?aid=36901" class="button tiny alert">Delete Activity</a>              
                  </div>
                </div>
              </form>
              
            </div>
                <a href="past-activity.php" class="button tiny">Show More Past Activity</a>      
      
      </div>
    </div>
                <!-- DISTANCE ACTIVITY LOGGING FORM -->
 
            <div id="myModal" class="reveal-modal" data-reveal>
            <a class="close-reveal-modal">&#215;</a>
              <div class="row">
                <div class="large-12 columns">
                <h2 class="custom-font-small-blue">Log an Activity</h2>
                <hr />
                </div>
              </div>
              <form id="modal-form" action="log_activity.php" method="post" data-abide>
                <div class="row">
                  <div class="large-6 medium-12 columns">
                    <h3 class="global-h2">Date of Activity:</h3>
                	<div class="row date">
                  	  
     
           <!-- Month -->
                        <div class="large-4 medium-4 columns">
               <label>Month
                 <select id="month" name="month" required>
                   <option value="1" >01 January</option>
                   <option value="2" >02 February</option>
                   <option value="3" >03 March</option>
                   <option value="4" >04 April</option>
                   <option value="5" >05 May</option>
                   <option value="6" >06 June</option>
                   <option value="7" >07 July</option>
                   <option value="8" selected="selected">08 August</option>
                   <option value="9" >09 September</option>
                   <option value="10" >10 October</option>
                   <option value="11" >11 November</option>
                   <option value="12" >12 December</option>
                 </select>
               </label>
             </div>
           <!-- End Month -->
         
           <!-- Day -->
             <div class="large-4 medium-4 columns">
               <label>Day
                 <select id="day" name="day" required>
                   <option value="1" >01</option>
                   <option value="2" >02</option>
                   <option value="3" >03</option>
                   <option value="4" >04</option>
                   <option value="5" >05</option>
                   <option value="6" >06</option>
                   <option value="7" >07</option>
                   <option value="8" >08</option>
                   <option value="9" >09</option>
                   <option value="10" >10</option>
                   <option value="11" >11</option>
                   <option value="12" >12</option>
                   <option value="13" >13</option>
                   <option value="14" >14</option>
                   <option value="15" >15</option>
                   <option value="16" >16</option>
                   <option value="17" >17</option>
                   <option value="18" >18</option>
                   <option value="19" >19</option>
                   <option value="20" >20</option>
                   <option value="21" >21</option>
                   <option value="22" >22</option>
                   <option value="23" >23</option>
                   <option value="24" selected="selected">24</option>
                   <option value="25" >25</option>
                   <option value="26" >26</option>
                   <option value="27" >27</option>
                   <option value="28" >28</option>
                   <option value="29" >29</option>
                   <option value="30" >30</option>
                   <option value="31" >31</option>
                 </select>
               </label>
             </div>
           <!-- End Day -->
         
           <!-- Year -->
             <div class="large-4 medium-4 columns">
               <label>Year
                 <select id="year" name="year" required>
                   <option value="2008" >2008</option>
                   <option value="2009" >2009</option>
                   <option value="2010" >2010</option>
                   <option value="2011" >2011</option>
                   <option value="2012" >2012</option>
                   <option value="2013" >2013</option>
                   <option value="2014" >2014</option>
                   <option value="2015" selected="selected">2015</option>
                 </select>
               </label>
             </div>
           <!-- End Year -->
           
                      
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="large-6 medium-6 columns">
                  <h3 class="global-h2">Activity:</h3>
                  <div class="row">
                    <div class="large-12 columns">
                    
                    <label>Activity:
                      <select id="exercise" name="srch" onchange="su_magic(this, '');" class="excercise" required>
                                     <option id="77" name="Active Stretching" value="0:77" selected="selected">Active Stretching</option>
                        <option id="75" name="Badminton" value="0:75" >Badminton</option>
                        <option id="17" name="Basketball" value="0:17" >Basketball</option>
                        <option id="1" name="Biking" value="1:1" >Biking</option>
                        <option id="18" name="Bowling" value="0:18" >Bowling</option>
                        <option id="19" name="Boxing" value="0:19" >Boxing</option>
                        <option id="65" name="Calisthenics" value="0:65" >Calisthenics</option>
                        <option id="48" name="Canoeing" value="0:48" >Canoeing</option>
                        <option id="74" name="Cardio Session" value="0:74" >Cardio Session</option>
                        <option id="14" name="Carpentry" value="0:14" >Carpentry</option>
                        <option id="13" name="Child Care" value="0:13" >Child Care</option>
                        <option id="66" name="Circuit Training" value="0:66" >Circuit Training</option>
                        <option id="11" name="Cleaning" value="0:11" >Cleaning</option>
                        <option id="59" name="CrossFit" value="0:59" >CrossFit</option>
                        <option id="6" name="Dancing (Aerobic)" value="0:6" >Dancing (Aerobic)</option>
                        <option id="5" name="Dancing (Ballet, Modern, Jazz, Tap)" value="0:5" >Dancing (Ballet, Modern, Jazz, Tap)</option>
                        <option id="73" name="Disc Golf" value="0:73" >Disc Golf</option>
                        <option id="76" name="Elliptical" value="0:76" >Elliptical</option>
                        <option id="25" name="Field Hockey" value="0:25" >Field Hockey</option>
                        <option id="7" name="Fishing" value="0:7" >Fishing</option>
                        <option id="20" name="Football (Competitive)" value="0:20" >Football (Competitive)</option>
                        <option id="22" name="Frisbee (General)" value="0:22" >Frisbee (General)</option>
                        <option id="21" name="Frisbee (Ultimate)" value="0:21" >Frisbee (Ultimate)</option>
                        <option id="16" name="Gardening" value="0:16" >Gardening</option>
                        <option id="23" name="Golf (General)" value="0:23" >Golf (General)</option>
                        <option id="24" name="Gymnastics (General)" value="0:24" >Gymnastics (General)</option>
                        <option id="47" name="Hiking" value="1:47" >Hiking</option>
                        <option id="27" name="Horseback Riding" value="0:27" >Horseback Riding</option>
                        <option id="8" name="Hunting" value="0:8" >Hunting</option>
                        <option id="26" name="Ice Hockey" value="0:26" >Ice Hockey</option>
                        <option id="54" name="Ice Skating" value="0:54" >Ice Skating</option>
                        <option id="29" name="Juggling" value="0:29" >Juggling</option>
                        <option id="36" name="Jumping Rope" value="5:36" >Jumping Rope</option>
                        <option id="68" name="Kayaking" value="1:68" >Kayaking</option>
                        <option id="30" name="Kickball" value="0:30" >Kickball</option>
                        <option id="72" name="Kickboxing" value="0:72" >Kickboxing</option>
                        <option id="31" name="Lacrosse" value="0:31" >Lacrosse</option>
                        <option id="15" name="Lawn Mowing (Push Mower)" value="0:15" >Lawn Mowing (Push Mower)</option>
                        <option id="28" name="Martial Arts" value="0:28" >Martial Arts</option>
                        <option id="10" name="Mopping" value="0:10" >Mopping</option>
                        <option id="32" name="Motor-Cross" value="0:32" >Motor-Cross</option>
                        <option id="79" name="Pilates" value="0:79" >Pilates</option>
                        <option id="64" name="Planks" value="2:64" >Planks</option>
                        <option id="33" name="Polo" value="0:33" >Polo</option>
                        <option id="63" name="Pull-ups" value="2:63" >Pull-ups</option>
                        <option id="61" name="Push-ups" value="2:61" >Push-ups</option>
                        <option id="34" name="Racquetball" value="0:34" >Racquetball</option>
                        <option id="78" name="Recumbent Biking" value="1:78" >Recumbent Biking</option>
                        <option id="35" name="Rock Climbing" value="0:35" >Rock Climbing</option>
                        <option id="58" name="Rowing" value="5:58" >Rowing</option>
                        <option id="37" name="Rugby" value="0:37" >Rugby</option>
                        <option id="2" name="Running / Jogging" value="1:2" >Running / Jogging</option>
                        <option id="62" name="Sit-ups" value="2:62" >Sit-ups</option>
                        <option id="38" name="Skateboarding" value="0:38" >Skateboarding</option>
                        <option id="56" name="Skiing" value="0:56" >Skiing</option>
                        <option id="39" name="Soccer" value="0:39" >Soccer</option>
                        <option id="40" name="Softball" value="0:40" >Softball</option>
                        <option id="55" name="Speed Skating" value="0:55" >Speed Skating</option>
                        <option id="69" name="Spinning" value="0:69" >Spinning</option>
                        <option id="71" name="Squats" value="2:71" >Squats</option>
                        <option id="70" name="Stair Climbing" value="1:70" >Stair Climbing</option>
                        <option id="49" name="Surfing" value="0:49" >Surfing</option>
                        <option id="9" name="Sweeping" value="0:9" >Sweeping</option>
                        <option id="50" name="Swimming" value="1:50" >Swimming</option>
                        <option id="41" name="Table Tennis" value="0:41" >Table Tennis</option>
                        <option id="42" name="Tai Chi" value="0:42" >Tai Chi</option>
                        <option id="43" name="Tennis" value="0:43" >Tennis</option>
                        <option id="46" name="Track and Field" value="0:46" >Track and Field</option>
                        <option id="12" name="Vacuuming" value="0:12" >Vacuuming</option>
                        <option id="44" name="Volleyball" value="0:44" >Volleyball</option>
                        <option id="3" name="Walking" value="1:3" >Walking</option>
                        <option id="51" name="Water Aerobics" value="0:51" >Water Aerobics</option>
                        <option id="52" name="Water Polo" value="0:52" >Water Polo</option>
                        <option id="53" name="Water Volleyball" value="0:53" >Water Volleyball</option>
                        <option id="57" name="Weight Lifting" value="6:57" >Weight Lifting</option>
                        <option id="45" name="Wrestling" value="0:45" >Wrestling</option>
                        <option id="67" name="Yard Work" value="0:67" >Yard Work</option>
                        <option id="4" name="Yoga" value="0:4" >Yoga</option>
                        <option id="60" name="Zumba" value="0:60" >Zumba</option>
                                  </select>
                    </label>
                    
                    </div>
                    </div>
                    
                  </div>
                </div>
                <div class="row">
                  <div class="large-6 medium-6 columns">
                    <h3 class="global-h2">Time Spent Exercising:</h3>
                  </div>
                </div>
                <div class="row">
                  <div class="large-2 medium-2 columns">
                    <label>Minutes:
                      <input type="number" id="minutes" name ="minutes" min="1" max='480' required>
                    </label>
                  </div>
                  <div class="large-8 medium-8 columns"></div>
                </div>
                <div class="row">
                <div class="large-6 columns" id="su" name = "su"> 
                </div>
                </div>
                <div class="row">
                  <div class="large-6 medium-6 columns">
                    <h3 class="global-h2">Difficulty:</h3>
                    <label>How difficult was it to complete?
                      <select name="sd" id="sd" required>
                        <option value="1">Easy</option>
                        <option value="2">Somewhat difficult</option>
                        <option value="3">Difficult</option>
                        <option value="4">More difficult than usual</option>
                        <option value="5">Very difficult</option>
                      </select>
                    </label>
                  </div>
                </div>
                <div class="row">
                  <div class="large-12 columns">
                    <input type="submit"  id="submit" class="tiny button" value="Submit"/>                
                  </div>
                </div>
              </form>
              
            </div>
                  </div>
      <!-- End Day by Day Activity Log -->
      
    </div>
    
    <div class="large-4 medium-4 columns" style="margin-top:16px;">
      <h2 class="custom-font-small-blue">News</h2>
      <hr />
      <div class="global-p">
      <script language="JavaScript" src="http://feed2js.org//feed2js.php?src=http%3A%2F%2Fblog.extension.uga.edu%2Fwalkgeorgia%2Ffeed%2F&chan=y&num=3&desc=100>1&targ=y&utf=y"  charset="UTF-8" type="text/javascript"></script>
      </div>
      <noscript>
        <a href="http://feed2js.org//feed2js.php?src=http%3A%2F%2Fblog.extension.uga.edu%2Fwalkgeorgia%2Ffeed%2F&chan=y&num=3&desc=100>1&targ=y&utf=y&html=y">View RSS feed</a>
      </noscript>
      
    </div>
    
  </div> 
  
  <div class="row">
    <div class="large-6 medium-6 columns">
      <h2 class="custom-font-small-blue">Help Center</h2>
      <hr style="margin-top:-5px;" />
      <ul style="list-style:none;">
        <li><a href="/faq.php">Need help using the new system? View our F.A.Q!</a></li>
        <li><a href="/faq.php#I1">Where's the map?</a></li>
        <li><a href="/resources/county-resources.php">Find local resources in my county.</a></li>
      </ul>
    </div>
  </div>



<!-- End Main Section -->  
</div>
<!-- End Main Section -->

  <!-- Joyride -->
  <ol class="joyride-list" data-joyride>
  
    <li data-text="Next">
      <br />
      <p>Welcome to walk Georgia!  Here is a quick tour of your profile.</p>
    </li>
    
    <li data-text="Next">
      <br />
      <p>On the left you can click on the default picture to upload one of your own.  Below that, you can click "Log an Activity" to get started.</p>
    </li>
    
    <li data-text="Next">
      <p>Once you log an activity, your data will start to show up in the "past activity" section.  Clicking on one of your past activities will allow you to edit it if you entered it incorrectly the first time.</p>
    </li>
    
    <li data-text="Next">
      <p>The quick stats section lets you keep track of your progress.  Clicking on any of the sections will show a breakdown of where your total points, time, and distance came from.</p>
    </li>
    
    <li data-text="Next">
      <br />
      <p>The Groups section lets you create or join groups to get connected with friends, co-workes, or relatives.  Groups can help you keep track of the activity of multiple users at a time, as well as encourage friendly competition.</p>
    </li>
    
    <li data-button="End">
      <br />
      <p>That does it for our tour! Before you REALLY get going logging activity, we've got an optional survey to help make our site better.  <a href="https://ugeorgia.qualtrics.com/jfe/form/SV_6RPBzM9OKJgtW1D" target="_blank">If you'd like to participate in the survey, click here now</a>.  If not, continue on to your new account, move more, and live more!</p>
    </li>
  </ol>
  <!-- End Joyride -->
<div id="facebookShare" class="reveal-modal tiny" data-reveal aria-labelledby="facebookShare" aria-hidden="true" role="dialog">
    	<div class="row center">
        	<div class="large-12 columns">
            	<h3 class="custom-font-small-blue">You Logged:</h3>
                <h3 class="custom-font-big-blue" style="margin-top:0;"><br />
<b>Notice</b>:  Undefined index: pa in <b>F:\inetpub\wwwroot\dev\TEST PAGE.php</b> on line <b>384</b><br />
</h3>
                <h4 class="global-h2" style="margin-top:-1.5em;">Points</h4>
                
                <hr />
                <a href="http://dev.walkgeorgia.com" class="button small round secondary">Nice! Take Me Back to My Activity</a>
                <hr style="margin-top:-.3em;" />
                
            </div>
        </div>
        
        <div class="row center">
        	<div class="large-12 columns">
            	Share on Facebook: <div class="fb-share-button" data-href="http://www.walkgeorgia.com?name=Terry&points=<br />
<b>Notice</b>:  Undefined index: pa in <b>F:\inetpub\wwwroot\dev\TEST PAGE.php</b> on line <b>396</b><br />
" data-layout="button_count"></div>
            </div>
        </div>
</div>
  <!-- Footer -->
    <div class="footer">
      <div class="row">
        <div>
          <a class="tiny button alert" href="http://dev.walkgeorgia.com/contact-us.php"> Report a Bug </a>
        </div>
        
        <p class="global-p">
          <a href="http://www.facebook.com/walkgeorgia">Facebook</a> | <a href="http://twitter.com/walkga">Twitter</a> | <a href="http://blog.extension.uga.edu/walkgeorgia" target="_blank">Walk Georgia Blog</a>
        </p>
        <br />
          <small>The College of Agricultural and Environmental Sciences and the College of Family and Consumer Sciences cooperating.
The University of Georgia &copy; 2014. All Rights Reserved.</small>
          <hr><img alt="The University of Georgia Cooperative Extension" src="http://dev.walkgeorgia.com/img/ext.png"><br><br>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="http://dev.walkgeorgia.com/js/log_form.js"></script>
    <script type="text/javascript" src="http://dev.walkgeorgia.com/js/frontend.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://dev.walkgeorgia.com/js/foundation.min.js"></script>
    <script>
     $(document).foundation();
    </script>
 	<script type="text/javascript" src="js/foundation.min.js"></script>
    <script type="text/javascript" src="js/sha512.js"></script>
    <script type="text/javascript" src="js/log_form.js"></script>
    <script type="text/javascript" src="js/frontend.js"></script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script>
      $(document).foundation();
	</script>
    
    <script>
    
	$(document).ready(function() {
		$("#index-slideshow > div.slide:gt(0)").hide();

setInterval(function() { 
  $('#index-slideshow > div.slide:first')
    .fadeOut(2000)
    .next()
    .fadeIn(2000)
    .end()
    .appendTo('#index-slideshow');
},  5000);
	});
    
    </script>
    
      
     
  <!-- End Footer -->
  </body>
</html>