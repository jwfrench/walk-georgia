<html xmlns="http://www.w3.org/1999/xhtml"
  xmlns:og="http://ogp.me/ns#"
  xmlns:fb="http://www.facebook.com/2008/fbml">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta property="fb:app_id"          content="276401669208985" /> 
    <meta property="og:type"            content="website" /> 
    <meta property="og:url"             content="http://www.walkgeorgia.com" /> 
    <meta property="og:title"           content="Activity Logged!" /> 
    <meta property="og:site_name" 		content="Walk Georgia"/>
    <meta property="og:image"           content="https://dev.walkgeorgia.com/" />
    <meta property="og:description"    	content="You just logged your physical activity with Walk Georgia! We're stoked you use our site to help keep fit!" />
    <meta property="og:locale" content="en_US" />
  </head>
  <body>
  </body>
</html>