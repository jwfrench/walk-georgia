<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/back_api.php");
include_once ("$root/lib/groups_api.php"); 
$ss = SESSION::secure_session();
//EDITING STARTS HERE
if(filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING)!= null){
    $info = GROUP::get_info(filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING));
}
//If the user is logged in 
if($_SESSION['valid']){
	$redirect = REDIRECT::home();
}
//If user is not logged in
else{
	if(filter_input(INPUT_POST, 'FN', FILTER_SANITIZE_STRING) != null){
            $userID = ACCOUNT::register();
            $UUID=odbc_result(MSSQL::query('SELECT L_UUID FROM LOGIN WHERE L_ID=\''.$userID.'\''), 'L_UUID');
            $user_data_query = 'INSERT INTO USER_DATA VALUES (\''.$UUID.'\',\''.filter_input(INPUT_POST, 'birth-year', FILTER_SANITIZE_STRING).'\',\''.filter_input(INPUT_POST, 'sex', FILTER_SANITIZE_STRING).'\')';
            $user_data_insert = MSSQL::query($user_data_query);
            
            if(filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING)){
                $group = GROUP::join_group(filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING), $userID);
            } 
            $redirect = REDIRECT::login('You may now log in using your registered username and password!');
	}
}
