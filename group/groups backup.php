<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/groups_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if($_GET['logout']==1){
	SESSION::logout();
	REDIRECT::home();
}
if(isset($_POST['file'])){
	$img = GROUP::avatar_upload($_GET['group']);
}
if($_GET['join']){
	$join_group = GROUP::join_group($_GET['group'], $_SESSION['ID']);
}
if($_GET['leave']){
	$leave_group = GROUP::leave_group($_GET['group'], $_SESSION['ID']);
}
HTML_ELEMENT::head('Groups');
HTML_ELEMENT::top_nav();
//EDIT HERE
//If the group exists
if($_GET['group'] && ($info = GROUP::get_info($_GET['group']))){
?> 
<div class="main">
  <div class="row">
  <div class="large-12 columns">
  <?php if(isset($_GET['err_msg'])){?>
    <div data-alert class="alert-box alert center">
      <?php echo $_GET['err_msg'];?></a>
      <a href="#" class="close">&times;</a>
    </div>
        <?php
	}?>
    </div>
    <!-- Desktop Sidebar -->
    <div class="large-5 medium-5 small-12 columns">
      <div class="hide-for-small" style="margin-bottom:20px;">
        <div class="sidebar">
          <div align="center">
            <?php
          if (file_exists($_SERVER['DOCUMENT_ROOT']."/group/img/avatar/".$_GET['group'].".png")) {
			  $filename = "/group/img/avatar/".$_GET['group'].".png";
		  } else {
    			$filename = "../img/default-group-profile.png";
		  }
		  if('U.'.$_SESSION['ID'] == $info['G_PID'] || $isAdmin){
		  ?>
            <!-- Avatar -->
            <a href="#" data-reveal-id="avatar">
            <?php
		  }
			?>
            <img alt="Group Picture" src="<?php echo $filename; ?>" width="240px" height="240px" style="border-radius:50%;"/>
            </a>
            <!-- End Avatar -->
            <!-- Change Avatar Modal -->
            <div id="avatar" class="reveal-modal" data-reveal>
              <div class="row">
                <div class="large-12 columns">
                  <p class="global-p">Do you want to upload a new image?</p>
                  <form method="post" enctype="multipart/form-data">
                    <img id="blah" src="<?php echo $filename; ?>" alt="your image" height="240px;" width="240px;" style="border-radius:50%;" />
                    <br />
                    <br />
                    <script type="text/javascript">
	        			function readURL(input) {
			        	    if (input.files && input.files[0]) {
			    	            var reader = new FileReader();
			    	            reader.onload = function (e) {
				                    $('#blah').attr('src', e.target.result);
				                }
				                reader.readAsDataURL(input.files[0]);
				            }
			    	    }
				    </script>
                    <input type="file" name="group_img" id="group_img" onchange="readURL(this);">
                    <br>
                    <input type="submit" name="file" id-"file" value="Submit">
                  </form>
                </div>
              </div>
            <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End Change Avatar Modal -->
  
            <h1 class="global-h1-small"><?php echo $info['G_NAME'];?></h1>
            <?php
			if ($_SESSION['valid']){
				$joined = GROUP::hasJoined($_GET['group'], $_SESSION['ID']);
				if($joined){
				  ?>
                    <a href="#" class="button success expand" data-reveal-id="group-status">You're a Member</a>
                    <?php
					$isAdmin = GROUP::isAdmin($_GET['group']);
                    if('U.'.$_SESSION['ID'] == $info['G_PID'] || $isAdmin){
						?>
                    <a href="#" class="button expand" data-dropdown="group-tools">Group Tools &raquo;</a>
                    <?php
					}
					?>
                  <?php
				}else{
					//if you have logged in, want to join the group, AND it HAS NO special permission or data requirements
					if($info['G_META']=='' && $info['G_PERMISSIONS'] == '0:0:0'){
				      ?>
					    <a href="?group=<?php echo $_GET['group']; ?>&join=1" class="button expand">Join Group</a>
                      <?php
					}
					//if you have logged in, want to join the group, AND it HAS special permission or data requirements
					else{
						//modal here with permissions form
						?>
                          <div id="join-group" class="reveal-modal" data-reveal>
                            <div class="row">
                              <form method="get">
                                <div class="large-12 columns">
                                  <h2 class="global-h2">Join Group</h2>
                                  <hr />
                                  <p class="global-p">This group requires additional information to join:</p>
                                  <?php if($info['G_META']){ ?>
                                  <div class="row">
                                    <div class="large-6 columns">
                                      <?php
										 $inputs = GROUP::meta_form($info['G_META']);
									  ?>
                                    </div>
                                  </div>
                                  <?php }
                                  if($info['G_PERMISSIONS']){ ?>
                                  <div class="row">
                                    <div class="large-6 columns">
                                      <label>By joining this group, I give access to my name, profile picture, and exercise data I choose to the group's page.</label>
                                      <input id="checkbox1" type="checkbox"><label for="checkbox1">I Agree</label>
                                      <input type="hidden" id="join" name="join" value="1" />
                                      <input type="hidden" id="group" name="group" value="<?php echo $_GET['group']; ?>" />
                                    </div>
                                  </div>
                                  <?php } ?>
                                  <a href="?group=&join=1" class="button success">Join Group</a>
                                </div>
                              </form>
                            </div>
                            <a class="close-reveal-modal">&#215;</a>
                          </div>
                          <a class="button expand" data-reveal-id="join-group">Join Group</a>
                        <?php
					}
				}
			}else{
				//if you aren't logged in
				?>
                  <a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/login.php?group=<?php echo $_GET['group']; ?>" class="button expand">Join Group</a>
                <?php
			}
			?>
            <!-- User-Group Relationship Modal -->
            <div id="group-status" class="reveal-modal" data-reveal>
              <h3 class="global-h3">You are currently a member of this group.</h3>
              <br />
              <a href="?group=<?php echo $_GET['group']; ?>&leave=1" class="button alert">I'm Sure</a>
              <a class="button">Whoops! Cancel</a>
              <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End User-Group Relationship Modal -->
            
            <!-- Group Tools Dropdown -->
            <?php 
			
			if('U.'.$_SESSION['ID'] == $info['G_PID'] || $isAdmin){
			?>
            <ul id="group-tools" class="medium content f-dropdown" data-dropdown-content>
              <li><a href="subgroups.php?group=<?php echo $_GET['group']; ?>">Manage Sub-Groups</a></li>
              <li><a href="#" data-reveal-id="edit-group">Edit Group</a></li>
              <li><a href="#" data-reveal-id="delete-group">Delete Group</a></li>
              <li><a href="#" data-reveal-id="share-url">Invite Members</a></li>
              <li><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].'/reporting.php?group='.$_GET['group'] ?>">Generate Group Report</a></li>
            </ul>
            <!-- End Group Tools Dropdown -->
            
            <!-- Invite Others Modal -->
            <div id="share-url" class="reveal-modal" data-reveal>
            <div class="row">
              <div class="large-12 columns">
                <h2 class="global-h2">Share Group Link</h2>
                <hr style="margin-top:-5px;" />
                <p class="global-p">The link below is unique to your group.  Copy it and distriubte it however you like (email, print, social media) to the people you want to join your group!</p>
              </div>
            </div>
            <div class="row">
              <div class="large-12 columns">
              <form>
                <div class="row collapse">
                  <div class="small-3 large-2 columns">
                    <span class="prefix">Link:</span>
                  </div>
                  <div class="small-9 large-10 columns">
                    <input type="text" placeholder="Link here" 
                    value="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?group='.$_GET['group']; ?>" readonly>
                  </div>
                </div>
              </form>
              </div>
            </div>
            <a class="close-reveal-modal">&#215;</a>
            </div>                        
            <!-- End Invite Others Modal -->
            <!-- Edit Group Info Modal -->
            <div id="edit-group" class="reveal-modal" data-reveal>
              <div class="row">
                <div class="large-12 columns">
                  <h2 class="global-h2">Edit Group Info</h2>
                  <hr style="margin-top:-5px;" />
                </div>
              </div>
              <form method="post" action="edit_group.php">
              <div class="row">
                <div class="large-12 columns">
                  <div class="row">
                    <div class="large-6 columns">
                      <label>Group Name</label>
                      <input type="text" id="NAME" name="NAME" value="<?php echo $info['G_NAME']; ?>" />
                    </div>
                  </div>
                  <div class="row">
                    <div class="large-12 columns">
                      <label>Group Description</label>
                      <textarea id="DESC" name="DESC" style="height:100px;"><?php echo $info['G_DESC']; ?></textarea>
                    </div>
                  </div>
                  <div class="row">
                    <div class="large-6 columns">
                      <label>Group Visibility</label>
                      <select id="G_VIS" name="G_VIS">
                        <option id="G_VIS_1" name="G_VIS_1" value="1" <?php if($info['G_VIS']==1){echo 'selected="selected"';} ?>>Public (Any user can find the group and join)</option>
                        <option id="G_VIS_0" name="G_VIS_0" value="0" <?php if($info['G_VIS']==0){echo 'selected="selected"';} ?>>Private (Only users who have the link to the group's page may join)</option>
                      </select>
                      <input type="hidden" id="G_ID" name="G_ID" value="<?php echo $_GET['group']; ?>" />
                      <br />
                      <br />
                    </div>
                  </div>
                  <div class="row">
                    <div class="large-6 columns">
                      <input type="submit" class="tiny button" value="Submit"/>
                    </div>
                  </div>
                </div>
              </div>
              </form>
              <a class="close-reveal-modal">&#215;</a>
            </div>                        
            <!-- End Edit Group Info Modal -->
            <!-- Delete Group Modal -->
            <div id="delete-group" class="reveal-modal" data-reveal>
              <div class="row">
                <div class="large-12 columns">
                  <h2 class="global-h2">Delete Group</h2>
                  <hr style="margin-top:-5px;" />
                  <p class="global-p">Are you sure you want to permanently delete this group?</p>
                  <a href="delete_group.php?group=<?php echo $_GET['group']; ?>" class="button tiny alert">Delete</a> <a href="#" class="button tiny">Cancel</a>
                </div>
              </div>
            <a class="close-reveal-modal">&#215;</a>
            </div>            
            <!-- End Delete Group Modal -->
            
            <?php
			}
			?>
          </div>
          
        </div>
      </div>
      <!-- End Desktop Sidebar -->
      
      <!-- Mobile Sidebar -->
      <div class="sidebar show-for-small-only" style="margin-bottom:20px;">
          <div align="center">
            <img src="../img/default-group-profile.png" alt="Session Image" />
  
            <h1 class="global-h1-small"><?php echo $info['G_NAME'];?></h1>
            <?php
            if ($_SESSION['valid']){
				$joined = GROUP::hasJoined($_GET['group'], $_SESSION['ID']);
				if($joined){
				  ?>
                    <a href="#" class="button success expand" data-reveal-id="group-status">You're a Member</a>
                    <a href="#" class="button expand" data-dropdown="group-tools">Group Tools &raquo;</a>
                  <?php
				  	if($info['P_ID'] == 'U.'.$_SESSION['ID']){
						//you get admin tools	
					}
				}else{
					//if you have logged in, want to join the group, AND it HAS NO special permission or data requirements
					if($info['G_META']=='' && $info['G_PERMISSIONS'] == ''){
				      ?>
					    <a href="?group=<?php echo $_GET['group']; ?>&join=1" class="button expand">Join Group</a>
                      <?php
					}
					//if you have logged in, want to join the group, AND it HAS special permission or data requirements
					else{
						//modal here with permissions form
						?>
                          <div id="join-group" class="reveal-modal" data-reveal>
                            <div class="row">
                              <form method="get">
                                <div class="large-12 columns">
                                  <h2 class="global-h2">Join Group</h2>
                                  <hr />
                                  <p class="global-p">This group requires additional information to join:</p>
                                  <?php if($info['G_META']){ ?>
                                  <div class="row">
                                    <div class="large-6 columns">
                                      <?php
										 $inputs = GROUP::meta_form($info['G_META']);
									  ?>
                                    </div>
                                  </div>
                                  <?php }
                                  if($info['G_PERMISSIONS']){ ?>
                                  <div class="row">
                                    <div class="large-6 columns">
                                      <label>By joining this group, I give access to my name, profile picture, and exercise data I choose to the group's page.</label>
                                      <input id="checkbox1" type="checkbox" required /><label for="checkbox1">I Agree</label>
                                      <input type="hidden" id="join" name="join" value="1" />
                                      <input type="hidden" id="group" name="group" value="<?php echo $_GET['group']; ?>" />
                                    </div>
                                  </div>
                                  <?php } ?>
                                  <input type="submit" class="button tiny success" value="Submit">
                                </div>
                              </form>
                            </div>
                            <a class="close-reveal-modal">&#215;</a>
                          </div>
                          <a class="button expand" data-reveal-id="join-group">Join Group</a>
                        <?php
					}
				}
			}else{
				//if you aren't logged in
				?>
                  <a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/login.php?group=<?php echo $_GET['group']; ?>" class="button expand">Join Group</a>
                <?php
			}
			?>
            
            <!-- User-Group Relationship Modal -->
            <div id="group-status" class="reveal-modal" data-reveal>
              <h3 class="global-h3">Are you sure you want to leave this group?</h3>
              <br />
              <a href="?group=<?php echo $_GET['group']; ?>&leave=1" class="button alert">I'm Sure</a>
              <a class="button">Whoops! Cancel</a>
              <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End User-Group Relationship Modal -->
            
            <!-- Group Tools Dropdown -->
            <ul id="group-tools" class="medium content f-dropdown" data-dropdown-content>
              <li><a href="#">Group Chat</a></li>
              <li><a href="#">Potential Admin Option 1</a></li>
              <li><a href="#">Potential Admin Option 2</a></li>
            </ul>
            <!-- End Group Tools Dropdown -->
      
          </div>
          
        </div>
      <!-- End Mobile -->
    </div>
    
    
    <!-- Right Side Top -->
    <div class="large-7 medium-7 columns">
      <div class="row">
        <div class="medium-12 columns">
          <!-- Session Description -->
          <div align="justify">
            <h2 class="global-h2-gray">Description:</h2>
            <br />
            <br />
            <p class="global-p">
            <?php echo $info['G_DESC'];
			?>
            </p>
          </div>
          <!-- End Session Description -->
          <hr />
          <!-- Users Section -->
          <div class="row" style="margin-top:20px;">
            <div class="large-12 columns">
              <ul class="breadcrumbs">
              <?php $bc = GROUP::breadcrumbs($_GET['group']); ?>   
              </ul>
              <hr style="margin-top:10px; margin-bottom:0px;" />
              <br />
              <?php  $subgroups = GROUP::list_subgroups($_GET['group']); 
			  if(!$subgroups){
				  $subgroups = GROUP::list_users_in_group($_GET['group'], 16); 
			  }else{
				  $subgroups = GROUP::list_users_in_group($_GET['group'], 0); 
			  }
			  $breakdown = GROUP::getBreakdown($_GET['group']);
			  $points_total = array_sum($breakdown['POINTS']);
			  $time_total = array_sum($breakdown['TIME']);
			  $distance_total = array_sum($breakdown['DISTANCE']);

			  ?>
            </div>
          </div>
          <!-- End Users Section -->
          <!-- Session Stats -->
          <h2 class="global-h2-gray"><b>Group Stats:</b></h2>
      
          <div class="stat-unit"> 
            <div class="row">
              <div class="large-12 columns">     
                <img src="../img/points-icon.png" class="stat-icon hide-for-small-only" alt="Points Icon"> <a href="#" data-dropdown="drop1" class="statlink"><b>Total Points: <?php echo $points_total;?>  </b></a>
        
                <div id="drop1" data-dropdown-content class="f-dropdown content">
                  <small><b>Breakdown:</b></small>
                  <hr style="margin-top:5px; margin-bottom:5px;" />
                  <small>To find out where these points came from, look in the "Past Activity" section below.</small>
                </div>
            
              </div>
            </div>
          </div>
      
          <div class="stat-unit">
        <div class="row">
          <div class="large-12 columns">
            <img alt="Your Total Logged Hours" src="../img/time-icon.png" class="stat-icon hide-for-small-only"> <a href="#" data-dropdown="drop2" class="statlink"><b>
			<?php if($time_total < 3600){ ?>
            Total Minutes Exercised: <?php echo ($time_total/60); 
			}else{
			?>
            Total Hours Exercised: <?php echo number_format($time_total/3600)." h ".(($time_total % 3600)/60)." m"; 
			}
			?>
            </b></a>
            <div id="drop2" data-dropdown-content class="f-dropdown content">
              <small><b>Breakdown:</b></small>
              <hr style="margin-top:5px; margin-bottom:5px;" />
              <?php 
			  	foreach($breakdown['TIME'] as $key =>$activity){
					
					if ($activity < 60){
					?>
                    <small><?php echo ACTIVITY::activity_to_form($key).": ".$activity['TIME']."m"; ?></small>
                    <br />
                    <?php
					}else if ($activity['TIME'] < 3600){
					?>
                    <small><?php echo ACTIVITY::activity_to_form($key).": ".number_format($activity/3600)."h ".(($activity % 3600)/60)."m"; ?></small>
                    <br />
                    <?php 
					}
				}
			  ?>
            </div>
        
          </div>
        </div>
      </div>
    
          <div class="stat-unit">
        <div class="row">
          <div class="large-12 columns">
            <img alt="Your Total Logged Distance" src="../img/distance-icon.png" class="stat-icon hide-for-small-only"> <a href="#" data-dropdown="drop3" class="statlink"><b>Total Miles Traveled: <?php echo $distance_total; ?></b></a>
        
            <div id="drop3" data-dropdown-content class="f-dropdown content">
              <small><b>Breakdown:</b></small>
              <hr style="margin-top:5px; margin-bottom:5px;" />
              <?php 
			  	foreach($breakdown['DISTANCE'] as $key =>$activity){
					?>
                    <small><?php echo ACTIVITY::activity_to_form($key).": ".$activity." miles"; ?></small>
                    <br />
                    <?php
				}
			  ?>
            </div>
        
          </div>
        </div>
      </div>
      
      <hr style="margin-bottom:5px;" /> 
      <a href="#"><small>Questions about these stats?  Click here.</small></a>
       
          <!-- End Session Stats -->
          
          
      
          <hr style="margin-top:0px;" />
          <a href="#" data-reveal-id="view-all-users" class="button tiny">View All Users</a>
      
          <!-- View all Users Modal -->
          <div id="view-all-users" class="reveal-modal" data-reveal>
      
          <div class="row">
            <div class="large-6 columns">
              <h2 class="global-h2">Users:</h2>
              <hr />
            </div>
          </div>
          <a class="close-reveal-modal">&#215;</a>
          </div>
          <!-- End View all Users Modal -->
     
        </div>
      </div>  
    </div>
  </div>
<!-- End Main Section -->  
</div>
<!-- End Main Section -->
<?php 
}
//If no group id is specified, or if there is an error loading the group page
		else{
			$gohome = REDIRECT::home();
		}

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../js/modernizr.js"></script>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script src="../js/foundation/foundation.js"></script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script src="../js/foundation/foundation.topbar.js"></script>
    <script src="../js/foundation/foundation.abide.js"></script>
    <script src="../js/foundation/foundation.reveal.js"></script>
    <script src="../js/foundation/foundation.tooltip.js"></script>
    <script src="../js/foundation/foundation.offcanvas.js"></script>
    <script src="../js/foundation/foundation.equalizer.js"></script>
    <script src="../js/foundation/foundation.dropdown.js"></script>
    <script type="text/javascript" src="../js/sha512.js"></script>
    <script type="text/javascript" src="../js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>