<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/groups_api.php");
include_once ("$root/lib/back_api.php");
//If no group id is specified, or if there is an error loading the group page
$group_id = filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING);
$HTTP_HOST = filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);
if(empty($group_id)||!isset($group_id)){
  $gohome = REDIRECT::home('');
}else{
	if(is_numeric ($group_id) && GROUP::exists($group_id)){
    	$info = GROUP::get_info($group_id);
	}else{
		$gohome = REDIRECT::home('');
		exit();
	}
}
$ss = SESSION::secure_session();

if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
	SESSION::logout();
	REDIRECT::home();
}
$isAdmin = GROUP::isAdmin($group_id);
if($isAdmin){
  if(filter_input(INPUT_POST, 'file', FILTER_SANITIZE_STRING) != null){
  	$img = GROUP::avatar_upload($group_id) ;
  }
}
if(filter_input(INPUT_GET, 'join', FILTER_SANITIZE_STRING) != null){
	$join_group = GROUP::join_group($group_id, $_SESSION['ID']);
}
if(filter_input(INPUT_GET, 'leave', FILTER_SANITIZE_STRING) != null){
	$leave_group = GROUP::leave_group($group_id, $_SESSION['ID']);
}
// $breakdown = GROUP::getBreakdown($group_id);
// $points_total = "0";
// $distance_total = "0";
// $time_total = "0";
// if(isset($breakdown['POINTS'])){
// 	if(is_array($breakdown['POINTS'])){
// 		$points_total = array_sum($breakdown['POINTS']);
// 	}
// 	if ($points_total < 1 || $points_total=='' || $points_total== NULL){
// 					$points_total = '0';
// 	}
// }
// if(isset($breakdown['TIME'])){
// 	if(is_array($breakdown['TIME'])){
// 		$time_total = array_sum($breakdown['TIME']);
// 	}
// 	if ($time_total < 1 || $time_total=='' || $time_total== NULL){
// 					$time_total = '0';
// 	}
// }
// if(isset($breakdown['DISTANCE'])){
// 	if(is_array($breakdown['DISTANCE'])){
// 		$distance_total = array_sum($breakdown['DISTANCE']);
// 	}
// 	if ($distance_total < 1 || $distance_total=='' || $distance_total== NULL){
// 					$distance_total = '0';
// 	}
// }


$date_range = '';
$group_activity_date_range = '';
$group = array();
$group_id = filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING);
$isAdmin = GROUP::isAdmin($group_id);

// Get Group Info, Subgroup IDs, Member IDs, if Group Logging is turned on, get Total Group Points else use Members to calculate points.
$is_group_logging = false;
$breakdown_begin = microtime(true);
$group['info'] = GROUP::get_info($group_id);
$group['members'] = GROUP::getAllMembers($group_id, array());
$breakdown_end = microtime(true);

// Only get group data from group created date. Make this for any new groups created after 7/19/2016
if($group_id > 153201) {
  $date1 =  date("m/d/Y", strtotime($group['info']['G_DATE_CREATED']));
  $date2 = date("m/d/Y");
  if(empty($date_range)) {
    $date_range = 'AL_DATE >= \'' . $date1 . '\' AND AL_DATE <= \'' . $date2 . '\' AND';;
  }
  if(empty($group_activity_date_range)) {
    $group_activity_date_range = 'GA_DATE >= \'' . $date1 . '\' AND GA_DATE <= \'' . $date2 . '\' AND';
  }
}

// Query to load all user data. Can sort by AL_PA == Activity Log, Points Added. So members with the most points are at the top.
$user_order = 'L_FNAME DESC';
if(isset($_GET['user_order'])){
  $user_order = filter_input(INPUT_GET, 'user_order', FILTER_SANITIZE_STRING);
}
$all_users_begin = microtime(true);
// $sql = 'SELECT L_FNAME, L_LNAME, L_ID, IS_ADMIN FROM LOGIN WHERE L_ID IN (SELECT U_ID FROM GROUP_MEMBER WHERE G_ID= \''.$group_id.'\') GROUP BY L_FNAME, L_LNAME, L_COUNTY, L_ID, L_E';
$sql = 'SELECT IS_ADMIN, L_FNAME, L_LNAME, L_ID FROM GROUP_MEMBER INNER JOIN LOGIN ON U_ID = L_ID WHERE G_ID = \''.$group_id.'\'';
$all_users_non_active = MSSQL::query($sql);
while(odbc_fetch_array($all_users_non_active)) {
  $this_array = array();
  $this_array['id'] = odbc_result($all_users_non_active, 'L_ID');
  $this_array['fname'] = odbc_result($all_users_non_active, 'L_FNAME');
  $this_array['lname'] = odbc_result($all_users_non_active, 'L_LNAME');
  //$this_array['email'] = odbc_result($all_users_non_active, 'L_E');
  //$this_array['county'] = odbc_result($all_users_non_active, 'L_COUNTY');
  $this_array['is_admin'] = odbc_result($all_users_non_active, 'IS_ADMIN');
  $group['all_members'][] = $this_array;
}
$count_users = odbc_num_rows($all_users_non_active);
$all_users = array();
$all_activity_types = array();
// $sql1 = 'SELECT L_ID, L_FNAME, L_LNAME, L_COUNTY, L_E FROM LOG WHERE '.$date_range.' L_ID IN (SELECT U_ID FROM GROUP_MEMBER WHERE G_ID= \''.$group_id.'\') GROUP BY L_FNAME, L_LNAME, L_COUNTY, L_ID, L_E ORDER BY '.$user_order.';';
// $user = MSSQL::query($sql1);
//
// // For each row of users with activity, set their id, fname, lname, email, county, points, seconds, and miles. And then add that user to the All_users array.
// $x_user_array_count = 0;
// while(odbc_fetch_array($user)) {
//   $this_array = array();
//   $this_array['id'] = odbc_result($user, 'L_ID');
//   $this_array['fname'] = odbc_result($user, 'L_FNAME');
//   $this_array['lname'] = odbc_result($user, 'L_LNAME');
//   $this_array['email'] = odbc_result($user, 'L_E');
//   $this_array['county'] = odbc_result($user, 'L_COUNTY');
//   //$this_array['points'] = odbc_result($user, 'AL_PA');
//   // $this_array['seconds'] = odbc_result($user, 'AL_TIME');
//   // $this_array['miles'] = odbc_result($user, 'AL_UNIT');
//   // $this_array['points'] = odbc_result($user, 'AL_PA');
//   $all_users[$x_user_array_count] = $this_array;
//   $all_users_key_value[$this_array['id']] = $x_user_array_count;
//   $x_user_array_count++;
// }
// $all_users_length = count($all_users);
// $all_users_end = microtime(true);
$group['seconds'] = 0;
$group['miles'] = 0;
$group['points'] = 0;
$sql2 = 'SELECT AL_AID, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\', SUM(AL_UNIT) AS \'AL_UNIT\' FROM dbo.LOG WHERE AL_UID IN (SELECT U_ID FROM GROUP_MEMBER WHERE G_ID = \''.$group_id.'\') GROUP BY AL_AID, AL_PA';
$all_activities = MSSQL::query($sql2);
while(odbc_fetch_array($all_activities)) {
  if(isset($all_activity_types[odbc_result($all_activities, 'AL_AID')])) {
    $all_activity_types[odbc_result($all_activities, 'AL_AID')]['seconds'] += odbc_result($all_activities, 'AL_TIME');
    $all_activity_types[odbc_result($all_activities, 'AL_AID')]['miles'] += odbc_result($all_activities, 'AL_UNIT');
    $all_activity_types[odbc_result($all_activities, 'AL_AID')]['points'] += odbc_result($all_activities, 'AL_PA');
  }
  else {
    $sql4 = "SELECT A_NAME, A_UNIT FROM ACTIVITY WHERE A_ID = '".odbc_result($all_activities, 'AL_AID')."'";
    $activity_information = MSSQL::query($sql4);
    $all_activity_types[odbc_result($all_activities, 'AL_AID')]['name'] = odbc_result($activity_information, 'A_NAME');
    $all_activity_types[odbc_result($all_activities, 'AL_AID')]['metric'] = odbc_result($activity_information, 'A_UNIT');
    $all_activity_types[odbc_result($all_activities, 'AL_AID')]['seconds'] = odbc_result($all_activities, 'AL_TIME');
    $all_activity_types[odbc_result($all_activities, 'AL_AID')]['miles'] = odbc_result($all_activities, 'AL_UNIT');
    $all_activity_types[odbc_result($all_activities, 'AL_AID')]['points'] = odbc_result($all_activities, 'AL_PA');
  }
  // Add to the total group points / time / distance (if the activity is distance based (metric == 1))
  $group['seconds'] += intval(odbc_result($all_activities, 'AL_TIME'));
  if($all_activity_types[odbc_result($all_activities, 'AL_AID')]['metric'] == 1 ) {
    $group['miles'] += intval(odbc_result($all_activities, 'AL_UNIT'));
  }
  $group['points'] += intval(odbc_result($all_activities, 'AL_PA'));
}
$group['total_members'] = count($group['all_members']);
$group['active_members'] = array();
$group['activities'] = array();
$group['counties'] = array();

// Begin Group Log Reporting
if($group["info"]["G_GLOG"] == '1') {
  $is_group_logging = true;
  $group['subgroups'] = GROUP::getSubs($group_id, array());
  //$group['group_log_points'] = GROUP::getTotalGroupPoints($group_id, array());
  $sql2 = "SELECT GA_AID, GA_TIME, GA_UNIT, GA_PA, GA_DATE, GA_MEMBER_COUNT FROM GROUP_ACTIVITY WHERE ".$group_activity_date_range." GA_GID='".$group_id."'";
  //echo("156: ".$sql2."<BR>");
  $group_activities = MSSQL::query($sql2);
  while(odbc_fetch_array($group_activities)) {
    $this_array = array();
    $this_array['group'] = $group["info"]["G_NAME"];
    $this_array['group_id'] = $group_id;
    if(isset($all_activity_types[odbc_result($group_activities, 'GA_AID')])) {
      $this_array['activity'] = $all_activity_types[odbc_result($group_activities, 'GA_AID')];
      $all_activity_types[odbc_result($group_activities, 'GA_AID')]['seconds'] += odbc_result($group_activities, 'GA_TIME');
      $all_activity_types[odbc_result($group_activities, 'GA_AID')]['miles'] += odbc_result($group_activities, 'GA_UNIT');
      $all_activity_types[odbc_result($group_activities, 'GA_AID')]['points'] += odbc_result($group_activities, 'GA_PA');
    }
    else {
      $sql4 = "SELECT A_NAME, A_UNIT FROM ACTIVITY WHERE A_ID = '".odbc_result($group_activities, 'GA_AID')."'";
      $activity_information = MSSQL::query($sql4);
      $this_array['activity'] = odbc_result($activity_information, 'A_NAME');
      $all_activity_types[odbc_result($group_activities, 'GA_AID')]['metric'] = odbc_result($activity_information, 'A_UNIT');
      $all_activity_types[odbc_result($group_activities, 'GA_AID')]['name'] = odbc_result($activity_information, 'A_NAME');
      $all_activity_types[odbc_result($group_activities, 'GA_AID')]['seconds'] = odbc_result($group_activities, 'GA_TIME');
      $all_activity_types[odbc_result($group_activities, 'GA_AID')]['miles'] = odbc_result($group_activities, 'GA_UNIT');
      $all_activity_types[odbc_result($group_activities, 'GA_AID')]['points'] = odbc_result($group_activities, 'GA_PA');
    }
    $group['seconds'] += intval(odbc_result($group_activities, 'GA_TIME'));
    if($all_activity_types[odbc_result($group_activities, 'GA_AID')]['metric'] == 1 ) {
      $group['miles'] += intval(odbc_result($group_activities, 'GA_UNIT'));
    }
    $group['points'] += intval(odbc_result($group_activities, 'GA_PA'));
  }
  $subgroup_breakdown_begin = microtime(true);
  $subgroups_length = count($group['subgroups']);
  for($i = 0; $i < $subgroups_length; $i++) {
    $subgroup_begin = microtime(true);
    $subgroups[$i]["info"] = GROUP::get_info($group['subgroups'][$i]);
    $subgroups[$i]["info"]["G_ID"] = $group['subgroups'][$i];
    $subgroups[$i]['points'] = 0;
    $subgroups[$i]['seconds'] = 0;
    $subgroups[$i]['miles'] = 0;

    // If group logging is turned on, points are equal to the group's log points.
    if($subgroups[$i]["info"]["G_GLOG"] == '1') {
      $is_group_logging = true;
      $subgroups[$i]['activities'] = array();
      $sql3 = "SELECT GA_AID, GA_TIME, GA_UNIT, GA_PA, GA_DATE, GA_MEMBER_COUNT FROM GROUP_ACTIVITY WHERE ".$group_activity_date_range." GA_GID = '".$subgroups[$i]["info"]["G_ID"]."'";
      //echo("339: ".$sql3."<BR>");
      $subgroup_activities = MSSQL::query($sql3);
      while(odbc_fetch_array($subgroup_activities)) {
        $this_array = array();
        $this_array['group'] = $subgroups[$i]["info"]["G_NAME"];
        $this_array['group_id'] = $subgroups[$i]["info"]["G_ID"];
        if(isset($all_activity_types[odbc_result($subgroup_activities, 'GA_AID')])) {
          $this_array['activity'] = $all_activity_types[odbc_result($subgroup_activities, 'GA_AID')];
          $all_activity_types[odbc_result($subgroup_activities, 'GA_AID')]['seconds'] += odbc_result($subgroup_activities, 'GA_TIME');
          $all_activity_types[odbc_result($subgroup_activities, 'GA_AID')]['miles'] += odbc_result($subgroup_activities, 'GA_UNIT');
          $all_activity_types[odbc_result($subgroup_activities, 'GA_AID')]['points'] += odbc_result($subgroup_activities, 'GA_PA');
        }
        else {
          $sql4 = "SELECT A_NAME, A_UNIT FROM ACTIVITY WHERE A_ID = '".odbc_result($subgroup_activities, 'GA_AID')."'";
          $activity_information = MSSQL::query($sql4);
          $this_array['activity'] = odbc_result($activity_information, 'A_NAME');
          $all_activity_types[odbc_result($subgroup_activities, 'GA_AID')]['metric'] = odbc_result($activity_information, 'A_UNIT');
          $all_activity_types[odbc_result($subgroup_activities, 'GA_AID')]['name'] = odbc_result($activity_information, 'A_NAME');
          $all_activity_types[odbc_result($subgroup_activities, 'GA_AID')]['seconds'] = odbc_result($subgroup_activities, 'GA_TIME');
          $all_activity_types[odbc_result($subgroup_activities, 'GA_AID')]['miles'] = odbc_result($subgroup_activities, 'GA_UNIT');
          $all_activity_types[odbc_result($subgroup_activities, 'GA_AID')]['points'] = odbc_result($subgroup_activities, 'GA_PA');
        }
        $group['seconds'] += intval(odbc_result($subgroup_activities, 'GA_TIME'));
        if($all_activity_types[odbc_result($subgroup_activities, 'GA_AID')]['metric'] == 1 ) {
          $group['miles'] += intval(odbc_result($subgroup_activities, 'GA_UNIT'));
        }
        $group['points'] += intval(odbc_result($subgroup_activities, 'GA_PA'));
      }
    }
    else {
      $subgroups[$i]['member_ids'] = GROUP::getAllMembers($group['subgroups'][$i], array());
      // Declaring variables and counts.
      $subgroups[$i]['active_members'] = 0;
      $subgroups[$i]['total_members'] = count($subgroups[$i]['member_ids']);

      $all_users_key_begin = microtime(true);
      for($y = 0; $y < $subgroups[$i]['total_members']; $y++) {
        // If the member id is in here, they have an activity, if not then they are inactive.
        if(isset($all_users_key_value[$subgroups[$i]['member_ids'][$y]])) {
          $subgroups[$i]['active_members']++;
          // To get the user information. Get the $all_users array position by sending the current id of the subgroup member to the key_value array.
          $subgroups[$i]['points'] += intval($all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['points']);
          $subgroups[$i]['seconds'] += intval($all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['seconds']);
          $subgroups[$i]['miles'] += intval($all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['miles']);
          if($all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['county'] != null) {
            // Need to make sure $group['counties'] is not null so in_array() does not throw an error
            if(count($group['counties']) > 0) {
              if(!in_array($all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['county'], $group['counties'])) {
                $group['counties'][] = $all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['county'];
              }
            }
            else {
              $group['counties'][] = $all_users[$all_users_key_value[$subgroups[$i]['member_ids'][$y]]]['county'];
            }
          }

          // Unset the value to get the remaining users in the main group, not any subgroup, and add their points. After all subgroups run, $group['members'] will only contain the ids of those not in a subgroup
          if(($key = array_search($all_users_key_value[$subgroups[$i]['member_ids'][$y]], $group['members']) !== false)) {
            unset($group['members'][$key]);
          }
        }
      }
      $all_users_key_end = microtime(true);
      $subgroups[$i]['Points Time Taken'] = ($all_users_key_end - $all_users_key_begin);

      // Saving for later, although this method is a bottleneck. We can use the already loaded user's and their points rather than re-querying it for every subgroup.
      //$subgroups[$i]['Individual Points'] = GROUP::getTotalIndividualPoints($group['subgroups'][$i], array(), $subgroups[$i]['member_ids']);
    }
    $subgroup_end = microtime(true);
    $subgroups[$i]["Time Taken"] = ($subgroup_end - $subgroup_begin." seconds.");
  }
  $subgroup_breakdown_end = microtime(true);
}


// foreach($all_users_key_value as $key => $val) {
//   $group['active_members'][] = $all_users[$val];
//   $group['points'] += intval($all_users[$val]['points']);
//   $group['seconds'] += intval($all_users[$val]['seconds']);
//   $group['miles'] += intval($all_users[$val]['miles']);
// }

// echo("<pre>");
// var_dump($all_activity_types);
// echo("</pre>");
// echo("<pre>");
// var_dump($group);
// echo("</pre>");
// exit();


HTML_ELEMENT::head('Groups');
HTML_ELEMENT::top_nav();
//EDIT HERE
if(filter_input(INPUT_GET, 'err_msg', FILTER_SANITIZE_STRING) != null){?>
    <div data-alert class="alert-box alert center">
      <?php echo filter_input(INPUT_GET, 'err_msg', FILTER_SANITIZE_STRING);?></a>
      <a href="#" class="close">&times;</a>
    </div>
        <?php
	}
?>
<div class="main">
    <?php
//alert boxes that pop up when action taken
if(filter_input(INPUT_GET, 'join', FILTER_SANITIZE_STRING) != null){
        $GROUP_ID = filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING);
        $join = MSSQL::query("SELECT G_NAME FROM GROUPS WHERE G_ID ='".$GROUP_ID."'");
        $name = odbc_result($join, 'G_NAME');
        echo "<div data-alert class=\"alert-box success\" style=\"margin-bottom:0px; text-align: center;\">You have joined the group: $name<a class=\"close\">&times;</a></div>";

}
if(filter_input(INPUT_GET, 'leave', FILTER_SANITIZE_STRING) != null){
        $GROUP_ID = filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING);
        $join = MSSQL::query("SELECT G_NAME FROM GROUPS WHERE G_ID ='".$GROUP_ID."'");
        $name = odbc_result($join, 'G_NAME');
	echo "<div data-alert class=\"alert-box success\" style=\"margin-bottom:0px; text-align: center;\">You have left the group: $name<a class=\"close\">&times;</a></div>";
}
?>
  <!-- Group Header -->
  <div class="blue-bg-group-header" style="margin-top:-20px;">
    <div class="row" style="">
      <div class="large-3 medium-3 hide-for-small columns">
            <?php
          if (file_exists(filter_input(INPUT_SERVER, 'DOCUMENT_ROOT', FILTER_SANITIZE_STRING)."/group/img/".$group_id.".png")) {
			  $filename = "/group/img/".$group_id.".png";
		  } else {
    			$filename = "../img/default-group-profile.png";
		  }
		  if($isAdmin){
		  ?>

            <!-- Avatar -->
            <a href="#" data-reveal-id="avatar">
          <?php
		  }
			?>
            <div class="avatar-medium" style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat;"></div>
            </a>
            <!-- End Avatar -->

            <!-- Change Avatar Modal -->
            <div id="avatar" class="reveal-modal" data-reveal>
              <div class="row">
                <div class="large-12 columns">
                  <p class="global-p">Do you want to upload a new image?</p>
                  <form method="post" enctype="multipart/form-data">
                    <img id="blah" src="<?php echo $filename; ?>" alt="your image" height="240px;" width="240px;" style="border-radius:50%;" />
                    <br />
                    <br />
                    <script type="text/javascript">
	        			function readURL(input) {
			        	    if (input.files && input.files[0]) {
			    	            var reader = new FileReader();
			    	            reader.onload = function (e) {
				                    $('#blah').attr('src', e.target.result);
				                }
				                reader.readAsDataURL(input.files[0]);
				            }
			    	    }
				    </script>
                    <input type="file" name="group_img" id="group_img" onchange="readURL(this);">
                    <br>
                    <input type="submit" name="file" id="file" value="Submit">
                  </form>
                </div>
              </div>
            <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End Change Avatar Modal -->
      </div>
      <div class="large-9 medium-9 small-12 columns">
        <h1 class="custom-font-small-white"><?php echo $info['G_NAME'];?></h1>
        <hr style="color:white; border-color:white; margin-top:-5px; margin-bottom:5px;" />
        <p class="global-p-white">
        <?php echo $info['G_DESC'];?>
        </p>
        <?php
			if (isset($_SESSION['valid'])){
				$joined = GROUP::hasJoined($group_id, $_SESSION['ID']);
				if($joined){
				  ?>
                    <a href="#" class="button success round tiny" data-reveal-id="group-status">Member Options</a>
                    <?php
                    if($isAdmin){
						?>
                    <a href="#" class="button secondary round tiny" data-reveal-id="group-tools">Group Tools</a>
				  <?php
					}
					?>
                  <?php
				}else{
					//if you have logged in, want to join the group, AND it HAS NO special permission or data requirements
					if($info['G_META']=='' && $info['G_PERMISSIONS'] == '0:0:0'){
				      ?>
					    <a href="?group=<?php echo $group_id; ?>&join=1" class="round tiny secondary button">Join Group</a>
                      <?php
					  if($isAdmin){
						?>
                   		 <a href="#" class="button secondary round tiny" data-reveal-id="group-tools">Group Tools</a>
				  		<?php
					  }
					}
					//if you have logged in, want to join the group, AND it HAS special permission or data requirements
					else{
						//modal here with permissions form
						?>
                          <div id="join-group" class="reveal-modal small" data-reveal>
                            <div class="row">
                              <form method="get" data-abide>
                                <div class="large-12 columns">
                                  <h2 class="custom-font-small-blue">Join Group</h2>
                                  <hr />
                                  <p class="global-p">This group requires additional information to join:</p>
                                  <?php if($info['G_META']){ ?>
                                  <div class="row">
                                    <div class="large-6 columns">
                                      <?php
										 $inputs = GROUP::meta_form($info['G_META']);
									  ?>
                                    </div>
                                  </div>
                                  <?php }
                                  if($info['G_PERMISSIONS']){ ?>
                                  <div class="row">
                                    <div class="large-6 columns">
                                      <label>By joining this group, I give access to my name, profile picture, and exercise data to the group page, which may be visible to the public.</label>
                                      <input id="checkbox1" type="checkbox" required><label for="checkbox1">I Agree</label>
                                      <input type="hidden" id="join" name="join" value="1" />
                                      <input type="hidden" id="group" name="group" value="<?php echo $group_id; ?>" />
                                    </div>
                                  </div>
                                  <?php } ?>
                                  <input type="submit" class="button small round success" value="Join Group">
                                </div>
                              </form>
                            </div>
                            <a class="close-reveal-modal">&#215;</a>
                          </div>
                          <a class="round tiny secondary button" data-reveal-id="join-group">Join Group</a>
                        <?php
						if($isAdmin){
						?>
                    		<a href="#" class="button secondary round tiny" data-reveal-id="group-tools">Group Tools</a>
				  		<?php
						}
					}
				}
			}else{
				//if you aren't logged in
				?>
                  <a href="http://<?php echo $HTTP_HOST; ?>/login.php?group=<?php echo $group_id; ?>" class="round tiny secondary button">Join Group</a>
                <?php
			}
			?>
      </div>
    </div>

  </div>
  <!-- End Group Header -->

  <div class="row">
  <div class="large-12 columns">
     <ul class="breadcrumbs hide-for-small" style="margin-top:20px;">
       <?php $bc = GROUP::breadcrumbs($group_id); ?>
     </ul>

    </div>

    </div>



    <!-- Modals Section -->

            <!-- User-Group Relationship Modal -->
            <div id="group-status" class="reveal-modal tiny" data-reveal>
              <div class="row center">
                <div class="large-12 columns">
                  <h3 class="custom-font-small-blue">Membership Status</h3>
                  <hr style="margin-top:-5px;" />
                  <p class="global-p-small">
                  </p>
                  <br />
                  <a href="add_favorite.php?gid=<?php echo $group_id; ?>" class="button tiny round success">Add to My Favorites</a>
                  <a href="?group=<?php echo $group_id; ?>&leave=1" class="button tiny round alert">Yes, leave this group.</a>
                  <!-- <a href="#" class="button tiny round secondary">Cancel</a> -->
                </div>
              </div>
              <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End User-Group Relationship Modal -->
            <div id="add-to-my-groups-modal" class="reveal-modal tiny" data-reveal>
              <div class="row center">
                <div class="large-12 columns">
                  <h3 class="custom-font-small-blue">Added to My Groups Page:</h3>
                  <hr style="margin-top:-5px;" />
                  <p class="global-p-small">
                  Success!  You've added a link to this page.  Now whenever you need to quickly get back to this group's page, you can by using the links on your My Groups page.
                  </p>
                  <br />
                  <a href="add_favorite.php?group=<?php echo $group_id; ?>" class="button tiny round success">Take me to My Groups</a>
                </div>
              </div>
              <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- Add to My Groups Modal -->

            <!-- End Add to My Groups Modal -->

            <!-- Group Tools Modal -->
            <?php

			if($isAdmin){
			?>
            <div id="group-tools" class="reveal-modal" data-reveal>
              <div class="row">
                <div class="large-12 columns">
                  <h2 class="custom-font-small-blue">Group Tools</h2>
                  <hr style="margin-top:-5px;">
                </div>
              </div>
              <div class="row">
                <div class="large-6 medium-6 small-12 columns">
                  <h2 class="global-h2">Group Logging Preferences:</h2>
                  <hr style="margin-top:-5px; margin-bottom:5px;">
                  <p class="global-p-small">Choose to log the activity for your entire group, or to use the total of all users' activity.</p>
                  <a href="#" class="button tiny success expand" data-reveal-id="activity-settings">Change Activity Settings</a>
                </div>
                <div class="large-6 medium-6 small-12 columns">
                  <h2 class="global-h2">Add / Edit Subgroups:</h2>
                  <hr style="margin-top:-5px; margin-bottom:5px;">
                  <p class="global-p-small">Create or edit your subgroups to organize your members.  Here you can add new sugroups or edit what you already have.</p>
                  <a href="subgroups.php?group=<?php echo $group_id; ?>" class="button tiny expand">Add / Edit Subgroups</a>
                </div>
              </div>
              <div class="row">
                <div class="large-6 medium-6 small-12 columns">
                  <h2 class="global-h2">Invite People To Join:</h2>
                  <hr style="margin-top:-5px; margin-bottom:5px;">
                  <p class="global-p-small">If you want more group members, click the button below and share it with others even if they don't have an account.</p>
                  <a href="#" data-reveal-id="share-url" class="tiny button expand">Invite Members</a>
                </div>
                <div class="large-6 medium-6 small-12 columns">
                  <h2 class="global-h2">Group Report:</h2>
                  <hr style="margin-top:-5px; margin-bottom:5px;">
                  <p class="global-p-small">Reports are a way to get in-depth information on your group's activity.  Click below to generate a report.</p>
                  <a href="<?php echo 'http://'.$HTTP_HOST.'/reporting.php?group='.$group_id ?>" target="_blank" class="tiny button expand">Generate Group Report</a>
                </div>
              </div>
              <div class="row">
                <div class="large-6 medium-6 small-12 columns">
                  <h2 class="global-h2">Edit Group:</h2>
                  <hr style="margin-top:-5px; margin-bottom:5px;">
                  <p class="global-p-small">Change the title of the group, the description, and more.  Click below to get started editing the group info.</p>
                  <a href="#" data-reveal-id="edit-group" class="button tiny expand">Edit Group</a>
                </div>
                <div class="large-6 medium-6 small-12 columns">
                  <h2 class="global-h2">Delete Group:</h2>
                  <hr style="margin-top:-5px; margin-bottom:5px;">
                  <p class="global-p-small">If for some reason, you are sure you want to permanently delete this group, you may do so below.</p>
                  <a href="#" data-reveal-id="delete-group" class="button alert tiny expand">Delete Group</a>
                </div>
              </div>

              <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End Group Tools Modal -->

             <!-- Activity Settings Modal -->
            <div id="activity-settings" class="reveal-modal medium" data-reveal>
            <div class="row">
              <div class="large-12 columns">
                <h2 class="custom-font-small-blue">Group Logging Options</h2>
                <hr style="margin-top:-5px;" />
                <p class="global-p">Change how your group's activity is entered and calculated.</p>
              </div>
            </div>

            <div class="row">

      <div class="large-6 medium-6 columns center">
          <div class="white-bg">
            <i class="fi-torsos size-60 blue"></i>
            <h2 class="global-h2">Log Activity For Group Option</h2>
            <hr style="margin-top:-5px; margin-bottom:10px;">
            <?php if($info['G_GLOG']){ ?>
            <a href="#" class="button tiny expand success">Log For Group is Currently On</a>
            <?php }else{ ?>
            <a href="enable_group_logging.php?group=<?php echo $group_id; ?>" class="button tiny expand">Switch to Log For Group</a>
			<?php }?>
            <p class="global-p-small justify">This option allows the group administrator to enter their group’s activity as one total. Perfect for schools or group fitness classes that want to track their progress as a team, but not individually.</p>
          </div>
        </div>

        <div class="large-6 medium-6 columns center">
          <div class="white-bg">
            <i class="fi-torsos-all size-60 blue"></i>
            <h2 class="global-h2">Log Activity as Individuals Option</h2>
            <hr style="margin-top:-5px; margin-bottom:10px;">
            <?php if($info['G_GLOG']){ ?>
            <a href="disable_group_logging.php?group=<?php echo $group_id; ?>" class="button tiny expand">Switch to Total User's Activity</a>
            <?php }else{ ?>
            <a href="#" class="button tiny expand success">Total User's Activity is Currently On</a>
			<?php }?>
            <p class="global-p-small justify">In this option, as members of the group log their own personal activity, that activity contributes to the total of this group. The group administrator can view this group's report, as well as the progress and personal activity of individual members. This option is perfect for organizations who are creating challenges, competitions, or incentives for members or employees.</p>
          </div>
        </div>

    </div>


            <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End Activity Settings Modal -->

            <!-- Invite Others Modal -->
            <div id="share-url" class="reveal-modal medium" data-reveal>
            <div class="row">
              <div class="large-12 columns">
                <h2 class="custom-font-small-blue">Share Group Link</h2>
                <hr style="margin-top:-5px;" />
                <p class="global-p">The link below is unique to your group.  Copy it and send it however you like (email, print, social media) to the people you want to join your group!</p>
              </div>
            </div>
            <div class="row">
              <div class="large-12 columns">
              <form>
                <div class="row collapse">
                  <div class="small-3 large-2 columns">
                    <span class="prefix">Link:</span>
                  </div>
                  <div class="small-9 large-10 columns">
                    <input type="text" class="copytext" placeholder="Link here"
                    value="<?php echo 'http://'.$HTTP_HOST.filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_SANITIZE_STRING).'?group='.$group_id; ?>" readonly>
                  </div>
                    <div>
                        <button type="button" class="copybtn" >Copy to Clipboard</button>
                    </div>
                </div>
              </form>
              </div>
            </div>
            <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End Invite Others Modal -->

            <!-- Edit Group Info Modal -->
            <div id="edit-group" class="reveal-modal medium" data-reveal>
              <div class="row">
                <div class="large-12 columns">
                  <h2 class="custom-font-small-blue">Edit Group Info</h2>
                  <hr style="margin-top:-5px;" />
                </div>
              </div>
              <form method="post" action="edit_group.php">
              <div class="row">
                <div class="large-12 columns">
                  <div class="row">
                    <div class="large-6 columns">
                      <label>Group Name</label>
                      <input type="text" id="NAME" name="NAME" value="<?php echo $info['G_NAME']; ?>" />
                    </div>
                  </div>
                  <div class="row">
                    <div class="large-12 columns">
                      <label>Group Description</label>
                      <textarea id="DESC" name="DESC" style="height:100px;"><?php echo $info['G_DESC']; ?></textarea>
                    </div>
                  </div>
                  <div class="row">
                    <div class="large-6 columns">
                      <label>Group Visibility</label>
                      <select id="G_VIS" name="G_VIS">
                        <option id="G_VIS_1" name="G_VIS_1" value="0" <?php if($info['G_VIS']==0){echo 'selected="selected"';} ?>>Public (Any user can find the group and join)</option>
                        <option id="G_VIS_0" name="G_VIS_0" value="1" <?php if($info['G_VIS']==1){echo 'selected="selected"';} ?>>Private (Only users who have the link to the group's page may join)</option>
                      </select>
                      <input type="hidden" id="G_ID" name="G_ID" value="<?php echo $group_id; ?>" />
                      <br />
                      <br />
                    </div>
                  </div>
                  <div class="row">
                    <div class="large-6 columns">
                      <input type="submit" class="tiny button" value="Save Changes"/>
                    </div>
                  </div>
                </div>
              </div>
              </form>
              <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End Edit Group Info Modal -->

            <!-- Delete Group Modal -->
            <div id="delete-group" class="reveal-modal tiny" data-reveal>
              <div class="row center">
                <div class="large-12 columns">
                  <h2 class="custom-font-small-blue">Delete Group</h2>
                  <hr style="margin-top:-5px;" />
                  <p class="global-p">Are you sure you want to permanently delete this group?</p>
                  <a href="delete_group.php?group=<?php echo $group_id; ?>" class="button tiny alert round">Delete</a> <a href="#" class="button tiny round">Cancel</a>
                </div>
              </div>
            <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End Delete Group Modal -->

            <?php
			}
			?>


      <!-- End Modals Section -->


      <div class="row" style="margin-top:20px;">
        <!-- Past Activity -->
        <div class="large-5 columns">
        <?php $activity = GROUP::display_activity(5); ?>
        </div>
        <!-- End Past Activity -->

        <!-- Past Activity Modal -->
        <!-- Invite Others Modal -->
            <div id="group-past-activity" class="reveal-modal" data-reveal>
            <div class="row">
              <div class="large-12 columns">
               <?php
                // if($info['G_GLOG']){
                //   $list = GROUP::display_activity_table($group_id);
                // }
                // else {
                //   $list = GROUP::display_activity_table_indiv($group_id);
                // }
	  		  ?>
              </div>
            </div>
            <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End Invite Others Modal -->
        <!-- End Past Activity Modal -->


          <!-- Quick Stats Section -->

          <div class="large-7 medium-7 small-12 columns">

          <h2 class="custom-font-small-blue">Quick Stats</h2>
        <hr style="margin-top:10px; margin-bottom:15px;" />

            <!-- Points -->
        <div class="blue-bg-link" id="points-toggle" style="padding: 10px 20px 10px 20px; margin: 0px 0px 0px 0px; cursor:pointer;">
        <h2 class="global-h2-white"><b></b></h2>
        <h2 class="custom-font-big-white" style="margin-bottom:-20px;"><?php echo $group['points'];?></h2>
        <h3 class="global-h2-white" style="margin-top:-20px;">Points Earned</h3>

        <!-- Points Breakdown -->
        <div id="points-breakdown" style="display:none;">
          <ul class="global-p-white">
            <?php
	  	      foreach($all_activity_types as $activity) {
            ?>
        	    <li><?php echo($activity['name'].": ".$activity['points'])?></li>
            <?php
			      }
            ?>
          </ul>
        </div>
        <!-- End Points Breakdown -->

      </div>
      <!-- End Points -->

      <!-- Time -->
      <div class="peach-bg-link" id="time-toggle" style="padding: 10px 20px 10px 20px; margin: 0 0 0 0; cursor:pointer;">
        <div style="display:block; margin-bottom:-15px;">
          <h3 class="global-h2-white"><b>Time Exercised:</b></h3>
        </div>
        <!-- Hours -->
        <div style="display:inline-block; margin-right:15px;">
          <h2 class="custom-font-big-white" style="margin-bottom:-20px;"><?php echo $hours = number_format(floor($group['seconds']/3600)); ?></h2>
          <h3 class="global-h2-white" style="margin-top:-20px;"><b>Hours</b></h3>
        </div>
        <!-- End Hours -->
        <!-- Minutes -->
        <div style="display:inline-block">
          <h2 class="custom-font-big-white" style="margin-bottom:-20px;"><?php echo $minutes = floor(($group['seconds']%3600)/60); ?></h2>
          <h3 class="global-h2-white" style="margin-top:-20px;"><b>Minutes</b></h3>
        </div>
        <!-- End Minutes -->

        <!-- Time Breakdown -->
        <div id="time-breakdown" style="display:none;">
          <ul class="global-p-white">
            <?php
	  	      foreach($all_activity_types as $activity) {
            ?>
        	    <li><?php echo($activity['name'].": ".number_format(($activity['seconds'] / 3600))."h ".number_format((($activity['seconds'] % 3600) / 60))."m"); ?></li>
            <?php
			      }
            ?>
          </ul>
        </div>
        <!-- End Time Breakdown -->

      </div>
      <!-- End Time -->

      <!-- Distance -->
      <div class="green-bg-link" id="distance-toggle" style="padding: 10px 20px 10px 20px; margin: 0 0 0 0; cursor:pointer;">
        <div style="display:block; margin-bottom:-15px;">
          <h3 class="global-h2-white"><b>Distance Traveled:</b></h3>
        </div>
        <!-- Miles -->
        <div style="display:inline-block; margin-right:15px;">
          <h2 class="custom-font-big-white" style="margin-bottom:-20px;"><?php echo number_format($group['miles']); ?></h2>
          <h3 class="global-h2-white" style="margin-top:-20px;"><b>Miles</b></h3>
        </div>
        <!-- End Miles -->

        <!-- Distance Breakdown -->
        <div id="distance-breakdown" style="display:none;">
          <ul class="global-p-white">
            <?php
	  	      foreach($all_activity_types as $activity) {
              // Check to make sure it has miles logged and that the metric is 1, which means that is a distance based activity.
              if(($activity['miles']) && ($activity['metric'] == 1)) {
              ?>
          	    <li><?php echo($activity['name'].": ".number_format($activity['miles'])." miles"); ?></li>
              <?php
              }
			      }
            ?>
          <?php
			  	// foreach($breakdown['DISTANCE'] as $key=>$activity){
					// $DA = MSSQL::query('SELECT A_UNIT FROM ACTIVITY WHERE A_ID=\''.$key.'\'');
					// $DA = odbc_result($DA, 'A_UNIT');
					// if($DA ==1){
					?>
                    <!--<li><?php // echo ACTIVITY::activity_to_form($key).': '.number_format($activity).' miles'; ?></li>-->
                    <?php
				// 	}
				// }
			  ?>

          </ul>
        </div>
        <!-- End Distance Breakdown -->

      </div>

      <!-- End Distance -->

      <?php
					//$isAdmin = GROUP::isAdmin($group_id);
                    if($isAdmin){
						?>
                    <br />
                    <a href="<?php echo 'http://'.$HTTP_HOST.'/reporting.php?group='.$group_id ?>" class="tiny button" target="_blank">Generate Group Report</a>
                    <?php
					}
					?>

          </div>

          <!-- End Quick Stats Section -->


         </div>


  <div class="row">

    <div class="large-6 medium-6 columns">

      <!-- Members Section -->

      <h2 class="custom-font-small-blue">Members</h2>
            <hr style="margin-top:10px; margin-bottom:0px;" />
            <br />
            <div class="data-table">
        			<table class="display" id="" style="width:100%;">
        			  <thead>
          				<th class="global-p-white" style="width:60px; padding:5px;">
          				  <img src="../../img/default-group-icon.png" alt="default group icon" style="display:block; margin:auto;"/>
          				</th>
          				<th class="global-h2">
          					User Name
          				</th>
          				<?php
          					if($isAdmin) {
          						?>
          				<th class="global-h2">
          					Admin Status
          				</th>
          						<?php
          					}
          				?>
        			  </thead>
              <?php
              // $members = GROUP::list_users_in_group($group_id, 4);
              for($x = 0; $x < $group['total_members']; $x++) {
              ?>
                <tr>
                  <td class="global-p" style="width:60px; padding:5px;">
                	  <a href="<?php echo ('http://'.$HTTP_HOST.'/'); ?>index.php?uid=<?php echo ($group['all_members'][$x]['id']); ?>">
                		<?php $avatar = ACCOUNT::avatar_small($group['all_members'][$x]['id']);?>
                	</a>
                  </td>
                  <td class="global-p">
                	<a href="<?php echo ('http://'.$HTTP_HOST.'/'); ?>index.php?uid=<?php echo ($group['all_members'][$x]['id']); ?>">
                	  <br />
                	  <p class="global-p"><?php echo ($group['all_members'][$x]['fname'].' '.$group['all_members'][$x]['lname']); ?></p>
                	</a>
                  </td>
                  <?php if($isAdmin) { ?>
                    <td class="global-p">
                  	<?php
                    if($group['all_members'][$x]['is_admin']) {
                  	?>
                      <a href="<?php echo ('http://'.$HTTP_HOST.'/'); ?>group/demote_admin.php?group=<?php echo ($group_id); ?>&uid=<?php echo ($group['all_members'][$x]['id']); ?>" class="button tiny expand alert">Remove Admin Status</a>
                  	<?php
                    } else {
                    ?>
                      <a href="<?php echo ('http://'.$HTTP_HOST.'/'); ?>group/promote_to_admin.php?group=<?php echo ($group_id); ?>&uid=<?php echo ($group['all_members'][$x]['id']); ?>" class="button tiny expand">Make Admin</a>
                    <?php } ?>
                    </td>
                  <?php } ?>
                </tr>
              <?php } ?>
              </table>
            </div>

    </div>
    <!-- End Members Section -->

    <div class="large-6 medium-6 columns">

      <!-- Subgroup Section -->

        <h2 class="custom-font-small-blue">Subgroups</h2>
        <hr style="margin-top:10px; margin-bottom:15px;" />

              <?php
			    $subgroups = GROUP::list_subgroups_table($group_id);
			  ?>


         <!-- End Subgroup Section -->

    </div>

    </div>

<!-- End Main Section -->
</div>
<!-- End Main Section -->
<?php
//STOPEDITING

//JAVASCRIPTS GO HERE
?>
	<div class="footer">
      <div class="row">
        <div>
          <a class="tiny button alert" href="http://<?php echo $HTTP_HOST; ?>/contact-us.php"> Report a Bug </a>
        </div>

        <p class="global-p">
          <a href="http://www.facebook.com/walkgeorgia">Facebook</a> | <a href="https://twitter.com/walkga">Twitter</a> | <a href="http://blog.extension.uga.edu/walkgeorgia">Walk Georgia Blog</a>
        </p>
        <br />
          <small>The College of Agricultural and Environmental Sciences and the College of Family and Consumer Sciences cooperating.
The University of Georgia &copy; 2014. All Rights Reserved.</small>
          <hr><img alt="The University of Georgia Cooperative Extension" style="width:87px; height:31px;" src="../img/ext.png"><br><br>
        </div>
      </div>
    </div>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="../js/foundation.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/plug-ins/380cb78f450/integration/foundation/dataTables.foundation.js"></script>
<script type="text/javascript" src="../js/frontend.js"></script>
<script type="text/javascript" src="../js/log_form.js"></script>
<script>
     $(document).foundation();
</script>
<script>
$(document).ready(function() {
    	  $('table.display').DataTable({
			"oLanguage": {
              "sEmptyTable": " "
            },
	        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
			responsive:true
		  });
            $(".copybtn").click(function(){
                var copyTextArea = document.querySelector('.copytext');
               copyTextArea.select();
              document.execCommand('copy');
    });
});
</script>
</body>
</html>
