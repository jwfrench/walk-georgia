<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/groups_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
$group = filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING);
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
	SESSION::logout();
	REDIRECT::home();
}
if(filter_input(INPUT_POST, 'file', FILTER_SANITIZE_STRING) != null){
	$img = GROUP::avatar_upload($group);
}
HTML_ELEMENT::head('Groups');
HTML_ELEMENT::top_nav();
//EDIT HERE
//If the group exists
if($group){
  $info = GROUP::get_info($group);
?> 
<div class="main">
  <div class="row center" style="margin-top:2em;">
  
  <div class="large-5 medium-5 columns">
    <div class="sidebar">
      <!-- Avatar -->
       <?php $avatar = GROUP::avatar_medium($group);?>
      <!-- End Avatar -->
      <!-- Change Avatar Modal -->
        <div id="avatar" class="reveal-modal" data-reveal="">
          <div class="row">
            <div class="large-12 columns">
            <p class="global-p">Do you want to upload a new image?</p>
              <form method="post" enctype="multipart/form-data">
                <img id="blah" src="<?php echo $filename; ?>" alt="your image" height="240px;" width="240px;" style="border-radius:50%;" />
                  <br>
                  <br>
                  <script type="text/javascript">
	        			function readURL(input) {
			        	    if (input.files && input.files[0]) {
			    	            var reader = new FileReader();
			    	            reader.onload = function (e) {
				                    $('#blah').attr('src', e.target.result);
				                }
				                reader.readAsDataURL(input.files[0]);
				            }
			    	    }
			      </script>
                  <input type="file" name="group_img" id="group_img" onchange="readURL(this);">
                  <br>
                  <input type="submit" name="file" id-"file"="" value="Submit">
              </form>
            </div>
          </div>
          <a class="close-reveal-modal">×</a>
        </div>
      <!-- End Change Avatar Modal -->
  
      <h1 class="custom-font-small-blue"><?php echo $info['G_NAME']; ?></h1>
      
      </div>
      <a href="http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/group/groups.php?group=<?php echo $group; ?>" class="button tiny" style="float:left; width:50%">Back to Group</a>
      <a href="../faq.php#F12" class="button tiny success" style="float:left; width:50%;">Help</a>    
      <!-- End Desktop Sidebar -->


    </div>
    
    <!-- Right Side Sub-Group Creation -->
      <div class="small-12 medium-7 large-7 columns">
        <div class="row">
          <div class="large-12 columns">
            <h2 class="custom-font-small-blue">Create / Edit Sub-Groups</h2>
            <hr style="margin-top:5px;" />
          </div>
        </div>
        
        <!-- Breadcrumb -->
        <div class="row">
          <div class="large-12 columns">
            <ul class="breadcrumbs">
              <?php $bc = GROUP::breadcrumbs_sub($group);?>
            </ul>
          </div>
        </div>
        <!-- End Breadcrumb -->
        
        <div class="row" style="list-style:none;">
          <div class="large-12 columns">
            <?php $subgroups = GROUP::list_subgroups($group); ?>
            <a href="#" class="button tiny success expand round" style="text-align:left; padding-left:15px;" data-reveal-id="create-a-group">Create a new subgroup</a>
          </div>
        <div class="row">
          <div class="small-6 medium-3 columns sub-group-container center" style="margin-bottom:10px;">
          
          </div>
        </div>
        </div>
        
      </div>
<!-- End Main Section -->  
</div>
<!-- End Main Section -->
<!-- Create a group Modal Part I (Group Info / Visibility) -->
  <div id="create-a-group" class="reveal-modal medium" data-reveal>
    <div class="row">
      <div class="large-12 columns"> 
        <h2 class="custom-font-small-blue">Create a Sub-Group</h2>
        <hr style="margin-top:-5px;" />
      </div>
    </div>
    <div class="row">
      <div class="large-12 columns">
        <form action="create_subgroup.php" id="group-form" method="post">
          <div id="0">
            <div class="row">
              <div class="large-12 columns">
                <div class="row">
                  <div class="large-6 columns">
                    <label>Group Name</label>
                    <input type="text" id="G_NAME" name="G_NAME" placeholder="Ex: The Alpha Team" />
                  </div>
                </div>
                <div class="row">
                  <div class="large-12 columns">
                    <label>Group Description</label>
                    <textarea id="G_DESC" name="G_DESC" placeholder="Enter your group description here." style="height:100px;"></textarea>
                    <input id="G_PERM" name="G_PERM" type="hidden" value="<?php echo $info['G_PERMISSIONS']; ?>">
                    <input type="hidden" id="G_META" name="G_META" value="<?php echo $info['G_META']; ?>"/>
                    <input type="hidden" id="PARENT_ID" name="PARENT_ID" value="<?php echo $group; ?>"/>
                    <button class="tiny submit" onclick="return submit_form();">Create Subgroup</button>
                  </div>
                </div>
              </div>
            </div>
          </div>    
        </form>
      </div>
    </div>
    <a href="delete_group.php?group=<?php echo $group; ?>" class="close-reveal-modal">&#215;</a>
  </div>
<!-- Create a group Modal Part IV (Additional Information) -->     
            
</div>
      
    
    
<?php 
}
//If no group id is specified, or if there is an error loading the group page
		else{
			$gohome = REDIRECT::home();
		}

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../js/modernizr.js"></script>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script src="../js/foundation/foundation.js"></script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script src="../js/foundation/foundation.topbar.js"></script>
    <script src="../js/foundation/foundation.abide.js"></script>
    <script src="../js/foundation/foundation.reveal.js"></script>
    <script src="../js/foundation/foundation.tooltip.js"></script>
    <script src="../js/foundation/foundation.offcanvas.js"></script>
    <script src="../js/foundation/foundation.equalizer.js"></script>
    <script src="../js/foundation/foundation.dropdown.js"></script>
    <script type="text/javascript" src="../js/sha512.js"></script>
    <script type="text/javascript" src="../js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>