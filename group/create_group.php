<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/groups_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
//Grab the user's Session Info
$info = GROUP::get_info($_SESSION['ID']);
//Collect the Group Preferences
$G_VIS = $_POST['G_VIS'];
$U_NAME = 0;
$U_EMAIL = 0;
$U_COUNTY = 0;
if(filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING)){
	if($_POST['name'] == 'on'){
		$U_NAME = 1;
	}
}
if(filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL)){
	if($_POST['email'] == 'on'){
		$U_EMAIL = 1;
	}
}
if(filter_input(INPUT_POST, 'county', FILTER_SANITIZE_STRING)){
	if($_POST['county'] == 'on'){
		$U_COUNTY = 1;
	}
}
//This should change to be 3 separate fields in the database
$G_PERM = $U_NAME.":".$U_EMAIL.":".$U_COUNTY;
$G_META ='';
for($i=0;$i <50; $i++){
	if(isset($_POST['create_meta_'.$i])){
		$G_META .=$_POST['create_meta_'.$i].":";
	}
}

$G_META = rtrim($G_META, ':');

if((filter_input(INPUT_POST, 'PARENT_ID', FILTER_SANITIZE_STRING)!= '')&&(filter_input(INPUT_POST, 'PARENT_ID', FILTER_SANITIZE_STRING)!= NULL)){
    $create_group = GROUP::create(filter_input(INPUT_POST, 'PARENT_ID', FILTER_SANITIZE_STRING), filter_input(INPUT_POST, 'G_DESC', FILTER_SANITIZE_STRING), filter_input(INPUT_POST, 'G_NAME', FILTER_SANITIZE_STRING), $G_VIS, $G_META, $G_PERM, filter_input(INPUT_POST, 'G_ISROOTGROUP', FILTER_SANITIZE_STRING), $info['G_COUNTY'], filter_input(INPUT_POST, 'G_GLOG', FILTER_SANITIZE_STRING));
    $group_uuid = odbc_result(MSSQL::query('SELECT G_UUID FROM GROUPS WHERE G_ID=\''.$create_group.'\''), 'G_UUID');
    for($i=0;$i<150;$i++){
            if(isset($_POST['sub_'.$i])){
              $sub_group = GROUP::create_sub($group_uuid, $_POST['sub_'.$i], $_POST['sub_'.$i], $G_VIS, $G_META, $G_PERM, '0', $info['G_COUNTY'], filter_input(INPUT_POST, 'G_GLOG', FILTER_SANITIZE_STRING));
            } 
    }
} 
$redirect = REDIRECT::group($create_group);
?>