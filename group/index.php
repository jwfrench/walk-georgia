<?php
include_once("../lib/template_api.php");
include_once("../lib/back_api.php");
include_once("../lib/groups_api.php");
$ss = SESSION::secure_session();
HTML_ELEMENT::head('Walk Georgia | ');
HTML_ELEMENT::top_nav();

//EDIT HERE
//If the group exists
if($_GET['group']){
  $info = GROUP::get_info($_GET['group']);
?> 
<div class="main">
  <div class="row" style="margin-top:80px;">
    <!-- Desktop Sidebar -->
    <div class="large-5 medium-5 small-12 columns">
      <div class="hide-for-small" style="margin-bottom:20px;">
        <div class="sidebar">
          <div align="center">
            <img src="../img/default-group-profile.png" alt="Image of the Group" />
  
            <h1 class="global-h1-small"><?php echo $info['G_NAME'];?></h1>
            <?php
			if ($_SESSION['valid']){
				if($joined){
				  ?>
                    <a href="#" class="button success expand" data-reveal-id="group-status">You're a Member</a>
                    <a href="#" class="button expand" data-dropdown="group-tools">Group Tools &raquo;</a>
                  <?php
				  	if($info['P_ID'] == 'U.'.$_SESSION['ID']){
						//you get admin tools	
					}
				}else{
					//if you have logged in, want to join the group, AND it HAS NO special permission or data requirements
					if($info['G_META']=='' && $info['G_PERMISSIONS'] == ''){
				      ?>
					    <a href="?group=<?php echo $_GET['group']; ?>&join=1" class="button expand">Join Group</a>
                      <?php
					}
					//if you have logged in, want to join the group, AND it HAS special permission or data requirements
					else{
						//modal here with permissions form
						?>
                          <div id="join-group" class="reveal-modal" data-reveal>
                            <div class="row">
                              <form>
                                <div class="large-12 columns">
                                  <h2 class="global-h2">Join Group</h2>
                                  <hr />
                                  <p class="global-p">This group requires additional information to join:</p>
                                  <?php if($info['G_META']){ ?>
                                  <div class="row">
                                    <div class="large-6 columns">
                                      <?php
										  GROUPS::meta_form($_GET['group']);
									  ?>
                                    </div>
                                  </div>
                                  <?php }
                                  if($info['G_PERMISSIONS']){ ?>
                                  <div class="row">
                                    <div class="large-6 columns">
                                      <label>By joining this group, I give access to my name, profile picture, and exercise data I choose to the group's page.</label>
                                      <input id="checkbox1" type="checkbox"><label for="checkbox1">I Agree</label>
                                    </div>
                                  </div>
                                  <?php } ?>
                                  <a href="?group=<?php echo $_GET['group']; ?>&join=1" class="button success">Join Group</a>
                                </div>
                              </form>
                            </div>
                            <a class="close-reveal-modal">&#215;</a>
                          </div>
                          <a class="button expand" data-reveal-id="join-group">Join Group</a>
                        <?php
					}
				}
			}else{
				?>
                  <a href="www.walkgeorgia.org/login.php?group=<?php echo $_GET['group']; ?>" class="button expand">Join Group</a>
                <?php
			}
			?>
            <!-- User-Group Relationship Modal -->
            <div id="group-status" class="reveal-modal" data-reveal>
              <h3 class="global-h3">You are currently a member of this group.</h3>
              <br />
              <a href="#" class="button alert">Leave group.</a>
              <h3 class="global-h3">Are you sure you want to leave this group?</h3>
              <br />
              <a class="button alert">I'm sure.</a> <a class="button">Whoops! Cancel</a>
              <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End User-Group Relationship Modal -->
            
            <!-- Group Tools Dropdown -->
            <ul id="group-tools" class="medium content f-dropdown" data-dropdown-content>
              <li><a href="#">Group Chat</a></li>
              <li><a href="#">Potential Admin Option 1</a></li>
              <li><a href="#">Potential Admin Option 2</a></li>
            </ul>
            <!-- End Group Tools Dropdown -->
      
          </div>
          
        </div>
      </div>
      <!-- End Desktop Sidebar -->
      
      <!-- Mobile Sidebar -->
      <div class="sidebar show-for-small-only" style="margin-bottom:20px;">
          <div align="center">
            <img src="../img/default-group-profile.png" alt="Session Image" />
  
            <h1 class="global-h1-small">Group Title</h1>
 
            <a href="#" class="button expand">Join Group</a>
            <a href="#" class="button success expand" data-reveal-id="group-status">You're a Member</a>
            <a href="#" class="button expand" data-dropdown="group-tools">Group Tools &raquo;</a>
            
            <!-- User-Group Relationship Modal -->
            <div id="group-status" class="reveal-modal" data-reveal>
              <h3 class="global-h3">You are currently a member of this group.</h3>
              <br />
              <a href="#" class="button alert">Leave group.</a>
              <h3 class="global-h3">Are you sure you want to leave this group?</h3>
              <br />
              <a class=" button alert">I'm sure.</a> <a class="button">Whoops! Cancel</a>
              <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End User-Group Relationship Modal -->
            
            <!-- Group Tools Dropdown -->
            <ul id="group-tools" class="medium content f-dropdown" data-dropdown-content>
              <li><a href="#">Group Chat</a></li>
              <li><a href="#">Potential Admin Option 1</a></li>
              <li><a href="#">Potential Admin Option 2</a></li>
            </ul>
            <!-- End Group Tools Dropdown -->
      
          </div>
          
        </div>
      <!-- End Mobile -->
    </div>
    
    
    <!-- Right Side Top -->
    <div class="large-7 medium-7 columns">
      <div class="row">
        <div class="medium-12 columns">
          <!-- Session Description -->
          <div align="justify">
            <h2 class="global-h2-gray">Description:</h2>
            <br />
            <br />
            <p class="global-p">
            <?php echo $info['G_DESC']; ?>
            <br />
            <?php print_r($info); ?>
            </p>
          </div>
          <!-- End Session Description -->
          <hr />
          
          <!-- Session Stats -->
          <h2 class="global-h2-gray"><b>Group Stats:</b></h2>
      
          <div class="stat-unit"> 
            <div class="row">
              <div class="large-12 columns">     
                <img src="../img/points-icon.png" class="stat-icon hide-for-small-only" alt="Points Icon"> <a href="#" data-dropdown="drop1" class="statlink"><b>Total Points: 2014</b></a>
        
                <div id="drop1" data-dropdown-content class="f-dropdown content">
                  <small><b>Breakdown:</b></small>
                  <hr style="margin-top:5px; margin-bottom:5px;" />
                  <small>To find out where these points came from, look in the "Past Activity" section below.</small>
                </div>
            
              </div>
            </div>
          </div>
      
          <div class="stat-unit">
            <div class="row">
              <div class="large-12 columns">
                <img src="../img/time-icon.png" class="stat-icon hide-for-small-only" alt="Time Icon"> <a href="#" data-dropdown="drop2" class="statlink"><b>Hours Exercised: 41</b></a>
        
                <div id="drop2" data-dropdown-content class="f-dropdown content">
                  <small><b>Breakdown:</b></small>
                  <hr style="margin-top:5px; margin-bottom:5px;" />
                  <small>Biking: 20</small>
                  <br />
                  <small>Running: 20</small>
                  <br />
                  <small>Yoga: 1</small>
                </div>
        
              </div>
            </div>
          </div>
    
          <div class="stat-unit">
            <div class="row">
              <div class="large-12 columns">
                <img src="../img/distance-icon.png" class="stat-icon hide-for-small-only" alt="Distance Icon"> <a href="#" data-dropdown="drop3" class="statlink"><b>Miles Traveled: 10</b></a>
        
                <div id="drop3" data-dropdown-content class="f-dropdown content">
                  <small><b>Breakdown:</b></small>
                  <hr style="margin-top:5px; margin-bottom:5px;" />
                  <small>Biking: 5</small>
                  <br />
                  <small>Running: 5</small>
                </div>
        
              </div>
            </div>
          </div>
      
          <hr style="margin-bottom:5px;" /> 
          <a href="#"><small>Questions about these stats?  Click here.</small></a>
       
          <!-- End Session Stats -->
          
          <!-- Users Section -->
          <div class="row" style="margin-top:20px;">
            <div class="large-12 columns">
              <h2 class="global-h2-gray">Users:</h2>
              <hr style="margin-top:10px; margin-bottom:0px;" />
      
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>John French</strong></p>
                  </div>
                </div>
              </div>
              
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>Zach Galifinakis</strong></p>
                  </div>
                </div>
              </div>
              
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>Sean Penn</strong></p>
                  </div>
                </div>
              </div>
              
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>Jeff Goldblum</strong></p>
                  </div>
                </div>
              </div>
              
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>Bill Murray</strong></p>
                  </div>
                </div>
              </div>
      
      
            </div>
          </div>
          <!-- End Users Section -->
      
          <hr style="margin-top:0px;" />
          <a href="#" data-reveal-id="view-all-users" class="button tiny">View All Users</a>
      
          <!-- View all Users Modal -->
          <div id="view-all-users" class="reveal-modal" data-reveal>
      
          <div class="row">
            <div class="large-6 columns">
              <h2 class="global-h2">Users:</h2>
              <hr />
      
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>John French</strong></p>
                  </div>
                </div>
              </div>
              
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>John French</strong></p>
                  </div>
                </div>
              </div>
              
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>John French</strong></p>
                  </div>
                </div>
              </div>
              
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>John French</strong></p>
                  </div>
                </div>
              </div>
              
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>John French</strong></p>
                  </div>
                </div>
              </div>
      
            </div>
          </div>
          <a class="close-reveal-modal">&#215;</a>
          </div>
          <!-- End View all Users Modal -->
     
        </div>
      </div>  
    </div>
  </div>
<!-- End Main Section -->  
</div>
<!-- End Main Section -->
<?php 
}
//If no group id is specified, or if there is an error loading the group page
		else{
			$gohome = REDIRECT::home();
		}

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
    <script type="text/javascript" src="../js/sha512.js"></script>
    <script type="text/javascript" src="../js/log_form.js"></script>
    <script type="text/javascript" src="../js/frontend.js"></script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>