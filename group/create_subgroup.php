<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/groups_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
$info = GROUP::get_info(filter_input(INPUT_POST, 'PARENT_ID', FILTER_SANITIZE_STRING));
$create_group = GROUP::create_sub($info['G_UUID'], filter_input(INPUT_POST, 'G_DESC', FILTER_SANITIZE_STRING), filter_input(INPUT_POST, 'G_NAME', FILTER_SANITIZE_STRING), $info['G_VIS'], $info['G_META'], $info['G_PERMISSIONS'], '0', $info['G_COUNTY'], $info['G_GLOG']); 
$go_to_group = REDIRECT::group($create_group);
?>
