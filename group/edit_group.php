<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/groups_api.php");
include_once ("$root/lib/back_api.php");
$create_group = GROUP::edit(filter_input(INPUT_POST, 'G_ID', FILTER_SANITIZE_STRING), filter_input(INPUT_POST, 'DESC', FILTER_SANITIZE_STRING), filter_input(INPUT_POST, 'NAME', FILTER_SANITIZE_STRING), filter_input(INPUT_POST, 'G_VIS', FILTER_SANITIZE_STRING));
REDIRECT::group(filter_input(INPUT_POST, 'G_ID', FILTER_SANITIZE_STRING));
?>