<?php
require_once("lib/back_api.php");
require_once("Mail.php");
/* mail setup recipients, subject etc */
$sql = "SELECT L_E FROM LOGIN";
$query = MSSQL::query($sql);
$email = odbc_result($query, 'L_E');
$recipients .= $email;
while(odbc_fetch_row($query)){
	$email = odbc_result($query, 'L_E');
	$recipients .= ", ".$email;
}
$headers["From"] = "walkgeorgia@uga.edu";
$headers["To"] = $recipients;
$headers["Subject"] = "Last Day Reminder";
$headers["date"] = date('Y-m-d');
$mailmsg = "
Thank you for being part of the Walk Georgia program's pilot session this summer. To improve the Walk Georgia experience and encourage participation in the program, we need your feedback and suggestions. We could not grow this program without your responses and user-driven recommendations. 

Would you please take a few minutes to complete our survey? Your responses will be used to help UGA Extension enhance Walk Georgia. The survey is available at: https://ugeorgia.qualtrics.com/SE/?SID=SV_0TAhnDssKthTFTn

The pilot will run through late November, and we welcome your continued use and suggestions. Join us at http://pilot.walkgeorgia.org or email walkga@uga.edu. Don't forget to contact your local UGA Extension office for more information on how community organizations and schools you're involved in can move more and live more with Walk Georgia! Find your local Extension office by calling 1-800-ASK-UGA1 or visiting extension.uga.edu.

Thank you for your help,
The Walk Georgia Team
";

// SMTP server name, port, user/passwd
$smtpinfo["host"] = "waikiki.cc.uga.edu";
$smtpinfo["port"] = "25";
$smtpinfo['localhost'] = $_SERVER['SERVER_NAME'];
// Create the mail object using the Mail::factory method
$mail_object =& Mail::factory("smtp", $smtpinfo);
// Ok send mail 
$mail_object->send($recipients, $headers, $mailmsg);
if (PEAR::isError($mail)){
    echo('<p>' . $mail->getMessage() . '</p>');
} else {
    echo('<p>Message successfully sent!</p><br />'.$headers["date"].'<br /><pre>Headers<br />');
} 
print_r($headers);
?><br />
SMTP Info
<?php
print_r($smtpinfo);
?><br />
<?php
echo $mailmsg;

?>
