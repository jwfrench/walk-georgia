<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
  SESSION::logout();
  REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

<div id="main">

  <!-- Global Header Container -->
  <div id="top nojava">

  <!-- Main Content -->
  <div class="row center">
        <h2 class="font -large -blue -secondary pt4-ns pt3">middle school lesson plans</h2>
    <div class="large-6 medium-6 columns">
      <div class="white-bg">
        <i class="fi-book-bookmark size-60 blue"></i>
        <hr style="margin-top:-5px; margin-bottom:10px;">
        <a href="resources/lesson-plans/BloodPressure.pdf" target="_blank" class="button expand">"High Blood Pressure" Exercise</a>
        <a target="_blank" class="button expand secondary" style="margin-top:-25px;" href="resources/lesson-plans/HighBloodPressure_Key.pdf">"High Blood Pressure" Key</a>
      </div>
    </div>
    <div class="large-6 medium-6 columns">
      <div class="white-bg">
        <i class="fi-book-bookmark size-60 blue"></i>
        <hr style="margin-top:-5px; margin-bottom:10px;">
        <a href="resources/lesson-plans/GoodCommunication.pdf" target="_blank" class="button expand">"Being a Good Communicator" Exercise</a>
        <a target="_blank" class="button expand secondary" style="margin-top:-25px;" href="resources/lesson-plans/GoodCommunication.pdf">"Being a Good Communicator" Key</a>
      </div>
    </div>
  </div>

  <div class="row center">

    <div class="large-6 medium-6 columns">
      <div class="white-bg">
        <i class="fi-book-bookmark size-60 blue"></i>
        <hr style="margin-top:-5px; margin-bottom:10px;">
        <a href="resources/lesson-plans/FoodSafety.pdf" target="_blank" class="button expand">"Practicing Food Safety
          " Exercise</a>
          <a target="_blank" class="button expand secondary" style="margin-top:-25px;" href="resources/lesson-plans/FoodSafety_Key.pdf">"Practicing Food Safety
            " Key</a>
          </div>
        </div>

        <div class="large-6 medium-6 columns">
          <div class="white-bg">
            <i class="fi-book-bookmark size-60 blue"></i>
            <hr style="margin-top:-5px; margin-bottom:10px;">
            <a href="resources/lesson-plans/FoodLabels.pdf" target="_blank" class="button expand">"Reading
              Food Labels" Exercise</a>
              <a target="_blank" class="button expand secondary" style="margin-top:-25px;" href="resources/lesson-plans/FoodLabels_Key.pdf">"Reading Food Labels" Key</a>
            </div>
          </div>

        </div>

        <div class="row center">
          <div class="large-6 medium-6 columns">
            <div class="white-bg">
              <i class="fi-book-bookmark size-60 blue"></i>
              <hr style="margin-top:-5px; margin-bottom:10px;">
              <a href="resources/lesson-plans/Cardio.pdf" target="_blank" class="button expand">"Cardiovascular Exercise" Exercise</a>
              <a target="_blank" class="button expand secondary" style="margin-top:-25px;" href="resources/lesson-plans/Cardio_Key.pdf">"Cardiovascular Exercise" Key</a>
            </div>
          </div>
          <div class="large-6 medium-6 columns">
            <div class="white-bg">
              <i class="fi-book-bookmark size-60 blue"></i>
              <hr style="margin-top:-5px; margin-bottom:10px;">
              <a href="resources/lesson-plans/Validinfo.pdf" target="_blank" class="button expand">"Finding Valid Information" Exercise</a>
              <a target="_blank" class="button expand secondary" style="margin-top:-25px;" href="resources/lesson-plans/ValidInfo_Key.pdf">"Finding Valid Information" Key</a>
            </div>
          </div>
        </div>

        <div class="row center">
          <div class="large-6 medium-6 columns">
            <div class="white-bg">
              <i class="fi-book-bookmark size-60 blue"></i>
              <hr style="margin-top:-5px; margin-bottom:10px;">
              <a href="resources/lesson-plans/Activity-Points.pdf" target="_blank" class="button expand">"Activity Points" Exercise</a>
              <a target="_blank" class="button expand secondary" style="margin-top:-25px;" href="resources/lesson-plans/Activity Points KEY.pdf">"Activity Points" Key</a>
              <p class="global-p center">Operations and Algebraic Thinking: Write and interpret numerical expressions</p>
            </div>
          </div>

          <div class="large-6 medium-6 columns">
            <div class="white-bg">
              <i class="fi-book-bookmark size-60 blue"></i>
              <hr style="margin-top:-5px; margin-bottom:10px;">
              <a href="resources/lesson-plans/Every-Beat-of-Your-Heart.pdf" target="_blank" class="button expand">"Every Beat of Your Heart" Exercise</a>
              <a href="#" class="button expand secondary" style="margin-top:-25px;">N/A</a>
              <p class="global-p center">Using techniques and attention to detail, this exercise teaches students about the measurement and changes of heart rate with exercise.</p>
            </div>
          </div>

        </div>

        <div class="row center">

          <div class="large-6 medium-6 columns">
            <div class="white-bg">
              <i class="fi-book-bookmark size-60 blue"></i>
              <hr style="margin-top:-5px; margin-bottom:10px;">
              <a href="resources/lesson-plans/Jog-A-Thon.pdf" target="_blank" class="button expand">"Jog A Thon" Exercise</a>
              <a href="resources/lesson-plans/Jog A Thon KEY.pdf" class="button expand secondary" target="_blank" style="margin-top:-25px;">"Jog A Thon" Key</a>
              <p class="global-p center">Number and Operations with Fractions: Use equivalent fractions as a strategy to add and subtract fractions.</p>
            </div>
          </div>

          <div class="large-6 medium-6 columns">
            <div class="white-bg">
              <i class="fi-book-bookmark size-60 blue"></i>
              <hr style="margin-top:-5px; margin-bottom:10px;">
              <a href="resources/lesson-plans/Lets-Walk.pdf" target="_blank" class="button expand">"Let's Walk!" Exercise</a>
              <a href="resources/lesson-plans/Lets-Walk-Key.pdf" class="button expand secondary" target="_blank" style="margin-top:-25px;">"Lets Walk!" Key</a>
              <p class="global-p center">Operations and Algebraic Thinking: Analyzing patterns and relationships.</p>
            </div>
          </div>

        </div>

        <div class="row center">

          <div class="large-6 medium-6 columns">
            <div class="white-bg">
              <i class="fi-book-bookmark size-60 blue"></i>
              <hr style="margin-top:-5px; margin-bottom:10px;">
              <a href="resources/lesson-plans/Salsa Recipe Updated_KSedit.pdf" target="_blank" class="button expand">"Let's Cook!" Exercise</a>
              <a href="resources/lesson-plans/Salsa Recipe KEY.pdf" class="button expand secondary" target="_blank" style="margin-top:-25px;">"Salsa Recipe" Key</a>
              <p class="global-p center">Using a recipe as a guide, this exercise helps teach fractions.</p>
            </div>
          </div>

          <div class="large-6 medium-6 columns">
            <div class="white-bg">
              <i class="fi-book-bookmark size-60 blue"></i>
              <hr style="margin-top:-5px; margin-bottom:10px;">
              <a href="resources/lesson-plans/Snack-Attack.pdf" target="_blank" class="button expand">"Snack Attack" Exercise</a>
              <a href="resources/lesson-plans/Snack Attack KEY.pdf" class="button expand secondary" target="_blank" style="margin-top:-25px;">"Snack Attack" Key</a>
              <p class="global-p center">This exercise is meant to teach about calories, how they work, and the math behind them.</p>
            </div>
          </div>

        </div>

        <!-- End Main -->
      </div>
      <!-- End Main -->

      <?php

      //STOPEDITING

      HTML_ELEMENT::footer();

      //JAVASCRIPTS GO HERE
      ?>
      <script type="text/javascript">

      $(function() {

        $('#toggle4').click(function() {
          $('.toggle4').slideToggle('fast');
          return false;
        });

      });

      </script>
      <script type="text/javascript">
      function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
      </script>
      <script type="text/javascript">
      (function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
      </script>
      <script src="js/foundation/foundation.abide.js"></script>
      <script src="js/foundation/foundation.reveal.js"></script>
      <script src="js/foundation/foundation.tooltip.js"></script>
      <script src="js/foundation/foundation.offcanvas.js"></script>
      <script src="js/foundation/foundation.equalizer.js"></script>
      <script src="js/foundation/foundation.dropdown.js"></script>
      <script type="text/javascript" src="js/sha512.js"></script>
      <script type="text/javascript" src="js/log_form.js"></script>
      <script>
      $(document).foundation();
      </script>
      <!-- End Footer -->
    </body>
    </html>
