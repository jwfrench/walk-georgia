<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();
if(isset($_SESSION['ID'])){
  $info = ACCOUNT::get_info($_SESSION['ID']);
}
//EDIT HERE

?>

<div id="main nojava">
    <div id="top nojava">

    <!-- Header -->
    <div class="row nojava" style="margin-top:2%;">
      <div class="large-12 columns center">
        
      </div>
    </div>
     
    <!-- End Header -->
    </div>
    <!-- Main Content -->
    <div class="row nojava">
      <div class="large-12 columns center">
        <h1 class="custom-font-big-blue hide-for-small-only">contact us</h1>
        <h2 class="custom-font-small-blue hide-for-medium-up">contact us</h2>
        <?php if(filter_input(INPUT_GET, 'msg', FILTER_SANITIZE_STRING) != null){echo filter_input(INPUT_GET, 'msg', FILTER_SANITIZE_STRING);} ?>
        <hr />
      </div>
    </div>
    
    <div class="row nojava">
      <div class="large-6 columns center">
        <h2 class="custom-font-small-blue">Have a Question?</h2>
        <p class="global-p">Check to see if it's already been answered.  We've been hard at work building resources to help you learn how to use Walk Georgia better.  If you just can't figure something out, check out our FAQ and resources first, then feel free to send us an email if you still can't find what you are looking for.</p>
        <a href="/resources/index.php" class="button tiny round success">Help Resources</a>
      </div>
      <div class="large-6 columns">
        <form data-abide method="post" action="contact-script.php">
          <div class="row">
            <div class="large-12 columns">
              <label>Type of comment/question:
                <select id="subject" name="subject">
                  <option value="Walk GA - General Question">General Question/Comment</option>
                  <option value="Walk GA - Tech Problem">Technical Problem / Bug Report</option>
                </select>
              </label>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <label>Name:
                <input id="name" name="name" type="text" placeholder="First and Last Name" value="<?php if(isset($info)){echo $info['FNAME'].' '.$info['LNAME'];} ?>" required/>
                <small class="error" data-error-message="">A first and last name is required.</small>
              </label>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <label>Email:</label>
                <input id="email" name="email" type="text" placeholder="" value="<?php if(isset($info)){echo $info['EMAIL'];} ?>" required/>
                <small class="error" data-error-message="">An email is required.</small>
            </div>
          </div>  
          <div class="row">
            <div class="large-12 columns">
              <label>Enter your question/comment:</label>
              <textarea id="body" name="body" placeholder="Enter your question or comment here." style="height:200px;" required></textarea>
              <small class="error" data-error-message="">A message is required.</small>
              <?php
              if(isset($info)){
                $admin_info = MSSQL::query('SELECT L_FNAME, L_LNAME, L_E, L_COUNTY FROM LOGIN WHERE RIGHT(L_FLAGS, 1) =\'1\' AND L_COUNTY=\''.$info['COUNTY'].'\'');
        			  $admin_string ='';
        			  while(odbc_fetch_row($admin_info)){
        				    $admin_string .= odbc_result($admin_info, 'L_FNAME').' '.odbc_result($admin_info, 'L_LNAME').', '.odbc_result($admin_info, 'L_LNAME').', '.odbc_result($admin_info, 'L_E').'  |  '.odbc_result($admin_info, 'L_COUNTY');
        			  }
              }
			  ?>
              <input type="hidden" name='admin_info' id='admin_info' value='<?php if(isset($admin_string)){echo $admin_string;} ?>' />
              <input type="submit" class="tiny button" value="Submit" />
            </div>
          </div>
        </form>
      </div>
    </div>
    
    
  <!-- End Main Section -->          
  </div>
  <!-- End Main Section -->

<?php

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script src="js/foundation/foundation.abide.js"></script>
    <script src="js/foundation/foundation.reveal.js"></script>
    <script src="js/foundation/foundation.offcanvas.js"></script>
    <script src="js/foundation/foundation.equalizer.js"></script>
    <script src="js/foundation/foundation.dropdown.js"></script>
    <script src="js/foundation/foundation.alert.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>