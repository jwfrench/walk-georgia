<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/back_api.php");
include_once("$root/lib/template_api.php");
$ss = SESSION::secure_session();
$ID ='';
$group = '';
$_SESSION['ID'] = isset($_SESSION['ID']) ? $_SESSION['ID'] : ""; //defining variable 'ID'

if(($_SESSION['ID'])!= null){
  $ID = $_SESSION['ID'];
  $gohome = REDIRECT::home('Welcome back!');
  exit();
}
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
  SESSION::logout();
  REDIRECT::home();
}
if(filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING) != null){
  $group = filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING);
}
if(filter_input(INPUT_POST, 'group', FILTER_SANITIZE_STRING) != null){
  $group = filter_input(INPUT_POST, 'group', FILTER_SANITIZE_STRING);
}
//EDITING STARTS HERE
//If the user is logged in
if($ID){
  $redirect = REDIRECT::home('Welcome back!');
  exit();
}
//If user is not logged in
else{
  if(filter_input(INPUT_POST, 'EM', FILTER_SANITIZE_EMAIL) != null){
    $login = SESSION::login(filter_input(INPUT_POST, 'EM', FILTER_SANITIZE_EMAIL), filter_input(INPUT_POST, 'P', FILTER_SANITIZE_STRING), $group);
    //if the user wants to be remembered on that device, and that device
    if(!empty($group)){
      $go_to_group = REDIRECT::group($group);
      exit();
    }
    if(!$login){
      REDIRECT::bad_login();
      exit();
    }else{
      $redirect = REDIRECT::home('Success');

    }

  }else{
    HTML_ELEMENT::head('Login', $ID);
    HTML_ELEMENT::top_nav();
    ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>

    function checkForEnter(e, f, P) {
      if (e.keyCode === 13) {
        return regformhash(f, P);
      }
    }
    </script>
    <div id="email_warning" data-alert style="display: none;" class="alert-box warning round">
      Email not found.
      <a href="#" class="close" >&times;</a>
    </div>
    <div data-alert style="display: none;" id="delete_success" class="alert-box success radius">
      Account successfully deleted.
      <a href="#" class="close">&times;</a>
    </div>

    <div class="row nojava" style="margin-top:10%">
      <div align="center">
        <h1 class="custom-font-small">login</h1>
      </div>
      <br />
      <div class="medium-6 medium-centered large-centered large-6 columns">
        <?php if (filter_input(INPUT_GET, 'msg', FILTER_SANITIZE_STRING) != null){?>
          <div data-alert class="alert-box">
            <?php echo filter_input(INPUT_GET, 'msg', FILTER_SANITIZE_STRING); ?>
            <a href="#" class="close">&times;</a>
          </div>
          <?php
        } ?>
        <?php if (filter_input(INPUT_GET, 'fail', FILTER_SANITIZE_STRING) != null){?>
          <div data-alert class="alert-box warning">
            Your username and/or password was incorrect.
            <a href="#" class="close">&times;</a>
          </div>
          <?php
        } ?>
        <?php if (filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING) != null){ ?>
          <div data-alert class="alert-box warning">
            You must sign in before you may join a group.
            <a href="#" class="close">&times;</a>
          </div>
          <?php
        } ?>
        <form data-abide method="post">
          <input type="hidden" name="group_id" id="group_id" value="<?php echo $group; ?>">
          <div class="email-field">
            <label>Email</label>
            <input type="email" name="EM" id="EM" required pattern="email">
            <small class="error" data-error-message="">A valid email address is required.</small>
          </div>

          <div class="password-field">
            <label>Password</label>
            <input type="password" name="P" id="P" onkeyup="return checkForEnter(event, this.form, this.form.P);" required pattern="^(?=.*\d).{4,30}$">
            <small class="error">Passwords must be between 4 and 30 characters, and contain at least one number.</small>
          </div>

          <div class="row">
            <div class="large-6 columns">
              <input type="submit" value="Login" class="button expand tiny" onclick="regformhash(this.form, this.form.P);"/>
            </div>
            <div class="large-6 columns">
              <a href="register.php<?php if(filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING) != null){echo "?group=".$group; } ?>" class="button secondary expand tiny">New user? Create an account.</a>
            </div>
          </div>

          <div class="row">
            <div class="small-12 columns font -standard">
              <input type="checkbox" checked="checked" name="remember-me" id="remember-me" value="true"/> Remember me on this device
            </div>
          </div>
          <div class="row">
            <div class="small-6 columns">
              <p class="global-p"><a href="reset_password.php" >Forgot your password?</a></p>
              <p class="global-p"><a href="register.php<?php if((filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING)!= null)){echo "?group=".filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING); } ?>">New user? Create an account.</a></p>
            </div>
          </div>

        </form>
      </div>
    </div>
    <?php
  }
}
//EDITING ENDS HERE

HTML_ELEMENT::footer();
//JAVASCRIPTS GO HERE
?>
<script src="../js/forms.js"></script>
<script src="../js/sha512.js"></script>
<script src="../js/foundation/foundation.abide.js"></script>
<script>
$(document).foundation();
</script>
<!-- End Footer -->
</body>
</html>
