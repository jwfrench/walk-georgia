﻿<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout')){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

<div id="main">

  <!-- Global Header Container -->
  <div id="top nojava">
  
    <!-- Header Image Container -->
    <div id="slideshow">
      
      <!-- Page Title Container -->
      <div id="slideshow-title">
        <div class="row nojava center">
          <h2 class="custom-font-big-white">about us</h2> 
          <br /><br /><br />
      
          <!-- Header Buttons -->   
          <div class="large-12 columns expand">
            <div class="row center nojava">
              <div class="small-12 medium-4 large-4 columns">
                <a href="#ourStory" class="button small expand round">Our Story</a>
              </div>
              <div class="small-12 medium-4 large-4 columns">
                <a href="faq.php" class="button small success expand round">FAQ</a>
              </div>
              <div class="small-12 medium-4 large-4 columns">
                <a href="resources/index.php" class="button small expand round secondary">Resources</a>
              </div>
            </div>
          </div>
          <!-- End Header Buttons -->
              
        </div>
      </div>
      <!-- End Page Title Container --> 
       
    </div>
    <!-- End Header Image Container -->
    
  </div>
  <!-- End Global Header Container -->
    
    <!-- Main Content -->
    <a name="top"></a>
    
    <div class="row" style="margin-top:20px;"> 
    
      <div class="large-12 medium-12 columns">
        <h2 class="custom-font-small">Walk Georgia TV Spot</h2>
        
        <div class="videoWrapper">
        <iframe src="//www.youtube.com/embed/YTqkyRtrdsk" frameborder="0" allowfullscreen></iframe>
        </div>
      </div>
    </div>
    
    <div class="row" style="margin-top:30px;">
    
    <div class="large-6 medium-6 columns">
        <img src="img/iPhone.jpg" alt="iPhone Screenshot of user interface" />
        <h2 class="custom-font-small">How it Works</h2>
        <p class="global-p" style="text-align:justify;">
        Walk Georgia is, at its heart, a very sophisticated system for keeping track of your physical activity. Based on user and administrator input and feedback, we have learned a great deal since the program began in 2008. With this iteration of the Walk Georgia website, we are rolling out many new features and upgraded functionality. If you have questions about how the system works, head over to our FAQ by clicking the button below.
        </p>
        <div class="center"><a href="faq.php" class="button round success">View the FAQ</a></div>
      </div>
      
      
      <a name="ourStory"></a>
      <div class="large-6 medium-6 columns">
         
        <h2 class="custom-font-small">Who We Are</h2>    
        <p class="global-p justify">Walk Georgia is a free Web-based fitness program designed to encourage activity and exercise through accountability and community! The Walk Georgia program provides free fitness tracking, research-based knowledge, and resources on fitness. The website enables users to track their activities and record fitness progress; users can see how they stack up against other individuals in their county, the state, and can even create custom group challenges! The website also provides resources on health and the tools and information to get physically active in each Georgia county. Users can choose to participate individually, or within a group to foster accountability or competition! Get started by registering <a href="http://www.walkgeorgia.org/register.php">here</a>. We hope you’ll be inspired to join us and others in Georgia as we get more active and become healthier! Walk Georgia is administered in your county by local UGA Extension agents. For more information, call your county Extension office, 1-800-ASK-UGA1.</p>
        
        <h2 class="custom-font-small">A Brief History</h2>
        <p class="global-p" style="text-align:justify;">
          Walk Georgia is supported in part by a grant from The Coca-Cola Foundation. The goal of this public-private partnership is to help improve the lives of Georgia residents through simple ways of being more physically active and making healthy lifestyle choices. Coca-Cola shares our belief that it will take all of us – businesses, government, communities, academia – coming together to address the serious, complex issue of obesity. We are pleased to be partnering with The Coca-Cola Foundation to improve the health and wellness of Georgians.
        </p>
        <p><img src="img/about-us-banner.jpg" alt="Walk Georgia and Coke Logo" /></p>
	<p><a class="button" href="Walk-Georgia-Evaluation-Report-2017.pdf">Project Evaluation Report</a></p>
        <hr />
        
        <h2 class="custom-font-small">Goals</h2>
        <p class="global-p justify">
        The goals of this program, sponsored by the University of Georgia Cooperative Extension and its partners, are to help you:
        <ul class="global-p">
          <li>Get more fit.</li>
          <li>Develop the habit of regular physical activity.</li>
          <li>Get support from others who are also interested in moving more.</li>
          <li>Have fun!</li>
        </ul>
        </p>
        
      </div>
      
    </div>
    
    <div class="row">
      <div class="large-12 columns">
        <a name="resources"></a>
        <h2 class="custom-font-small">Additional Resources</h2>
        <p class="global-p">
        We've been hard at work compiling resources for Walk Georgia users. Most of these items can be located through our ‘Resources' page. We've combined various materials – recipes, Georgia State Park information, online resources and reviews, and health and wellness articles authored by Extension personnel – from our official blog and newsletter.  Visit the Resources page to learn more.
        </p>
        <div class="center"><a href="resources/index.php" class="button round secondary">Visit the Resources Page</a></div>
      <hr />
      </div>
    </div>
            
  </div>

<?php

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script type="text/javascript" src="js/sha512.js"></script>
    <script type="text/javascript" src="js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>