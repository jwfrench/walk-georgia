<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/sessions_api.php");
$session = filter_input(INPUT_GET, 'session', FILTER_SANITIZE_STRING);
$HTTP_HOST = filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);
if(filter_input(INPUT_GET, 'sess_id', FILTER_SANITIZE_STRING) != null){
	$join_session = SESSIONS::join_session_by_add(filter_input(INPUT_GET, 'sess_id', FILTER_SANITIZE_STRING), $_GET['uid']);
	header('Location:http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/edit_user_sessions.php?id='.filter_input(INPUT_GET, 'uid', FILTER_SANITIZE_STRING));
}
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING)==1){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Administration', $_SESSION['valid']);
HTML_ELEMENT::top_nav();
//START EDITING HERE

?>
<div style="margin-top:20px;"></div>
    
    <!-- Main Content -->
    
      <div class="row">
        <div class="large-12 columns center">
          <h1 class="global-h1"><a href="panel.php">Admin Backend</a></h1>
        </div>
      </div>
    <?php
		if(filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING)){
			$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
			$sql = "SELECT * FROM LOGIN WHERE L_ID = '$id'";
			$execute = MSSQL::query($sql);
			
	?>
      <div class="row">
        <div class="large-12 columns">
        <?php
			$USER_ID = odbc_result($execute, 1);
			$info = ACCOUNT::get_info($USER_ID);
			?>
        <!-- When you click 'Edit Sessions' -->
        
          <div class="row">
            <div class="large-12 columns">
              <h2 class="global-h1-small"><?php echo $info['FNAME']." ".$info['LNAME'];?>'s Sessions</h2>
              <p class="global-p">Click on the "Edit Sessions" button to reorganize this user's Sessions Info.</p>
            </div>
          </div>
          
          <?php $activegroups = SESSIONS::listCurrentSessions($id);?>
          
      <!-- Group Search Function -->  
          
          <div class="row">
            <div class="large-12 columns">
              <h2 class="global-h1-small">Add Session by ID:</h2> 
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <form>
                <div class="row">
                  <div class="large-6 columns">
                    <div class="row collapse">
                      <div class="small-10 columns">
                        <input type="number" id="sess_id" name="sess_id" placeholder="Enter Session ID here to add user">
                        <input type="hidden" id="uid" name="uid" value="<?php echo $id;?>">
                      </div>
                      <div class="small-2 columns">
                        <input type="submit" class="button postfix" value="Submit">
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>      
        <!-- End Session Search Function -->          
        
        <!-- When you click 'Edit Session' -->
        <?php 
		}
		?>
        </div>
      </div>
    
    <!-- End Main Content -->
<?php

//END EDITING HERE

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script>
		<?php echo $activegroups; ?>
	</script>
    <script type="text/javascript" src="js/frontend.js"></script>
  <!-- End Footer -->
  </body>
</html>