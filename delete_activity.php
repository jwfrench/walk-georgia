<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
$log = ACTIVITY::delete_activity(filter_input(INPUT_GET, 'aid', FILTER_SANITIZE_STRING));
REDIRECT::home('Activity deleted');
?>