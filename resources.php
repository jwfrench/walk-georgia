<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING)==1){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

  <div id="main">
    <div id="top">

    <!-- Header -->
    <div class="row">
      <div class="large-12 columns center">
        <a href="index.php"><img src="img/single-color-logo.png" alt="Move More, Live More"></a>
      </div>
    </div>
     
    <!-- End Header -->

    </div>
    
    <!-- Main Content -->
    <a name="top"></a>
    <div class="row">
      <div class="large-12 columns center">
        <h1 class="custom-font-big hide-for-small-only">resources</h1>
        <h1 class="custom-font-small hide-for-medium-up">resources</h1>
        <hr />
        
      </div>
    </div>
    
    <div class="row">  
      <div class="large-12 columns center">
        <img src="img/ext.png" alt="Walk Georgia and Coke Logo" />
        <br />
        <br />
        <p class="global-p">
          <strong>
          We're hard at work putting together educational resources for our users.  Since our goal is to help build healthier lifestyles, we want to give you access to more than just exercise.  We're putting together helpful articles, recipies, exercise tips, and more!  Check back here in the coming weeks to see all the helpful resources we'll have for you.
          </strong>
        </p>
        <hr />
      </div>
    </div>    
    
    
  <!-- End Main Content -->          
  </div>
  <!-- End Main Content -->

<?php

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../js/modernizr.js"></script>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script src="js/foundation/foundation.js"></script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script src="js/foundation/foundation.topbar.js"></script>
    <script src="js/foundation/foundation.abide.js"></script>
    <script src="js/foundation/foundation.reveal.js"></script>
    <script src="js/foundation/foundation.tooltip.js"></script>
    <script src="js/foundation/foundation.offcanvas.js"></script>
    <script src="js/foundation/foundation.equalizer.js"></script>
    <script src="js/foundation/foundation.dropdown.js"></script>
    <script type="text/javascript" src="js/sha512.js"></script>
    <script type="text/javascript" src="js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>