<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/back_api.php");
include_once("$root/lib/template_api.php");
require_once('Mail.php');
$ss = SESSION::secure_session();
$EM = filter_input(INPUT_POST, 'EM', FILTER_SANITIZE_EMAIL);
$pass = filter_input(INPUT_POST, 'P', FILTER_SANITIZE_STRING);
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING)==1){
	SESSION::logout();
	REDIRECT::home();
}
//if they are seeing this page via an email or if an admin sent a password reset request
if($EM != null){

    //if they have input their requested password change
    if($pass != null){
      $reset = ACCOUNT::reset_password();
      $redirect = REDIRECT::login('Password reset, use your new login credentials to log in.');
    } else {
      //they need to get an email
      $sendmail = ACCOUNT::mail_reset($EM);
			if($sendmail) {
        $msg = 'err_msg=An email has been sent to your email address containing a password reset link.';
        $redirect = REDIRECT::current_page($msg);
			}
			else {
				$msg = 'err_msg=There is no account registered with this email address.';
				$msg = 'fail=1';
        $redirect = REDIRECT::current_page($msg);
			}
    }
}
if(!empty($_SESSION['valid'])) {
	HTML_ELEMENT::head('Reset Your Password', $_SESSION['valid']);
}
else {
	HTML_ELEMENT::head('Reset Password');
}
HTML_ELEMENT::top_nav();

//EDITING STARTS HERE
			?>
  <script>
	  function checkForEnter(e, f, P) {
	    	if (e.keyCode == 13) {
	        	return regformhash(f, P);
	    	}
		}
  </script>
  <div class="row nojava" style="margin-top:10%">
    <div align="center">
      <h1 class="custom-font-small">reset your password</h1>
    </div>
    <br />
    <div data-alert class="row center large-6">
     <?php if(filter_input(INPUT_GET, 'err_msg', FILTER_SANITIZE_STRING) != null){?>
	     <br />
	     <div data-alert class="center large-12">
	      <?php echo filter_input(INPUT_GET, 'err_msg', FILTER_SANITIZE_STRING);?>
	     </div>
	     <br />
		 <?php } elseif((filter_input(INPUT_GET, 'fail', FILTER_SANITIZE_STRING) == 1)) { ?>
			 <br />
	     <div data-alert class="center large-12">
         <p>
       Sorry, it appears that this email is not registered with the website.<br>Please <a href="/register.php">Sign up Here</a> to start moving more and living more!
     </p>
	     </div>
	     <br />
			<?php } ?>
  </div>
    <div class="medium-6 medium-centered large-centered large-6 columns">
      <form action="" data-abide method="post">
          <div class="email-field">
            <label>Email</label>
            <input id="email" type="email" name="EM" id="EM" value="<?php echo filter_input(INPUT_GET, 'EM', FILTER_SANITIZE_EMAIL); ?>" required <?php if(filter_input(INPUT_GET, 'EM', FILTER_SANITIZE_EMAIL)){echo 'readonly';} ?>></input>
            <small class="error">An email address is required.</small>
          </div>
          <?php
		  $user_uuid = odbc_result(MSSQL::query('SELECT L_UUID FROM LOGIN WHERE L_E =\''.filter_input(INPUT_GET, 'EM', FILTER_SANITIZE_EMAIL).'\''), 'L_UUID');
		  if((filter_input(INPUT_GET, 'UUID', FILTER_SANITIZE_STRING) == $user_uuid) && !(empty($user_uuid))){
		  ?>
          <div class="password-field">
    	    <label>Password</label>
            <input type="password" name="password" id="password" onkeyup="return checkForEnter(event, this.form, this.form.P);" required pattern="^(?=.*\d).{4,30}$">
            <small class="error">Passwords must be between 4 and 30 characters, and contain at least one number.</small>
 	  </div>
          <div class="password2">
            <label for="confirmPassword">Confirm Password</label>
            <input id="confirmPassword" type="password" data-equalto="password" required name="confirmPassword"></input>
            <small class="error" data-error-message="">Passwords must match.</small>
          </div>
          <?php
		  }
		  ?>
          <div class="row">
            <div class="medium-12 large-12 columns">
               <input type="submit" value="Submit" class="button expand tiny" onclick="return regformhash(this.form,this.form.password);"/></button>
               <br />
               <!--
               Saving this for when we actually have it runnning
               <small>Sign in with <a href="#">Facebook</a>, <a href="#">Google</a>, or <a href="#">Twitter</a>.</small>
               -->
            </div>
          </div>
      </form>
    </div>
  </div>
			<?php

//EDITING ENDS HERE
HTML_ELEMENT::footer();
//JAVASCRIPTS GO HERE
?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/forms.js"></script>
    <script src="js/sha512.js"></script>
    <script src="js/foundation/foundation.js"></script>
    <script src="js/foundation/foundation.reveal.js"></script>
    <script src="js/foundation/foundation.topbar.js"></script>
    <script src="js/foundation/foundation.abide.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>
