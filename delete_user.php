<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/back_api.php");
$join_group = ACCOUNT::delete_account(filter_input(INPUT_GET, 'uuid', FILTER_SANITIZE_STRING));
header('Location:http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING).'/?logout=1');
?>