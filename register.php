<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/back_api.php");
include_once("$root/lib/template_api.php");
include_once ("$root/lib/groups_api.php");
$ss = SESSION::secure_session();
//If the user is logged in
$_SESSION['valid'] = isset($_SESSION['valid']) ? $_SESSION['valid'] : null; //defining variable 'valid'
$_GET['logout'] = (filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null) ? filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) : ""; //defining variable 'lougout'
$_GET['group'] = (filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING) != null) ? filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING) : ""; //defining variable 'group'

	if(!empty($_SESSION['valid'])){
		$redirect = REDIRECT::home();
	}

if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING)==1){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Registration', $_SESSION['valid']);
HTML_ELEMENT::top_nav();

//EDITING STARTS HERE

if(filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING)){
	$info = GROUP::get_info(filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING));
}
//If the user is logged in
if($_SESSION['valid']){
	$redirect = REDIRECT::home();
}
//If user is not logged in
else{
?>
  <div class="row nojava" style="margin-top:2%">
    <div class="center"><h1 class="custom-font-small">Create an Account</h1></div>
    <br />
    <div class="medium-6 medium-centered large-centered large-6 columns">
      <form data-abide method="post" action="register_user.php">
          <div class="name-field">
            <label>First Name</label>
            <input type="text" name="FN" id="FN" required pattern="alpha">
            <small class="error" data-error-message="">An alphabetical first name required.</small>
          </div>
          <div class="name-field">
            <label>Last Name</label>
            <input type="text" name="LN" id="LN" required pattern="alpha">
            <small class="error" data-error-message="">An alphabetical last name required</small>
          </div>

          <div class="sex">
            <label>Sex (optional)</label>
            <ul class="small-block-grid-2 medium-block-grid-4">
              <li>
                <input type="radio" name="sex" value="0" id="female"><label>Female</label>
              </li>
              <li>
                <input type="radio" name="sex" value="1" id="male"><label>Male</label>
                <br />
              </li>
            </ul>
          </div>
         <div class="age">
              <label>Year of Birth (optional)
                  <input  type="text" name="birth-year"  pattern="1[8-9][0-9][0-9]|200[0-3]"></input>
                  <small class="error" data-error-message="">Hi there! Looks like you're under 13. We're not able to grant accounts to participants under 13 because of the Children’s Online Privacy Protection Act (COPPA). We encourage you to work with a parent or guardian to keep track of your physical activity, and please look around the site for fun fitness demos, activities, and recipes!</small>
              </label>
          </div>

          <div class="email-field">
            <label>Email</label>
            <input id="email" type="email" name="EM" id="EM" required></input>
            <small class="error">An email address is required.</small>
          </div>
          <div class="email-field">
            <label>Re-enter Email</label>
           <input type="email" data-equalto="email" required></input>
           <small class="error">The emails must match.</small>
          </div>
          <div class="password">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" required pattern="^(?=.*\d).{4,30}$">
          <small class="error">Passwords must be between 4 and 30 characters, and contain at least one number.</small>
          </div>
          <div class="password2">
            <label for="confirmPassword">Confirm Password</label>
            <input id="confirmPassword" type="password" data-equalto="password" required name="confirmPassword" data-invalid=""></input>
            <small class="error" data-error-message="">Passwords must match.</small>
          </div>
          <div class="county">
            <label>County of Participation
               <select name="CN" id="CN" required>
                <option ></option>
                <option value="Appling">Appling</option>
                <option value="Atkinson">Atkinson</option>
                <option value="Bacon">Bacon</option>
                <option value="Baker">Baker</option>
                <option value="Baldwin">Baldwin</option>
                <option value="Banks">Banks</option>
                <option value="Barrow">Barrow</option>
                <option value="Bartow">Bartow</option>
                <option value="Ben Hill">Ben Hill</option>
                <option value="Berrien">Berrien</option>
                <option value="Bibb">Bibb</option>
                <option value="Bleckley">Bleckley</option>
                <option value="Brantley">Brantley</option>
                <option value="Brooks">Brooks</option>
                <option value="Bryan">Bryan</option>
                <option value="Bulloch">Bulloch</option>
                <option value="Burke">Burke</option>
                <option value="Butts">Butts</option>
                <option value="Calhoun">Calhoun</option>
                <option value="Camden">Camden</option>
                <option value="Candler">Candler</option>
                <option value="Carroll">Carroll</option>
                <option value="Catoosa">Catoosa</option>
                <option value="Charlton">Charlton</option>
                <option value="Chatham">Chatham</option>
                <option value="Chattahoochee">Chattahoochee</option>
                <option value="Chattooga">Chattooga</option>
                <option value="Cherokee">Cherokee</option>
                <option value="Clarke">Clarke</option>
                <option value="Clay">Clay</option>
                <option value="Clayton">Clayton</option>
                <option value="Clinch">Clinch</option>
                <option value="Cobb">Cobb</option>
                <option value="Coffee">Coffee</option>
                <option value="Colquitt">Colquitt</option>
                <option value="Columbia">Columbia</option>
                <option value="Cook">Cook</option>
                <option value="Coweta">Coweta</option>
                <option value="Crawford">Crawford</option>
                <option value="Crisp">Crisp</option>
                <option value="Dade">Dade</option>
                <option value="Dawson">Dawson</option>
                <option value="Decatur">Decatur</option>
                <option value="DeKalb">DeKalb</option>
                <option value="Dodge">Dodge</option>
                <option value="Dooly">Dooly</option>
                <option value="Dougherty">Dougherty</option>
                <option value="Douglas">Douglas</option>
                <option value="Early">Early</option>
                <option value="Echols">Echols</option>
                <option value="Effingham">Effingham</option>
                <option value="Elbert">Elbert</option>
                <option value="Emanuel">Emanuel</option>
                <option value="Evans">Evans</option>
                <option value="Fannin">Fannin</option>
                <option value="Fayette">Fayette</option>
                <option value="Floyd">Floyd</option>
                <option value="Forsyth">Forsyth</option>
                <option value="Franklin">Franklin</option>
                <option value="Fulton">Fulton</option>
                <option value="Gilmer">Gilmer</option>
                <option value="Glascock">Glascock</option>
                <option value="Glynn">Glynn</option>
                <option value="Gordon">Gordon</option>
                <option value="Grady">Grady</option>
                <option value="Greene">Greene</option>
                <option value="Gwinnett">Gwinnett</option>
                <option value="Habersham">Habersham</option>
                <option value="Hall">Hall</option>
                <option value="Hancock">Hancock</option>
                <option value="Haralson">Haralson</option>
                <option value="Harris">Harris</option>
                <option value="Hart">Hart</option>
                <option value="Heard">Heard</option>
                <option value="Henry">Henry</option>
                <option value="Houston">Houston</option>
                <option value="Irwin">Irwin</option>
                <option value="Jackson">Jackson</option>
                <option value="Jasper">Jasper</option>
                <option value="Jeff Davis">Jeff Davis</option>
                <option value="Jefferson">Jefferson</option>
                <option value="Jenkins">Jenkins</option>
                <option value="Johnson">Johnson</option>
                <option value="Jones">Jones</option>
                <option value="Lamar">Lamar</option>
                <option value="Lanier">Lanier</option>
                <option value="Laurens">Laurens</option>
                <option value="Lee">Lee</option>
                <option value="Liberty">Liberty</option>
                <option value="Lincoln">Lincoln</option>
                <option value="Long">Long</option>
                <option value="Lowndes">Lowndes</option>
                <option value="Lumpkin">Lumpkin</option>
                <option value="Macon">Macon</option>
                <option value="Madison">Madison</option>
                <option value="Marion">Marion</option>
                <option value="McDuffie">McDuffie</option>
                <option value="McIntosh">McIntosh</option>
                <option value="Meriwether">Meriwether</option>
                <option value="Miller">Miller</option>
                <option value="Mitchell">Mitchell</option>
                <option value="Monroe">Monroe</option>
                <option value="Montgomery">Montgomery</option>
                <option value="Morgan">Morgan</option>
                <option value="Murray">Murray</option>
                <option value="Muscogee">Muscogee</option>
                <option value="Newton">Newton</option>
                <option value="Oconee">Oconee</option>
                <option value="Oglethorpe">Oglethorpe</option>
                <option value="Paulding">Paulding</option>
                <option value="Peach">Peach</option>
                <option value="Pickens">Pickens</option>
                <option value="Pierce">Pierce</option>
                <option value="Pike">Pike</option>
                <option value="Polk">Polk</option>
                <option value="Pulaski">Pulaski</option>
                <option value="Putnam">Putnam</option>
                <option value="Quitman">Quitman</option>
                <option value="Rabun">Rabun</option>
                <option value="Randolph">Randolph</option>
                <option value="Richmond">Richmond</option>
                <option value="Rockdale">Rockdale</option>
                <option value="Schley">Schley</option>
                <option value="Screven">Screven</option>
                <option value="Seminole">Seminole</option>
                <option value="Spalding">Spalding</option>
                <option value="Stephens">Stephens</option>
                <option value="Stewart">Stewart</option>
                <option value="Sumter">Sumter</option>
                <option value="Talbot">Talbot</option>
                <option value="Taliaferro">Taliaferro</option>
                <option value="Tattnall">Tattnall</option>
                <option value="Taylor">Taylor</option>
                <option value="Telfair">Telfair</option>
                <option value="Terrell">Terrell</option>
                <option value="Thomas">Thomas</option>
                <option value="Tift">Tift</option>
                <option value="Toombs">Toombs</option>
                <option value="Towns">Towns</option>
                <option value="Treutlen">Treutlen</option>
                <option value="Troup">Troup</option>
                <option value="Turner">Turner</option>
                <option value="Twiggs">Twiggs</option>
                <option value="Union">Union</option>
                <option value="Upson">Upson</option>
                <option value="Walker">Walker</option>
                <option value="Walton">Walton</option>
                <option value="Ware">Ware</option>
                <option value="Warren">Warren</option>
                <option value="Washington">Washington</option>
                <option value="Wayne">Wayne</option>
                <option value="Webster">Webster</option>
                <option value="Wheeler">Wheeler</option>
                <option value="White">White</option>
                <option value="Whitfield">Whitfield</option>
                <option value="Wilcox">Wilcox</option>
                <option value="Wilkes">Wilkes</option>
                <option value="Wilkinson">Wilkinson</option>
                <option value="Worth">Worth</option>
              </select>
            </label>
          </div>


          <?php
	         if(filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING) && (($info['G_META'] != (NULL)) ||($info['G_PERMISSIONS'] != '0:0:0'))){
          ?>
		  <div class="center"><h2 class="custom-font-small">Additional Info For Your Group</h2></div>
          <?php
			if($info['G_META']){
			  $inputs = GROUP::meta_form($info['G_META']);
			}

				?>

                <?php

	  }
	  ?>

          <div class="row">
            <div class="medium-12 large-12 columns">

              <div class="center">
                <ul class="small-block-grid-2">
                  <li>
                    <i class="fi-torsos-all size-36"></i>
                    <br />
                    <input type="radio" name="privacy" value="0" id="public"><label for="">Make My Profile Public</label>
                    <br />
                    <span data-tooltip aria-haspopup="true" class="has-tip" title="A public profile allows other users to search for you and verify that you have an account to more easily be able to add you to groups or promote you to administrative positions.  You can always change your choice by going to My Account and clicking Preferences.">Why Public?</span>
                  </li>
                  <li>
                    <i class="fi-prohibited size-36"></i>
                    <br />
                    <input type="radio" name="privacy" value="1" id="private"><label for="">Make My Profile Private</label>
                    <br />
                    <span data-tooltip aria-haspopup="true" class="has-tip" title="Private profiles are for users who aren't interested in joining groups or participating with others. If you just want to get more fit by yourself, this is the type for you.  You can always change your choice by going to My Account and clicking Preferences.">Why Private?</span>
                  </li>
                </ul>
              </div>

              <label for="checkbox1">By creating an account you agree to our <a href="img/Walk Georgia Terms and Conditions.pdf" target="_blank">Terms and Conditions</a>, <a href="img/Walk Georgia Privacy Policy.pdf" target="_blank">Privacy Policy</a>, and are at least 13 years old.
              <br />
                 <input type="checkbox" id="checkbox1" required /> I Agree
               </label>

               <label for="checkbox2">
                 <input type="checkbox" id="newsletter-sign-up" name="newsletter-sign-up" checked /> I would like to receive official newsletters from Walk Georgia.
               </label>
               <br />
               <br />
               <input type="submit" value="Join" class="button expand tiny" onclick="return regformhash(this.form,this.form.password);"/></button>
               <br />
               <!--
               Saving this for when we actually have it runnning
               <small>Sign in with <a href="#">Facebook</a>, <a href="#">Google</a>, or <a href="#">Twitter</a>.</small>
               -->
            </div>
          </div>
      </form>
  </div>
  </div>
    <?php
}
//EDITING ENDS HERE

HTML_ELEMENT::footer();
//JAVASCRIPTS HERE
?>
<script src="../js/modernizr.js"></script>
    <script src="../js/forms.js"></script>
    <script src="../js/sha512.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>
