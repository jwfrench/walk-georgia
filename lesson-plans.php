<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
  SESSION::logout();
  REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

<div id="main">

  <!-- Global Header Container -->
  <div id="top nojava">

    <!-- Header Image Container -->
    <div id="slideshow">
      <!-- Page Title Container -->
      <div class="row nojava center pa5-ns pt3">
        <h2 class="custom-font-big-white">lesson plans</h2>
        <p class="gotham-small-white">Educational Exercises Based on Our Program</p>
      </div>
    </div>
  </div>

  <div class="cf mw7-ns center pa3 pv5-ns">
    <a href="lesson-plans-elementary.php">
      <div class="fl w-100 w-50-ns pr2-ns ">
        <div class="ba b--black-20 pv3 pv4-ns">
          <h3>Elementary Schools</h3>
          <i class="fi-book-bookmark size-60 blue"></i>
        </div>
      </div>
    </a>
    <a href="lesson-plans-middle.php">
      <div class="fl w-100 w-50-ns pl2-ns mt3 mt0-ns">
        <div class="ba b--black-20 pv3 pv4-ns">
          <h3>Middle Schools</h3>
          <i class="fi-book-bookmark size-60 font -green"></i>
        </div>
      </a>
    </div>
  </div>

      <?php

      //STOPEDITING

      HTML_ELEMENT::footer();

      //JAVASCRIPTS GO HERE
      ?>
      <script type="text/javascript">

      $(function() {

        $('#toggle4').click(function() {
          $('.toggle4').slideToggle('fast');
          return false;
        });

      });

      </script>
      <script type="text/javascript">
      function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
      </script>
      <script type="text/javascript">
      (function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
      </script>
      <script src="js/foundation/foundation.abide.js"></script>
      <script src="js/foundation/foundation.reveal.js"></script>
      <script src="js/foundation/foundation.tooltip.js"></script>
      <script src="js/foundation/foundation.offcanvas.js"></script>
      <script src="js/foundation/foundation.equalizer.js"></script>
      <script src="js/foundation/foundation.dropdown.js"></script>
      <script type="text/javascript" src="js/sha512.js"></script>
      <script type="text/javascript" src="js/log_form.js"></script>
      <script>
      $(document).foundation();
      </script>
      <!-- End Footer -->
    </body>
    </html>
