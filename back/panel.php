<?php
include_once("../lib/template_api.php");
include_once("../lib/back_api.php");
$ss = SESSION::secure_session();
if($_GET['logout']==1){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Administration', $_SESSION['valid']);
HTML_ELEMENT::top_nav();

//START EDITING HERE

?>
<div style="margin-top:20px;"></div>
    
    <!-- Main Content -->
    
      <div class="row">
        <div class="large-12 columns center">
          <h1 class="global-h1">Admin Backend</h1>
        </div>
      </div>
      
    <!-- Search Function -->  
      <div class="row">
        <div class="large-12 columns">
          <h2 class="global-h1-small">Search For Users:</h2>
        </div>
      </div>
      <div class="row">
        <div class="large-12 columns">
          <form>
            <div class="row">
              <div class="large-6 columns">
                <div class="row collapse">
                  <div class="small-10 columns">
                    <input type="text" placeholder="Search by name or email.">
                  </div>
                  <div class="small-2 columns">
                    <a href="#" class="button postfix">Search</a>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      
    <!-- End Search Function -->
    
      <div class="row">
        <div class="large-12 columns">
        
        <!-- User -->
          <div class="group-unit">
            <div class="row">
              <div class="large-8 medium-7 small-12 columns">
                <img src="../img/default-group-icon.png" class="group-icon" alt="Picture of the Group" height="56px" width="56px" style="border-radius:50%;"> 
                <h3 class="global-h3" style="padding-top:18px;">Sample User</h3>
              </div>
              <div class="large-4 medium-5 small-12 columns right" style="padding-top:15px; padding-left:20%; text-align:left;">
                <a href="#" class="button tiny" data-dropdown="drop1">Admin Options</a>
          
                <ul id="drop1" class="f-dropdown" style="text-align:left;" data-dropdown-content>
                  <li><a href="#">Edit User Groups</a></li>
                  <li><a href="#">Reset Password</a></li>
                  <li><a href="#" class="alert">Delete User</a></li>
                </ul>
              </div>
            </div>
          </div>
        <!-- End User -->
        
        <br />
        <br />
        <br />
        <br />
        
        <!-- When you click 'Edit Groups' -->
        
          <div class="row">
            <div class="large-12 columns">
              <h2 class="global-h1-small">Sample User's Groups</h2>
              <p class="global-p">Click on the "Edit Group" button to reorganize this user within a given group.  To add the user to a new group, click the "Add to a New Group" button below.</p>
            </div>
          </div>
          
          <div class="group-unit">
            <div class="row">
              <div class="large-8 medium-7 small-12 columns">
                <img src="../img/default-group-icon.png" class="group-icon" alt="Picture of the Group" height="56px" width="56px" style="border-radius:50%;"> 
                <h3 class="global-h3" style="padding-top:18px;">Sample Group Group</h3>
              </div>
              <div class="large-4 medium-5 small-12 columns" align="right" style="padding-top:15px; padding-left:5%;">
                <a href="http://dev.walkgeorgia.com/group/groups.php?group=227" class="button tiny">Edit Group</a> 
                <a href="#" class="button tiny alert" data-reveal-id="group-status_227">Remove User</a>
              </div>
            </div>
          </div>
          
          <div class="row" style="margin-top:40px;">
            <div class="large-12 columns">
              <a href="#" class="button tiny">Add to a New Group</a>
            </div>
          </div>
          
      <!-- Group Search Function -->  
          <div class="row">
            <div class="large-12 columns">
              <h2 class="global-h1-small">Search For Groups:</h2> 
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <form>
                <div class="row">
                  <div class="large-6 columns">
                    <div class="row collapse">
                      <div class="small-10 columns">
                        <input type="text" placeholder="Search by Group Title">
                      </div>
                      <div class="small-2 columns">
                        <a href="#" class="button postfix">Search</a>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
          
          <div class="row">
            <div class="large-12 columns">
              <h2 class="global-h1-small">Add Group by ID:</h2> 
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <form>
                <div class="row">
                  <div class="large-6 columns">
                    <div class="row collapse">
                      <div class="small-10 columns">
                        <input type="text" placeholder="Enter Group ID here to add user">
                      </div>
                      <div class="small-2 columns">
                        <a href="#" class="button postfix">Submit</a>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
          
          <div class="row">
            <div class="large-12 columns">
              <h2 class="global-h1-small">Results:</h2>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              
              <div class="group-unit">
                <div class="row">
                  <div class="large-8 medium-7 small-12 columns">
                    <img src="../img/default-group-icon.png" class="group-icon" alt="Picture of the Group" height="56px" width="56px" style="border-radius:50%;"> 
                    <h3 class="global-h3" style="padding-top:18px;">Sample Group Group</h3>
                  </div>
                  <div class="large-4 medium-5 small-12 columns" align="right" style="padding-top:15px; padding-left:5%;">
                    <a href="http://dev.walkgeorgia.com/group/groups.php?group=227" class="button tiny">Edit Group</a> 
                    <a href="#" class="button tiny alert" data-reveal-id="group-status_227">Remove User</a>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
      
        <!-- End Group Search Function -->          
        
        <!-- When you click 'Edit Groups' -->
          
          
        </div>
      </div>
    <!-- User List -->
    
    
    
    <!-- End User List -->
    
    <!-- End Main Content -->
<?php
 $accounts = ACCOUNT::displayRegistered();
//END EDITING HERE

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../js/foundation/foundation.js"></script>
    <script src="../js/foundation/foundation.topbar.js"></script>
    <script src="../js/foundation/foundation.reveal.js"></script>
    <script src="../js/foundation/foundation.tooltip.js"></script>
    <script src="../js/foundation/foundation.offcanvas.js"></script>
    <script src="../js/foundation/foundation.equalizer.js"></script>
    <script src="../js/foundation/foundation.dropdown.js"></script>
    <script src="../js/foundation/foundation.alert.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>