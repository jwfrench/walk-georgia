<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once("$root/lib/template_api.php");
include_once ("$root/lib/back_api.php");
$ss = SESSION::secure_session();
if(filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) != null){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

<!-- Begin Main Section -->
<div id="main">
<!-- Begin Main Section -->
  <div class="blue-bg" style="margin-top:0px; padding:10px; padding-bottom:0px;">
      <a href="index.php" class="button round success tiny" style="margin-top:10px;">Back to My Activity</a>
    </div>
  <div class="row">
    <div class="large-12 columns">
      <?php
    	$list = ACTIVITY::display_activity_table($_SESSION['ID']);
	  ?>
    </div>
  </div>
  <!-- End Past Activity List -->

   
    

<!-- End Main Section -->            
</div>
<div class="footer">
      <div class="row">
        <div>
          <a class="tiny button alert" href="http://<?php echo filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/contact-us.php"> Report a Bug </a>
        </div>
        
        <p class="global-p">
          <a href="http://www.facebook.com/walkgeorgia">Facebook</a> | <a href="https://twitter.com/walkga">Twitter</a> | <a href="http://blog.extension.uga.edu/walkgeorgia">Walk Georgia Blog</a>
        </p>
        <br />
          <small>The College of Agricultural and Environmental Sciences and the College of Family and Consumer Sciences cooperating.
The University of Georgia &copy; 2014. All Rights Reserved.</small>
          <hr><img alt="The University of Georgia Cooperative Extension" style="width:87px; height:31px;" src="../img/ext.png"><br><br>
        </div>
      </div>
    </div><script type="text/javascript" src="../js/dataTables.foundation.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="../js/foundation.min.js"></script>
<script>
     $(document).foundation();
</script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/plug-ins/380cb78f450/integration/foundation/dataTables.foundation.js"></script>

<script>
$(document).ready(function() {
    	  $('table.display').DataTable({
			"oLanguage": {
              "sEmptyTable": " "
            },
	        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
			responsive:true
		  });
		} );
</script>
</body>
</html>