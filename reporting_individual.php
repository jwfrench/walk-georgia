<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/back_api.php");
include_once ("$root/lib/groups_api.php");
$ss = SESSION::secure_session();
$current_user_info = ACCOUNT::get_info($_SESSION['ID']);
$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
$info = ACCOUNT::get_info($id);
$permission =0;
if($current_user_info['IS_COUNTY_ADMIN']){
	$permission += 1;
}
if($id == $_SESSION['ID']){
	$permission += 1;
}
if(filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING) != null){
	if(GROUP::isAdmin(filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING))){
		$permission += 1;
	}
}
if($permission < 1 ){
	$redirect =REDIRECT::home('Access to reports is limited to County Administrators, Group Admins, and the respective individuals');
}
$date_range = '';
$day1 = '';
$day2 = '';
$month1 = '';
$month2 = '';
$year1 = '';
$year2 = '';
//GET THE DATE INFO
if(filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING) != null) {
  if(filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING) == null) {
    header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting_individual.php?id='. filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING).'&group='. filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).'&datepicker='.filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING).'&datepickerEnd='.urlencode(date("m/d/Y")).'');
  }
}
if(filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING) != null) {
  if(filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING) == null) {
    header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting_individual.php?id='. filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING).'&group='. filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).'&datepicker=01/01/'.date("Y").'&datepickerEnd='.filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING).'');
  }
}
if(filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING) != null) {
  if(filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING) == null) {
    header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting_individual.php?id='. filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING).'&group='. filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).'&datepickerMobile='.filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING).'&datepickerEndMobile='.urlencode(date("m/d/Y")));
  }
}
if(filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING) != null) {
  if(filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING) == null) {
    header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting_individual.php?id='. filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING).'&group='. filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).'&datepickerMobile=01/01/'.date("Y").'&datepickerEndMobile='.filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING));
  }
}
if ((filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING) != null) && (filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING) != null)) {
    //day 1
    $date1 = explode('/', filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING));
    if(count($date1) != 3) {
      header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting_individual.php?id='. filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING).'&group='. filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).'&datepicker=01/01/'.date("Y").'&datepickerEnd='.urlencode(date("m/d/Y")));
    }
    $month1 = $date1[0];
    $day1 = $date1[1];
    $year1 = $date1[2];

    //day 2
    $date2 = explode('/', filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING));
    if(count($date2) != 3) {
      header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting_individual.php?id='. filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING).'&group='. filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).'&datepicker=01/01/'.date("Y").'&datepickerEnd='.urlencode(date("m/d/Y")));
    }
    $month2 = $date2[0];
    $day2 = $date2[1];
    $year2 = $date2[2];
    $date1= $year1.'-'.$month1.'-'.$day1;
    $date2= $year2.'-'.$month2.'-'.$day2;

    $date_range = 'AL_DATE >= \'' . $date1 . '\' AND AL_DATE <= \'' . $date2 . '\' AND';
}
if ((filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING) != null) && (filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING) != null)) {
    //day 1
    $date1 = explode('-', filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING));
    if(count($date1) != 3) {
      header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting_individual.php?id='. filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING).'&group='. filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).'&datepickerMobile=01/01/'.date("Y").'&datepickerEndMobile='.urlencode(date("m/d/Y")));
    }
    $month1 = $date1[1];
    $day1 = $date1[2];
    $year1 = $date1[0];

    //day 2
    $date2 = explode('-', filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING));
    if(count($date2) != 3) {
      header('Location: http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING) .'/reporting_individual.php?id='. filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING).'&group='. filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING).'&datepickerMobile=01/01/'.date("Y").'&datepickerEndMobile='.urlencode(date("m/d/Y")));
    }
    $month2 = $date2[1];
    $day2 = $date2[2];
    $year2 = $date2[0];
    $date1= $year1.'-'.$month1.'-'.$day1;
    $date2= $year2.'-'.$month2.'-'.$day2;

    $date_range = 'AL_DATE >= \'' . $date1 . '\' AND AL_DATE <= \'' . $date2 . '\' AND';
}

$breakdown = ACTIVITY::get_breakdown($id);
$past_activity = MSSQL::query('SELECT * FROM LOG WHERE AL_UID=\''.$id.'\'');
$group = MSSQL::query('SELECT * FROM GROUPS WHERE G_ID IN (SELECT G_ID FROM GROUP_MEMBER WHERE U_ID=\''.$id.'\') ORDER BY G_ID;');
$sql2 = 'SELECT AL_AID, SUM(AL_PA) AS \'AL_PA\', SUM(AL_TIME) AS \'AL_TIME\', SUM(AL_UNIT) AS \'AL_UNIT\', COUNT(AL_AID) AS COUNT FROM LOG WHERE ' . $date_range . ' AL_UID =\''.$id.'\' GROUP BY AL_AID ORDER BY AL_PA DESC;';
$activity = MSSQL::query($sql2);
$no_of_activities = odbc_num_rows($activity);
if(isset($breakdown['POINTS']) && !is_string($breakdown['POINTS'])){
  $points = array_sum($breakdown['POINTS']);
}else{
  $points = 0;
}

if(isset($breakdown['TOTALS']['DISTANCE']) && !is_string($breakdown['TOTALS']['DISTANCE'])){
  $distance = array_sum($breakdown['TOTALS']['DISTANCE']);
}else{
  $distance = 0;
}
if(isset($breakdown['TOTALS']['TIME']) && !is_string($breakdown['TOTALS']['TIME'])){
    $time = array_sum($breakdown['TOTALS']['TIME']);
}else{
  $time = 0;
}
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Walk Georgia | Reporting</title>
    <link rel="stylesheet" href="../../css/foundation.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.1/b-flash-1.2.1/b-html5-1.2.1/b-print-1.2.1/r-2.1.0/datatables.min.css"/>
		<link type="text/css" media="screen" rel="stylesheet" href="css/responsive-tables.css" />
    <script src="js/vendor/modernizr.js"></script>
    <style media="screen">
        .dt-buttons{
            margin-left: 2em;
            margin-top: 1.3em;
        }
    </style>
  </head>
  <body>

  <div id="main">

    <!-- Header -->
      <div class="row" style="margin-bottom:20px;">
        <div class="large-12 columns center">
          <img src="img/single-color-logo.png" alt="logo" />
          <img src="img/ext.png" alt="UGA extension logo" />
          <br />
          <br />
          <h1 class="custom-font-small font -blue">Official Individual Report</h1>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <h2 class="custom-font-small font -black"><?php echo $info['FNAME']." ".$info['LNAME'] ?></h2>
          <!-- <a href="#" class="button tiny">Printer Friendly Version</a> -->
        </div>
      </div>
    <!-- End Header -->

    <!-- Report Body -->

      <div class="row">

      <!-- Avatar -->
        <div class="large-3 columns">
          <?php
		    $avatar = ACCOUNT::avatar_medium($id);
		  ?>
        </div>
      <!-- End Avatar -->

      <!-- Personal Info -->
        <div class="large-4 columns">
          <h2 class="global-h2">Personal Information:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />

          <ul class="global-p" style="list-style:none; line-height:2">
            <li><b>Name:</b> <?php echo $info['FNAME']." ".$info['LNAME'];?></li>
            <li><b>Email:</b> <?php echo $info['EMAIL'] ;?></li>
            <li><b>County:</b> <?php echo $info['COUNTY'] ;?></li>
           <li><b>Member Since:</b> <?php echo $info['DATE_JOINED']; ?></li>
          </ul>

        </div>
      <!-- End Personal Info -->

      <!-- Overall Stats -->
        <div class="large-5 columns">
          <h2 class="global-h2">Overall Stats:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />

          <ul class="global-p" style="list-style:none; line-height:2">
            <li><b>Total Points Earned: </b><?= $points ?></li>
            <li><b>Total Time Exercised: </b><?php echo floor($time/3600).' Hours '.floor(($time%3600)/60) .' Minutes'; ?></li>
            <li><b>Total Miles From Distance Exercises <span data-tooltip aria-haspopup="true" class="has-tip" title="The total miles of all user's logged distance-based activites, such as running, biking, hiking, etc.">(?)</span>:</b>
			<?php
				if(!is_string($breakdown['TOTALS']['DISTANCE'])){echo $distance;}else{echo '0';}?></li>

          </ul>
        </div>
      <!-- End Overal Stats -->

      </div>
      <form>
                        <!-- MOBILE Date Picker -->
                        <div class="row show-for-small-only">
                            <div class="small-12 columns">
                                <p class="mb font -secondary -bold">
                                    Select Start Date
                                </p>
                                <input type="date" id="datepickerMobile" name="datepickerMobile"<?php if((filter_input(INPUT_GET, 'datepickerMobile', FILTER_SANITIZE_STRING) != null) && (!empty($month1)) && (!empty($day1)) && (!empty($year1))) { echo (' value="' . $month1. '/' .$day1. '/' . $year1. '"'); } ?>>
                                <p class="mb font -secondary -bold">
                                    Select End Date
                                </p>
                                <input type="date" id="datepickerEndMobile" name="datepickerEndMobile"<?php if((filter_input(INPUT_GET, 'datepickerEndMobile', FILTER_SANITIZE_STRING) != null) && (!empty($month2)) && (!empty($day2)) && (!empty($year2))) { echo (' value="' . $month2. '/' .$day2. '/' . $year2. '"'); } else { echo ('value="'.date('m/d/Y').'"'); } ?>>
                            </div>
                        </div>
                        <!-- End MOBILE Date Picker -->
                        <!-- Medium Up Date Picker -->
                        <div class="row collapse show-for-medium-up pt1">
                            <div class="medium-2 medium-offset-2 columns">
                                <a href="#" id="startDate" class="button postfix font -primary">Select Start Date</a>
                            </div>
                            <div class="medium-2 columns">
                                <input type="text" id="datepicker" name="datepicker" class="font -standard -primary -bold" <?php if((filter_input(INPUT_GET, 'datepicker', FILTER_SANITIZE_STRING) != null) && (!empty($month1)) && (!empty($day1)) && (!empty($year1))) { echo (' value="' . $month1. '/' .$day1. '/' . $year1. '"'); } ?>>
                            </div>
                            <div class="medium-2 columns">
                                <a href="#" id="endDate" class="button postfix font -primary">Select End Date</a>
                            </div>
                            <div class="medium-2 columns end">
                                <input type="text" id="datepickerEnd" name="datepickerEnd" class="font -primary -standard -bold" <?php if((filter_input(INPUT_GET, 'datepickerEnd', FILTER_SANITIZE_STRING) != null) && (!empty($month2)) && (!empty($day2)) && (!empty($year2))) { echo (' value="' . $month2. '/' .$day2. '/' . $year2. '"'); } else { echo ('value="'.date('m/d/Y').'"'); } ?>>
                            </div>
                            <!-- End Medium Up Date Picker -->
                        </div>
                        <div class="tc pb1">
                            <a href="<?php echo 'http://' . filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING); ?>/reporting_individual.php?id=<?= $id ?>">RESET </a>
                            <!-- End Date Range -->
                        </div>
                        <div class="tc">
                            <input type="hidden" id="id" name="id" value="<?= filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING); ?>">
                            <input type="submit" name="submit" class="button success font -primary" value ="Generate Report!">
                            <!-- End Date Range -->
                        </div>
                    </form>
      <div class="row" style="margin-top:20px;">

      <!-- Past Activity -->
        <div class="large-12 columns">
          <h2 class="global-h2">Past Activity:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <table id='' class='responsive display'>
            <thead>
              	<th>
                  Activity
                </th>
                <th>
                  Points
                </th>
                <th>
                  Time(Hours)
                </th>
                <th>
                  Distance (Miles)
                </th>
                <th>
                  Date
                </th>
            </thead>
          <?php while(odbc_fetch_row($past_activity)){?>
            <tr>
              <td>
                <?php echo ACTIVITY::activity_to_form(odbc_result($past_activity, 'AL_AID')); ?>
              </td>
              <td>
                <?php echo odbc_result($past_activity, 'AL_PA'); ?>
              </td>
              <td>
                <?php echo number_format(odbc_result($past_activity, 'AL_TIME')/3600, 2, '.', ''); ?>
              </td>
              <td>
			  <?php
			    if(ACTIVITY::is_distance_based(odbc_result($past_activity, 'AL_AID'))){
				  echo number_format(odbc_result($past_activity, 'AL_UNIT'));
				}else{
				  echo 0;
				}?></td>
                <td>
                <?php echo odbc_result($past_activity, 'AL_DATE'); ?>
              </td>
            </tr>
          <?php }?>
          </table>
        </div>
        <br>

      <!-- End Past Activity -->

      <!-- User's Groups -->
        <div class="large-12 columns">
          <h2 class="global-h2">User's Groups:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <table id='' class='responsive display'>
            <thead>
              	<th>
                  Group
                </th>
                <th>
                  Members
                </th>
                <th>
                  Points
                </th>
                <th>
                  Time (Hours)
                </th>
                <th>
                  Distance (Miles)
                </th>
            </thead>
          <?php
		    while(odbc_fetch_row($group)){
		      $group_info = GROUP::getBreakdown(odbc_result($group, 'G_ID'));
			  $g_members = odbc_result(MSSQL::query('SELECT COUNT(DISTINCT U_ID) AS COUNT FROM GROUP_MEMBER WHERE G_ID =  \''.odbc_result($group, 'G_ID').'\''), 'COUNT');
		  ?>
            <tr>
              <td>
                <a href="<?php echo 'http://'.filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);?>/reporting.php?group=<?php echo odbc_result($group, 'G_ID'); ?>"><?php echo odbc_result($group, 'G_NAME'); ?></a>
              </td>
              <td>
                <?php echo $g_members; ?>
              </td>
              <td><?php if(isset($group_info['POINTS'])){echo array_sum($group_info['POINTS']);}else{echo '0';} ?></td>
              <td><?php if(isset($group_info['TIME'])){echo number_format(array_sum($group_info['TIME'])/3600, 2, '.', '');}else{echo '0';}?></td>
              <td><?php if(isset($group_info['DISTANCE'])){echo array_sum($group_info['DISTANCE']);}else{echo '0';} ?></td>
            </tr>
          <?php }?>
          </table>

        </div>
      <!-- End Subgroups -->

      </div>

      <div class="row">

      <!-- Activity Breakdown -->
        <div class="large-12 columns">
          <h2 class="global-h2">Activity Breakdown:</h2>
          <hr style="margin-top:-5px; margin-bottom:5px;" />
          <table id="" class="responsive display">
            <thead>
              	<th>
                  Activity
                </th>
                <th>
                  Points
                </th>
                <th>
                  Time(Hours)
                </th>
                <th>
                  Times Logged
                </th>
                <th>
                  Distance (Miles)
                </th>
              </thead>
          <?php
      	  for($i =1; $i <= $no_of_activities ; $i++){
		    odbc_fetch_row($activity, $i);
	      ?>
      	    <tr>
              <td><?php echo ACTIVITY::activity_to_form(odbc_result($activity, 'AL_AID'), 2, '.', '');?></td>
              <td><?php echo odbc_result($activity, 'AL_PA'); ?></td>
              <td><?php echo number_format(odbc_result($activity, 'AL_TIME')/3600);?></td>
              <td><?php echo odbc_result($activity, 'COUNT'); ?></td>
              <td>
			  <?php
			    if(ACTIVITY::is_distance_based(odbc_result($activity, 'AL_AID'))){
				  echo number_format(odbc_result($activity, 'AL_UNIT'));
				}else{
				  echo number_format((((odbc_result($activity, 'AL_PA')*100)-300)/3.3)/3660, 2, '.', '');
				}?></td>
            </tr>
	  <?php
	  }
	  ?>
          </table>
        </div>
      <!-- End Activity Breakdown -->

      </div>
    <!-- End Report Body -->




    <!-- End Main Content -->
     </div>
  </div>

  <script src="https://code.jquery.com/jquery-1.12.1.min.js"></script>
  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
  <script src="js/foundation.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.1/b-flash-1.2.1/b-html5-1.2.1/b-print-1.2.1/r-2.1.0/datatables.min.js"></script>


  <script type="text/javascript" src="js/responsive-tables.js"></script>
  <script>
  $(document).foundation();

  //DATEPICKER
  $(function () {
		$("#datepicker").datepicker({maxDate: new Date("<?php echo(date('m-d-Y')); ?>")});
      $("#startDate").click(function () {
          $("#datepicker").datepicker("show");
      });
			$("#datepickerEnd").datepicker({maxDate: new Date("<?php echo(date('m-d-Y')); ?>")});
      $("#endDate").click(function () {
          $("#datepickerEnd").datepicker("show");
      });
  });

  $(document).ready(function () {
      $('table.display').DataTable({
          "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
          responsive: true,
          // stateSave: true,
          // "dom": 'T<"clear">lfrtip',
          dom: 'lBfrtip',
          buttons: [
              'copy', 'excel', 'pdf'
          ]
      });
  });
  </script>
  </body>
</html>
