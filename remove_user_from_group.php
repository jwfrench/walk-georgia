<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/groups_api.php");
$join_group = GROUP::leave_group(filter_input(INPUT_GET, 'gid', FILTER_SANITIZE_STRING), filter_input(INPUT_GET, 'uid', FILTER_SANITIZE_STRING));
header('Location:http://'.$_SERVER['HTTP_HOST'].'/edit_user_groups.php?id='.(filter_input(INPUT_GET, 'uid', FILTER_SANITIZE_STRING)));
?>
