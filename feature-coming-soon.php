<?php
include_once("lib/template_api.php");
include_once ("lib/back_api.php");
$ss = SESSION::secure_session();
if($_GET['logout']==1){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();

//EDIT HERE

?>

  <div id="main">
    <div id="top">

    <!-- Header -->
    <div class="row">
      <div class="large-12 columns center">
        <a href="index.php"><img src="img/single-color-logo.png" alt="Move More, Live More"></a>
      </div>
    </div>
     
    <!-- End Header -->

    <!-- Sign up and Log in Buttons -->
    <div class="row">
      <div class="large-12 columns">
        <hr />
      </div>
    </div>
    <div class="row center" style="margin-bottom:20px;">
      <div class="large-6 columns">
        <a href="register-down.php" class="button expand">Create an Account</a>
      </div>    
      <div class="large-6 columns">
        <a href="login.php" class="button expand">Log In</a>
      </div>
    </div>
    <!-- End Sign up and Log in Buttons -->
    </div>
    
    <!-- Main Content -->
    <div class="row">
      <div class="large-12 columns center">
        <h1 class="custom-font-big hide-for-small">Whoops!</h1>
        <h1 class="custom-font-small show-for-small-only">Whoops!</h1>
      </div>
    </div>
    <div class="row">
      <div class="large-12 columns center">
        <h2 class="custom-font-small">This feature is still under development.</h2>
      </div>
    </div>
    <div class="row center">
      <div class="large-12 columns">
        <p class="global-p">We're hard at work on this site, and since we've completely rebuilt it from the ground up, there are still some things we are tweaking.  Check back soon for this feature to be up and running.</p>
      </div>
    </div>
    <!-- End Main Content-->        
  </div>

<?php

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../js/modernizr.js"></script>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script src="js/foundation/foundation.js"></script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script src="js/foundation/foundation.topbar.js"></script>
    <script src="js/foundation/foundation.abide.js"></script>
    <script src="js/foundation/foundation.reveal.js"></script>
    <script src="js/foundation/foundation.tooltip.js"></script>
    <script src="js/foundation/foundation.offcanvas.js"></script>
    <script src="js/foundation/foundation.equalizer.js"></script>
    <script src="js/foundation/foundation.dropdown.js"></script>
    <script type="text/javascript" src="js/sha512.js"></script>
    <script type="text/javascript" src="js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>