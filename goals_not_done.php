<?php
include_once("lib/template_api.php");
include_once("lib/back_api.php");
$ss = SESSION::secure_session();
if($_GET['logout']==1){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia', $_SESSION['valid']);
HTML_ELEMENT::top_nav();

//START EDITING HERE
?>
<!-- Beginning of Main Section -->
<div class="main">
<!-- End of the Beginning of Main Section -->

<!-- Page Header -->
<div class="row" style="margin-top:10px; padding:10px;">
  <div class="large-12" columns>
    <h1 class="global-h1"><b>MY GOALS:</b></h1>
    <hr style="margin-top:10px;" />
  </div>
</div>
<!-- End Page Header -->
<div class="row">
  <div class="large-7 medium-7 small-12 columns">
  
    <div class="current-goal-unit">
      <h2 class="global-h2"><b>Run 3 Miles:</b></h2>
      <div class="progress">
        <span class="meter" style="width:23%"></span>
      </div>
      <a class="goal-progress">23% Complete</a>
      <a class="leave-goal">Delete Goal</a>
    </div>
    
    <div class="current-goal-unit">
      <h2 class="global-h2"><b>Run 3 Miles:</b></h2>
      <div class="progress">
        <span class="meter" style="width:23%"></span>
      </div>
      <a class="goal-progress">23% Complete</a>
      <a class="leave-goal">Delete Goal</a>
    </div>
    
    <hr />
   
    <!-- Create a Goal Section -->
    <a href="#" class="button" data-reveal-id="start-menu">Create a Goal</a>
    <!-- End Create a Goal Section -->
    
    <!-- Create a Goal Modals -->
    
      <!-- Start Menu Modal -->
      <div id="start-menu" class="reveal-modal" data-reveal>
        <div class="row">
          <div class="large-12 columns">
            <h2 class="global-h2">Create a Goal</h2>
            <hr />
          </div>
        </div>
        <div class="row">
          <div class="large-12 columns">
            <h3 class="global-h3">Time, Distance, or Reps:</h3>
          </div>
        </div>
        
        <div class="row">
          <div class="large-9 columns">
            <p class="global-p">Exercise goals can fall into many differnt categories.  We've grouped them into three main types to help you set your goals: Time, Distance, and Reps.  Click on the icon below of the type of goal you'd like to set.  You can have a goal that works with up to two out of the three categories.  For example, you can set a goal to run 24 miles in 5 hours, using the distance and the time categories.  Click an icon to try it out.</p>
          </div>
          <div class="large-3 columns">
             <ul class="small-block-grid-3">
              <li><a href="#" data-reveal-id="time-based-goal"><img src="../img/time-icon.png" alt=""></a></li>
              <li><img src="../img/distance-icon.png" alt=""></li>
              <li><img src="../img/reps-icon.png" alt=""></li>
            </ul>
          </div>
        </div>
      <a class="close-reveal-modal">&#215;</a>
      </div>
      <!-- End Start Menu Modal -->
    
      <!-- Time-Based Goal Modal --> 
        <div id="time-based-goal" class="reveal-modal" data-reveal>
          <div class="row">
            <div class="large-12 columns">
              <h2 class="global-h2">Create a Time-Based Goal</h2> 
            <hr />
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <form>
                <label>Type of Time-Based Goal:
                  <select>
                    <option value="option-1">Set a goal of total hours you'd like to spend exercising.  Any time spent doing any activity will count towards this goal.</option>
                    <option value="option-2">Set a goal of total hours spent doing one specific activity.  Ex: Run for 24 hours.</option>
                  </select>
                </label>
                
                <button class="small">Next</button>
              </form>
            </div>
          </div>
              
        
        <!-- If they choose total time spent exercising with any activity (Option 1) -->
          <form>
            <div class="row">
              <div class="large-4 columns">
                <h3 class="goal-title">Total Time Goal:</h3>
                <br />
                <br />
                <div class="row collapse">
                <label>Enter the total number of hours for your goal:</label>
                  <div class="small-9 columns">
                    <input type="number" placeholder="" />
                  </div>
                  <div class="small-3 columns">
                    <span class="postfix">hours</span>
                  </div>
                </div>
                
                <button class="small secondary">Back</button> <button class="small">Next</button>
                
              </div>
            </div>
          </form>
          <!-- End Option 1 -->
          
          <!-- If they choose total time spent exercising with a specific activity (Option 2) -->
            <!-- Option 2, Part 1 -->
            <form>
              <div class="row">
                <div class="large-12 columns">
                <h3 class="goal-title">Total Spent Doing Specific Activity:</h3>
                <br />
                <br />
                  <label>Select Activity:
                    <input type="text" placeholder="Ex: Walking" />
                  </label>
                  
                  <button class="small">Next</button>
                  
                </div>
              </div>
            </form>
            <!-- End Option 2, Part 1 -->
            <!-- Option 2, Part 2 -->
            <form>
            <div class="row">
              <div class="large-4 columns">
                <div class="row collapse">
                <label>Enter the total number of hours for your goal:</label>
                  <div class="small-9 columns">
                    <input type="number" placeholder="" />
                  </div>
                  <div class="small-3 columns">
                    <span class="postfix">hours</span>
                  </div>
                </div>
                
                <button class="small secondary">Back</button> <button class="small">Next</button>
                
              </div>
            </div>
          </form>
          <!-- End Option 2, Part 2 -->
          
        <a class="close-reveal-modal">&#215;</a>
        </div>
          
      <!-- End Time-Based Goal Modal -->
      
      <!-- Distance-Based Goal Modal -->
      
        <div id="distance-based-goal" class="reveal-modal" data-reveal>
          <div class="row">
            <div class="large-12 columns">
              <h2 class="global-h2">Create a Distance-Based Goal</h2> 
              <hr />
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns"> 
              <form>
            
                <label>Type of Distance-Based Goal:
                  <select>
                    <option value="distance-option-1">Set a goal of total distance traveled doing any activity.</option>
                    <option value="distance-option-2">Set a goal of total distance traceled doing one specific activity.  Ex: Walk 500 miles.</option>
                  </select>
                </label>
                
                <button class="small">Next</button>
                
              
          </form>
          </div>
         </div>
          
        <!-- Distance-Based-Option-1 -->
          <form>
            <div class="row">
              <div class="large-5 columns">
                <h3 class="goal-title">Total Distance Goal:</h3>
                <br />
                <br />
                <div class="row collapse">
                <label>Enter the total number of miles for your goal:</label>
                  <div class="small-9 columns">
                    <input type="number" placeholder="" />
                  </div>
                  <div class="small-3 columns">
                    <span class="postfix">miles</span>
                  </div>
                </div>
                
                <button class="small secondary">Back</button> <button class="small">Next</button>
                
              </div>
            </div>
          </form>
          <!-- End Distance-Based-Option-1 -->
          
          <!-- Distance-Based-Option-2 -->
            <!-- Option 2, Part 1 -->
            <form>
              <div class="row">
                <div class="large-12 columns">
                <h3 class="goal-title">Total Distance of One Activty:</h3>
                  <label>Select Activity:
                    <input type="text" placeholder="Ex: Walking" />
                  </label>
                  
                  <button class="small">Next</button>
                  
                </div>
              </div>
            </form>
            <!-- End Distance-Based-Option-2, Part 1 -->
            <!-- Distance-Based-Option-2, Part 2 -->
            <form>
            <div class="row">
              <div class="large-5 columns">
                <div class="row collapse">
                <label>Enter the total number of miles for your goal:</label>
                  <div class="small-9 columns">
                    <input type="number" placeholder="" />
                  </div>
                  <div class="small-3 columns">
                    <span class="postfix">miles</span>
                  </div>
                </div>
                
                <button class="small secondary">Back</button> <button class="small">Next</button>
                
              </div>
            </div>
          </form>
          <!-- End Distance-Based-Option-2, Part 2 -->
          
        <a class="close-reveal-modal">&#215;</a>
        </div>
      
      <!-- End Distance-Based Goal Modal -->
      
      <!-- Reps-Based Goal Modal -->
      
        <div id="reps-based-goal" class="reveal-modal" data-reveal>
          <div class="row">
            <div class="large-12 columns">
              <h2 class="global-h2">Create a Reps-Based Goal</h2> 
              <hr />
            </div>
          </div>      
          <!-- Reps-Based Part 1 -->
          <form>
              <div class="row">
                <div class="large-12 columns">
                <h3 class="goal-title">Select Activity for Goal:</h3>
                  <label>Select Activity:
                    <input type="text" placeholder="Ex: Push-ups" />
                  </label>
                  
                  <button class="small">Next</button>
                  
                </div>
              </div>
            </form>
            <!-- End Reps-Based Part 1 -->
            <!-- Reps-Based Part 2 -->
            <form>
            <div class="row">
              <div class="large-5 columns">
                <div class="row collapse">
                <label>Enter the total number of reps for your goal:</label>
                  <div class="small-9 columns">
                    <input type="number" placeholder="Ex: 1000" />
                  </div>
                  <div class="small-3 columns">
                    <span class="postfix">reps</span>
                  </div>
                </div>
                <small>Tip: Enter a high number of repetitions that will take you a difficult period of time to attain.  Setting you goal with a high number of reps that requires many exercise sessions will encourage you to stay active more frequently.</small>
                <br />
                <br />
                <button class="small secondary">Back</button> <button class="small">Next</button>
                
              </div>
            </div>
          </form>
          <!-- End Reps-Based Part 2 -->
          
        <a class="close-reveal-modal">&#215;</a>
        </div>
      
      <!-- End Reps-Based Goal Modal -->
    
    <!-- End Create a Goal Modals -->
    
  </div>
  
  <div class="large-5 medium-5 columns goals-container">
    <h3 class="global-h3">Suggested Goals:</h3>
    <hr style="margin-top:5px;" />
    
      <div class="suggested-goal-unit">
        <div class="suggested-goal-title">Exercise for 5 Hours</div>
        <a class="suggested-goal-begin">Start Goal</a>
      </div>
      
      <div class="suggested-goal-unit">
        <div class="suggested-goal-title">Walk 50 Miles</div>
        <a class="suggested-goal-begin">Start Goal</a>
      </div>
      
      <div class="suggested-goal-unit">
        <div class="suggested-goal-title">Do 500 Push Ups</div>
        <a class="suggested-goal-begin">Start Goal</a>
      </div>
      
    <hr />  
  </div>
</div>

<!-- End of the Main Section -->
</div>
<!-- End of the Main Section -->
<?php

//END EDITING HERE

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script type="text/javascript">
        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../js/foundation/foundation.js"></script>
    <script src="../js/foundation/foundation.topbar.js"></script>
    <script src="../js/foundation/foundation.reveal.js"></script>
    <script src="../js/foundation/foundation.tooltip.js"></script>
    <script src="../js/foundation/foundation.offcanvas.js"></script>
    <script src="../js/foundation/foundation.equalizer.js"></script>
    <script src="../js/foundation/foundation.dropdown.js"></script>
    <script src="../js/foundation/foundation.alert.js"></script>
	<script src="../js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>