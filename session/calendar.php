<?php
include_once("../lib/template_api.php");
include_once("../lib/back_api.php");
include_once("../lib/sessions_api.php");
$ss = SESSION::secure_session();
if($_GET['logout']==1){
	SESSION::logout();
	REDIRECT::home();
}
if(isset($_POST['file'])){
	$img = SESSIONS::avatar_upload($_GET['sessions']);
}
HTML_ELEMENT::head('Sessions');
HTML_ELEMENT::top_nav();
//EDIT HERE
//If the sessions exists
if($_GET['join']){
	$join_sessions = SESSIONS::join_sessions($_GET['sessions'], $_SESSION['ID']);
}
if($_GET['leave']){
	$leave_sessions = SESSIONS::leave_sessions($_GET['sessions']);
}
if($_GET['sessions']){
  $info = SESSIONS::get_info($_GET['sessions']);
?> 
<div class="main">

  <div class="row">
    <!-- Desktop Sidebar -->
    <div class="large-5 medium-5 small-12 columns">
      <div class="hide-for-small" style="margin-bottom:20px;">
        <div class="sidebar">
          <div align="center">
            <?php
          if (file_exists($_SERVER['DOCUMENT_ROOT']."/sessions/img/avatar/".$_GET['session'].".png")) {
			  $filename = "/sessions/img/avatar/".$_GET['sessions'].".png";
		  } else {
    			$filename = "../img/default-session-profile.png";
		  }
		  ?>
            <!-- Avatar -->
            <a href="#" data-reveal-id="avatar">
            <img alt="Session Picture" src="<?php echo $filename; ?>" width="240px" height="240px" style="border-radius:50%;"/>
            </a>
            <!-- End Avatar -->
            <!-- Change Avatar Modal -->
            <div id="avatar" class="reveal-modal" data-reveal>
              <div class="row">
                <div class="large-12 columns">
                  <p class="global-p">Do you want to upload a new image?</p>
                  <form method="post" enctype="multipart/form-data">
                    <img id="blah" src="<?php echo $filename; ?>" alt="your image" height="240px;" width="240px;" style="border-radius:50%;" />
                    <br />
                    <br />
                    <script type="text/javascript">
	        			function readURL(input) {
			        	    if (input.files && input.files[0]) {
			    	            var reader = new FileReader();
			    	            reader.onload = function (e) {
				                    $('#blah').attr('src', e.target.result);
				                }
				                reader.readAsDataURL(input.files[0]);
				            }
			    	    }
				    </script>
                    <input type="file" name="session_img" id="session_img" onchange="readURL(this);">
                    <br>
                    <input type="submit" name="file" id-"file" value="Submit">
                  </form>
                </div>
              </div>
            <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End Change Avatar Modal -->
  
            <h1 class="global-h1-small"><?php echo $info['S_NAME'];?></h1>
            <?php
			if ($_SESSION['valid']){
				$joined = SESSIONS::hasJoined($_GET['sessions'], $_SESSION['ID']);
				if($joined){
				  ?>
                    <a href="#" class="button success expand" data-reveal-id="session-status">You're a Member</a>
                    <a href="#" class="button expand" data-dropdown="session-tools">Session Tools &raquo;</a>
                  <?php
				  	if($info['P_ID'] == 'U.'.$_SESSION['ID']){
						//you get admin tools	
					}
				}else{
					//if you have logged in and want to join the session
					?>
						<a href="?sessions=<?php echo $_GET['sessions']; ?>&join=1" class="button expand">Join Session</a>
                    <?php
				}
			}else{
				//if you aren't logged in
				?>
                  <a href="http://dev.walkgeorgia.com/login.php?sessions=<?php echo $_GET['sessions']; ?>" class="button expand">Join Session</a>
                <?php
			}
			?>
            <!-- User-Session Relationship Modal -->
            <div id="session-status" class="reveal-modal" data-reveal>
              <h3 class="global-h3">You are currently a member of this session.</h3>
              <br />
              <a href="?sessions=<?php echo $_GET['sessions']; ?>&leave=1" class="button alert">I'm Sure</a>
              <a class="button">Whoops! Cancel</a>
              <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End User-Session Relationship Modal -->
            
            <!-- Session Tools Dropdown -->
            <ul id="session-tools" class="medium content f-dropdown" data-dropdown-content>
              <li><a href="#">Session Chat</a></li>
              <li><a href="#">Potential Admin Option 1</a></li>
              <li><a href="#">Potential Admin Option 2</a></li>
            </ul>
            <!-- End Session Tools Dropdown -->
      
          </div>
          
        </div>
      </div>
      <!-- End Desktop Sidebar -->
      
      <!-- Mobile Sidebar -->
      <div class="sidebar show-for-small-only" style="margin-bottom:20px;">
          <div align="center">
            <img src="../img/default-session-profile.png" alt="Session Image" />
  
            <h1 class="global-h1-small">Session Title</h1>
 
            <a href="#" class="button expand">Join Session</a>
            <a href="#" class="button success expand" data-reveal-id="session-status">You're Participating</a>
            
            <div id="session-status" class="reveal-modal" data-reveal>
              <h3 class="global-h3">You are currently a member of session.</h3>
              <h3 class="global-h3">Are you sure you want to leave this session?</h3>
              <br />
              <a href="?group=<?php echo $_GET['sessions']; ?>&leave=1" class="button alert">I'm Sure</a>
              <a class="button">Whoops! Cancel</a>
              <a class="close-reveal-modal">&#215;</a>
            </div>
      
          </div>
          
        </div>
      <!-- End Mobile Sidebar -->
      
      
    </div>
    
    
    <!-- Right Side Top -->
    <div class="large-7 medium-7 columns">
      <div class="row">
        <div class="medium-12 columns">
          <!-- Session Description -->
          <div align="justify">
            <h2 class="global-h2-gray">Description:</h2>
            <br />
            <br />
            <p class="global-p">
            <?php echo $info['S_DESC'];?>
            </p>
          </div>
          <!-- End Session Description -->
          <hr />
     
          <!-- Days Remaining in Session -->
		  <?php 
		  if(strtotime(date("Y-m-d"))-(strtotime($info['S_STARTDATE'])) < 0){
			  //it hasn't started ended
			  echo '<span class="label radius">This Session starts in '.(((strtotime($info['S_STARTDATE']))-(strtotime(date("Y-m-d")))) /60/60/24).' Days</span>';
		  };
		  if(strtotime(date("Y-m-d"))-(strtotime($info['S_ENDDATE'])) >= 0){
			  //it has ended
			  echo '<span class="label radius success">This Session is complete</span>';
		  }else
		  if(strtotime(date("Y-m-d"))-(strtotime($info['S_ENDDATE']))<= 0 && strtotime(date("Y-m-d"))-(strtotime($info['S_STARTDATE'])) >=0){
			  $days = ((strtotime(date("Y-m-d")))-(strtotime($info['S_STARTDATE']))/60/60/24);
			  if(($days < 10) && ($days > 0)){ $alert = 'alert';}
			  //it has started
			  echo '<span class="label radius'.$alert.'">This Session has '.(((strtotime(date("Y-m-d"))-(strtotime($info['S_STARTDATE']))))/60/60/24).' Days Left</span>';
		  };
		  ?>
          <br />
          <br />
          <div class="progress">
            <span class="meter" style="width:66%;"></span>
          </div>
          <!-- End Days Remaining in Session -->
          <hr />
          
          <!-- Session Stats -->
          <h2 class="global-h2-gray"><b>Session Stats:</b></h2>
      
          <div class="stat-unit"> 
            <div class="row">
              <div class="large-12 columns">     
                <img src="../img/points-icon.png" class="stat-icon hide-for-small-only" alt="Points Icon"> <a href="#" data-dropdown="drop1" class="statlink"><b>Total Points: 2014</b></a>
        
                <div id="drop1" data-dropdown-content class="f-dropdown content">
                  <small><b>Breakdown:</b></small>
                  <hr style="margin-top:5px; margin-bottom:5px;" />
                  <small>To find out where these points came from, look in the "Past Activity" section below.</small>
                </div>
            
              </div>
            </div>
          </div>
      
          <div class="stat-unit">
            <div class="row">
              <div class="large-12 columns">
                <img src="../img/time-icon.png" class="stat-icon hide-for-small-only" alt="Time Icon"> <a href="#" data-dropdown="drop2" class="statlink"><b>Hours Exercised: 41</b></a>
        
                <div id="drop2" data-dropdown-content class="f-dropdown content">
                  <small><b>Breakdown:</b></small>
                  <hr style="margin-top:5px; margin-bottom:5px;" />
                  <small>Biking: 20</small>
                  <br />
                  <small>Running: 20</small>
                  <br />
                  <small>Yoga: 1</small>
                </div>
        
              </div>
            </div>
          </div>
    
          <div class="stat-unit">
            <div class="row">
              <div class="large-12 columns">
                <img src="../img/distance-icon.png" class="stat-icon hide-for-small-only" alt="Distance Icon"> <a href="#" data-dropdown="drop3" class="statlink"><b>Miles Traveled: 10</b></a>
        
                <div id="drop3" data-dropdown-content class="f-dropdown content">
                  <small><b>Breakdown:</b></small>
                  <hr style="margin-top:5px; margin-bottom:5px;" />
                  <small>Biking: 5</small>
                  <br />
                  <small>Running: 5</small>
                </div>
        
              </div>
            </div>
          </div>
      
          <hr style="margin-bottom:5px;" /> 
          <a href="#"><small>Questions about these stats?  Click here.</small></a>
       
          <!-- End Session Stats -->
          
          <!-- Users Section -->
          <div class="row" style="margin-top:20px;">
            <div class="large-12 columns">
              <h2 class="global-h2-gray">Users:</h2>
              <hr style="margin-top:10px; margin-bottom:0px;" />
      
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>John French</strong></p>
                  </div>
                </div>
              </div>
              
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>Zach Galifinakis</strong></p>
                  </div>
                </div>
              </div>
              
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>Sean Penn</strong></p>
                  </div>
                </div>
              </div>
              
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>Jeff Goldblum</strong></p>
                  </div>
                </div>
              </div>
              
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>Bill Murray</strong></p>
                  </div>
                </div>
              </div>
      
      
            </div>
          </div>
          <!-- End Users Section -->
      
          <hr style="margin-top:0px;" />
          <a href="#" data-reveal-id="view-all-users" class="button tiny">View All Users</a>
      
          <!-- View all Users Modal -->
          <div id="view-all-users" class="reveal-modal" data-reveal>
      
          <div class="row">
            <div class="large-6 columns">
              <h2 class="global-h2">Users:</h2>
              <hr />
      
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>John French</strong></p>
                  </div>
                </div>
              </div>
              
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>John French</strong></p>
                  </div>
                </div>
              </div>
              
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>John French</strong></p>
                  </div>
                </div>
              </div>
              
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>John French</strong></p>
                  </div>
                </div>
              </div>
              
              <div class="user-unit">
                <div class="row">
                  <div class="large-12 columns">
                    <img src="../img/sample-profile-image-mobile.png" alt="Small Profile Image">
                    <p class="global-p"><strong>John French</strong></p>
                  </div>
                </div>
              </div>
      
            </div>
          </div>
          <a class="close-reveal-modal">&#215;</a>
          </div>
          <!-- End View all Users Modal -->          
          
        </div>
      </div>  
    </div>
  </div>
<!-- End Main Section -->
<?php 
}
//If no sessions id is specified, or if there is an error loading the sessions page
		else{
			$gohome = REDIRECT::home();
		}

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../js/modernizr.js"></script>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script src="../js/foundation/foundation.js"></script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
    <script src="../js/foundation/foundation.topbar.js"></script>
    <script src="../js/foundation/foundation.abide.js"></script>
    <script src="../js/foundation/foundation.reveal.js"></script>
    <script src="../js/foundation/foundation.tooltip.js"></script>
    <script src="../js/foundation/foundation.offcanvas.js"></script>
    <script src="../js/foundation/foundation.equalizer.js"></script>
    <script src="../js/foundation/foundation.dropdown.js"></script>
    <script type="text/javascript" src="../js/sha512.js"></script>
    <script type="text/javascript" src="../js/log_form.js"></script>
    <script>
      $(document).foundation();
    </script>
  <!-- End Footer -->
  </body>
</html>