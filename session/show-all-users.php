<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include("$root/lib/template_api.php");
include("$root/lib/groups_api.php");
$ss = SESSION::secure_session();
$sort = filter_input(INPUT_GET, 'sort', FILTER_SANITIZE_STRING);
if($_GET['logout']==1){
	SESSION::logout();
	REDIRECT::home();
}
HTML_ELEMENT::head('Welcome to Walk Georgia');
HTML_ELEMENT::top_nav();
$info = GROUP::get_info(filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING));
//EDIT HERE

?>

<div id="main">
    
  <div class="row">
    <div class="large-12 columns">
      <h1 class="custom-font-small-blue"><?php echo $info['G_NAME'];?>'s Users</h1>
      <hr />
    </div>
  </div>   
  
  <div class="row">
  
  <form>
    <input type="hidden" name="group" id="group" value="<?php echo filter_input(INPUT_GET, 'group', FILTER_SANITIZE_STRING); ?>"/>
    <input type="hidden" name="search" id="search" value="<?php echo filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING); ?>"/>
    <div class="large-6 columns">
      <h2 class="global-h2">Search:</h2>
      <div class="row">
        <div class="large-12 columns">
          <div class="row collapse">
            <div class="small-10 columns">
              <input type="text" name="search" id="search" placeholder="Search for Users in this Group">
            </div>
            <div class="small-2 columns">
              <a href="#" class="button postfix">Go</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="large-6 columns">
      <h2 class="global-h2">Arrange:</h2>
      <div class="row">
        <div class="large-12 columns">
          <label>
            <select id="sort" name ="sort" onchange="form.submit();">
              <option <?php if($sort=='L_LNAME ASC'){echo 'selected="selected"';} ?> value="L_LNAME ASC">Descending Alphabetical Order (Last Name)</option>
              <option <?php if($sort=='L_LNAME DESC'){echo 'selected="selected"';} ?> value="L_LNAME DESC">Ascending Alphabetical Order (Last Name)</option>
              <option <?php if($sort=='POINTS DESC'){echo 'selected="selected"';} ?> value="POINTS DESC">Total Points (Highest First)</option>
              <option <?php if($sort=='SECONDS DESC'){echo 'selected="selected"';} ?> value="SECONDS DESC">Total Time (Highest First)</option>
            </select>
          </label>
        </div>
      </div>
    </div>
    
  </form>  
    
  </div>
  
  <div class="row">
    <div class="large-12 columns">
      <hr />
    </div>
  </div>
  
  <div class="row">
    <div class="large-12 columns">
      
      <?php GROUP::list_all_users_page(); ?>
    
    
    </div>
  </div>
            
</div>

<?php

//STOPEDITING

HTML_ELEMENT::footer();

//JAVASCRIPTS GO HERE
?>
	<script type="text/javascript">

        $(function() {

            $('#toggle4').click(function() {
	        $('.toggle4').slideToggle('fast');
	        return false;
              });

            });

    </script>
    <script type="text/javascript">
	function formhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="p";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit()}function regformhash(e,t){var n=document.createElement("input");e.appendChild(n);n.name="P";n.type="hidden";n.value=hex_sha512(t.value);t.value="";e.submit();return true}
	</script>
    <script type="text/javascript">
	(function(e,t,n,r){"use strict";Foundation.libs.alert={name:"alert",version:"5.1.1",settings:{animation:"fadeOut",speed:300,callback:function(){}},init:function(e,t,n){this.bindings(t,n)},events:function(){var t=this,n=this.S;e(this.scope).off(".alert").on("click.fndtn.alert","["+this.attr_name()+"] a.close",function(e){var r=n(this).closest("["+t.attr_name()+"]"),i=r.data(t.attr_name(true)+"-init")||t.settings;e.preventDefault();r[i.animation](i.speed,function(){n(this).trigger("closed").remove();i.callback()})})},reflow:function(){}}})(jQuery,this,this.document)
	</script>
  <!-- End Footer -->
  </body>
</html>