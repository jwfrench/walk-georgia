<?php
//If no group id is specified, or if there is an error loading the group page
include("../lib/template_api.php");
include("../lib/back_api.php");
include("../lib/groups_api.php");
if(empty($_GET['group'])||!isset($_GET['group'])){
  $gohome = REDIRECT::home();
  exit();
}else{
    $info = GROUP::get_info($_GET['group']);	
}

$ss = SESSION::secure_session();

if($_GET['logout']==1){
	SESSION::logout();
	REDIRECT::home();
}
if(GROUP::isAdmin($_GET['group'])){
  if(isset($_POST['file'])){
  	$img = GROUP::avatar_upload($_GET['group']);
  }
}
if($_GET['join']){
	$join_group = GROUP::join_group($_GET['group'], $_SESSION['ID']);
}
if($_GET['leave']){
	$leave_group = GROUP::leave_group($_GET['group'], $_SESSION['ID']);
}
$breakdown = GROUP::getBreakdown($_GET['group']);
$points_total = array_sum($breakdown['POINTS']);
if(!$points_total>0){$points_total = '0';}
$time_total  = array_sum($breakdown['TIME']);
if(!$time_total>0){$time_total = '0';}
$distance_total = array_sum($breakdown['DISTANCE']);
if(!$distance_total>0){$distance_total = '0';}
HTML_ELEMENT::head('Groups');
HTML_ELEMENT::top_nav();
//EDIT HERE
?> 

<div class="main">
  <!-- Group Header -->
  <div class="blue-bg-group" style="margin-top:-20px;"> 
  
    <div class="row" style="">
      <div class="large-3 medium-3 hide-for-small columns">
            <?php
          if (file_exists($_SERVER['DOCUMENT_ROOT']."/group/img/avatar/".$_GET['group'].".png")) {
			  $filename = "/group/img/avatar/".$_GET['group'].".png";
		  } else {
    			$filename = "../img/default-group-profile.png";
		  }
		  if('U.'.$_SESSION['ID'] == $info['G_PID'] || $isAdmin){
		  ?>
            <!-- Avatar -->
            <a href="#" data-reveal-id="avatar">
            <?php
		  }
			?>
            <div class="avatar-medium" style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat;"></div>
            </a>
            <!-- End Avatar -->

            <!-- Change Avatar Modal -->
            <div id="avatar" class="reveal-modal" data-reveal>
              <div class="row">
                <div class="large-12 columns">
                  <p class="global-p">Do you want to upload a new image?</p>
                  <form method="post" enctype="multipart/form-data">
                    <img id="blah" src="<?php echo $filename; ?>" alt="your image" height="240px;" width="240px;" style="border-radius:50%;" />
                    <br />
                    <br />
                    <script type="text/javascript">
	        			function readURL(input) {
			        	    if (input.files && input.files[0]) {
			    	            var reader = new FileReader();
			    	            reader.onload = function (e) {
				                    $('#blah').attr('src', e.target.result);
				                }
				                reader.readAsDataURL(input.files[0]);
				            }
			    	    }
				    </script>
                    <input type="file" name="group_img" id="group_img" onchange="readURL(this);">
                    <br>
                    <input type="submit" name="file" id-"file" value="Submit">
                  </form>
                </div>
              </div>
            <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End Change Avatar Modal -->
      </div>
      <div class="large-9 medium-9 small-12 columns">
        <h1 class="custom-font-small-white"><?php echo $info['G_NAME'];?></h1>
        <hr style="color:white; border-color:white; margin-top:-5px; margin-bottom:5px;" />
        <p class="global-p-white">
        <?php echo $info['G_DESC'];
			?>
        </p>
        <?php
			if ($_SESSION['valid']){
				$joined = GROUP::hasJoined($_GET['group'], $_SESSION['ID']);
				if($joined){
				  ?>
                    <a href="#" class="button success round tiny" data-reveal-id="group-status">You're a Member</a>
                    <?php
					$isAdmin = GROUP::isAdmin($_GET['group']);
                    if($isAdmin){
						?>
                    <a href="#" class="button secondary round tiny" data-reveal-id="group-tools">Group Tools</a>
				  <?php
					}
					?>
                  <?php
				}else{
					//if you have logged in, want to join the group, AND it HAS NO special permission or data requirements
					if($info['G_META']=='' && $info['G_PERMISSIONS'] == '0:0:0'){
				      ?>
					    <a href="?group=<?php echo $_GET['group']; ?>&join=1" class="round tiny secondary button">Join Group</a>
                      <?php
					}
					//if you have logged in, want to join the group, AND it HAS special permission or data requirements
					else{
						//modal here with permissions form
						?>
                          <div id="join-group" class="reveal-modal" data-reveal>
                            <div class="row">
                              <form method="get" data-abide>
                                <div class="large-12 columns">
                                  <h2 class="global-h2">Join Group</h2>
                                  <hr />
                                  <p class="global-p">This group requires additional information to join:</p>
                                  <?php if($info['G_META']){ ?>
                                  <div class="row">
                                    <div class="large-6 columns">
                                      <?php
										 $inputs = GROUP::meta_form($info['G_META']);
									  ?>
                                    </div>
                                  </div>
                                  <?php }
                                  if($info['G_PERMISSIONS']){ ?>
                                  <div class="row">
                                    <div class="large-6 columns">
                                      <label>By joining this group, I give access to my name, profile picture, and exercise data I choose to the group's page.</label>
                                      <input id="checkbox1" type="checkbox" required><label for="checkbox1">I Agree</label>
                                      <input type="hidden" id="join" name="join" value="1" />
                                      <input type="hidden" id="group" name="group" value="<?php echo $_GET['group']; ?>" />
                                    </div>
                                  </div>
                                  <?php } ?>
                                  <input type="submit" class="button success" value="Join Group">
                                </div>
                              </form>
                            </div>
                            <a class="close-reveal-modal">&#215;</a>
                          </div>
                          <a class="round tiny secondary button" data-reveal-id="join-group">Join Group</a>
                        <?php
					}
				}
			}else{
				//if you aren't logged in
				?>
                  <a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/login.php?group=<?php echo $_GET['group']; ?>" class="round tiny secondary button">Join Group</a>
                <?php
			}
			?>
      </div>
    </div>

  </div>
  <!-- End Group Header -->

  <div class="row">
  <div class="large-12 columns">
  <?php if(isset($_GET['err_msg'])){?>
    <div data-alert class="alert-box alert center">
      <?php echo $_GET['err_msg'];?></a>
      <a href="#" class="close">&times;</a>
    </div>
        <?php
	}?>
    
    
    
     <ul class="breadcrumbs hide-for-small" style="margin-top:20px;">
       <?php $bc = GROUP::breadcrumbs($_GET['group']); ?>   
     </ul>
    
    </div>
    
    </div>
    
    
    
    <!-- Modals Section -->
            
            <!-- User-Group Relationship Modal -->
            <div id="group-status" class="reveal-modal" data-reveal>
              <h3 class="global-h2">Membership Status</h3>
              <hr style="margin-top:-5px;" />
              <p class="global-p">
                You are currently a member of this group.  Would you like to leave this group?
              </p>
              <a href="?group=<?php echo $_GET['group']; ?>&leave=1" class="button tiny alert">Yes, leave this group.</a>
              <a class="button tiny">Whoops! Cancel</a>
              <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End User-Group Relationship Modal -->
            
            <!-- Group Tools Modal -->
            <?php 
			
			if($isAdmin){
			?>
            <div id="group-tools" class="reveal-modal" data-reveal>
              <div class="row">
                <div class="large-12 columns">
                  <h2 class="global-h2">Group Tools</h2>
                  <hr style="margin-top:-5px;">
                </div>
              </div>
              <div class="row center">
                <div class="large-6 medium-6 small-12 columns">
                  <a href="subgroups.php?group=<?php echo $_GET['group']; ?>" class="button tiny expand">Manage Sub-Groups</a>
                </div>
                <div class="large-6 medium-6 small-12 columns">
                  <a href="#" data-reveal-id="edit-group" class="button tiny expand">Edit Group</a>
                </div>
              </div>
              <div class="row center">
                <div class="large-6 medium-6 small-12 columns">
                  <a href="#" data-reveal-id="share-url" class="tiny button expand">Invite Members</a>
                </div>
                <div class="large-6 medium-6 small-12 columns">
                  <a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].'/reporting.php?group='.$_GET['group'] ?>" class="tiny button expand">Generate Group Report</a>
                </div>
              </div>
              <div class="row center">
                <div class="large-6 medium-6 small-12 columns">
                  <a href="#" data-reveal-id="delete-group" class="button alert tiny expand">Delete Group</a>
                </div>
              </div>

              <a class="close-reveal-modal">&#215;</a>
            </div>
            <!-- End Group Tools Modal -->
            
            <!-- Invite Others Modal -->
            <div id="share-url" class="reveal-modal" data-reveal>
            <div class="row">
              <div class="large-12 columns">
                <h2 class="global-h2">Share Group Link</h2>
                <hr style="margin-top:-5px;" />
                <p class="global-p">The link below is unique to your group.  Copy it and distriubte it however you like (email, print, social media) to the people you want to join your group!</p>
              </div>
            </div>
            <div class="row">
              <div class="large-12 columns">
              <form>
                <div class="row collapse">
                  <div class="small-3 large-2 columns">
                    <span class="prefix">Link:</span>
                  </div>
                  <div class="small-9 large-10 columns">
                    <input type="text" placeholder="Link here" 
                    value="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?group='.$_GET['group']; ?>" readonly>
                  </div>
                </div>
              </form>
              </div>
            </div>
            <a class="close-reveal-modal">&#215;</a>
            </div>                        
            <!-- End Invite Others Modal -->
            <!-- Edit Group Info Modal -->
            <div id="edit-group" class="reveal-modal" data-reveal>
              <div class="row">
                <div class="large-12 columns">
                  <h2 class="global-h2">Edit Group Info</h2>
                  <hr style="margin-top:-5px;" />
                </div>
              </div>
              <form method="post" action="edit_group.php">
              <div class="row">
                <div class="large-12 columns">
                  <div class="row">
                    <div class="large-6 columns">
                      <label>Group Name</label>
                      <input type="text" id="NAME" name="NAME" value="<?php echo $info['G_NAME']; ?>" />
                    </div>
                  </div>
                  <div class="row">
                    <div class="large-12 columns">
                      <label>Group Description</label>
                      <textarea id="DESC" name="DESC" style="height:100px;"><?php echo $info['G_DESC']; ?></textarea>
                    </div>
                  </div>
                  <div class="row">
                    <div class="large-6 columns">
                      <label>Group Visibility</label>
                      <select id="G_VIS" name="G_VIS">
                        <option id="G_VIS_1" name="G_VIS_1" value="1" <?php if($info['G_VIS']==1){echo 'selected="selected"';} ?>>Public (Any user can find the group and join)</option>
                        <option id="G_VIS_0" name="G_VIS_0" value="0" <?php if($info['G_VIS']==0){echo 'selected="selected"';} ?>>Private (Only users who have the link to the group's page may join)</option>
                      </select>
                      <input type="hidden" id="G_ID" name="G_ID" value="<?php echo $_GET['group']; ?>" />
                      <br />
                      <br />
                    </div>
                  </div>
                  <div class="row">
                    <div class="large-6 columns">
                      <input type="submit" class="tiny button" value="Submit"/>
                    </div>
                  </div>
                </div>
              </div>
              </form>
              <a class="close-reveal-modal">&#215;</a>
            </div>                        
            <!-- End Edit Group Info Modal -->
            
            <!-- Delete Group Modal -->
            <div id="delete-group" class="reveal-modal" data-reveal>
              <div class="row">
                <div class="large-12 columns">
                  <h2 class="global-h2">Delete Group</h2>
                  <hr style="margin-top:-5px;" />
                  <p class="global-p">Are you sure you want to permanently delete this group?</p>
                  <a href="delete_group.php?group=<?php echo $_GET['group']; ?>" class="button tiny alert">Delete</a> <a href="#" class="button tiny">Cancel</a>
                </div>
              </div>
            <a class="close-reveal-modal">&#215;</a>
            </div>            
            <!-- End Delete Group Modal -->
            
            <?php
			}
			?>
          

      <!-- End Modals Section -->
      
      
      <div class="row" style="margin-top:20px;"> 
      
        <div class="large-5 medium-5 columns">
        
        <!-- Info & Events -->
        <h2 class="custom-font-small-blue">Info & Events</h2>
        <hr style="margin-top:10px; margin-bottom:15px;" />
        
        <p class="global-p">We are currently developing an exciting new widget for this area that we'll tell you more about soon.  We've moved the subgroup and members widget below to give you more room.</p>
        
        <!-- End Info & Events -->
        
         
       </div>  
          

          
          
          <!-- Quick Stats Section -->
          
          <div class="large-7 medium-7 small-12 columns">
          
          <h2 class="custom-font-small-blue">Quick Stats</h2>
        <hr style="margin-top:10px; margin-bottom:15px;" />
            
            <!-- Points -->
        <div class="blue-bg-link" id="points-toggle" style="padding: 10px 20px 10px 20px; margin: 0px 0px 0px 0px; cursor:pointer;">
        <h2 class="global-h2-white"><b></b></h2>
        <h2 class="custom-font-big-white" style="margin-bottom:-20px;"><?php echo $points_total;?></h2>
        <h3 class="global-h2-white" style="margin-top:-20px;">Points Earned</h3>
        
        <!-- Points Breakdown -->
        <div id="points-breakdown" style="display:none;">
          <ul class="global-p-white">
            <?php 
			  	foreach($breakdown['POINTS'] as $key =>$activity){
					if($activity < 60){
					?>
                    	<li><?php echo ACTIVITY::activity_to_form($key).": ".number_format($activity)."s"; ?></li>
                    <?php
					}else if ($activity < 3600){
						?>
						<li><?php echo ACTIVITY::activity_to_form($key).": ".number_format(($activity/60))."m"; ?></li>
                        <?php
					}else{
						?>
						<li><?php echo ACTIVITY::activity_to_form($key).": ".number_format(($activity/3600))."h ".number_format((($time_total%3600)/60))."m"; ?></li>
                        <?php
					}

					
				}
			  ?>
          </ul>
        </div>
        <!-- End Points Breakdown -->
        
      </div>
      <!-- End Points -->
      
      <!-- Time -->
      <div class="peach-bg-link" id="time-toggle" style="padding: 10px 20px 10px 20px; margin: 0 0 0 0; cursor:pointer;">
        <div style="display:block; margin-bottom:-15px;">
          <h3 class="global-h2-white"><b>Time Exercised:</b></h3>
        </div>
        <!-- Hours -->
        <div style="display:inline-block; margin-right:15px;">
          <h2 class="custom-font-big-white" style="margin-bottom:-20px;"><?php echo $hours = number_format(floor($time_total/3600)); ?></h2>  
          <h3 class="global-h2-white" style="margin-top:-20px;"><b>Hours</b></h3> 
        </div>
        <!-- End Hours -->
        <!-- Minutes -->
        <div style="display:inline-block"> 
          <h2 class="custom-font-big-white" style="margin-bottom:-20px;"><?php echo $minutes = floor(($time_total%3600)/60); ?></h2>  
          <h3 class="global-h2-white" style="margin-top:-20px;"><b>Minutes</b></h3> 
        </div>
        <!-- End Minutes -->

        <!-- Time Breakdown -->
        <div id="time-breakdown" style="display:none;">
          <ul class="global-p-white">
            <?php 
			  	foreach($breakdown['TIME'] as $key =>$activity){
					if($activity < 60){
					?>
                    	<li><?php echo ACTIVITY::activity_to_form($key).": ".number_format($activity)."s"; ?></li>
                    <?php
					}else if ($activity < 3600){
						?>
						<li><?php echo ACTIVITY::activity_to_form($key).": ".number_format(($activity/60))."m"; ?></li>
                        <?php
					}else{
						?>
						<li><?php echo ACTIVITY::activity_to_form($key).": ".number_format(($activity/3600))."h ".number_format((($time_total%3600)/60))."m"; ?></li>
                        <?php
					}

					
				}
			  ?>
          </ul>
        </div>
        <!-- End Time Breakdown -->
        
      </div>
      <!-- End Time -->
      
      <!-- Distance -->
      <div class="green-bg-link" id="distance-toggle" style="padding: 10px 20px 10px 20px; margin: 0 0 0 0; cursor:pointer;">
        <div style="display:block; margin-bottom:-15px;">
          <h3 class="global-h2-white"><b>Distance Traveled:</b></h3>
        </div>
        <!-- Miles -->
        <div style="display:inline-block; margin-right:15px;">
          <h2 class="custom-font-big-white" style="margin-bottom:-20px;"><?php echo number_format($distance_total); ?></h2>  
          <h3 class="global-h2-white" style="margin-top:-20px;"><b>Miles</b></h3> 
        </div>
        <!-- End Miles -->

        <!-- Distance Breakdown -->
        <div id="distance-breakdown" style="display:none;">
          <ul class="global-p-white">
          <?php 
			  	foreach($breakdown['DISTANCE'] as $key=>$activity){
					$DA = MSSQL::query('SELECT A_UNIT FROM ACTIVITY WHERE A_ID=\''.$key.'\'');
					$DA = odbc_result($DA, 'A_UNIT');
					if($DA ==1){
					?>
                    <li><?php echo ACTIVITY::activity_to_form($key).': '.number_format($activity).' miles'; ?></li>
                    <?php
					}
				}
			  ?>
           
          </ul>
        </div>
        <!-- End Distance Breakdown -->
        
      </div>
      
      <!-- End Distance -->
      
      <?php
					$isAdmin = GROUP::isAdmin($_GET['group']);
                    if($isAdmin){
						?>
                    <br /><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].'/reporting.php?group='.$_GET['group'] ?>" class="tiny button">Generate Group Report</a>
                    <?php
					}
					?>
            
          </div>
          
          <!-- End Quick Stats Section -->
          
      
         </div> 
       
  
  <div class="row">
  
    <div class="large-6 medium-6 columns">
    
      <!-- Subgroup Section -->
         
        <h2 class="custom-font-small-blue">Subgroups</h2>
        <hr style="margin-top:10px; margin-bottom:15px;" />
         
              <?php  
			    $subgroups = GROUP::list_subgroups($_GET['group']);
			  ?>
         
         
         <!-- End Subgroup Section -->
    
    </div>
  
    <div class="large-6 medium-6 columns">
    
      <!-- Members Section -->
      
      <h2 class="custom-font-small-blue">Members</h2>
              <hr style="margin-top:10px; margin-bottom:0px;" />
              <br />
              <?php 
			    $members = GROUP::list_users_in_group($_GET['group'], 4);
			  ?>
              
              
      </div>  
      <!-- End Members Section -->
    
    </div>
  
<!-- End Main Section -->  
</div>
<!-- End Main Section -->
<?php 
//STOPEDITING

//JAVASCRIPTS GO HERE
?>
	<div class="footer">
      <div class="row">
        <div>
          <a class="tiny button alert" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/contact-us.php"> Report a Bug </a>
        </div>
        
        <p class="global-p">
          <a href="http://www.facebook.com/walkgeorgia">Facebook</a> | <a href="https://twitter.com/walkga">Twitter</a> | <a href="http://blog.extension.uga.edu/walkgeorgia">Walk Georgia Blog</a>
        </p>
        <br />
          <small>The College of Agricultural and Environmental Sciences and the College of Family and Consumer Sciences cooperating.
The University of Georgia &copy; 2014. All Rights Reserved.</small>
          <hr><img alt="The University of Georgia Cooperative Extension" style="width:87px; height:31px;" src="../img/ext.png"><br><br>
        </div>
      </div>
    </div><script type="text/javascript" src="../js/dataTables.foundation.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="../js/foundation.min.js"></script>
<script>
     $(document).foundation();
</script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/plug-ins/380cb78f450/integration/foundation/dataTables.foundation.js"></script>

<script>
$(document).ready(function() {
    	  $('table.display').DataTable({
			"oLanguage": {
              "sEmptyTable": " "
            },
	        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
			responsive:true
		  });
		} );
</script>
</body>
</html>