<?php
include("../lib/template_api.php");
include("../lib/back_api.php");
include("../lib/sessions_api.php");
//If no session id is specified, or if there is an error loading the session page
if(empty($_GET['session'])||!isset($_GET['session'])){
  $gohome = REDIRECT::home();
  exit();
}else{
	$info = SESSIONS::get_info($_GET['session']);	
}
//LOGIN SESSION, NOT WALK GA SESSION
$ss = SESSION::secure_session();

if($_GET['logout']==1){
	SESSION::logout();
	REDIRECT::home();
}
if(SESSIONS::isAdmin($_GET['session'])){
  if(isset($_POST['file'])){
  	$img = SESSIONS::avatar_upload($_GET['session']);
  }
}
if($_GET['join']){
	$join_session = SESSIONS::join_session($_GET['session']);
}
if($_GET['leave']){
	$leave_session = SESSIONS::leave_session($_GET['session'], $_SESSION['ID']);
}
$breakdown = SESSIONS::getBreakdown($_GET['session'], $info['S_STARTDATE'], $info['S_ENDDATE']);
$points_total = array_sum($breakdown['POINTS']);
if(!$points_total>0){$points_total = '0';}
$time_total  = array_sum($breakdown['TIME']);
if(!$time_total>0){$time_total = '0';}
$distance_total = array_sum($breakdown['DISTANCE']);
if(!$distance_total>0){$distance_total = '0';}
$UUID=odbc_result(MSSQL::query('SELECT L_UUID FROM LOGIN WHERE L_ID=\''.$_SESSION['ID'].'\''), 'L_UUID');
$isAdmin =odbc_result(MSSQL::query('SELECT IS_ADMIN FROM SESSION_MEMBER WHERE U_ID=\''.$_SESSION['ID'].'\''), 'IS_ADMIN');
HTML_ELEMENT::head('Sessions');
HTML_ELEMENT::top_nav();
//EDIT HERE
?>

<div class="main">
  <!-- Session Header -->
  <div class="peach-bg" style="margin-top:-10px;">
  
	<div class="row" style="margin-top:2em;">
	  <div class="large-3 medium-3 hide-for-small columns">
			<?php
		  if (file_exists($_SERVER['DOCUMENT_ROOT']."/session/img/avatar/".$_GET['session'].".png")) {
			  $filename = "/session/img/avatar/".$_GET['session'].".png";
		  } else {
				$filename = "../img/default-session-profile.png";
		  }
		  if($UUID == $info['S_PID'] || $isAdmin){
		  ?>
			<!-- Avatar -->
			<a href="#" data-reveal-id="avatar">
			<?php
		  }
			?>
			<div class="avatar-medium" style="background:url(<?php echo $filename; ?>);background-position: center;background-size:cover;background-repeat: no-repeat;"></div>
			</a>
			<!-- End Avatar -->

			<!-- Change Avatar Modal -->
			<div id="avatar" class="reveal-modal" data-reveal>
			  <div class="row">
				<div class="large-12 columns">
				  <p class="global-p">Do you want to upload a new image?</p>
				  <form method="post" enctype="multipart/form-data">
					<img id="blah" src="<?php echo $filename; ?>" alt="your image" height="240px;" width="240px;" style="border-radius:50%;" />
					<br />
					<br />
					<script type="text/javascript">
						function readURL(input) {
							if (input.files && input.files[0]) {
								var reader = new FileReader();
								reader.onload = function (e) {
									$('#blah').attr('src', e.target.result);
								}
								reader.readAsDataURL(input.files[0]);
							}
						}
					</script>
					<input type="file" name="session_img" id="session_img" onchange="readURL(this);">
					<br>
					<input type="submit" name="file" id="file" value="Submit">
				  </form>
				</div>
			  </div>
			<a class="close-reveal-modal">&#215;</a>
			</div>
			<!-- End Change Avatar Modal -->
	  </div>
	  <div class="large-9 medium-9 small-12 columns">
		<h1 class="custom-font-small-white"><?php echo $info['S_NAME'];?></h1>
		<hr style="color:white; border-color:white; margin-top:-5px; margin-bottom:5px;" />
		<p class="global-p-white">
		<?php echo $info['S_DESC'];
			?>
		</p>
		<?php
			if ($_SESSION['valid']){
				$joined = SESSIONS::hasJoined($_GET['session'], $_SESSION['ID']);
				if($joined){
				  ?>
					<a href="#" class="button success round tiny" data-reveal-id="session-status">You're a Member</a>
					<?php
					$isAdmin = SESSIONS::isAdmin($_GET['session']);
					if($isAdmin){
						?>
					<a href="#" class="button secondary round tiny" data-reveal-id="session-tools">Session Tools</a>
				  <?php
					}
				}else{
					//if you have logged in and want to join the session
					?>
						<a href="?session=<?php echo $_GET['session']; ?>&join=1" class="round tiny secondary button">Join Session</a>
					<?php
				}
			}else{
				//if you aren't logged in
				?>
				  <a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/login.php" class="round tiny secondary button">Join Session</a>
				<?php
			}
			?>
	  </div>
	</div>

  </div>
  <!-- End Session Header -->

  <div class="row">
  <div class="large-12 columns">
  <?php if(isset($_GET['err_msg'])){?>
	<div data-alert class="alert-box alert center">
	  <?php echo $_GET['err_msg'];?></a>
	  <a href="#" class="close">&times;</a>
	</div>
		<?php
	}?>
	
	<?php
		$datetime1 = date_create(date('Y-m-d'));
		$datetime2 = date_create($info['S_ENDDATE']);
		$datetime3 = date_create($info['S_STARTDATE']);
		if($datetime1 <= $datetime2 && $datetime1 >= $datetime3){
			$interval = date_diff($datetime1, $datetime2);
			$total_days = date_diff($datetime3, $datetime2);
			$percent = (($total_days->format('%a') - $interval->format('%a')) / $total_days->format('%a'));
			$days_left = $interval->format('%a days remaining ');
		}else if($datetime1 < $datetime2 && $datetime1 < $datetime3){
			$total_days = date_diff($datetime3, $datetime2);
			$percent = 0;
			$days_left = $total_days->format('%a days remaining ');
		}else{
			$days_left = '0 days remaining';	
			$percent = 1;
		}
	?>
	<!-- Progress Bar -->
	<div class="row" style="margin-top:2em;">
	  <div class="large-12 columns center">
		<h2 class="custom-font-small-blue"><?php echo $days_left;?></h2>
		<div class="progress small-12 large-12 round">
		 <span class="meter" style="width: <?php echo $percent*100; ?>%"></span>
		 </div>
	   </div>
	 </div>
	 <!-- End Progress Bar -->
	 
	
	</div>
	
	</div>
	
	
	
	<!-- Modals Section -->
			
			<!-- User-Session Relationship Modal -->
			<div id="session-status" class="reveal-modal" data-reveal>
			  <h3 class="global-h2">Membership Status</h3>
			  <hr style="margin-top:-5px;" />
			  <p class="global-p">
				You are currently a member of this session.  Would you like to leave this session?
			  </p>
			  <a href="?session=<?php echo $_GET['session']; ?>&leave=1" class="button tiny alert">Yes, leave this session.</a>
			  <a class="button tiny">Whoops! Cancel</a>
			  <a class="close-reveal-modal">&#215;</a>
			</div>
			<!-- End User-Session Relationship Modal -->
			
			<!-- Session Tools Modal -->
			<?php 
			
			if($isAdmin){
			?>
			<div id="session-tools" class="reveal-modal" data-reveal>
			  <div class="row">
				<div class="large-12 columns">
				  <h2 class="global-h2">Session Tools</h2>
				  <hr style="margin-top:-5px;">
				</div>
			  </div>
			  <div class="row center">
				<div class="large-6 medium-6 small-12 columns">
				  <a href="#" data-reveal-id="edit-session" class="button tiny expand">Edit Session</a>
				</div>
			  </div>
			  <div class="row center">
				<div class="large-6 medium-6 small-12 columns">
				  <a href="#" data-reveal-id="share-url" class="tiny button expand">Invite Members</a>
				</div>
				<div class="large-6 medium-6 small-12 columns">
				  <a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].'/reporting_session.php?session='.$_GET['session'] ?>" class="tiny button expand">Generate Session Report</a>
				</div>
			  </div>
			  <div class="row center">
				<div class="large-6 medium-6 small-12 columns">
				  <a href="#" data-reveal-id="delete-session" class="button alert tiny expand">Delete Session</a>
				</div>
			  </div>

			  <a class="close-reveal-modal">&#215;</a>
			</div>
			<!-- End Session Tools Modal -->
			
			<!-- Invite Others Modal -->
			<div id="share-url" class="reveal-modal" data-reveal>
			<div class="row">
			  <div class="large-12 columns">
				<h2 class="global-h2">Share Session Link</h2>
				<hr style="margin-top:-5px;" />
				<p class="global-p">The link below is unique to your session.  Copy it and distriubte it however you like (email, print, social media) to the people you want to join your session!</p>
			  </div>
			</div>
			<div class="row">
			  <div class="large-12 columns">
			  <form>
				<div class="row collapse">
				  <div class="small-3 large-2 columns">
					<span class="prefix">Link:</span>
				  </div>
				  <div class="small-9 large-10 columns">
					<input type="text" placeholder="Link here" 
					value="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?session='.$_GET['session']; ?>" readonly>
				  </div>
				</div>
			  </form>
			  </div>
			</div>
			<a class="close-reveal-modal">&#215;</a>
			</div>						
			<!-- End Invite Others Modal -->
			<!-- Edit Session Info Modal -->
			<div id="edit-session" class="reveal-modal" data-reveal>
			  <div class="row">
				<div class="large-12 columns">
				  <h2 class="global-h2">Edit Session Info</h2>
				  <hr style="margin-top:-5px;" />
				</div>
			  </div>
			  <form method="post" action="edit_session.php">
			  <div class="row">
				<div class="large-12 columns">
				  <div class="row">
					<div class="large-6 columns">
					  <label>Session Name</label>
					  <input type="text" id="NAME" name="NAME" value="<?php echo $info['S_NAME']; ?>" />
					</div>
				  </div>
				  <div class="row">
					<div class="large-12 columns">
					  <label>Session Description</label>
					  <textarea id="DESC" name="DESC" style="height:100px;"><?php echo $info['S_DESC']; ?></textarea>
					</div>
				  </div>
				  <div class="row">
					<div class="large-6 columns">
					  <label>Session Visibility</label>
					  <select id="S_VIS" name="S_VIS">
						<option id="S_VIS_1" name="S_VIS_1" value="1" <?php if($info['S_VIS']==1){echo 'selected="selected"';} ?>>Public (Any user can find the session and join)</option>
						<option id="S_VIS_0" name="S_VIS_0" value="0" <?php if($info['S_VIS']==0){echo 'selected="selected"';} ?>>Private (Only users who have the link to the session's page may join)</option>
					  </select>
					  <input type="hidden" id="S_ID" name="S_ID" value="<?php echo $_GET['session']; ?>" />
					  <br />
					  <br />
					</div>
				  </div>
				  <div class="row">
					<div class="large-6 columns">
					  <input type="submit" class="tiny button" value="Submit"/>
					</div>
				  </div>
				</div>
			  </div>
			  </form>
			  <a class="close-reveal-modal">&#215;</a>
			</div>						
			<!-- End Edit Session Info Modal -->
			
			<!-- Delete Session Modal -->
			<div id="delete-session" class="reveal-modal" data-reveal>
			  <div class="row">
				<div class="large-12 columns">
				  <h2 class="global-h2">Delete Session</h2>
				  <hr style="margin-top:-5px;" />
				  <p class="global-p">Are you sure you want to permanently delete this session?</p>
				  <a href="delete_session.php?session=<?php echo $_GET['session']; ?>" class="button tiny alert">Delete</a> <a href="#" class="button tiny">Cancel</a>
				</div>
			  </div>
			<a class="close-reveal-modal">&#215;</a>
			</div>			
			<!-- End Delete Session Modal -->
			
			<?php
			}
			?>
		  

	  <!-- End Modals Section -->
	  
	  
	  <div class="row" style="margin-top:20px;"> 
	  
		<div class="large-5 medium-5 columns">
		
		<!-- Info & Events -->
		<h2 class="custom-font-small-blue">Session Dates</h2>
		<hr style="margin-top:10px; margin-bottom:15px;" />
		
		<h2 class="global-h2">Start Date:</h2>
		<p class="global-p"><?php echo $info['S_STARTDATE']; ?></p>
		<h2 class="global-h2">End Date:</h2>
		<p class="global-p"><?php echo $info['S_ENDDATE']; ?></p>
		
		<!-- End Info & Events -->
		
		 
	   </div>  
		  

		  
		  
		  <!-- Quick Stats Section -->
		  
		  <div class="large-7 medium-7 small-12 columns">
		  
		  <h2 class="custom-font-small-blue">Quick Stats</h2>
		<hr style="margin-top:10px; margin-bottom:15px;" />
			
			<!-- Points -->
		<div class="blue-bg-link" id="points-toggle" style="padding: 10px 20px 10px 20px; margin: 0px 0px 0px 0px; cursor:pointer;">
		<h2 class="global-h2-white"><b></b></h2>
		<h2 class="custom-font-big-white" style="margin-bottom:-20px;"><?php echo $points_total;?></h2>
		<h3 class="global-h2-white" style="margin-top:-20px;">Points Earned</h3>
		
		<!-- Points Breakdown -->
		<div id="points-breakdown" style="display:none;">
		  <ul class="global-p-white">
			<?php 
			  	foreach($breakdown['POINTS'] as $key =>$activity){
					?>
                    	<li><?php echo ACTIVITY::activity_to_form($key).": ".$activity; ?></li>
                    <?php
				}
			  ?>
		  </ul>
		</div>
		<!-- End Points Breakdown -->
		
	  </div>
	  <!-- End Points -->
	  
	  <!-- Time -->
	  <div class="peach-bg-link" id="time-toggle" style="padding: 10px 20px 10px 20px; margin: 0 0 0 0; cursor:pointer;">
		<div style="display:block; margin-bottom:-15px;">
		  <h3 class="global-h2-white"><b>Time Exercised:</b></h3>
		</div>
		<!-- Hours -->
		<div style="display:inline-block; margin-right:15px;">
		  <h2 class="custom-font-big-white" style="margin-bottom:-20px;"><?php echo $hours = number_format(floor($time_total/3600)); ?></h2>  
		  <h3 class="global-h2-white" style="margin-top:-20px;"><b>Hours</b></h3> 
		</div>
		<!-- End Hours -->
		<!-- Minutes -->
		<div style="display:inline-block"> 
		  <h2 class="custom-font-big-white" style="margin-bottom:-20px;"><?php echo $minutes = floor(($time_total%3600)/60); ?></h2>  
		  <h3 class="global-h2-white" style="margin-top:-20px;"><b>Minutes</b></h3> 
		</div>
		<!-- End Minutes -->

		<!-- Time Breakdown -->
		<div id="time-breakdown" style="display:none;">
		  <ul class="global-p-white">
			<?php 
			  	foreach($breakdown['TIME'] as $key =>$activity){
					if($activity < 60){
					?>
						<li><?php echo ACTIVITY::activity_to_form($key).": ".number_format($activity)."s"; ?></li>
					<?php
					}else if ($activity < 3600){
						?>
						<li><?php echo ACTIVITY::activity_to_form($key).": ".number_format(($activity/60))."m"; ?></li>
						<?php
					}else{
						?>
						<li><?php echo ACTIVITY::activity_to_form($key).": ".number_format(($activity/3600))."h ".number_format((($time_total%3600)/60))."m"; ?></li>
						<?php
					}

					
				}
			  ?>
		  </ul>
		</div>
		<!-- End Time Breakdown -->
		
	  </div>
	  <!-- End Time -->
	  
	  <!-- Distance -->
	  <div class="green-bg-link" id="distance-toggle" style="padding: 10px 20px 10px 20px; margin: 0 0 0 0; cursor:pointer;">
		<div style="display:block; margin-bottom:-15px;">
		  <h3 class="global-h2-white"><b>Distance Traveled:</b></h3>
		</div>
		<!-- Miles -->
		<div style="display:inline-block; margin-right:15px;">
		  <h2 class="custom-font-big-white" style="margin-bottom:-20px;"><?php echo number_format($distance_total); ?></h2>  
		  <h3 class="global-h2-white" style="margin-top:-20px;"><b>Miles</b></h3> 
		</div>
		<!-- End Miles -->

		<!-- Distance Breakdown -->
		<div id="distance-breakdown" style="display:none;">
		  <ul class="global-p-white">
		  <?php 
			  	foreach($breakdown['DISTANCE'] as $key=>$activity){
					$DA = MSSQL::query('SELECT A_UNIT FROM ACTIVITY WHERE A_ID=\''.$key.'\'');
					$DA = odbc_result($DA, 'A_UNIT');
					if($DA ==1){
					?>
					<li><?php echo ACTIVITY::activity_to_form($key).': '.number_format($activity).' miles'; ?></li>
					<?php
					}
				}
			  ?>
		   
		  </ul>
		</div>
		<!-- End Distance Breakdown -->
		
	  </div>
	  
	  <!-- End Distance -->
	  
	  <?php
					$isAdmin = SESSIONS::isAdmin($_GET['session']);
					if($isAdmin){
						?>
					<br /><a href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].'/reporting_session.php?session='.$_GET['session'] ?>" class="tiny button">Generate Session Report</a>
					<?php
					}
					?>
			
		  </div>
		  
		  <!-- End Quick Stats Section -->
		  
	  
		 </div> 
	   
  
  <div class="row">
	<div class="large-12 medium-12 columns">
	
	  <!-- Members Section -->
	  
	  <h2 class="custom-font-small-blue">Members</h2>
			  <hr style="margin-top:10px; margin-bottom:0px;" />
			  <br />
			  <?php 
				$members = SESSIONS::list_users_in_session($_GET['session'], 4);
			  ?>
			  
			  
	  </div>  
	  <!-- End Members Section -->
	
	</div>
  
<!-- End Main Section -->  
</div>
<!-- End Main Section -->
<?php 
//STOPEDITING

//JAVASCRIPTS GO HERE
?>
	<div class="footer">
      <div class="row">
        <div>
          <a class="tiny button alert" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/contact-us.php"> Report a Bug </a>
        </div>
        
        <p class="global-p">
          <a href="http://www.facebook.com/walkgeorgia">Facebook</a> | <a href="https://twitter.com/walkga">Twitter</a> | <a href="http://blog.extension.uga.edu/walkgeorgia">Walk Georgia Blog</a>
        </p>
        <br />
          <small>The College of Agricultural and Environmental Sciences and the College of Family and Consumer Sciences cooperating.
The University of Georgia &copy; 2014. All Rights Reserved.</small>
          <hr><img alt="The University of Georgia Cooperative Extension" style="width:87px; height:31px;" src="../img/ext.png"><br><br>
        </div>
      </div>
    </div>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="../js/foundation.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/plug-ins/380cb78f450/integration/foundation/dataTables.foundation.js"></script>
<script type="text/javascript" src="../js/frontend.js"></script>
<script>
     $(document).foundation();
</script>
<script>
$(document).ready(function() {
    	  $('table.display').DataTable({
			"oLanguage": {
              "sEmptyTable": " "
            },
	        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
			responsive:true
		  });
		} );
</script>
</body>
</html>