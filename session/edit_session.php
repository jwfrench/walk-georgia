<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include_once ("$root/lib/sessions_api.php");
$create_group = SESSIONS::edit(filter_input(INPUT_POST, 'S_ID', FILTER_SANITIZE_STRING), filter_input(INPUT_POST, 'NAME', FILTER_SANITIZE_STRING), filter_input(INPUT_POST, 'DESC', FILTER_SANITIZE_STRING), filter_input(INPUT_POST, 'S_VIS', FILTER_SANITIZE_STRING));
$go_to_sessions = REDIRECT::sessions(filter_input(INPUT_POST, 'S_ID', FILTER_SANITIZE_STRING));
?>